/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FlexLayoutModule} from '@angular/flex-layout';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {
  DateAdapter,
  ErrorStateMatcher,
  MAT_DATE_LOCALE,
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatMenuModule,
  MatNativeDateModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatSelectModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  ShowOnDirtyErrorStateMatcher
} from '@angular/material';

import {AppComponent} from './app.component';
import {LoginService} from './services/login.service';
import {AlertService} from './services/alert.service';
import {AppRoutingModule} from './routing/app-routing.module';
import {CustomDateAdapter} from './util/custom-date-adapter';
import {HeaderComponent} from './components/core/header/header.component';
import {FooterComponent} from './components/core/footer/footer.component';
import {PathService} from './services/path.service';
import {CountryService} from './services/country.service';
import {OrganizationService} from './services/organization.service';
import {HandleOrganizationComponent} from './components/organization/handle-organization/handle-organization.component';
import {OrganizationComponent} from './components/organization/organization.component';
import {FormatOrganizationTypePipe} from './pipes/format-organization-type.pipe';
import {AdministrationComponent} from './components/administration/administration.component';
import {FormatOrganizationStatusPipe} from './pipes/format-organization-status.pipe';
import {HandleUserComponent} from './components/organization/user/handle-user/handle-user.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AlertComponent} from './components/core/alert/alert.component';
import {UserService} from './services/user.service';
import {LoginComponent} from './components/login/login.component';
import {RecoverComponent} from './components/login/recover-password/recover-password.component';
import {OrganizationBelongingPipe} from './pipes/organization-belonging.pipe';
import {CreatePasswordComponent} from './components/login/create-password/create-password/create-password.component';
import {StartComponent} from './components/start/start.component';
import {AuthService} from './auth/auth.service';
import {AuthGuard} from './auth/auth-guard.service';
import {ErrorInterceptor} from './auth/error-interceptor';
import {HandleProductComponent} from './components/product/product-tabs/handle-product/handle-product.component';
import {ProductComponent} from './components/product/product.component';
import {ProductService} from './services/product.service';
import {HandleArticleComponent} from './components/product/article-tabs/handle-article/handle-article.component';
import {FormatProductStatusPipe} from './pipes/format-product-status.pipe';
import {ProductTabsComponent} from './components/product/product-tabs/product-tabs.component';
import {ConnectionComponent} from './components/product/product-tabs/connection/connection.component';
import {ArticleTabsComponent} from './components/product/article-tabs/article-tabs.component';
import {CategoryDialogComponent} from './components/product/category-dialog/category-dialog.component';
import {ProductReplacementDialogComponent} from './components/product/product-tabs/handle-product/product-replacement-dialog/product-replacement-dialog.component';
import {ProfileComponent} from './components/profile/profile.component';
import {FormatArticleTypePipe} from './pipes/format-article-type.pipe';
import {ArticleConnectionComponent} from './components/product/article-tabs/article-connection/article-connection.component';
import {ConnectionDialogComponent} from './components/product/connection-dialog/connection-dialog.component';
import {AgreementComponent} from './components/agreement/agreement.component';
import {AgreementService} from './services/agreement.service';
import {HandleAgreementComponent} from './components/agreement/handle-agreement/handle-agreement.component';
import {AddUsersDialogComponent} from './components/general/add-users-dialog/add-users-dialog.component';
import {FormatAgreementStatusPipe} from './pipes/format-agreement-status.pipe';
import {ShareWithCustomerDialogComponent} from './components/agreement/handle-agreement/share-with-customer-dialog/share-with-customer-dialog.component';
import {HandlePricelistComponent} from './components/agreement/handle-pricelist/handle-pricelist.component';
import {AddPricelistRowDialogComponent} from './components/agreement/handle-pricelist/add-pricelist-row-dialog/add-pricelist-row-dialog.component';
import {FormatPricelistStatusPipe} from './pipes/format-pricelist-status.pipe';
import {FormatDeliveryDirectionPipe} from './pipes/format-delivery-direction.pipe';
import {HandleGeneralPricelistComponent} from './components/general-pricelist/handle-general-pricelist/handle-general-pricelist.component';
import {BusinesslevelComponent} from './components/organization/businesslevel/businesslevel.component';
import {AddBusinesslevelDialogComponent} from './components/organization/businesslevel/add-businesslevel-dialog/add-businesslevel-dialog.component';
import {UpdateBusinesslevelDialogComponent} from './components/organization/businesslevel/update-businesslevel-dialog/update-businesslevel-dialog.component';
import {PricelistDialogComponent} from './components/agreement/handle-agreement/pricelist-dialog/pricelist-dialog.component';
import {HandleGeneralPricelistPricelistComponent} from './components/general-pricelist/handle-general-pricelist-pricelist/handle-general-pricelist-pricelist.component';
import {GeneralPricelistPricelistDialogComponent} from './components/general-pricelist/general-pricelist-pricelist-dialog/general-pricelist-pricelist-dialog.component';
import {CommonService} from './services/common.service';
import {AutofocusModule} from 'angular-autofocus-fix';
import {UploadDialogComponent} from './components/product/upload-dialog/upload-dialog.component';
import {DeleteMediaDialogComponent} from './components/product/delete-media-dialog/delete-media-dialog.component';
import {DeleteArticleDialogComponent} from './components/product/article-tabs/handle-article/delete-article-dialog/delete-article-dialog.component';
import {DeleteProductDialogComponent} from './components/product/product-tabs/handle-product/delete-product-dialog/delete-product-dialog.component';
import {ExportProductsAndArticlesComponent} from './components/product/export-products-and-articles/export-products-and-articles.component';
import {ExportByProductsDialogComponent} from './components/product/export-products-and-articles/export-by-products-dialog/export-by-products-dialog.component';
import {ArticleReplacementDialogComponent} from './components/product/article-tabs/handle-article/article-replacement-dialog/article-replacement-dialog.component';
import {ImportDialogComponent} from './components/product/import-dialog/import-dialog.component';
import {SearchComponent} from './components/search/search.component';
import {ExportfileComponent} from './components/exportfile/exportfile.component';
import {GpDialogComponent} from './components/exportfile/gp-dialog/gp-dialog.component';
import {ExportsettingComponent} from './components/exportfile/exportsetting/exportsetting.component';
import {ExportService} from './services/export.service';
import {ExportSettingDialogComponent} from './components/exportfile/exportsetting/export-setting-dialog/export-setting-dialog.component';
import {HandleExportfileComponent} from './components/exportfile/handle-exportfile/handle-exportfile.component';
import {RemoveGpDialogComponent} from './components/exportfile/handle-exportfile/remove-gp-dialog/remove-gp-dialog.component';
import {ColumnsDialogComponent} from './components/search/columns-dialog/columns-dialog.component';
import {CacheInterceptor} from './auth/cache-interceptor';
import {HelptextService} from './services/helptext.service';
import {TokenLinkDialogComponent} from './components/organization/user/handle-user/token-link-dialog/token-link-dialog.component';
import {ImportPricelistDialogComponent} from './components/agreement/handle-pricelist/import-pricelist-dialog/import-pricelist-dialog.component';
import {ImportGeneralPricelistPricelistDialogComponent} from './components/general-pricelist/handle-general-pricelist-pricelist/import-general-pricelist-pricelist-dialog/import-general-pricelist-pricelist-dialog.component';
import {AddGeneralPricelistPricelistRowDialogComponent} from './components/general-pricelist/handle-general-pricelist-pricelist/add-general-pricelist-pricelist-row-dialog/add-general-pricelist-pricelist-row-dialog.component';
import {AssortmentComponent} from './components/assortment/assortment.component';
import {HandleAssortmentComponent} from './components/assortment/handle-assortment/handle-assortment.component';
import {AssortmentService} from './services/assortment.service';
import {AddArticlesToAssortmentComponent} from './components/assortment/handle-assortment/add-articles-to-assortment/add-articles-to-assortment.component';
import {ViewGeneralPricelistComponent} from './components/general-pricelist/view-general-pricelist/view-general-pricelist.component';
import {ArticlePricelistComponent} from './components/product/article-tabs/article-pricelist/article-pricelist.component';
import {AddArticleToPricelistDialogComponent} from './components/product/article-tabs/article-pricelist/add-article-to-pricelist-dialog/add-article-to-pricelist-dialog.component';
import {SendWelcomeMailDialogComponent} from './components/administration/send-welcome-mail-dialog/send-welcome-mail-dialog.component';
import {FormatExportSettingStatusPipe} from './pipes/format-export-setting-status';
import { ChangeBasedOnDialogComponent } from './components/product/article-tabs/handle-article/change-based-on-dialog/change-based-on-dialog.component';
import {DynamicTextService} from "./services/dynamictext.service";
import {AttachMessageDialogComponent} from "./components/product/attach-message-dialog/attach-message-dialog.component";

import { registerLocaleData } from '@angular/common';
import localeSv from '@angular/common/locales/sv';
import localeSvExtra from '@angular/common/locales/extra/sv';
registerLocaleData(localeSv, 'sv-SE', localeSvExtra);


@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,
    HeaderComponent,
    FooterComponent,
    OrganizationComponent,
    AdministrationComponent,
    HandleOrganizationComponent,
    HandleUserComponent,
    FormatAgreementStatusPipe,
    FormatArticleTypePipe,
    FormatDeliveryDirectionPipe,
    FormatOrganizationTypePipe,
    FormatOrganizationStatusPipe,
    FormatPricelistStatusPipe,
    FormatProductStatusPipe,
    FormatExportSettingStatusPipe,
    LoginComponent,
    RecoverComponent,
    OrganizationBelongingPipe,
    CreatePasswordComponent,
    StartComponent,
    HandleProductComponent,
    ProductComponent,
    HandleArticleComponent,
    ProductTabsComponent,
    ConnectionComponent,
    ArticleTabsComponent,
    CategoryDialogComponent,
    ProductReplacementDialogComponent,
    ProfileComponent,
    ArticleConnectionComponent,
    ConnectionDialogComponent,
    AgreementComponent,
    HandleAgreementComponent,
    AddUsersDialogComponent,
    ShareWithCustomerDialogComponent,
    HandlePricelistComponent,
    AddPricelistRowDialogComponent,
    HandleGeneralPricelistComponent,
    BusinesslevelComponent,
    AddBusinesslevelDialogComponent,
    UpdateBusinesslevelDialogComponent,
    PricelistDialogComponent,
    HandleGeneralPricelistPricelistComponent,
    GeneralPricelistPricelistDialogComponent,
    UploadDialogComponent,
    DeleteMediaDialogComponent,
    DeleteArticleDialogComponent,
    DeleteProductDialogComponent,
    ExportProductsAndArticlesComponent,
    ExportByProductsDialogComponent,
    ArticleReplacementDialogComponent,
    ImportDialogComponent,
    SearchComponent,
    ExportfileComponent,
    GpDialogComponent,
    ExportsettingComponent,
    ExportSettingDialogComponent,
    HandleExportfileComponent,
    RemoveGpDialogComponent,
    ColumnsDialogComponent,
    TokenLinkDialogComponent,
    ImportPricelistDialogComponent,
    ImportGeneralPricelistPricelistDialogComponent,
    AddGeneralPricelistPricelistRowDialogComponent,
    AssortmentComponent,
    HandleAssortmentComponent,
    AddArticlesToAssortmentComponent,
    ViewGeneralPricelistComponent,
    ArticlePricelistComponent,
    AddArticleToPricelistDialogComponent,
    SendWelcomeMailDialogComponent,
    ChangeBasedOnDialogComponent,
    AttachMessageDialogComponent
  ],
  entryComponents: [
    CategoryDialogComponent,
    ProductReplacementDialogComponent,
    ArticleReplacementDialogComponent,
    ConnectionDialogComponent,
    AddUsersDialogComponent,
    ShareWithCustomerDialogComponent,
    AddPricelistRowDialogComponent,
    AddBusinesslevelDialogComponent,
    UpdateBusinesslevelDialogComponent,
    PricelistDialogComponent,
    GeneralPricelistPricelistDialogComponent,
    UploadDialogComponent,
    DeleteMediaDialogComponent,
    DeleteArticleDialogComponent,
    DeleteProductDialogComponent,
    ExportByProductsDialogComponent,
    ImportDialogComponent,
    GpDialogComponent,
    ExportSettingDialogComponent,
    RemoveGpDialogComponent,
    ColumnsDialogComponent,
    TokenLinkDialogComponent,
    ImportPricelistDialogComponent,
    ImportGeneralPricelistPricelistDialogComponent,
    AddGeneralPricelistPricelistRowDialogComponent,
    AddArticleToPricelistDialogComponent,
    SendWelcomeMailDialogComponent,
    ChangeBasedOnDialogComponent,
    AttachMessageDialogComponent
  ],
  imports: [
    AutofocusModule,
    BrowserModule,
    HttpClientModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatToolbarModule,
    MatButtonModule,
    MatCardModule,
    MatChipsModule,
    MatDialogModule,
    MatExpansionModule,
    MatIconModule,
    MatMenuModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatSortModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSelectModule,
    MatNativeDateModule,
    MatTabsModule,
    MatTooltipModule,
    ReactiveFormsModule,
    AppRoutingModule,
  ],
  providers: [
    AuthService,
    AuthGuard,
    LoginService,
    AlertService,
    PathService,
    CountryService,
    OrganizationService,
    UserService,
    ProductService,
    AgreementService,
    CommonService,
    ExportService,
    AssortmentService,
    HelptextService,
    DynamicTextService,
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: CacheInterceptor, multi: true},
    {provide: MAT_DATE_LOCALE, useValue: 'sv'},
    {provide: DateAdapter, useClass: CustomDateAdapter},
    {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}
  ],
  bootstrap: [AppComponent]
})

export class AppModule {

}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../auth/auth-guard.service';

import {StartComponent} from '../components/start/start.component';
import {AdministrationComponent} from '../components/administration/administration.component';
import {HandleOrganizationComponent} from '../components/organization/handle-organization/handle-organization.component';
import {OrganizationComponent} from '../components/organization/organization.component';
import {HandleUserComponent} from '../components/organization/user/handle-user/handle-user.component';
import {LoginComponent} from '../components/login/login.component';
import {RecoverComponent} from '../components/login/recover-password/recover-password.component';
import {CreatePasswordComponent} from '../components/login/create-password/create-password/create-password.component';
import {ProductComponent} from '../components/product/product.component';
import {ProductTabsComponent} from '../components/product/product-tabs/product-tabs.component';
import {ArticleTabsComponent} from '../components/product/article-tabs/article-tabs.component';
import {ProfileComponent} from '../components/profile/profile.component';
import {AgreementComponent} from '../components/agreement/agreement.component';
import {HandleAgreementComponent} from '../components/agreement/handle-agreement/handle-agreement.component';
import {HandlePricelistComponent} from '../components/agreement/handle-pricelist/handle-pricelist.component';
import {HandleGeneralPricelistComponent} from '../components/general-pricelist/handle-general-pricelist/handle-general-pricelist.component';
import {HandleGeneralPricelistPricelistComponent} from '../components/general-pricelist/handle-general-pricelist-pricelist/handle-general-pricelist-pricelist.component';
import {ExportProductsAndArticlesComponent} from '../components/product/export-products-and-articles/export-products-and-articles.component';
import {SearchComponent} from '../components/search/search.component';
import {ExportfileComponent} from '../components/exportfile/exportfile.component';
import {ExportsettingComponent} from '../components/exportfile/exportsetting/exportsetting.component';
import {HandleExportfileComponent} from '../components/exportfile/handle-exportfile/handle-exportfile.component';
import {AssortmentComponent} from '../components/assortment/assortment.component';
import {HandleAssortmentComponent} from '../components/assortment/handle-assortment/handle-assortment.component';
import {AddArticlesToAssortmentComponent} from '../components/assortment/handle-assortment/add-articles-to-assortment/add-articles-to-assortment.component';
import {ViewGeneralPricelistComponent} from '../components/general-pricelist/view-general-pricelist/view-general-pricelist.component';

const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'login/password/create', component: CreatePasswordComponent},
  {path: 'login/password/recover', component: RecoverComponent},
  {path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},
  {path: 'exportsettings', component: ExportsettingComponent, canActivate: [AuthGuard]},
  {path: 'start', component: StartComponent},
  {path: 'organization', component: OrganizationComponent, canActivate: [AuthGuard]},
  {path: 'search', component: SearchComponent, canActivate: [AuthGuard]},
  {path: 'administration', component: AdministrationComponent, canActivate: [AuthGuard]},
  {path: 'generalpricelist', component: ViewGeneralPricelistComponent, canActivate: [AuthGuard]},
  {path: 'organization/handle', component: HandleOrganizationComponent, canActivate: [AuthGuard]},
  {path: 'organization/:id/handle', component: HandleOrganizationComponent, canActivate: [AuthGuard]},
  {path: 'organization/:id/handle/:tab', component: HandleOrganizationComponent, canActivate: [AuthGuard]},
  {path: 'organization/:organizationId/user/handle', component: HandleUserComponent, canActivate: [AuthGuard]},
  {path: 'organization/:organizationId/user/:userId/handle', component: HandleUserComponent, canActivate: [AuthGuard]},
  {path: 'organization/:organizationId/product', component: ProductComponent, canActivate: [AuthGuard]},
  {path: 'organization/:organizationId/product/export', component: ExportProductsAndArticlesComponent, canActivate: [AuthGuard]},
  {path: 'organization/:organizationId/product/handle', component: ProductTabsComponent, canActivate: [AuthGuard]},
  {path: 'organization/:organizationId/product/copy', component: ProductTabsComponent, canActivate: [AuthGuard]},
  {path: 'organization/:organizationId/product/:productId/handle', component: ProductTabsComponent, canActivate: [AuthGuard]},
  {path: 'organization/:organizationId/article/handle', component: ArticleTabsComponent, canActivate: [AuthGuard]},
  {path: 'organization/:organizationId/article/copy', component: ArticleTabsComponent, canActivate: [AuthGuard]},
  {path: 'organization/:organizationId/article/:articleId/handle', component: ArticleTabsComponent, canActivate: [AuthGuard]},
  {path: 'organization/:organizationId/agreement', component: AgreementComponent, canActivate: [AuthGuard]},
  {path: 'organization/:organizationId/agreement/handle', component: HandleAgreementComponent, canActivate: [AuthGuard]},
  {path: 'organization/:organizationId/agreement/:agreementId/handle', component: HandleAgreementComponent, canActivate: [AuthGuard]},
  {
    path: 'organization/:organizationId/agreement/:agreementId/pricelist/:pricelistId/handle',
    component: HandlePricelistComponent,
    canActivate: [AuthGuard]
  },
  {path: 'organization/:organizationId/generalpricelist', component: HandleGeneralPricelistComponent, canActivate: [AuthGuard]},
  {path: 'organization/:organizationId/generalpricelist/:id', component: HandleGeneralPricelistComponent, canActivate: [AuthGuard]},
  {
    path: 'organization/:organizationId/generalpricelist/:id/pricelist/:pricelistId',
    component: HandleGeneralPricelistPricelistComponent,
    canActivate: [AuthGuard]
  },
  {path: 'organization/:organizationId/exportfile', component: ExportfileComponent, canActivate: [AuthGuard]},
  {path: 'organization/:organizationId/exportfile/:exportSettingId', component: HandleExportfileComponent, canActivate: [AuthGuard]},
  {path: 'organization/:organizationId/assortment', component: AssortmentComponent, canActivate: [AuthGuard]},
  {path: 'organization/:organizationId/assortment/handle', component: HandleAssortmentComponent, canActivate: [AuthGuard]},
  {path: 'organization/:organizationId/assortment/:assortmentId/handle', component: HandleAssortmentComponent, canActivate: [AuthGuard]},
  {
    path: 'organization/:organizationId/assortment/:assortmentId/add-article',
    component: AddArticlesToAssortmentComponent,
    canActivate: [AuthGuard]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})

export class AppRoutingModule {
}

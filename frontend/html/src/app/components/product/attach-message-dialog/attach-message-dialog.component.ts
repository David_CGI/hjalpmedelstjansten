/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';


@Component({
  selector: 'app-attach-message-dialog-dialog',
  templateUrl: './attach-message-dialog.component.html',
  styleUrls: ['./attach-message-dialog.component.scss']
})
export class AttachMessageDialogComponent implements OnInit {

  loading = false;
  message = '';

  constructor(public dialogRef: MatDialogRef<AttachMessageDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
  }

  done() {
    this.message = this.message.replace(new RegExp('\n', 'g'), "QQQ");
    this.message = this.message.replace(new RegExp('\/', 'g'), "@@@");
    this.message = this.message.replace(new RegExp('\\\\', 'g'), "£££");
    this.message = encodeURIComponent(this.message);
    console.log('done has been pressed');
    //this.loading = true;
    if (this.message.length == 0)
      this.message='-';
    this.dialogRef.close(this.message);
    /*
    this.productService.deleteArticle(this.data.organizationId, this.data.articleId).subscribe(
    res => {
      this.alertService.clear();
      this.alertService.success('Sparat');
      this.dialogRef.close('success');
      this.loading = false;

    }, error => {
      this.alertService.clear();
      error.error.errors.forEach(err => {
        this.alertService.error(err);
        this.loading = false;
      });
    });
*/
  }

}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {ActivatedRoute} from '@angular/router';
import {ProductService} from '../../../../services/product.service';
import {Product} from '../../../../models/product/product.model';
import {ProductOrArticle} from '../../product.component';
import {HelptextService} from '../../../../services/helptext.service';
import {AuthService} from '../../../../auth/auth.service';
import {AlertService} from '../../../../services/alert.service';
import {Subject} from 'rxjs/Subject';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';

@Component({
  selector: 'app-connection',
  templateUrl: './connection.component.html',
  styleUrls: ['./connection.component.scss']
})
export class ConnectionComponent implements OnInit {

  displayedColumns = ['typ', 'benämning', 'artikelnummer', 'status', 'kategori', 'anchor'];

  redirectedFromSearch = false;
  loaded = false;
  loading = false;
  organizationId: number;
  productId: number;
  product: Product;
  articles = [];
  dataSource = null;
  query = '';
  tmpQuery = '';
  searchChanged: Subject<string> = new Subject<string>();
  queryParams = '';
  searchTotal;
  offset = 25;
  totalPages;
  currentPage = 1;
  paginatorArray = [];
  allPages;
  helpTexts;
  helpTextsLoaded = false;
  isOwnOrganization = false;

  // Filters
  filterH = false;
  filterT = false;
  filterR = false;
  filterTj = false;
  filterI = false;
  filterPublished = false;
  filterDiscontinued = false;

  constructor(private authService: AuthService,
              private productService: ProductService,
              private route: ActivatedRoute,
              private alertService: AlertService,
              private helpTextService: HelptextService) {
    this.route.params.subscribe(params => {
      this.organizationId = params.organizationId;
      this.productId = params.productId;
      Number(this.organizationId) === this.authService.getOrganizationId() ? this.isOwnOrganization = true : this.isOwnOrganization = false;
    });
    this.route.queryParams.subscribe(qParams => {
      if (qParams.search) {
        this.redirectedFromSearch = qParams.search;
      }
    });
    // The user is considered done with their search after 500 ms without further input
    this.searchChanged.pipe(
      debounceTime(500),
      distinctUntilChanged())
      .subscribe(query => {
          this.query = encodeURIComponent(query);
          this.onQueryChange();
        }
      );
  }

  ngOnInit() {
  }

  onQueryChanged(text: string) {
    this.searchChanged.next(text);
  }

  initConnection(productId) {
    this.productId = productId;
    this.getProduct();
  }

  getProduct() {
    if (!this.helpTextsLoaded) {
      this.helpTexts = this.helpTextService.getTexts().subscribe(
        texts => {
          this.helpTexts = texts;
          this.helpTextsLoaded = true;
        }
      );
    }
    this.productService.getProduct(this.organizationId, this.productId).subscribe(
      res => {
        this.product = res;
        this.loaded = true;
        this.searchArticles('');
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  onFilterChange() {
    this.queryParams = '';
    this.currentPage = 1;
    this.filterH ? this.queryParams += '&type=H' : this.queryParams = this.queryParams;
    this.filterT ? this.queryParams += '&type=T' : this.queryParams = this.queryParams;
    this.filterR ? this.queryParams += '&type=R' : this.queryParams = this.queryParams;
    this.filterTj ? this.queryParams += '&type=Tj' : this.queryParams = this.queryParams;
    this.filterI ? this.queryParams += '&type=I' : this.queryParams = this.queryParams;
    this.filterPublished ? this.queryParams += '&status=PUBLISHED' : this.queryParams = this.queryParams;
    this.filterDiscontinued ? this.queryParams += '&status=DISCONTINUED' : this.queryParams = this.queryParams;
    this.searchArticles(this.query + this.queryParams);
  }

  onQueryChange() {
    this.onFilterChange();
  }

  searchArticles(query) {
    this.loading = true;
    this.productService.searchArticles(query, this.organizationId, this.productId).subscribe(
      res => {
        this.searchTotal = res.headers.get('X-Total-Count');
        this.searchTotal !== '0' ? this.articles = res.body : this.articles = [];
        this.totalPages = Math.ceil(this.searchTotal / this.offset);
        this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
        this.dataSource = new MatTableDataSource<Article>(this.articles);
        this.generatePaginator();
        this.loading = false;
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  goToPage(page) {
    if (page !== '...' && page !== this.currentPage) {
      this.loading = true;
      this.currentPage = page;
      this.productService.searchArticles(
        this.query + this.queryParams + '&offset=' + (this.currentPage - 1) * this.offset, this.organizationId, this.productId).subscribe(
        res => {
          this.articles = res.body;
          this.dataSource = new MatTableDataSource<ProductOrArticle>(this.articles);
          this.currentPage = page;
          this.generatePaginator();
          this.loading = false;
        }, error => {
          this.alertService.clear();
          error.error.errors.forEach(err => {
            this.alertService.error(err);
          });
        }
      );
    }
  }

  generatePaginator() {
    let hasFirstEllipsis = false;
    let hasSecondEllipsis = false;
    this.paginatorArray = [];
    if (this.searchTotal > this.offset) {
      if (this.totalPages < 6) {
        this.paginatorArray = this.allPages;
      } else {
        this.allPages.forEach((pageNumber) => {
          if (pageNumber === 1
            || pageNumber === this.currentPage
            || pageNumber === this.totalPages
            || pageNumber === this.currentPage + 1
            || pageNumber === this.currentPage - 1) {
            this.paginatorArray.push(pageNumber);
          } else if (!hasFirstEllipsis && pageNumber < this.currentPage && (pageNumber !== this.currentPage - 1)) {
            this.paginatorArray.push('...');
            hasFirstEllipsis = true;
          } else if (!hasSecondEllipsis && pageNumber > this.currentPage && pageNumber !== this.currentPage + 1) {
            this.paginatorArray.push('...');
            hasSecondEllipsis = true;
          }
        });
      }
    }
  }

}

export interface Article {
  type: string;
  articleName: string;
  articleNumber: string;
  status: string;
  code: string;
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, OnInit, ViewChild} from '@angular/core';
import {HandleProductComponent} from './handle-product/handle-product.component';
import {ConnectionComponent} from './connection/connection.component';
import {ActivatedRoute} from '@angular/router';
import {ProductService} from "../../../services/product.service";
import * as fileSaver from "file-saver";
import {OrganizationService} from "../../../services/organization.service";
import {AlertService} from "../../../services/alert.service";
import {HelptextService} from "../../../services/helptext.service";
import { delay } from 'q';

@Component({
  selector: 'app-product-tabs',
  templateUrl: './product-tabs.component.html',
  styleUrls: ['./product-tabs.component.scss']
})
export class ProductTabsComponent implements OnInit {
  @ViewChild(HandleProductComponent) childProduct: HandleProductComponent;
  @ViewChild(ConnectionComponent) childProductConnection: ConnectionComponent;
  loaded = false;
  redirectedFromSearch = false;
  organizationId: number;
  tabIndex = 0;
  productId: number;
  hasArticleBasedOnIt = true;

  query = '';
  queryParams = '';
  helpTexts;
  helpTextsLoaded = false;

  private isCheckingIfProductIsRemovable: boolean;

  constructor(private route: ActivatedRoute,
              private productService: ProductService,
              private alertService: AlertService,
              private helpTextService: HelptextService) {
    this.route.queryParams.subscribe(qParams => {
      if (qParams.search) {
        this.redirectedFromSearch = qParams.search;
      }
    });
  }

  ngOnInit() {
    this.helpTexts = this.helpTextService.getTexts().subscribe(
      texts => {
        this.helpTexts = texts;
        this.helpTextsLoaded = true;
      }
    );

    this.organizationId = this.childProduct.organizationId;
    this.productId = this.childProduct.productId;
    if (this.productId) {
      this.isCheckingIfProductIsRemovable = true;
      this.productService.searchArticles('', this.organizationId, this.productId).subscribe(res => {
        this.hasArticleBasedOnIt = res.headers.get('X-Total-Count') !== '0';
        this.loaded = true;
      });

    }
    this.isCheckingIfProductIsRemovable = false;
  }



  toggleEditMode() {
    this.childProduct.toggleEditMode();
  }

  exportSearchResults() {
    const query = '?query=' + this.childProductConnection.query + this.childProductConnection.queryParams;

    this.productService.exportArticlesForProduct(this.organizationId, this.productId, query).subscribe(

      data => {
        fileSaver.saveAs(data.body, data.headers.get('Content-Disposition').split(';')[1].trim().split('=')[1]);
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  onTabSelection(event) {
    this.tabIndex = event.index;
    if (event.index === 1) {
      this.childProductConnection.product = this.childProduct.product;
      this.childProductConnection.productId = this.childProduct.productId;
      if (!this.childProductConnection.productId) {
        this.childProductConnection.initConnection(this.childProduct.productId);
      } else {
        this.childProductConnection.getProduct();
      }
    }
  }

}

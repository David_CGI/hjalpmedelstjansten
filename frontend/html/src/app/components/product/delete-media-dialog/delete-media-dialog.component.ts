/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {UploadDialogComponent} from '../upload-dialog/upload-dialog.component';
import {ProductService} from '../../../services/product.service';
import {AlertService} from '../../../services/alert.service';

@Component({
  selector: 'app-delete-media-dialog',
  templateUrl: './delete-media-dialog.component.html',
  styleUrls: ['./delete-media-dialog.component.scss']
})
export class DeleteMediaDialogComponent implements OnInit {

  loading = false;


  constructor(public dialogRef: MatDialogRef<UploadDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private productService: ProductService,
              private alertService: AlertService) {
  }

  ngOnInit() {
  }


  done() {
    console.log('done has been pressed');
    this.loading = true;
    if (this.data.productId) {
      this.productService.deleteMediaFromProduct(this.data.organizationId, this.data.productId, this.data.media.id).subscribe(
        res => {
          this.alertService.clear();
          this.alertService.success('Sparat');
          this.dialogRef.close('success');
          this.loading = false;
        }, error => {
          this.alertService.clear();
          error.error.errors.forEach(err => {
            this.alertService.error(err);
            this.loading = false;
          });
        });
    } else {
      this.productService.deleteMediaFromArticle(this.data.organizationId, this.data.articleId, this.data.media.id).subscribe(
        res => {
          this.alertService.clear();
          this.alertService.success('Sparat');
          this.dialogRef.close('success');
          this.loading = false;
        }, error => {
          this.alertService.clear();
          error.error.errors.forEach(err => {
            this.alertService.error(err);
            this.loading = false;
          });
        });
    }
  }

}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ProductService} from '../../../../../services/product.service';
import {AlertService} from '../../../../../services/alert.service';
import {Product} from '../../../../../models/product/product.model';
import {ElectronicAddress} from '../../../../../models/electronic-address.model';

@Component({
  selector: 'app-change-based-on-dialog',
  templateUrl: './change-based-on-dialog.component.html',
  styleUrls: ['./change-based-on-dialog.component.scss']
})
export class ChangeBasedOnDialogComponent implements OnInit {


  saving = false;
  selectedBasedOnProduct: Product;

  constructor(public dialogRef: MatDialogRef<ChangeBasedOnDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private productService: ProductService,
              private alertService: AlertService) {
  }

  ngOnInit() {
  }

  done() {
    this.saving = true;

    this.productService.switchBasedOn(this.data.organizationId, this.data.articleId, this.selectedBasedOnProduct.id).subscribe(
      res => {
        this.saving = false;
        this.alertService.success('Sparat');
        this.dialogRef.close({
          'article': res
        });
      }, error => {
        this.saving = false;
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

}

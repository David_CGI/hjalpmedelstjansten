/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatTableDataSource} from '@angular/material';
import {CategoryDialogComponent} from '../../../category-dialog/category-dialog.component';
import {ProductService} from '../../../../../services/product.service';
import {SelectionModel} from '@angular/cdk/collections';
import {SimpleArticle} from '../../../../../models/product/simple-article.model';
import {AlertService} from '../../../../../services/alert.service';
import {HelptextService} from '../../../../../services/helptext.service';
import {Subject} from 'rxjs/Subject';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {ElectronicAddress} from '../../../../../models/electronic-address.model';

@Component({
  selector: 'app-article-replacement-dialog',
  templateUrl: './article-replacement-dialog.component.html',
  styleUrls: ['./article-replacement-dialog.component.scss']
})
export class ArticleReplacementDialogComponent implements OnInit {

  displayedColumns = ['checkbox', 'typ', 'benämning', 'artikelnummer', 'status', 'kategori'];

  loading = true;
  saving = false;
  dataSource = null;
  searchTotal;
  offset = 25;
  totalPages;
  currentPage = 1;
  paginatorArray = [];
  allPages;
  query = '';
  tmpQuery = '';
  searchChanged: Subject<string> = new Subject<string>();
  additionalQuery = '&type=' + this.data.article.category.articleType + '&status=PUBLISHED';

  replaced = false;
  replacementDate: Date;
  articles: Array<SimpleArticle>;
  selection = new SelectionModel<SimpleArticle>(true, []);
  pageSelection = new SelectionModel<SimpleArticle>(true, []);
  helpTexts;
  helpTextsLoaded = false;

  constructor(public dialogRef: MatDialogRef<CategoryDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private productService: ProductService,
              private alertService: AlertService,
              private helpTextService: HelptextService) {
    // The user is considered done with their search after 500 ms without further input
    this.searchChanged.pipe(
      debounceTime(500),
      distinctUntilChanged())
      .subscribe(query => {
          this.query = encodeURIComponent(query);
          this.onQueryChange();
        }
      );
  }

  ngOnInit() {
    this.helpTexts = this.helpTextService.getTexts().subscribe(
      texts => {
        this.helpTexts = texts;
        this.helpTextsLoaded = true;
      }
    );
    this.productService.getArticlesByOrganization(this.query + this.additionalQuery, this.data.organizationId).subscribe(
      res => {
        this.searchTotal = res.headers.get('X-Total-Count');
        this.searchTotal !== '0' ? this.articles = res.body.filter(article => article.id !== this.data.article.id) : this.articles = [];
        this.totalPages = Math.ceil(this.searchTotal / this.offset);
        this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
        this.dataSource = new MatTableDataSource<SimpleArticle>(this.articles);
        this.generatePaginator();
        if (this.data.article.replacedByArticles && this.data.article.replacedByArticles.length) {
          this.replaced = true;
          this.data.article.replacedByArticles.forEach(art => {
            // Only care about id, name and number
            this.selection.select({
              'id': art.id,
              'type': 'ARTICLE',
              'articleType': art.category.articleType,
              'name': art.articleName,
              'number': art.articleNumber,
              'status': 'PUBLISHED',
              'code': art.category.code,
              'deleteable': null,
              'organizationName': null,
              'organizationId': null
            });
          });
          this.dataSource.data.forEach(row => {
            if (this.selection.selected.some(x => x.id === row.id)) {
              this.pageSelection.select(row);
              this.selection.selected.forEach(sel => {
                if (sel.id === row.id) {
                  this.selection.deselect(sel);
                  this.selection.select(row);
                }
              });
            }
          });
        }
      }, error => {
        this.loading = false;
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );

    if (this.data.article.replacementDate) {
      this.replacementDate = this.convertToDateString(this.data.article.replacementDate);
    }
  }

  onQueryChanged(text: string) {
    this.searchChanged.next(text);
  }

  onQueryChange() {
    this.loading = true;
    this.productService.getArticlesByOrganization(this.query + this.additionalQuery, this.data.organizationId).subscribe(
      res => {
        this.searchTotal = res.headers.get('X-Total-Count');
        this.searchTotal !== '0' ? this.articles = res.body.filter(article => article.id !== this.data.article.id) : this.articles = [];
        this.totalPages = Math.ceil(this.searchTotal / this.offset);
        this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
        this.dataSource = new MatTableDataSource<SimpleArticle>(this.articles);
        this.generatePaginator();
      }, error => {
        this.loading = false;
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  goToPage(page) {
    if (page !== '...' && page !== this.currentPage) {
      this.loading = true;
      this.currentPage = page;
      this.pageSelection.clear();
      this.productService.getArticlesByOrganization(
        this.query + this.additionalQuery + '&offset=' + (this.currentPage - 1) * this.offset, this.data.organizationId)
        .subscribe(
          res => {
            this.articles = res.body.filter(article => article.id !== this.data.article.id);
            this.dataSource = new MatTableDataSource<SimpleArticle>(this.articles);
            this.dataSource.data.forEach(row => {
              if (this.selection.selected.some(x => x.id === row.id)) {
                this.pageSelection.select(row);
              }
            });
            this.currentPage = page;
            this.generatePaginator();
          }, error => {
            this.alertService.clear();
            error.error.errors.forEach(err => {
              this.alertService.error(err);
            });
          }
        );
    }
  }

  generatePaginator() {
    let hasFirstEllipsis = false;
    let hasSecondEllipsis = false;
    this.paginatorArray = [];
    if (this.searchTotal > this.offset) {
      if (this.totalPages < 6) {
        this.paginatorArray = this.allPages;
      } else {
        this.allPages.forEach((pageNumber) => {
          if (pageNumber === 1
            || pageNumber === this.currentPage
            || pageNumber === this.totalPages
            || pageNumber === this.currentPage + 1
            || pageNumber === this.currentPage - 1) {
            this.paginatorArray.push(pageNumber);
          } else if (!hasFirstEllipsis && pageNumber < this.currentPage && (pageNumber !== this.currentPage - 1)) {
            this.paginatorArray.push('...');
            hasFirstEllipsis = true;
          } else if (!hasSecondEllipsis && pageNumber > this.currentPage && pageNumber !== this.currentPage + 1) {
            this.paginatorArray.push('...');
            hasSecondEllipsis = true;
          }
        });
      }
    }
    this.loading = false;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.pageSelection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {

    if (this.isAllSelected()) {
      this.pageSelection.clear();
      this.dataSource.data.forEach(row => {
        this.selection.selected.forEach(selectedRow => {
          if (selectedRow.id === row.id) {
            this.selection.deselect(selectedRow);
          }
        });
      });
    } else {
      this.dataSource.data.forEach(row => this.pageSelection.select(row));
      this.dataSource.data.forEach(row => {
        if (!this.selection.selected.some(x => x.id === row.id)) {
          this.selection.select(row);
        }
      });
    }
  }

  toggleSelections(element) {
    this.selection.toggle(element);
    this.pageSelection.toggle(element);
  }

  convertToDateString(date) {
    return new Date(date);
  }

  getMillisecondsFromDate(date): number {
    if (date) {
      return date.getTime();
    }
    return null;
  }

  done() {
    this.saving = true;
    this.data.article.replacedByArticles = [];
    this.data.article.replacementDate = this.getMillisecondsFromDate(this.replacementDate);
    if (this.replaced) {
      this.selection.selected.forEach(replacementArticle => {
        this.data.article.replacedByArticles.push(
          {'id': replacementArticle.id}
        );
      });
    }
    if (!this.data.article.inactivateRowsOnReplacement) {
      this.data.article.inactivateRowsOnReplacement = false;
    }
    this.productService.updateArticle(this.data.organizationId, this.data.article.id, this.data.article).subscribe(
      res => {
        this.saving = false;
        if (!res.manufacturerElectronicAddress) {
          res.manufacturerElectronicAddress = new ElectronicAddress();
        }
        this.alertService.success('Sparat');
        this.dialogRef.close({
          'article': res,
          'replaced': this.replaced,
        });
      }, error => {
        this.saving = false;
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

}

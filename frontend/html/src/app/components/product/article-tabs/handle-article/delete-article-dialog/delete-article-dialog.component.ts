/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ProductService} from "../../../../../services/product.service";
import {AlertService} from "../../../../../services/alert.service";


@Component({
  selector: 'app-delete-article-dialog',
  templateUrl: './delete-article-dialog.component.html',
  styleUrls: ['./delete-article-dialog.component.scss']
})
export class DeleteArticleDialogComponent implements OnInit {

  loading = false;

  constructor(public dialogRef: MatDialogRef<DeleteArticleDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private productService: ProductService,
              private alertService: AlertService) {
  }

  ngOnInit() {
  }

  done() {
    this.loading = true;
    this.productService.deleteArticle(this.data.organizationId, this.data.articleId).subscribe(
    res => {
      this.alertService.clear();
      this.alertService.success('Sparat');
      this.dialogRef.close('success');
      this.loading = false;

    }, error => {
      this.alertService.clear();
      error.error.errors.forEach(err => {
        this.alertService.error(err);
        this.loading = false;
      });
    });

  }

}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, OnInit} from '@angular/core';
import {ProductService} from '../../../../services/product.service';
import {ActivatedRoute} from '@angular/router';
import {Pricelistrow} from '../../../../models/agreement/pricelistrow.model';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {AuthService} from '../../../../auth/auth.service';
import {HasPricelistRowResponse} from '../../../../models/product/has-pricelist-row-response.model';
import {AddArticleToPricelistDialogComponent} from './add-article-to-pricelist-dialog/add-article-to-pricelist-dialog.component';
import {CommonService} from '../../../../services/common.service';
import {OrganizationService} from "../../../../services/organization.service";

@Component({
  selector: 'app-article-pricelist',
  templateUrl: './article-pricelist.component.html',
  styleUrls: ['./article-pricelist.component.scss']
})
export class ArticlePricelistComponent implements OnInit {
  displayedColumns = ['avtalsnummer', 'avtalsnamn', 'prislistenummer', 'pris', 'leverantör', 'kund', 'gäller från', 'status'];
  loaded = false;
  loading = false;
  organizationId: number;
  userOrganizationId: number;
  articleId: number;
  dataSource = null;
  dataSource2 = null;
  pricelistRows: Array<Pricelistrow>;
  generalPricelistPricelistRows: Array<Pricelistrow>;
  hasPricelistRows: HasPricelistRowResponse = new HasPricelistRowResponse();
  flexDirection: string;

  // Permissions
  hasPermissionToCreatePricelistRows = this.hasPermission('pricelistrow:create_own');
  hasPermissionToViewPricelistRows = this.hasPermission('pricelistrow:view_own');
  hasPermissionToViewPricelistRowsExists = this.hasPermission('pricelistrow:view_exists');
  hasPermissionToViewGeneralPricelistPricelistRows = this.hasPermission('generalpricelist_pricelistrow:view_own');
  hasPermissionToViewAllGeneralPricelistPricelistRows = this.hasPermission('generalpricelist_pricelistrow:view_all');
  isOwnOrganization = false;
  isSupplier = false;
  isCustomer = false;

  constructor(private authService: AuthService,
              private route: ActivatedRoute,
              private productService: ProductService,
              private organizationService: OrganizationService,
              private dialog: MatDialog,
              private commonService: CommonService) {
    this.commonService.isUsingIE() ? this.flexDirection = 'row' : this.flexDirection = 'column';
    this.route.params.subscribe(params => {
      this.organizationId = params.organizationId;
      this.articleId = params.articleId;
    });
    this.userOrganizationId = this.authService.getOrganizationId();
    Number(this.organizationId) === this.authService.getOrganizationId() ? this.isOwnOrganization = true : this.isOwnOrganization = false;
    this.hasPermissionToCreatePricelistRows && this.isOwnOrganization ?
      this.hasPermissionToCreatePricelistRows = true : this.hasPermissionToCreatePricelistRows = false;
    this.hasPermissionToViewGeneralPricelistPricelistRows && this.isOwnOrganization ?
      this.hasPermissionToViewGeneralPricelistPricelistRows = true : this.hasPermissionToViewGeneralPricelistPricelistRows = false;
  }

  hasPermission(permission): boolean {
    if (!localStorage.getItem('token')) {
      return false;
    }
    return this.authService.hasPermission(permission);
  }

  ngOnInit() {
    this.organizationService.getOrganization(this.authService.getOrganizationId()).subscribe(
      res => {
        res.organizationType === 'SUPPLIER' ? this.isSupplier = true : this.isSupplier = false;
        res.organizationType === 'CUSTOMER' ? this.isCustomer = true : this.isCustomer = false;
      }
    );
  }

  getPricelists(isSupplier) {
    this.loading = true;
    let loadingAgreementRows = true;
    let loadingGPRows = true;
    if (isSupplier) {
      this.hasPermissionToViewPricelistRows && this.isOwnOrganization ?
        this.hasPermissionToViewPricelistRows = true : this.hasPermissionToViewPricelistRows = false;
      this.hasPermissionToViewPricelistRowsExists && this.isOwnOrganization ?
        this.hasPermissionToViewPricelistRowsExists = true : this.hasPermissionToViewPricelistRowsExists = false;
    }
    if (this.hasPermissionToViewPricelistRows) {
      this.productService.getPricelistRowsForArticle(this.organizationId, this.articleId).subscribe(
        res => {
          this.pricelistRows = res;
          this.dataSource = new MatTableDataSource<Pricelistrow>(this.pricelistRows);
          loadingAgreementRows = false;
          if (!loadingGPRows) {
            this.loading = false;
            this.loaded = true;

          }
        }
      );
    } else if (this.hasPermissionToViewPricelistRowsExists) {
      this.productService.hasPricelistRowsForArticle(this.organizationId, this.articleId).subscribe(
        res => {
          this.hasPricelistRows = res;
          loadingAgreementRows = false;
          if (!loadingGPRows) {
            this.loading = false;
            this.loaded = true;

          }
        }
      );
    } else {
      loadingAgreementRows = false;
      if (!loadingGPRows) {
        this.loading = false;
        this.loaded = true;

      }
    }

    if (this.hasPermissionToViewGeneralPricelistPricelistRows || this.hasPermissionToViewAllGeneralPricelistPricelistRows) {
      this.productService.getGeneralPricelistPricelistRowsForArticle(this.organizationId, this.articleId).subscribe(
        data => {
          this.generalPricelistPricelistRows = data;
          this.dataSource2 = new MatTableDataSource<Pricelistrow>(this.generalPricelistPricelistRows);
          loadingGPRows = false;
          if (!loadingAgreementRows) {
            this.loading = false;
            this.loaded = true;

          }
        }
      );
    } else {
      this.loaded = true;
      loadingGPRows = false;
      if (!loadingAgreementRows) {
        this.loading = false;

      }
    }
  }

  openCreatePricelistRowDialog() {
    const dialogRef = this.dialog.open(AddArticleToPricelistDialogComponent, {
      width: '80%',
      data: {
        'organizationId': this.userOrganizationId,
        'articleId': this.articleId
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getPricelists(true);
      }
    });
  }

}

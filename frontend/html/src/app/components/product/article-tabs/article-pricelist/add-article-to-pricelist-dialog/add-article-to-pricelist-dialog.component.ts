/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, Inject, OnInit} from '@angular/core';
import {Pricelist} from '../../../../../models/agreement/pricelist.model';
import {AgreementService} from '../../../../../services/agreement.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {AlertService} from '../../../../../services/alert.service';
import {Pricelistrow} from '../../../../../models/agreement/pricelistrow.model';
import {Article} from '../../../../../models/product/article.model';

@Component({
  selector: 'app-add-article-to-pricelist-dialog',
  templateUrl: './add-article-to-pricelist-dialog.component.html',
  styleUrls: ['./add-article-to-pricelist-dialog.component.scss']
})
export class AddArticleToPricelistDialogComponent implements OnInit {
  loadingAgreements = false;
  loadingPricelists = false;
  saving = false;
  agreements = [];
  selectedAgreement: number;
  pricelists: Array<Pricelist>;
  selectedPricelist: number;

  constructor(private agreementService: AgreementService,
              private dialogRef: MatDialogRef<AddArticleToPricelistDialogComponent>,
              @Inject(MAT_DIALOG_DATA) private data: any,
              private alertService: AlertService) {
  }

  ngOnInit() {
    this.loadingAgreements = true;
    this.agreementService.searchAgreements(this.data.organizationId, '?limit=0').subscribe(
      agreements => {
        this.agreements = agreements.body;
        this.loadingAgreements = false;
      }, error => {
        this.loadingAgreements = false;
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  onAgreementSelection() {
    this.loadingPricelists = true;
    this.selectedPricelist = null;
    this.agreementService.getPriceLists(this.data.organizationId, this.selectedAgreement).subscribe(
      pricelists => {
        this.pricelists = pricelists;
        this.loadingPricelists = false;
      }
    );
  }

  done() {
    this.saving = true;
    const pricelistRows = new Array<Pricelistrow>();
    const pricelistRow = new Pricelistrow();
    pricelistRow.article = new Article();
    pricelistRow.article.id = this.data.articleId;
    pricelistRow.status = 'CREATED';
    pricelistRow.leastOrderQuantity = 1;
    pricelistRows.push(pricelistRow);
    this.agreementService.createPricelistRows(
      this.data.organizationId, this.selectedAgreement, this.selectedPricelist, pricelistRows).subscribe(
      res => {
        this.saving = false;
        this.alertService.success('Sparat');
        this.dialogRef.close(res);
      }, error => {
        this.saving = false;
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, OnInit} from '@angular/core';
import {ProductService} from '../../../../services/product.service';
import {ActivatedRoute} from '@angular/router';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {Article} from '../../../../models/product/article.model';
import {ConnectionDialogComponent} from '../../connection-dialog/connection-dialog.component';
import {AlertService} from '../../../../services/alert.service';
import {HelptextService} from '../../../../services/helptext.service';
import {AuthService} from '../../../../auth/auth.service';
import {Subject} from 'rxjs/Subject';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {CommonService} from '../../../../services/common.service';

@Component({
  selector: 'app-article-connection',
  templateUrl: './article-connection.component.html',
  styleUrls: ['./article-connection.component.scss']
})
export class ArticleConnectionComponent implements OnInit {

  displayedColumns = ['typ', 'benämning', 'produkt/artnr', 'status', 'kategori', 'disconnect', 'anchor'];

  redirectedFromSearch = false;
  loaded = false;
  loading = false;
  isOwnOrganization = false;
  organizationId: number;
  articleId: number;
  article: Article;
  connections = [];
  dataSource = null;
  query = '';
  tmpQuery = '';
  searchChanged: Subject<string> = new Subject<string>();
  queryParams = '';
  searchTotal;
  offset = 25;
  totalPages;
  currentPage = 1;
  paginatorArray = [];
  allPages;
  helpTexts;
  helpTextsLoaded = false;
  flexDirection: string;

  // Filters
  filterProducts = false;
  filterArticles = false;
  filterH = false;
  filterT = false;
  filterR = false;
  filterTj = false;
  filterI = false;
  filterPublished = false;
  filterDiscontinued = false;

  constructor(private authService: AuthService,
              private productService: ProductService,
              private route: ActivatedRoute,
              private connectionDialog: MatDialog,
              private alertService: AlertService,
              private helpTextService: HelptextService,
              private commonService: CommonService) {
    this.commonService.isUsingIE() ? this.flexDirection = 'row' : this.flexDirection = 'column';
    this.route.params.subscribe(params => {
      this.organizationId = params.organizationId;
      this.articleId = params.articleId;
      Number(this.organizationId) === this.authService.getOrganizationId() ? this.isOwnOrganization = true : this.isOwnOrganization = false;
    });
    this.route.queryParams.subscribe(qParams => {
      if (qParams.search) {
        this.redirectedFromSearch = qParams.search;
      }
    });
    // The user is considered done with their search after 500 ms without further input
    this.searchChanged.pipe(
      debounceTime(500),
      distinctUntilChanged())
      .subscribe(query => {
          this.query = encodeURIComponent(query);
          this.onQueryChange();
        }
      );
  }

  ngOnInit() {
  }

  getArticle() {
    if (!this.helpTextsLoaded) {
      this.helpTexts = this.helpTextService.getTexts().subscribe(
        texts => {
          this.helpTexts = texts;
          this.helpTextsLoaded = true;
        }
      );
    }
    this.productService.getArticle(this.organizationId, this.articleId).subscribe(
      res => {
        this.article = res;
        this.searchConnectedArticles('');
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  initConnections(article) {
    if (!this.helpTextsLoaded) {
      this.helpTexts = this.helpTextService.getTexts().subscribe(
        texts => {
          this.helpTexts = texts;
          this.helpTextsLoaded = true;
        }
      );
    }
    this.article = article;
    this.connections = [];
    this.checkFitsToProducts();
    this.checkFitsToArticles();
    this.loaded = true;
  }

  checkFitsToProducts() {
    if (this.article.fitsToProducts && this.article.fitsToProducts.length) {
      this.article.fitsToProducts.forEach((element) => {
        this.productService.getProduct(this.organizationId, element.id).subscribe(
          result => {
            this.connections.push({
              'articleType': result.category.articleType,
              'name': result.productName,
              'number': result.productNumber,
              'status': result.status,
              'code': result.category.code,
              'id': result.id,
              'type': 'PRODUCT'
            });
            this.dataSource = new MatTableDataSource<Connection>(this.connections);
          }, error => {
            this.alertService.clear();
            error.error.errors.forEach(err => {
              this.alertService.error(err);
            });
          }
        );
      });

    } else {
      this.article.fitsToProducts = [];
    }
  }

  checkFitsToArticles() {
    if (this.article.fitsToArticles && this.article.fitsToArticles.length) {
      this.article.fitsToArticles.forEach((element) => {
        this.productService.getArticle(this.organizationId, element.id).subscribe(
          result => {
            this.connections.push({
              'articleType': result.category.articleType,
              'name': result.articleName,
              'number': result.articleNumber,
              'status': result.status,
              'code': result.category.code,
              'id': result.id,
              'type': 'ARTICLE'
            });
            this.dataSource = new MatTableDataSource<Connection>(this.connections);
          }, error => {
            this.alertService.clear();
            error.error.errors.forEach(err => {
              this.alertService.error(err);
            });
          }
        );
      });

    } else {
      this.article.fitsToArticles = [];
    }
  }

  onQueryChanged(text: string) {
    this.searchChanged.next(text);
  }

  onQueryChange() {
    this.onFilterChange();
  }

  onFilterChange() {
    this.queryParams = '';
    this.currentPage = 1;
    if (!this.filterArticles) {
      this.filterH = false;
      this.filterT = false;
      this.filterR = false;
      this.filterTj = false;
      this.filterI = false;
    }
    this.filterProducts ? this.queryParams += '&includeProducts=true' : this.queryParams += '&includeProducts=false';
    this.filterArticles ? this.queryParams += '&includeArticles=true' : this.queryParams += '&includeArticles=false';
    this.filterH ? this.queryParams += '&type=H' : this.queryParams = this.queryParams;
    this.filterT ? this.queryParams += '&type=T' : this.queryParams = this.queryParams;
    this.filterR ? this.queryParams += '&type=R' : this.queryParams = this.queryParams;
    this.filterTj ? this.queryParams += '&type=Tj' : this.queryParams = this.queryParams;
    this.filterI ? this.queryParams += '&type=I' : this.queryParams = this.queryParams;
    this.filterPublished ? this.queryParams += '&status=PUBLISHED' : this.queryParams = this.queryParams;
    this.filterDiscontinued ? this.queryParams += '&status=DISCONTINUED' : this.queryParams = this.queryParams;
    this.searchConnectedArticles(this.query + this.queryParams);
  }

  searchConnectedArticles(query) {
    this.loading = true;
    this.productService.searchConnectedArticles(query, this.organizationId, this.articleId).subscribe(
      res => {
        this.searchTotal = res.headers.get('X-Total-Count');
        this.searchTotal !== '0' ? this.connections = res.body : this.connections = [];
        this.totalPages = Math.ceil(this.searchTotal / this.offset);
        this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
        this.dataSource = new MatTableDataSource<Connection>(this.connections);
        this.loaded = true;
        this.loading = false;
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  goToPage(page) {
    if (page !== '...' && page !== this.currentPage) {
      this.currentPage = page;
      this.productService.searchConnectedArticles(
        this.query + this.queryParams + '&offset=' + (this.currentPage - 1) * this.offset, this.organizationId, this.articleId).subscribe(
        res => {
          this.connections = res.body;
          this.dataSource = new MatTableDataSource<Connection>(this.connections);
          this.currentPage = page;
          this.generatePaginator();
        }, error => {
          this.alertService.clear();
          error.error.errors.forEach(err => {
            this.alertService.error(err);
          });
        }
      );
    }
  }

  generatePaginator() {
    let hasFirstEllipsis = false;
    let hasSecondEllipsis = false;
    this.paginatorArray = [];
    if (this.searchTotal > this.offset) {
      if (this.totalPages < 6) {
        this.paginatorArray = this.allPages;
      } else {
        this.allPages.forEach((pageNumber) => {
          if (pageNumber === 1
            || pageNumber === this.currentPage
            || pageNumber === this.totalPages
            || pageNumber === this.currentPage + 1
            || pageNumber === this.currentPage - 1) {
            this.paginatorArray.push(pageNumber);
          } else if (!hasFirstEllipsis && pageNumber < this.currentPage && (pageNumber !== this.currentPage - 1)) {
            this.paginatorArray.push('...');
            hasFirstEllipsis = true;
          } else if (!hasSecondEllipsis && pageNumber > this.currentPage && pageNumber !== this.currentPage + 1) {
            this.paginatorArray.push('...');
            hasSecondEllipsis = true;
          }
        });
      }
    }
    this.loading = false;
  }

  openConnectionDialog() {
    const dialogRef = this.connectionDialog.open(ConnectionDialogComponent, {
      width: '80%',
      data: {
        'organizationId': this.organizationId,
        'articleType': this.article.category.articleType,
        'article': this.article
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        if (!this.articleId) {
          this.articleId = result.id;
          this.article.id = result.id;
        }
        this.searchConnectedArticles(this.query);
      }
    });
  }

  onRowClick(type) {
    if (type === 'ARTICLE') {
      location.reload();
    }
  }

  removeConnection(element) {
    this.productService.removeConnection(this.organizationId, this.articleId, element.id).subscribe(
      res => {
        this.article = res;
        this.searchConnectedArticles(this.query);
        this.alertService.success('Koppling borttagen');
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

}

export interface Connection {
  type: string;
  name: string;
  number: string;
  status: string;
  code: string;
  deleteable: boolean;
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, OnInit, ViewChild} from '@angular/core';
import {HandleArticleComponent} from './handle-article/handle-article.component';
import {ProductService} from '../../../services/product.service';
import {ActivatedRoute} from '@angular/router';
import {Product} from '../../../models/product/product.model';
import {Article} from '../../../models/product/article.model';
import {ArticleConnectionComponent, Connection} from './article-connection/article-connection.component';
import {AlertService} from '../../../services/alert.service';
import {OrganizationService} from '../../../services/organization.service';
import {AuthService} from '../../../auth/auth.service';
import {ArticlePricelistComponent} from './article-pricelist/article-pricelist.component';
import {HasPricelistRowResponse} from "../../../models/product/has-pricelist-row-response.model";
import * as fileSaver from "file-saver";
import {HelptextService} from '../../../services/helptext.service';

@Component({
  selector: 'app-article-tabs',
  templateUrl: './article-tabs.component.html',
  styleUrls: ['./article-tabs.component.scss']
})
export class ArticleTabsComponent implements OnInit {
  @ViewChild(HandleArticleComponent) childArticle: HandleArticleComponent;
  @ViewChild(ArticleConnectionComponent) childArticleConnection: ArticleConnectionComponent;
  @ViewChild(ArticlePricelistComponent) childArticlePricelist: ArticlePricelistComponent;

  redirectedFromSearch = false;
  loaded = false;
  tabIndex = 0;
  organizationId: number;
  productId: number;
  articleId: number;
  product: Product;
  article: Article;
  haveConnectedArticles: boolean;
  hasPricelistRows: HasPricelistRowResponse = new HasPricelistRowResponse();
  isLeafNode = false;

  private isCheckingIfArticleIsRemovable: boolean;

  query = '';
  queryParams = '';
  helpTexts;
  helpTextsLoaded = false;

  // Permissions
  hasPermissionToViewPricelistRows = this.hasPermission('pricelistrow:view_own');
  hasPermissionToViewPricelistRowsExists = this.hasPermission('pricelistrow:view_exists');
  hasPermissionToViewGeneralPricelistPricelistRows = this.hasPermission('generalpricelist_pricelistrow:view_own');
  hasPermissionToViewAllGeneralPricelistPricelistRows = this.hasPermission('generalpricelist_pricelistrow:view_all');
  isSupplier = false;
  isOwnOrganization = false;


  constructor(private route: ActivatedRoute,
              private authService: AuthService,
              private productService: ProductService,
              private organizationService: OrganizationService,
              private alertService: AlertService,
              private helpTextService: HelptextService) {
    this.route.params.subscribe(params => {
      this.organizationId = params.organizationId;
      this.articleId = params.articleId;
    });
    this.route.queryParams.subscribe(qParams => {
      this.productId = qParams.productid;
      if (qParams.search) {
        this.redirectedFromSearch = qParams.search;
      }
    });
    Number(this.organizationId) === this.authService.getOrganizationId() ? this.isOwnOrganization = true : this.isOwnOrganization = false;
    this.hasPermissionToViewGeneralPricelistPricelistRows && this.isOwnOrganization ?
      this.hasPermissionToViewGeneralPricelistPricelistRows = true : this.hasPermissionToViewGeneralPricelistPricelistRows = false;
  }

  hasPermission(permission): boolean {
    if (!localStorage.getItem('token')) {
      return false;
    }
    return this.authService.hasPermission(permission);
  }

  ngOnInit() {
    this.helpTexts = this.helpTextService.getTexts().subscribe(
      texts => {
        this.helpTexts = texts;
        this.helpTextsLoaded = true;
      }
    );

    this.organizationService.getOrganization(this.authService.getOrganizationId()).subscribe(
      res => {
        res.organizationType === 'SUPPLIER' ? this.isSupplier = true : this.isSupplier = false;
        if (this.isSupplier) {
          this.hasPermissionToViewPricelistRows && this.isOwnOrganization ?
            this.hasPermissionToViewPricelistRows = true : this.hasPermissionToViewPricelistRows = false;
          this.hasPermissionToViewPricelistRowsExists && this.isOwnOrganization ?
            this.hasPermissionToViewPricelistRowsExists = true : this.hasPermissionToViewPricelistRowsExists = false;
        }
        if (this.productId) {
          this.productService.getProduct(this.organizationId, this.productId).subscribe(
            product => {
              this.product = product;
              this.loaded = true;
            }, error => {
              this.alertService.clear();
              error.error.errors.forEach(err => {
                this.alertService.error(err);
              });
            }
          );
        } else {
          this.loaded = true;
        }
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
    if (this.articleId){
      this.productService.hasPricelistRowsForArticle(this.organizationId, this.articleId).subscribe(
        res => {
          this.hasPricelistRows = res;
        }
      );
    }
    if(this.articleId)
    {
      this.productService.getArticle(this.organizationId, this.articleId).subscribe(
        res => {
          let articleType = res.category.articleType.toLowerCase();
          if(articleType == "t" || articleType == "r" || articleType == "i" || articleType == "tj")
          {
             this.isLeafNode = true;
          }
        });

      this.isCheckingIfArticleIsRemovable = true;
      this.productService.searchConnectedArticles(this.query,this.organizationId,this.articleId).subscribe(
        res =>
        {
          this.haveConnectedArticles = false;
          if(res.body)
          {
            res.body.forEach(item => {
                if(item['deleteable'])
                {
                  this.haveConnectedArticles = true;
                }
              }
            )
          }
        }
      )
    }
    this.isCheckingIfArticleIsRemovable = false;
  }

  toggleEditMode() {
    this.childArticle.toggleEditMode();
  }

  exportSearchResults() {
    const query = '?query=' + this.childArticleConnection.query + this.childArticleConnection.queryParams;

    this.productService.exportProductsAndArticlesForArticle(this.organizationId, this.articleId, query).subscribe(

      data => {
        fileSaver.saveAs(data.body, data.headers.get('Content-Disposition').split(';')[1].trim().split('=')[1]);
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  onTabSelection(event, i) {
    this.tabIndex = event.index;
    this.childArticle.tabIndex = event.index;
    if (event.index === 1) {
      this.childArticleConnection.isOwnOrganization = this.childArticle.isOwnOrganization;
      this.article = this.childArticle.article;
      if (this.article.ceMarked) {
        this.article.ceDirective = this.childArticle.selectedCeDirective;
      }
      this.article.articleNumber = this.childArticle.articleNumberFormControl.value;
      this.article.articleName = this.childArticle.articleNameFormControl.value;
      this.article.basedOnProduct = this.childArticle.selectedBasedOnProduct;
      this.article.ceStandard = this.childArticle.selectedCeStandard;
      if (this.childArticle.selectedPreventiveMaintenanceValidity) {
        this.article.preventiveMaintenanceValidFrom = this.childArticle.selectedPreventiveMaintenanceValidity;
      } else {
        this.article.preventiveMaintenanceValidFrom = null;
      }
      this.article.orderUnit = this.childArticle.selectedOrderUnit;
      this.article.articleQuantityInOuterPackageUnit = this.childArticle.selectedArticleQuantityInOuterPackageUnit;
      this.article.packageContentUnit = this.childArticle.selectedPackageContentUnit;
      if (!this.childArticle.articleId) {
        this.childArticleConnection.initConnections(this.article);
      } else {
        this.childArticleConnection.articleId = this.childArticle.articleId;
        this.childArticleConnection.getArticle();
      }
    } else if (event.index === 0 && this.childArticleConnection.articleId) {
      this.childArticle.loaded = false;
      if (i === 1) {
        this.articleId = this.childArticleConnection.articleId;
        this.article.id = this.childArticleConnection.articleId;
        this.childArticle.articleId = this.childArticleConnection.articleId;
        this.childArticle.article.id = this.childArticleConnection.articleId;
        this.childArticle.article.fitsToArticles = this.childArticleConnection.article.fitsToArticles;
        this.childArticle.article.fitsToProducts = this.childArticleConnection.article.fitsToProducts;
        this.childArticle.getMedia();
      } else {
        this.childArticle.initialize();
      }
    }
    if (event.index === 2) {
      this.childArticlePricelist.getPricelists(this.isSupplier);
    }
  }

}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, OnInit} from '@angular/core';
import {LoginService} from '../../services/login.service';
import {AuthService} from '../../auth/auth.service';
import {UserService} from '../../services/user.service';
import {CommonService} from '../../services/common.service';
import {DynamicTextService} from "../../services/dynamictext.service";
import {DynamicText} from "../../models/dynamictext.model";

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss']
})
export class StartComponent implements OnInit {
  flexDirection: string;
  dynamicTextLoaded = false;
  dynamicText: DynamicText;

  constructor(private authService: AuthService,
              private dynamicTextService: DynamicTextService,
              private loginService: LoginService,
              private userService: UserService,
              private commonService: CommonService) {
    this.commonService.isUsingIE() ? this.flexDirection = 'row' : this.flexDirection = 'column';
  }

  ngOnInit() {
    this.userService.getProfile().subscribe(
      res => {
        this.authService.login(res);
      }
    );
    this.dynamicTextService.getDynamicText(1).subscribe(
      data => {
        this.dynamicText = data;
        this.dynamicTextLoaded = true;
      }
    );
  }
}

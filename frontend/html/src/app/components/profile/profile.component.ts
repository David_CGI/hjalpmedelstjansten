/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, OnInit} from '@angular/core';
import {UserService} from '../../services/user.service';
import {User} from '../../models/user/user.model';
import {OrganizationService} from '../../services/organization.service';
import {Organization} from '../../models/organization/organization.model';
import {FormControl, Validators} from '@angular/forms';
import {AlertService} from '../../services/alert.service';
import {ElectronicAddress} from '../../models/electronic-address.model';
import {HelptextService} from '../../services/helptext.service';
import {CommonService} from '../../services/common.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  loaded = false;
  saving = false;
  editMode = false;
  userProfile: User;
  organization: Organization;
  admins: Array<User>;
  businessLevels = '';
  editEmailMode = false;
  cachedEmail = '';
  validTo: Date;
  validFrom: Date;
  adminString = '';
  helpTexts;
  helpTextsLoaded = false;
  flexDirection: string;

  // Form controls
  emailFormControl = new FormControl('', {
    updateOn: 'blur'
  });
  repeatedEmailFormControl = new FormControl('', {
    updateOn: 'blur'
  });

  emailError = '';
  repeatedEmailError;

  constructor(private userService: UserService,
              private organizationService: OrganizationService,
              private alertService: AlertService,
              private helpTextService: HelptextService,
              private commonService: CommonService) {
    this.commonService.isUsingIE() ? this.flexDirection = 'row' : this.flexDirection = 'column';
  }

  ngOnInit() {
    this.helpTexts = this.helpTextService.getTexts().subscribe(
      texts => {
        this.helpTexts = texts;
        this.helpTextsLoaded = true;
      }
    );
    this.getProfile();
    this.onEditModeChange();
  }

  getProfile() {
    this.adminString = '';
    this.businessLevels = '';
    this.userService.getProfile().subscribe(
      res => {
        this.userProfile = res;
        this.emailFormControl.setValue(res.electronicAddress.email);
        this.organizationService.getOrganization(res.userEngagements[0].organizationId).subscribe(
          org => {
            this.organization = org;
            this.loaded = true;
          }, error => {
            this.alertService.clear();
            error.error.errors.forEach(err => {
              this.alertService.error(err);
            });
          }
        );
        this.userService.getUserByRoles(this.userProfile.userEngagements[0].organizationId, 'Customeradmin&roleName=Supplieradmin')
          .subscribe(
            admins => {
              this.admins = admins;
              this.admins.forEach((e) => {
                if (this.adminString) {
                  this.adminString += ' eller ';
                }
                this.adminString += e.firstName + ' ' + e.lastName;
              });
            }, error => {
              this.alertService.clear();
              error.error.errors.forEach(err => {
                this.alertService.error(err);
              });
            }
          );
        if (res.userEngagements[0].businessLevels) {
          res.userEngagements[0].businessLevels.forEach((e) => {
            if (this.businessLevels) {
              this.businessLevels += '\n';
            }
            this.businessLevels += e.name;
          });
        }
        if (!this.userProfile.electronicAddress) {
          this.userProfile.electronicAddress = new ElectronicAddress();
        }
        this.convertToDateString();
      }
    );
  }

  toggleEditMode() {
    this.editMode = !this.editMode;
    this.onEditModeChange();
  }

  onEditModeChange() {
    if (!this.editMode) {
      this.editEmailMode = false;
      this.emailFormControl.disable();
      this.repeatedEmailFormControl.setValue('');
      this.repeatedEmailFormControl.disable();
    } else {
      if (this.editEmailMode) {
        this.emailFormControl.enable();
      } else {
        this.emailFormControl.disable();
      }
      this.repeatedEmailFormControl.enable();
    }
  }

  toggleEditEmailMode() {
    if (!this.editEmailMode) {
      this.cachedEmail = this.emailFormControl.value;
    } else {
      this.emailFormControl.setValue(this.cachedEmail);
      this.repeatedEmailFormControl.setValue('');
    }
    this.editEmailMode = !this.editEmailMode;
    this.onEditModeChange();
  }

  isValidEmail(): boolean {
    if (this.editEmailMode) {
      if (this.emailFormControl.value === this.repeatedEmailFormControl.value) {
        return true;
      } else {
        this.repeatedEmailError = 'E-post och upprepad E-post överensstämmer inte';
        this.repeatedEmailFormControl.setErrors(Validators.pattern(''));
        this.alertService.clear();
        this.alertService.errorWithMessage(this.repeatedEmailError);
        return false;
      }
    }
    return true;
  }

  convertToDateString() {
    this.validFrom = new Date(this.userProfile.userEngagements[0].validFrom);
    if (this.userProfile.userEngagements[0].validTo) {
      this.validTo = new Date(this.userProfile.userEngagements[0].validTo);
    }
  }

  saveProfile() {
    if (this.isValidEmail()) {
      this.saving = true;
      this.userProfile.electronicAddress.email = this.emailFormControl.value;
      this.userService.updateUser(this.organization.id, this.userProfile.id, this.userProfile).subscribe(
        res => {
          this.editEmailMode = false;
          this.editMode = false;
          this.onEditModeChange();
          this.saving = false;
          this.alertService.success('Sparat');
        }, error => {
          this.alertService.clear();
          this.saving = false;
          error.error.errors.forEach(err => {
            this.alertService.error(err);
          });
        }
      );
    }
  }

  cancel() {
    this.getProfile();
    this.toggleEditMode();
  }

}

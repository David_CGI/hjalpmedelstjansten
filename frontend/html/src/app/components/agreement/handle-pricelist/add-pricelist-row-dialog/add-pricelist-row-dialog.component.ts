/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, Inject, OnInit} from '@angular/core';
import {AuthService} from '../../../../auth/auth.service';
import {MAT_DIALOG_DATA, MatDialogRef, MatTableDataSource} from '@angular/material';
import {Article} from '../../../../models/product/article.model';
import {ActivatedRoute} from '@angular/router';
import {SelectionModel} from '@angular/cdk/collections';
import {HelptextService} from '../../../../services/helptext.service';
import {AgreementService} from '../../../../services/agreement.service';
import {AlertService} from '../../../../services/alert.service';
import {Subject} from 'rxjs/Subject';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {CommonService} from '../../../../services/common.service';

import { ViewChild } from '@angular/core';
import { MatTable } from '@angular/material';

@Component({
  selector: 'app-add-pricelist-row-dialog',
  templateUrl: './add-pricelist-row-dialog.component.html',
  styleUrls: ['./add-pricelist-row-dialog.component.scss']
})
export class AddPricelistRowDialogComponent implements OnInit {
  displayedColumns = ['add', 'typ', 'benämning', 'artikelnummer', 'status', 'kategori'];

  displayedColumns2 = ['remove', 'typ', 'benämning', 'artikelnummer', 'status', 'kategori'];

  @ViewChild(MatTable) table2: MatTable<any>;

  loading = true;
  loading2 = false;
  show: boolean;
  organizationId: number;
  agreementId: number;
  pricelistId: number;
  query = '';
  tmpQuery = '';
  searchChanged: Subject<string> = new Subject<string>();
  queryParams = '';
  searchTotal;
  offset = 25;
  totalPages;
  totalPages2;
  currentPage = 1;
  currentPage2 = 1;
  paginatorArray = [];
  paginatorArray2 = [];
  allPages;
  allPages2;
  dataSource = null;
  articles = [];
  searchSelection = new SelectionModel<Article>(true, []);

  dataSource2 = null;
  markedArticles = [];

  helpTexts;
  flexDirection;

  // Filters
  filterH = false;
  filterT = false;
  filterR = false;
  filterTJ = false;
  filterI = false;
  filterPublished = false;
  filterDiscontinued = false;

  constructor(public dialogRef: MatDialogRef<AddPricelistRowDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private route: ActivatedRoute,
              private authService: AuthService,
              private agreementService: AgreementService,
              private helpTextService: HelptextService,
              private alertService: AlertService,
              private commonService: CommonService) {
    this.commonService.isUsingIE() ? this.flexDirection = 'row' : this.flexDirection = 'column';
    this.organizationId = data.organizationId;
    this.agreementId = data.agreementId;
    this.pricelistId = data.pricelistId;

    // The user is considered done with their search after 500 ms without further input
    this.searchChanged.pipe(
      debounceTime(500),
      distinctUntilChanged())
      .subscribe(query => {
          this.query = encodeURIComponent(query);
          this.onQueryChange();
        }
      );
  }

  ngOnInit() {
    this.helpTexts = this.helpTextService.getTexts().subscribe(
      texts => {
        this.helpTexts = texts;
      }
    );
    this.searchArticles('');
    this.dataSource2 = new MatTableDataSource<Article>();
    this.show = false;
  }

  onQueryChanged(text: string) {
    this.searchChanged.next(text);
  }

  onQueryChange() {
    this.onFilterChange();
  }

  onFilterChange() {
    this.queryParams = '';
    this.currentPage = 1;
    this.filterH ? this.queryParams += '&type=H' : this.queryParams = this.queryParams;
    this.filterT ? this.queryParams += '&type=T' : this.queryParams = this.queryParams;
    this.filterR ? this.queryParams += '&type=R' : this.queryParams = this.queryParams;
    this.filterTJ ? this.queryParams += '&type=Tj' : this.queryParams = this.queryParams;
    this.filterI ? this.queryParams += '&type=I' : this.queryParams = this.queryParams;
    this.filterPublished ? this.queryParams += '&status=PUBLISHED' : this.queryParams = this.queryParams;
    this.filterDiscontinued ? this.queryParams += '&status=DISCONTINUED' : this.queryParams = this.queryParams;
    this.searchArticles(this.query + this.queryParams);
  }

  searchArticles(query) {
    this.loading = true;
    this.agreementService.searchArticlesForPricelist(query, this.organizationId, this.agreementId, this.pricelistId).subscribe(
      res => {
        this.searchTotal = res.headers.get('X-Total-Count');
        this.searchTotal !== '0' ? this.articles = res.body : this.articles = [];
        this.totalPages = Math.ceil(this.searchTotal / this.offset);
        this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
        this.dataSource = new MatTableDataSource<Article>(this.articles);
        this.generatePaginator();
      }
    );
  }

  hasPermission(permission): boolean {
    if (!localStorage.getItem('token')) {
      return false;
    }
    return this.authService.hasPermission(permission);
  }

  showSelected(){
    this.show = true;
  }

  hideSelected(){
    this.show = false;
  }

  goToPage(page) {
    this.loading = true;
    if (page !== '...' && page !== this.currentPage) {
      this.currentPage = page;
      this.searchSelection.clear();
      this.agreementService.searchArticlesForPricelist(
        this.query + this.queryParams + '&offset=' +
        (this.currentPage - 1) * this.offset, this.organizationId, this.agreementId, this.pricelistId)
        .subscribe(
          res => {
            this.articles = res.body;
            this.dataSource = new MatTableDataSource<Article>(this.articles);
            this.currentPage = page;
            this.generatePaginator();
          }, error => {
            this.alertService.clear();
            error.error.errors.forEach(err => {
              this.alertService.error(err);
            });
          }
        );
    }
  }

  goToPage2(page) {
    //this.loading2 = true;
    if (page !== '...' && page !== this.currentPage2) {
      this.currentPage2 = page;

      let offset = (this.currentPage2 - 1) * this.offset;
      let endoffset = this.currentPage2 * this.offset;

      this.dataSource2.data = this.markedArticles.slice(offset, endoffset);
      this.currentPage2 = page;
      this.generatePaginator2();
    }
  }

  generatePaginator() {
    let hasFirstEllipsis = false;
    let hasSecondEllipsis = false;
    this.paginatorArray = [];
    if (this.searchTotal > this.offset) {
      if (this.totalPages < 6) {
        this.paginatorArray = this.allPages;
      } else {
        this.allPages.forEach((pageNumber) => {
          if (pageNumber === 1
            || pageNumber === this.currentPage
            || pageNumber === this.totalPages
            || pageNumber === this.currentPage + 1
            || pageNumber === this.currentPage - 1) {
            this.paginatorArray.push(pageNumber);
          } else if (!hasFirstEllipsis && pageNumber < this.currentPage && (pageNumber !== this.currentPage - 1)) {
            this.paginatorArray.push('...');
            hasFirstEllipsis = true;
          } else if (!hasSecondEllipsis && pageNumber > this.currentPage && pageNumber !== this.currentPage + 1) {
            this.paginatorArray.push('...');
            hasSecondEllipsis = true;
          }
        });
      }
    }
    this.loading = false;
  }

  generatePaginator2() {
    let hasFirstEllipsis = false;
    let hasSecondEllipsis = false;
    this.paginatorArray2 = [];
    if (this.markedArticles.length > this.offset) {
      if (this.totalPages2 < 6) {
        this.paginatorArray2 = this.allPages2;
      } else {
        this.allPages2.forEach((pageNumber) => {
          if (pageNumber === 1
            || pageNumber === this.currentPage2
            || pageNumber === this.totalPages2
            || pageNumber === this.currentPage2 + 1
            || pageNumber === this.currentPage2 - 1) {
            this.paginatorArray2.push(pageNumber);
          } else if (!hasFirstEllipsis && pageNumber < this.currentPage2 && (pageNumber !== this.currentPage2 - 1)) {
            this.paginatorArray2.push('...');
            hasFirstEllipsis = true;
          } else if (!hasSecondEllipsis && pageNumber > this.currentPage2 && pageNumber !== this.currentPage2 + 1) {
            this.paginatorArray2.push('...');
            hasSecondEllipsis = true;
          }
        });
      }
    }
    this.loading2 = false;
  }

  addRow(element){
    this.dataSource.data.forEach(row => {
      if (row.id === element.id) {
        if (!this.markedArticles.find(e => e.id === element.id)) {
            this.markedArticles.push(row);
            let offset = (this.currentPage2 - 1) * this.offset;
            let endoffset = this.currentPage2  * this.offset;
            this.dataSource2.data = this.markedArticles.slice(offset, endoffset);

            //this.dataSource2.data = this.markedArticles;
            this.dataSource2.filter = "";
        }
      }
    });
    this.totalPages2 = Math.ceil(this.markedArticles.length / this.offset);
    this.allPages2 = Array(this.totalPages2).fill(1).map((x, i) => i + 1);
    this.generatePaginator2();
  }

  addAllRows(){
    this.dataSource.data.forEach(row => {
        if (!this.markedArticles.find(e => e.id === row.id)) {
          this.markedArticles.push(row);
        }
    });
    let offset = (this.currentPage2 - 1) * this.offset;
    let endoffset = this.currentPage2  * this.offset;
    this.dataSource2.data = this.markedArticles.slice(offset, endoffset);
    //this.dataSource2.data = this.markedArticles;
    this.dataSource2.filter = "";

    this.totalPages2 = Math.ceil(this.markedArticles.length / this.offset);
    this.allPages2 = Array(this.totalPages2).fill(1).map((x, i) => i + 1);
    this.generatePaginator2();
  }

  deleteRow(element) {
    //ta bort raden från valda
    let initialPage = this.currentPage2;
    let initialNumberOfPages = this.totalPages2;
    let j = 0;

    this.markedArticles.forEach(row => {
      if (row.id === element.id) {
        this.markedArticles.splice(j, 1);
      }
      j++;
    });

    this.dataSource2.data = this.markedArticles;
    this.dataSource2.filter = "";

    this.totalPages2 = Math.ceil(this.markedArticles.length / this.offset);
    this.allPages2 = Array(this.totalPages2).fill(1).map((x, i) => i + 1);

    debugger;
    if (this.totalPages2 !== initialNumberOfPages)
      this.currentPage2 = initialPage -1;
    else
      this.currentPage2 = initialPage;

    if (this.currentPage2 == 0)
      this.currentPage2 = 1;

    this.generatePaginator2();

    let offset = (this.currentPage2 - 1) * this.offset;
    if (offset < 0)
      offset = 0;
    this.dataSource2.data = this.markedArticles.slice(offset);

    this.generatePaginator2();
debugger;
    this.show = this.markedArticles.length > 0;
    debugger;
  }

  deleteAllRows(){
    let initialPage = this.currentPage2;
    let initialNumberOfPages = this.totalPages2;

    let offset = (this.currentPage2 - 1) * this.offset;
    this.markedArticles.splice(offset, this.offset);

    this.dataSource2.data = this.markedArticles;
    this.dataSource2.filter = "";

    this.totalPages2 = Math.ceil(this.markedArticles.length / this.offset);
    this.allPages2 = Array(this.totalPages2).fill(1).map((x, i) => i + 1);

    debugger;
    if (this.totalPages2 !== initialNumberOfPages)
      this.currentPage2 = initialPage -1;
    else
      this.currentPage2 = initialPage;

    if (this.currentPage2 == 0)
      this.currentPage2 = 1;


    this.generatePaginator2();

    offset = (this.currentPage2 - 1) * this.offset;
    if (offset < 0)
      offset = 0;
    this.dataSource2.data = this.markedArticles.slice(offset);

    this.generatePaginator2();
    //this.markedArticles = [];
    this.show = this.markedArticles.length > 0;
  }

  done() {
    this.dialogRef.close(this.markedArticles);
  }
}

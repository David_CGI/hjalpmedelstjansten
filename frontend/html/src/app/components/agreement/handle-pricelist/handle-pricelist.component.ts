/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {AuthService} from '../../../auth/auth.service';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {AgreementService} from '../../../services/agreement.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductService} from '../../../services/product.service';
import {Pricelist} from '../../../models/agreement/pricelist.model';
import {Pricelistrow} from '../../../models/agreement/pricelistrow.model';
import {GuaranteeUnit} from '../../../models/agreement/guarantee-unit.model';
import {OrganizationService} from '../../../services/organization.service';
import {SelectionModel} from '@angular/cdk/collections';
import {AlertService} from '../../../services/alert.service';
import {AddPricelistRowDialogComponent} from './add-pricelist-row-dialog/add-pricelist-row-dialog.component';
import {Article} from '../../../models/product/article.model';
import {CommonService} from '../../../services/common.service';
import {PreventiveMaintenance} from '../../../models/preventive-maintenance.model';
import {UserService} from '../../../services/user.service';
import {HelptextService} from '../../../services/helptext.service';
import {ImportPricelistDialogComponent} from './import-pricelist-dialog/import-pricelist-dialog.component';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {Subject} from 'rxjs/Subject';
import {AttachMessageDialogComponent} from "../../product/attach-message-dialog/attach-message-dialog.component";

@Component({
  selector: 'app-handle-pricelist',
  templateUrl: './handle-pricelist.component.html',
  styleUrls: ['./handle-pricelist.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HandlePricelistComponent implements OnInit {

  displayedColumns = ['checkbox', 'typ', 'artnr', 'benämning', 'kategori', 'beställningsenhet',
    'utgått', 'pris', 'gäller från', 'min best', 'levtid', 'garanti(antal)', '(enhet)',
    '(gäller från)', '(villkor)', 'status', 'inactivate', 'anchor'];
  displayedColumnsCustomer = ['activate', 'decline', 'typ', 'artnr', 'benämning', 'kategori', 'beställningsenhet',
    'utgått', 'pris', 'gäller från', 'min best', 'levtid', 'garanti(antal)', '(enhet)', '(gäller från)',
    '(villkor)', 'status', 'inactivate', 'anchor'];
  loaded = false;
  loading = true;
  saving = false;
  editMode = false;
  organizationId: number;
  agreementId: number;
  pricelistId: number;
  isCustomer: boolean;
  isPricelistApprover: boolean;
  userId: number;
  selection = new SelectionModel<Pricelistrow>(true, []);
  tmpSelection = [];
  activationSelection = new SelectionModel<Pricelistrow>(true, []);
  tmpActivationSelection = [];
  declineSelection = new SelectionModel<Pricelistrow>(true, []);
  tmpDeclineSelection = [];
  units: Array<GuaranteeUnit>;
  query = '';
  tmpQuery = '';
  searchChanged: Subject<string> = new Subject<string>();
  queryParams = '';
  searchTotal;
  offset = 25;
  totalPages;
  currentPage = 1;
  paginatorArray = [];
  allPages;
  dataSource = null;
  pricelist: Pricelist;
  pricelistRows = [];
  pricelistValidFrom: Date;
  warrantyValidFrom: Array<PreventiveMaintenance>;
  helpTexts;
  flexDirection: string;

  // Filters
  filterActive = false;
  filterPendingApproval = false;
  filterDeclined = false;
  filterCreated = false;
  filterChanged = false;
  filterInactive = false;
  filterPendingInactivation = false;
  filterShowRowsWithPrices = false;
  filterShowRowsWithoutPrices = false;
  filterH = false;
  filterT = false;
  filterR = false;
  filterTJ = false;
  filterI = false;

  // Permissions
  hasPermissionToViewPricelist = this.hasPermission('pricelist:view_own');
  hasPermissionToViewPricelistRow = this.hasPermission('pricelistrow:view_own');
  hasPermissionToUpdatePricelistRow = this.hasPermission('pricelistrow:update_own');
  hasPermissionToSendForCustomerApproval = this.hasPermission('pricelistrow:sendForCustomerApproval');
  hasPermissionToActivatePricelistRow = this.hasPermission('pricelistrow:activate');
  hasPermissionToDeclinePricelistRow = this.hasPermission('pricelistrow:decline');
  hasPermissionToApproveInactivatePricelistRow = this.hasPermission('pricelistrow:approve_inactivate');
  hasPermissionToDeclineInactivatePricelistRow = this.hasPermission('pricelistrow:decline_inactivate');
  hasPermissionToReopenInactivatedPricelistRow = this.hasPermission('pricelistrow:reopen');
  hasPermissionToExportPricelistRows = this.hasPermission('agreementpricelistexport:view');
  hasPermissionToImportPricelistRows = this.hasPermission('agreementpricelistimport:view');
  isOwnOrganization = false;

  constructor(private route: ActivatedRoute,
              private authService: AuthService,
              private productService: ProductService,
              private agreementService: AgreementService,
              private organizationService: OrganizationService,
              private commonService: CommonService,
              private userService: UserService,
              private alertService: AlertService,
              private dialog: MatDialog,
              private router: Router,
              private helpTextService: HelptextService) {
    this.commonService.isUsingIE() ? this.flexDirection = 'row' : this.flexDirection = 'column';
    this.route.params.subscribe(params => {
      this.organizationId = params.organizationId;
      this.agreementId = params.agreementId;
      this.pricelistId = params.pricelistId;
    });
    Number(this.organizationId) === this.authService.getOrganizationId() ? this.isOwnOrganization = true : this.isOwnOrganization = false;
    this.hasPermissionToViewPricelist && this.isOwnOrganization ?
      this.hasPermissionToViewPricelist = true : this.hasPermissionToViewPricelist = false;
    this.hasPermissionToUpdatePricelistRow && this.isOwnOrganization ?
      this.hasPermissionToUpdatePricelistRow = true : this.hasPermissionToUpdatePricelistRow = false;
    this.hasPermissionToSendForCustomerApproval && this.isOwnOrganization ?
      this.hasPermissionToSendForCustomerApproval = true : this.hasPermissionToSendForCustomerApproval = false;

    // The user is considered done with their search after 500 ms without further input
    this.searchChanged.pipe(
      debounceTime(500),
      distinctUntilChanged())
      .subscribe(query => {
          this.query = encodeURIComponent(query);
          this.onQueryChange();
        }
      );
  }

  ngOnInit() {
    this.helpTexts = this.helpTextService.getTexts().subscribe(
      texts => {
        this.helpTexts = texts;
      }
    );
    this.organizationService.getOrganization(this.organizationId).subscribe(
      res => {
        res.organizationType === 'CUSTOMER' ? this.isCustomer = true : this.isCustomer = false;
      }
    );
    this.commonService.getPreventiveMaintenances().subscribe(
      res => {
        this.warrantyValidFrom = res;
        this.productService.getGuaranteeUnits().subscribe(
          units => {
            this.units = units;
            this.userService.getProfile().subscribe(
              profile => {
                this.userId = profile.id;
                this.agreementService.getPriceList(this.organizationId, this.agreementId, this.pricelistId).subscribe(
                  pricelist => {
                    this.pricelist = pricelist;
                    this.isPricelistApprover = this.pricelist.agreement.customerPricelistApprovers.some(x => x.id === this.userId);
                    this.convertToDateString();
                    this.getPricelistRows('');
                  }, error => {
                    this.alertService.clear();
                    error.error.errors.forEach(err => {
                      this.alertService.error(err);
                    });
                  }
                );
              }
            );
          }
        );
      }
    );
  }

  toggleEditMode() {
    this.editMode = !this.editMode;
    if (!this.editMode) {
      this.hasPermissionToActivatePricelistRow || this.hasPermissionToDeclinePricelistRow ?
        this.displayedColumnsCustomer.push('anchor') : this.displayedColumns.push('anchor');
      this.getPricelistRows(this.query + this.queryParams + '&offset=' + (this.currentPage - 1) * this.offset);
    } else {
      this.hasPermissionToActivatePricelistRow || this.hasPermissionToDeclinePricelistRow ?
        this.displayedColumnsCustomer.splice(-1, 1) : this.displayedColumns.splice(-1, 1);
    }
  }

  onQueryChanged(text: string) {
    this.searchChanged.next(text);
  }

  onQueryChange() {
    this.onFilterChange();
  }

  onFilterChange() {
    this.queryParams = '';
    this.currentPage = 1;
    this.filterActive ? this.queryParams += '&status=ACTIVE' : this.queryParams = this.queryParams;
    this.filterPendingApproval ? this.queryParams += '&status=PENDING_APPROVAL' : this.queryParams = this.queryParams;
    this.filterDeclined ? this.queryParams += '&status=DECLINED' : this.queryParams = this.queryParams;
    if (!this.isCustomer) {
      this.filterCreated ? this.queryParams += '&status=CREATED' : this.queryParams = this.queryParams;
      this.filterChanged ? this.queryParams += '&status=CHANGED' : this.queryParams = this.queryParams;
    }
    this.filterInactive ? this.queryParams += '&status=INACTIVE' : this.queryParams = this.queryParams;
    this.filterPendingInactivation ? this.queryParams += '&status=PENDING_INACTIVATION' : this.queryParams = this.queryParams;
    this.filterShowRowsWithPrices ? this.queryParams += '&showRowsWithPrices=true' : this.queryParams += '&showRowsWithPrices=false';
    this.filterShowRowsWithoutPrices ? this.queryParams += '&showRowsWithoutPrices=true'
      : this.queryParams += '&showRowsWithoutPrices=false';
    this.filterH ? this.queryParams += '&type=H' : this.queryParams = this.queryParams;
    this.filterT ? this.queryParams += '&type=T' : this.queryParams = this.queryParams;
    this.filterR ? this.queryParams += '&type=R' : this.queryParams = this.queryParams;
    this.filterTJ ? this.queryParams += '&type=Tj' : this.queryParams = this.queryParams;
    this.filterI ? this.queryParams += '&type=I' : this.queryParams = this.queryParams;
    this.getPricelistRows(this.query + this.queryParams);
  }

  exportPricelistRows() {
    window.location.href = this.agreementService.getExportPricelistRowsUrl(
      this.organizationId, this.agreementId, this.pricelistId) + '?query=' + this.query + this.queryParams;
  }

  exportPricelist() {
    window.location.href = this.agreementService.getExportPricelistUrl(
      this.organizationId, this.agreementId, this.pricelistId);
  }

  openImportPricelistDialog() {
    const dialogRef = this.dialog.open(ImportPricelistDialogComponent, {
      width: '80%',
      data: {
        'organizationId': this.organizationId,
        'agreementId': this.agreementId,
        'pricelistId': this.pricelistId
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getPricelistRows(this.query + this.queryParams);
    });
  }

  getPricelistRows(query) {
    this.loading = true;
    this.tmpSelection = this.selection.selected;
    this.tmpActivationSelection = this.activationSelection.selected;
    this.tmpDeclineSelection = this.declineSelection.selected;
    this.agreementService.getPricelistRows(this.organizationId, this.agreementId, this.pricelistId, query).subscribe(
      res => {
        this.searchTotal = res.headers.get('X-Total-Count');
        this.searchTotal !== '0' ? this.pricelistRows = res.body : this.pricelistRows = [];
        this.totalPages = Math.ceil(this.searchTotal / this.offset);
        this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
        // Extract units
        this.pricelistRows.forEach((row) => {
          if (row.warrantyQuantityUnit) {
            row.warrantyQuantityUnit = this.units.find(x => x.id === row.warrantyQuantityUnit.id);
          } else {
            row.warrantyQuantityUnit = new GuaranteeUnit();
          }
          if (row.warrantyValidFrom) {
            row.warrantyValidFrom = this.warrantyValidFrom.find(x => x.id === row.warrantyValidFrom.id);
          } else {
            row.warrantyValidFrom = new PreventiveMaintenance();
          }
          if (!row.warrantyTerms) {
            row.warrantyTerms = '';
          }

          if (row.article.status === 'DISCONTINUED' && this.isCustomer) {
            row.price = '';
          }

          row.validFrom = new Date(row.validFrom);
          this.tmpSelection.forEach(tmpRow => {
            if (tmpRow.id === row.id) {
              this.selection.deselect(this.selection.selected.find(x => x.id === row.id));
              this.selection.select(row);
            }
          });
          this.tmpActivationSelection.forEach(tmpRow => {
            if (tmpRow.id === row.id) {
              this.activationSelection.deselect(this.activationSelection.selected.find(x => x.id === row.id));
              this.activationSelection.select(row);
            }
          });
          this.tmpDeclineSelection.forEach(tmpRow => {
            if (tmpRow.id === row.id) {
              this.declineSelection.deselect(this.declineSelection.selected.find(x => x.id === row.id));
              this.declineSelection.select(row);
            }
          });
        });
        this.dataSource = new MatTableDataSource<Pricelistrow>(this.pricelistRows);
        this.generatePaginator();
        this.loaded = true;
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  getDisplayedColumns() {
    let result;
    this.hasPermissionToActivatePricelistRow || this.hasPermissionToDeclinePricelistRow ?
      result = this.displayedColumnsCustomer : result = this.displayedColumns;
    return result;
  }

  /** Whether a row is selected for activation and declined at the same time. */
  hasFaultySelection() {
    return this.activationSelection.selected.some(item => this.declineSelection.selected.indexOf(item) >= 0);
  }

  /** Whether the number of selected elements matches the total number of selectable rows. */
  isAllSelected() {
    let result = true;
    let numRows = 0;
    this.dataSource.data.forEach(item => {
      if (item.status === 'CREATED' || item.status === 'DECLINED' || item.status === 'CHANGED') {
        numRows++;
        const rowChecked = this.selection.selected.some(function (el) {
          return (el.id === item.id);
        });
        if (!rowChecked) {
          result = false;
        }
      }
    });
    return result;
  }

  /** Selects all selectable rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(
        row => {
          if (row.status === 'CREATED' || row.status === 'DECLINED' || row.status === 'CHANGED') {
            this.selection.select(row);
          }
        });
  }

  /** Whether the number of selected elements matches the total number of selectable rows. */
  isAllSelectedApproval(type) {
    let result = true;
    if (type === 'activation') {
      this.dataSource.data.forEach(item => {
        if (item.status === 'PENDING_APPROVAL' || item.status === 'PENDING_INACTIVATION') {
          const rowChecked = this.activationSelection.selected.some(function (el) {
            return (el.id === item.id);
          });
          if (!rowChecked) {
            result = false;
          }
        }
      });
    } else {
      this.dataSource.data.forEach(item => {
        if (item.status === 'PENDING_APPROVAL' || item.status === 'PENDING_INACTIVATION') {
          const rowChecked = this.declineSelection.selected.some(function (el) {
            return (el.id === item.id);
          });
          if (!rowChecked) {
            result = false;
          }
        }
      });
    }
    return result;
  }

  /** Selects all selectable rows if they are not all selected; otherwise clear selection. */
  masterToggleApprovals(type) {
    if (type === 'activation') {
      this.isAllSelectedApproval('activation') ?
        this.activationSelection.clear() :
        this.dataSource.data.forEach(
          row => {
            if (row.status === 'PENDING_APPROVAL' || row.status === 'PENDING_INACTIVATION') {
              this.activationSelection.select(row);
              this.declineSelection.deselect(row);
            }
          });
    } else {
      this.isAllSelectedApproval('decline') ?
        this.declineSelection.clear() :
        this.dataSource.data.forEach(
          row => {
            if (row.status === 'PENDING_APPROVAL' || row.status === 'PENDING_INACTIVATION') {
              this.activationSelection.deselect(row);
              this.declineSelection.select(row);
            }
          });
    }

  }

  hasPermission(permission): boolean {
    if (!localStorage.getItem('token')) {
      return false;
    }
    return this.authService.hasPermission(permission);
  }

  goToPage(page) {
    if (page !== '...' && page !== this.currentPage) {
      this.loading = true;
      this.tmpSelection = this.selection.selected;
      this.tmpActivationSelection = this.activationSelection.selected;
      this.tmpDeclineSelection = this.declineSelection.selected;
      this.currentPage = page;
      this.agreementService.getPricelistRows(
        this.organizationId, this.agreementId, this.pricelistId,
        this.query + this.queryParams + '&offset=' + (this.currentPage - 1) * this.offset)
        .subscribe(
          res => {
            this.pricelistRows = res.body;
            // Extract units dates and selections
            this.pricelistRows.forEach((row) => {
              if (row.warrantyQuantityUnit) {
                row.warrantyQuantityUnit = this.units.find(x => x.id === row.warrantyQuantityUnit.id);
              } else {
                row.warrantyQuantityUnit = new GuaranteeUnit();
              }
              if (row.warrantyValidFrom) {
                row.warrantyValidFrom = this.warrantyValidFrom.find(x => x.id === row.warrantyValidFrom.id);
              } else {
                row.warrantyValidFrom = new PreventiveMaintenance();
              }
              if (!row.warrantyTerms) {
                row.warrantyTerms = '';
              }
              row.validFrom = new Date(row.validFrom);
              this.tmpSelection.forEach(tmpRow => {
                if (tmpRow.id === row.id) {
                  this.selection.deselect(this.selection.selected.find(x => x.id === row.id));
                  this.selection.select(row);
                }
              });
              this.tmpActivationSelection.forEach(tmpRow => {
                if (tmpRow.id === row.id) {
                  this.activationSelection.deselect(this.activationSelection.selected.find(x => x.id === row.id));
                  this.activationSelection.select(row);
                }
              });
              this.tmpDeclineSelection.forEach(tmpRow => {
                if (tmpRow.id === row.id) {
                  this.declineSelection.deselect(this.declineSelection.selected.find(x => x.id === row.id));
                  this.declineSelection.select(row);
                }
              });
            });
            this.dataSource = new MatTableDataSource<Pricelistrow>(this.pricelistRows);
            this.currentPage = page;
            this.generatePaginator();
          }, error => {
            this.alertService.clear();
            error.error.errors.forEach(err => {
              this.alertService.error(err);
            });
          }
        );
    }
  }

  generatePaginator() {
    let hasFirstEllipsis = false;
    let hasSecondEllipsis = false;
    this.paginatorArray = [];
    if (this.searchTotal > this.offset) {
      if (this.totalPages < 6) {
        this.paginatorArray = this.allPages;
      } else {
        this.allPages.forEach((pageNumber) => {
          if (pageNumber === 1
            || pageNumber === this.currentPage
            || pageNumber === this.totalPages
            || pageNumber === this.currentPage + 1
            || pageNumber === this.currentPage - 1) {
            this.paginatorArray.push(pageNumber);
          } else if (!hasFirstEllipsis && pageNumber < this.currentPage && (pageNumber !== this.currentPage - 1)) {
            this.paginatorArray.push('...');
            hasFirstEllipsis = true;
          } else if (!hasSecondEllipsis && pageNumber > this.currentPage && pageNumber !== this.currentPage + 1) {
            this.paginatorArray.push('...');
            hasSecondEllipsis = true;
          }
        });
      }
    }
    this.loading = false;
  }

  convertToDateString() {
    this.pricelistValidFrom = new Date(this.pricelist.validFrom);
  }

  getMillisecondsFromDate(date) {
    if (date) {
      return date.getTime();
    }
    return null;
  }

  openAddPricelistRowDialog() {
    const dialogRef = this.dialog.open(AddPricelistRowDialogComponent, {
      width: '80%',
      data: {
        'organizationId': this.organizationId,
        'agreementId': this.agreementId,
        'pricelistId': this.pricelistId
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.loading = true;
        const pricelistRows = new Array<Pricelistrow>();
        result.forEach(article => {
          const tmpArticle = new Article();
          const pricelistRow = new Pricelistrow();
          pricelistRow.status = 'CREATED';
          pricelistRow.leastOrderQuantity = 1;
          tmpArticle.id = article.id;
          pricelistRow.article = tmpArticle;
          pricelistRows.push(pricelistRow);
        });
        this.agreementService.createPricelistRows(this.organizationId, this.agreementId, this.pricelistId, pricelistRows).subscribe(
          res => {
            this.onFilterChange();
            this.alertService.success('Sparat');
          }, error => {
            this.loading = false;
            this.alertService.clear();
            error.error.errors.forEach(err => {
              this.alertService.error(err);
            });
          }
        );
      }
    });
  }

  updatePricelistRow(row) {
    if (row.status === 'ACTIVE_EDIT') {
      row.status = 'ACTIVE';
    }
    if (row.validFrom) {
      row.validFrom = this.getMillisecondsFromDate(row.validFrom);
    }
    if (row.warrantyValidFrom && !row.warrantyValidFrom.id) {
      row.warrantyValidFrom = null;
    }
    this.agreementService.updatePricelistRow(this.organizationId, this.agreementId, this.pricelistId, row.id, row).subscribe(
      res => {
        row.status = res.status;
        if (row.validFrom) {
          row.validFrom = new Date(row.validFrom);
        }
        this.alertService.clear();
        this.alertService.success('Sparat');
      }, error => {
        if (row.validFrom) {
          row.validFrom = new Date(row.validFrom);
        }
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  onCheckActivation(event, element) {
    this.activationSelection.toggle(element);
    if (event.checked) {
      if (this.declineSelection.isSelected(element)) {
        this.declineSelection.deselect(element);
      }
    }
  }

  onCheckDecline(event, element) {
    this.declineSelection.toggle(element);
    if (event.checked) {
      if (this.activationSelection.isSelected(element)) {
        this.activationSelection.deselect(element);
      }
    }
  }

  sendForApproval(comment) {
    this.saving = true;
    this.selection.selected.forEach((row) => {
      if (row.validFrom) {
        row.validFrom = this.getMillisecondsFromDate(row.validFrom);
      }
    });
    this.agreementService.sendPricelistRowsForApproval(
      this.organizationId, this.agreementId, this.pricelistId, this.selection.selected, comment).subscribe(
      res => {
        res.forEach((newRow) => {
          this.pricelistRows.forEach((oldRow) => {
            if (oldRow.id === newRow.id) {
              oldRow.status = newRow.status;
              if (newRow.validFrom) {
                oldRow.validFrom = new Date(newRow.validFrom);
              }
              this.dataSource = new MatTableDataSource<Pricelistrow>(this.pricelistRows);
            }
          });
        });
        this.selection.clear();
        this.saving = false;
        this.alertService.success('Markerade rader har skickats för godkännande');
      }, error => {
        this.pricelistRows.forEach((row) => {
          if (row.validFrom) {
            row.validFrom = new Date(row.validFrom);
          }
        });
        this.saving = false;
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  openAttachMessageDialog(sender, rows) {
    const dialogRef = this.dialog.open(AttachMessageDialogComponent, {
      width: '80%'
    });
    if (sender == 'supplier') {
      if (rows == 'marked') {
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            this.sendForApproval(result);
          }
        });
      }
      if (rows == 'all') {
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            this.sendAllForApproval(result);
          }
        });
      }
    }
    if (sender == 'customer') {
      if (rows == 'marked') {
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            this.handleApprovalRequests(result);
          }
        });
      }
      if (rows == 'all') {
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            this.approveAll(result);
          }
        });
      }
    }
  }

  sendAllForApproval(comment) {
    this.loading = true;
    this.agreementService.sendAllPricelistRowsForApproval(this.organizationId, this.agreementId, this.pricelistId, comment).subscribe(
      res => {
        this.selection.clear();
        this.getPricelistRows(this.query + this.queryParams);
        this.alertService.success('Samtliga rader i prislistan med status Ny och Ändrad har skickats för godkännande');
      }, error => {
        this.pricelistRows.forEach((row) => {
          if (row.validFrom) {
            row.validFrom = new Date(row.validFrom);
          }
        });
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
        this.loading = false;
      }
    );
  }

  editPricelistRow(row) {
    // Temporary status to handle editing of active rows
    row.status = 'ACTIVE_EDIT';
  }

  deletePricelistRow(row) {
    this.agreementService.deleteAgreementPriceListRow(this.organizationId, this.agreementId, this.pricelistId, row.id.toString()).subscribe(
      res => {
        this.alertService.clear();
        this.getPricelistRows(this.query + this.queryParams);
        this.alertService.success('Tog bort artikeln \"' + row.article.articleName + '\" från prislista på inköpsavtal');
      }, error => {
        if (row.validFrom) {
          row.validFrom = new Date(row.validFrom);
        }
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  requestInactivation(row) {
    const pricelistRows = new Array<Pricelistrow>();
    row.validFrom = this.getMillisecondsFromDate(row.validFrom);
    pricelistRows.push(row);
    this.agreementService.requestInactivationOfPricelistRow(
      this.organizationId, this.agreementId, this.pricelistId, pricelistRows).subscribe(
      res => {
        row.status = res[0].status;
        if (res[0].validFrom) {
          row.validFrom = new Date(res[0].validFrom);
        }
        this.dataSource = new MatTableDataSource<Pricelistrow>(this.pricelistRows);
        this.alertService.success('Inaktivering har begärts');
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
        this.loading = false;
      }
    );
  }

  reopenInactivated(row) {
    const pricelistRows = new Array<Pricelistrow>();
    row.validFrom = this.getMillisecondsFromDate(row.validFrom);
    pricelistRows.push(row);
    this.agreementService.reopenInactivatedPricelistRow(
      this.organizationId, this.agreementId, this.pricelistId, pricelistRows).subscribe(
      res => {
        row.status = res[0].status;
        if (res[0].validFrom) {
          row.validFrom = new Date(res[0].validFrom);
        }
        this.dataSource = new MatTableDataSource<Pricelistrow>(this.pricelistRows);
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
        this.loading = false;
      }
    );
  }

  handleApprovalRequests(comment) {
    const approveInactivations = new Array<Pricelistrow>();
    const declineInactivations = new Array<Pricelistrow>();
    const approveActivations = new Array<Pricelistrow>();
    const declineActivations = new Array<Pricelistrow>();
    if (this.activationSelection.selected.length) {
      this.activationSelection.selected.forEach(item => {
        item.validFrom = this.getMillisecondsFromDate(item.validFrom);
        if (item.status === 'PENDING_INACTIVATION') {
          approveInactivations.push(item);
        } else {
          approveActivations.push(item);
        }
      });
      if (approveActivations.length) {
        this.agreementService.activatePricelistRows(
          this.organizationId, this.agreementId, this.pricelistId, approveActivations, comment).subscribe(
          res => {
            res.forEach((newRow) => {
              this.pricelistRows.forEach((oldRow) => {
                if (oldRow.id === newRow.id) {
                  oldRow.status = newRow.status;
                  if (newRow.validFrom) {
                    oldRow.validFrom = new Date(newRow.validFrom);
                  }
                }
              });
            });
            if (approveActivations.length == 1)
              this.alertService.success(approveActivations.length + ' rad godkänd');
            else
              this.alertService.success(approveActivations.length + ' rader godkända');
            this.dataSource = new MatTableDataSource<Pricelistrow>(this.pricelistRows);
            this.activationSelection.clear();
          }, error => {
            this.alertService.clear();
            error.error.errors.forEach(err => {
              this.alertService.error(err);
            });
            this.loading = false;
          }
        );
      }
      if (approveInactivations.length) {
        this.agreementService.approveInactivationOfPricelistRow(
          this.organizationId, this.agreementId, this.pricelistId, approveInactivations, comment).subscribe(
          res => {
            if (approveInactivations.length == 1)
              this.alertService.success( approveInactivations.length + ' rad inaktiverad');
            else
              this.alertService.success( approveInactivations.length + ' rader inaktiverade');
            res.forEach((newRow) => {
              this.pricelistRows.forEach((oldRow) => {
                if (oldRow.id === newRow.id) {
                  oldRow.status = newRow.status;
                  if (newRow.validFrom) {
                    oldRow.validFrom = new Date(newRow.validFrom);
                  }
                }
              });
            });
            this.dataSource = new MatTableDataSource<Pricelistrow>(this.pricelistRows);
            this.activationSelection.clear();
          }, error => {
            this.alertService.clear();
            error.error.errors.forEach(err => {
              this.alertService.error(err);
            });
            this.loading = false;
          }
        );
      }
    }

    if (this.declineSelection.selected.length) {
      this.declineSelection.selected.forEach(item => {
        item.validFrom = this.getMillisecondsFromDate(item.validFrom);
        if (item.status === 'PENDING_INACTIVATION') {
          declineInactivations.push(item);
        } else {
          declineActivations.push(item);
        }
      });
      if (declineActivations.length) {
        this.agreementService.declinePricelistRows(
          this.organizationId, this.agreementId, this.pricelistId, declineActivations, comment).subscribe(
          res => {
            if (declineActivations.length == 1)
              this.alertService.success( declineActivations.length + ' rad avböjd');
            else
              this.alertService.success( declineActivations.length + ' rader avböjda');
            res.forEach((newRow) => {
              this.pricelistRows.forEach((oldRow) => {
                if (oldRow.id === newRow.id) {
                  oldRow.status = newRow.status;
                  if (newRow.validFrom) {
                    oldRow.validFrom = new Date(newRow.validFrom);
                  }
                }
              });
            });
            this.dataSource = new MatTableDataSource<Pricelistrow>(this.pricelistRows);
            this.declineSelection.clear();
          }, error => {
            this.alertService.clear();
            error.error.errors.forEach(err => {
              this.alertService.error(err);
            });
            this.loading = false;
          }
        );
      }
      if (declineInactivations.length) {
        this.agreementService.declineInactivationOfPricelistRow(
          this.organizationId, this.agreementId, this.pricelistId, declineInactivations, comment).subscribe(
          res => {
            if (declineInactivations.length == 1)
              this.alertService.success('Inaktivering har avböjts för ' + declineInactivations.length + ' rad');
            else
              this.alertService.success('Inaktivering har avböjts för ' + declineInactivations.length + ' rader');
            res.forEach((newRow) => {
              this.pricelistRows.forEach((oldRow) => {
                if (oldRow.id === newRow.id) {
                  oldRow.status = newRow.status;
                  if (newRow.validFrom) {
                    oldRow.validFrom = new Date(newRow.validFrom);
                  }
                }
              });
            });
            this.dataSource = new MatTableDataSource<Pricelistrow>(this.pricelistRows);
            this.declineSelection.clear();
          }, error => {
            this.alertService.clear();
            error.error.errors.forEach(err => {
              this.alertService.error(err);
            });
            this.loading = false;
          }
        );
      }
    }
  }

  approveAll(comment) {
    this.loading = true;
    this.agreementService.activateAllPricelistRows(
      this.organizationId, this.agreementId, this.pricelistId, comment).subscribe(
      res => {
        this.activationSelection.clear();
        this.declineSelection.clear();
        this.getPricelistRows(this.query + this.queryParams);
        this.alertService.success('Samtliga rader i prislistan har godkänts');
      }, error => {
        this.pricelistRows.forEach((row) => {
          if (row.validFrom) {
            row.validFrom = new Date(row.validFrom);
          }
        });
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
        this.loading = false;
      }
    );
  }
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatTableDataSource} from '@angular/material';
import {OrganizationService} from '../../../../services/organization.service';
import {BusinessLevel} from '../../../../models/organization/business-level.model';
import {SelectionModel} from '@angular/cdk/collections';
import {AlertService} from '../../../../services/alert.service';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {Subject} from 'rxjs/Subject';

@Component({
  selector: 'app-share-with-customer-dialog',
  templateUrl: './share-with-customer-dialog.component.html',
  styleUrls: ['./share-with-customer-dialog.component.scss']
})
export class ShareWithCustomerDialogComponent implements OnInit {
  displayedColumns = ['checkbox', 'organisation', 'org.nr', 'status'];
  loading = true;
  customers = [];
  tableOrg: Array<TableOrg> = new Array<TableOrg>();
  dataSource = null;
  query = '';
  tmpQuery = '';
  searchChanged: Subject<string> = new Subject<string>();
  searchTotal;
  offset = 25;
  totalPages;
  currentPage = 1;
  paginatorArray = [];
  allPages;
  customersToShareWith = [];
  businessLevelsToShareWith = [];
  selection = new SelectionModel<TableOrg>(true, []);

  constructor(public dialogRef: MatDialogRef<ShareWithCustomerDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private organizationService: OrganizationService,
              private alertService: AlertService) {
    this.customersToShareWith = data.customersToShareWith;
    this.businessLevelsToShareWith = data.businessLevelsToShareWith;

    // The user is considered done with their search after 500 ms without further input
    this.searchChanged.pipe(
      debounceTime(500),
      distinctUntilChanged())
      .subscribe(query => {
          this.query = encodeURIComponent(query);
          this.onQueryChange();
        }
      );
  }

  ngOnInit() {
    this.searchCustomers('');
  }

  onQueryChanged(text: string) {
    this.searchChanged.next(text);
  }

  onQueryChange() {
    this.searchCustomers(this.query);
  }

  searchCustomers(query) {
    this.loading = true;
    this.tableOrg = [];
    this.organizationService.searchOrganizations(query + '&type=CUSTOMER').subscribe(
      res => {
        this.searchTotal = res.headers.get('X-Total-Count');
        this.searchTotal !== '0' ? this.customers = res.body : this.customers = [];

        this.customers.forEach(customer => {
          this.organizationService.getActiveBusinessLevels(customer.id).subscribe(
            levels => {
              this.tableOrg.push({
                'id': customer.id,
                'organizationName': customer.organizationName,
                'organizationNumber': customer.organizationNumber,
                'active': customer.active,
                'businessLevel': null
              });
              levels.forEach(level => {
                this.tableOrg.push({
                  'id': customer.id,
                  'organizationName': customer.organizationName + ' - ' + level.name,
                  'organizationNumber': customer.organizationNumber,
                  'active': customer.active,
                  'businessLevel': level
                });
              });
              this.totalPages = Math.ceil(this.searchTotal / this.offset);
              this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
              this.dataSource = new MatTableDataSource<TableOrg>(this.tableOrg);
              this.generatePaginator();
            }, error => {
              this.loading = false;
              this.alertService.clear();
              error.error.errors.forEach(err => {
                this.alertService.error(err);
              });
            }
          );
        });

      }
    );
  }

  goToPage(page) {
    if (page !== '...' && page !== this.currentPage) {
      this.currentPage = page;
      this.searchCustomers(this.query + '&offset=' + (this.currentPage - 1) * this.offset);
    }
  }

  generatePaginator() {
    let hasFirstEllipsis = false;
    let hasSecondEllipsis = false;
    this.paginatorArray = [];
    if (this.searchTotal > this.offset) {
      if (this.totalPages < 6) {
        this.paginatorArray = this.allPages;
      } else {
        this.allPages.forEach((pageNumber) => {
          if (pageNumber === 1
            || pageNumber === this.currentPage
            || pageNumber === this.totalPages
            || pageNumber === this.currentPage + 1
            || pageNumber === this.currentPage - 1) {
            this.paginatorArray.push(pageNumber);
          } else if (!hasFirstEllipsis && pageNumber < this.currentPage && (pageNumber !== this.currentPage - 1)) {
            this.paginatorArray.push('...');
            hasFirstEllipsis = true;
          } else if (!hasSecondEllipsis && pageNumber > this.currentPage && pageNumber !== this.currentPage + 1) {
            this.paginatorArray.push('...');
            hasSecondEllipsis = true;
          }
        });
      }
    }
    this.loading = false;
  }

  onCheck(event, element) {
    this.selection.toggle(element);
    if (element.businessLevel) {
      this.selection.selected.forEach(selection => {
        if (selection.id === element.id && !selection.businessLevel) {
          this.selection.deselect(selection);
        }
      });
    } else {
      this.selection.selected.forEach(selection => {
        if (selection.id === element.id && selection.businessLevel) {
          this.selection.deselect(selection);
        }
      });
    }
  }

  done() {
    this.customersToShareWith = [];
    this.businessLevelsToShareWith = [];
    this.selection.selected.forEach(element => {
      if (element.businessLevel) {
        this.businessLevelsToShareWith.push(element.businessLevel);
      } else {
        this.customersToShareWith.push({
          'id': element.id,
          'organizationName': element.organizationName,
          'organizationNumber': element.organizationNumber,
          'active': element.active
        });
      }
    });
    this.dialogRef.close({'customers': this.customersToShareWith, 'businessLevels': this.businessLevelsToShareWith});
  }

}

export class TableOrg {
  id: number;
  organizationName: string;
  organizationNumber: string;
  active: boolean;
  businessLevel: BusinessLevel;
}

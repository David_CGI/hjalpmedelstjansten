/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {SendWelcomeMailDialogComponent} from './send-welcome-mail-dialog/send-welcome-mail-dialog.component';
import {CommonService} from '../../services/common.service';
import {DynamicTextService} from "../../services/dynamictext.service";
import {DynamicText} from "../../models/dynamictext.model";
import {AlertService} from "../../services/alert.service";

@Component({
  selector: 'app-administration',
  templateUrl: './administration.component.html',
  styleUrls: ['./administration.component.scss']
})
export class AdministrationComponent implements OnInit {
  flexDirection: string;
  dynamicTextLoaded = false;
  dynamicText: DynamicText;

  constructor(private dialog: MatDialog,
              private dynamicTextService: DynamicTextService,
              private commonService: CommonService,
              private alertService: AlertService) {
    this.commonService.isUsingIE() ? this.flexDirection = 'row' : this.flexDirection = 'column';
  }

  ngOnInit() {
    this.dynamicTextService.getDynamicText(1).subscribe(
      data => {
        this.dynamicText = data;
        this.dynamicTextLoaded = true;
      }
    );
  }

  openSendWelcomeMailDialog() {
    const dialogRef = this.dialog.open(SendWelcomeMailDialogComponent, {
      width: '80%'
    });
  }

  saveInfoTextToDatabase() {
    this.dynamicTextService.updateDynamicText(1, this.dynamicText).subscribe(
      data => {
        this.dynamicText = data;
        this.alertService.success('Sparat');
      }
    );

  }

}

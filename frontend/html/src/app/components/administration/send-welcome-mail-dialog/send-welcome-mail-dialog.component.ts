/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {AlertService} from '../../../services/alert.service';
import {LoginService} from '../../../services/login.service';

@Component({
  selector: 'app-send-welcome-mail-dialog',
  templateUrl: './send-welcome-mail-dialog.component.html',
  styleUrls: ['./send-welcome-mail-dialog.component.scss']
})
export class SendWelcomeMailDialogComponent implements OnInit {
  saving = false;

  constructor(public dialogRef: MatDialogRef<SendWelcomeMailDialogComponent>,
              private loginService: LoginService,
              private alertService: AlertService) {
  }

  ngOnInit() {
  }

  done() {
    this.saving = true;
    this.loginService.welcomeAll().subscribe(
      res => {
        this.saving = false;
        this.alertService.clear();
        this.alertService.success('Skickat');
        this.dialogRef.close('success');
      }, error => {
        this.saving = false;
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      });
  }

}

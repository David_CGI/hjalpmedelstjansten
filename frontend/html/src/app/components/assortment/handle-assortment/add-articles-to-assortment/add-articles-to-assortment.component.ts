/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, OnInit} from '@angular/core';
import {ProductService} from '../../../../services/product.service';
import {AssortmentService} from '../../../../services/assortment.service';
import {OrganizationService} from '../../../../services/organization.service';
import {AlertService} from '../../../../services/alert.service';
import {FormControl} from '@angular/forms';
import {AuthService} from '../../../../auth/auth.service';
import {HelptextService} from '../../../../services/helptext.service';
import {Organization} from '../../../../models/organization/organization.model';
import {Assortment} from '../../../../models/assortment/assortment.model';
import {Location} from '@angular/common';
import {CommonService} from '../../../../services/common.service';
import {ActivatedRoute} from '@angular/router';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {AgreementService} from '../../../../services/agreement.service';
import {Pricelist} from '../../../../models/agreement/pricelist.model';
import {Pricelistrow} from '../../../../models/agreement/pricelistrow.model';
import {SelectionModel} from '@angular/cdk/collections';
import {CategoryDialogComponent} from '../../../product/category-dialog/category-dialog.component';
import {SimpleArticle} from '../../../../models/product/simple-article.model';
import {Subject} from 'rxjs/Subject';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';

@Component({
  selector: 'app-add-articles-to-assortment',
  templateUrl: './add-articles-to-assortment.component.html',
  styleUrls: ['./add-articles-to-assortment.component.scss']
})
export class AddArticlesToAssortmentComponent implements OnInit {
  displayedColumns = ['checkbox', 'typ', 'artikelnummer', 'benämning', 'organisation', 'status'];

  loaded = false;
  loading = false;
  saving = false;
  selection = new SelectionModel<Pricelistrow>(true, []);
  query = '';
  tmpQuery = '';
  searchChanged: Subject<string> = new Subject<string>();
  queryParams = '';
  searchTotal;
  offset = 25;
  totalPages;
  currentPage = 1;
  paginatorArray = [];
  allPages;
  dataSource = null;
  allArticles = [];
  pricelistArticles = [];
  assortment: Assortment = new Assortment();
  agreements = [];
  selectedAgreement: number;
  pricelists: Array<Pricelist>;
  selectedPricelist: number;
  organization: Organization = new Organization();
  organizationId: number;
  assortmentId: number;
  helpTexts;
  helpTextsLoaded = false;
  flexDirection: string;

  // Permissions
  hasPermissionToCreateAssortment = this.hasPermission('assortment:create');
  hasPermissionToDeleteAssortment = this.hasPermission('assortment:delete');
  hasPermissionToUpdateAssortment = this.hasPermission('assortment:update');
  isOwnOrganization = false;

  filterCategory = '';

  // Form controls

  nameFormControl = new FormControl(null, {
    updateOn: 'blur'
  });
  validFromFormControl = new FormControl(null, {
    updateOn: 'blur'
  });
  validToFormControl = new FormControl(null, {
    updateOn: 'blur'
  });
  categoryFormControl = new FormControl(null, {
    updateOn: 'blur'
  });

  // Validation errors
  nameError;
  validFromError;
  validToError;

  constructor(private organizationService: OrganizationService,
              private assortmentService: AssortmentService,
              private productService: ProductService,
              private agreementService: AgreementService,
              private commonService: CommonService,
              private route: ActivatedRoute,
              private alertService: AlertService,
              private authService: AuthService,
              private dialog: MatDialog,
              private location: Location,
              private helpTextService: HelptextService) {
    this.commonService.isUsingIE() ? this.flexDirection = 'row' : this.flexDirection = 'column';

    this.route.params.subscribe(params => {
      this.organizationId = params.organizationId;
      this.assortmentId = params.assortmentId;
    });
    Number(this.organizationId) === this.authService.getOrganizationId() ? this.isOwnOrganization = true : this.isOwnOrganization = false;
    this.hasPermissionToCreateAssortment && this.isOwnOrganization ?
      this.hasPermissionToCreateAssortment = true : this.hasPermissionToCreateAssortment = false;
    this.hasPermissionToDeleteAssortment && this.isOwnOrganization ?
      this.hasPermissionToDeleteAssortment = true : this.hasPermissionToDeleteAssortment = false;
    this.hasPermissionToUpdateAssortment && this.isOwnOrganization ?
      this.hasPermissionToUpdateAssortment = true : this.hasPermissionToUpdateAssortment = false;

    // The user is considered done with their search after 500 ms without further input
    this.searchChanged.pipe(
      debounceTime(500),
      distinctUntilChanged())
      .subscribe(query => {
          this.query = encodeURIComponent(query);
          this.onQueryChange();
        }
      );
  }

  ngOnInit() {
    this.helpTexts = this.helpTextService.getTexts().subscribe(
      texts => {
        this.helpTexts = texts;
        this.helpTextsLoaded = true;
        this.loaded = true;
      }
    );
    this.assortmentService.getAssortment(this.organizationId, this.assortmentId).subscribe(
      res => {
        this.assortment = res;
        this.agreementService.searchAgreements(this.organizationId, '?limit=0').subscribe(
          agreements => {
            this.agreements = agreements.body;
          }, error => {
            this.alertService.clear();
            error.error.errors.forEach(err => {
              this.alertService.error(err);
            });
          }
        );
      }
    );
  }

  onQueryChanged(text: string) {
    this.searchChanged.next(text);
  }

  onQueryChange() {
    this.onFilterChange();
  }

  onFilterChange() {
    this.queryParams = '';
    this.currentPage = 1;
    this.allArticles = [];
    this.pricelistArticles = [];
    this.selection.clear();
    if (this.filterCategory) {
      this.queryParams += this.filterCategory;
    }
    if (this.selectedPricelist) {
      this.searchArticlesForPricelistRows();
    } else {
      this.searchArticles(this.query + '&includeArticles=true&includeProducts=false' + this.queryParams);
    }
  }

  searchArticles(query) {
    this.loading = true;
    this.productService.search(query).subscribe(
      res => {
        this.searchTotal = res.headers.get('X-Total-Count');
        this.searchTotal !== '0' ? this.allArticles = res.body : this.allArticles = [];
        this.totalPages = Math.ceil(this.searchTotal / this.offset);
        this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
        this.dataSource = new MatTableDataSource<SimpleArticle>(this.allArticles);
        this.generatePaginator();
      }, error => {
        this.loading = false;
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  goToPage(page) {
    if (page !== '...' && page !== this.currentPage) {
      this.loading = true;
      this.currentPage = page;
      if (!this.selectedPricelist) {
        this.productService.search(
          this.query + '&includeArticles=true&includeProducts=false' + this.queryParams +
          '&offset=' + (this.currentPage - 1) * this.offset).subscribe(
          res => {
            this.allArticles = res.body;
            this.dataSource = new MatTableDataSource<SimpleArticle>(this.allArticles);
            this.currentPage = page;
            this.generatePaginator();
          }, error => {
            this.alertService.clear();
            error.error.errors.forEach(err => {
              this.alertService.error(err);
            });
          }
        );
      } else {
        this.agreementService.getPricelistRows(
          this.organizationId, this.selectedAgreement, this.selectedPricelist, this.query + this.queryParams +
          '&offset=' + (this.currentPage - 1) * this.offset).subscribe(
          res => {
            this.pricelistArticles = res.body;
            this.dataSource = new MatTableDataSource<Pricelistrow>(this.pricelistArticles);
            this.currentPage = page;
            this.generatePaginator();
          }, error => {
            this.alertService.clear();
            error.error.errors.forEach(err => {
              this.alertService.error(err);
            });
          }
        );
      }
    }
  }

  generatePaginator() {
    let hasFirstEllipsis = false;
    let hasSecondEllipsis = false;
    this.paginatorArray = [];
    if (this.searchTotal > this.offset) {
      if (this.totalPages < 6) {
        this.paginatorArray = this.allPages;
      } else {
        this.allPages.forEach((pageNumber) => {
          if (pageNumber === 1
            || pageNumber === this.currentPage
            || pageNumber === this.totalPages
            || pageNumber === this.currentPage + 1
            || pageNumber === this.currentPage - 1) {
            this.paginatorArray.push(pageNumber);
          } else if (!hasFirstEllipsis && pageNumber < this.currentPage && (pageNumber !== this.currentPage - 1)) {
            this.paginatorArray.push('...');
            hasFirstEllipsis = true;
          } else if (!hasSecondEllipsis && pageNumber > this.currentPage && pageNumber !== this.currentPage + 1) {
            this.paginatorArray.push('...');
            hasSecondEllipsis = true;
          }
        });
      }
    }
    this.loaded = true;
    this.loading = false;
  }

  hasPermission(permission): boolean {
    if (!localStorage.getItem('token')) {
      return false;
    }
    return this.authService.hasPermission(permission);
  }

  onAgreementSelection() {
    this.selectedPricelist = null;
    if (this.selectedAgreement) {
      this.agreementService.getPriceLists(this.organizationId, this.selectedAgreement).subscribe(
        pricelists => {
          this.pricelists = pricelists;
        }
      );
    } else {
      this.onFilterChange();
    }
  }

  searchArticlesForPricelistRows() {
    this.agreementService.getPricelistRows(
      this.organizationId, this.selectedAgreement, this.selectedPricelist, this.query + this.queryParams).subscribe(
      res => {
        this.searchTotal = res.headers.get('X-Total-Count');
        this.searchTotal !== '0' ? this.pricelistArticles = res.body : this.pricelistArticles = [];
        this.totalPages = Math.ceil(this.searchTotal / this.offset);
        this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
        this.dataSource = new MatTableDataSource<Pricelistrow>(this.pricelistArticles);
        this.generatePaginator();
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  openCategoryDialog() {
    const dialogRef = this.dialog.open(CategoryDialogComponent, {
      width: '90%',
      data: {'includeCodelessCategories': true}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        result.code ? this.categoryFormControl.setValue(result.code + ' ' + result.name) : this.categoryFormControl.setValue(result.name);
        this.filterCategory = '&category=' + result.id;
        this.onFilterChange();
      }
    });
  }

  clearCategory() {
    this.categoryFormControl.setValue('');
    this.filterCategory = '';
    this.onFilterChange();

  }

  /** Whether the number of selected elements matches the total number of selectable rows. */
  isAllSelected() {
    let result = true;
    let numRows = 0;
    this.dataSource.data.forEach(item => {
      numRows++;
      const rowChecked = this.selection.selected.some(function (el) {
        return (el.id === item.id);
      });
      if (!rowChecked) {
        result = false;
      }
    });
    return result;
  }

  /** Selects all selectable rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(
        row => {
          this.selection.select(row);
        });
  }

  resetValidation() {
    this.nameError = null;
    this.nameFormControl.markAsDirty();
    this.validFromError = null;
    this.validFromFormControl.markAsDirty();
    this.validToError = null;
    this.validToFormControl.markAsDirty();
  }

  addArticlesToAssortment() {
    this.saving = true;
    const articles = [];
    if (this.selectedPricelist) {
      this.selection.selected.forEach(article => {
        articles.push(article.article);
      });
    } else {
      this.selection.selected.forEach(article => {
        articles.push(
          {'id': article.id});
      });
    }

    this.assortmentService.addArticlesToAssortment(this.organizationId, this.assortmentId, articles).subscribe(
      res => {
        this.saving = false;
        this.alertService.success('Sparat');
      }, error => {
        this.saving = false;
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  goBack() {
    this.location.back();
  }

}

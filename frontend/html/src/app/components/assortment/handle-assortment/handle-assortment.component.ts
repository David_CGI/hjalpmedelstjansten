/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {CommonService} from '../../../services/common.service';
import {Organization} from '../../../models/organization/organization.model';
import {FormControl, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {AuthService} from '../../../auth/auth.service';
import {HelptextService} from '../../../services/helptext.service';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {OrganizationService} from '../../../services/organization.service';
import {AlertService} from '../../../services/alert.service';
import {Assortment} from '../../../models/assortment/assortment.model';
import {AssortmentService} from '../../../services/assortment.service';
import {County} from '../../../models/assortment/county.model';
import {UserService} from '../../../services/user.service';
import {User} from '../../../models/user/user.model';
import {SelectionModel} from '@angular/cdk/collections';
import {Municipality} from '../../../models/assortment/municipality.model';
import {Article} from '../../../models/product/article.model';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {Subject} from 'rxjs/Subject';
import * as fileSaver from "file-saver";
import {ProductService} from "../../../services/product.service";

@Component({
  selector: 'app-handle-assortment',
  templateUrl: './handle-assortment.component.html',
  styleUrls: ['./handle-assortment.component.scss']
})
export class HandleAssortmentComponent implements OnInit {
  displayedColumns = ['typ', 'artikelnummer', 'artikelbenämning', 'organisation', 'remove'];

  loaded = false;
  loading = false;
  saving = false;
  editMode = false;
  isSuperAdmin = false;
  isCustomer = false;
  query = '';
  tmpQuery = '';
  searchChanged: Subject<string> = new Subject<string>();
  queryParams = '';
  searchTotal;
  offset = 25;
  totalPages;
  currentPage = 1;
  paginatorArray = [];
  allPages;
  dataSource = null;
  articles = [];
  assortmentManagers: Array<User>;
  selectedAssortmentManagers: Array<User>;
  selectedAssortmentManagersString = '';
  assortment: Assortment = new Assortment();
  counties: Array<County>;
  selectedCounty: County;
  municipalitySelection = new SelectionModel<Municipality>(true, []);
  organization: Organization = new Organization();
  organizationId: number;
  assortmentId: number;
  helpTexts;
  helpTextsLoaded = false;
  flexDirection: string;

  // Permissions
  hasPermissionToCreateAssortment = this.hasPermission('assortment:create');
  hasPermissionToDeleteAssortment = this.hasPermission('assortment:delete');
  hasPermissionToUpdateAssortment = this.hasPermission('assortment:update');
  hasPermissionToAddAllArticles = this.hasPermission('assortmentarticle:create_all');
  hasPermissionToDeleteAllArticles = this.hasPermission('assortmentarticle:delete_all');
  hasPermissionToAddArticles = false;
  hasPermissionToDeleteArticles = false;
  isOwnOrganization = false;

  // Form controls

  nameFormControl = new FormControl(null, {
    updateOn: 'blur'
  });

  validFromFormControl = new FormControl(null, {
    updateOn: 'blur'
  });
  validToFormControl = new FormControl(null, {
    updateOn: 'blur'
  });

  // Validation errors
  nameError;
  validFromError;
  validToError;

  constructor(private organizationService: OrganizationService,
              private assortmentService: AssortmentService,
              private userService: UserService,
              private commonService: CommonService,
              private route: ActivatedRoute,
              private alertService: AlertService,
              private authService: AuthService,
              private dialog: MatDialog,
              private location: Location,
              private productService: ProductService,
              private helpTextService: HelptextService) {
    this.commonService.isUsingIE() ? this.flexDirection = 'row' : this.flexDirection = 'column';
    this.route.params.subscribe(params => {
      this.organizationId = params.organizationId;
      this.assortmentId = params.assortmentId;
    });
    Number(this.organizationId) === this.authService.getOrganizationId() ? this.isOwnOrganization = true : this.isOwnOrganization = false;
    this.hasPermissionToCreateAssortment && this.isOwnOrganization ?
      this.hasPermissionToCreateAssortment = true : this.hasPermissionToCreateAssortment = false;
    this.hasPermissionToDeleteAssortment && this.isOwnOrganization ?
      this.hasPermissionToDeleteAssortment = true : this.hasPermissionToDeleteAssortment = false;
    this.hasPermissionToUpdateAssortment && this.isOwnOrganization ?
      this.hasPermissionToUpdateAssortment = true : this.hasPermissionToUpdateAssortment = false;
    this.isSuperAdmin = this.authService.isSuperAdmin();

    // The user is considered done with their search after 500 ms without further input
    this.searchChanged.pipe(
      debounceTime(500),
      distinctUntilChanged())
      .subscribe(query => {
          this.query = encodeURIComponent(query);
          this.onQueryChange();
        }
      );
  }

  ngOnInit() {
    this.helpTexts = this.helpTextService.getTexts().subscribe(
      texts => {
        this.helpTexts = texts;
        this.helpTextsLoaded = true;
      }
    );
    this.assortmentService.getCounties().subscribe(
      counties => {
        this.counties = counties;
        this.userService.getUserByRoles(this.organizationId, 'CustomerAssignedAssortmentManager').subscribe(
          managers => {
            this.assortmentManagers = managers;

            this.onEditModeChange();
            if (this.assortmentId) {
              this.getAssortment();
            } else {
              // Set default values
              this.editMode = true;
              this.onEditModeChange();
              this.loaded = true;
            }
          }, error => {
            this.alertService.clear();
            error.error.errors.forEach(err => {
              this.alertService.error(err);
            });
          }
        );
      }
    );
  }

  toggleEditMode() {
    this.editMode = !this.editMode;
    this.onEditModeChange();
  }

  exportSearchResults() {
    const query = '?query=' + this.query + this.queryParams;
    this.assortmentService.exportArticlesForAssortment(this.organizationId, this.assortmentId, query).subscribe(
      data => {
        fileSaver.saveAs(data.body, data.headers.get('Content-Disposition').split(';')[1].trim().split('=')[1]);
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  onEditModeChange() {
    if (!this.editMode) {
      this.nameFormControl.disable();
      this.validFromFormControl.disable();
      this.validToFormControl.disable();
      this.selectedAssortmentManagersString = '';
      if (this.assortment.managers && this.assortment.managers.length) {
        this.assortment.managers.forEach(manager => {
          if (this.selectedAssortmentManagersString) {
            this.selectedAssortmentManagersString += '\n';
          }
          this.selectedAssortmentManagersString += (manager.firstName + ' ' + manager.lastName);
        });
      }

    } else if (!this.isSuperAdmin) {
      this.nameFormControl.enable();
      this.validFromFormControl.enable();
      this.validToFormControl.enable();
    }
  }

  onQueryChanged(text: string) {
    this.searchChanged.next(text);
  }

  onQueryChange() {
    this.currentPage = 1;
    this.getArticlesForAssortment('?query=' + this.query);
  }

  goToPage(page) {
    if (page !== '...' && page !== this.currentPage) {
      this.loading = true;
      this.currentPage = page;
      this.assortmentService.searchArticlesForAssortment(this.organizationId, this.assortmentId,
        '?query=' + this.query + '&offset=' + (this.currentPage - 1) * this.offset).subscribe(
        res => {
          this.articles = res.body;
          this.dataSource = new MatTableDataSource<Assortment>(this.articles);
          this.currentPage = page;
          this.generatePaginator();
        }, error => {
          this.alertService.clear();
          error.error.errors.forEach(err => {
            this.alertService.error(err);
          });
        }
      );
    }
  }

  generatePaginator() {
    let hasFirstEllipsis = false;
    let hasSecondEllipsis = false;
    this.paginatorArray = [];
    if (this.searchTotal > this.offset) {
      if (this.totalPages < 6) {
        this.paginatorArray = this.allPages;
      } else {
        this.allPages.forEach((pageNumber) => {
          if (pageNumber === 1
            || pageNumber === this.currentPage
            || pageNumber === this.totalPages
            || pageNumber === this.currentPage + 1
            || pageNumber === this.currentPage - 1) {
            this.paginatorArray.push(pageNumber);
          } else if (!hasFirstEllipsis && pageNumber < this.currentPage && (pageNumber !== this.currentPage - 1)) {
            this.paginatorArray.push('...');
            hasFirstEllipsis = true;
          } else if (!hasSecondEllipsis && pageNumber > this.currentPage && pageNumber !== this.currentPage + 1) {
            this.paginatorArray.push('...');
            hasSecondEllipsis = true;
          }
        });
      }
    }
    this.loaded = true;
    this.loading = false;
  }

  hasPermission(permission): boolean {
    if (!localStorage.getItem('token')) {
      return false;
    }
    return this.authService.hasPermission(permission);
  }

  getAssortment() {
    this.assortmentService.getAssortment(this.organizationId, this.assortmentId).subscribe(
      res => {
        this.assortmentId = res.id;
        this.assortment = res;
        this.nameFormControl.setValue(res.name);
        if (res.managers) {
          const userId = this.authService.getUserId();
          res.managers.forEach(manager => {
            if (manager.id === userId) {
              this.hasPermissionToAddArticles = true;
              this.hasPermissionToDeleteArticles = true;
            }
          });
          this.selectedAssortmentManagers = this.assortmentManagers.filter(function (o) {
            return res.managers.some(function (o2) {
              return o.id === o2.id;
            });
          });
          this.selectedAssortmentManagersString = '';
          this.assortment.managers.forEach(manager => {
            if (this.selectedAssortmentManagersString) {
              this.selectedAssortmentManagersString += '\n';
            }
            this.selectedAssortmentManagersString += (manager.firstName + ' ' + manager.lastName);
          });
        }
        if (this.assortment.county && this.assortment.county) {
          this.selectedCounty = this.counties.find(x => x.id === this.assortment.county.id);
          if (this.selectedCounty.municipalities && this.selectedCounty.municipalities.length) {
            this.selectedCounty.municipalities.forEach(municipality => {
              this.assortment.municipalities.forEach(selectedMunicipality => {
                if (municipality.id === selectedMunicipality.id) {
                  this.municipalitySelection.select(municipality);
                }
              });
            });
          }
        }
        this.convertToDateString();
        this.loaded = true;
        this.getArticlesForAssortment('?query=');
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  getArticlesForAssortment(query) {
    this.loading = true;
    this.assortmentService.searchArticlesForAssortment(this.organizationId, this.assortmentId, query).subscribe(
      res => {
        this.searchTotal = res.headers.get('X-Total-Count');
        this.searchTotal !== '0' ? this.articles = res.body : this.articles = [];
        this.dataSource = new MatTableDataSource<Article>(this.articles);
        this.totalPages = Math.ceil(this.searchTotal / this.offset);
        this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
        this.generatePaginator();
      }, error => {
        this.loading = false;
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  removeArticleFromAssortment(articleId) {
    this.assortmentService.removeArticleFromAssortment(this.organizationId, this.assortmentId, articleId).subscribe(
      res => {
        this.getArticlesForAssortment('?query=' + this.query);
        this.alertService.success('Sparat');
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.handleServerValidation(err);
          this.alertService.error(err);
        });
      }
    );
  }

  convertToDateString() {
    this.validFromFormControl.setValue(new Date(this.assortment.validFrom));
    if (this.assortment.validTo) {
      this.validToFormControl.setValue(new Date(this.assortment.validTo));
    } else {
      this.validToFormControl.setValue(null);
    }
  }

  getMillisecondsFromDate(date): number {
    if (date) {
      return date.getTime();
    }
    return null;
  }

  handleServerValidation(error): void {
    switch (error.field) {
      case 'name': {
        this.nameError = error.message;
        this.nameFormControl.setErrors(Validators.pattern(''));
        break;
      }
      case 'validFrom': {
        this.validFromError = error.message;
        this.validFromFormControl.setErrors(Validators.pattern(''));
        break;
      }
      case 'validTo': {
        this.validToError = error.message;
        this.validToFormControl.setErrors(Validators.pattern(''));
        break;
      }
    }
  }

  resetValidation() {
    this.nameError = null;
    this.nameFormControl.markAsDirty();
    this.validFromError = null;
    this.validFromFormControl.markAsDirty();
    this.validToError = null;
    this.validToFormControl.markAsDirty();
  }

  onCountySelection() {
    this.municipalitySelection.clear();
  }

  saveAssortment() {
    this.saving = true;
    this.resetValidation();
    this.assortment.name = this.nameFormControl.value;
    this.assortment.validFrom = this.getMillisecondsFromDate(this.validFromFormControl.value);
    this.assortment.validTo = this.getMillisecondsFromDate(this.validToFormControl.value);
    this.assortment.managers = this.selectedAssortmentManagers;
    if (this.isSuperAdmin) {
      this.assortment.county = this.selectedCounty;
      this.assortment.municipalities = this.municipalitySelection.selected;
    }

    if (this.assortmentId) {
      this.updateAssortment();
    } else {
      this.createAssortment();
    }
  }

  updateAssortment() {
    this.assortmentService.updateAssortment(this.organizationId, this.assortmentId, this.assortment).subscribe(
      res => {
        this.assortment = res;
        this.toggleEditMode();
        this.saving = false;
        this.alertService.success('Sparat');
      }, error => {
        this.saving = false;
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.handleServerValidation(err);
          this.alertService.error(err);
        });
      }
    );
  }

  createAssortment() {
    this.assortmentService.createAssortment(this.organizationId, this.assortment).subscribe(
      res => {
        this.assortmentId = res.id;
        this.assortment = res;
        this.toggleEditMode();
        this.saving = false;
        this.alertService.success('Sparat');
      }, error => {
        this.saving = false;
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.handleServerValidation(err);
          this.alertService.error(err);
        });
      }
    );
  }

  cancel() {
    this.getAssortment();
    this.toggleEditMode();
  }

  goBack() {
    this.location.back();
  }

}

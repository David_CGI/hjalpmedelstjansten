/*
* Copyright (C) 2019 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../auth/auth.service';
import {MatTableDataSource} from '@angular/material';
import {ActivatedRoute} from '@angular/router';
import {Assortment} from '../../models/assortment/assortment.model';
import {AssortmentService} from '../../services/assortment.service';
import {AlertService} from '../../services/alert.service';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {Subject} from 'rxjs/Subject';
import {HelptextService} from '../../services/helptext.service';
import {StorageHelper as AssortmentStorage} from '../../helpers/storage.helper';
import * as fileSaver from "file-saver";

@Component({
  selector: 'app-assortment',
  templateUrl: './assortment.component.html',
  styleUrls: ['./assortment.component.scss']
})
export class AssortmentComponent implements OnInit {
  displayedColumns = ['namn', 'organisation', 'län', 'giltigt från', 'giltigt till', 'utbudsansvarig', 'anchor'];

  //Sets the storage index to be used.
  storage = new AssortmentStorage('Assortment');
  loadSessionStorage = false;


  loaded = false;
  loading = false;
  organizationId: number;
  isSuperAdmin = false;
  query = '';
  tmpQuery = '';
  searchChanged: Subject<string> = new Subject<string>();
  queryParams = '';
  searchTotal;
  offset = 25;
  totalPages;
  currentPage = 1;
  paginatorArray = [];
  allPages;
  dataSource = null;
  assortments = [];
  helpTexts;
  helpTextsLoaded = false;

  // Filters
  filterCurrent = false;
  filterFuture = false;
  filterDiscontinued = false;
  include = 'ALL';

  // Permissions
  hasPermissionToViewAssortment = this.hasPermission('assortment:view');
  hasPermissionToCreateAssortment = this.hasPermission('assortment:create');
  isOwnOrganization = false;

  constructor(private route: ActivatedRoute,
              private authService: AuthService,
              private assortmentService: AssortmentService,
              private alertService: AlertService,
              private helpTextService: HelptextService) {
    this.route.params.subscribe(params => this.organizationId = params.organizationId);
    Number(this.organizationId) === this.authService.getOrganizationId() ? this.isOwnOrganization = true : this.isOwnOrganization = false;
    this.hasPermissionToViewAssortment && this.isOwnOrganization ?
      this.hasPermissionToViewAssortment = true : this.hasPermissionToViewAssortment = false;
    this.hasPermissionToCreateAssortment && this.isOwnOrganization ?
      this.hasPermissionToCreateAssortment = true : this.hasPermissionToCreateAssortment = false;
    this.isSuperAdmin = this.authService.isSuperAdmin();

    // The user is considered done with their search after 500 ms without further input
    this.searchChanged.pipe(
      debounceTime(500),
      distinctUntilChanged())
      .subscribe(query => {
          this.query = encodeURIComponent(query);
          this.onQueryChange();
          this.currentPage = 1;

        }
      );
  }

  ngOnInit() {
    this.helpTexts = this.helpTextService.getTexts().subscribe(
      texts => {
        this.helpTexts = texts;
        this.helpTextsLoaded = true;
      }
    );

    if(this.storage.hasPageBeenVisisted()) {
      this.loadSessionStorage = true;
      this.getSessionStorage();

      if(this.currentPage > 1) {
          //Assortment differs implementation-wise from the other modules - it's a different recursive flow - have to call searchAssortments manually.
          this.searchAssortments('?query=' + this.query + this.queryParams + '&offset=' + (this.currentPage - 1) * this.offset);
      } else  {
         this.searchAssortments('?query=' + this.tmpQuery + this.queryParams);
      }

    } else {
      this.searchAssortments('?include=ALL');
    }
}

  setSessionStorage() {
    this.storage.setItem('Query', this.tmpQuery.toString());
    this.storage.setItem('Offset', this.offset.toString());
    this.storage.setItem('FilterDiscontinued', this.filterDiscontinued.toString());
    this.storage.setItem('CurrentPage', this.currentPage.toString());
    this.storage.setItem('HelpTextsLoaded', this.helpTextsLoaded.toString());
    this.storage.setItem('FilterFuture', this.filterFuture.toString());
    this.storage.setItem('FilterCurrent', this.filterCurrent.toString());
    this.storage.setItem('Include', this.include);
  }

  getSessionStorage() {
    this.offset = this.storage.getItemNumber('Offset');
    this.currentPage = this.storage.getItemNumber('CurrentPage');
    this.filterDiscontinued = this.storage.getItemBoolean('FilterDiscontinued');
    this.helpTextsLoaded = this.storage.getItemBoolean('HelpTextsLoaded');
    this.filterFuture = this.storage.getItemBoolean('FilterFuture');
    this.filterCurrent = this.storage.getItemBoolean('FilterCurrent');
    this.include = this.storage.getItemString('Include');


    //Query needs to be at the end of the method.
    if (this.storage.getItemString('Query') !== 'null' || this.storage.getItemString('Query') !== '' ) {
    this.tmpQuery = this.storage.getItemString('Query');
    this.query = this.storage.getItemString('Query');
  }
}

  onQueryChanged(text: string) {
    this.searchChanged.next(text);
  }

  onQueryChange() {
    this.onFilterChange();
  }

  onFilterChange() {
    this.queryParams = '';
    this.currentPage = 1;
    this.filterCurrent ? this.queryParams += '&status=CURRENT' : this.queryParams = this.queryParams;
    this.filterFuture ? this.queryParams += '&status=FUTURE' : this.queryParams = this.queryParams;
    this.filterDiscontinued ? this.queryParams += '&status=DISCONTINUED' : this.queryParams = this.queryParams;
    this.queryParams += '&include=' + this.include;
    this.searchAssortments('?query=' + this.query + this.queryParams);
    this.setSessionStorage();
  }

  searchAssortments(query) {
    this.loading = true;
    if (this.isSuperAdmin) {
      this.assortmentService.searchAllAssortments(query).subscribe(
        res => {
          this.searchTotal = res.headers.get('X-Total-Count');
          this.searchTotal !== '0' ? this.assortments = res.body : this.assortments = [];
          this.dataSource = new MatTableDataSource<Assortment>(this.assortments);
          this.totalPages = Math.ceil(this.searchTotal / this.offset);
          this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
          this.generatePaginator();
          this.setSessionStorage();

        }, error => {
          this.alertService.clear();
          error.error.errors.forEach(err => {
            this.alertService.error(err);
          });
        }
      );
    } else {
      this.assortmentService.searchAssortments(this.organizationId, query).subscribe(
        res => {
          this.searchTotal = res.headers.get('X-Total-Count');
          this.searchTotal !== '0' ? this.assortments = res.body : this.assortments = [];
          this.dataSource = new MatTableDataSource<Assortment>(this.assortments);
          this.totalPages = Math.ceil(this.searchTotal / this.offset);
          this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
          this.generatePaginator();
          this.setSessionStorage();

        }, error => {
          this.alertService.clear();
          error.error.errors.forEach(err => {
            this.alertService.error(err);
          });
        }
      );
    }

  }

  exportAssortments() {
    // window.location.href = this.assortmentService.getExportAssortmentsUrl(this.organizationId) +
    // '?query=' + this.query + this.queryParams;
  }

  exportSearchResults() {
    const query = '?query=' + this.query + this.queryParams;
    if (!this.isSuperAdmin) {
      this.assortmentService.exportAssortments(this.organizationId, query).subscribe(
        data => {
          fileSaver.saveAs(data.body, data.headers.get('Content-Disposition').split(';')[1].trim().split('=')[1]);
        }, error => {
          this.alertService.clear();
          error.error.errors.forEach(err => {
            this.alertService.error(err);
          });
        }
      );
    }
    else {
      this.assortmentService.exportAllAssortments(query).subscribe(
        data => {
          fileSaver.saveAs(data.body, data.headers.get('Content-Disposition').split(';')[1].trim().split('=')[1]);
        }, error => {
          this.alertService.clear();
          error.error.errors.forEach(err => {
            this.alertService.error(err);
          });
        }
      );
    }
  }

  hasPermission(permission): boolean {
    if (!localStorage.getItem('token')) {
      return false;
    }
    return this.authService.hasPermission(permission);
  }

  goToPage(page) {
    if (page !== '...' && page !== this.currentPage) {
      this.currentPage = page;
      this.storage.setItem('CurrentPage', page);
      this.searchAssortments('?query=' + this.query + this.queryParams + '&offset=' + (this.currentPage - 1) * this.offset);
    }
  }

  generatePaginator() {
    let hasFirstEllipsis = false;
    let hasSecondEllipsis = false;
    this.paginatorArray = [];
    if (this.searchTotal > this.offset) {
      if (this.totalPages < 6) {
        this.paginatorArray = this.allPages;
      } else {
        this.allPages.forEach((pageNumber) => {
          if (pageNumber === 1
            || pageNumber === this.currentPage
            || pageNumber === this.totalPages
            || pageNumber === this.currentPage + 1
            || pageNumber === this.currentPage - 1) {
            this.paginatorArray.push(pageNumber);
          } else if (!hasFirstEllipsis && pageNumber < this.currentPage && (pageNumber !== this.currentPage - 1)) {
            this.paginatorArray.push('...');
            hasFirstEllipsis = true;
          } else if (!hasSecondEllipsis && pageNumber > this.currentPage && pageNumber !== this.currentPage + 1) {
            this.paginatorArray.push('...');
            hasSecondEllipsis = true;
          }
        });
      }
    }
    this.loaded = true;
    this.loading = false;
  }

}

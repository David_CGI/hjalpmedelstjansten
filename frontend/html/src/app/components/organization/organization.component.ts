/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {OrganizationService} from '../../services/organization.service';
import {AuthService} from '../../auth/auth.service';
import {HelptextService} from '../../services/helptext.service';
import {AlertService} from '../../services/alert.service';
import {Subject} from 'rxjs/Subject';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import * as fileSaver from "file-saver";
import {StorageHelper as OrganisationStorage} from '../../helpers/storage.helper';

@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.scss']
})
export class OrganizationComponent implements OnInit {

  displayedColumns = ['organisation', 'orggrupp', 'orgnr', 'gln', 'land', 'status', 'anchor'];

  //Sets the storage index to be used.
  storage = new OrganisationStorage('Organization');
  loadSessionStorage = false;

  loaded = false;
  loading = true;
  organizations = [];
  dataSource = null;
  query = '';
  queryParams = '';
  tmpQuery = '';
  searchChanged: Subject<string> = new Subject<string>();
  searchTotal;
  offset = 25;
  totalPages;
  currentPage = 1;
  paginatorArray = [];
  allPages;
  helpTexts;
  helpTextsLoaded = false;
  isServiceOwner: boolean;

  // Permissions
  hasPermissionToCreateOrganization = this.hasPermission('organization:create');

  constructor(private organizationService: OrganizationService,
              private authService: AuthService,
              private alertService: AlertService,
              private helpTextService: HelptextService) {
    // The user is considered done with their search after 500 ms without further input
    this.searchChanged.pipe(
      debounceTime(500),
      distinctUntilChanged())
      .subscribe(query => {
          this.query = encodeURIComponent(query);
          this.onQueryChange();
          this.currentPage = 1;
        }
      );
  }

  ngOnInit() {
    this.organizationService.getOrganization(this.authService.getOrganizationId()).subscribe(
      res => {
        res.organizationType === 'SERVICE_OWNER' ? this.isServiceOwner = true : this.isServiceOwner = false;
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
    this.helpTexts = this.helpTextService.getTexts().subscribe(
      texts => {
        this.helpTexts = texts;
        this.helpTextsLoaded = true;
      }
    );


    if (this.storage.hasPageBeenVisisted()) {
      this.loadSessionStorage = true;
      this.getSessionStorage();

      if(this.currentPage > 1) {
        this.goToPage(this.currentPage);
      } else  {
      this.searchOrganizations(this.tmpQuery);
      }

      this.loadSessionStorage = false;
    } else {
      this.searchOrganizations('');
    }
  }

  onQueryChanged(text: string) {
    this.searchChanged.next(text);
  }

  onQueryChange() {
    this.searchOrganizations(this.query);
  }

  hasPermission(permission): boolean {
    if (!localStorage.getItem('token')) {
      return false;
    }
    return this.authService.hasPermission(permission);
  }

  setSessionStorage() {

    this.storage.setItem('Query', this.tmpQuery.toString());
    this.storage.setItem('CurrentPage', this.currentPage.toString());
    this.storage.setItem('HelpTextsLoaded', this.helpTextsLoaded.toString());
  }

  getSessionStorage() {

    this.currentPage = this.storage.getItemNumber('CurrentPage');
    this.helpTextsLoaded = this.storage.getItemBoolean('HelpTextsLoaded');

    //Query needs to be at the end of the method.
    if(this.storage.getItemString('Query') !== 'null' || this.storage.getItemString('Query') !== '' ) {
      this.tmpQuery = this.storage.getItemString('Query');
      this.query = this.storage.getItemString('Query');
  }
}

  searchOrganizations(query) {
    this.loading = true;
    this.organizationService.searchOrganizations(query).subscribe(
      res => {
        this.searchTotal = res.headers.get('X-Total-Count');
        this.searchTotal !== '0' ? this.organizations = res.body : this.organizations = [];
        this.totalPages = Math.ceil(this.searchTotal / this.offset);
        this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
        this.dataSource = new MatTableDataSource<Organization>(this.organizations);
        this.generatePaginator();
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  exportSearchResults() {
    const query = '?query=' + this.query + this.queryParams;

    this.organizationService.exportOrganizations(query).subscribe(
      data => {
        fileSaver.saveAs(data.body, data.headers.get('Content-Disposition').split(';')[1].trim().split('=')[1]);
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  goToPage(page) {

    if (page !== '...' && page !== this.currentPage || this.loadSessionStorage) {
      this.loading = true;
      this.currentPage = page;
      this.organizationService.searchOrganizations(this.query + '&offset=' + (this.currentPage - 1) * this.offset).subscribe(
        res => {
          this.organizations = res.body;
          this.searchTotal = res.headers.get('X-Total-Count');
          this.searchTotal !== '0' ? this.organizations = res.body : this.organizations = [];
          this.totalPages = Math.ceil(this.searchTotal / this.offset);
          this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
          this.dataSource = new MatTableDataSource<Organization>(this.organizations);
          this.storage.setItem('CurrentPage', this.currentPage.toString());
          this.currentPage = page;
          this.generatePaginator();
        }, error => {
          this.alertService.clear();
          error.error.errors.forEach(err => {
            this.alertService.error(err);
          });
        }
      );
    }
  }

  generatePaginator() {
    let hasFirstEllipsis = false;
    let hasSecondEllipsis = false;
    this.paginatorArray = [];
    if (this.searchTotal > this.offset) {
      if (this.totalPages < 6) {
        this.paginatorArray = this.allPages;
      } else {
        this.allPages.forEach((pageNumber) => {
          if (pageNumber === 1
            || pageNumber === this.currentPage
            || pageNumber === this.totalPages
            || pageNumber === this.currentPage + 1
            || pageNumber === this.currentPage - 1) {
            this.paginatorArray.push(pageNumber);
          } else if (!hasFirstEllipsis && pageNumber < this.currentPage && (pageNumber !== this.currentPage - 1)) {
            this.paginatorArray.push('...');
            hasFirstEllipsis = true;
          } else if (!hasSecondEllipsis && pageNumber > this.currentPage && pageNumber !== this.currentPage + 1) {
            this.paginatorArray.push('...');
            hasSecondEllipsis = true;
          }
        });
      }
    }

    this.setSessionStorage();
    this.loading = false;
    this.loaded = true;
  }

}

export interface Organization {
  organization: string;
  type: string;
  orgnumber: string;
  country: string;
  status: string;
}



/*
* Copyright (C) 2019 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, OnInit} from '@angular/core';
import {Organization} from '../../../models/organization/organization.model';
import {OrganizationService} from '../../../services/organization.service';
import {ActivatedRoute, Router} from '@angular/router';
import {PostAddress} from '../../../models/post-address.model';
import {AlertService} from '../../../services/alert.service';
import {FormControl, Validators} from '@angular/forms';
import {CountryService} from '../../../services/country.service';
import {ElectronicAddress} from '../../../models/electronic-address.model';
import {AuthService} from '../../../auth/auth.service';
import {Location} from '@angular/common';
import {MatTableDataSource} from '@angular/material';
import {UserService} from '../../../services/user.service';
import {HelptextService} from '../../../services/helptext.service';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {Subject} from 'rxjs/Subject';
import {CommonService} from '../../../services/common.service';
import * as fileSaver from "file-saver";

@Component({
  selector: 'app-handle-organization',
  templateUrl: './handle-organization.component.html',
  styleUrls: ['./handle-organization.component.scss']
})
export class HandleOrganizationComponent implements OnInit {
  displayedColumns = ['namn', 'användarnamn', 'titel', 'e-post', 'status', 'anchor'];

  loaded = false;
  saving = false;
  editMode = false;
  showSaveBar = true;
  initialTab = 0;
  organization: Organization = new Organization();
  organizationId: number;
  countries = [];
  businessLevels = '';
  visitAddress: PostAddress = new PostAddress();
  deliveryAddress: PostAddress = new PostAddress();
  users = [];
  filteredUsers = [];
  dataSource = null;
  query = '';
  tmpQuery = '';
  searchChanged: Subject<string> = new Subject<string>();
  searchTotal;
  offset = 25;
  totalPages;
  currentPage = 1;
  paginatorArray = [];
  allPages;
  showInactiveUsers = false;
  helpTexts;
  helpTextsLoaded = false;
  flexDirection: string;
  isServiceOwner: boolean;
  tabName = "";

  // Permissions
  hasPermissionToUpdateOrganization = this.hasPermission('organization:update');
  hasPermissionToDeleteOrganization = this.hasPermission('organization:delete');
  hasPermissionToUpdateContact = this.hasPermission('organization:update_contact');
  hasPermissionToCreateBusinessLevel = this.hasPermission('businesslevel:create');
  hasPermissionToInactivateBusinessLevel = this.hasPermission('businesslevel:inactivate');
  hasPermissionToCreateUser = this.hasPermission('user:create');
  hasPermissionToCreateOwnUser = this.hasPermission('user:create_own');
  hasPermissionToViewUser = this.hasPermission('user:view');
  isOwnOrganization = false;

  // Form controls
  nameFormControl = new FormControl(null, {
    updateOn: 'blur'
  });

  organizationNumberFormControl = new FormControl(null, {
    updateOn: 'blur'
  });

  glnFormControl = new FormControl(null, {
    validators: Validators.pattern('^(\\d{13})?$'),
    updateOn: 'blur'
  });

  validFromFormControl = new FormControl(null, {
    updateOn: 'blur'
  });
  validToFormControl = new FormControl(null, {
    updateOn: 'blur'
  });

  webFormControl = new FormControl(null, {
    updateOn: 'blur'
  });

  emailFormControl = new FormControl('', {
    updateOn: 'blur'
  });

  visitZipFormControl = new FormControl(null, {
    updateOn: 'blur'
  });

  deliverZipFormControl = new FormControl(null, {
    updateOn: 'blur'
  });

  // Validation errors
  nameError;
  numberError;
  glnError;
  validFromError;
  validToError;
  webError;
  emailError;
  visitZipError;
  deliverZipError;

  constructor(private organizationService: OrganizationService,
              private countryService: CountryService,
              private userService: UserService,
              private route: ActivatedRoute,
              private router: Router,
              private alertService: AlertService,
              private authService: AuthService,
              private location: Location,
              private helpTextService: HelptextService,
              private commonService: CommonService) {
    this.commonService.isUsingIE() ? this.flexDirection = 'row' : this.flexDirection = 'column';
    this.route.params.subscribe(params => {
      this.organizationId = params.id;
      if (params.tab === 'user') {
        this.initialTab = 2;
      }
    });
    Number(this.organizationId) === this.authService.getOrganizationId() ? this.isOwnOrganization = true : this.isOwnOrganization = false;
    this.hasPermissionToUpdateContact && this.isOwnOrganization ?
      this.hasPermissionToUpdateContact = true : this.hasPermissionToUpdateContact = false;
    this.hasPermissionToCreateOwnUser && this.isOwnOrganization ?
      this.hasPermissionToCreateOwnUser = true : this.hasPermissionToCreateOwnUser = false;

    // The user is considered done with their search after 500 ms without further input
    this.searchChanged.pipe(
      debounceTime(500),
      distinctUntilChanged())
      .subscribe(query => {
          this.query = encodeURIComponent(query);
          this.onQueryChange();
        }
      );
  }

  ngOnInit() {
    this.helpTexts = this.helpTextService.getTexts().subscribe(
      texts => {
        this.helpTexts = texts;
        this.helpTextsLoaded = true;
      }
    );
    this.organizationService.getOrganization(this.authService.getOrganizationId()).subscribe(
      res => {
        res.organizationType === 'SERVICE_OWNER' ? this.isServiceOwner = true : this.isServiceOwner = false;
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
    this.onEditModeChange();

    this.countryService.getCountries().subscribe(
      res => {
        this.countries = res;
        this.organization.country = this.countries.find(x => x.name === 'Sverige');
        if (this.organizationId) {
          this.organizationNumberFormControl.disable();
          if (this.hasPermissionToViewUser) {
            this.searchUsers();
          }
          this.getOrganization();
        } else {
          // Set default values
          this.editMode = true;
          this.onEditModeChange();

          this.validFromFormControl.setValue(new Date());

          this.organization.electronicAddress = new ElectronicAddress();
          this.organization.postAddresses = new Array<PostAddress>();

          this.visitAddress.addressType = 'VISIT';
          this.deliveryAddress.addressType = 'DELIVERY';
          this.organization.organizationType = 'SUPPLIER';
          this.loaded = true;
        }
      }
    );
  }

  onTabSelection(event) {
    this.tabName = event.tab.textLabel;
    if (event.index === 0) {
      this.showSaveBar = true;
      this.getBusinessLevels();
    } else {
      this.showSaveBar = false;
    }
  }

  toggleEditMode() {
    this.editMode = !this.editMode;
    this.onEditModeChange();
  }

  onEditModeChange() {
    if (!this.editMode) {
      this.glnFormControl.disable();
      this.nameFormControl.disable();
      this.validFromFormControl.disable();
      this.validToFormControl.disable();
      this.emailFormControl.disable();
      this.webFormControl.disable();
      this.visitZipFormControl.disable();
      this.deliverZipFormControl.disable();
    } else {
      if (this.hasPermissionToUpdateOrganization) {
        this.glnFormControl.enable();
        this.nameFormControl.enable();
        this.validFromFormControl.enable();
        this.validToFormControl.enable();
        this.emailFormControl.enable();
        this.webFormControl.enable();
        this.visitZipFormControl.enable();
        this.deliverZipFormControl.enable();
      }
      if (this.hasPermissionToUpdateContact && !this.hasPermissionToUpdateOrganization) {
        this.emailFormControl.enable();
        this.webFormControl.enable();
        this.visitZipFormControl.enable();
        this.deliverZipFormControl.enable();
      }
    }
  }

  hasPermission(permission): boolean {
    if (!localStorage.getItem('token')) {
      return false;
    }
    return this.authService.hasPermission(permission);
  }

  getOrganization() {
    this.organizationService.getOrganization(this.organizationId).subscribe(
      res => {
        this.organization = res;
        this.nameFormControl.setValue(res.organizationName);
        this.organizationNumberFormControl.setValue(res.organizationNumber);
        this.glnFormControl.setValue(res.gln);
        this.webFormControl.setValue(res.electronicAddress.web);
        this.emailFormControl.setValue(res.electronicAddress.email);
        this.splitPostAddresses();
        this.convertToDateString();

        // Extract business levels
        this.getBusinessLevels();
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  getBusinessLevels() {
    this.organizationService.getBusinessLevels(this.organizationId).subscribe(
      res => {
        this.businessLevels = '';
        res.forEach((e) => {
          if (this.businessLevels) {
            this.businessLevels += '\n';
          }
          this.businessLevels += e.name;
        });
        this.loaded = true;
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  saveOrganization() {
    this.saving = true;
    this.resetValidation();

    this.organization.validFrom = this.getMillisecondsFromDate(this.validFromFormControl.value);
    this.organization.validTo = this.getMillisecondsFromDate(this.validToFormControl.value);
    this.organization.organizationName = this.nameFormControl.value;
    this.organization.organizationNumber = this.organizationNumberFormControl.value;
    this.organization.gln = this.glnFormControl.value;
    this.organization.electronicAddress.web = this.webFormControl.value;
    this.organization.electronicAddress.email = this.emailFormControl.value;

    this.organization.postAddresses = [];
    this.visitAddress.postCode = this.visitZipFormControl.value;
    this.organization.postAddresses.push(this.visitAddress);
    this.deliveryAddress.postCode = this.deliverZipFormControl.value;
    this.organization.postAddresses.push(this.deliveryAddress);

    if (this.organizationId) {
      this.updateOrganization();
    } else {
      this.createOrganization();
    }
  }

  updateOrganization() {
    this.organizationService.updateOrganization(this.organizationId, this.organization).subscribe(
      data => {
        this.organization = data;
        this.toggleEditMode();
        this.saving = false;
        this.alertService.success('Sparat');
      }, error => {
        this.alertService.clear();
        this.saving = false;
        error.error.errors.forEach(err => {
          this.handleServerValidation(err);
          this.alertService.error(err);
        });
      }
    );
  }

  createOrganization() {
    this.organizationService.createOrganization(this.organization).subscribe(
      data => {
        this.organizationId = data.id;
        this.organization = data;
        this.toggleEditMode();
        this.saving = false;
        this.router.navigate(['/organization/' + data.id + '/handle']);
        this.alertService.success('Sparat');
      }, error => {

        this.alertService.clear();
        this.saving = false;
        error.error.errors.forEach(err => {
          this.handleServerValidation(err);
          this.alertService.error(err);
        });
      }
    );
  }

  deleteOrganization() {
    this.saving = true;
    this.organizationService.deleteOrganization(this.organizationId).subscribe(
      res => {
        this.saving = false;
        this.router.navigate(['/organization']);
        this.alertService.success('Organisation borttagen');
      }, error => {
        this.alertService.clear();
        this.saving = false;
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  onQueryChanged(text: string) {
    this.searchChanged.next(text);
  }

  onQueryChange() {
    this.searchUsers();
  }

  onShowInactiveUsersChange() {
    this.filterUsers();
  }

  searchUsers() {
    this.userService.searchUsers(this.organizationId, this.query).subscribe(
      res => {
        this.searchTotal = res.headers.get('X-Total-Count');
        this.searchTotal !== '0' ? this.users = res.body : this.users = [];
        this.totalPages = Math.ceil(this.searchTotal / this.offset);
        this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
        this.filterUsers();
      }
    );
  }

  exportSearchResults() {
    const query = '?query=' + this.query;

    this.userService.exportUsers(!this.showInactiveUsers, this.organizationId, query).subscribe(
      data => {
        fileSaver.saveAs(data.body, data.headers.get('Content-Disposition').split(';')[1].trim().split('=')[1]);
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  filterUsers() {
    if (this.showInactiveUsers) {
      this.filteredUsers = this.users;
    } else {
      this.filteredUsers = this.users.filter(
        user => user.active);
    }
    this.dataSource = new MatTableDataSource<User>(this.filteredUsers);
    this.generatePaginator();
  }

  goToPage(page) {
    if (page !== '...' && page !== this.currentPage) {
      this.currentPage = page;
      this.userService.searchUsers(this.organizationId, this.query + '&offset=' + (this.currentPage - 1) * this.offset).subscribe(
        res => {
          this.users = res.body;
          this.filterUsers();
          this.currentPage = page;
          this.generatePaginator();
        }, error => {
          this.alertService.clear();
          error.error.errors.forEach(err => {
            this.alertService.error(err);
          });
        }
      );
    }
  }

  generatePaginator() {
    let hasFirstEllipsis = false;
    let hasSecondEllipsis = false;
    this.paginatorArray = [];
    if (this.searchTotal > this.offset) {
      if (this.totalPages < 6) {
        this.paginatorArray = this.allPages;
      } else {
        this.allPages.forEach((pageNumber) => {
          if (pageNumber === 1
            || pageNumber === this.currentPage
            || pageNumber === this.totalPages
            || pageNumber === this.currentPage + 1
            || pageNumber === this.currentPage - 1) {
            this.paginatorArray.push(pageNumber);
          } else if (!hasFirstEllipsis && pageNumber < this.currentPage && (pageNumber !== this.currentPage - 1)) {
            this.paginatorArray.push('...');
            hasFirstEllipsis = true;
          } else if (!hasSecondEllipsis && pageNumber > this.currentPage && pageNumber !== this.currentPage + 1) {
            this.paginatorArray.push('...');
            hasSecondEllipsis = true;
          }
        });
      }
    }
  }

  splitPostAddresses() {
    this.visitAddress = new PostAddress();
    this.deliveryAddress = new PostAddress();
    if (this.organization.postAddresses[0].addressType === 'VISIT') {
      this.visitAddress = this.organization.postAddresses[0];
      this.visitZipFormControl.setValue(this.organization.postAddresses[0].postCode);
    } else {
      this.deliveryAddress = this.organization.postAddresses[0];
      this.deliverZipFormControl.setValue(this.organization.postAddresses[0].postCode);
    }

    if (this.organization.postAddresses[1].addressType === 'VISIT') {
      this.visitAddress = this.organization.postAddresses[1];
      this.visitZipFormControl.setValue(this.organization.postAddresses[1].postCode);
    } else {
      this.deliveryAddress = this.organization.postAddresses[1];
      this.deliverZipFormControl.setValue(this.organization.postAddresses[1].postCode);
    }
  }

  convertToDateString() {
    this.validFromFormControl.setValue(new Date(this.organization.validFrom));
    if (this.organization.validTo) {
      this.validToFormControl.setValue(new Date(this.organization.validTo));
    } else {
      this.validToFormControl.setValue(null);
    }
  }

  getMillisecondsFromDate(date): number {
    if (date) {
      return date.getTime();
    }
    return null;
  }

  handleServerValidation(error): void {
    switch (error.field) {
      case 'organizationName': {
        this.nameError = error.message;
        this.nameFormControl.setErrors(Validators.pattern(''));
        break;
      }
      case 'organizationNumber': {
        this.numberError = error.message;
        this.organizationNumberFormControl.setErrors(Validators.pattern(''));
        break;
      }
      case 'gln': {
        this.glnError = error.message;
        this.glnFormControl.setErrors(Validators.pattern(''));
        break;
      }
      case 'validFrom': {
        this.validFromError = error.message;
        this.validFromFormControl.setErrors(Validators.pattern(''));
        break;
      }
      case 'validTo': {
        this.validToError = error.message;
        this.validToFormControl.setErrors(Validators.pattern(''));
        break;
      }
      case 'electronicAddress.web': {
        this.webError = error.message;
        this.webFormControl.setErrors(Validators.pattern(''));
        break;
      }
      case 'electronicAddress.email': {
        this.emailError = error.message;
        this.emailFormControl.setErrors(Validators.pattern(''));
        break;
      }
      case 'postAddresses[0].postCode': {
        this.visitZipError = error.message;
        this.visitZipFormControl.setErrors(Validators.pattern(''));
        break;
      }
      case 'postAddresses[1].postCode': {
        this.deliverZipError = error.message;
        this.deliverZipFormControl.setErrors(Validators.pattern(''));
        break;
      }
    }
  }

  resetValidation() {
    this.nameError = null;
    this.nameFormControl.markAsDirty();
    this.numberError = null;
    this.organizationNumberFormControl.markAsDirty();
    this.glnError = null;
    this.glnFormControl.markAsDirty();
    this.validFromError = null;
    this.validFromFormControl.markAsDirty();
    this.validToError = null;
    this.validToFormControl.markAsDirty();
    this.webError = null;
    this.webFormControl.markAsDirty();
    this.emailError = null;
    this.emailFormControl.markAsDirty();
    this.visitZipError = null;
    this.visitZipFormControl.markAsDirty();
    this.deliverZipError = null;
    this.deliverZipFormControl.markAsDirty();
  }

  cancel() {
    this.getOrganization();
    this.toggleEditMode();
  }

  goBack() {
    this.location.back();
  }

}

export interface User {
  name: string;
  username: string;
  title: string;
  email: string;
  status: string;
}

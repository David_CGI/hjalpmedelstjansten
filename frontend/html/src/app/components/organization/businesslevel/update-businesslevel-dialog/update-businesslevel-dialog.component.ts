/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-update-businesslevel-dialog',
  templateUrl: './update-businesslevel-dialog.component.html',
  styleUrls: ['./update-businesslevel-dialog.component.scss']
})
export class UpdateBusinesslevelDialogComponent implements OnInit {
  organizationId: number;
  id: string;
  businessLevelName: string;


  constructor(public dialogRef: MatDialogRef<UpdateBusinesslevelDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    this.organizationId = this.data.organizationId;
    this.id = this.data.businessLevelId;
    this.businessLevelName = this.data.businessLevelName;
  }

  done() {
    this.dialogRef.close(
      {
        'id': this.id,
        'name': this.businessLevelName
      }
    );
  }
}

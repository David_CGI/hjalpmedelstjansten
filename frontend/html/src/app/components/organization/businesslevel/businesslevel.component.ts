/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../auth/auth.service';
import {MatDialog} from '@angular/material';
import {AddBusinesslevelDialogComponent} from './add-businesslevel-dialog/add-businesslevel-dialog.component';
import {UpdateBusinesslevelDialogComponent} from './update-businesslevel-dialog/update-businesslevel-dialog.component';
import {OrganizationService} from '../../../services/organization.service';
import {ActivatedRoute} from '@angular/router';
import {BusinessLevel} from '../../../models/organization/business-level.model';
import {AlertService} from '../../../services/alert.service';

@Component({
  selector: 'app-businesslevel',
  templateUrl: './businesslevel.component.html',
  styleUrls: ['./businesslevel.component.scss']
})
export class BusinesslevelComponent implements OnInit {
  organizationId: number;
  businessLevels: Array<BusinessLevel> = new Array<BusinessLevel>();
  loading = false;

  // Permissions
  hasPermissionToCreateBusinessLevel = this.hasPermission('businesslevel:create');
  hasPermissionToInactivateBusinessLevel = this.hasPermission('businesslevel:inactivate');
  hasPermissionToDeleteBusinessLevel = this.hasPermission('businesslevel:delete');

  constructor(private authService: AuthService,
              private route: ActivatedRoute,
              private addBusinessLevelDialog: MatDialog,
              private organizationService: OrganizationService,
              private updateBusinessLevelDialog: MatDialog,
              private alertService: AlertService) {
    this.route.params.subscribe(params => this.organizationId = params.id);
  }

  ngOnInit() {
    this.loading = true;
    this.organizationService.getBusinessLevels(this.organizationId).subscribe(
      res => {
        this.businessLevels = res;
        this.loading = false;
      }, error => {
        this.loading = false;
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  hasPermission(permission): boolean {
    if (!localStorage.getItem('token')) {
      return false;
    }
    return this.authService.hasPermission(permission);
  }

  openAddBusinessLevelDialog() {
    const dialogRef = this.addBusinessLevelDialog.open(AddBusinesslevelDialogComponent, {
      width: '80%'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.loading = true;
        const businessLevel = {'name': result};
        this.organizationService.createBusinessLevel(this.organizationId, businessLevel).subscribe(
          res => {
            this.businessLevels.push(res);
            this.loading = false;
            this.alertService.success('Sparat');
          }, error => {
            this.alertService.clear();
            this.loading = false;
            error.error.errors.forEach(err => {
              this.alertService.error(err);
            });
          }
        );
      }
    });
  }

  openUpdateBusinessLevelDialog(id, name) {
    const dialogRef = this.updateBusinessLevelDialog.open(UpdateBusinesslevelDialogComponent, {
      width: '80%',
      data: {
        'organizationId': this.organizationId,
        'businessLevelId': id,
        'businessLevelName': name
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.organizationService.updateBusinessLevel(this.organizationId, id, result).subscribe(
          res => {
            this.businessLevels.forEach(level => {
              if (level.id === res.id) {
                level.name = res.name;
              }
            });
            this.loading = false;
            this.alertService.success('Sparat');
          }, error => {
            this.alertService.clear();
            this.loading = false;
            error.error.errors.forEach(err => {
              this.alertService.error(err);
            });
          }
        );
      }
    });
  }

  inactivateBusinessLevel(id) {
    this.loading = true;
    this.organizationService.inactivateBusinessLevel(this.organizationId, id).subscribe(
      res => {
        this.businessLevels.forEach(level => {
          if (level.id === res.id) {
            level.status = res.status;
          }
        });
        this.loading = false;
        this.alertService.success('Sparat');
      }, error => {
        this.alertService.clear();
        this.loading = false;
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );

  }

  deleteBusinessLevel(id, name) {
    this.loading = true;
    this.organizationService.deleteBusinessLevel(this.organizationId, id).subscribe(
      data => {
        this.organizationService.getBusinessLevels(this.organizationId).subscribe(
          res => {
            this.loading = false;
            this.businessLevels = res;
          }
        );
        this.alertService.success('Verksamhetsområde ' + name + ' borttaget');
      }, error => {
        this.alertService.clear();
        this.loading = false;
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );

  }

}

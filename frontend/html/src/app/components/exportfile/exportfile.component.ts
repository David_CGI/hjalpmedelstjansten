/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {ExportSetting} from '../../models/export-setting.model';
import {ExportService} from '../../services/export.service';
import {ActivatedRoute} from '@angular/router';
import {AlertService} from '../../services/alert.service';
import {CommonService} from '../../services/common.service';

@Component({
  selector: 'app-exportfile',
  templateUrl: './exportfile.component.html',
  styleUrls: ['./exportfile.component.scss']
})
export class ExportfileComponent implements OnInit {

  displayedColumns = ['kund', 'filnamn', 'verksamhetsområde', 'senast levererad'];

  organizationId: number;
  exportSettings: Array<ExportSetting> = new Array<ExportSetting>();
  dataSource = null;
  flexDirection: string;

  constructor(private exportService: ExportService,
              private route: ActivatedRoute,
              private alertService: AlertService,
              private commonService: CommonService) {
    this.commonService.isUsingIE() ? this.flexDirection = 'row' : this.flexDirection = 'column';
    this.route.params.subscribe(params => {
      this.organizationId = params.organizationId;
    });
  }

  ngOnInit() {
    this.getExportSettings();
  }

  getExportSettings() {
    this.exportService.getExportSettingsForOrganization(this.organizationId).subscribe(
      res => {
        this.exportSettings = res;
        this.dataSource = new MatTableDataSource<ExportSetting>(this.exportSettings);
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, OnInit} from '@angular/core';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {GpDialogComponent} from '../gp-dialog/gp-dialog.component';
import {ActivatedRoute} from '@angular/router';
import {ExportService} from '../../../services/export.service';
import {GeneralPricelist} from '../../../models/general-pricelist/general-pricelist.model';
import {ExportSetting} from '../../../models/export-setting.model';
import {RemoveGpDialogComponent} from './remove-gp-dialog/remove-gp-dialog.component';
import {AlertService} from '../../../services/alert.service';
import {CommonService} from '../../../services/common.service';
import * as fileSaver from "file-saver";
import {HelptextService} from "../../../services/helptext.service";

@Component({
  selector: 'app-handle-exportfile',
  templateUrl: './handle-exportfile.component.html',
  styleUrls: ['./handle-exportfile.component.scss']
})
export class HandleExportfileComponent implements OnInit {

  displayedColumns = ['leverantör', 'avtalsnummer', 'giltigt from', 'giltigt tom', 'delete'];

  loading = false;
  organizationId: number;
  exportSettingId: number;
  exportSetting: ExportSetting;
  GPs: Array<GeneralPricelist> = new Array<GeneralPricelist>();
  dataSource = null;
  searchTotal;
  offset = 25;
  totalPages;
  currentPage = 1;
  paginatorArray = [];
  allPages;
  flexDirection: string;
  helpTexts;
  helpTextsLoaded = false;

  constructor(private exportService: ExportService,
              private route: ActivatedRoute,
              private dialog: MatDialog,
              private alertService: AlertService,
              private commonService: CommonService,
              private helpTextService: HelptextService) {
    this.commonService.isUsingIE() ? this.flexDirection = 'row' : this.flexDirection = 'column';
    this.route.params.subscribe(params => {
      this.organizationId = params.organizationId;
      this.exportSettingId = params.exportSettingId;
    });
  }

  ngOnInit() {
    this.getExportSettings();
    this.helpTexts = this.helpTextService.getTexts().subscribe(
      texts => {
        this.helpTexts = texts;
        this.helpTextsLoaded = true;
      }
    );
  }

  getExportSettings() {
    this.loading = true;
    this.exportService.getExportSettingForOrganization(this.organizationId, this.exportSettingId).subscribe(
      res => {
        this.exportSetting = res;
        this.GPs = res.generalPricelists;
        this.dataSource = new MatTableDataSource<GeneralPricelist>(this.GPs);
        this.loading = false;
      }, error => {
        this.loading = false;
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  exportGeneralPricelists() {
    this.exportService.exportGeneralPricelists(this.organizationId, this.exportSettingId).subscribe(
      data => {
        fileSaver.saveAs(data.body, data.headers.get('Content-Disposition').split(';')[1].trim().split('=')[1]);
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  openAddGPDialog() {
    const dialogRef = this.dialog.open(GpDialogComponent, {
      width: '90%',
      data: {'organizationId': this.organizationId, 'exportSettingsId': this.exportSettingId}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getExportSettings();
      }
    });
  }

  removeGP(gp) {
    const dialogRef = this.dialog.open(RemoveGpDialogComponent, {
      width: '40%',
      data: {'organizationId': this.organizationId, 'exportSettingId': this.exportSettingId, 'gp': gp}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getExportSettings();
      }
    });
  }

}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, Inject, OnInit} from '@angular/core';
import {AlertService} from '../../../../services/alert.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {UploadDialogComponent} from '../../../product/upload-dialog/upload-dialog.component';
import {ExportService} from '../../../../services/export.service';
import {GeneralPricelist} from '../../../../models/general-pricelist/general-pricelist.model';

@Component({
  selector: 'app-remove-gp-dialog',
  templateUrl: './remove-gp-dialog.component.html',
  styleUrls: ['./remove-gp-dialog.component.scss']
})
export class RemoveGpDialogComponent implements OnInit {
  saving = false;
  GPs: Array<GeneralPricelist>;

  constructor(public dialogRef: MatDialogRef<UploadDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private exportService: ExportService,
              private alertService: AlertService) {
  }

  ngOnInit() {
  }

  done() {
    this.saving = true;
    this.GPs = new Array<GeneralPricelist>();
    this.GPs.push(this.data.gp);
    this.exportService.removeGPFromExportSetting(this.data.organizationId, this.data.exportSettingId, this.GPs).subscribe(
      res => {
        this.alertService.clear();
        this.saving = false;
        this.alertService.success('Sparat');
        this.dialogRef.close(res);
      }, error => {
        this.saving = false;
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      });
  }
}

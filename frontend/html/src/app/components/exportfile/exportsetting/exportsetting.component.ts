/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, OnInit} from '@angular/core';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {ExportService} from '../../../services/export.service';
import {ExportSettingDialogComponent} from './export-setting-dialog/export-setting-dialog.component';
import {ExportSetting} from '../../../models/export-setting.model';
import {AlertService} from '../../../services/alert.service';
import {CommonService} from '../../../services/common.service';

@Component({
  selector: 'app-exportsetting',
  templateUrl: './exportsetting.component.html',
  styleUrls: ['./exportsetting.component.scss']
})
export class ExportsettingComponent implements OnInit {

  displayedColumns = ['kund', 'filnamn', 'verksamhetsområde', 'senast levererad', 'aktiv', 'edit', 'start'];

  loading = false;
  exportSettings = [];
  dataSource = null;
  searchTotal;
  offset = 25;
  totalPages;
  currentPage = 1;
  paginatorArray = [];
  allPages;
  flexDirection: string;

  constructor(private exportService: ExportService,
              private dialog: MatDialog,
              private alertService: AlertService,
              private commonService: CommonService) {
    this.commonService.isUsingIE() ? this.flexDirection = 'row' : this.flexDirection = 'column';
  }

  ngOnInit() {
    this.getExportSettings();
  }

  getExportSettings() {
    this.loading = true;
    this.exportService.getExportSettings().subscribe(
      res => {
        this.exportSettings = res;
        this.dataSource = new MatTableDataSource<ExportSetting>(this.exportSettings);
        this.loading = false;
      }, error => {
        this.alertService.clear();
        this.loading = false;
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  editExportSettings(element) {
    const dialogRef = this.dialog.open(ExportSettingDialogComponent, {
      width: '90%',
      data: {
        'exportSettings': element
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getExportSettings();
      }
    });
  }

  openAddExportSettingDialog() {
    const dialogRef = this.dialog.open(ExportSettingDialogComponent, {
      width: '90%'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getExportSettings();
      }
    });
  }
  startExport(element)
  {
    this.exportService.queueExport(element).subscribe(
      res => {
        this.alertService.success('Lagt till ' +res.organization.organizationName +  ' (' + res.filename + ') i exportkön och kommer att köras inom 30 min');
      }, error => {

        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );

  }
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {LoginService} from '../../../../services/login.service';
import {Login} from '../../../../models/login.model';
import {AlertService} from '../../../../services/alert.service';

@Component({
  selector: 'app-create-password',
  templateUrl: './create-password.component.html',
  styleUrls: ['./create-password.component.scss']
})
export class CreatePasswordComponent implements OnInit {
  loaded = false;
  password: string;
  confirmPassword: string;
  token: string;
  login: Login = new Login();
  isValidActivationLink: boolean;

  constructor(private route: ActivatedRoute,
              private loginService: LoginService,
              private router: Router,
              private alertService: AlertService) {
    this.route.queryParams.subscribe(params => {
      this.login.email = params['email'];
      this.login.token = params['token'];
    });
  }

  ngOnInit() {
    this.loginService.validate(this.login).subscribe(
      res => {
        this.isValidActivationLink = true;
        this.loaded = true;
      }, err => {
        if (err.status === 403) {
          this.isValidActivationLink = false;
          this.loaded = true;
        }
      }
    );
  }

  changePassword() {
    if (this.password === this.confirmPassword) {
      this.login.newPassword = this.password;
      this.loginService.changePassword(this.login).subscribe(
        res => {
          this.router.navigate(['/'], {queryParams: {passwordCreated: true}});
        }, error => {
          this.alertService.clear();
          error.error.errors.forEach(err => {
            this.alertService.error(err);
          });
        }
      );
    } else {
      this.alertService.clear();
      this.alertService.errorWithMessage('Lösenord och upprepat lösenord överrensstämmer inte');
    }
  }

}

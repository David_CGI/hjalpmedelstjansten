/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, OnInit} from '@angular/core';
import {LoginService} from '../../../services/login.service';
import {Login} from '../../../models/login.model';
import {AlertService} from '../../../services/alert.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-recover',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.scss']
})
export class RecoverComponent implements OnInit {

  username: string;
  email: string;
  userData: Login = new Login();

  constructor(private loginService: LoginService,
              private alertService: AlertService,
              private router: Router) {
  }

  ngOnInit() {
  }

  sendRecoverLink() {
    this.userData.username = this.username;
    this.userData.email = this.email;
    this.loginService.requestPassword(this.userData).subscribe(
      data => {
        this.router.navigate(['/'], {queryParams: {passwordRequested: true}});
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
            this.alertService.error(err);
          }
        );
      }
    );
  }

}


/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {AgreementService} from '../../../services/agreement.service';
import {GeneralPricelist} from '../../../models/general-pricelist/general-pricelist.model';
import {AlertService} from '../../../services/alert.service';
import {Subject} from 'rxjs/Subject';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {HelptextService} from '../../../services/helptext.service';

@Component({
  selector: 'app-view-general-pricelist',
  templateUrl: './view-general-pricelist.component.html',
  styleUrls: ['./view-general-pricelist.component.scss']
})
export class ViewGeneralPricelistComponent implements OnInit {
  displayedColumns = ['avtalsnummer', 'avtalsnamn', 'leverantör', 'giltigt from', 'giltigt tom', 'anchor'];

  loading = false;
  query = '';
  tmpQuery = '';
  searchChanged: Subject<string> = new Subject<string>();
  queryParams = '';
  searchTotal;
  offset = 25;
  totalPages;
  currentPage = 1;
  paginatorArray = [];
  allPages;
  dataSource = null;
  generalPricelists = [];
  helpTexts;
  helpTextsLoaded = false;

  // Filters
  filterCurrent = false;
  filterFuture = false;
  filterDiscontinued = false;

  constructor(private agreementService: AgreementService,
              private alertService: AlertService,
              private helpTextService: HelptextService) {
    // The user is considered done with their search after 500 ms without further input
    this.searchChanged.pipe(
      debounceTime(500),
      distinctUntilChanged())
      .subscribe(query => {
          this.query = encodeURIComponent(query);
          this.onQueryChange();
        }
      );
  }

  ngOnInit() {
    this.helpTexts = this.helpTextService.getTexts().subscribe(
      texts => {
        this.helpTexts = texts;
        this.helpTextsLoaded = true;
      }
    );
    this.searchGeneralPricelists('');
  }

  onQueryChanged(text: string) {
    this.searchChanged.next(text);
  }

  onQueryChange() {
    this.onFilterChange();
  }

  onFilterChange() {
    this.queryParams = '';
    this.currentPage = 1;
    this.filterCurrent ? this.queryParams += '&status=CURRENT' : this.queryParams = this.queryParams;
    this.filterFuture ? this.queryParams += '&status=FUTURE' : this.queryParams = this.queryParams;
    this.filterDiscontinued ? this.queryParams += '&status=DISCONTINUED' : this.queryParams = this.queryParams;
    this.searchGeneralPricelists(this.query + this.queryParams);
  }

  searchGeneralPricelists(query) {
    this.loading = true;
    this.agreementService.searchGeneralPricelists(query).subscribe(
      res => {
        this.searchTotal = res.headers.get('X-Total-Count');
        this.searchTotal !== '0' ? this.generalPricelists = res.body : this.generalPricelists = [];
        this.dataSource = new MatTableDataSource<GeneralPricelist>(this.generalPricelists);
        this.totalPages = Math.ceil(this.searchTotal / this.offset);
        this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
        this.generatePaginator();
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  goToPage(page) {
    if (page !== '...' && page !== this.currentPage) {
      this.loading = true;
      this.currentPage = page;
      this.agreementService.searchGeneralPricelists(
        this.query + this.queryParams + '&offset=' + (this.currentPage - 1) * this.offset).subscribe(
        res => {
          this.generalPricelists = res.body;
          this.dataSource = new MatTableDataSource<GeneralPricelist>(this.generalPricelists);
          this.currentPage = page;
          this.generatePaginator();
        }, error => {
          this.alertService.clear();
          error.error.errors.forEach(err => {
            this.alertService.error(err);
          });
        }
      );
    }
  }

  generatePaginator() {
    let hasFirstEllipsis = false;
    let hasSecondEllipsis = false;
    this.paginatorArray = [];
    if (this.searchTotal > this.offset) {
      if (this.totalPages < 6) {
        this.paginatorArray = this.allPages;
      } else {
        this.allPages.forEach((pageNumber) => {
          if (pageNumber === 1
            || pageNumber === this.currentPage
            || pageNumber === this.totalPages
            || pageNumber === this.currentPage + 1
            || pageNumber === this.currentPage - 1) {
            this.paginatorArray.push(pageNumber);
          } else if (!hasFirstEllipsis && pageNumber < this.currentPage && (pageNumber !== this.currentPage - 1)) {
            this.paginatorArray.push('...');
            hasFirstEllipsis = true;
          } else if (!hasSecondEllipsis && pageNumber > this.currentPage && pageNumber !== this.currentPage + 1) {
            this.paginatorArray.push('...');
            hasSecondEllipsis = true;
          }
        });
      }
    }
    this.loading = false;
  }

}

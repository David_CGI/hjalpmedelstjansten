/*
* Copyright (C) 2019 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ProductService} from '../../../services/product.service';
import {OrganizationService} from '../../../services/organization.service';
import {AgreementService} from '../../../services/agreement.service';
import {AlertService} from '../../../services/alert.service';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {GuaranteeUnit} from '../../../models/agreement/guarantee-unit.model';
import {AuthService} from '../../../auth/auth.service';
import {ActivatedRoute} from '@angular/router';
import {GeneralPricelistPricelist} from '../../../models/general-pricelist/general-pricelist-pricelist.model';
import {GeneralPricelistPricelistrow} from '../../../models/general-pricelist/general-pricelist-pricelistrow.model';
import {Article} from '../../../models/product/article.model';
import {PreventiveMaintenance} from '../../../models/preventive-maintenance.model';
import {CommonService} from '../../../services/common.service';
import {ImportGeneralPricelistPricelistDialogComponent} from './import-general-pricelist-pricelist-dialog/import-general-pricelist-pricelist-dialog.component';
import {AddGeneralPricelistPricelistRowDialogComponent} from './add-general-pricelist-pricelist-row-dialog/add-general-pricelist-pricelist-row-dialog.component';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {Subject} from 'rxjs/Subject';
import * as fileSaver from "file-saver";
import {HelptextService} from '../../../services/helptext.service';

@Component({
  selector: 'app-handle-general-pricelist-pricelist',
  templateUrl: './handle-general-pricelist-pricelist.component.html',
  styleUrls: ['./handle-general-pricelist-pricelist.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HandleGeneralPricelistPricelistComponent implements OnInit {

  displayedColumns = ['typ', 'artnr', 'benämning', 'kategori', 'beställningsenhet',
    'utgått', 'pris', 'gäller från', 'min best', 'levtid', 'garanti(antal)', '(enhet)',
    '(gäller från)', '(villkor)', 'status', 'inactivate', 'anchor'];
  loaded = false;
  loading = false;
  editMode = false;
  organizationId: number;
  token;
  ownOrganizationId: number;
  generalPricelistId: number;
  pricelistId: number;
  isCustomer: boolean;
  isServiceOwner: boolean;
  units: Array<GuaranteeUnit>;
  query = '';
  tmpQuery = '';
  searchChanged: Subject<string> = new Subject<string>();
  queryParams = '';
  searchTotal;
  offset = 25;
  totalPages;
  currentPage = 1;
  paginatorArray = [];
  allPages;
  dataSource = null;
  pricelist: GeneralPricelistPricelist;
  pricelistRows = [];
  pricelistValidFrom: Date;
  warrantyValidFrom: Array<PreventiveMaintenance>;
  flexDirection: string;
  helpTexts;
  helpTextsLoaded = false;

  // Filters
  filterActive = false;
  filterDiscontinued = false;
  filterShowRowsWithPrices = false;
  filterShowRowsWithoutPrices = false;
  filterH = false;
  filterT = false;
  filterR = false;
  filterTJ = false;
  filterI = false;

  // Permissions
  hasPermissionToViewGeneralPricelistPricelist = this.hasPermission('generalpricelist_pricelist:view_own');
  hasPermissionToUpdateGeneralPricelistPricelistRow = this.hasPermission('generalpricelist_pricelistrow:update_own');
  hasPermissionToInactivateGeneralPricelistPricelistRow = this.hasPermission('generalpricelist_pricelistrow:inactivate_own');
  hasPermissionToExportGeneralPricelistPricelistRows = this.hasPermission('generalpricelistpricelistexport:view');
  hasPermissionToImportGeneralPricelistPricelistRows = this.hasPermission('generalpricelistpricelistimport:view');
  isOwnOrganization = false;

  constructor(private route: ActivatedRoute,
              private authService: AuthService,
              private productService: ProductService,
              private commonService: CommonService,
              private agreementService: AgreementService,
              private organizationService: OrganizationService,
              private alertService: AlertService,
              private dialog: MatDialog,
              private helpTextService: HelptextService) {
    this.commonService.isUsingIE() ? this.flexDirection = 'row' : this.flexDirection = 'column';
    this.route.params.subscribe(params => {
      this.organizationId = params.organizationId;
      this.token = JSON.parse(localStorage.getItem('token'));

      this.ownOrganizationId = this.token.userEngagements[0].organizationId;

      this.generalPricelistId = params.id;
      this.pricelistId = params.pricelistId;
    });
    Number(this.organizationId) === this.authService.getOrganizationId() ? this.isOwnOrganization = true : this.isOwnOrganization = false;
    this.hasPermissionToViewGeneralPricelistPricelist && this.isOwnOrganization ?
      this.hasPermissionToViewGeneralPricelistPricelist = true : this.hasPermissionToViewGeneralPricelistPricelist = false;
    this.hasPermissionToUpdateGeneralPricelistPricelistRow && this.isOwnOrganization ?
      this.hasPermissionToUpdateGeneralPricelistPricelistRow = true : this.hasPermissionToUpdateGeneralPricelistPricelistRow = false;
    this.hasPermissionToInactivateGeneralPricelistPricelistRow && this.isOwnOrganization ?
      this.hasPermissionToInactivateGeneralPricelistPricelistRow = true :
      this.hasPermissionToInactivateGeneralPricelistPricelistRow = false;

    // The user is considered done with their search after 500 ms without further input
    this.searchChanged.pipe(
      debounceTime(500),
      distinctUntilChanged())
      .subscribe(query => {
          this.query = encodeURIComponent(query);
          this.onQueryChange();
        }
      );
  }

  ngOnInit() {
    this.helpTexts = this.helpTextService.getTexts().subscribe(
      texts => {
        this.helpTexts = texts;
        this.helpTextsLoaded = true;
      }
    );
    this.organizationService.getOrganization(this.authService.getOrganizationId()).subscribe(
      res => {
        res.organizationType === 'CUSTOMER' ? this.isCustomer = true : this.isCustomer = false;
        res.organizationType === 'SERVICE_OWNER' ? this.isServiceOwner = true : this.isServiceOwner = false;
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
    this.productService.getGuaranteeUnits().subscribe(
      units => {
        this.units = units;
        this.commonService.getPreventiveMaintenances().subscribe(
          preventiveMaintenances => {
            this.warrantyValidFrom = preventiveMaintenances;
            this.agreementService.getGeneralPricelistPriceList(this.organizationId, this.pricelistId).subscribe(
              pricelist => {
                this.pricelist = pricelist;
                this.convertToDateString();
                this.getPricelistRows('');
              }, error => {
                this.alertService.clear();
                error.error.errors.forEach(err => {
                  this.alertService.error(err);
                });
              }
            );
          }
        );
      }
    );
  }

  toggleEditMode() {
    this.editMode = !this.editMode;

    if (!this.editMode) {
      this.displayedColumns.push('anchor');
      this.getPricelistRows(this.query + this.queryParams + '&offset=' + (this.currentPage - 1) * this.offset);
    } else {
      this.displayedColumns.splice(-1, 1);
    }
  }

  exportSearchResults() {
    const query = '?query=' + this.query + this.queryParams + '&ownOrganizationUniqueId=' + this.ownOrganizationId;

    this.productService.exportGeneralPricelistPricelistRows(this.organizationId, this.pricelistId, query).subscribe(
      data => {
        fileSaver.saveAs(data.body, data.headers.get('Content-Disposition').split(';')[1].trim().split('=')[1]);
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  onQueryChanged(text: string) {
    this.searchChanged.next(text);
  }

  onQueryChange() {
    this.onFilterChange();
  }

  onFilterChange() {
    this.queryParams = '';
    this.currentPage = 1;
    this.filterActive ? this.queryParams += '&status=ACTIVE' : this.queryParams = this.queryParams;
    this.filterDiscontinued ? this.queryParams += '&status=INACTIVE' : this.queryParams = this.queryParams;
    this.filterShowRowsWithPrices ? this.queryParams += '&showRowsWithPrices=true' : this.queryParams += '&showRowsWithPrices=false';
    this.filterShowRowsWithoutPrices ? this.queryParams += '&showRowsWithoutPrices=true'
      : this.queryParams += '&showRowsWithoutPrices=false';
    this.filterH ? this.queryParams += '&type=H' : this.queryParams = this.queryParams;
    this.filterT ? this.queryParams += '&type=T' : this.queryParams = this.queryParams;
    this.filterR ? this.queryParams += '&type=R' : this.queryParams = this.queryParams;
    this.filterTJ ? this.queryParams += '&type=Tj' : this.queryParams = this.queryParams;
    this.filterI ? this.queryParams += '&type=I' : this.queryParams = this.queryParams;
    this.getPricelistRows(this.query + this.queryParams);
  }

  exportPricelistOnlyDiscontinued() {
    window.location.href = this.agreementService.getExportGeneralPricelistPricelistUrl(
      this.organizationId, this.pricelistId, 'discontinued');
  }

  exportPricelistAll() {
    window.location.href = this.agreementService.getExportGeneralPricelistPricelistUrl(
      this.organizationId, this.pricelistId, 'all');
  }

  exportPricelistOnlyInProduction() {
    window.location.href = this.agreementService.getExportGeneralPricelistPricelistUrl(
      this.organizationId, this.pricelistId, 'inProduction');
  }

  openImportPricelistDialog() {
    const dialogRef = this.dialog.open(ImportGeneralPricelistPricelistDialogComponent, {
      width: '80%',
      data: {
        'organizationId': this.organizationId,
        'pricelistId': this.pricelistId
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getPricelistRows(this.query + this.queryParams);
      }
    });
  }

  /* exportPricelistRows() {
     window.location.href = this.agreementService.getExportPricelistRowsUrl(this.organizationId, this.agreementId, this.pricelistId) + '?query=' + this.query + this.queryParams;
   }*/

  getPricelistRows(query) {
    this.loading = true;
    this.agreementService.getGeneralPricelistPricelistRows(this.organizationId, this.pricelistId, query).subscribe(
      res => {
        this.searchTotal = res.headers.get('X-Total-Count');
        this.searchTotal !== '0' ? this.pricelistRows = res.body : this.pricelistRows = [];
        this.totalPages = Math.ceil(this.searchTotal / this.offset);
        this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
        // Extract units
        this.pricelistRows.forEach((row) => {
          if (row.warrantyQuantityUnit) {
            row.warrantyQuantityUnit = this.units.find(x => x.id === row.warrantyQuantityUnit.id);
          } else {
            row.warrantyQuantityUnit = new GuaranteeUnit();
          }
          if (row.warrantyValidFrom) {
            row.warrantyValidFrom = this.warrantyValidFrom.find(x => x.id === row.warrantyValidFrom.id);
          } else {
            row.warrantyValidFrom = new PreventiveMaintenance();
          }
          if (!row.warrantyTerms) {
            row.warrantyTerms = '';
          }

          row.validFrom = new Date(row.validFrom);
        });
        this.dataSource = new MatTableDataSource<GeneralPricelistPricelistrow>(this.pricelistRows);
        this.generatePaginator();
        this.loaded = true;
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  hasPermission(permission): boolean {
    if (!localStorage.getItem('token')) {
      return false;
    }
    return this.authService.hasPermission(permission);
  }

  goToPage(page) {
    if (page !== '...' && page !== this.currentPage) {
      this.loading = true;
      this.currentPage = page;
      this.agreementService.getGeneralPricelistPricelistRows(
        this.organizationId, this.pricelistId,
        this.query + this.queryParams + '&offset=' + (this.currentPage - 1) * this.offset)
        .subscribe(
          res => {
            this.pricelistRows = res.body;
            // Extract units dates and selections
            this.pricelistRows.forEach((row) => {
              if (row.warrantyQuantityUnit) {
                row.warrantyQuantityUnit = this.units.find(x => x.id === row.warrantyQuantityUnit.id);
              } else {
                row.warrantyQuantityUnit = new GuaranteeUnit();
              }
              if (row.warrantyValidFrom) {
                row.warrantyValidFrom = this.warrantyValidFrom.find(x => x.id === row.warrantyValidFrom.id);
              } else {
                row.warrantyValidFrom = new PreventiveMaintenance();
              }
              if (!row.warrantyTerms) {
                row.warrantyTerms = '';
              }
              row.validFrom = new Date(row.validFrom);
            });
            this.dataSource = new MatTableDataSource<GeneralPricelistPricelistrow>(this.pricelistRows);
            this.currentPage = page;
            this.generatePaginator();
          }, error => {
            this.alertService.clear();
            error.error.errors.forEach(err => {
              this.alertService.error(err);
            });
          }
        );
    }
  }

  generatePaginator() {
    let hasFirstEllipsis = false;
    let hasSecondEllipsis = false;
    this.paginatorArray = [];
    if (this.searchTotal > this.offset) {
      if (this.totalPages < 6) {
        this.paginatorArray = this.allPages;
      } else {
        this.allPages.forEach((pageNumber) => {
          if (pageNumber === 1
            || pageNumber === this.currentPage
            || pageNumber === this.totalPages
            || pageNumber === this.currentPage + 1
            || pageNumber === this.currentPage - 1) {
            this.paginatorArray.push(pageNumber);
          } else if (!hasFirstEllipsis && pageNumber < this.currentPage && (pageNumber !== this.currentPage - 1)) {
            this.paginatorArray.push('...');
            hasFirstEllipsis = true;
          } else if (!hasSecondEllipsis && pageNumber > this.currentPage && pageNumber !== this.currentPage + 1) {
            this.paginatorArray.push('...');
            hasSecondEllipsis = true;
          }
        });
      }
    }
    this.loading = false;
  }

  convertToDateString() {
    this.pricelistValidFrom = new Date(this.pricelist.validFrom);
  }

  getMillisecondsFromDate(date) {
    if (date) {
      return date.getTime();
    }
    return null;
  }

  openAddPricelistRowDialog() {
    const dialogRef = this.dialog.open(AddGeneralPricelistPricelistRowDialogComponent, {
      width: '80%',
      data: {
        'organizationId': this.organizationId,
        'pricelistId': this.pricelistId
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const pricelistRows = new Array<GeneralPricelistPricelistrow>();
        result.forEach(article => {
          const tmpArticle = new Article();
          const pricelistRow = new GeneralPricelistPricelistrow();
          pricelistRow.status = 'ACTIVE';
          pricelistRow.leastOrderQuantity = 1;
          tmpArticle.id = article.id;
          pricelistRow.article = tmpArticle;
          pricelistRows.push(pricelistRow);
        });
        this.agreementService.createGeneralPricelistPricelistRows(this.organizationId, this.pricelistId, pricelistRows).subscribe(
          res => {
            this.onFilterChange();
            this.alertService.success('Sparat');
          }, error => {
            this.alertService.clear();
            error.error.errors.forEach(err => {
              this.alertService.error(err);
            });
          }
        );
      }
    });
  }

  updatePricelistRow(row) {
    if (row.validFrom) {
      row.validFrom = this.getMillisecondsFromDate(row.validFrom);
    }
    if (row.warrantyValidFrom && !row.warrantyValidFrom.id) {
      row.warrantyValidFrom = null;
    }
    this.agreementService.updateGeneralPricelistPricelistRow(this.organizationId, this.pricelistId, row.id, row).subscribe(
      res => {
        if (row.validFrom) {
          row.validFrom = new Date(row.validFrom);
        }
        this.alertService.clear();
        this.alertService.success('Sparat');
      }, error => {
        if (row.validFrom) {
          row.validFrom = new Date(row.validFrom);
        }
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  inactivatePrice(row) {

    this.agreementService.deleteGeneralPricelistPriceListRow(this.organizationId, this.pricelistId, row.id).subscribe(
      res => {
        this.alertService.clear();
        this.getPricelistRows(this.query + this.queryParams);
        this.alertService.success('Tog bort artikeln \"' + row.article.articleName + '\" från generell prislista');
      }, error => {
        if (row.validFrom) {
          row.validFrom = new Date(row.validFrom);
        }
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );

  }
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, Inject, OnInit} from '@angular/core';
import {Article} from '../../../../models/product/article.model';
import {AuthService} from '../../../../auth/auth.service';
import {SelectionModel} from '@angular/cdk/collections';
import {HelptextService} from '../../../../services/helptext.service';
import {MAT_DIALOG_DATA, MatDialogRef, MatTableDataSource} from '@angular/material';
import {AgreementService} from '../../../../services/agreement.service';
import {ActivatedRoute} from '@angular/router';
import {AlertService} from '../../../../services/alert.service';
import {Subject} from 'rxjs/Subject';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';
import {CommonService} from '../../../../services/common.service';

@Component({
  selector: 'app-add-general-pricelist-pricelist-row-dialog',
  templateUrl: './add-general-pricelist-pricelist-row-dialog.component.html',
  styleUrls: ['./add-general-pricelist-pricelist-row-dialog.component.scss']
})
export class AddGeneralPricelistPricelistRowDialogComponent implements OnInit {
  displayedColumns = ['checkbox', 'typ', 'benämning', 'artikelnummer', 'status', 'kategori'];

  loading = true;
  organizationId: number;
  pricelistId: number;
  query = '';
  tmpQuery = '';
  searchChanged: Subject<string> = new Subject<string>();
  queryParams = '';
  searchTotal;
  offset = 25;
  totalPages;
  currentPage = 1;
  paginatorArray = [];
  allPages;
  dataSource = null;
  articles = [];
  selection = new SelectionModel<Article>(true, []);
  pageSelection = new SelectionModel<Article>(true, []);
  helpTexts;
  flexDirection;

  // Filters
  filterH = false;
  filterT = false;
  filterR = false;
  filterTJ = false;
  filterI = false;
  filterPublished = false;
  filterDiscontinued = false;

  constructor(public dialogRef: MatDialogRef<AddGeneralPricelistPricelistRowDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private route: ActivatedRoute,
              private authService: AuthService,
              private agreementService: AgreementService,
              private alertService: AlertService,
              private helpTextService: HelptextService,
              private commonService: CommonService) {
    this.commonService.isUsingIE() ? this.flexDirection = 'row' : this.flexDirection = 'column';
    this.organizationId = data.organizationId;
    this.pricelistId = data.pricelistId;

    // The user is considered done with their search after 500 ms without further input
    this.searchChanged.pipe(
      debounceTime(500),
      distinctUntilChanged())
      .subscribe(query => {
          this.query = encodeURIComponent(query);
          this.onQueryChange();
        }
      );
  }

  ngOnInit() {
    this.helpTexts = this.helpTextService.getTexts().subscribe(
      texts => {
        this.helpTexts = texts;
      }
    );
    this.searchArticles('');
  }

  onQueryChanged(text: string) {
    this.searchChanged.next(text);
  }

  onQueryChange() {
    this.onFilterChange();
  }

  onFilterChange() {
    this.queryParams = '';
    this.currentPage = 1;
    this.filterH ? this.queryParams += '&type=H' : this.queryParams = this.queryParams;
    this.filterT ? this.queryParams += '&type=T' : this.queryParams = this.queryParams;
    this.filterR ? this.queryParams += '&type=R' : this.queryParams = this.queryParams;
    this.filterTJ ? this.queryParams += '&type=Tj' : this.queryParams = this.queryParams;
    this.filterI ? this.queryParams += '&type=I' : this.queryParams = this.queryParams;
    this.filterPublished ? this.queryParams += '&status=PUBLISHED' : this.queryParams = this.queryParams;
    this.filterDiscontinued ? this.queryParams += '&status=DISCONTINUED' : this.queryParams = this.queryParams;
    this.searchArticles(this.query + this.queryParams);
    this.deSelectAll();
  }

  searchArticles(query) {
    this.loading = true;
    this.agreementService.searchArticlesForGeneralPricelistPricelist(query, this.organizationId, this.pricelistId).subscribe(
      res => {
        this.searchTotal = res.headers.get('X-Total-Count');
        this.searchTotal !== '0' ? this.articles = res.body : this.articles = [];
        this.totalPages = Math.ceil(this.searchTotal / this.offset);
        this.allPages = Array(this.totalPages).fill(1).map((x, i) => i + 1);
        this.dataSource = new MatTableDataSource<Article>(this.articles);
        this.generatePaginator();
      }, error => {
        this.alertService.clear();
        error.error.errors.forEach(err => {
          this.alertService.error(err);
        });
      }
    );
  }

  hasPermission(permission): boolean {
    if (!localStorage.getItem('token')) {
      return false;
    }
    return this.authService.hasPermission(permission);
  }

  goToPage(page) {
    if (page !== '...' && page !== this.currentPage) {
      this.loading = true;
      this.currentPage = page;
      this.pageSelection.clear();
      this.agreementService.searchArticlesForGeneralPricelistPricelist(
        this.query + this.queryParams + '&offset=' +
        (this.currentPage - 1) * this.offset, this.organizationId, this.pricelistId)
        .subscribe(
          res => {
            this.articles = res.body;
            this.dataSource = new MatTableDataSource<Article>(this.articles);
            this.dataSource.data.forEach(row => {
              if (this.selection.selected.some(x => x.id === row.id)) {
                this.pageSelection.select(row);
              }
            });
            this.currentPage = page;
            this.generatePaginator();
          }, error => {
            this.alertService.clear();
            error.error.errors.forEach(err => {
              this.alertService.error(err);
            });
          }
        );
    }
  }

  generatePaginator() {
    let hasFirstEllipsis = false;
    let hasSecondEllipsis = false;
    this.paginatorArray = [];
    if (this.searchTotal > this.offset) {
      if (this.totalPages < 6) {
        this.paginatorArray = this.allPages;
      } else {
        this.allPages.forEach((pageNumber) => {
          if (pageNumber === 1
            || pageNumber === this.currentPage
            || pageNumber === this.totalPages
            || pageNumber === this.currentPage + 1
            || pageNumber === this.currentPage - 1) {
            this.paginatorArray.push(pageNumber);
          } else if (!hasFirstEllipsis && pageNumber < this.currentPage && (pageNumber !== this.currentPage - 1)) {
            this.paginatorArray.push('...');
            hasFirstEllipsis = true;
          } else if (!hasSecondEllipsis && pageNumber > this.currentPage && pageNumber !== this.currentPage + 1) {
            this.paginatorArray.push('...');
            hasSecondEllipsis = true;
          }
        });
      }
    }
    this.loading = false;
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.pageSelection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    if (this.isAllSelected()) { //all is selected => clear all
      this.pageSelection.clear();
      this.dataSource.data.forEach(row => {
        this.selection.selected.forEach(selectedRow => {
          if (selectedRow.id === row.id) {
            this.selection.deselect(selectedRow);
          }
        });
      });
    } else {  //all is NOT selected => select ALL
      this.dataSource.data.forEach(row => this.pageSelection.select(row));
      this.dataSource.data.forEach(row => {
        if (!this.selection.selected.some(x => x.id === row.id)) {
          this.selection.select(row);
        }
      });
    }
  }

  deSelectAll() {
    this.pageSelection.clear();
    debugger;
    this.dataSource.data.forEach(row => {
      this.selection.selected.forEach(selectedRow => {
        if (selectedRow.id === row.id) {
          this.selection.deselect(selectedRow);
        }
      });
    });
  }

  toggleSelections(element) {
    this.selection.toggle(element);
    this.pageSelection.toggle(element);
  }

  done() {
    this.dialogRef.close(this.selection.selected);
  }
}

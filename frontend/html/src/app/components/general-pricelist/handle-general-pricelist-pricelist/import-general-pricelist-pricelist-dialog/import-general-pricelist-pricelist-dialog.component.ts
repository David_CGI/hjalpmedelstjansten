/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, Inject, OnInit} from '@angular/core';
import {UploadDialogComponent} from '../../../product/upload-dialog/upload-dialog.component';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ErrorMessage} from '../../../../models/error-message.model';
import {AgreementService} from '../../../../services/agreement.service';
import {AlertService} from '../../../../services/alert.service';

@Component({
  selector: 'app-import-general-pricelist-pricelist-dialog',
  templateUrl: './import-general-pricelist-pricelist-dialog.component.html',
  styleUrls: ['./import-general-pricelist-pricelist-dialog.component.scss']
})
export class ImportGeneralPricelistPricelistDialogComponent implements OnInit {

  file;
  formData: FormData;
  errors: Array<ErrorMessage>;
  importInProgress = false;
  showImportText = false;

  constructor(public dialogRef: MatDialogRef<UploadDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private agreementService: AgreementService,
              private alertService: AlertService) {
  }

  ngOnInit() {
  }

  fileSelected(file) {
    this.file = file;
  }

  import() {
    this.importInProgress = true;
    this.showImportText = false;
    this.formData = new FormData();
    this.formData.append('file', this.file);
    this.errors = new Array<ErrorMessage>();

    this.agreementService.importGeneralPricelistPricelist(this.data.organizationId, this.data.pricelistId, this.formData).subscribe(
      res => {
        this.importInProgress = false;
        this.showImportText = true;
        this.file = null;
        this.alertService.clear();
        this.alertService.success('Filen har lästs in. Resultatet kommer att skickas till din e-post');
      }, error => {
        this.importInProgress = false;
        this.alertService.clear();
        this.errors = new Array<ErrorMessage>();
        this.alertService.errorWithMessage('Någonting gick fel');
        error.error.errors.forEach(err => {
          this.errors.push(err);
        });
      });
  }

}

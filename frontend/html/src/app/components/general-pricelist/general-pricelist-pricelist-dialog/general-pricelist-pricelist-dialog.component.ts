/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ActivatedRoute} from '@angular/router';
import {FormControl, Validators} from '@angular/forms';
import {AgreementService} from '../../../services/agreement.service';
import {AlertService} from '../../../services/alert.service';
import {GeneralPricelistPricelist} from '../../../models/general-pricelist/general-pricelist-pricelist.model';

@Component({
  selector: 'app-general-pricelist-pricelist-dialog',
  templateUrl: './general-pricelist-pricelist-dialog.component.html',
  styleUrls: ['./general-pricelist-pricelist-dialog.component.scss']
})
export class GeneralPricelistPricelistDialogComponent implements OnInit {
  saving = false;
  organizationId: number;
  agreementId: number;
  pricelist: GeneralPricelistPricelist = new GeneralPricelistPricelist();
  pricelistId: number;

  // Form controls
  numberFormControl = new FormControl(null, {
    updateOn: 'blur'
  });
  validFromFormControl = new FormControl(null, {
    updateOn: 'blur'
  });

  // Validation errors
  numberError;
  validFromError;

  constructor(public dialogRef: MatDialogRef<GeneralPricelistPricelistDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private route: ActivatedRoute,
              private agreementService: AgreementService,
              private alertService: AlertService) {
    this.organizationId = data.organizationId;
    this.numberFormControl.setValue(data.pricelistNumber);
    this.validFromFormControl.setValue(new Date(data.validFrom));
    this.pricelistId = data.pricelistId;
  }

  ngOnInit() {
  }

  getMillisecondsFromDate(date): number {
    if (date) {
      return date.getTime();
    }
    return null;
  }

  handleServerValidation(error): void {
    switch (error.field) {
      case 'number': {
        this.numberError = error.message;
        this.numberFormControl.setErrors(Validators.pattern(''));
        break;
      }
      case 'validFrom': {
        this.validFromError = error.message;
        this.validFromFormControl.setErrors(Validators.pattern(''));
        break;
      }
    }
  }

  resetValidation() {
    this.validFromError = null;
    this.validFromFormControl.markAsDirty();
  }

  done() {
    this.saving = true;
    this.resetValidation();
    this.pricelist.validFrom = this.getMillisecondsFromDate(this.validFromFormControl.value);
    this.pricelist.number = this.numberFormControl.value;

    if (!this.pricelistId) {
      this.agreementService.createGeneralPricelistPricelist(this.organizationId, this.pricelist).subscribe(
        res => {
          this.saving = false;
          this.alertService.success('Sparat');
          this.dialogRef.close('success');
        }, error => {
          this.saving = false;
          this.alertService.clear();
          error.error.errors.forEach(err => {
            this.handleServerValidation(err);
            this.alertService.error(err);
          });
        }
      );
    } else {
      this.agreementService.updateGeneralPricelistPricelist(this.organizationId, this.pricelistId, this.pricelist).subscribe(
        res => {
          this.saving = false;
          this.alertService.success('Sparat');
          this.dialogRef.close('success');
        }, error => {
          this.saving = false;
          this.alertService.clear();
          error.error.errors.forEach(err => {
            this.handleServerValidation(err);
            this.alertService.error(err);
          });
        }
      );
    }
  }

}

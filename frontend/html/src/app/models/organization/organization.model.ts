/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Country} from '../country.model';
import {ElectronicAddress} from '../electronic-address.model';
import {PostAddress} from '../post-address.model';

export class Organization {
  id: number;
  active: boolean;
  organizationName: string;
  organizationType: string;
  gln: number;
  organizationNumber: string;
  validFrom: number;
  validTo: number;
  country: Country;
  electronicAddress: ElectronicAddress;
  postAddresses: Array<PostAddress>;
}

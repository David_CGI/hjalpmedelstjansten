/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Category} from './category.model';
import {Product} from './product.model';
import {Ce} from './ce.model';
import {ElectronicAddress} from '../electronic-address.model';
import {OrderUnit} from './order-unit.model';
import {ArticleCategoryProperty} from './article-category-property.model';
import {PreventiveMaintenance} from '../preventive-maintenance.model';

export class Article {
  id: number;
  articleName: string;
  articleNumber: string;
  status: string;
  category: Category;
  extendedCategories: Array<Category>;
  basedOnProduct: Product;
  fitsToProducts: Array<any>;
  fitsToArticles: Array<any>;
  gtin: string;
  supplementedInformation: string;
  ceMarked: boolean;
  customerUnique: boolean;
  ceDirective: Ce;
  ceStandard: Ce;
  preventiveMaintenanceNumberOfDays: number;
  preventiveMaintenanceValidFrom: PreventiveMaintenance;
  manufacturer: string;
  manufacturerArticleNumber: string;
  trademark: string;
  manufacturerElectronicAddress: ElectronicAddress;
  orderUnit: OrderUnit;
  articleQuantityInOuterPackage: number;
  articleQuantityInOuterPackageUnit: OrderUnit;
  packageContent: number;
  packageContentUnit: OrderUnit;
  packageLevelBase: number;
  packageLevelMiddle: number;
  packageLevelTop: number;
  replacedByArticles: Array<Article>;
  replacementDate: number;
  type: string;
  categoryPropertys: Array<ArticleCategoryProperty>;
  color: string;
  organizationId: number;
  organizationName: string;
  numberEditable: boolean;
  inactivateRowsOnReplacement: boolean;
}

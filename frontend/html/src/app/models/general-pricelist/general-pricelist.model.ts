/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {OrderUnit} from '../product/order-unit.model';
import {Organization} from '../organization/organization.model';
import {GeneralPricelistPricelist} from './general-pricelist-pricelist.model';
import {PreventiveMaintenance} from '../preventive-maintenance.model';

export class GeneralPricelist {
  id: number;
  generalPricelistName: string;
  generalPricelistNumber: string;
  validFrom: number;
  validTo: number;
  ownerOrganization: Organization;
  currentPricelist: GeneralPricelistPricelist;
  deliveryTimeH: number;
  deliveryTimeT: number;
  deliveryTimeR: number;
  deliveryTimeI: number;
  deliveryTimeTJ: number;
  warrantyQuantityH: number;
  warrantyQuantityT: number;
  warrantyQuantityR: number;
  warrantyQuantityI: number;
  warrantyQuantityTJ: number;
  warrantyQuantityHUnit: OrderUnit;
  warrantyQuantityTUnit: OrderUnit;
  warrantyQuantityRUnit: OrderUnit;
  warrantyQuantityIUnit: OrderUnit;
  warrantyQuantityTJUnit: OrderUnit;
  warrantyValidFromH: PreventiveMaintenance;
  warrantyValidFromT: PreventiveMaintenance;
  warrantyValidFromR: PreventiveMaintenance;
  warrantyValidFromI: PreventiveMaintenance;
  warrantyValidFromTJ: PreventiveMaintenance;
  warrantyTermsH: string;
  warrantyTermsT: string;
  warrantyTermsR: string;
  warrantyTermsI: string;
  warrantyTermsTJ: string;
  hasPricelistRows: boolean;
}

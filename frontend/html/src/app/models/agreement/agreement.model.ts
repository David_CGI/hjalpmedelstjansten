/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Organization} from '../organization/organization.model';
import {BusinessLevel} from '../organization/business-level.model';
import {User} from '../user/user.model';
import {GuaranteeUnit} from './guarantee-unit.model';
import {PreventiveMaintenance} from '../preventive-maintenance.model';

export class Agreement {
  id: number;
  agreementName: string;
  agreementNumber: string;
  articlesExistOnAgreement: boolean;
  validFrom: number;
  validTo: number;
  extensionOptionTo: number;
  sendReminderOn: number;
  endDate: number;
  supplierOrganization: Organization;
  customerBusinessLevel: BusinessLevel;
  supplierContactPersons: Array<User>;
  customerPricelistApprovers: Array<User>;
  sharedWithCustomers: Array<Organization>;
  sharedWithCustomersBusinessLevels: Array<BusinessLevel>;
  customerOrganization: Organization;
  deliveryTimeH: number;
  deliveryTimeI: number;
  deliveryTimeR: number;
  deliveryTimeT: number;
  deliveryTimeTJ: number;
  status: string;
  warrantyQuantityH: number;
  warrantyQuantityHUnit: GuaranteeUnit;
  warrantyQuantityI: number;
  warrantyQuantityIUnit: GuaranteeUnit;
  warrantyQuantityR: number;
  warrantyQuantityRUnit: GuaranteeUnit;
  warrantyQuantityT: number;
  warrantyQuantityTUnit: GuaranteeUnit;
  warrantyQuantityTJ: number;
  warrantyQuantityTJUnit: GuaranteeUnit;
  warrantyTermsH: string;
  warrantyTermsI: string;
  warrantyTermsR: string;
  warrantyTermsT: string;
  warrantyTermsTJ: string;
  warrantyValidFromH: PreventiveMaintenance;
  warrantyValidFromI: PreventiveMaintenance;
  warrantyValidFromR: PreventiveMaintenance;
  warrantyValidFromT: PreventiveMaintenance;
  warrantyValidFromTJ: PreventiveMaintenance;
}

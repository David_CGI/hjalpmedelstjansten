/*
* Copyright (C) 2019 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

import {PathService} from './path.service';
import {DynamicText} from "../models/dynamictext.model";
import {ExportSetting} from "../models/export-setting.model";

@Injectable()
export class DynamicTextService {

  constructor(private httpClient: HttpClient,
              private pathService: PathService) {
  }

  public getDynamicText(uniqueId): Observable<DynamicText> {
    return this.httpClient.get<DynamicText>(this.pathService.getDynamicText(uniqueId));
  }

  public updateDynamicText(uniqueId, dynamicText): Observable<DynamicText> {
    return this.httpClient.put<DynamicText>(this.pathService.getDynamicText(uniqueId), dynamicText);
  }

}

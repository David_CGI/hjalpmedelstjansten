/*
* Copyright (C) 2019 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {PathService} from './path.service';
import {Assortment} from '../models/assortment/assortment.model';
import {County} from '../models/assortment/county.model';
import {Article} from '../models/product/article.model';

@Injectable()
export class AssortmentService {

  constructor(private httpClient: HttpClient,
              private pathService: PathService) {
  }

  public searchAssortments(organizationId, query): Observable<HttpResponse<Array<Assortment>>> {
    return this.httpClient.get<Array<Assortment>>(this.pathService.getAssortments(organizationId) + query, {observe: 'response'});
  }

  public searchAllAssortments(query): Observable<HttpResponse<Array<Assortment>>> {
    return this.httpClient.get<Array<Assortment>>(this.pathService.getAllAssortments() + query, {observe: 'response'});
  }

  public createAssortment(organizationId, assortment): Observable<Assortment> {
    return this.httpClient.post<Assortment>(this.pathService.getAssortments(organizationId), assortment);
  }

  public updateAssortment(organizationId, assortmentId, assortment): Observable<Assortment> {
    return this.httpClient.put<Assortment>(this.pathService.getAssortments(organizationId) + '/' + assortmentId, assortment);
  }

  public getAssortment(organizationId, assortmentId): Observable<Assortment> {
    return this.httpClient.get<Assortment>(this.pathService.getAssortments(organizationId) + '/' + assortmentId);
  }

  public searchArticlesForAssortment(organizationId, assortmentId, query): Observable<HttpResponse<Array<Article>>> {
    return this.httpClient.get<Array<Article>>(
      this.pathService.getAssortmentArticles(organizationId, assortmentId)  + query, {observe: 'response'});
  }

  public exportArticlesForAssortment(organizationId, assortmentId, query) {
    return this.httpClient.post(this.pathService.getExportArticlesForAssortment(organizationId, assortmentId) + query, {}, {
      responseType: 'blob',
      observe: 'response'
    });
  }

  public exportAssortments(organizationId, query) {
    return this.httpClient.post(this.pathService.getExportAssortments(organizationId) + query, {}, {
      responseType: 'blob',
      observe: 'response'
    });
  }

  public exportAllAssortments(query) {
    return this.httpClient.post(this.pathService.getExportAllAssortments() + query, {}, {
      responseType: 'blob',
      observe: 'response'
    });
  }

  public addArticlesToAssortment(organizationId, assortmentId, articles) {
    return this.httpClient.post<Assortment>(this.pathService.getAssortmentArticles(organizationId, assortmentId), articles);
  }

  public removeArticleFromAssortment(organizationId, assortmentId, articleId) {
    return this.httpClient.delete<Assortment>(this.pathService.getAssortmentArticles(organizationId, assortmentId) + articleId);
  }

  public getCounties(): Observable<Array<County>> {
    return this.httpClient.get<Array<County>>(this.pathService.getCounties());
  }

}

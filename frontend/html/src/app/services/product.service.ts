/*
* Copyright (C) 2019 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

import {PathService} from './path.service';
import {Product} from '../models/product/product.model';
import {Category} from '../models/product/category.model';
import {CeArray} from '../models/product/ce-array';
import {Article} from '../models/product/article.model';
import {OrderUnit} from '../models/product/order-unit.model';
import {GuaranteeUnit} from '../models/agreement/guarantee-unit.model';
import {SearchedProductOrArticle} from '../models/product/searched-product-or-article.model';
import {CategoryProperty} from '../models/product/category-property.model';
import {Media} from '../models/product/media.model';
import {SimpleArticle} from '../models/product/simple-article.model';
import {Pricelistrow} from '../models/agreement/pricelistrow.model';
import {HasPricelistRowResponse} from '../models/product/has-pricelist-row-response.model';
import {PackageUnit} from '../models/product/package-unit.model';

@Injectable()
export class ProductService {

  constructor(private httpClient: HttpClient,
              private pathService: PathService) {
  }

  public search(query): Observable<HttpResponse<Array<SearchedProductOrArticle>>> {
    return this.httpClient.get<Array<SearchedProductOrArticle>>(this.pathService.getSearch() + query, {observe: 'response'});
  }

  public searchProductsAndArticles(query, organizationId): Observable<HttpResponse<Array<SearchedProductOrArticle>>> {
    return this.httpClient.post<Array<SearchedProductOrArticle>>(this.pathService.searchProductsAndArticles
    (organizationId) + query, {}, {observe: 'response'});
  }

  public searchProducts(query, organizationId): Observable<HttpResponse<Array<Product>>> {
    return this.httpClient.get<Array<Product>>(this.pathService.searchProducts(organizationId) + query, {observe: 'response'});
  }

  public exportSearchProducts(organizationId, query) {
    return this.httpClient.post(this.pathService.getExportProductsAndArticles(organizationId) + query, {}, {
      responseType: 'blob',
      observe: 'response'
    });
  }

  public exportProductsAndArticlesForArticle(organizationId, articleId, query) {
    return this.httpClient.post(this.pathService.getExportProductsAndArticlesForArticle(organizationId, articleId) + query, {}, {
      responseType: 'blob',
      observe: 'response'
    });
  }

  public exportArticlesForProduct(organizationId, productId, query) {
    return this.httpClient.post(this.pathService.getExportArticlesForProduct(organizationId, productId) + query, {}, {
      responseType: 'blob',
      observe: 'response'
    });
  }

  public exportGeneralPricelistPricelistRows(organizationId, pricelistId, query) {
    return this.httpClient.post(this.pathService.getExportGeneralPricelistPricelistRows(organizationId, pricelistId) + query, {}, {
      responseType: 'blob',
      observe: 'response'
    });
  }

  public exportSearchResults(query) {
    return this.httpClient.post(this.pathService.getExportSearchResults() + query, {}, {
      responseType: 'blob',
      observe: 'response'
    });
  }

  public getArticlesByOrganization(query, organizationId): Observable<HttpResponse<Array<SimpleArticle>>> {
    return this.httpClient.get<Array<SimpleArticle>>(this.pathService.getArticlesByOrganization(organizationId) + query, {observe: 'response'});
  }

  public getProducts(organizationId): Observable<Array<Product>> {
    return this.httpClient.get<Array<Product>>(this.pathService.getProduct(organizationId));
  }

  public export(organizationId, queryParams) {
    return this.httpClient.get(this.pathService.getProduct(organizationId) + 'export?' + queryParams, {
      responseType: 'blob',
      observe: 'response'
    });
  }

  public import(organizationId, formData) {
    return this.httpClient.post(this.pathService.getProduct(organizationId) + 'import', formData);
  }

  public getProductsByType(organizationId, articleType): Observable<Array<Product>> {
    return this.httpClient.get<Array<Product>>(this.pathService.getProductsByQuery(organizationId) + 'type=' + articleType);
  }

  public getProductsByStatus(organizationId, status, status2): Observable<Array<Product>> {
    return this.httpClient.get<Array<Product>>(this.pathService.getProductsByQuery(organizationId) + 'status=' + status + '&status=' + status2);
  }

  public getProduct(organizationId, productId): Observable<Product> {
    return this.httpClient.get<Product>(this.pathService.getProduct(organizationId) + productId);
  }

  public getProductsByQuery(organizationId, query): Observable<Array<Product>> {
    return this.httpClient.get<Array<Product>>(this.pathService.getProductsByQuery(organizationId) + query);
  }

  public getMediaForProduct(organizationId, productId): Observable<Media> {
    return this.httpClient.get<Media>(this.pathService.getProduct(organizationId) + productId + '/media');
  }

  public getMediaForArticle(organizationId, articleId): Observable<Media> {
    return this.httpClient.get<Media>(this.pathService.getArticle(organizationId) + articleId + '/media');
  }

  public uploadMediaToProduct(organizationId, productId, mediaType, formData): Observable<Media> {
    return this.httpClient.post<Media>(
      this.pathService.getProduct(organizationId) + productId + '/media/' + mediaType, formData);
  }

  public deleteMediaFromProduct(organizationId, productId, mediaId): Observable<Object> {
    return this.httpClient.delete(this.pathService.getProduct(organizationId) + productId + '/media/' + mediaId);
  }

  public deleteArticle(organizationId, articleId): Observable<Object> {
    return this.httpClient.delete(this.pathService.getArticle(organizationId) + articleId);
  }

  public deleteProduct(organizationId, productId): Observable<Object> {
    return this.httpClient.delete(this.pathService.getProduct(organizationId) + productId);
  }

  public uploadMediaToArticle(organizationId, articleId, mediaType, formData): Observable<Media> {
    return this.httpClient.post<Media>(
      this.pathService.getArticle(organizationId) + articleId + '/media/' + mediaType, formData);
  }

  public deleteMediaFromArticle(organizationId, articleId, mediaId): Observable<Object> {
    return this.httpClient.delete(this.pathService.getArticle(organizationId) + articleId + '/media/' + mediaId);
  }

  public getPricelistRowsForArticle(organizationId, articleId): Observable<Array<Pricelistrow>> {
    return this.httpClient.get<Array<Pricelistrow>>(this.pathService.getArticle(organizationId) + articleId + '/pricelistrows');
  }

  public hasPricelistRowsForArticle(organizationId, articleId): Observable<HasPricelistRowResponse> {
    return this.httpClient.get<HasPricelistRowResponse>(this.pathService.getArticle(organizationId) + articleId + '/pricelistrowsexist');
  }

  public getGeneralPricelistPricelistRowsForArticle(organizationId, articleId): Observable<Array<Pricelistrow>> {
    return this.httpClient.get<Array<Pricelistrow>>(this.pathService.getArticle(organizationId) + articleId + '/generalpricelistrows');
  }

  public getCategories(queryParams): Observable<Array<Category>> {
    return this.httpClient.get<Array<Category>>(this.pathService.getCategories() + queryParams);
  }

  public searchCategories(query): Observable<Array<Category>> {
    return this.httpClient.get<Array<Category>>(this.pathService.searchCategories() + query);
  }

  public getChildCategories(code): Observable<Array<Category>> {
    return this.httpClient.get<Array<Category>>(this.pathService.getChildCategories() + code);
  }

  public createProduct(organizationId, product): Observable<Product> {
    return this.httpClient.post<Product>(this.pathService.getProduct(organizationId), product);
  }

  public updateProduct(organizationId, productId, product): Observable<Product> {
    return this.httpClient.put<Product>(this.pathService.getProduct(organizationId) + productId, product);
  }

  public getCE(): Observable<CeArray> {
    return this.httpClient.get<CeArray>(this.pathService.getCE());
  }

  public getOrderUnits(): Observable<Array<OrderUnit>> {
    return this.httpClient.get<Array<OrderUnit>>(this.pathService.getOrderUnits());
  }

  public getGuaranteeUnits(): Observable<Array<GuaranteeUnit>> {
    return this.httpClient.get<Array<GuaranteeUnit>>(this.pathService.getGuaranteeUnits());
  }

  public getPackageUnits(): Observable<Array<PackageUnit>> {
    return this.httpClient.get<Array<PackageUnit>>(this.pathService.getPackageUnits());
  }

  public getCategoryProperties(categoryId): Observable<Array<CategoryProperty>> {
    return this.httpClient.get<Array<CategoryProperty>>(this.pathService.getCategoryProperties(categoryId));
  }

  public searchArticles(query, organizationId, productId): Observable<HttpResponse<Array<Article>>> {
    return this.httpClient.get<Array<Article>>(this.pathService.searchArticles(organizationId, productId) + query, {observe: 'response'});
  }

  public getArticle(organizationId, articleId): Observable<Article> {
    return this.httpClient.get<Article>(this.pathService.getArticle(organizationId) + articleId);
  }

  public createArticle(organizationId, article): Observable<Article> {
    return this.httpClient.post<Article>(this.pathService.getArticle(organizationId), article);
  }

  public updateArticle(organizationId, articleId, article): Observable<Article> {
    return this.httpClient.put<Article>(this.pathService.getArticle(organizationId) + articleId, article);
  }

  public getArticlesBasedOnProduct(organizationId, productId, offset): Observable<HttpResponse<Array<Article>>> {
    return this.httpClient.get<Array<Article>>(this.pathService.getProduct(
      organizationId) + productId + '/basedon?offset=' + offset, {observe: 'response'});
  }

  public getArticlesFittingToProduct(organizationId, productId, offset): Observable<HttpResponse<Array<Article>>> {
    return this.httpClient.get<Array<Article>>(this.pathService.getProduct(
      organizationId) + productId + '/fitsto?offset=' + offset, {observe: 'response'});
  }

  public searchConnectedArticles(query, organizationId, articleId): Observable<HttpResponse<Array<Article>>> {
    return this.httpClient.get<Array<Article>>(this.pathService.searchConnectedArticles(organizationId, articleId) + query, {observe: 'response'});
  }

  public removeConnection(organizationId, articleId, fitsToId): any {
    return this.httpClient.delete(this.pathService.removeConnection(organizationId, articleId) + fitsToId);
  }

  public switchBasedOn(organizationId, articleId, basedOnId): Observable<Article> {
    return this.httpClient.post<Article>(this.pathService.getArticle(organizationId) + articleId + '/switchbasedon/' + basedOnId, {});
  }
}

/*
* Copyright (C) 2019 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

import {PathService} from './path.service';
import {BusinessLevel} from '../models/organization/business-level.model';
import {Organization} from '../models/organization/organization.model';
import {Role} from '../models/user/role.model';

@Injectable()
export class OrganizationService {

  constructor(private httpClient: HttpClient,
              private pathService: PathService) {
  }

  public getBusinessLevels(id): Observable<Array<BusinessLevel>> {
    return this.httpClient.get<Array<BusinessLevel>>(this.pathService.getBusinessLevels(id));
  }

  public getRoles(id): Observable<Array<Role>> {
    return this.httpClient.get<Array<Role>>(this.pathService.getRoles(id));
  }

  public getActiveBusinessLevels(id): Observable<Array<BusinessLevel>> {
    return this.httpClient.get<Array<BusinessLevel>>(this.pathService.getBusinessLevels(id) + '?status=ACTIVE');
  }

  public createBusinessLevel(id, businessLevel): Observable<BusinessLevel> {
    return this.httpClient.post<BusinessLevel>(this.pathService.getBusinessLevels(id), businessLevel);
  }

  public updateBusinessLevel(organizationId, businessLevelId, businessLevel): Observable<BusinessLevel> {
    return this.httpClient.post<BusinessLevel>(this.pathService.getBusinessLevels(
      organizationId) + '/' + businessLevelId + '/update', businessLevel);
  }

  public inactivateBusinessLevel(organizationId, businessLevelId): Observable<BusinessLevel> {
    return this.httpClient.post<BusinessLevel>(this.pathService.getBusinessLevels(
      organizationId) + '/' + businessLevelId + '/inactivate', {});
  }

  public deleteBusinessLevel(organizationId, businessLevelId): Observable<Object> {
    return this.httpClient.delete(this.pathService.getBusinessLevels(organizationId) + '/' + businessLevelId);
  }

  public searchOrganizations(query): Observable<HttpResponse<Array<Organization>>> {
    return this.httpClient.get<Array<Organization>>(this.pathService.searchOrganizations() + query, {observe: 'response'});
  }

  public exportOrganizations(query) {
    return this.httpClient.post(this.pathService.getExportOrganizations() + query, {}, {
      responseType: 'blob',
      observe: 'response'
    });
  }

  public getOrganization(id): Observable<Organization> {
    return this.httpClient.get<Organization>(this.pathService.getOrganization() + id);
  }

  public updateOrganization(id, organization): Observable<Organization> {
    return this.httpClient.put<Organization>(this.pathService.getOrganization() + id, organization);
  }

  public createOrganization(organization): Observable<Organization> {
    return this.httpClient.post<Organization>(this.pathService.getOrganization(), organization);
  }

  public deleteOrganization(id): Observable<Object> {
    return this.httpClient.delete(this.pathService.getOrganization() + id);
  }
}

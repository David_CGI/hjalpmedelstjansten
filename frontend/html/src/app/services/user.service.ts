/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

import {PathService} from './path.service';
import {User} from '../models/user/user.model';

@Injectable()
export class UserService {

  constructor(private httpClient: HttpClient,
              private pathService: PathService) {
  }

  public searchUsers(organizationId, query): Observable<HttpResponse<Array<User>>> {
    return this.httpClient.get<Array<User>>(this.pathService.searchUsers(organizationId) + query, {observe: 'response'});
  }

  public exportUsers(onlyActives, organizationId, query) {
    return this.httpClient.post(this.pathService.getExportUsers(onlyActives, organizationId) + query, {}, {
      responseType: 'blob',
      observe: 'response'
    });
  }

  public getUser(organizationId, userId): Observable<User> {
    return this.httpClient.get<User>(this.pathService.getUser(organizationId) + userId);
  }

  public updateUser(organizationId, userId, user): Observable<User> {
    return this.httpClient.put<User>(this.pathService.getUser(organizationId) + userId, user);
  }

  public createUser(organizationId, user): Observable<User> {
    return this.httpClient.post<User>(this.pathService.getUser(organizationId), user);
  }

  public deleteUser(organizationId, userId): Observable<Object> {
    return this.httpClient.delete(this.pathService.getUser(organizationId) + userId);
  }

  public getProfile(): Observable<User> {
    return this.httpClient.get<User>(this.pathService.getProfile());
  }

  public getUserByRoles(organizationId, roles): Observable<Array<User>> {
    return this.httpClient.get<Array<User>>(this.pathService.getUsersByRole(organizationId) + roles);
  }

  public searchUsersByBusinessLevel(query, organizationId, businessLevel): Observable<HttpResponse<Array<User>>> {
    return this.httpClient.get<Array<User>>(
      this.pathService.getUsersByBusinessLevel(query, organizationId) + businessLevel, {observe: 'response'});
  }

  public searchPricelistApprovers(organizationId, query): Observable<HttpResponse<Array<User>>> {
    return this.httpClient.get<Array<User>>(this.pathService.getPricelistApprovers(organizationId) + query, {observe: 'response'});
  }

}

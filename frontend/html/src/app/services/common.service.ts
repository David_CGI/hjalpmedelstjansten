/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {PathService} from './path.service';
import {PreventiveMaintenance} from '../models/preventive-maintenance.model';
import {DocumentType} from '../models/document-type.model';

@Injectable()
export class CommonService {

  constructor(private httpClient: HttpClient,
              private pathService: PathService) {
  }


  public getPreventiveMaintenances(): Observable<Array<PreventiveMaintenance>> {
    return this.httpClient.get<Array<PreventiveMaintenance>>(this.pathService.getPreventiveMaintenances());
  }

  public getDocumentTypes(): Observable<Array<DocumentType>> {
    return this.httpClient.get<Array<DocumentType>>(this.pathService.getDocumentTypes());
  }

  isUsingIE() {
    return /*@cc_on!@*/false || !!document['documentMode'];
  }
}

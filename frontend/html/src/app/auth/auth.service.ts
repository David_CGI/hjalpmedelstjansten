/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/share';

@Injectable()
export class AuthService {
  isLoginSubject = new BehaviorSubject<boolean>(this.hasToken());


  /**
   *
   * @returns {Observable<T>}
   */
  isLoggedIn(): Observable<boolean> {
    return this.isLoginSubject.asObservable().share();
  }

  hasPermission(permission): boolean {
    let isPermitted = false;
    JSON.parse(localStorage.getItem('token')).userEngagements[0].roles.forEach((role) => {
      role.permissions.forEach((rolePermission) => {
        if (rolePermission.name === permission) {
          isPermitted = true;
        }
      });
    });
    return isPermitted;
  }

  isSuperAdmin(): boolean {
    return JSON.parse(localStorage.getItem('token')).userEngagements[0].roles.some(x => x.name === 'Superadmin');
  }

  getOrganizationId(): number {
    return JSON.parse(localStorage.getItem('token')).userEngagements[0].organizationId;
  }

  getUserId(): number {
    return JSON.parse(localStorage.getItem('token')).id;
  }

  /**
   *  Login the user then tell all the subscribers about the new status
   */
  login(profile): void {
    localStorage.setItem('token', JSON.stringify(profile));
    this.isLoginSubject.next(true);
  }

  /**
   * Log out the user then tell all the subscribers about the new status
   */
  logout(): void {
    this.clearLocalStorage();
  }

  clearLocalStorage() {
    localStorage.removeItem('token');
    localStorage.clear();
    this.isLoginSubject.next(false);
  }

  /**
   * if we have token the user is loggedIn
   * @returns {boolean}
   */
  private hasToken(): boolean {
    return !!localStorage.getItem('token');
  }
}

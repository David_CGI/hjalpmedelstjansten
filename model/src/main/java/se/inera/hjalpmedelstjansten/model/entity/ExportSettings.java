/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.model.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * JPA Entity class for agreement export settings. Customers configures which 
 * general pricelists to include in export XML files. 
 * 
 * @author Tommy Berglund
 */
@Entity
@NamedQueries({
    @NamedQuery(name = ExportSettings.FIND_BY_CUSTOMER, query = "SELECT e FROM ExportSettings e WHERE e.organization.uniqueId = :organizationUniqueId"),
    @NamedQuery(name = ExportSettings.SEARCH_ALL, query = "SELECT e FROM ExportSettings e ORDER BY e.organization.organizationName ASC"),
    @NamedQuery(name = ExportSettings.FIND_ALL, query = "SELECT e FROM ExportSettings e ORDER BY e.organization.organizationName ASC"),
    @NamedQuery(name = ExportSettings.FIND_BY_FILE_NAME, query = "SELECT e FROM ExportSettings e WHERE e.filename = :filename"),
    @NamedQuery(name = ExportSettings.FIND_BY_ORGANIZATION, query = "SELECT e FROM ExportSettings e WHERE e.organization.uniqueId = :organizationUniqueId ORDER BY e.uniqueId ASC"),
    @NamedQuery(name = ExportSettings.FIND_BY_ORGANIZATION_AND_BUSINESS_LEVELS, query = "SELECT e FROM ExportSettings e WHERE e.organization.uniqueId = :organizationUniqueId AND e.businessLevel.uniqueId IN :businessLevelIds ORDER BY e.organization.organizationName ASC"),
    @NamedQuery(name = ExportSettings.FIND_BY_ORGANIZATION_AND_NO_BUSINESS_LEVELS, query = "SELECT e FROM ExportSettings e WHERE e.organization.uniqueId = :organizationUniqueId AND e.businessLevel IS NULL ORDER BY e.organization.organizationName ASC")
})
@Table(
    uniqueConstraints = {
        @UniqueConstraint(columnNames={"organizationId", "businessLevelId"})   
    }
)
public class ExportSettings implements Serializable {

    public static final String FIND_BY_CUSTOMER = "HJMT_AGREEMENTEXPORTSETTINGS_FIND_BY_CUSTOMER";
    public static final String SEARCH_ALL = "HJMT_AGREEMENTEXPORTSETTINGS_SEARCH_ALL";
    public static final String FIND_ALL = "HJMT_AGREEMENTEXPORTSETTINGS_FIND_ALL";
    public static final String FIND_BY_ORGANIZATION = "HJMT_AGREEMENTEXPORTSETTINGS_FIND_BY_ORGANIZATION";
    public static final String FIND_BY_ORGANIZATION_AND_BUSINESS_LEVELS = "HJMT_AGREEMENTEXPORTSETTINGS_FIND_BY_ORGANIZATION_AND_BUSINESS_LEVELS";
    public static final String FIND_BY_ORGANIZATION_AND_NO_BUSINESS_LEVELS = "HJMT_AGREEMENTEXPORTSETTINGS_FIND_BY_ORGANIZATION_AND_NO_BUSINESS_LEVELS";
    public static final String FIND_BY_FILE_NAME = "HJMT_AGREEMENTEXPORTSETTINGS_FIND_BY_FILE_NAME";
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long uniqueId;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "organizationId", nullable = false)
    private Organization organization;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "businessLevelId", nullable = true)
    private BusinessLevel businessLevel;
    
    @Column(nullable = false, unique = true)
    private String filename;
    
    @Column(nullable = false)
    private int numberOfExports;
    
    @Column(nullable = true)
    private Date lastExported;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date created;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date lastUpdated;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable
    (
        name="ExportSettingsGeneralPricelist",
        joinColumns={ @JoinColumn(name="ExportSettingsId", referencedColumnName="uniqueId") },
        inverseJoinColumns={ @JoinColumn(name="GeneralPricelistId", referencedColumnName="uniqueId") }
    )
    private List<GeneralPricelist> generalPricelists;
    
    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean enabled;
    
    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public List<GeneralPricelist> getGeneralPricelists() {
        return generalPricelists;
    }

    public void setGeneralPricelists(List<GeneralPricelist> generalPricelists) {
        this.generalPricelists = generalPricelists;
    }

    public BusinessLevel getBusinessLevel() {
        return businessLevel;
    }

    public void setBusinessLevel(BusinessLevel businessLevel) {
        this.businessLevel = businessLevel;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Date getLastExported() {
        return lastExported;
    }

    public void setLastExported(Date lastExported) {
        this.lastExported = lastExported;
    }
    
    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public int getNumberOfExports() {
        return numberOfExports;
    }

    public void setNumberOfExports(int numberOfExports) {
        this.numberOfExports = numberOfExports;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
    
    @PrePersist
    private void prePersist() {
        this.created = new Date();
    }
    
    @PreUpdate
    private void preUpdate() {
        this.lastUpdated = new Date();
    }
    
}
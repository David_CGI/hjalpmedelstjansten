/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.model.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;

/**
 * JPA Entity class for approved agreement pricelist row
 * 
 * @author Per Abrahamsson
 */
@Entity
//@NamedQueries({

//})
public class ApprovedAgreementPricelistRow implements Serializable {

    @Id
    @Column(nullable = false)
    private Long uniqueId;
      
    @Column(nullable = true)
    private BigDecimal price;
    
    @Temporal(TemporalType.DATE)
    @Column(nullable = true)
    private Date validFrom;
    
    @Column(nullable = false)
    private int leastOrderQuantity;
    
    @Column(nullable = true)
    private Integer deliveryTime;
    
    @Column(nullable = true)
    private Integer warrantyQuantity;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyQuantityUnitId", nullable = true)
    private CVGuaranteeUnit warrantyQuantityUnit;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyValidFromId", nullable = true)
    private CVPreventiveMaintenance warrantyValidFrom;
    
    @Column(nullable = true)
    private String warrantyTerms;

    @Column(nullable = false)
    private String approvedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date approvedDate;
    
    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }


    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }


    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }


    public int getLeastOrderQuantity() {
        return leastOrderQuantity;
    }

    public void setLeastOrderQuantity(int leastOrderQuantity) {
        this.leastOrderQuantity = leastOrderQuantity;
    }


    public Integer getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Integer deliveryTime) {
        this.deliveryTime = deliveryTime;
    }


    public Integer getWarrantyQuantity() {
        return warrantyQuantity;
    }

    public void setWarrantyQuantity(Integer warrantyQuantity) {
        this.warrantyQuantity = warrantyQuantity;
    }


    public CVGuaranteeUnit getWarrantyQuantityUnit() {
        return warrantyQuantityUnit;
    }

    public void setWarrantyQuantityUnit(CVGuaranteeUnit warrantyQuantityUnit) {
        this.warrantyQuantityUnit = warrantyQuantityUnit;
    }


    public CVPreventiveMaintenance getWarrantyValidFrom() {
        return warrantyValidFrom;
    }

    public void setWarrantyValidFrom(CVPreventiveMaintenance warrantyValidFrom) {
        this.warrantyValidFrom = warrantyValidFrom;
    }


    public String getWarrantyTerms() {
        return warrantyTerms;
    }

    public void setWarrantyTerms(String warrantyTerms) {
        this.warrantyTerms = warrantyTerms;
    }


    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public Date getApprovedDate() {return approvedDate;}

    @PrePersist
    private void prePersist() {
        this.approvedDate = new Date();
    }
    
    @PreUpdate
    private void preUpdate() {
        this.approvedDate = new Date();
    }
    
}
/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.model.entity;

import se.inera.hjalpmedelstjansten.model.entity.cv.CVCountry;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;

/**
 *
 * @author Tommy Berglund
 */
@Entity
@NamedQueries({
    @NamedQuery(name = Organization.FIND_ALL, query = "SELECT o FROM Organization o ORDER BY o.organizationName ASC"),
    @NamedQuery(name = Organization.COUNT_SEARCH, query = "SELECT COUNT(o) FROM Organization o WHERE o.organizationName LIKE :query AND o.organizationType IN :organizationTypes"),
    @NamedQuery(name = Organization.SEARCH, query = "SELECT o FROM Organization o WHERE o.organizationName LIKE :query AND o.organizationType IN :organizationTypes ORDER BY o.organizationName ASC"),
    @NamedQuery(name = Organization.FIND_BY_ORGANIZATION_NUMBER_AND_TYPE, query = "SELECT o FROM Organization o WHERE o.organizationNumber = :organizationNumber AND o.organizationType = :type"),
    @NamedQuery(name = Organization.FIND_BY_GLN_AND_TYPE, query = "SELECT o FROM Organization o WHERE o.gln = :gln AND o.organizationType = :type"),
    @NamedQuery(name = Organization.FIND_BY_GLN, query = "SELECT o FROM Organization o WHERE o.gln = :gln"),
    @NamedQuery(name = Organization.FIND_BY_ORGANIZATION_TYPE, query = "SELECT o FROM Organization o WHERE o.organizationType = :organizationType")
})
@Table(
    uniqueConstraints = {
        @UniqueConstraint(columnNames={"gln", "organizationType"}),
        @UniqueConstraint(columnNames={"organizationNumber", "organizationType"})    
    }
)
public class Organization implements Serializable {
    
    public enum OrganizationType {
        CUSTOMER, SUPPLIER, SERVICE_OWNER
    }
    
    public static final String FIND_ALL = "HJMT_ORGANIZATION_FIND_ALL";
    public static final String SEARCH = "HJMT_ORGANIZATION_SEARCH";
    public static final String COUNT_SEARCH = "HJMT_ORGANIZATION_COUNT_SEARCH";
    public static final String FIND_BY_ORGANIZATION_NUMBER_AND_TYPE = "HJMT_ORGANIZATION_FIND_BY_ORGANIZATION_NUMBER_AND_TYPE";
    public static final String FIND_BY_GLN_AND_TYPE = "HJMT_ORGNIZATION_FIND_BY_GLN_AND_TYPE";
    public static final String FIND_BY_GLN = "HJMT_ORGNIZATION_FIND_BY_GLN";
    public static final String FIND_BY_ORGANIZATION_TYPE = "HJMT_ORGNIZATION_FIND_BY_ORGANIZATION_TYPE";
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long uniqueId;
    
    @Column(nullable = false)
    private String organizationName;
    
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private OrganizationType organizationType;
        
    @Column(unique = false, nullable = false, length = 13)
    private String gln;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "countryId")
    private CVCountry country;
    
    @Column(unique = false, nullable = false, updatable = false)
    private String organizationNumber;
           
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date validFrom;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date validTo;
    
    @OneToMany(
            mappedBy = "organization",
            cascade = {CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.MERGE},
            fetch = FetchType.LAZY
    )
    private List<BusinessLevel> businessLevels;

    @OneToMany(
        mappedBy = "organization", 
        cascade = {CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.MERGE},
        fetch = FetchType.LAZY
    )
    @Size(min = 0, max = 2)
    private List<PostAddress> postAddresses;
    
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.MERGE})
    @JoinColumn(name = "electronicAddressId")
    private ElectronicAddress electronicAddress;
    
    @Column(nullable = false, unique = true)
    private String mediaFolderName;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date created;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date lastUpdated;
    
    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public OrganizationType getOrganizationType() {
        return organizationType;
    }

    public void setOrganizationType(OrganizationType organizationType) {
        this.organizationType = organizationType;
    }

    public String getOrganizationNumber() {
        return organizationNumber;
    }

    public void setOrganizationNumber(String organizationNumber) {
        this.organizationNumber = organizationNumber;
    }
    
    public String getGln() {
        return gln;
    }

    public void setGln(String gln) {
        this.gln = gln;
    }

    public CVCountry getCountry() {
        return country;
    }

    public void setCountry(CVCountry country) {
        this.country = country;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public List<BusinessLevel> getBusinessLevels() {
        return businessLevels;
    }

    public void setBusinessLevels(List<BusinessLevel> businessLevels) {
        this.businessLevels = businessLevels;
    }

    public List<PostAddress> getPostAddresses() {
        return postAddresses;
    }

    public void setPostAddresses(List<PostAddress> postAddresses) {
        this.postAddresses = postAddresses;
    }

    public ElectronicAddress getElectronicAddress() {
        return electronicAddress;
    }

    public void setElectronicAddress(ElectronicAddress electronicAddress) {
        this.electronicAddress = electronicAddress;
    }

    public String getMediaFolderName() {
        return mediaFolderName;
    }

    public void setMediaFolderName(String mediaFolderName) {
        this.mediaFolderName = mediaFolderName;
    }
    
    @PrePersist
    private void prePersist() {
        this.created = new Date();
    }
    
    @PreUpdate
    private void preUpdate() {
        this.lastUpdated = new Date();
    }
    
}
/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.model.entity;

import se.inera.hjalpmedelstjansten.model.entity.cv.CVOrderUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEStandard;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEDirective;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPackageUnit;

/**
 * JPA Entity class for Article. 
 * - articleNumber must be unique per organization
 * - gtin is globally unique but in the service it is not
 * 
 * @author Tommy Berglund
 */
@Entity
@NamedQueries({
    @NamedQuery(name = Article.COUNT_SEARCH_BASED_ON_PRODUCT, query = "SELECT COUNT(a) FROM Article a WHERE a.basedOnProduct.uniqueId = :uniqueId AND a.basedOnProduct.organization.uniqueId = :organizationUniqueId ORDER BY a.articleNumber ASC"),
    @NamedQuery(name = Article.SEARCH_BASED_ON_PRODUCT, query = "SELECT a FROM Article a WHERE a.basedOnProduct.uniqueId = :productUniqueId AND a.basedOnProduct.organization.uniqueId = :organizationUniqueId ORDER BY a.articleNumber ASC"),
    @NamedQuery(name = Article.SEARCH_IDS_BASED_ON_PRODUCT, query = "SELECT a.uniqueId FROM Article a WHERE a.basedOnProduct.uniqueId = :productUniqueId AND a.basedOnProduct.organization.uniqueId = :organizationUniqueId ORDER BY a.articleNumber ASC"),
    @NamedQuery(name = Article.COUNT_SEARCH_FITS_TO_PRODUCT, query = "SELECT COUNT(a) FROM Article a JOIN a.fitsToProducts fp WHERE fp.uniqueId = :uniqueId AND fp.organization.uniqueId = :organizationUniqueId ORDER BY a.articleNumber ASC"),
    @NamedQuery(name = Article.SEARCH_FITS_TO_PRODUCT, query = "SELECT a FROM Article a JOIN a.fitsToProducts fp WHERE fp.uniqueId = :uniqueId AND fp.organization.uniqueId = :organizationUniqueId ORDER BY a.articleNumber ASC"),
    @NamedQuery(name = Article.FIND_BY_STATUS_AND_REPLACEMENT_DATE_PASSED, query = "SELECT a FROM Article a WHERE a.status = :status AND a.replacementDate < :replacementDate"),
    @NamedQuery(name = Article.FIND_BY_GTIN, query = "SELECT a FROM Article a WHERE a.gtin = :gtin"),
    @NamedQuery(name = Article.FIND_BY_ARTICLE_NUMBER_AND_ORGANIZATION, query = "SELECT a FROM Article a WHERE a.articleNumber = :articleNumber AND a.organization.uniqueId = :organizationUniqueId"),
    @NamedQuery(name = Article.FIND_BY_ORGANIZATION_AND_CATEGORY, query = "SELECT a FROM Article a WHERE a.organization.uniqueId = :organizationUniqueId AND a.category.uniqueId = :categoryUniqueId"),
    @NamedQuery(name = Article.FIND_REPLACES, query = "SELECT a FROM Article a JOIN a.replacedByArticles ar WHERE ar.uniqueId = :articleUniqueId"),
    @NamedQuery(name = Article.GET_IDS_BASED_ON_PRODUCT, query = "SELECT a.uniqueId FROM Article a WHERE a.basedOnProduct.uniqueId = :productUniqueId"),
    @NamedQuery(name = Article.GET_IDS_FITS_TO_PRODUCT, query = "SELECT a.uniqueId FROM Article a JOIN a.fitsToProducts ftp WHERE ftp.uniqueId = :productUniqueId"),
    @NamedQuery(name = Article.GET_IDS_FITS_TO_ARTICLE, query = "SELECT a.uniqueId FROM Article a JOIN a.fitsToArticles fta WHERE fta.uniqueId = :articleUniqueId")
})
public class Article implements Serializable {

    public static final String COUNT_SEARCH_BASED_ON_PRODUCT = "HJMT_ARTICLE_COUNT_SEARCH_BASED_ON_PRODUCT";
    public static final String SEARCH_BASED_ON_PRODUCT = "HJMT_ARTICLE_SEARCH_BASED_ON_PRODUCT";
    public static final String SEARCH_IDS_BASED_ON_PRODUCT = "HJMT_ARTICLE_SEARCH_IDS_BASED_ON_PRODUCT";
    public static final String COUNT_SEARCH_FITS_TO_PRODUCT = "HJMT_ARTICLE_COUNT_SEARCH_FITS_TO_PRODUCT";
    public static final String SEARCH_FITS_TO_PRODUCT = "HJMT_ARTICLE_SEARCH_FITS_TO_PRODUCT";
    public static final String FIND_BY_STATUS_AND_REPLACEMENT_DATE_PASSED = "HJMT_ARTICLE_FIND_BY_STATUS_AND_REPLACEMENT_DATE_PASSED";
    public static final String FIND_BY_GTIN = "HJMT_ARTICLE_FIND_BY_GTIN";
    public static final String FIND_BY_ARTICLE_NUMBER_AND_ORGANIZATION = "HJMT_ARTICLE_FIND_BY_ARTICLE_NUMBER_AND_ORGANIZATION";
    public static final String FIND_BY_ORGANIZATION_AND_CATEGORY = "HJMT_ARTICLE_FIND_BY_ORGANIZATION_AND_CATEGORY";
    public static final String FIND_REPLACES = "HJMT_ARTICLE_FIND_REPLACES";
    public static final String GET_IDS_BASED_ON_PRODUCT = "HJMT_ARTICLE_GET_IDS_BASED_ON_PRODUCT";
    public static final String GET_IDS_FITS_TO_PRODUCT = "HJMT_ARTICLE_GET_IDS_FITS_TO_PRODUCT";
    public static final String GET_IDS_FITS_TO_ARTICLE = "HJMT_ARTICLE_GET_IDS_FITS_TO_ARTICLE";

    public enum Type {
        H, T, R, I, Tj
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long uniqueId;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "organizationId", nullable = false)
    private Organization organization;
    
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Product.Status status; 
    
    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean statusOverridden;
    
    @Column(nullable = false)
    private String articleName;
    
    @Column(nullable = false, length = 35)
    private String articleNumber;
        
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "basedOnProductId", nullable = true)
    private Product basedOnProduct;
    
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name="ArticleFitsToProduct",
        joinColumns={@JoinColumn(name="articleId", referencedColumnName="uniqueId")},
        inverseJoinColumns={@JoinColumn(name="productId", referencedColumnName="uniqueId")}) 
    private List<Product> fitsToProducts;
    
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name="ArticleFitsToArticle",
        joinColumns={@JoinColumn(name="fittedByArticleId", referencedColumnName="uniqueId")},
        inverseJoinColumns={@JoinColumn(name="fitsToArticleId", referencedColumnName="uniqueId")}) 
    private List<Article> fitsToArticles;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "categoryId", nullable = true)
    private Category category;
    
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name="ArticleExtendedCategory",
        joinColumns={@JoinColumn(name="articleId", referencedColumnName="uniqueId")},
        inverseJoinColumns={@JoinColumn(name="categoryId", referencedColumnName="uniqueId")}) 
    private List<Category> extendedCategories;
    
    @Column(nullable = true, length = 13)
    private String gtin;
    
    @Column(nullable = true, length = 512)
    private String supplementedInformation;

    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean supplementedInformationOverridden;

    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean ceMarked;
    
    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean ceMarkedOverridden;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ceDirectiveId", nullable = true)
    private CVCEDirective ceDirective;
    
    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean ceDirectiveOverridden;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ceStandardId", nullable = true)
    private CVCEStandard ceStandard;
    
    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean ceStandardOverridden;
    
    @Column(nullable = true)
    private String manufacturer;
    
    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean manufacturerOverridden;
    
    @Column(nullable = true)
    private String manufacturerArticleNumber;
    
    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean manufacturerArticleNumberOverridden;
    
    @Column(nullable = true)
    private String trademark;
    
    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean trademarkOverridden;
    
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JoinColumn(name = "manufacturerElectronicAddressId", nullable = true)
    private ElectronicAddress manufacturerElectronicAddress;
    
    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean manufacturerElectronicAddressOverridden;
    
    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST})
    @JoinColumn(name = "catalogUniqueNumberId", nullable = true)
    private CatalogueUniqueNumber catalogUniqueNumber;
    
    @Column(nullable = true, columnDefinition = "TINYINT(1)")
    private Boolean customerUnique;
    
    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean customerUniqueOverridden;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "preventiveMaintenanceValidFromId", nullable = true)
    private CVPreventiveMaintenance preventiveMaintenanceValidFrom;
    
    @Column(nullable = true)
    private Integer preventiveMaintenanceNumberOfDays;

    @Column(nullable = true)
    private String preventiveMaintenanceDescription; 
    
    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean preventiveMaintenanceValidFromOverridden;
    
    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean preventiveMaintenanceNumberOfDaysOverridden;
    
    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean preventiveMaintenanceDescriptionOverridden;
    
    @Column(nullable = true)
    private String color;
    
    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean colorOverridden;
    
    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
        name="ArticleReplacedByArticle",
        joinColumns={@JoinColumn(name="articleId", referencedColumnName="uniqueId")},
        inverseJoinColumns={@JoinColumn(name="replacedByArticleId", referencedColumnName="uniqueId")}) 
    private List<Article> replacedByArticles;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date replacementDate;
    
    @Column(nullable = true, columnDefinition = "TINYINT(1)")
    private Boolean inactivateRowsOnReplacement;
    
    // All order information is treated as "one" so overriding one field
    // means overriding all order information.
    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean orderInformationOverridden;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "orderUnitId", nullable = true)
    private CVOrderUnit orderUnit;

    private Double articleQuantityInOuterPackage;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "articleQuantityInOuterPackageUnitId", nullable = true)
    private CVOrderUnit articleQuantityInOuterPackageUnit;
    
    private Double packageContent;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "packageContentId", nullable = true)
    private CVPackageUnit packageContentUnit;
    
    @Column(nullable = true)
    private Integer packageLevelBase;

    @Column(nullable = true)
    private Integer packageLevelMiddle;

    @Column(nullable = true)
    private Integer packageLevelTop;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date created;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date lastUpdated;
    
    @OneToMany(
            mappedBy = "article",
            cascade = {CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.MERGE},
            fetch = FetchType.LAZY,
            orphanRemoval = true
    )
    private List<ResourceSpecificPropertyValue> resourceSpecificPropertyValues;
    
    @Column(nullable = false, unique = true)
    private String mediaFolderName;
    
    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }
    
    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public String getArticleNumber() {
        return articleNumber;
    }

    public void setArticleNumber(String articleNumber) {
        this.articleNumber = articleNumber;
    }

    public Product getBasedOnProduct() {
        return basedOnProduct;
    }

    public void setBasedOnProduct(Product basedOnProduct) {
        this.basedOnProduct = basedOnProduct;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Product.Status getStatus() {
        return status;
    }

    public void setStatus(Product.Status status) {
        this.status = status;
    }

    public boolean isStatusOverridden() {
        return statusOverridden;
    }

    public void setStatusOverridden(boolean statusOverridden) {
        this.statusOverridden = statusOverridden;
    }

    public String getGtin() {
        return gtin;
    }

    public void setGtin(String gtin) {
        this.gtin = gtin;
    }

    public List<Product> getFitsToProducts() {
        return fitsToProducts;
    }

    public void setFitsToProducts(List<Product> fitsToProducts) {
        this.fitsToProducts = fitsToProducts;
    }

    public List<Article> getFitsToArticles() {
        return fitsToArticles;
    }

    public void setFitsToArticles(List<Article> fitsToArticles) {
        this.fitsToArticles = fitsToArticles;
    }

    public String getSupplementedInformation() {
        return supplementedInformation;
    }

    public void setSupplementedInformation(String supplementedInformation) {
        this.supplementedInformation = supplementedInformation;
    }

    public boolean isSupplementedInformationOverridden() {
        return supplementedInformationOverridden;
    }

    public void setSupplementedInformationOverridden(boolean supplementedInformationOverridden) {
        this.supplementedInformationOverridden = supplementedInformationOverridden;
    }

    public CVCEDirective getCeDirective() {
        return ceDirective;
    }

    public void setCeDirective(CVCEDirective ceDirective) {
        this.ceDirective = ceDirective;
    }

    public CVCEStandard getCeStandard() {
        return ceStandard;
    }

    public void setCeStandard(CVCEStandard ceStandard) {
        this.ceStandard = ceStandard;
    }

    public boolean isCeMarked() {
        return ceMarked;
    }

    public void setCeMarked(boolean ceMarked) {
        this.ceMarked = ceMarked;
    }

    public boolean isCeMarkedOverridden() {
        return ceMarkedOverridden;
    }

    public void setCeMarkedOverridden(boolean ceMarkedOverridden) {
        this.ceMarkedOverridden = ceMarkedOverridden;
    }

    public boolean isCeDirectiveOverridden() {
        return ceDirectiveOverridden;
    }

    public void setCeDirectiveOverridden(boolean ceDirectiveOverridden) {
        this.ceDirectiveOverridden = ceDirectiveOverridden;
    }

    public boolean isCeStandardOverridden() {
        return ceStandardOverridden;
    }

    public void setCeStandardOverridden(boolean ceStandardOverridden) {
        this.ceStandardOverridden = ceStandardOverridden;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public boolean isManufacturerOverridden() {
        return manufacturerOverridden;
    }

    public void setManufacturerOverridden(boolean manufacturerOverridden) {
        this.manufacturerOverridden = manufacturerOverridden;
    }

    public String getManufacturerArticleNumber() {
        return manufacturerArticleNumber;
    }

    public void setManufacturerArticleNumber(String manufacturerArticleNumber) {
        this.manufacturerArticleNumber = manufacturerArticleNumber;
    }

    public boolean isManufacturerArticleNumberOverridden() {
        return manufacturerArticleNumberOverridden;
    }

    public void setManufacturerArticleNumberOverridden(boolean manufacturerArticleNumberOverridden) {
        this.manufacturerArticleNumberOverridden = manufacturerArticleNumberOverridden;
    }

    public String getTrademark() {
        return trademark;
    }

    public void setTrademark(String trademark) {
        this.trademark = trademark;
    }

    public boolean isTrademarkOverridden() {
        return trademarkOverridden;
    }

    public void setTrademarkOverridden(boolean trademarkOverridden) {
        this.trademarkOverridden = trademarkOverridden;
    }

    public ElectronicAddress getManufacturerElectronicAddress() {
        return manufacturerElectronicAddress;
    }

    public void setManufacturerElectronicAddress(ElectronicAddress manufacturerElectronicAddress) {
        this.manufacturerElectronicAddress = manufacturerElectronicAddress;
    }

    public boolean isManufacturerElectronicAddressOverridden() {
        return manufacturerElectronicAddressOverridden;
    }

    public void setManufacturerElectronicAddressOverridden(boolean manufacturerElectronicAddressOverridden) {
        this.manufacturerElectronicAddressOverridden = manufacturerElectronicAddressOverridden;
    }

    public CatalogueUniqueNumber getCatalogUniqueNumber() {
        return catalogUniqueNumber;
    }

    public void setCatalogUniqueNumber(CatalogueUniqueNumber catalogUniqueNumber) {
        this.catalogUniqueNumber = catalogUniqueNumber;
    }

    public Boolean isCustomerUnique() {
        return customerUnique;
    }

    public void setCustomerUnique(Boolean customerUnique) {
        this.customerUnique = customerUnique;
    }

    public boolean isCustomerUniqueOverridden() {
        return customerUniqueOverridden;
    }

    public void setCustomerUniqueOverridden(boolean customerUniqueOverridden) {
        this.customerUniqueOverridden = customerUniqueOverridden;
    }

    public CVPreventiveMaintenance getPreventiveMaintenanceValidFrom() {
        return preventiveMaintenanceValidFrom;
    }

    public void setPreventiveMaintenanceValidFrom(CVPreventiveMaintenance preventiveMaintenanceValidFrom) {
        this.preventiveMaintenanceValidFrom = preventiveMaintenanceValidFrom;
    }

    public Integer getPreventiveMaintenanceNumberOfDays() {
        return preventiveMaintenanceNumberOfDays;
    }

    public void setPreventiveMaintenanceNumberOfDays(Integer preventiveMaintenanceNumberOfDays) {
        this.preventiveMaintenanceNumberOfDays = preventiveMaintenanceNumberOfDays;
    }

    public String getPreventiveMaintenanceDescription() {
        return preventiveMaintenanceDescription;
    }

    public void setPreventiveMaintenanceDescription(String preventiveMaintenanceDescription) {
        this.preventiveMaintenanceDescription = preventiveMaintenanceDescription;
    }

    public boolean isPreventiveMaintenanceValidFromOverridden() {
        return preventiveMaintenanceValidFromOverridden;
    }

    public void setPreventiveMaintenanceValidFromOverridden(boolean preventiveMaintenanceValidFromOverridden) {
        this.preventiveMaintenanceValidFromOverridden = preventiveMaintenanceValidFromOverridden;
    }

    public boolean isPreventiveMaintenanceNumberOfDaysOverridden() {
        return preventiveMaintenanceNumberOfDaysOverridden;
    }

    public void setPreventiveMaintenanceNumberOfDaysOverridden(boolean preventiveMaintenanceNumberOfDaysOverridden) {
        this.preventiveMaintenanceNumberOfDaysOverridden = preventiveMaintenanceNumberOfDaysOverridden;
    }

    public boolean isPreventiveMaintenanceDescriptionOverridden() {
        return preventiveMaintenanceDescriptionOverridden;
    }

    public void setPreventiveMaintenanceDescriptionOverridden(boolean preventiveMaintenanceDescriptionOverridden) {
        this.preventiveMaintenanceDescriptionOverridden = preventiveMaintenanceDescriptionOverridden;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isColorOverridden() {
        return colorOverridden;
    }

    public void setColorOverridden(boolean colorOverridden) {
        this.colorOverridden = colorOverridden;
    }

    public List<Category> getExtendedCategories() {
        return extendedCategories;
    }

    public void setExtendedCategories(List<Category> extendedCategories) {
        this.extendedCategories = extendedCategories;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public List<Article> getReplacedByArticles() {
        return replacedByArticles;
    }

    public void setReplacedByArticles(List<Article> replacedByArticles) {
        this.replacedByArticles = replacedByArticles;
    }

    public Date getReplacementDate() {
        return replacementDate;
    }

    public void setReplacementDate(Date replacementDate) {
        this.replacementDate = replacementDate;
    }

    public Boolean isInactivateRowsOnReplacement() {
        return inactivateRowsOnReplacement;
    }

    public void setInactivateRowsOnReplacement(Boolean inactivateRowsOnReplacement) {
        this.inactivateRowsOnReplacement = inactivateRowsOnReplacement;
    }

    public boolean isOrderInformationOverridden() {
        return orderInformationOverridden;
    }

    public void setOrderInformationOverridden(boolean orderInformationOverridden) {
        this.orderInformationOverridden = orderInformationOverridden;
    }

    public CVOrderUnit getOrderUnit() {
        return orderUnit;
    }

    public void setOrderUnit(CVOrderUnit orderUnit) {
        this.orderUnit = orderUnit;
    }

    public Double getArticleQuantityInOuterPackage() {
        return articleQuantityInOuterPackage;
    }

    public void setArticleQuantityInOuterPackage(Double articleQuantityInOuterPackage) {
        this.articleQuantityInOuterPackage = articleQuantityInOuterPackage;
    }

    public CVOrderUnit getArticleQuantityInOuterPackageUnit() {
        return articleQuantityInOuterPackageUnit;
    }

    public void setArticleQuantityInOuterPackageUnit(CVOrderUnit articleQuantityInOuterPackageUnit) {
        this.articleQuantityInOuterPackageUnit = articleQuantityInOuterPackageUnit;
    }

    public Double getPackageContent() {
        return packageContent;
    }

    public void setPackageContent(Double packageContent) {
        this.packageContent = packageContent;
    }

    public CVPackageUnit getPackageContentUnit() {
        return packageContentUnit;
    }

    public void setPackageContentUnit(CVPackageUnit packageContentUnit) {
        this.packageContentUnit = packageContentUnit;
    }

    public Integer getPackageLevelBase() {
        return packageLevelBase;
    }

    public void setPackageLevelBase(Integer packageLevelBase) {
        this.packageLevelBase = packageLevelBase;
    }

    public Integer getPackageLevelMiddle() {
        return packageLevelMiddle;
    }

    public void setPackageLevelMiddle(Integer packageLevelMiddle) {
        this.packageLevelMiddle = packageLevelMiddle;
    }

    public Integer getPackageLevelTop() {
        return packageLevelTop;
    }

    public void setPackageLevelTop(Integer packageLevelTop) {
        this.packageLevelTop = packageLevelTop;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public List<ResourceSpecificPropertyValue> getResourceSpecificPropertyValues() {
        return resourceSpecificPropertyValues;
    }

    public void setResourceSpecificPropertyValues(List<ResourceSpecificPropertyValue> resourceSpecificPropertyValues) {
        this.resourceSpecificPropertyValues = resourceSpecificPropertyValues;
    }

    public String getMediaFolderName() {
        return mediaFolderName;
    }

    public void setMediaFolderName(String mediaFolderName) {
        this.mediaFolderName = mediaFolderName;
    }
    
    @PrePersist
    private void prePersist() {
        this.created = new Date();
        this.catalogUniqueNumber = new CatalogueUniqueNumber();
    }
    
    @PreUpdate
    private void preUpdate() {
        this.lastUpdated = new Date();
    }
    
}
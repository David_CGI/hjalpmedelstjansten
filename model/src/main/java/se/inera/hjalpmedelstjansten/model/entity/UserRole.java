/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.model.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author Tommy Berglund
 */
@Entity
@NamedQueries({
    @NamedQuery(name = UserRole.FIND_BY_NAMES, query = "SELECT u FROM UserRole u WHERE u.name IN :names")
})
public class UserRole implements Serializable {

    public enum RoleName {
        Baseuser, 
        BaseTokenuser, 
        Superadmin, 
        Supplieradmin, 
        SupplierAgreementViewer, 
        SupplierAgreementManager,
        SupplierProductAndArticleHandler, 
        Customeradmin, 
        CustomerAgreementViewer,
        CustomerAgreementManager,
        CustomerAssortmentManager,
        CustomerAssignedAssortmentManager
    }
    
    public static final String FIND_BY_NAMES =  "HJMT_USERROLE_FIND_BY_NAMES";
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long uniqueId;
    
    @Column(unique = true, nullable = false)
    @Enumerated(EnumType.STRING)
    private RoleName name;
    
    @Column(nullable = false)
    private String description;
    
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name="UserRoleUserPermission",
        joinColumns={@JoinColumn(name="userRoleId", referencedColumnName="uniqueId")},
        inverseJoinColumns={@JoinColumn(name="userPermissionId", referencedColumnName="uniqueId")}) 
    private List<UserPermission> permissions;
    
    public Long getUniqueId() {
        return uniqueId;
    }
    
    public RoleName getName() {
        return name;
    }

    public void setName(RoleName name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<UserPermission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<UserPermission> permissions) {
        this.permissions = permissions;
    }

}

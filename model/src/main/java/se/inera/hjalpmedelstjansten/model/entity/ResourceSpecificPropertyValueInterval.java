/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.model.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Answer to interval type.
 * 
 * @author Tommy Berglund
 */
@Entity
@DiscriminatorValue(value = "INTERVAL")
public class ResourceSpecificPropertyValueInterval extends ResourceSpecificPropertyValue {

    @Column(nullable = true)
    private Double intervalFromValue;
    
    @Column(nullable = true)
    private Double intervalToValue;

    public Double getFromValue() {
        return intervalFromValue;
    }

    public void setFromValue(Double fromValue) {
        this.intervalFromValue = fromValue;
    }

    public Double getToValue() {
        return intervalToValue;
    }

    public void setToValue(Double toValue) {
        this.intervalToValue = toValue;
    }
        
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.model.entity.cv;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 * Package Unit for <code>Product</code> and <code>Article</code>
 * 
 * @author Tommy Berglund
 */
@Entity
@NamedQueries({
    @NamedQuery(name = CVPackageUnit.FIND_ALL, query = "SELECT u FROM CVPackageUnit u ORDER BY u.displayOrder ASC"),
    @NamedQuery(name = CVPackageUnit.FIND_BY_CODE, query = "SELECT u FROM CVPackageUnit u WHERE u.code = :code"),
    @NamedQuery(name = CVPackageUnit.FIND_BY_NAME, query = "SELECT u FROM CVPackageUnit u WHERE u.name = :name")
})
public class CVPackageUnit implements Serializable {

    public static final String FIND_ALL = "HJMT_CV_PACKAGE_UNIT_FIND_ALL";
    public static final String FIND_BY_CODE = "HJMT_CV_PACKAGE_UNIT_FIND_BY_CODE";
    public static final String FIND_BY_NAME = "HJMT_CV_PACKAGE_UNIT_FIND_BY_NAME";
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long uniqueId;
    
    @Column(nullable = false, unique = true)
    private String name;
    
    @Column(nullable = true, unique = true)
    private String code;
    
    @Column(nullable = false)
    private int displayOrder;
    
    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

}

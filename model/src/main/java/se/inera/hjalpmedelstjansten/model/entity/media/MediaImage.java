/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.model.entity.media;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 * Media of type image
 * 
 * @author Tommy Berglund
 */
@Entity
@NamedQueries({
    @NamedQuery(name = MediaImage.FIND_BY_PRODUCT, query = "SELECT m FROM MediaImage m WHERE m.product.uniqueId = :productUniqueId ORDER BY m.created DESC"),
    @NamedQuery(name = MediaImage.FIND_BY_PRODUCT_AND_MAIN, query = "SELECT m FROM MediaImage m WHERE m.product.uniqueId = :productUniqueId AND m.mainImage IS TRUE")
})
public class MediaImage extends Media {
    
    public static final String FIND_BY_PRODUCT = "HJMT_MEDIAIMAGE_FIND_BY_PRODUCT";
    public static final String FIND_BY_PRODUCT_AND_MAIN = "HJMT_MEDIAIMAGE_FIND_BY_PRODUCT_AND_MAIN";
    
    @Column(nullable = true)
    private String alternativeText;

    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean mainImage;
    
    @Column(nullable = false)
    private String fileName;
        
    @Column(nullable = true)
    private String originalUrl;
    
    @Column(nullable = false)
    private String externalKey;
    
    public String getAlternativeText() {
        return alternativeText;
    }

    public void setAlternativeText(String alternativeText) {
        this.alternativeText = alternativeText;
    }

    public boolean isMainImage() {
        return mainImage;
    }

    public void setMainImage(boolean mainImage) {
        this.mainImage = mainImage;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getOriginalUrl() {
        return originalUrl;
    }

    public void setOriginalUrl(String originalUrl) {
        this.originalUrl = originalUrl;
    }

    public String getExternalKey() {
        return externalKey;
    }

    public void setExternalKey(String externalKey) {
        this.externalKey = externalKey;
    }
    
}

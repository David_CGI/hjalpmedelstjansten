/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.inera.hjalpmedelstjansten.model.entity.media;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

/**
 *
 * @author tomber
 */
@Entity
@NamedQueries({
    @NamedQuery(name = ArticleMediaImage.FIND_BY_ARTICLE, query = "SELECT a FROM ArticleMediaImage a WHERE a.article.uniqueId = :articleUniqueId ORDER BY a.created DESC"),
    @NamedQuery(name = ArticleMediaImage.FIND_BY_ARTICLE_MAIN_FIRST, query = "SELECT a FROM ArticleMediaImage a WHERE a.article.uniqueId = :articleUniqueId ORDER BY a.mediaImage.mainImage desc"),
    @NamedQuery(name = ArticleMediaImage.FIND_BY_ARTICLE_AND_MAIN, query = "SELECT a FROM ArticleMediaImage a WHERE a.article.uniqueId = :articleUniqueId AND a.mediaImage.mainImage IS TRUE"),
    @NamedQuery(name = ArticleMediaImage.FIND_BY_MEDIAIMAGE, query = "SELECT a FROM ArticleMediaImage a WHERE a.mediaImage.uniqueId = :mediaUniqueId ORDER BY a.created DESC")
})
public class ArticleMediaImage extends ArticleMedia {
    
    public static final String FIND_BY_ARTICLE = "HJMT_ARTICLEMEDIAIMAGE_FIND_BY_ARTICLE";
    public static final String FIND_BY_ARTICLE_MAIN_FIRST = "HJMT_ARTICLEMEDIAIMAGE_FIND_BY_ARTICLE_MAIN_FIRST";
    public static final String FIND_BY_ARTICLE_AND_MAIN = "HJMT_ARTICLEMEDIAIMAGE_FIND_BY_ARTICLE_AND_MAIN";
    public static final String FIND_BY_MEDIAIMAGE = "HJMT_ARTICLEMEDIAIMAGE_FIND_BY_MEDIAIMAGE";
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mediaImageId", nullable = false)
    private MediaImage mediaImage;

    public MediaImage getMediaImage() {
        return mediaImage;
    }

    public void setMediaImage(MediaImage mediaImage) {
        this.mediaImage = mediaImage;
    }

}

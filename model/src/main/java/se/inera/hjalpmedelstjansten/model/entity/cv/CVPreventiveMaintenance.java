/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.inera.hjalpmedelstjansten.model.entity.cv;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author tomber
 */
@Entity
@NamedQueries({
    @NamedQuery(name = CVPreventiveMaintenance.FIND_ALL, query = "SELECT c FROM CVPreventiveMaintenance c ORDER BY c.displayOrder ASC"),
    @NamedQuery(name = CVPreventiveMaintenance.FIND_BY_CODE, query = "SELECT c FROM CVPreventiveMaintenance c WHERE c.code = :code"),
    @NamedQuery(name = CVPreventiveMaintenance.FIND_BY_NAME, query = "SELECT c FROM CVPreventiveMaintenance c WHERE c.name = :name")
})
public class CVPreventiveMaintenance implements Serializable {
    
    public static final String FIND_ALL = "HJMT_CV_PREVENTIVE_MAINTENANCE_FIND_ALL";
    public static final String FIND_BY_CODE = "HJMT_CV_PREVENTIVE_MAINTENANCE_FIND_BY_CODE";
    public static final String FIND_BY_NAME = "HJMT_CV_PREVENTIVE_MAINTENANCE_FIND_BY_NAME";
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long uniqueId;
    
    @Column(unique = true, nullable = false)
    private String code;
    
    @Column(unique = true, nullable = false)
    private String name;

    @Column(unique = true, nullable = false)
    private int displayOrder;
    
    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }
    
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.model.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * JPA Entity class for Pricelist for a General Pricelist. Status is not saved per 
 * pricelist, instead it is calculated when needed based on the validFrom, this 
 * in order to avoid writing a batch job to update statuses daily. 
 * 
 * @author Tommy Berglund
 */
@Entity
@NamedQueries({
    @NamedQuery(name = GeneralPricelistPricelist.GET_ALL_BY_GENERAL_PRICELIST, query = "SELECT p FROM GeneralPricelistPricelist p WHERE p.generalPricelist.uniqueId = :generalPricelistUniqueId ORDER BY p.validFrom ASC"),
    @NamedQuery(name = GeneralPricelistPricelist.FIND_BY_VALID_FROM_AND_GENERAL_PRICELIST, query = "SELECT p FROM GeneralPricelistPricelist p WHERE p.generalPricelist.uniqueId = :generalPricelistUniqueId AND p.validFrom = :validFrom"),
    @NamedQuery(name = GeneralPricelistPricelist.FIND_BY_NUMBER_AND_GENERAL_PRICELIST, query = "SELECT p FROM GeneralPricelistPricelist p WHERE p.generalPricelist.uniqueId = :generalPricelistUniqueId AND p.number = :number"),
    @NamedQuery(name = GeneralPricelistPricelist.FIND_BY_PASSED_VALID_FROM_AND_ORGANIZATION, query = "SELECT p FROM GeneralPricelistPricelist p WHERE p.generalPricelist.ownerOrganization.uniqueId = :organizationUniqueId AND p.validFrom <= :validFrom ORDER BY p.validFrom DESC"),
    @NamedQuery(name = GeneralPricelistPricelist.FIND_BY_PASSED_VALID_FROM_AND_GENERAL_PRICELIST, query = "SELECT p FROM GeneralPricelistPricelist p WHERE p.generalPricelist.uniqueId = :generalPricelistUniqueId AND p.validFrom <= :validFrom ORDER BY p.validFrom DESC")
})
public class GeneralPricelistPricelist implements Serializable {

    public static final String GET_ALL_BY_GENERAL_PRICELIST = "HJMT_PRICELIST_GET_ALL_BY_GENERAL_PRICELIST";
    public static final String FIND_BY_VALID_FROM_AND_GENERAL_PRICELIST = "HJMT_PRICELIST_FIND_BY_VALID_FROM_AND_GENERAL_PRICELIST";
    public static final String FIND_BY_NUMBER_AND_GENERAL_PRICELIST = "HJMT_PRICELIST_FIND_BY_NUMBER_AND_GENERAL_PRICELIST";
    public static final String FIND_BY_PASSED_VALID_FROM_AND_ORGANIZATION = "HJMT_PRICELIST_FIND_BY_PASSED_VALID_FROM_AND_ORGANIZATION";
    public static final String FIND_BY_PASSED_VALID_FROM_AND_GENERAL_PRICELIST = "HJMT_PRICELIST_FIND_BY_PASSED_VALID_FROM_AND_GENERAL_PRICELIST";
    
    public enum Status {
        PAST, CURRENT, FUTURE
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long uniqueId;
    
    @Column(nullable = false)
    private String number;
        
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "generalPricelistId", nullable = false)
    private GeneralPricelist generalPricelist;
    
    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    private Date validFrom;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date created;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date lastUpdated;
    
    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public GeneralPricelist getGeneralPricelist() {
        return generalPricelist;
    }

    public void setGeneralPricelist(GeneralPricelist generalPricelist) {
        this.generalPricelist = generalPricelist;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @PrePersist
    private void prePersist() {
        this.created = new Date();
    }
    
    @PreUpdate
    private void preUpdate() {
        this.lastUpdated = new Date();
    }
    
}
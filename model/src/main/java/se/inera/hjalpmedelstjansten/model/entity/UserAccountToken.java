/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.model.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Tommy Berglund
 */
@Entity
@NamedQueries({
    @NamedQuery(name = UserAccountToken.FIND_BY_EXPIRED_TOKENS, query = "SELECT uat FROM UserAccountToken uat WHERE uat.tokenExpires < :date"),
    @NamedQuery(name = UserAccountToken.FIND_BY_USERACCOUNT, query = "SELECT uat FROM UserAccountToken uat WHERE uat.userAccount.uniqueId = :userAccountUniqueId")
})
public class UserAccountToken implements Serializable {
        
    public static final String FIND_BY_EXPIRED_TOKENS = "HJMTJ_USERACCOUNTTOKEN_FIND_BY_EXPIRED_TOKENS";
    public static final String FIND_BY_USERACCOUNT = "HJMTJ_USERACCOUNTTOKEN_FIND_BY_USERACCOUNT";

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long uniqueId;
        
    @Column(nullable = false)
    private String token;
    
    @Column(nullable = true)
    private String salt;
    
    @Column(nullable = true)
    private int iterations;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date tokenExpires;    
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userAccountId", nullable = false)
    private UserAccount userAccount;
    
    public Long getUniqueId() {
        return uniqueId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public int getIterations() {
        return iterations;
    }

    public void setIterations(int iterations) {
        this.iterations = iterations;
    }

    public Date getTokenExpires() {
        return tokenExpires;
    }

    public void setTokenExpires(Date tokenExpires) {
        this.tokenExpires = tokenExpires;
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

}

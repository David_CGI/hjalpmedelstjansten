/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.model.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

/**
 * Grouping of categories as defined by external api
 * 
 * @author Tommy Berglund
 */
@Entity
@NamedQueries({
    @NamedQuery(name = ExternalCategoryGrouping.FIND_ALL, query = "SELECT ecg FROM ExternalCategoryGrouping ecg ORDER BY ecg.uniqueId ASC"),
    @NamedQuery(name = ExternalCategoryGrouping.FIND_ALL_NON_CATEGORY, query = "SELECT ecg FROM ExternalCategoryGrouping ecg WHERE ecg.category IS NULL ORDER BY ecg.uniqueId ASC")
})
public class ExternalCategoryGrouping implements Serializable {

    public static final String FIND_ALL = "HJMT_EXTERNALCATEGORYGROUPING_FIND_ALL";
    public static final String FIND_ALL_NON_CATEGORY = "HJMT_EXTERNALCATEGORYGROUPING_FIND_ALL_NON_CATEGORY";
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long uniqueId;
    
    @Column(nullable = false)
    private String name;
    
    @Column(nullable = false, length = 500)
    private String description;
        
    @Column(nullable = true)
    private Integer displayOrder;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parentId", nullable = true)
    private ExternalCategoryGrouping parent;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="categoryId", nullable = true)
    private Category category;
    
    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    public ExternalCategoryGrouping getParent() {
        return parent;
    }

    public void setParent(ExternalCategoryGrouping parent) {
        this.parent = parent;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

}

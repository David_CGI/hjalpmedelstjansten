/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.model.entity;

import java.io.Serializable;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * Answer parent class. Use of single table inheritance for performance reasons.
 * 
 * @author Tommy Berglund
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "propertyType")
public abstract class ResourceSpecificPropertyValue implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long uniqueId;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "categorySpecificPropertyId", nullable = false)
    private CategorySpecificProperty categorySpecificProperty;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "productId", nullable = true)
    private Product product;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "articleId", nullable = true)
    private Article article;
    
    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public CategorySpecificProperty getCategorySpecificProperty() {
        return categorySpecificProperty;
    }

    public void setCategorySpecificProperty(CategorySpecificProperty categorySpecificProperty) {
        this.categorySpecificProperty = categorySpecificProperty;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

}

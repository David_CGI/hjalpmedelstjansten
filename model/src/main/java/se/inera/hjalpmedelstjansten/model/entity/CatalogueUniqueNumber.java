/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.model.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

/**
 * JPA Entity class for Product
 * 
 * @author Tommy Berglund
 */
@Entity
public class CatalogueUniqueNumber implements Serializable {
        
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "CatalogueUniqueNumberIdSequence")
    @SequenceGenerator(name = "CatalogueUniqueNumberIdSequence", sequenceName = "CatalogueUniqueNumberIdSequence")
    private Long uniqueId;
    
    public Long getUniqueId() {
        return uniqueId;
    }
    
    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.inera.hjalpmedelstjansten.model.entity.media;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

/**
 *
 * @author tomber
 */
@Entity
@NamedQueries({
    @NamedQuery(name = ArticleMediaDocument.FIND_BY_ARTICLE, query = "SELECT a FROM ArticleMediaDocument a WHERE a.article.uniqueId = :articleUniqueId ORDER BY a.created DESC"),
    @NamedQuery(name = ArticleMediaDocument.FIND_BY_MEDIADOCUMENT, query = "SELECT a FROM ArticleMediaDocument a WHERE a.mediaDocument.uniqueId = :mediaUniqueId ORDER BY a.created DESC")
})
public class ArticleMediaDocument extends ArticleMedia {
    
    public static final String FIND_BY_ARTICLE = "HJMT_ARTICLEMEDIADOCUMENT_FIND_BY_ARTICLE";
    public static final String FIND_BY_MEDIADOCUMENT = "HJMT_ARTICLEMEDIADOCUMENT_FIND_BY_MEDIADOCUMENT";
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mediaDocumentId", nullable = false)
    private MediaDocument mediaDocument;

    public MediaDocument getMediaDocument() {
        return mediaDocument;
    }

    public void setMediaDocument(MediaDocument mediaDocument) {
        this.mediaDocument = mediaDocument;
    }
        
}

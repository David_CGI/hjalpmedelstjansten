/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.model.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Tommy Berglund
 */
@Entity
public class InternalAudit implements Serializable {
    
    public enum EntityType {
        ORGANIZATION, 
        USER, 
        PRODUCT, 
        ARTICLE, 
        AGREEMENT, 
        AGREEMENT_PRICELIST, 
        AGREEMENT_PRICELIST_ROW, 
        GENERAL_PRICELIST,
        GENERAL_PRICELIST_PRICELIST, 
        GENERAL_PRICELIST_PRICELIST_ROW, 
        ASSORTMENT,
        MEDIA,
        EXPORTFILE
    }
    
    public enum ActionType {
        CREATE, UPDATE, DELETE, LOGIN, LOGOUT, VIEW
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "InternalAuditIdSequence" )
    @SequenceGenerator(name = "InternalAuditIdSequence", sequenceName = "InternalAuditIdSequence")
    private Long id;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date auditTime;

    @Enumerated(EnumType.STRING)
    private EntityType entityType;
            
    @Enumerated(EnumType.STRING)
    private ActionType actionType;
    
    @Column(nullable = true)
    private Long userEngagementId;
    
    @Column(nullable = true)
    private Long entityId;
    
    @Column(nullable = true)
    private String sessionId;
    
    @Column(nullable = true)
    private String requestIp;
    
    public Long getId() {
        return id;
    }

    public Date getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(Date auditTime) {
        this.auditTime = auditTime;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }    

    public Long getUserEngagementId() {
        return userEngagementId;
    }

    public void setUserEngagementId(Long userEngagementId) {
        this.userEngagementId = userEngagementId;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public String getRequestIp() {
        return requestIp;
    }

    public void setRequestIp(String requestIp) {
        this.requestIp = requestIp;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
    
}

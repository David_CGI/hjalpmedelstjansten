/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.model.entity;

import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;

/**
 * JPA Entity class for General Pricelist. Only one general pricelist per 
 * organization (supplier) is allowed. 
 * 
 * @author Tommy Berglund
 */
@Entity
@NamedQueries({
    @NamedQuery(name = GeneralPricelist.FIND_BY_ORGANIZATION, query = "SELECT gp FROM GeneralPricelist gp WHERE gp.ownerOrganization.uniqueId = :organizationUniqueId")
})
public class GeneralPricelist implements Serializable {

    public static final String FIND_BY_ORGANIZATION = "HJMT_GENERALPRICELIST_FIND_BY_ORGANIZATION";
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long uniqueId;
    
    @Column(nullable = true)
    private String generalPricelistName;
    
    @Column(nullable = false)
    private String generalPricelistNumber;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ownerOrganizationId", nullable = false, unique = true)
    private Organization ownerOrganization;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date validFrom;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date validTo;
        
    @Column(nullable = true)
    private Integer deliveryTimeH;

    @Column(nullable = true)
    private Integer deliveryTimeT;
    
    @Column(nullable = true)
    private Integer deliveryTimeI;
    
    @Column(nullable = true)
    private Integer deliveryTimeR;
    
    @Column(nullable = true)
    private Integer deliveryTimeTJ;

    @Column(nullable = true)
    private Integer warrantyQuantityH;

    @Column(nullable = true)
    private Integer warrantyQuantityT;
    
    @Column(nullable = true)
    private Integer warrantyQuantityI;
    
    @Column(nullable = true)
    private Integer warrantyQuantityR;
    
    @Column(nullable = true)
    private Integer warrantyQuantityTJ;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyQuantityHUnitId", nullable = true)
    private CVGuaranteeUnit warrantyQuantityHUnit;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyQuantityTUnitId", nullable = true)
    private CVGuaranteeUnit warrantyQuantityTUnit;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyQuantityIUnitId", nullable = true)
    private CVGuaranteeUnit warrantyQuantityIUnit;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyQuantityRUnitId", nullable = true)
    private CVGuaranteeUnit warrantyQuantityRUnit;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyQuantityTJUnitId", nullable = true)
    private CVGuaranteeUnit warrantyQuantityTJUnit;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyValidFromHId", nullable = true)
    private CVPreventiveMaintenance warrantyValidFromH;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyValidFromTId", nullable = true)
    private CVPreventiveMaintenance warrantyValidFromT;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyValidFromIId", nullable = true)
    private CVPreventiveMaintenance warrantyValidFromI;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyValidFromRId", nullable = true)
    private CVPreventiveMaintenance warrantyValidFromR;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "warrantyValidFromTJId", nullable = true)
    private CVPreventiveMaintenance warrantyValidFromTJ;
    
    @Column(nullable = true)
    private String warrantyTermsH;
    
    @Column(nullable = true)
    private String warrantyTermsT;
    
    @Column(nullable = true)
    private String warrantyTermsI;
    
    @Column(nullable = true)
    private String warrantyTermsR;
    
    @Column(nullable = true)
    private String warrantyTermsTJ;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date created;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date lastUpdated;
    
    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getGeneralPricelistName() {
        return generalPricelistName;
    }

    public void setGeneralPricelistName(String generalPricelistName) {
        this.generalPricelistName = generalPricelistName;
    }

    public String getGeneralPricelistNumber() {
        return generalPricelistNumber;
    }

    public void setGeneralPricelistNumber(String generalPricelistNumber) {
        this.generalPricelistNumber = generalPricelistNumber;
    }

    public Organization getOwnerOrganization() {
        return ownerOrganization;
    }

    public void setOwnerOrganization(Organization ownerOrganization) {
        this.ownerOrganization = ownerOrganization;
    }
    
    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public Integer getDeliveryTimeH() {
        return deliveryTimeH;
    }

    public void setDeliveryTimeH(Integer deliveryTimeH) {
        this.deliveryTimeH = deliveryTimeH;
    }

    public Integer getDeliveryTimeT() {
        return deliveryTimeT;
    }

    public void setDeliveryTimeT(Integer deliveryTimeT) {
        this.deliveryTimeT = deliveryTimeT;
    }

    public Integer getDeliveryTimeI() {
        return deliveryTimeI;
    }

    public void setDeliveryTimeI(Integer deliveryTimeI) {
        this.deliveryTimeI = deliveryTimeI;
    }

    public Integer getDeliveryTimeR() {
        return deliveryTimeR;
    }

    public void setDeliveryTimeR(Integer deliveryTimeR) {
        this.deliveryTimeR = deliveryTimeR;
    }

    public Integer getDeliveryTimeTJ() {
        return deliveryTimeTJ;
    }

    public void setDeliveryTimeTJ(Integer deliveryTimeTJ) {
        this.deliveryTimeTJ = deliveryTimeTJ;
    }

    public Integer getWarrantyQuantityH() {
        return warrantyQuantityH;
    }

    public void setWarrantyQuantityH(Integer warrantyQuantityH) {
        this.warrantyQuantityH = warrantyQuantityH;
    }

    public Integer getWarrantyQuantityT() {
        return warrantyQuantityT;
    }

    public void setWarrantyQuantityT(Integer warrantyQuantityT) {
        this.warrantyQuantityT = warrantyQuantityT;
    }

    public Integer getWarrantyQuantityI() {
        return warrantyQuantityI;
    }

    public void setWarrantyQuantityI(Integer warrantyQuantityI) {
        this.warrantyQuantityI = warrantyQuantityI;
    }

    public Integer getWarrantyQuantityR() {
        return warrantyQuantityR;
    }

    public void setWarrantyQuantityR(Integer warrantyQuantityR) {
        this.warrantyQuantityR = warrantyQuantityR;
    }

    public Integer getWarrantyQuantityTJ() {
        return warrantyQuantityTJ;
    }

    public void setWarrantyQuantityTJ(Integer warrantyQuantityTJ) {
        this.warrantyQuantityTJ = warrantyQuantityTJ;
    }

    public CVGuaranteeUnit getWarrantyQuantityHUnit() {
        return warrantyQuantityHUnit;
    }

    public void setWarrantyQuantityHUnit(CVGuaranteeUnit warrantyQuantityHUnit) {
        this.warrantyQuantityHUnit = warrantyQuantityHUnit;
    }

    public CVGuaranteeUnit getWarrantyQuantityTUnit() {
        return warrantyQuantityTUnit;
    }

    public void setWarrantyQuantityTUnit(CVGuaranteeUnit warrantyQuantityTUnit) {
        this.warrantyQuantityTUnit = warrantyQuantityTUnit;
    }

    public CVGuaranteeUnit getWarrantyQuantityIUnit() {
        return warrantyQuantityIUnit;
    }

    public void setWarrantyQuantityIUnit(CVGuaranteeUnit warrantyQuantityIUnit) {
        this.warrantyQuantityIUnit = warrantyQuantityIUnit;
    }

    public CVGuaranteeUnit getWarrantyQuantityRUnit() {
        return warrantyQuantityRUnit;
    }

    public void setWarrantyQuantityRUnit(CVGuaranteeUnit warrantyQuantityRUnit) {
        this.warrantyQuantityRUnit = warrantyQuantityRUnit;
    }

    public CVGuaranteeUnit getWarrantyQuantityTJUnit() {
        return warrantyQuantityTJUnit;
    }

    public void setWarrantyQuantityTJUnit(CVGuaranteeUnit warrantyQuantityTJUnit) {
        this.warrantyQuantityTJUnit = warrantyQuantityTJUnit;
    }

    public CVPreventiveMaintenance getWarrantyValidFromH() {
        return warrantyValidFromH;
    }

    public void setWarrantyValidFromH(CVPreventiveMaintenance warrantyValidFromH) {
        this.warrantyValidFromH = warrantyValidFromH;
    }

    public CVPreventiveMaintenance getWarrantyValidFromT() {
        return warrantyValidFromT;
    }

    public void setWarrantyValidFromT(CVPreventiveMaintenance warrantyValidFromT) {
        this.warrantyValidFromT = warrantyValidFromT;
    }

    public CVPreventiveMaintenance getWarrantyValidFromI() {
        return warrantyValidFromI;
    }

    public void setWarrantyValidFromI(CVPreventiveMaintenance warrantyValidFromI) {
        this.warrantyValidFromI = warrantyValidFromI;
    }

    public CVPreventiveMaintenance getWarrantyValidFromR() {
        return warrantyValidFromR;
    }

    public void setWarrantyValidFromR(CVPreventiveMaintenance warrantyValidFromR) {
        this.warrantyValidFromR = warrantyValidFromR;
    }

    public CVPreventiveMaintenance getWarrantyValidFromTJ() {
        return warrantyValidFromTJ;
    }

    public void setWarrantyValidFromTJ(CVPreventiveMaintenance warrantyValidFromTJ) {
        this.warrantyValidFromTJ = warrantyValidFromTJ;
    }

    public String getWarrantyTermsH() {
        return warrantyTermsH;
    }

    public void setWarrantyTermsH(String warrantyTermsH) {
        this.warrantyTermsH = warrantyTermsH;
    }

    public String getWarrantyTermsT() {
        return warrantyTermsT;
    }

    public void setWarrantyTermsT(String warrantyTermsT) {
        this.warrantyTermsT = warrantyTermsT;
    }

    public String getWarrantyTermsI() {
        return warrantyTermsI;
    }

    public void setWarrantyTermsI(String warrantyTermsI) {
        this.warrantyTermsI = warrantyTermsI;
    }

    public String getWarrantyTermsR() {
        return warrantyTermsR;
    }

    public void setWarrantyTermsR(String warrantyTermsR) {
        this.warrantyTermsR = warrantyTermsR;
    }

    public String getWarrantyTermsTJ() {
        return warrantyTermsTJ;
    }

    public void setWarrantyTermsTJ(String warrantyTermsTJ) {
        this.warrantyTermsTJ = warrantyTermsTJ;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @PrePersist
    private void prePersist() {
        this.created = new Date();
    }
    
    @PreUpdate
    private void preUpdate() {
        this.lastUpdated = new Date();
    }
    
}
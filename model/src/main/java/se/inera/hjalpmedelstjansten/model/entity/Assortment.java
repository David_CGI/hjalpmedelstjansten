/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.model.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCounty;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVMunicipality;

/**
 * JPA Entity class for Assortment
 * 
 * @author Tommy Berglund
 */
@Entity
@NamedQueries({
    @NamedQuery(name = Assortment.GET_ARTICLE_UNIQUE_IDS, query = "SELECT ar.uniqueId FROM Assortment a JOIN a.articles ar WHERE a.uniqueId = :assortmentUniqueId"),
    @NamedQuery(name = Assortment.FIND_BY_ARTICLE, query = "SELECT a FROM Assortment a JOIN a.articles ar WHERE ar.uniqueId = :articleUniqueId")
})
public class Assortment implements Serializable {

    public enum Status {
        FUTURE, CURRENT, DISCONTINUED
    }

    public static final String GET_ARTICLE_UNIQUE_IDS = "HJMT_ASSORTMENT_GET_ARTICLE_UNIQUE_IDS";
    public static final String FIND_BY_ARTICLE = "HJMT_ASSORTMENT_FIND_BY_ARTICLE";
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long uniqueId;
    
    @Column(nullable = false)
    private String name;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customerOrganizationId", nullable = false)
    private Organization customer;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date validFrom;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date validTo;
        
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "countyId", nullable = true)
    private CVCounty county;
    
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name="AssortmentCVMunicipality",
        joinColumns={@JoinColumn(name="assortmentId", referencedColumnName="uniqueId")},
        inverseJoinColumns={@JoinColumn(name="municipalityId", referencedColumnName="uniqueId")}) 
    private List<CVMunicipality> municipalities;
    
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name="AssortmentUserEngagement",
        joinColumns={@JoinColumn(name="assortmentId", referencedColumnName="uniqueId")},
        inverseJoinColumns={@JoinColumn(name="userEngagementId", referencedColumnName="uniqueId")}) 
    private List<UserEngagement> managers;
    
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name="AssortmentArticle",
        joinColumns={@JoinColumn(name="assortmentId", referencedColumnName="uniqueId")},
        inverseJoinColumns={@JoinColumn(name="articleId", referencedColumnName="uniqueId")}) 
    private List<Article> articles;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date created;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date lastUpdated;
    
    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Organization getCustomer() {
        return customer;
    }

    public void setCustomer(Organization customer) {
        this.customer = customer;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public CVCounty getCounty() {
        return county;
    }

    public void setCounty(CVCounty county) {
        this.county = county;
    }

    public List<CVMunicipality> getMunicipalities() {
        return municipalities;
    }

    public void setMunicipalities(List<CVMunicipality> municipalities) {
        this.municipalities = municipalities;
    }

    public List<UserEngagement> getManagers() {
        return managers;
    }

    public void setManagers(List<UserEngagement> managers) {
        this.managers = managers;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
    
    @PrePersist
    private void prePersist() {
        this.created = new Date();
    }
    
    @PreUpdate
    private void preUpdate() {
        this.lastUpdated = new Date();
    }
    
}
/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.model.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

/**
 * Category of <code>Product</code> and <code>Category</code>
 * 
 * @author Tommy Berglund
 */
@Entity
@NamedQueries({
    @NamedQuery(name = Category.FIND_LEAF_NODES, query = "SELECT c FROM Category c WHERE c.articleType IS NOT NULL"),
    @NamedQuery(name = Category.FIND_BY_CODE, query = "SELECT c FROM Category c WHERE c.code = :code"),
    @NamedQuery(name = Category.FIND_BY_CODE_AND_LEAFNODE, query = "SELECT c FROM Category c WHERE c.code = :code AND c.articleType IS NOT NULL"),
    @NamedQuery(name = Category.FIND_BY_ID_AND_LEAFNODE, query = "SELECT c FROM Category c WHERE c.uniqueId = :uniqueId AND c.articleType IS NOT NULL"),
    @NamedQuery(name = Category.FIND_FIRST_LEVEL_CATEGORIES, query = "SELECT c FROM Category c WHERE c.parent IS NULL ORDER BY c.uniqueId ASC"),
    @NamedQuery(name = Category.SEARCH_CATEGORIES, query = "SELECT c FROM Category c WHERE (c.name LIKE :query OR c.code LIKE :query) AND c.articleType IS NOT NULL ORDER BY c.code ASC"),
    @NamedQuery(name = Category.SEARCH_CATEGORIES_BY_PARENT, query = "SELECT c FROM Category c WHERE c.parent.uniqueId = :uniqueId ORDER BY c.code ASC"),
    @NamedQuery(name = Category.FIND_BY_ARTICLE_TYPE_EMPTY_CODE, query = "SELECT c FROM Category c WHERE c.articleType = :articleType AND c.code IS NULL")    
})
public class Category implements Serializable {

    public static final String FIND_LEAF_NODES = "HJMT_CATEGORY_FIND_ALL";
    public static final String FIND_BY_CODE = "HJMT_CATEGORY_FIND_BY_CODE";
    public static final String FIND_FIRST_LEVEL_CATEGORIES = "HJMT_CATEGORY_FIND_FIRST_LEVEL_CATEGORIES";
    public static final String SEARCH_CATEGORIES = "HJMT_CATEGORY_SEARCH_CATEGORIES";
    public static final String SEARCH_CATEGORIES_BY_PARENT = "HJMT_CATEGORY_SEARCH_CATEGORIES_BY_CODE";
    public static final String FIND_BY_ID_AND_LEAFNODE = "HJMT_CATEGORY_FIND_BY_ID_CHILDLESS";
    public static final String FIND_BY_CODE_AND_LEAFNODE = "HJMT_CATEGORY_FIND_BY_CODE_AND_LEAFNODE";
    public static final String FIND_BY_ARTICLE_TYPE_EMPTY_CODE = "HJMT_CATEGORY_FIND_BY_ARTICLE_TYPE_EMPTY_CODE";
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long uniqueId;
    
    @Column(nullable = false)
    private String name;
    
    @Column(nullable = true, unique = true)
    private String code;
    
    @Column(nullable = true, length = 1000)
    private String description;
    
    @Column(nullable = true)
    @Enumerated(EnumType.STRING)
    private Article.Type articleType;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parentCategoryId", nullable = true)
    private Category parent;
    
    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Article.Type getArticleType() {
        return articleType;
    }

    public void setArticleType(Article.Type articleType) {
        this.articleType = articleType;
    }

    public Category getParent() {
        return parent;
    }

    public void setParent(Category parent) {
        this.parent = parent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.model.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 * JPA Entity class for Pricelist for agreements. Status is not saved per pricelist, 
 * instead it is calculated when needed based on the validFrom, this in order to 
 * avoid writing a batch job to update statuses daily. 
 * 
 * @author Tommy Berglund
 */
@Entity
@NamedQueries({
    @NamedQuery(name = AgreementPricelist.GET_ALL_BY_AGREEMENT, query = "SELECT p FROM AgreementPricelist p WHERE p.agreement.uniqueId = :agreementUniqueId ORDER BY p.validFrom ASC"),
    @NamedQuery(name = AgreementPricelist.FIND_BY_VALID_FROM_AND_AGREEMENT, query = "SELECT p FROM AgreementPricelist p WHERE p.agreement.uniqueId = :agreementUniqueId AND p.validFrom = :validFrom"),
    @NamedQuery(name = AgreementPricelist.FIND_BY_NUMBER_AND_AGREEMENT, query = "SELECT p FROM AgreementPricelist p WHERE p.agreement.uniqueId = :agreementUniqueId AND p.number = :number"),
    @NamedQuery(name = AgreementPricelist.FIND_BY_PASSED_VALID_FROM_AND_AGREEMENT, query = "SELECT p FROM AgreementPricelist p WHERE p.agreement.uniqueId = :agreementUniqueId AND p.validFrom <= :validFrom ORDER BY p.validFrom DESC"),
    @NamedQuery(name = AgreementPricelist.FIND_BY_NOT_PASSED_VALID_FROM_AND_AGREEMENT, query = "SELECT p FROM AgreementPricelist p WHERE p.agreement.uniqueId = :agreementUniqueId AND p.validFrom > :validFrom ORDER BY p.validFrom DESC")
})
@Table(
    uniqueConstraints = {
        @UniqueConstraint(
            columnNames = {"number", "agreementId"},
            name = "numberIsUniquePerAgreement"
        ),
        @UniqueConstraint(
            columnNames = {"validFrom", "agreementId"},
            name = "validFromIsUniquePerAgreement"
        )
    }
)
public class AgreementPricelist implements Serializable {

    public static final String GET_ALL_BY_AGREEMENT = "HJMT_PRICELIST_GET_ALL_BY_AGREEMENT";
    public static final String FIND_BY_VALID_FROM_AND_AGREEMENT = "HJMT_PRICELIST_FIND_BY_VALID_FROM_AND_AGREEMENT";
    public static final String FIND_BY_NUMBER_AND_AGREEMENT = "HJMT_PRICELIST_FIND_BY_NUMBER_AND_AGREEMENT";
    public static final String FIND_BY_PASSED_VALID_FROM_AND_AGREEMENT = "HJMT_PRICELIST_FIND_BY_PASSED_VALID_FROM_AND_AGREEMENT";
    public static final String FIND_BY_NOT_PASSED_VALID_FROM_AND_AGREEMENT = "HJMT_PRICELIST_FIND_BY_NOT_PASSED_VALID_FROM_AND_AGREEMENT";
    
    public enum Status {
        PAST, CURRENT, FUTURE
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long uniqueId;
    
    @Column(nullable = false)
    private String number;
        
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "agreementId", nullable = false)
    private Agreement agreement;
    
    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    private Date validFrom;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date created;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date lastUpdated;
    
    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Agreement getAgreement() {
        return agreement;
    }

    public void setAgreement(Agreement agreement) {
        this.agreement = agreement;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @PrePersist
    private void prePersist() {
        this.created = new Date();
    }
    
    @PreUpdate
    private void preUpdate() {
        this.lastUpdated = new Date();
    }
    
}
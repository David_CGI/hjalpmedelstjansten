/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.model.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 * Property specific of a <code>Category</code>
 * 
 * @author Tommy Berglund
 */
@Entity
@NamedQueries({
    @NamedQuery(name = CategorySpecificProperty.FIND_BY_CATEGORY, query = "SELECT c FROM CategorySpecificProperty c WHERE c.category.uniqueId = :categoryUniqueId ORDER BY c.name ASC")
})
public class CategorySpecificProperty implements Serializable {

    public static final String FIND_BY_CATEGORY = "HJMT_CATEGORY_SPECIFIC_PROPERTY_FIND_BY_CATEGORY";

    public enum Type {
        TEXTFIELD, 
        DECIMAL, 
        VALUELIST_SINGLE, 
        VALUELIST_MULTIPLE, 
        INTERVAL
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long uniqueId;
    
    @Column(nullable = false)
    private String name;
    
    @Column(nullable = true, length = 500)
    private String description;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "categoryId", nullable = false)
    private Category category;
    
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Type type;

    @OneToMany(
            mappedBy = "categorySpecificProperty",
            cascade = {CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.MERGE},
            fetch = FetchType.EAGER,
            orphanRemoval = true
    )
    private List<CategorySpecificPropertyListValue> categorySpecificPropertyListValues;
    
    public Long getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CategorySpecificPropertyListValue> getCategorySpecificPropertyListValues() {
        return categorySpecificPropertyListValues;
    }

    public void setCategorySpecificPropertyListValues(List<CategorySpecificPropertyListValue> categorySpecificPropertyListValues) {
        this.categorySpecificPropertyListValues = categorySpecificPropertyListValues;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.inera.hjalpmedelstjansten.model.entity.media;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

/**
 *
 * @author tomber
 */
@Entity
@NamedQueries({
    @NamedQuery(name = ArticleMediaVideo.FIND_BY_ARTICLE, query = "SELECT a FROM ArticleMediaVideo a WHERE a.article.uniqueId = :articleUniqueId ORDER BY a.created DESC"),
    @NamedQuery(name = ArticleMediaVideo.FIND_BY_MEDIAVIDEO, query = "SELECT a FROM ArticleMediaVideo a WHERE a.mediaVideo.uniqueId = :mediaUniqueId ORDER BY a.created DESC")
})
public class ArticleMediaVideo extends ArticleMedia {
    
    public static final String FIND_BY_ARTICLE = "HJMT_ARTICLEMEDIAVIDEO_FIND_BY_ARTICLE";
    public static final String FIND_BY_MEDIAVIDEO = "HJMT_ARTICLEMEDIAVIDEO_FIND_BY_MEDIAVIDEO";
            
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mediaVideoId", nullable = false)
    private MediaVideo mediaVideo;

    public MediaVideo getMediaVideo() {
        return mediaVideo;
    }

    public void setMediaVideo(MediaVideo mediaVideo) {
        this.mediaVideo = mediaVideo;
    }

}

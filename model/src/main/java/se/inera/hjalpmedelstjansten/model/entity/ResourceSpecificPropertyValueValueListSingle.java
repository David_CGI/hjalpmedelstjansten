/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.model.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * Answer to list of single type.
 * 
 * @author Tommy Berglund
 */
@Entity
@DiscriminatorValue(value = "LISTSINGLE")
public class ResourceSpecificPropertyValueValueListSingle extends ResourceSpecificPropertyValue {

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "singleCategorySpecificPropertyListValueId", nullable = true)
    private CategorySpecificPropertyListValue value;
    
    public CategorySpecificPropertyListValue getValue() {
        return value;
    }

    public void setValue(CategorySpecificPropertyListValue value) {
        this.value = value;
    }
    
}

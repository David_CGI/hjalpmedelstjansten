/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.model.entity.cv;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

/**
 * JPA Entity class for Counties
 * 
 * @author Tommy Berglund
 */
@Entity
@NamedQueries({
    @NamedQuery(name = CVCounty.FIND_ALL, query = "SELECT c FROM CVCounty c ORDER BY c.name ASC")
})
public class CVCounty implements Serializable {

    public static final String FIND_ALL = "HJMT_CVCOUNTY_FIND_ALL";
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long uniqueId;
    
    @Column(nullable = false, unique = true)
    private String name;
    
    @Column(nullable = false, unique = true)
    private String code;
    
    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean showMunicipalities;
        
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "county")
    @OrderBy("name ASC")
    private List<CVMunicipality> municipalities;
    
    public Long getUniqueId() {
        return uniqueId;
    }
    
    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean getShowMunicipalities() {
        return showMunicipalities;
    }

    public void setShowMunicipalities(boolean showMunicipalities) {
        this.showMunicipalities = showMunicipalities;
    }

    public List<CVMunicipality> getMunicipalities() {
        return municipalities;
    }

    public void setMunicipalities(List<CVMunicipality> municipalities) {
        this.municipalities = municipalities;
    }

}

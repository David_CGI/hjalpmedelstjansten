/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.model.entity.media;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVDocumentType;

/**
 * Media of type document
 * 
 * @author Tommy Berglund
 */
@Entity
@NamedQueries({
    @NamedQuery(name = MediaDocument.FIND_BY_PRODUCT, query = "SELECT m FROM MediaDocument m WHERE m.product.uniqueId = :productUniqueId ORDER BY m.created DESC")
})
public class MediaDocument extends Media {

    public static final String FIND_BY_PRODUCT = "HJMT_MEDIADOCUMENT_FIND_BY_PRODUCT";
    
    public enum FileType {
        PDF, WORD, EXCEL
    }
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "documentTypeId", nullable = true)
    private CVDocumentType documentType;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private FileType fileType;
    
    @Column(nullable = false)
    private String fileName;
        
    @Column(nullable = false)
    private String externalKey;
    
    @Column(nullable = true)
    private String originalUrl;
    
    public CVDocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(CVDocumentType documentType) {
        this.documentType = documentType;
    }

    public FileType getFileType() {
        return fileType;
    }

    public void setFileType(FileType fileType) {
        this.fileType = fileType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getOriginalUrl() {
        return originalUrl;
    }

    public void setOriginalUrl(String originalUrl) {
        this.originalUrl = originalUrl;
    }

    public String getExternalKey() {
        return externalKey;
    }

    public void setExternalKey(String externalKey) {
        this.externalKey = externalKey;
    }
    
}

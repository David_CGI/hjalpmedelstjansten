#!/usr/bin/env bash

H2_DIR="$1"

if [ -z "$H2_DIR" ]; then
    H2_DIR="$(pwd)"/h2
fi

echo "H2_DIR=${H2_DIR}"

docker kill hjmtj-backend
docker rm hjmtj-backend

s2i build https://github.com/openshift/openshift-jee-sample \
-e TZ=Europe/Stockholm \
-e MYSQL_DATASOURCE=MySQLDS \
-e MYSQL_SERVICE_HOST=hjalpmedelstjansten_db-hjmtj_1 \
-e MYSQL_SERVICE_PORT=3306 \
-e MYSQL_DATABASE=hjmtj \
-e MYSQL_USER=test \
-e MYSQL_PASSWORD=test \
\
-e DB_CONNECTION_URL=jdbc:mysql://hjalpmedelstjansten_db-hjmtj_1:3306/hjmtj?rewriteBatchedStatements=true \
-e DB_USERNAME=test \
-e DB_PASSWORD=test \
-e HJMTJ_ENVIRONMENT=local \
-e REDIS_HOST=hjmtj-redis \
-e HJMTJ_SERVICE_BASE_URL=http://localhost/ \
-e ELASTICSEARCH_URL=http://hjmtj-elasticsearch:9200 \
-e ELASTICSEARCH_INDEX_NAME=product_test \
-e ELASTICSEARCH_MIN_SCORE=0.9 \
-e LOAD_SAMPLE_DATA_SERVICE_DISABLED=true \
-e MYSQL_SCHEMA_GENERATION_ACTION=drop-and-create \
-e MEDIA_BASE_URL=https://test-cdn.hjm.aws.ineratest.org/ \
-e MIGRATION_SET_EMAILS=true \
-e USE_GENERATED_VALUE_FOR_PRICE=false \
-e ALLOW_LIST_TESTUSERS=true \
\
-e PRODUCT_URL_HOST=ftp \
-e PRODUCT_URL_USERNAME=user \
-e PRODUCT_URL_PASSWORD=password \
-e PRODUCT_URL_FILE=ISO9999Products_28_20190114_1715.zip \
-e AGREEMENT_URL_FILE=HjalpmedelAgreements_24_20190114_1443.zip \
-e GP_URL_FILE=GPAgreements_22_20190114_1419.zip \
-e ASSORTMENT_URL_FILE=ASSORTMENT_NotVGRInternt_23_20190114_1422.zip \
-e ACTORS_URL_FILE=Actors_25_20190114_1459.zip \
-e AGREEMENT_APPROVERS_URL_FILE=RoleAgreementOwners_26_20190114_1510.zip \
-e ASSORTMENT_OWNERS_URL_FILE=RoleAssortmentOwnerSpecific_27_20190114_1532.zip \
-e SUPPLIERS_PW_URL_FILE=suppliers.pw \
-e CUSTOMERS_PW_URL_FILE=customers.pw \
-e MIGRATION_LOCAL_DS=java:jboss/datasources/ExampleDS2 \
-e H2_SCHEMA_GENERATION_ACTION=update \
-e OPENSHIFT_GEAR_MEMORY_MB=20480 \
-e ARTIFACT_DIR=migration/target \
openshift/wildfly-140-centos7 wildflytest

docker run -p 8080:8080 -p 9990:9990 -v $H2_DIR:/h2 --name hjmtj-backend --network hjalpmedelstjansten_default wildflytest
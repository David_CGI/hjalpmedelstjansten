#!/usr/bin/env bash
mvn clean install -f ../pom.xml -DskipTests=true

mkdir -p ./modules/hjmtj/main
cp ./../web/customization/dev.properties ./modules/hjmtj/main/app.properties
cp ./../web/customization/module.xml ./modules/hjmtj/main/
cp ./../web/customization/elasticsearch_index_source.json ./modules/hjmtj/main/
cp ./../web/customization/texts.properties ./modules/hjmtj/main/
cp ./../web/customization/ValidationMessages.properties ./modules/hjmtj/main/
cp ./../web/customization/shiro.ini ./modules/hjmtj/main/

docker cp ./modules/ hjmtj-backend:/wildfly/
docker cp target/hjmtj.war hjmtj-backend:/wildfly/standalone/deployments
docker cp target/migration.war hjmtj-backend:/wildfly/standalone/deployments

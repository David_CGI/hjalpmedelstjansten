#!/usr/bin/env bash
scriptdir=`dirname "$BASH_SOURCE"`

if ([ -z $1  ] || [ -z $2  ] ); then
    echo 'Usage: run.sh <S3 bucket> <path to upload.lst> [-nc]'
    echo 'eg. run.sh ind-hjmtest-test-cdn ./scripts'
    exit 1
fi
BUCKET=$1
UPLOAD_LIST=$2
NO_CHECK=$3

if ([ -z $NO_CHECK  ]); then
    NO_CHECK="false"
fi

echo 'Setting up environment...'
curl -O https://bootstrap.pypa.io/get-pip.py
python get-pip.py --user
export PATH=~/.local/bin:$PATH
pip install awscli --upgrade --user
export PATH=./.local/bin/aws:$PATH

echo 'Configuring aws credentials...'
aws configure set aws_access_key_id ${AWS_ACCESS_KEY_ID}
aws configure set aws_secret_access_key ${AWS_SECRET_ACCESS_KEY}
aws configure set default.region eu-north-1

echo 'Expected checksum is'
command -v md5sum
if [ $? -ne 0 ]; then
    MD5=$(md5 $UPLOAD_LIST)
else
    MD5=$(md5sum --tag $UPLOAD_LIST)
fi
echo $MD5

echo 'Actual checksum is'
MD5_CHECKSUM=$(cat $UPLOAD_LIST.md5)
echo $MD5_CHECKSUM
if ([ "$MD5_CHECKSUM" = "$MD5"  ]); then
    echo 'MD5 checksum OK'
else
    echo 'MD5 checksum is wrong!'
    exit 1
fi

echo 'Starting upload!'
EXPECTED=$(wc -l < $UPLOAD_LIST)
echo 'Number of entries =' `expr $EXPECTED - 1`
COUNT=0;

while IFS=';' read -r url key filename; do
    $scriptdir/upload_file.sh "$NO_CHECK" "$BUCKET" "$key" "$url" &

    COUNT=`expr $COUNT + 1`
    RATE=`expr $COUNT / $SECONDS`
    if [[ $(( $COUNT % 1000 )) == 0 ]]; then
        echo $COUNT '/' $EXPECTED '(' $RATE 'media/second)'
    fi
done < $UPLOAD_LIST

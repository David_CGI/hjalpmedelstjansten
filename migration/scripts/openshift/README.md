# Instructions
1. Configure aws locally using 'aws configure'.

2. Execute './copy-to-openshift.sh <pod name>'.

3. Inside Openshift pod terminal, execute './run.sh <bucket> [-nc]'.
#!/usr/bin/env bash

if ([ -z $1  ]); then
    echo 'missing arg:'
    echo 'copy-to-openshift.sh <destination pod name>'
    exit 1
fi

echo 'This script copies the stuff you need into the openshift pod'
echo -n 'Would you like to copy your local ~/.aws/credentials to openshift? (Y/N)'
read answer
if [ "$answer" != "${answer#[Yy]}" ] ;then
    cat ~/.aws/credentials
    oc cp ~/.aws/ $1:/home/jboss/.aws/
fi

echo -n "Do you wish to generate new upload.lst file by reading from H2? "
read answer
if [ "$answer" != "${answer#[Yy]}" ] ;then
    MEDIA_SQL_SCRIPT=$(pwd)/upload.sql
    echo 'Using '${MEDIA_SQL_SCRIPT}
    ./../runsql.sh ${MEDIA_SQL_SCRIPT} | sed -e 's|--> ||' | grep -e '^https' > upload.lst
    md5 ./upload.lst > upload.lst.md5
fi

echo -n "Do you wish to copy upload.lst to openshift? "
read answer
if [ "$answer" != "${answer#[Yy]}" ] ;then
    oc cp ./upload.lst $1:/home/jboss/upload.lst
    oc cp ./upload.lst.md5 $1:/home/jboss/upload.lst.md5
fi


echo -n "Do you wish to upload diff[upload.lst, upload.lst.bak] to openshift? "
read answer
if [ "$answer" != "${answer#[Yy]}" ] ;then
    java -jar ./../../diff/target/diff.jar ./upload.lst ./upload.lst.bak > diff.lst
    md5 ./diff.lst > diff.lst.md5
    oc cp ./diff.lst $1:/home/jboss/upload.lst
    oc cp ./diff.lst.md5 $1:/home/jboss/upload.lst.md5
fi

oc cp ./upload.sh $1:/home/jboss/upload.sh
oc cp ./upload_file.sh $1:/home/jboss/upload_file.sh
oc cp ./run.sh $1:/home/jboss/run.sh

#!/usr/bin/env bash
param=$1
bucket=$2
key=$3
url=$4

if [ "$param" = "-nc" ]; then
   curl -sNL "$url" | aws s3 cp - s3://$bucket/"$key"
   echo 'UPLOAD' ${BASE_URL}$key
else
   RESPONSE=$(aws s3api head-object --bucket $bucket --key "$key")
   EXISTS=$?
   CONTENT_LENGTH=$(echo $RESPONSE | python -c "import json,sys;obj=json.load(sys.stdin);print obj['ContentLength'];")

   if ([ $EXISTS -ne 0 ] || [ "$CONTENT_LENGTH" = "0" ]); then
        curl -sNL "$url" | aws s3 cp - s3://$bucket/"$key"
        echo 'UPLOAD' ${BASE_URL}$key
    else
        echo 'SKIP' ${BASE_URL}$key
    fi
fi
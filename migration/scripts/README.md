#README
Execute run.sh <input file> <output path> to save media to disk.

Execute upload.sh <input file> to upload to s3. Note: you must have awscli configured (see s3 api documentation for how to).
#!/usr/bin/env bash
MEDIA_SQL_SCRIPT=$(pwd)/media.sql
echo 'Using '${MEDIA_SQL_SCRIPT}
./runsql.sh ${MEDIA_SQL_SCRIPT} | sed -e 's|--> ||' | grep -e '^https' > $1
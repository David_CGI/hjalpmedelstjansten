#!/usr/bin/env bash

if ([ -z $1  ] || [ -z $2  ]); then
    echo 'missing args:'
    echo 'run.sh <path to media.lst> <output dir>'
    exit 1
fi

echo -n "Do you wish to generate new *.lst files by reading from H2? "
read answer
if [ "$answer" != "${answer#[Yy]}" ] ;then
    ./generate.sh $1
fi

echo -n "Do you wish to download media to $1? "
read answer
if [ "$answer" != "${answer#[Yy]}" ] ;then
    ./download.sh $1 $2
    ./archive.sh $2
fi


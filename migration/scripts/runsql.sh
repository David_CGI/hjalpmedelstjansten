#!/usr/bin/env bash
SCRIPT_DIR=$(dirname "$0")
H2_DIR=$SCRIPT_DIR/../h2/
H2_JDBC_URL='jdbc:h2:'$H2_DIR'/migration'
TARGET_DIR=$SCRIPT_DIR/../target/

mkdir -p $TARGET_DIR
wget -nc -nd http://repo2.maven.org/maven2/com/h2database/h2/1.4.197/h2-1.4.197.jar --directory-prefix $TARGET_DIR
java -cp $TARGET_DIR/h2-*.jar org.h2.tools.RunScript -user sa -password sa -url ${H2_JDBC_URL} -showResults -script "$1"

#!/usr/bin/env bash
VISUERA_BASEPATH=https://webcache.visuera.com/cache_vgregion?ref=

echo '=== Started downloading '$1'.lst ==='
EXPECTED=$(wc -l < $1)
echo 'Number of entries =' `expr $EXPECTED - 1`

COUNT=0;
while read -r url filename tail; do
    outdir=$2/${url/$VISUERA_BASEPATH}
    mkdir -p ${outdir}
    wget -c -q -nc -H -T 0 -O "$outdir/$filename" "$url"

    COUNT=`expr $COUNT + 1`
    if [[ $(( $COUNT % 1000 )) == 0 ]]; then
        echo $COUNT '/' $EXPECTED
    fi
done <$1

ACTUAL=$(ls -1 ./$2/*/* | wc -l)
echo 'Successfully downloaded =' $ACTUAL
echo '=== Finished downloading '$1'.lst ! ==='

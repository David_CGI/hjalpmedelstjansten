package se.inera.hjmtj.migration.infra.jpa.entities;

import org.junit.Test;
import se.inera.hjmtj.migration.domain.model.product.entities.Product;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductCategorySpecificProperty;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductDocument;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductFile;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductImage;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class ProductAPINodeTest {

    @Test
    public void deserializeMedia() {
        String xml = "<Product id=\"5019\">\n" +
                "      <Identifier>c,c54911e6-47bb-4780-aff9-a3d300b77590,Product</Identifier>\n" +
                "      <Timestamp>2018-02-27T11:20:51</Timestamp>\n" +
                "      <CategoryId>ISO9999_092409</CategoryId>\n" +
                "      <CategoryPath>Produkt\\Vara\\ISO-klassificerade hjälpmedel (ISO 9999)\\Hjälpmedelsområden\\09 Personlig vård och skydd\\0924 Urinavledare\\092409 Urindroppsamlare för män</CategoryPath>\n" +
                "      <_x0030_016fec8f85a4547bfa729f11547f08a>false</_x0030_016fec8f85a4547bfa729f11547f08a>\n" +
                "      <_x0031_1fd813b4adb47f58d1d2e4bf67ce658>92409</_x0031_1fd813b4adb47f58d1d2e4bf67ce658>\n" +
                "      <_x0031_7fa973736524d658b7b944561195907>\n" +
                "        <CEmark>true</CEmark>\n" +
                "        <isoattr_3007>Medicintekniska direktivet 93/42/EEG</isoattr_3007>\n" +
                "        <Directive idref=\"2357\" />\n" +
                "      </_x0031_7fa973736524d658b7b944561195907>\n" +
                "      <_x0033_8071ade9e5b4661b968fd00c68c6e2e>false</_x0033_8071ade9e5b4661b968fd00c68c6e2e>\n" +
                "      <_x0037_e33e6739e7147f3ba609f1b28dcec42>\n" +
                "        <_x0031_1e8176862574ba9bb5cedab346dad93>30</_x0031_1e8176862574ba9bb5cedab346dad93>\n" +
                "      </_x0037_e33e6739e7147f3ba609f1b28dcec42>\n" +
                "      <BasedOnTemplateProduct idref=\"5020\" />\n" +
                "      <Bild>\n" +
                "        <OriginalContentUrl>https://webcache.visuera.com/cache_vgregion?ref=c%2cbb281c16-9358-443d-9eff-a479010c98d8%2cBlobData&amp;u=8D2441C88597B80</OriginalContentUrl>\n" +
                "        <ScaledImageUrl maxwidth=\"80\" maxheight=\"80\">https://webcache.visuera.com/cache_vgregion?ref=c%2cbb281c16-9358-443d-9eff-a479010c98d8%2cBlobData&amp;u=8D2441C88597B80&amp;ts=80</ScaledImageUrl>\n" +
                "        <ScaledImageUrl maxwidth=\"200\" maxheight=\"200\">https://webcache.visuera.com/cache_vgregion?ref=c%2cbb281c16-9358-443d-9eff-a479010c98d8%2cBlobData&amp;u=8D2441C88597B80&amp;ts=200</ScaledImageUrl>\n" +
                "        <ScaledImageUrl maxwidth=\"73\">https://webcache.visuera.com/cache_vgregion?ref=c%2cbb281c16-9358-443d-9eff-a479010c98d8%2cBlobData&amp;u=8D2441C88597B80&amp;ts=73</ScaledImageUrl>\n" +
                "        <ScaledImageUrl maxwidth=\"740\">https://webcache.visuera.com/cache_vgregion?ref=c%2cbb281c16-9358-443d-9eff-a479010c98d8%2cBlobData&amp;u=8D2441C88597B80&amp;ts=740</ScaledImageUrl>\n" +
                "        <ScaledImageUrl maxwidth=\"89\" maxheight=\"83\">https://webcache.visuera.com/cache_vgregion?ref=c%2cbb281c16-9358-443d-9eff-a479010c98d8%2cBlobData&amp;u=8D2441C88597B80&amp;ts=83</ScaledImageUrl>\n" +
                "      </Bild>\n" +
                "      <Brand>Conveen</Brand>\n" +
                "      <Classification idref=\"354\" />\n" +
                "      <CreatedTime>2014-10-29T11:07:57</CreatedTime>\n" +
                "      <Documents>\n" +
                "        <Description>Bruksanvisning</Description>\n" +
                "        <DocumentType idref=\"175\" />\n" +
                "        <File>\n" +
                "          <OriginalContentUrl>https://webcache.visuera.com/cache_vgregion?ref=c%2cd8bc3e36-f629-4b2f-8707-a4ce00e46011%2cBlobData&amp;u=8D286D328990680</OriginalContentUrl>\n" +
                "          <ScaledImageUrl maxwidth=\"80\" maxheight=\"80\">https://webcache.visuera.com/cache_vgregion?ref=c%2cd8bc3e36-f629-4b2f-8707-a4ce00e46011%2cBlobData&amp;u=8D286D328990680&amp;ts=80</ScaledImageUrl>\n" +
                "          <ScaledImageUrl maxwidth=\"200\" maxheight=\"200\">https://webcache.visuera.com/cache_vgregion?ref=c%2cd8bc3e36-f629-4b2f-8707-a4ce00e46011%2cBlobData&amp;u=8D286D328990680&amp;ts=200</ScaledImageUrl>\n" +
                "          <ScaledImageUrl maxwidth=\"73\">https://webcache.visuera.com/cache_vgregion?ref=c%2cd8bc3e36-f629-4b2f-8707-a4ce00e46011%2cBlobData&amp;u=8D286D328990680&amp;ts=73</ScaledImageUrl>\n" +
                "          <ScaledImageUrl maxwidth=\"740\">https://webcache.visuera.com/cache_vgregion?ref=c%2cd8bc3e36-f629-4b2f-8707-a4ce00e46011%2cBlobData&amp;u=8D286D328990680&amp;ts=740</ScaledImageUrl>\n" +
                "          <ScaledImageUrl maxwidth=\"89\" maxheight=\"83\">https://webcache.visuera.com/cache_vgregion?ref=c%2cd8bc3e36-f629-4b2f-8707-a4ce00e46011%2cBlobData&amp;u=8D286D328990680&amp;ts=83</ScaledImageUrl>\n" +
                "        </File>\n" +
                "      </Documents>\n" +
                "      <Documents>\n" +
                "        <Description>Produktguide Inkontinens</Description>\n" +
                "        <DocumentType idref=\"171\" />\n" +
                "        <File>\n" +
                "          <OriginalContentUrl>https://webcache.visuera.com/cache_vgregion?ref=c%2c68afdcf4-4f35-4534-a745-a85d00b1e0e6%2cBlobData&amp;u=8D5529767EE4100</OriginalContentUrl>\n" +
                "          <ScaledImageUrl maxwidth=\"80\" maxheight=\"80\">https://webcache.visuera.com/cache_vgregion?ref=c%2c68afdcf4-4f35-4534-a745-a85d00b1e0e6%2cBlobData&amp;u=8D5529767EE4100&amp;ts=80</ScaledImageUrl>\n" +
                "          <ScaledImageUrl maxwidth=\"200\" maxheight=\"200\">https://webcache.visuera.com/cache_vgregion?ref=c%2c68afdcf4-4f35-4534-a745-a85d00b1e0e6%2cBlobData&amp;u=8D5529767EE4100&amp;ts=200</ScaledImageUrl>\n" +
                "          <ScaledImageUrl maxwidth=\"73\">https://webcache.visuera.com/cache_vgregion?ref=c%2c68afdcf4-4f35-4534-a745-a85d00b1e0e6%2cBlobData&amp;u=8D5529767EE4100&amp;ts=73</ScaledImageUrl>\n" +
                "          <ScaledImageUrl maxwidth=\"740\">https://webcache.visuera.com/cache_vgregion?ref=c%2c68afdcf4-4f35-4534-a745-a85d00b1e0e6%2cBlobData&amp;u=8D5529767EE4100&amp;ts=740</ScaledImageUrl>\n" +
                "          <ScaledImageUrl maxwidth=\"89\" maxheight=\"83\">https://webcache.visuera.com/cache_vgregion?ref=c%2c68afdcf4-4f35-4534-a745-a85d00b1e0e6%2cBlobData&amp;u=8D5529767EE4100&amp;ts=83</ScaledImageUrl>\n" +
                "        </File>\n" +
                "      </Documents>\n" +
                "      <Documents>\n" +
                "        <Description>Urinläckage hos män broschyr</Description>\n" +
                "        <DocumentType idref=\"171\" />\n" +
                "        <File>\n" +
                "          <OriginalContentUrl>https://webcache.visuera.com/cache_vgregion?ref=c%2cb99e8e69-faf8-4285-834b-a85d00b0f607%2cBlobData&amp;u=8D55296F0201680</OriginalContentUrl>\n" +
                "          <ScaledImageUrl maxwidth=\"80\" maxheight=\"80\">https://webcache.visuera.com/cache_vgregion?ref=c%2cb99e8e69-faf8-4285-834b-a85d00b0f607%2cBlobData&amp;u=8D55296F0201680&amp;ts=80</ScaledImageUrl>\n" +
                "          <ScaledImageUrl maxwidth=\"200\" maxheight=\"200\">https://webcache.visuera.com/cache_vgregion?ref=c%2cb99e8e69-faf8-4285-834b-a85d00b0f607%2cBlobData&amp;u=8D55296F0201680&amp;ts=200</ScaledImageUrl>\n" +
                "          <ScaledImageUrl maxwidth=\"73\">https://webcache.visuera.com/cache_vgregion?ref=c%2cb99e8e69-faf8-4285-834b-a85d00b0f607%2cBlobData&amp;u=8D55296F0201680&amp;ts=73</ScaledImageUrl>\n" +
                "          <ScaledImageUrl maxwidth=\"740\">https://webcache.visuera.com/cache_vgregion?ref=c%2cb99e8e69-faf8-4285-834b-a85d00b0f607%2cBlobData&amp;u=8D55296F0201680&amp;ts=740</ScaledImageUrl>\n" +
                "          <ScaledImageUrl maxwidth=\"89\" maxheight=\"83\">https://webcache.visuera.com/cache_vgregion?ref=c%2cb99e8e69-faf8-4285-834b-a85d00b0f607%2cBlobData&amp;u=8D55296F0201680&amp;ts=83</ScaledImageUrl>\n" +
                "        </File>\n" +
                "      </Documents>\n" +
                "      <Documents>\n" +
                "        <Description>Produktblad</Description>\n" +
                "        <DocumentType idref=\"171\" />\n" +
                "        <File>\n" +
                "          <OriginalContentUrl>https://webcache.visuera.com/cache_vgregion?ref=c%2cf2da483e-28b0-4096-87db-a4ce00e44121%2cBlobData&amp;u=8D286D318812700</OriginalContentUrl>\n" +
                "          <ScaledImageUrl maxwidth=\"80\" maxheight=\"80\">https://webcache.visuera.com/cache_vgregion?ref=c%2cf2da483e-28b0-4096-87db-a4ce00e44121%2cBlobData&amp;u=8D286D318812700&amp;ts=80</ScaledImageUrl>\n" +
                "          <ScaledImageUrl maxwidth=\"200\" maxheight=\"200\">https://webcache.visuera.com/cache_vgregion?ref=c%2cf2da483e-28b0-4096-87db-a4ce00e44121%2cBlobData&amp;u=8D286D318812700&amp;ts=200</ScaledImageUrl>\n" +
                "          <ScaledImageUrl maxwidth=\"73\">https://webcache.visuera.com/cache_vgregion?ref=c%2cf2da483e-28b0-4096-87db-a4ce00e44121%2cBlobData&amp;u=8D286D318812700&amp;ts=73</ScaledImageUrl>\n" +
                "          <ScaledImageUrl maxwidth=\"740\">https://webcache.visuera.com/cache_vgregion?ref=c%2cf2da483e-28b0-4096-87db-a4ce00e44121%2cBlobData&amp;u=8D286D318812700&amp;ts=740</ScaledImageUrl>\n" +
                "          <ScaledImageUrl maxwidth=\"89\" maxheight=\"83\">https://webcache.visuera.com/cache_vgregion?ref=c%2cf2da483e-28b0-4096-87db-a4ce00e44121%2cBlobData&amp;u=8D286D318812700&amp;ts=83</ScaledImageUrl>\n" +
                "        </File>\n" +
                "      </Documents>\n" +
                "      <Images />\n" +
                "      <IsCustom>false</IsCustom>\n" +
                "      <Manufacture>Coloplast</Manufacture>\n" +
                "      <OrderUnit idref=\"2029\" />\n" +
                "      <Package1>30</Package1>\n" +
                "      <Package2>480</Package2>\n" +
                "      <PackagePallet>9600</PackagePallet>\n" +
                "      <ReferencedCategory idref=\"353\" />\n" +
                "      <Supplier idref=\"2079\" />\n" +
                "      <SupplierProductName>Conveen urindroppsamlare med häftremsa, omkr 90 mm, PVC-fri</SupplierProductName>\n" +
                "      <SupplierProductNumber>051300</SupplierProductNumber>\n" +
                "      <UNSPSC>42142710</UNSPSC>\n" +
                "      <UpdatedTime>2018-02-27T11:20:51</UpdatedTime>\n" +
                "      <VGRProdNo>105978365</VGRProdNo>\n" +
                "      <d0823a697d0f40dc97c13f2bda60c6d9>Urindroppsamlare för män vid urininkontinens. Kopplas till en tömbar urinuppsamlingspåse. Fixeras med hjälp av häftstrip. PVC-fri.</d0823a697d0f40dc97c13f2bda60c6d9>\n" +
                "      <isoattr_22092 idref=\"5002\" />\n" +
                "      <isoattr_22099 idref=\"5009\" />\n" +
                "      <isoattr_22100>90</isoattr_22100>\n" +
                "      <isoattr_22101 idref=\"5004\" />\n" +
                "      <isoattr_22102>105</isoattr_22102>\n" +
                "    </Product>";

        Product result = null;
        try {
            result = getXmlProduct(xml);
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        assertNotNull(result.getMainImage());

        assertNotNull(result.getImages());
        assertEquals(0, result.getImages()
                              .size());
        for (ProductImage image : result.getImages()) {
            assertNull(image.getAltText());
            assertNull(image.getFile());
        }

        assertNotNull(result.getDocuments());
        assertEquals(4, result.getDocuments()
                              .size());
        for (ProductDocument document : result.getDocuments()) {
            assertNotNull(document.getFileDescription());
            assertNotNull(document.getFile());
        }
    }

    @Test
    public void deserializeWithCe() {
        String xml = "<Product id=\"2572\">\n" +
                "    <Identifier>c,7fb816e3-9195-4893-a110-a0fb00fa4b54,Product</Identifier>\n" +
                "    <Timestamp>2015-03-24T18:19:20</Timestamp>\n" +
                "    <CategoryId>ISO9999_061209</CategoryId>\n" +
                "    <CategoryPath>Produkt\\Vara\\ISO-klassificerade hjälpmedel (ISO 9999)\\Hjälpmedelsområden\\06 Ortoser och proteser\\0612 Nedre extremitetsortoser\\061209 Knäortoser</CategoryPath>\n" +
                "    <_x0031_7fa973736524d658b7b944561195907>\n" +
                "        <CEmark>true</CEmark>\n" +
                "        <isoattr_3007>Medicintekniska direktivet 93/42/EEG</isoattr_3007>\n" +
                "        <Directive idref=\"2357\" />\n" +
                "        <isoattr_3008>Annan standard</isoattr_3008>\n" +
                "        <Standard idref=\"2359\" />\n" +
                "    </_x0031_7fa973736524d658b7b944561195907>\n" +
                "    <ArtikelBen>VÄ/XL Knä Genum T3 Stab. Lång</ArtikelBen>\n" +
                "    <BasedOnTemplateProduct idref=\"2433\" />\n" +
                "    <Bild>\n" +
                "        <OriginalContentUrl>https://webcache.visuera.com/cache_vgregion?ref=c%2c71b9b77b-98a2-448a-969e-a465012df0d4%2cBlobData&amp;u=8D234762E494400</OriginalContentUrl>\n" +
                "        <ScaledImageUrl maxwidth=\"80\" maxheight=\"80\">https://webcache.visuera.com/cache_vgregion?ref=c%2c71b9b77b-98a2-448a-969e-a465012df0d4%2cBlobData&amp;u=8D234762E494400&amp;ts=80</ScaledImageUrl>\n" +
                "        <ScaledImageUrl maxwidth=\"200\" maxheight=\"200\">https://webcache.visuera.com/cache_vgregion?ref=c%2c71b9b77b-98a2-448a-969e-a465012df0d4%2cBlobData&amp;u=8D234762E494400&amp;ts=200</ScaledImageUrl>\n" +
                "        <ScaledImageUrl maxwidth=\"73\">https://webcache.visuera.com/cache_vgregion?ref=c%2c71b9b77b-98a2-448a-969e-a465012df0d4%2cBlobData&amp;u=8D234762E494400&amp;ts=73</ScaledImageUrl>\n" +
                "        <ScaledImageUrl maxwidth=\"740\">https://webcache.visuera.com/cache_vgregion?ref=c%2c71b9b77b-98a2-448a-969e-a465012df0d4%2cBlobData&amp;u=8D234762E494400&amp;ts=740</ScaledImageUrl>\n" +
                "        <ScaledImageUrl maxwidth=\"89\" maxheight=\"83\">https://webcache.visuera.com/cache_vgregion?ref=c%2c71b9b77b-98a2-448a-969e-a465012df0d4%2cBlobData&amp;u=8D234762E494400&amp;ts=83</ScaledImageUrl>\n" +
                "    </Bild>\n" +
                "    <Brand>DeRoyal</Brand>\n" +
                "    <Classification idref=\"219\" />\n" +
                "    <CreatedTime>2012-10-31T15:11:18</CreatedTime>\n" +
                "    <Images>\n" +
                "        <File>\n" +
                "            <OriginalContentUrl>https://webcache.visuera.com/cache_vgregion?ref=c%2c71b9b77b-98a2-448a-969e-a465012df0d4%2cBlobData&amp;u=8D234762E494400</OriginalContentUrl>\n" +
                "            <ScaledImageUrl maxwidth=\"80\" maxheight=\"80\">https://webcache.visuera.com/cache_vgregion?ref=c%2c71b9b77b-98a2-448a-969e-a465012df0d4%2cBlobData&amp;u=8D234762E494400&amp;ts=80</ScaledImageUrl>\n" +
                "            <ScaledImageUrl maxwidth=\"200\" maxheight=\"200\">https://webcache.visuera.com/cache_vgregion?ref=c%2c71b9b77b-98a2-448a-969e-a465012df0d4%2cBlobData&amp;u=8D234762E494400&amp;ts=200</ScaledImageUrl>\n" +
                "            <ScaledImageUrl maxwidth=\"73\">https://webcache.visuera.com/cache_vgregion?ref=c%2c71b9b77b-98a2-448a-969e-a465012df0d4%2cBlobData&amp;u=8D234762E494400&amp;ts=73</ScaledImageUrl>\n" +
                "            <ScaledImageUrl maxwidth=\"740\">https://webcache.visuera.com/cache_vgregion?ref=c%2c71b9b77b-98a2-448a-969e-a465012df0d4%2cBlobData&amp;u=8D234762E494400&amp;ts=740</ScaledImageUrl>\n" +
                "            <ScaledImageUrl maxwidth=\"89\" maxheight=\"83\">https://webcache.visuera.com/cache_vgregion?ref=c%2c71b9b77b-98a2-448a-969e-a465012df0d4%2cBlobData&amp;u=8D234762E494400&amp;ts=83</ScaledImageUrl>\n" +
                "        </File>\n" +
                "    </Images>\n" +
                "    <IsCustom>false</IsCustom>\n" +
                "    <OrderUnit idref=\"2029\" />\n" +
                "    <Package1>1</Package1>\n" +
                "    <ReferencedCategory idref=\"217\" />\n" +
                "    <Supplier idref=\"2038\" />\n" +
                "    <SupplierProductName>Genum Stab. lång TriTex Vä XL</SupplierProductName>\n" +
                "    <SupplierProductNumber>EU04131100709</SupplierProductNumber>\n" +
                "    <UpdatedTime>2015-03-24T18:19:20</UpdatedTime>\n" +
                "    <VGRMinOrderQty>1</VGRMinOrderQty>\n" +
                "    <VGRProdNo>103858577</VGRProdNo>\n" +
                "    <d0823a697d0f40dc97c13f2bda60c6d9>En anatomiskt designad knäortos med lateral pelott för patella-stabilitet. Halvelastiska band erbjuder goda justeringsmöjligheter. Två flexibla metallskenor ger lätt medial/lateral stabilisering.</d0823a697d0f40dc97c13f2bda60c6d9>\n" +
                "</Product>\n";


        Product result = null;
        try {
            result = getXmlProduct(xml);
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        assertNotNull(result.getProductMarking());
        assertNotNull(result.getProductMarking()
                            .getCeMark());
        assertNotNull(result.getProductMarking()
                            .getIsoattr_3007());
        assertNotNull(result.getProductMarking()
                            .getIsoattr_3008());

        // TODO we cannot test refs like this in a unit test
//        assertNotNull(result.getProductMarking().getProductMarkingDirective());
//        assertNotNull(result.getProductMarking().getProductMarkingDirective().getCode());
//        assertNotNull(result.getProductMarking().getProductMarkingStandard());
//        assertNotNull(result.getProductMarking().getProductMarkingStandard().getCode());
    }


    @Test
    public void deserialize() {
        String xml = " <Product id=\"177931\">\n" +
                "      <Identifier>c,0d093566-46b9-4026-b97b-a46600012bc6,Product</Identifier>\n" +
                "      <Timestamp>2016-02-17T16:12:32</Timestamp>\n" +
                "      <CategoryId>ISO9999_122203</CategoryId>\n" +
                "      <CategoryPath>Produkt\\Vara\\ISO-klassificerade hjälpmedel (ISO 9999)\\Hjälpmedelsområden\\12 Förflyttning\\1222 Manuella rullstolar\\122203 Manuella tvåhjulsdrivna rullstolar</CategoryPath>\n" +
                "      <_x0031_7fa973736524d658b7b944561195907 />\n" +
                "      <Brand>Gipsbenstöd</Brand>\n" +
                "      <Classification idref=\"703\" />\n" +
                "      <CreatedTime>2015-03-25T00:04:16</CreatedTime>\n" +
                "      <IsTemplateProduct>true</IsTemplateProduct>\n" +
                "      <OrderUnit idref=\"2029\" />\n" +
                "      <ReferencedCategory idref=\"1621\" />\n" +
                "      <Supplier idref=\"2192\" />\n" +
                "      <SupplierProductName>Benstöd gipsbenstöd</SupplierProductName>\n" +
                "      <SupplierProductNumber>11</SupplierProductNumber>\n" +
                "      <UpdatedTime>2016-02-17T16:12:32</UpdatedTime>\n" +
                "      <VGRProdNo>108058157</VGRProdNo>\n" +
                "      <d0823a697d0f40dc97c13f2bda60c6d9>Benstöd för gipsat ben i högläge, kraftigt i björkfanér m PT vändbar för hö/vä, fästs i ryggrören på rullstol m svenskremmar, 4 storlekar. Dyna läggs ovanpå sittytan.</d0823a697d0f40dc97c13f2bda60c6d9>\n" +
                "      <isoattr_22238 />\n" +
                "      <isoattr_22240 />\n" +
                "      <isoattr_22242 />\n" +
                "      <isoattr_22244 />\n" +
                "      <isoattr_22245 />\n" +
                "      <isoattr_22246 />\n" +
                "    </Product>";


        Product result = null;
        try {
            result = getXmlProduct(xml);
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        // All category-specific properties on this product have no value
        long count = result.getCategorySpecificProperties()
                           .stream()
                           .filter(ProductCategorySpecificProperty::isValuePresent)
                           .count();

        assertEquals(0, count);
        assertEquals("Manuella tvåhjulsdrivna rullstolar", result.getXmlCategories()
                                                                 .leaf()
                                                                 .getName());
    }

    private Product getXmlProduct(String xml) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Product.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        Product result = (Product) jaxbUnmarshaller.unmarshal(new StringReader(xml));

        assertNotNull(result);
        return result;
    }

    @Test
    public void deserializeWithIntervalDecimalValue() {
        String xml = " <Product id=\"177931\">\n" +
                "      <Identifier>c,0d093566-46b9-4026-b97b-a46600012bc6,Product</Identifier>\n" +
                "      <Timestamp>2016-02-17T16:12:32</Timestamp>\n" +
                "      <CategoryId>ISO9999_122203</CategoryId>\n" +
                "      <CategoryPath>Produkt\\Vara\\ISO-klassificerade hjälpmedel (ISO 9999)\\Hjälpmedelsområden\\12 Förflyttning\\1222 Manuella rullstolar\\122203 Manuella tvåhjulsdrivna rullstolar</CategoryPath>\n" +
                "      <_x0031_7fa973736524d658b7b944561195907 />\n" +
                "      <Brand>Gipsbenstöd</Brand>\n" +
                "      <Classification idref=\"703\" />\n" +
                "      <CreatedTime>2015-03-25T00:04:16</CreatedTime>\n" +
                "      <IsTemplateProduct>true</IsTemplateProduct>\n" +
                "      <OrderUnit idref=\"2029\" />\n" +
                "      <ReferencedCategory idref=\"1621\" />\n" +
                "      <Supplier idref=\"2192\" />\n" +
                "      <SupplierProductName>Benstöd gipsbenstöd</SupplierProductName>\n" +
                "      <SupplierProductNumber>11</SupplierProductNumber>\n" +
                "      <UpdatedTime>2016-02-17T16:12:32</UpdatedTime>\n" +
                "      <VGRProdNo>108058157</VGRProdNo>\n" +
                "      <d0823a697d0f40dc97c13f2bda60c6d9>Benstöd för gipsat ben i högläge, kraftigt i björkfanér m PT vändbar för hö/vä, fästs i ryggrören på rullstol m svenskremmar, 4 storlekar. Dyna läggs ovanpå sittytan.</d0823a697d0f40dc97c13f2bda60c6d9>\n" +
                "      <isoattr_22238 />\n" +
                "      <isoattr_22240 />\n" +
                "      <isoattr_22242><Min>99.9</Min><Max>155.1</Max></isoattr_22242>\n" +
                "      <isoattr_22244 />\n" +
                "      <isoattr_22245 />\n" +
                "      <isoattr_22246 />\n" +
                "    </Product>";


        Product result = null;
        try {
            result = getXmlProduct(xml);
        } catch (JAXBException e) {
            e.printStackTrace();
        }

        List<ProductCategorySpecificProperty> values = result.getCategorySpecificProperties()
                                                             .stream()
                                                             .filter(ProductCategorySpecificProperty::isValuePresent)
                                                             .collect(Collectors.toList());
        assertEquals(1, values.size());

        ProductCategorySpecificProperty firstValue = values.get(0);
        assertEquals(new BigDecimal("99.9"), firstValue.getIntervalDecimalValue()
                                                       .min());
        assertEquals(new BigDecimal("155.1"), firstValue.getIntervalDecimalValue()
                                                        .max());
    }

}
package se.inera.hjmtj.migration.httpclient;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.IntStream;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.nio.reactor.IOReactorException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class HttpClientConfigTest {
	private HttpClientConfig httpConfig;
	private AsyncClientConfig asyncConfig;
	private static final String USER_AGENT = "Mozilla/5.0";
	private static final String mediaUrl_1 = "https://webcache.visuera.com/cache_vgregion?ref=c%2c08b8448e-d05f-4d5c-93b6-9fff00e0fe18%2cBlobData&u=8D0C00FB3A1AE80";
	private static final String mediaUrl_2 = "https://webcache.visuera.com/cache_vgregion?ref=c%2cb61d83f0-d3cf-4d75-ac04-9fff00e0fe18%2cBlobData&u=8D0C00FB3A1AE80";
	private static final String mediaUrl_3 = "https://webcache.visuera.com/cache_vgregion?ref=c%2caa12f208-1820-4a99-a0bc-9fff00e0fdff%2cBlobData&u=8D0C00FB2708180";
	private static final String mediaUrl_4 = "https://webcache.visuera.com/cache_vgregion?ref=c%2c294bdf7c-7f44-493a-91f0-a4640154b406%2cBlobData&u=8D233C0BA992F80";
	private static final String mediaUrl_5 = "https://webcache.visuera.com/cache_vgregion?ref=c%2c33ab50ad-df6b-4d60-8201-a4640154c5f4%2cBlobData&u=8D233C0C4229780";
	private static final String mediaUrl_6 = "https://webcache.visuera.com/cache_vgregion?ref=c%2c03c81d19-26c0-434d-8b88-a465012db98d%2cBlobData&u=8D2347612DE3900";
	private static final String mediaUrl_7 = "https://webcache.visuera.com/cache_vgregion?ref=c%2c34b34170-31eb-4aab-b872-a465012dc7aa%2cBlobData&u=8D23476196CB080";
	private static final String mediaUrl_8 = "https://webcache.visuera.com/cache_vgregion?ref=c%2c24fb1859-a699-49e9-861a-a465012db3aa%2cBlobData&u=8D234760F4AB200";
	private static final String mediaUrl_9 = "https://webcache.visuera.com/cache_vgregion?ref=c%2c24fb1859-a699-49e9-861a-a465012db3aa%2cBlobData&u=8D234760F4AB200";
	private static final String mediaUrl_10 = "https://webcache.visuera.com/cache_vgregion?ref=c%2c82fddf9f-7a88-4f72-93d7-9fff00fc7085%2cBlobData&u=8D0C00FB69C9F00";
	private static final String[] listOfUrls = new String[] { mediaUrl_1, mediaUrl_2, mediaUrl_3, mediaUrl_4, mediaUrl_5, mediaUrl_6,
			mediaUrl_7, mediaUrl_8, mediaUrl_9, mediaUrl_10 };

	@Before
	public void init() throws IOReactorException {
		httpConfig = new HttpClientConfig();
		httpConfig.init();
	}

	@Test
	public void whenUseSSLWithHttpClient_thenCorrect() throws Exception {
		CloseableHttpClient httpClient = httpConfig.httpClient();
		HttpGet httpGet = new HttpGet(
				"https://webcache.visuera.com/cache_vgregion?ref=c%2c08b8448e-d05f-4d5c-93b6-9fff00e0fe18%2cBlobData&u=8D0C00FB3A1AE80");
		httpGet.addHeader("User-Agent", USER_AGENT);
		CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
		Assert.assertTrue(httpResponse.getStatusLine().getStatusCode() == 200);
		Assert.assertTrue(httpResponse.getEntity().getContentType().getValue().equals("image/jpeg"));
		ByteArrayOutputStream baos = new ByteArrayOutputStream(1024 * 1024);
		IOUtils.copy(httpResponse.getEntity().getContent(), baos);
		Assert.assertTrue(httpResponse.getEntity() != null);
		Assert.assertTrue(baos.toByteArray().length == httpResponse.getEntity().getContentLength());
		httpResponse.close();
	}

	//@Test
	public void whenUseSSLWithHttpAsyncClient_thenCorrect() throws Exception {
		CloseableHttpAsyncClient httpClient = asyncConfig.getHttpAsyncClient();
		httpClient.start();
		HttpGet request = new HttpGet(
				"https://webcache.visuera.com/cache_vgregion?ref=c%2c08b8448e-d05f-4d5c-93b6-9fff00e0fe18%2cBlobData&u=8D0C00FB3A1AE80");
		Future<HttpResponse> future = httpClient.execute(request, null);
		HttpResponse response = future.get();
		Assert.assertTrue((response.getStatusLine().getStatusCode() == 200));
		Assert.assertTrue(response.getEntity().getContentType().getValue().equals("image/jpeg"));
		ByteArrayOutputStream baos = new ByteArrayOutputStream(1024 * 1024);
		IOUtils.copy(response.getEntity().getContent(), baos);
		Assert.assertTrue(response.getEntity() != null);
		Assert.assertTrue(baos.toByteArray().length == response.getEntity().getContentLength());
		httpClient.close();
	}

	@Test
	public void loadTestwhenUseSSLWithHttpClient_thenCorrect() throws Exception {
		try {
			List<String> listOfUrlsList = new ArrayList<>(Arrays.asList(listOfUrls));
			List<Callable<String>> callables = new ArrayList<>();
			CloseableHttpClient httpClient = httpConfig.httpClient();
			IntStream.rangeClosed(1, 10).forEach(count -> {
				listOfUrlsList.stream().forEach(task -> {
					callables.add(new HttpCall(task, httpConfig.httpClient()));
				});
			});
			ExecutorService executorService = Executors.newSingleThreadExecutor();
			List<Future<String>> future = executorService.invokeAll(callables);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public class HttpCall implements Callable<String> {
		private String url;
		private CloseableHttpClient httpClient;

		public HttpCall(String url, CloseableHttpClient client) {
			this.url = url;
			this.httpClient = client;
		}

		@Override
		public String call() throws Exception {
			try {
				HttpGet request = new HttpGet(url);
				HttpResponse response = httpClient.execute(request);
				Assert.assertTrue((response.getStatusLine().getStatusCode() == 200));
				Assert.assertTrue(response.getEntity().getContentType().getValue().equals("image/jpeg"));
				ByteArrayOutputStream baos = new ByteArrayOutputStream(1024 * 1024);
				IOUtils.copy(response.getEntity().getContent(), baos);
				Assert.assertTrue(response.getEntity() != null);
				Assert.assertTrue(baos.toByteArray().length == response.getEntity().getContentLength());
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			return "true";
		}
	}

}

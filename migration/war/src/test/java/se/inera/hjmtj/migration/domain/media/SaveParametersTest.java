/*
 *
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 *  Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.media;

import org.junit.Test;
import se.inera.hjmtj.migration.domain.media.SaveParameters;
import se.inera.hjmtj.migration.domain.model.product.entities.Product;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductDocument;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductFile;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductImage;

import java.util.Collections;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SaveParametersTest {

    @Test
    public void computeInherited_mainImage() {
        String url = "www.test.com/test1.jpg";
        boolean doc = false;
        boolean mainImage = true;

        Product templateProduct = new Product();
        ProductFile mainImageFile = new ProductFile();
        mainImageFile.setOriginalContentUrl(url);
        templateProduct.setMainImage(mainImageFile);

        assertTrue(SaveParameters.computeInherited(templateProduct, url, mainImage, doc));

        url = "www.test.com/test2.jpg";
        assertFalse(SaveParameters.computeInherited(templateProduct, url, doc, mainImage));
    }

    @Test
    public void computeInherited_images() {
        String url = "www.test.com/test1.jpg";
        boolean doc = false;
        boolean mainImage = false;

        Product templateProduct = new Product();
        ProductFile mainImageFile = new ProductFile();
        mainImageFile.setOriginalContentUrl(url);
        ProductImage productImage = new ProductImage();
        productImage.setFile(mainImageFile);
        templateProduct.setImages(Collections.singleton(productImage));

        assertTrue(SaveParameters.computeInherited(templateProduct, url, mainImage, doc));

        url = "www.test.com/test2.jpg";
        assertFalse(SaveParameters.computeInherited(templateProduct, url, doc, mainImage));
    }

    @Test
    public void computeInherited_documents() {
        String url = "www.test.com/test1.jpg";
        boolean doc = true;
        boolean mainImage = false;

        Product templateProduct = new Product();
        ProductFile mainImageFile = new ProductFile();
        mainImageFile.setOriginalContentUrl(url);
        ProductDocument productImage = new ProductDocument();
        productImage.setFile(mainImageFile);
        templateProduct.setDocuments(Collections.singleton(productImage));

        assertTrue(SaveParameters.computeInherited(templateProduct, url, mainImage, doc));

        url = "www.test.com/test2.jpg";
        assertFalse(SaveParameters.computeInherited(templateProduct, url, doc, mainImage));
    }
}
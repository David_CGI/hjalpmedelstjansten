package se.inera.hjmtj.migration.domain.model.product;

import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedAcyclicGraph;
import org.junit.Test;
import se.inera.hjalpmedelstjansten.model.api.CategoryAPI;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjmtj.migration.domain.Graphs;

import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class AssistiveProductAPINodeTest {

    @Test
    public void sort() {
        DirectedAcyclicGraph<AssistiveProduct, DefaultEdge> graph = Graphs.directedAcyclicGraph();

        ArticleAPINode articleAPINode1 = new ArticleAPINode(graph, 1L);
        articleAPINode1.addFitsToArticlesItem(articleAPINode1);

        ArticleAPINode articleAPINode2 = new ArticleAPINode(graph, 2L);
        articleAPINode2.addFitsToArticlesItem(articleAPINode1);

        ArticleAPINode articleAPINode3 = new ArticleAPINode(graph, 3L);
        articleAPINode3.addFitsToArticlesItem(articleAPINode1);

        Graphs.topological(graph);
    }

    @Test
    public void instantiate() {
        DirectedAcyclicGraph<AssistiveProduct, DefaultEdge> graph = Graphs.directedAcyclicGraph();

        ArticleAPINode e1 = new ArticleAPINode(graph, 1L);
        ProductAPINode e2 = new ProductAPINode(graph, 2L);

        HashSet<Object> a = new HashSet<>();
        a.add(e1);
        a.add(e2);

        HashSet<Object> b = new HashSet<>();
        b.add(e2);
        b.add(e1);

        assertEquals(a, b);
    }

    @Test
    public void sort2() {
        DirectedAcyclicGraph<AssistiveProduct, DefaultEdge> graph = Graphs.directedAcyclicGraph();

        ProductAPINode migratedProductAPINode = newProduct(graph, 1);
        ProductAPINode migratedProductAPINode2 = newProduct(graph, 3);

        ArticleAPINode migratedArticleAPINode = newArticle(graph, 2, migratedProductAPINode);
        migratedArticleAPINode.addFitsToProductsItem(migratedProductAPINode2);

        ArticleAPINode migratedArticleAPINode2 = newArticle(graph, 4, migratedProductAPINode2);
        migratedArticleAPINode2.addFitsToArticlesItem(migratedArticleAPINode);

        List<AssistiveProduct> aid = Graphs.topological(graph);

        assertEquals(1, aid.get(0)
                           .getId()
                           .longValue());
        assertEquals(3, aid.get(1)
                           .getId()
                           .longValue());
        assertEquals(2, aid.get(2)
                           .getId()
                           .longValue());
        assertEquals(4, aid.get(3)
                           .getId()
                           .longValue());
    }

    private ProductAPINode newProduct(DirectedAcyclicGraph<AssistiveProduct, DefaultEdge> graph, long id) {
        ProductAPINode migratedProductAPINode = new ProductAPINode(graph, id);
        migratedProductAPINode.setProductNumber(String.valueOf(id));

        CategoryAPI category = new CategoryAPI();
        category.setArticleType(Article.Type.H);
        migratedProductAPINode.setCategory(category);

        return migratedProductAPINode;
    }

    private ArticleAPINode newArticle(DirectedAcyclicGraph<AssistiveProduct, DefaultEdge> graph, long id, ProductAPINode basedOnProductAPINode) {
        ArticleAPINode migratedArticleAPINode = new ArticleAPINode(graph, id);
        migratedArticleAPINode.setArticleNumber(String.valueOf(id));
        migratedArticleAPINode.setBasedOnProduct(basedOnProductAPINode);

        CategoryAPI category = new CategoryAPI();
        category.setArticleType(Article.Type.H);
        migratedArticleAPINode.setCategory(category);

        return migratedArticleAPINode;
    }

    @Test
    public void sort3() {
        DirectedAcyclicGraph<AssistiveProduct, DefaultEdge> graph = Graphs.directedAcyclicGraph();

        ProductAPINode migratedProductAPINode = newProduct(graph, 1);
        ArticleAPINode migratedArticleAPINode = newArticle(graph, 2, migratedProductAPINode);
        ArticleAPINode migratedArticleAPINode2 = newArticle(graph, 3, migratedProductAPINode);
        migratedArticleAPINode.addFitsToArticlesItem(migratedArticleAPINode2);
        migratedArticleAPINode2.addFitsToArticlesItem(migratedArticleAPINode);

        List<AssistiveProduct> aid = Graphs.topological(graph);

        assertEquals(1, aid.get(0)
                           .getId()
                           .longValue());
        assertEquals(3, aid.get(1)
                           .getId()
                           .longValue());
        assertEquals(2, aid.get(2)
                           .getId()
                           .longValue());
    }

    @Test
    public void sort4() {
        DirectedAcyclicGraph<AssistiveProduct, DefaultEdge> graph = Graphs.directedAcyclicGraph();

        newProduct(graph, 1);
        Graphs.topological(graph);
    }

    @Test
    public void sort6() {
        DirectedAcyclicGraph<AssistiveProduct, DefaultEdge> graph = Graphs.directedAcyclicGraph();

        ProductAPINode migratedProductAPINode = newProduct(graph, 1);

        ProductAPINode migratedProductAPINode2 = newProduct(graph, 2);
        migratedProductAPINode2.addDependency(migratedProductAPINode2);

        ProductAPINode migratedProductAPINode3 = newProduct(graph, 3);
        migratedProductAPINode3.addDependency(migratedProductAPINode2);

        migratedProductAPINode.addDependency(migratedProductAPINode3);

    }

}
package se.inera.hjmtj.migration.httpclient;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Assert;
import org.junit.Test;

public class GUIDUrlSeperateStringTest {
	public static final String REGEX = "\\s*,\\s*";
	public static final String REGEXP_FILENAME_2 = "(?:[a-zA-Z]\\:)\\\\([\\w-]+\\\\)*\\w([\\w-.])+";

	public static final String imageUrl1 = "https://webcache.visuera.com/cache_vgregion?ref=c%2c7eda20d0-72b1-4381-b183-a4640131dc7c%2cBlobData&u=8D3C7064D4D1380";
	public static final String imageUrl2 = "https://webcache.visuera.com/cache_vgregion?ref=c%2c0caa116a-f107-47e9-ae0a-a5d2007b2ee6%2cBlobData&u=8D3555ACF238100";

	@Test
	public void testSplit() throws UnsupportedEncodingException {
		String urlDecoded = URLDecoder.decode(imageUrl2, "UTF-8");
		String[] result = urlDecoded.split(REGEX);
		Assert.assertTrue(result[1].equals("0caa116a-f107-47e9-ae0a-a5d2007b2ee6"));
		String urlDecodedImage1 = URLDecoder.decode(imageUrl1, "UTF-8");
		String[] resultImage1 = urlDecodedImage1.split(REGEX);
		Assert.assertTrue(resultImage1[1].equals("7eda20d0-72b1-4381-b183-a4640131dc7c"));
	}

	@Test
	public void givenUrl_whenCanGetPathParams_thenCorrect() throws MalformedURLException {
		URL url = new URL("http://www.swereco.se/wp-content/uploads/2014/06/127560-175x131.jpg");
		Assert.assertTrue("/wp-content/uploads/2014/06/127560-175x131.jpg".equals(url.getFile()));
		String[] filePath = url.getFile().split("/");
		String fileName = filePath[filePath.length - 1];
		Assert.assertTrue("127560-175x131.jpg".equals(fileName));
	}

	@Test
	public void test_givenFilePath_getFileName() {
		String fileName = "C:\\Users\\perhenagren\\Desktop\\iWalk2.0\\3c8307bf2e587af5f63e60e1555001ef.jpg";
		 String[] result = fileName.split(REGEXP_FILENAME_2);
		 Assert.assertTrue("\\3c8307bf2e587af5f63e60e1555001ef.jpg".equals(result[1]));
		 String fileName_2="C:\\Users\\perhenagren\\Desktop\\iWalk2.0\\50853763_Alt01.jpg";
		 String[] result2 =fileName_2.split(REGEXP_FILENAME_2);
		 Assert.assertTrue("\\50853763_Alt01.jpg".equals(result2[1]));
		 String fileName_3 ="Akka Smart.gif";
		 String[] result3 =fileName_3.split(REGEXP_FILENAME_2);
		 Assert.assertTrue("Akka Smart.gif".equals(result3[0]));

	}
}
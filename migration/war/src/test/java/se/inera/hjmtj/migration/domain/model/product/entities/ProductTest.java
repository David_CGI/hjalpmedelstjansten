/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.product.entities;

import org.junit.Test;
import se.inera.hjmtj.migration.application.bootstrap.LoadAssistiveProducts;
import se.inera.hjmtj.migration.domain.xml.XMLFile;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ProductTest {

    @Test
    public void deserialize() {
        XMLFile xmlFile = new XMLFile(LoadAssistiveProducts.PERSIST_ORDER);

        List<Object> objects = xmlFile.load(getClass().getResourceAsStream("/product.xml"));
        assertEquals(16, objects.size());
        Product result = objects.stream()
                                .filter(Product.class::isInstance)
                                .map(Product.class::cast)
                                .findFirst()
                                .orElse(null);

        assertNotNull(result);

        assertNotNull(result.getVgrProdNo());
        assertEquals(108003344l, result.getVgrProdNo().longValue());

        assertNotNull(result.getOrderUnit());
        assertEquals("PCE", result.getOrderUnit()
                                  .getCode());

        assertNotNull(result.getClassifications());
        assertEquals("040312", result.getClassifications().stream().findFirst().map(ProductClassification::getCode).orElse(null));

        assertNotNull(result.getXmlCategories().leaf().getCode());
        assertEquals("040312", result.getXmlCategories().leaf().getCode());


        assertNotNull(result.getReferencedCategory());
        assertEquals(229l, result.getReferencedCategory()
                                 .getId()
                                 .longValue());
        assertEquals("040312", result.getReferencedCategory()
                                     .getClassification()
                                     .getCode());

        assertNotNull(result.getSupplier());
        assertEquals("5060591150015", result.getSupplier()
                                            .getGln());

        assertNotNull(result.getMainImage());

        assertNotNull(result.getImages());
        assertEquals(1, result.getImages()
                              .size());
    }
}
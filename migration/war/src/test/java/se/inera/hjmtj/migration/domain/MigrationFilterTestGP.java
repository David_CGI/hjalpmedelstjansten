/*
 *
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 *  Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain;

import org.junit.Test;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistAPI;
import se.inera.hjmtj.migration.domain.model.agreements.builders.GeneralPricelistAPIWithMultiplePricelists;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MigrationFilterTestGP {

    private final MigrationFilter migrationFilter = new MigrationFilter(new MigrationErrorRepository());

    private final GeneralPricelistPricelistAPI gp10 = pricelist("10", "2015-01-01 00:00:00.000000000");

    private final GeneralPricelistPricelistAPI gp11 = pricelist("11", "2016-01-01 00:00:00.000000000");

    private final GeneralPricelistPricelistAPI gp12 = pricelist("12", "2017-01-01 00:00:00.000000000");

    private final GeneralPricelistPricelistAPI gp13 = pricelist("13", "2018-01-01 00:00:00.000000000");

    private final GeneralPricelistPricelistAPI gp14 = pricelist("14", "2019-01-01 00:00:00.000000000");

    private final GeneralPricelistPricelistAPI gp15 = pricelist("15", "2030-01-01 00:00:00.000000000");

    private final GeneralPricelistAPIWithMultiplePricelists generalPricelistPricelistAPI = new GeneralPricelistAPIWithMultiplePricelists();

    @Test
    public void isDeprecated1() {
        generalPricelistPricelistAPI.setPricelistAPIs(Arrays.asList(gp10, gp11, gp12, gp13, gp14, gp15));

        assertTrue(migrationFilter.isDeprecated(generalPricelistPricelistAPI, gp10));
        assertTrue(migrationFilter.isDeprecated(generalPricelistPricelistAPI, gp11));
        assertTrue(migrationFilter.isDeprecated(generalPricelistPricelistAPI, gp12));
        assertTrue(migrationFilter.isDeprecated(generalPricelistPricelistAPI, gp13));

        assertFalse(migrationFilter.isDeprecated(generalPricelistPricelistAPI, gp14));

        assertTrue(migrationFilter.isDeprecated(generalPricelistPricelistAPI, gp15));
    }

    @Test
    public void isDeprecated2() {
        generalPricelistPricelistAPI.setPricelistAPIs(Arrays.asList(gp13, gp14, gp12, gp10, gp11, gp15));

        assertTrue(migrationFilter.isDeprecated(generalPricelistPricelistAPI, gp10));
        assertTrue(migrationFilter.isDeprecated(generalPricelistPricelistAPI, gp11));
        assertTrue(migrationFilter.isDeprecated(generalPricelistPricelistAPI, gp12));
        assertTrue(migrationFilter.isDeprecated(generalPricelistPricelistAPI, gp13));

        assertFalse(migrationFilter.isDeprecated(generalPricelistPricelistAPI, gp14));

        assertTrue(migrationFilter.isDeprecated(generalPricelistPricelistAPI, gp15));
    }

    private GeneralPricelistPricelistAPI pricelist(String number, String dateString) {
        GeneralPricelistPricelistAPI pricelist = new GeneralPricelistPricelistAPI();
        pricelist.setNumber(number);

        Timestamp date = Timestamp.valueOf(dateString);
        LocalDateTime localDate = date.toLocalDateTime();
        Long validFrom = DateTimes.toEpochMillis(localDate);
        pricelist.setValidFrom(validFrom);

        return pricelist;
    }
}
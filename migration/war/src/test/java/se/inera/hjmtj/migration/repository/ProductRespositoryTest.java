package se.inera.hjmtj.migration.repository;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import se.inera.hjmtj.migration.domain.model.product.entities.Product;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductFile;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductOrganization;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductOrganizationEconomicInfo;
import se.inera.hjmtj.migration.infra.products.ProductRepository;

public class ProductRespositoryTest {
	private EntityManager myEntityManager;

	@Before
	public void init() {
		myEntityManager = Mockito.mock(EntityManager.class);
		Query mockQuery = Mockito.mock(Query.class);
		TypedQuery<ProductOrganization> typeQuery = Mockito.mock(TypedQuery.class);
		Mockito.when(mockQuery.setParameter(Mockito.anyString(), Mockito.any())).thenReturn(mockQuery);
		Mockito.when(myEntityManager.createQuery(Mockito.anyString(), Mockito.any(Class.class))).thenReturn(typeQuery);
		Mockito.when(typeQuery.getSingleResult()).thenReturn(createValidProduct(2223, 444).getSupplier());
	}

	@Test
	public void testGetAllProductsWithImages() {
		ProductRepository repos = new ProductRepository(myEntityManager);
		Optional<List<ProductOrganization>> listOfProduct = repos.findAllProductsWithImageUrls();
		Assert.assertNotNull(listOfProduct);
	}

	public static Product createValidProduct(long productId, long organizationId) {
		Product product = new Product();
		product.setId(productId);
		product.setManufacturerProductNumber("222");
		product.setSupplierProductNumber("222");
		product.setVgrProdNo(123l);
		ProductOrganization prdocutOrg = createValidOrganization(organizationId, "name of org", "1231231", "34324324324");
		product.setSupplier(prdocutOrg);
		ProductFile image = new ProductFile();
		product.setMainImage(image);
		Set<Product> setOfProduct = new HashSet<>();
		setOfProduct.add(product);
		prdocutOrg.setProducts(setOfProduct);
		return product;
	}

	public static ProductOrganization createValidOrganization(long organizationId, String organizationName, String gln, String orgNr) {
		ProductOrganization organization = new ProductOrganization();
		organization.setId(organizationId);
		organization.setName(organizationName);
		organization.setGln(gln);
		ProductOrganizationEconomicInfo info = new ProductOrganizationEconomicInfo();
		info.setOrganizationNo(orgNr);
		organization.setEconomicInformation(info);
		return organization;
	}
}

/*
 *
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 *  Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.product.entities;

import org.junit.Test;
import se.inera.hjmtj.migration.application.bootstrap.LoadAssistiveProducts;
import se.inera.hjmtj.migration.domain.xml.XMLFile;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ProductFileTest {

    @Test
    public void deserialize() {
        XMLFile xmlFile = new XMLFile(LoadAssistiveProducts.PERSIST_ORDER);

        List<Object> objects = xmlFile.load(getClass().getResourceAsStream("/product.xml"));
        List<Product> result = objects.stream()
                                .filter(Product.class::isInstance)
                                .map(Product.class::cast)
                                .collect(Collectors.toList());
        assertEquals(12, result.size());

        assertNotNull(result);

        assertEquals("thumbnail", result.get(0).getMainImage().getFileName());
        assertEquals(".jpg", result.get(0).getMainImage().getFileExtension());
        assertEquals("thumbnail.jpg", result.get(0).getMainImage().fullFileName());

        assertEquals("c60c8b0b-3715-42cc-b146-a46501875cc6", result.get(1).getMainImage().getFileName());
        assertEquals(".jpg", result.get(1).getMainImage().getFileExtension());
        assertEquals("c60c8b0b-3715-42cc-b146-a46501875cc6.jpg", result.get(1).getMainImage().fullFileName());

        assertEquals("användarmanual_0204046", result.get(2).getMainImage().getFileName());
        assertEquals(".pdf", result.get(2).getMainImage().getFileExtension());
        assertEquals("användarmanual_0204046.pdf", result.get(2).getMainImage().fullFileName());

        assertEquals("användarmanual_0204046", result.get(3).getMainImage().getFileName());
        assertEquals(".pdf", result.get(3).getMainImage().getFileExtension());
        assertEquals("användarmanual_0204046.pdf", result.get(3).getMainImage().fullFileName());

        assertEquals("Trilogy inställningsGuide SV", result.get(4).getMainImage().getFileName());
        assertEquals(".pdf", result.get(4).getMainImage().getFileExtension());
        assertEquals("Trilogy inställningsGuide SV.pdf", result.get(4).getMainImage().fullFileName());

        assertEquals("29cfef1f-ce8a-4830-817e-a6b3010bba77", result.get(5).getMainImage().getFileName());
        assertEquals(".jpg", result.get(5).getMainImage().getFileExtension());
        assertEquals("29cfef1f-ce8a-4830-817e-a6b3010bba77.jpg", result.get(5).getMainImage().fullFileName());

        assertEquals("e93fc8d5-6d74-496f-8fae-a4640153553e", result.get(6).getMainImage().getFileName());
        assertEquals(".jpg", result.get(6).getMainImage().getFileExtension());
        assertEquals("e93fc8d5-6d74-496f-8fae-a4640153553e.jpg", result.get(6).getMainImage().fullFileName());

        assertEquals("57df469d-4faa-4453-84cf-a464015761a2", result.get(7).getMainImage().getFileName());
        assertEquals(".png", result.get(7).getMainImage().getFileExtension());
        assertEquals("57df469d-4faa-4453-84cf-a464015761a2.png", result.get(7).getMainImage().fullFileName());

        assertEquals("747fa20d-e603-40d2-9314-a464015100f4", result.get(8).getMainImage().getFileName());
        assertEquals(".jpg", result.get(8).getMainImage().getFileExtension());
        assertEquals("747fa20d-e603-40d2-9314-a464015100f4.jpg", result.get(8).getMainImage().fullFileName());

        assertEquals("f9c359ce-23f6-40a8-bfa5-a4650186ef5d", result.get(9).getMainImage().getFileName());
        assertEquals(".jpg", result.get(9).getMainImage().getFileExtension());
        assertEquals("f9c359ce-23f6-40a8-bfa5-a4650186ef5d.jpg", result.get(9).getMainImage().fullFileName());

        assertEquals("33b15d4b-b2e7-4c67-b458-a4640176ca9d", result.get(10).getMainImage().getFileName());
        assertEquals(".jpg", result.get(10).getMainImage().getFileExtension());
        assertEquals("33b15d4b-b2e7-4c67-b458-a4640176ca9d.jpg", result.get(10).getMainImage().fullFileName());

        assertEquals("lindegaard20082 red", result.get(11).getMainImage().getFileName());
        assertEquals(".jpg", result.get(11).getMainImage().getFileExtension());
        assertEquals("lindegaard20082 red.jpg", result.get(11).getMainImage().fullFileName());

    }
}
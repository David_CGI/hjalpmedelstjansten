/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.assortment;

import org.junit.Test;
import se.inera.hjmtj.migration.application.MigrationApplication;
import se.inera.hjmtj.migration.application.bootstrap.LoadAssortments;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementProductBase;
import se.inera.hjmtj.migration.domain.xml.XMLFile;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class AssortmentTest {

    @Test
    public void deserialize() {
        XMLFile xmlFile = new XMLFile(LoadAssortments.PERSIST_ORDER);

        List<Object> objects = xmlFile.load(getClass().getResourceAsStream("/assortment.xml"));
        assertEquals(7, objects.size());
        Assortment result = objects.stream()
                                   .filter(Assortment.class::isInstance)
                                   .map(Assortment.class::cast)
                                   .findFirst()
                                   .orElse(null);

        assertNotNull(result);
        assertEquals(1, result.getId().longValue());
        assertEquals("T1177 AGA GAS AB, Linde Healthcare", result.getName());
        assertEquals(false, result.isDisabled());

        assertNotNull(result.getProducts());
        assertEquals(2, result.getProducts()
                              .size());
        Set<String> vgrProdNo = result.getProducts()
                                      .stream()
                                      .map(AssortmentProduct::getVgrProdNo)
                                      .collect(Collectors.toSet());
        assertTrue(vgrProdNo.contains("108124066"));
        assertTrue(vgrProdNo.contains("1081240661"));

        assertEquals(2, result.getCounties()
                              .size());
        List<AssortmentCountyMunicipality> counties = result.getCounties()
                                                            .stream()
                                                            .sorted(Comparator.comparing(AssortmentCountyMunicipality::getId))
                                                            .collect(Collectors.toList());
        assertEquals(56910l, counties.get(0)
                                     .getId()
                                     .longValue());
        assertEquals("56910", counties.get(0)
                                      .getIdAsString());
        assertEquals("18", counties.get(0)
                                   .getCode());
        assertEquals("14", counties.get(1)
                                   .getCode());

        assertEquals(1, result.getCountyMunicipalities()
                              .size());
        assertEquals("24", result.getCountyMunicipalities()
                                 .iterator()
                                 .next()
                                 .getCode());
    }


}
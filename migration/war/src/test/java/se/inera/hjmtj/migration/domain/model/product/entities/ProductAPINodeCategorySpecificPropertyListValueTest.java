package se.inera.hjmtj.migration.domain.model.product.entities;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.StringReader;

import static org.junit.Assert.assertNotNull;

public class ProductAPINodeCategorySpecificPropertyListValueTest {

    private final String xml = "  <LaLaLa>" +
            "            <id>1</id>" +
            "       </LaLaLa>";

    @Test(expected = UnmarshalException.class)
    public void testProductCategorySpecificPropertyListValueDeserialization() throws JAXBException {

        JAXBContext jaxbContext = JAXBContext.newInstance(ProductCategorySpecificPropertyListValue.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        ProductCategorySpecificPropertyListValue result = (ProductCategorySpecificPropertyListValue) jaxbUnmarshaller.unmarshal(new StringReader(xml));
    }

    @Test
    public void testProductCategorySpecificPropertyListValueDeserialization2() throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.registerModule(new JavaTimeModule());
        xmlMapper.registerModule(new JacksonXmlModule());
        xmlMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);

        ProductCategorySpecificPropertyListValue result = xmlMapper.readValue(xml, ProductCategorySpecificPropertyListValue.class);
        assertNotNull(result);
    }
}
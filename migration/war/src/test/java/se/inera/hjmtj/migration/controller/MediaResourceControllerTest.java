package se.inera.hjmtj.migration.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.ClientProtocolException;
import org.junit.Assert;
import org.junit.Before;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import se.inera.hjmtj.migration.domain.model.product.entities.Product;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductFile;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductOrganization;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductOrganizationEconomicInfo;
import se.inera.hjmtj.migration.httpclient.ImageHttpClient;
import se.inera.hjmtj.migration.httpclient.ImageHttpClientTest;
import se.inera.hjmtj.migration.infra.BulkService;
import se.inera.hjmtj.migration.infra.images.ImageData;
import se.inera.hjmtj.migration.infra.products.ProductRepository;

public class MediaResourceControllerTest {
	private UploadToS3Controller imageController;
	private EntityManager myEntityManager;
	@InjectMocks
	private BulkService bulkService;
	private ProductRepository prodRepoService;
	private static String IMAGE_URL = "https://webcache.visuera.com/cache_vgregion?ref=c%2c9016226d-6d3d-4918-bb72-a4650009a675%2cBlobData&u=8D233E183874600";
	@Mock
	private ImageHttpClient httpClient;

	@Before
	public void init() {
		prodRepoService = Mockito.mock(ProductRepository.class);
		//bulkService = Mockito.mock(BulkService.class);
		myEntityManager = Mockito.mock(EntityManager.class);
		MockitoAnnotations.initMocks(this);
		Product test = createValidProduct(9999, 888);
		Optional<List<ProductOrganization>> setOfProducts = Optional.of(new ArrayList());
		setOfProducts.get().add(test.getSupplier());
		Mockito.when(prodRepoService.findAllProductsWithImageUrls()).thenReturn(setOfProducts);
	}

	public static Product createValidProduct(long productId, long organizationId) {
		Product product = new Product();
		product.setId(productId);
		product.setManufacturerProductNumber("222");
		product.setSupplierProductNumber("222");
		product.setVgrProdNo(123l);
		product.setSupplier(createValidOrganization(organizationId, "name of org", "1231231","34324324324"));
		ProductFile image = new ProductFile();
		image.setOriginalContentUrl(IMAGE_URL);
		product.setMainImage(image);
		return product;
	}

	public static ProductOrganization createValidOrganization(long organizationId, String organizationName, String gln, String orgNr) {
		ProductOrganization organization = new ProductOrganization();
		organization.setId(organizationId);
		organization.setName(organizationName);
		organization.setGln(gln);
		ProductOrganizationEconomicInfo info = new ProductOrganizationEconomicInfo();
		info.setOrganizationNo(orgNr);
		organization.setEconomicInformation(info);
		return organization;
	}

	//@Test
	public void testMigrateImage() throws ClientProtocolException, IOException, BulkService.ErrorMessageAPIException {
		UploadToS3Controller controller = new UploadToS3Controller(prodRepoService, bulkService, httpClient);
		Mockito.when(bulkService.checkMediaRepositoryIfImageExist(Mockito.anyString())).thenReturn(Boolean.FALSE.booleanValue());
		Mockito.when(bulkService.updateMediaRepository(Mockito.anyString(), Mockito.any())).thenReturn("");
		InputStream targetStream = ImageHttpClientTest.class.getClassLoader().getResourceAsStream("kalsingar.jpg");
		byte[] targetArray = IOUtils.toByteArray(targetStream);
		ImageData imageData = new ImageData();
		imageData.setData(targetArray);
		Mockito.when(httpClient.getImage(Mockito.anyString())).thenReturn(Optional.of(imageData));
		int count = controller.startImageMigrationPerSupplierForS3();
		Assert.assertEquals(count, 1);
	}
}

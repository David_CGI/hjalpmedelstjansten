package se.inera.hjmtj.migration.domain.model.actors;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import se.inera.hjmtj.migration.application.bootstrap.LoadActors;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.AgreementOrganization;
import se.inera.hjmtj.migration.domain.xml.XMLFile;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@Slf4j
public class ActorTest {

    @Test
    public void deserialize() {
        XMLFile xmlFile = new XMLFile(LoadActors.PERSIST_ORDER);

        List<Object> objects = xmlFile.load(getClass().getResourceAsStream("/actor.xml"));
        assertEquals(7, objects.size());
        assertEquals(3, objects.stream()
                               .filter(Actor.class::isInstance)
                               .map(Actor.class::cast)
                               .distinct()
                               .count());
        assertEquals(1, objects.stream()
                               .filter(ActorRole.class::isInstance)
                               .map(ActorRole.class::cast)
                               .distinct()
                               .count());
        assertEquals(1, objects.stream()
                               .filter(ActorRole.class::isInstance)
                               .map(ActorRole.class::cast)
                               .map(ActorRole::getActors)
                               .flatMap(Collection::stream)
                               .distinct()
                               .count());
        assertEquals(1, objects.stream()
                               .filter(ActorRole.class::isInstance)
                               .map(ActorRole.class::cast)
                               .map(ActorRole::getRoleType)
                               .distinct()
                               .peek(System.out::println)
                               .count());
        assertEquals(1, objects.stream()
                               .filter(ActorOrganization.class::isInstance)
                               .map(ActorOrganization.class::cast)
                               .distinct()
                               .count());
        assertEquals(5, objects.stream()
                               .filter(Actor.class::isInstance)
                               .map(Actor.class::cast)
                               .map(Actor::getFirstName)
                               .filter(Objects::nonNull)
                               .count());
        assertEquals(5, objects.stream()
                               .filter(Actor.class::isInstance)
                               .map(Actor.class::cast)
                               .map(Actor::getLastName)
                               .filter(Objects::nonNull)
                               .count());

        Actor result = objects.stream()
                              .filter(Actor.class::isInstance)
                              .map(Actor.class::cast)
                              .findFirst()
                              .orElse(null);

        assertNotNull(result);
        assertNotNull(result.getOrganization());
        assertNotNull(result.getFirstName());
        assertNotNull(result.getLastName());
        assertEquals("test@epost.se",result.getContactInformation().getEmail());
    }

}
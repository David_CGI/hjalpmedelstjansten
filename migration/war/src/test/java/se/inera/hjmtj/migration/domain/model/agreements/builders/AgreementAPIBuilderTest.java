/*
 *
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 *  Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.agreements.builders;

import org.junit.Test;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.model.api.BusinessLevelAPI;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjmtj.migration.application.bootstrap.LoadAgreements;
import se.inera.hjmtj.migration.domain.model.actorpricelistapprovers.ActorPricelistApproverActorRole;
import se.inera.hjmtj.migration.domain.model.actorpricelistapprovers.ActorPricelistApproverAgreement;
import se.inera.hjmtj.migration.domain.model.actors.Actor;
import se.inera.hjmtj.migration.domain.model.actors.ActorOrganization;
import se.inera.hjmtj.migration.domain.model.actors.ActorRole;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.Agreement;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.AgreementOrganization;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.AgreementParty;
import se.inera.hjmtj.migration.domain.model.organization.entities.Customer;
import se.inera.hjmtj.migration.domain.xml.FileIdResolver;
import se.inera.hjmtj.migration.domain.xml.XMLFile;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjCustomerRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjUserRepository;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

import static org.junit.Assert.assertEquals;

public class AgreementAPIBuilderTest {

    private final AgreementAPIBuilder agreementAPIBuilder;

    private final AgreementAPIBuilderBase.AgreementAPICreated agreementApi;

    public AgreementAPIBuilderTest() {
        agreementAPIBuilder = new AgreementAPIBuilder();
        agreementAPIBuilder.hjmtjCustomerRepository(new HjmtjCustomerRepository() {
            @Override
            public OrganizationAPI find(Customer customer) {
                if (customer.isBusinessLevel()) {
                    return null;
                }

                OrganizationAPI organization = new OrganizationAPI();
                organization.setOrganizationName(customer.getOrganizationName());
                organization.setGln(customer.getGln());

                return organization;
            }

            @Override
            public BusinessLevelAPI findBusinessLevel(Customer customer) {
                if (!customer.isBusinessLevel()) {
                    return null;
                }

                OrganizationAPI organization = new OrganizationAPI();
                organization.setOrganizationName("Root");
                organization.setGln(customer.getGln());

                BusinessLevelAPI businessLevelAPI = new BusinessLevelAPI();
                businessLevelAPI.setName(customer.getOrganizationName());
                businessLevelAPI.setOrganization(organization);

                return businessLevelAPI;
            }
        });

        agreementApi = new AgreementAPIBuilderBase.AgreementAPICreated();
        agreementApi.setSharedWithCustomers(new LinkedList<>());
        agreementApi.setSharedWithCustomersBusinessLevels(new LinkedList<>());
    }

    @Test
    public void getPricelistApprovers() {
        System.setProperty(FileIdResolver.DISABLE_VALIDATION, "true");
        XMLFile xmlFile = new XMLFile(LoadAgreements.PERSIST_ORDER);
        List<Object> objects = xmlFile.load(getClass().getResourceAsStream("/agreement.xml"));
        Agreement agreement = objects.stream()
                                     .filter(Agreement.class::isInstance)
                                     .map(Agreement.class::cast)
                                     .findFirst()
                                     .orElse(null);

        AgreementParty buyer = new AgreementParty();
        AgreementOrganization agreementOrganization = new AgreementOrganization();
        Customer customer = new Customer();
        customer.setGln("123");
        agreementOrganization.setCustomer(customer);
        buyer.setAgreementOrganization(agreementOrganization);
        agreement.setBuyer(buyer);

        ActorPricelistApproverAgreement actorPricelistApproverAgreement = new ActorPricelistApproverAgreement();
        HashSet<ActorPricelistApproverActorRole> approvers = new HashSet<>();
        ActorPricelistApproverActorRole approverActorRole = new ActorPricelistApproverActorRole();
        ActorRole actorRole = new ActorRole();
        HashSet<Actor> actors = new HashSet<>();
        Actor actor = new Actor();
        ActorOrganization actorOrganization = new ActorOrganization();
        actorOrganization.setCustomer(customer);
        actor.setOrganization(actorOrganization);
        actors.add(actor);
        actorRole.setActors(actors);
        approverActorRole.setActorRole(actorRole);
        approvers.add(approverActorRole);
        actorPricelistApproverAgreement.setApprovers(approvers);
        agreement.setActorPricelistApproverAgreement(actorPricelistApproverAgreement);

        AgreementAPIBuilder agreementAPIBuilder = new AgreementAPIBuilder();
        agreementAPIBuilder.organizationController(new OrganizationController() {
            @Override
            public Organization getOrganizationFromLoggedInUser(UserAPI userAPI) {
                Organization organization = new Organization();
                organization.setGln(customer.getGln());
                return organization;
            }
        });
        agreementAPIBuilder.hjmtjUserRepository(new HjmtjUserRepository() {
            @Override
            public UserAPI findBy(Actor actor) {
                return new UserAPI();
            }
        });
        List<UserAPI> pricelistApprovers = agreementAPIBuilder.getPricelistApprovers(agreement);
        Objects.requireNonNull(pricelistApprovers);
        assertEquals(1, pricelistApprovers.size());
    }

    @Test
    public void setSharedWith1() {
        Set<Customer> sharedWithCustomers = new HashSet<>();

        Customer a = new Customer();
        a.setOrganizationName("Test Organization");
        a.setGln("123");
        sharedWithCustomers.add(a);

        Customer b = new Customer();
        b.setType(Customer.BUSINESS_AREA);
        b.setOrganizationName("Test Organization, Center");
        b.setGln("123");
        sharedWithCustomers.add(b);

        Customer c = new Customer();
        c.setOrganizationName("Test Organization ii");
        c.setGln("1235");
        sharedWithCustomers.add(c);

        agreementAPIBuilder.setSharedWith(agreementApi, sharedWithCustomers);

        List<OrganizationAPI> sharedWithCustomersList = agreementApi.getSharedWithCustomers();
        assertEquals(2, sharedWithCustomersList.size());

        List<BusinessLevelAPI> sharedWithCustomersBusinessLevels = agreementApi.getSharedWithCustomersBusinessLevels();
        assertEquals(0, sharedWithCustomersBusinessLevels.size());
    }

    @Test
    public void setSharedWith2() {
        Set<Customer> sharedWithCustomers = new HashSet<>();

        Customer b = new Customer();
        b.setType(Customer.BUSINESS_AREA);
        b.setOrganizationName("Test Organization, Center");
        b.setGln("123");
        sharedWithCustomers.add(b);

        Customer c = new Customer();
        c.setOrganizationName("Test Organization ii");
        c.setGln("1235");
        sharedWithCustomers.add(c);

        agreementAPIBuilder.setSharedWith(agreementApi, sharedWithCustomers);

        List<OrganizationAPI> sharedWithCustomersList = agreementApi.getSharedWithCustomers();
        assertEquals(1, sharedWithCustomersList.size());

        List<BusinessLevelAPI> sharedWithCustomersBusinessLevels = agreementApi.getSharedWithCustomersBusinessLevels();
        assertEquals(1, sharedWithCustomersBusinessLevels.size());
    }

    @Test
    public void setSharedWith3() {
        Set<Customer> sharedWithCustomers = new HashSet<>();

        Customer a = new Customer();
        a.setType(Customer.BUSINESS_AREA);
        a.setOrganizationName("Test Organization, Ear Center");
        a.setGln("123");
        sharedWithCustomers.add(a);

        Customer b = new Customer();
        b.setType(Customer.BUSINESS_AREA);
        b.setOrganizationName("Test Organization, Sight Center");
        b.setGln("123");
        sharedWithCustomers.add(b);

        Customer c = new Customer();
        c.setOrganizationName("Test Organization ii");
        c.setGln("1235");
        sharedWithCustomers.add(c);

        agreementAPIBuilder.setSharedWith(agreementApi, sharedWithCustomers);

        List<OrganizationAPI> sharedWithCustomersList = agreementApi.getSharedWithCustomers();
        assertEquals(1, sharedWithCustomersList.size());

        List<BusinessLevelAPI> sharedWithCustomersBusinessLevels = agreementApi.getSharedWithCustomersBusinessLevels();
        assertEquals(2, sharedWithCustomersBusinessLevels.size());
    }

    @Test
    public void setSharedWith4() {
        agreementAPIBuilder.setSharedWith(agreementApi, Collections.emptySet());
        agreementApi.setSharedWithCustomers(null);
        agreementApi.setSharedWithCustomersBusinessLevels(null);

        Set<Customer> sharedWithCustomers = new HashSet<>();
        Customer a = new Customer();
        a.setType(Customer.BUSINESS_AREA);
        a.setOrganizationName("Test Organization, Ear Center");
        a.setGln("123");
        sharedWithCustomers.add(a);

        agreementAPIBuilder.setSharedWith(agreementApi, sharedWithCustomers);
    }
}
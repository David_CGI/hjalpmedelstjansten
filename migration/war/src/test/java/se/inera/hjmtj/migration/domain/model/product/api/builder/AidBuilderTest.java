//package se.inera.hjmtj.migration.domain.model.product.api.builder;
//
//import org.junit.Test;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import se.inera.hjmtj.migration.domain.model.product.entities.Product;
//import se.inera.hjmtj.migration.domain.model.product.entities.ProductCategoryPath;
//
//import static org.mockito.Mockito.*;
//
//public class AidBuilderTest {
//
//    private String categoryPath = "Produkt\\Vara\\ISO-klassificerade hjälpmedel (ISO 9999)\\Hjälpmedelsområden" +
//            "\\04 Personlig medicinsk behandling\\0406 Cirkulationsbehandling" +
//            "\\040606 Stödstrumpor och kompressionsstrumpor för armar och ben och andra delar av kroppen" +
//            "\\Kompressionsstrumpa";
//
//    @Test
//    public void createProduct() {
//        Product product = new Product();
//        ProductCategoryPath categories = new ProductCategoryPath();
//        product.setXmlCategories(categories);
//
//        AidBuilder aidBuilder = new AidBuilder(mock(AidBuilder.Context.class, RETURNS_MOCKS));
//        aidBuilder.createProduct(product);
//    }
//
//    @Test
//    public void createArticle() {
//        Product product = new Product();
//        ProductCategoryPath categories = new ProductCategoryPath();
//        product.setXmlCategories(categories);
//
//        AidBuilder aidBuilder = new AidBuilder(mock(AidBuilder.Context.class, RETURNS_MOCKS));
//        aidBuilder.createArticle(product);
//    }
//}
/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.categories;

import org.junit.Test;

import static org.junit.Assert.*;

public class ProductCategorySpecificPropertyTypeTest {

    @Test
    public void categoryCode() {
        String value = "Tilläggsfunktioner (sl-pr) (förtydligande) - Används i kategori: Produkt\\Vara\\ISO-klassificerade hjälpmedel (ISO 9999)\\Hjälpmedelsområden\\22 Kommunikation och information\\2239 Presentationsenheter för datorer\\223912 Speciella programvaror för presentation";
        String code = ProductCategorySpecificPropertyType.categoryCode(value);
        assertEquals("223912", code);

        value = "Tilläggsfunktioner (sl-pr) (förtydligande) - Används i kategori: Produkt\\Vara\\ISO-klassificerade hjälpmedel (ISO 9999)\\Hjälpmedelsområden\\22 Kommunikation och information\\2239 Presentationsenheter för datorer\\22";
        code = ProductCategorySpecificPropertyType.categoryCode(value);
        assertEquals("22", code);

        value = "Typ badkarsstol/pall - Refererens till: Typ badkarsstol/pall (isovl_22130) - Används i kategori: Produkt\\Vara\\ISO-klassificerade hjälpmedel (ISO 9999)\\Hjälpmedelsområden\\09 Personlig vård och skydd\\0933 Tvättning, bad och dusch\\093303 Bad- och duschstolar (med eller utan hjul), badbrädor, pallar, ryggstöd och sitsar";
        code = ProductCategorySpecificPropertyType.categoryCode(value);
        assertEquals("093303", code);
    }

    @Test
    public void name() {
        String value = "Tilläggsfunktioner (sl-pr) (förtydligande) - Används i kategori: Produkt\\Vara\\ISO-klassificerade hjälpmedel (ISO 9999)\\Hjälpmedelsområden\\22 Kommunikation och information\\2239 Presentationsenheter för datorer\\223912 Speciella programvaror för presentation";
        String name = ProductCategorySpecificPropertyType.name(value);
        assertEquals("Tilläggsfunktioner (sl-pr) (förtydligande)", name);

        value = "Typ badkarsstol/pall - Refererens till: Typ badkarsstol/pall (isovl_22130) - Används i kategori: Produkt\\Vara\\ISO-klassificerade hjälpmedel (ISO 9999)\\Hjälpmedelsområden\\09 Personlig vård och skydd\\0933 Tvättning, bad och dusch\\093303 Bad- och duschstolar (med eller utan hjul), badbrädor, pallar, ryggstöd och sitsar";
        name = ProductCategorySpecificPropertyType.name(value);
        assertEquals("Typ badkarsstol/pall", name);

        value = "Utformning koppling, sond/gastrostomitub/kopplingsslang knapp  - Refererens till: Utformning koppling, sond/gastrostomitub/kopplingsslang knapp  (aa02a5f2b6884275ad1e55cff826d25c) - Används i kategori: Produkt\\Vara\\ISO-klassificerade hjälpmedel (ISO 9999)\\Hjälpmedelsområden\\15 Hjälpmedel i hushållet\\1509 Att äta och dricka\\150930 Sonder";
        name = ProductCategorySpecificPropertyType.name(value);
        assertEquals("Utformning koppling, sond/gastrostomitub/kopplingsslang knapp", name);
    }
}
package se.inera.hjmtj.migration.httpclient;

import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import se.inera.hjmtj.migration.infra.images.ImageData;

public class ImageHttpClientTest  {
	@Mock
	private HttpClientConfig config;
	@Mock
	private CloseableHttpClient defaultHttpClient;
	@Mock
	private PoolingHttpClientConnectionManager mgr;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		Mockito.when(config.poolingConnectionManager()).thenReturn(mgr);
		Mockito.when(config.httpClient()).thenReturn(defaultHttpClient);
	}

	@Test
	public void testGetImage() throws ClientProtocolException, IOException {
		// given:
		InputStream targetStream =ImageHttpClientTest.class.getClassLoader().getResourceAsStream("kalsingar.jpg");
		HttpGet httpGet = Mockito.mock(HttpGet.class);
		CloseableHttpResponse httpResponse = Mockito.mock(CloseableHttpResponse.class);
		HttpEntity entity = Mockito.mock(HttpEntity.class);
		StatusLine statusLine = Mockito.mock(StatusLine.class);
		Mockito.when(statusLine.getStatusCode()).thenReturn(200);
		Mockito.when(httpResponse.getStatusLine()).thenReturn(statusLine);
		Mockito.when(httpResponse.getEntity()).thenReturn(entity);
		Mockito.when(httpResponse.getAllHeaders()).thenReturn(headersWithContentType());
		Mockito.when(entity.getContent()).thenReturn(targetStream);
		Mockito.when(defaultHttpClient.execute(Mockito.any())).thenReturn(httpResponse);
		ImageHttpClient client = new ImageHttpClient(config);
		// when:
		Optional<ImageData> data = client.getImage("");
		// then:
		Assert.assertNotNull(data);
	}

	private Header[] headersWithContentType() {
		Header headerContentType = new BasicHeader(HttpHeaders.CONTENT_TYPE, "image/jpeg"); 
		return new Header[] { headerContentType};
	}

}
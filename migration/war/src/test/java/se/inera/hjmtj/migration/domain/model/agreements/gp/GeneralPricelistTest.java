/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.agreements.gp;

import org.junit.Test;
import se.inera.hjmtj.migration.application.bootstrap.LoadGeneralPricelist;
import se.inera.hjmtj.migration.domain.xml.FileIdResolver;
import se.inera.hjmtj.migration.domain.xml.XMLFile;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class GeneralPricelistTest {

    @Test
    public void deserialize() {
        System.setProperty(FileIdResolver.DISABLE_VALIDATION,"true");
        XMLFile xmlFile = new XMLFile(LoadGeneralPricelist.PERSIST_ORDER);
        List<Object> objects = xmlFile.load(getClass().getResourceAsStream("/gp.xml"));
        GeneralPricelist result = objects.stream()
                                         .filter(GeneralPricelist.class::isInstance)
                                         .map(GeneralPricelist.class::cast)
                                         .findFirst()
                                         .orElse(null);

        assertNotNull(result);
        assertEquals(204, result.getRows()
                               .size());
        assertNotNull( result.getSeller());
        assertNotNull( result.getSeller().getAgreementOrganization());
        assertNotNull( result.getValid());
    }

}
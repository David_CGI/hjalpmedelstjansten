package se.inera.hjmtj.migration.domain.model;

import org.junit.Ignore;
import org.junit.Test;
import se.inera.hjmtj.migration.domain.DateTimes;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertEquals;

public class DateTimesTest {

    private static final ZoneId ZONE = ZoneId.of("Europe/Stockholm");

    private static final ZoneOffset OFFSET_STOCKHOLM = ZonedDateTime.now(ZONE)
                                                                    .getOffset();

    private final long winterTimeExpected = LocalDateTime.of(2018, 1, 2, 0, 0, 0)
                                                         .toInstant(OFFSET_STOCKHOLM)
                                                         .toEpochMilli();

    private final long summerTimeExpected = LocalDateTime.of(2015, 6, 30, 0, 0, 0)
                                                         .toInstant(OFFSET_STOCKHOLM)
                                                         .toEpochMilli();

    ;

    /**
     * Verify that zone time is taken into account when parsing datetime string that does not contain TZ.
     */
    @Ignore
    @Test
    public void toEpochMillis() {
        String winterTime = "2018-01-02T00:00:00";
        long winterActual = DateTimes.toEpochMillis(winterTime);
        assertEquals(winterTimeExpected, winterActual);

        String summerTime = "2015-06-30T00:00:00";
        long summerActual = DateTimes.toEpochMillis(summerTime);
        assertEquals(summerTimeExpected, summerActual);

        long date = DateTimes.toEpochMillis(LocalDate.of(2018,3,7));
        assertEquals(new Date(2018-1900,2,7).getTime(), date);

        long actual = DateTimes.toEpochMillis(DateTimes.epochMillisToDate(new Date(2018 - 1900, 2, 7).getTime()));
        assertEquals(new Date(2018-1900,2,7).getTime(), actual);
    }

}
/*
 *
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 *  Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain;

import org.junit.Test;
import se.inera.hjalpmedelstjansten.model.api.AgreementAPI;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistAPI;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Arrays;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MigrationFilterTestAgreement {

    private final MigrationFilter migrationFilter = new MigrationFilter(new MigrationErrorRepository());

    private final AgreementPricelistAPI a10 = pricelist("10", "2015-01-01 00:00:00.000000000");

    private final AgreementPricelistAPI a11 = pricelist("11", "2016-01-01 00:00:00.000000000");

    private final AgreementPricelistAPI a12 = pricelist("12", "2017-01-01 00:00:00.000000000");

    private final AgreementPricelistAPI a13 = pricelist("13", "2018-01-01 00:00:00.000000000");

    private final AgreementPricelistAPI a14 = pricelist("14", "2019-01-01 00:00:00.000000000");

    private final AgreementPricelistAPI a15 = pricelist("15", "2030-01-01 00:00:00.000000000");

    private final AgreementAPI agreementAPI = new AgreementAPI();

    @Test
    public void isDeprecated() {
        agreementAPI.setPricelists(Arrays.asList(a10, a11, a12, a13, a14, a15));

        assertTrue(migrationFilter.isDeprecated(agreementAPI, a10));
        assertTrue(migrationFilter.isDeprecated(agreementAPI, a11));

        assertFalse(migrationFilter.isDeprecated(agreementAPI, a12));
        assertFalse(migrationFilter.isDeprecated(agreementAPI, a13));
        assertFalse(migrationFilter.isDeprecated(agreementAPI, a14));
        assertFalse(migrationFilter.isDeprecated(agreementAPI, a15));

    }

    public AgreementPricelistAPI pricelist(String number, String validFromString) {
        AgreementPricelistAPI pricelist = new AgreementPricelistAPI();
        pricelist.setNumber(number);

        Timestamp date = Timestamp.valueOf(validFromString);
        LocalDateTime localDate = date.toLocalDateTime();
        Long validFrom = DateTimes.toEpochMillis(localDate);
        pricelist.setValidFrom(validFrom);

        return pricelist;
    }
}
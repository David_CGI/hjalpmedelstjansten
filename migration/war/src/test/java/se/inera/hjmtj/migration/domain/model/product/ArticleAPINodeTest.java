/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.product;

import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedAcyclicGraph;
import org.junit.Test;
import se.inera.hjalpmedelstjansten.model.api.CategoryAPI;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjmtj.migration.domain.Graphs;
import se.inera.hjmtj.migration.domain.exceptions.save.UnsupportedFitException;

import static org.junit.Assert.assertTrue;

public class ArticleAPINodeTest {

    private final DirectedAcyclicGraph<AssistiveProduct, DefaultEdge> graph = Graphs.directedAcyclicGraph();

    @Test
    public void addFit_ArticleT_To_Article() throws UnsupportedFitException {
        ArticleAPINode articleAPINode1 = new ArticleAPINode(graph, 1l);
        articleAPINode1.setCategory(category(Article.Type.T));

        ArticleAPINode articleAPINode2 = new ArticleAPINode(graph, 2l);
        articleAPINode2.setCategory(category(Article.Type.T));
        articleAPINode1.addFit(articleAPINode2);

        assertTrue(articleAPINode1.getFitsToArticles()
                                  .contains(articleAPINode2));
    }

    public CategoryAPI category(Article.Type type) {
        CategoryAPI categoryAPI = new CategoryAPI();
        categoryAPI.setArticleType(type);

        if (type.equals(Article.Type.H)) {
            categoryAPI.setCode("123456");
        }

        return categoryAPI;
    }

    @Test(expected = UnsupportedFitException.class)
    public void addFit_ArticleT_To_ArticleTwice() throws UnsupportedFitException {
        ArticleAPINode articleAPINode1 = new ArticleAPINode(graph, 1l);
        articleAPINode1.setCategory(category(Article.Type.T));

        ArticleAPINode articleAPINode2 = new ArticleAPINode(graph, 2l);
        articleAPINode2.setCategory(category(Article.Type.T));
        articleAPINode1.addFit(articleAPINode2);
        articleAPINode1.addFit(articleAPINode2);
    }

    @Test(expected = UnsupportedFitException.class)
    public void addFit_ArticleT_To_ArticleTj() throws UnsupportedFitException {
        ArticleAPINode articleAPINode1 = new ArticleAPINode(graph, 1l);
        articleAPINode1.setCategory(category(Article.Type.T));

        ArticleAPINode articleAPINode2 = new ArticleAPINode(graph, 2l);
        articleAPINode2.setCategory(category(Article.Type.Tj));
        articleAPINode1.addFit(articleAPINode2);
    }

    @Test(expected = UnsupportedFitException.class)
    public void addFit_ArticleT_To_ArticleT() throws UnsupportedFitException {
        ArticleAPINode articleAPINode1 = new ArticleAPINode(graph, 1l);
        articleAPINode1.setCategory(category(Article.Type.T));

        ArticleAPINode articleAPINode2 = new ArticleAPINode(graph, 2l);
        articleAPINode2.setCategory(category(Article.Type.R));
        articleAPINode1.addFit(articleAPINode2);
    }

    @Test(expected = UnsupportedFitException.class)
    public void addFit_ArticleT_To_ArticleI() throws UnsupportedFitException {
        ArticleAPINode articleAPINode1 = new ArticleAPINode(graph, 1l);
        articleAPINode1.setCategory(category(Article.Type.T));

        ArticleAPINode articleAPINode2 = new ArticleAPINode(graph, 2l);
        articleAPINode2.setCategory(category(Article.Type.I));
        articleAPINode1.addFit(articleAPINode2);
    }

    @Test
    public void addFit_ArticleT_To_ProductH() throws UnsupportedFitException {
        ProductAPINode productAPINode = new ProductAPINode(graph, 1l);
        productAPINode.setCategory(category(Article.Type.H));

        ArticleAPINode articleAPINode = new ArticleAPINode(graph, 2l);
        articleAPINode.setCategory(category(Article.Type.T));
        articleAPINode.addFit(productAPINode);

        assertTrue(articleAPINode.getFitsToProducts()
                                 .contains(productAPINode));
    }

    @Test(expected = UnsupportedFitException.class)
    public void addFit_ArticleT_To_ProductI() throws UnsupportedFitException {
        ProductAPINode productAPINode = new ProductAPINode(graph, 1l);
        productAPINode.setCategory(category(Article.Type.I));

        ArticleAPINode articleAPINode = new ArticleAPINode(graph, 2l);
        articleAPINode.setCategory(category(Article.Type.T));
        articleAPINode.addFit(productAPINode);
    }
}
/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.agreements.gp;

import org.junit.Test;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementRowBase;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class GeneralPricelistRowTest {

    @Test
    public void setGeneratedPriceWithPeriod() {
        BigDecimal price = new BigDecimal("123.5");
        assertEquals(123.5d, price.doubleValue(), 0.1d);

        AgreementRowBase agreementRowBase = new GeneralPricelistRow();
        agreementRowBase.setPrice(price);
        assertNotEquals(price, agreementRowBase.getPrice());
    }
}
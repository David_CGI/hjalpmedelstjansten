/*
 *
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 *  Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.product.entities;

import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

public class ProductCategoryTest {

    @Test
    public void parse() {
        LinkedList<ProductCategory> expected = new LinkedList<>();
        expected.add(new ProductCategory(null, "Produkt", null));
        expected.add(new ProductCategory(null, "Vara", expected.get(0)));
        expected.add(new ProductCategory(null, "ISO-klassificerade hjälpmedel (ISO 9999)", expected.get(1)));
        expected.add(new ProductCategory(null, "Hjälpmedelsområden", expected.get(2)));
        expected.add(new ProductCategory("04", "Personlig medicinsk behandling", expected.get(3)));
        expected.add(new ProductCategory("0406", "Cirkulationsbehandling", expected.get(4)));
        expected.add(new ProductCategory("040606", "Stödstrumpor och kompressionsstrumpor för armar och ben och andra delar av kroppen", expected.get(5)));
        expected.add(new ProductCategory(null, "Kompressionsstrumpa", expected.get(6)));

        ProductCategoryPath categories = new ProductCategoryPath();
        String categoryPath = "Produkt\\Vara\\ISO-klassificerade hjälpmedel (ISO 9999)\\Hjälpmedelsområden" +
                "\\04 Personlig medicinsk behandling\\0406 Cirkulationsbehandling" +
                "\\040606 Stödstrumpor och kompressionsstrumpor för armar och ben och andra delar av kroppen" +
                "\\Kompressionsstrumpa";
        categories.setCategoryPath(categoryPath);
        List<ProductCategory> actual = categories.categories();

        assertArrayEquals(expected.toArray(), actual.toArray());
        assertEquals(null, categories.leaf()
                                     .getCode());
        assertEquals("040606", categories.leaf()
                                         .getParent()
                                         .getCode());
        assertTrue(categories.isIso9999());
    }

    @Test
    public void parseAccessory() {
        LinkedList<ProductCategory> expected = new LinkedList<>();
        expected.add(new ProductCategory(null, "Produkt", null));
        expected.add(new ProductCategory(null, "Vara", expected.get(0)));
        expected.add(new ProductCategory(null, "ISO-klassificerade hjälpmedel (ISO 9999)", expected.get(1)));
        expected.add(new ProductCategory(null, "Tillbehör", expected.get(2)));

        ProductCategoryPath categories = new ProductCategoryPath();
        String categoryPath = "Produkt\\Vara\\ISO-klassificerade hjälpmedel (ISO 9999)\\Tillbehör";
        categories.setCategoryPath(categoryPath);
        List<ProductCategory> actual = categories.categories();

        assertArrayEquals(expected.toArray(), actual.toArray());
        assertEquals("Tillbehör", categories.leaf().getName());
        assertTrue(categories.isIso9999());
    }

    @Test
    public void parseNonISO9999() {
        ProductCategoryPath categories = new ProductCategoryPath();
        String categoryPath = "Produkt\\Vara\\Hjälpmedel vid funktionsnedsättning\\Förflyttning\\Gånghjälpmedel\\Gåbord";
        categories.setCategoryPath(categoryPath);

        assertFalse(categories.isIso9999());
    }
}
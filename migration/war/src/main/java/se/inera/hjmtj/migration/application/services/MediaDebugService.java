/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package se.inera.hjmtj.migration.application.services;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import se.inera.hjmtj.migration.application.bootstrap.LoadMedia;
import se.inera.hjmtj.migration.controller.UploadToS3Controller;
import se.inera.hjmtj.migration.domain.exceptions.save.ImageMigrationRunning;
import se.inera.hjmtj.migration.infra.organizations.SupplierRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.concurrent.locks.ReentrantLock;

@Slf4j
@ApplicationScoped
@Path("/imagemigration")
public class MediaDebugService {

    @Inject
    private UploadToS3Controller controller;

    @Inject
    private S3MediaService modelMigration;

    @Inject
    private LoadMedia prepareModel;

    private final ReentrantLock lock = new ReentrantLock();

    @Inject
    private SupplierRepository supplierRepository;

    public void tryAcquire() throws ImageMigrationRunning {
        if (!lock.tryLock()) {
            throw new ImageMigrationRunning();
        }
    }

    public void release() {
        if (lock.isHeldByCurrentThread()) {
            lock.unlock();
        }
    }

    @GET
    @Path("/s3")
    public String start() throws ImageMigrationRunning {
        tryAcquire();
        log.info("Started image migrating data!");
        try {
            int numberOfMigrated = controller.startImageMigrationPerSupplierForS3();
            return ("Started image migrating data!");
        } catch (Exception e) {
            log.info("service exception");
            log.info(e.getStackTrace()
                      .toString());
        } finally {
            release();
        }
        return null;
    }

    @GET
    @Path("/prepare")
    public String startPrepare() throws ImageMigrationRunning {
        tryAcquire();
        log.info("Started prepare for image migrating data!");
        try {
            prepareModel.startImageMigrationPerSupplierPrepareForModel();
            return ("Started image migrating data!");
        } catch (Exception e) {
            log.info("service exception");
            log.info(e.getStackTrace()
                      .toString());
        } finally {
            release();
        }
        return null;
    }

    @GET
    @Path("/model")
    public String startMigrationToModel() throws ImageMigrationRunning {
        tryAcquire();
        log.info("Started image migrating to model!");
        try {
            modelMigration.migrateSupplierImages(supplierRepository.suppliers());
            return ("Started image migrating data!");
        } catch (Exception e) {
            log.info("service exception");
            log.info(e.getStackTrace()
                      .toString());
        } finally {
            release();
        }
        return null;
    }

    @GET
    @Path("/progress")
    public String progress() throws ImageMigrationRunning {
        log.info("progress of image migrating data!");
        try {
            int numberOfMigrated = controller.getNumberOfImagesMigrated();
            return String.format("Number of images migrated %d", numberOfMigrated);
        } catch (Exception e) {
            log.info("service exception");
        }
        return null;
    }

    @GET
    @Path("/log")
    @Produces("text/plain")
    public String getLog() throws IOException {
        final File initialFile = new File("/wildfly/standalone/log/server.log");
        final InputStream targetStream = new DataInputStream(new FileInputStream(initialFile));
        String data = IOUtils.toString(targetStream, Charset.defaultCharset());
        return data;
    }
}

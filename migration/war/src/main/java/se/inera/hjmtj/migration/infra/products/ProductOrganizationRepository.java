/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.products;

import lombok.extern.slf4j.Slf4j;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedAcyclicGraph;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjmtj.migration.domain.Graphs;
import se.inera.hjmtj.migration.domain.HeapUsageInterceptor;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MessageTemplate;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.domain.model.product.ArticleAPINode;
import se.inera.hjmtj.migration.domain.model.product.AssistiveProduct;
import se.inera.hjmtj.migration.domain.model.product.ProductAPINode;
import se.inera.hjmtj.migration.domain.model.product.api.builder.AssistiveProductListBuilder;
import se.inera.hjmtj.migration.domain.model.product.api.builder.InventoryBuilderHelper;
import se.inera.hjmtj.migration.domain.model.product.entities.Product;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductOrganization;
import se.inera.hjmtj.migration.infra.categories.ProductCategorySpecificPropertyListValueTypeRepository;
import se.inera.hjmtj.migration.infra.categories.ProductCategorySpecificPropertyTypeRepository;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjCategoryRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjCvCeDirectiveRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjCvCeStandardRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.Validator;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;

@Slf4j
@Stateless
public class ProductOrganizationRepository {

    @PersistenceContext(unitName = "local")
    private EntityManager em;

    @Inject
    private ProductRepository productRepository;

    @Inject
    private InventoryBuilderHelper inventoryBuilderHelper;

    @Inject
    private HjmtjCategoryRepository hjmtjCategoryRepository;

    @Inject
    private ProductCategorySpecificPropertyListValueTypeRepository productCategorySpecificPropertyListValueTypeRepository;

    @Inject
    private ProductCategorySpecificPropertyTypeRepository productCategorySpecificPropertyTypeRepository;

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    @Inject
    private Validator validator;

    @Inject
    private HjmtjCvCeDirectiveRepository hjmtjCvCeDirectiveRepository;

    @Inject
    private HjmtjCvCeStandardRepository hjmtjCvCeStandardRepository;

    public List<AssistiveProduct> findAssistiveProducts(Supplier supplier, OrganizationAPI organizationAPI) {
        Objects.requireNonNull(supplier, "supplier must be non-null");
        Objects.requireNonNull(organizationAPI, "organizationAPI must be non-null");

        supplier = em.find(Supplier.class, supplier.getId());

        ProductOrganization productOrganization = supplier.getProductOrganization();
        Map<Long, Product> productsById = productOrganization != null && productOrganization.getProducts() != null ? productOrganization.getProducts()
                                                                                                                                        .stream()
                                                                                                                                        .collect(toMap(Product::getId, identity())) : Collections.emptyMap();

        DirectedAcyclicGraph<AssistiveProduct, DefaultEdge> graph = Graphs.directedAcyclicGraph();
        AssistiveProductListBuilder factory = new AssistiveProductListBuilder(new AssistiveProductListBuilder.Context(graph, productsById, inventoryBuilderHelper, productRepository, hjmtjCategoryRepository, productCategorySpecificPropertyTypeRepository, migrationErrorRepository, organizationAPI, validator, hjmtjCvCeDirectiveRepository, hjmtjCvCeStandardRepository));

        List<AssistiveProduct> result = factory.build();

        return Graphs.topological(graph);
    }

    public ProductOrganization findByIdentifier(String identifier) {
        List<ProductOrganization> list = em.createNamedQuery(ProductOrganization.FIND_BY_IDENTIFIER, ProductOrganization.class)
                                           .setParameter("identifier", identifier)
                                           .setMaxResults(1)
                                           .getResultList();

        return list.size() > 0 ? list.get(0) : null;
    }

    public List<ProductOrganization> values() {
        return em.createQuery("SELECT po FROM ProductOrganization po JOIN FETCH po.supplier", ProductOrganization.class)
                 .getResultList();
    }
}

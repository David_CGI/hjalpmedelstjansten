/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.organization.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.model.api.ElectronicAddressAPI;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.api.PostAddressAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCountryAPI;
import se.inera.hjalpmedelstjansten.model.entity.PostAddress;
import se.inera.hjmtj.migration.domain.DateTimes;
import se.inera.hjmtj.migration.domain.model.agreements.gp.GeneralPricelistOrganization;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.AgreementOrganization;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductOrganization;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjUserRepository;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PersistenceUnit;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;

/**
 * Created by jahwag on 2018-06-04.
 */
@PersistenceUnit(unitName = "local")
@Slf4j
@Entity
@NamedQueries({
        @NamedQuery(name = Supplier.FIND_ALL, query = "SELECT s FROM Supplier s "),
        @NamedQuery(name = Supplier.COUNT_ALL, query = "SELECT COUNT(o) FROM Supplier o "),
        @NamedQuery(name = Supplier.FIND_BY_NAME, query = "SELECT s FROM Supplier s WHERE trim(lower(s.name)) = :name "),
})
@Table(indexes = {
        @Index(columnList = "name"),
        @Index(columnList = "gln"),
})
@Getter
@Setter
public class Supplier implements Serializable {

    public static final LocalDate VALID_FROM_DEFAULT_VALUE = LocalDate.parse("2100-01-01");

    public static final String FIND_BY_NAME = "Supplier.FIND_BY_NAME";

    public static final String FIND_ALL = "Supplier.FIND_ALL";

    public static final String COUNT_ALL = "Supplier.COUNT_ALL";

    public static final String ORGANIZATION_TYPE_SUPPLIER = "SUPPLIER";

    private transient static final boolean MIGRATION_SET_EMAILS = Boolean.valueOf(System.getenv()
                                                                                        .getOrDefault("MIGRATION_SET_EMAILS", "false"));

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String gln;

    private String name;

    private String type;

    private String address;

    private String postCode;

    private String city;

    private String country;

    private String telephone;

    private String fax;

    private String email;

    private String organizationNumber;

    private LocalDate validFrom;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private Secret secret;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "supplier", optional = true)
    private GeneralPricelistOrganization generalPricelistOrganization;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "supplier", optional = true)
    private AgreementOrganization agreementOrganization;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "supplier", optional = true)
    private ProductOrganization productOrganization;

    public Supplier(VisueraOrganization visuera, LimeOrganization lime) {
        this(lime);
        this.fax = visuera.getFax();
        this.email = visuera.getEmail();

        if (city == null) {
            this.city = visuera.getCity();
            log.info(name + " city = " + visuera.getCity());
        }
        if (address == null) {
            this.address = visuera.getAddress();
            log.info(name + " address = " + visuera.getAddress());
        }
        if (postCode == null) {
            this.postCode = visuera.getPostCode();
            log.info(name + " postCode = " + visuera.getPostCode());
        }
        if (country == null) {
            this.country = visuera.getCountry();
            log.info(name + " country = " + visuera.getCountry());
        }
        if (organizationNumber == null) {
            this.organizationNumber = visuera.getOrganizationNumber();
            log.info(name + " organizationNumber = " + visuera.getOrganizationNumber());
        }
    }

    public Supplier(LimeOrganization csvLime) {
        String citySuffix = ", " + csvLime.getCity();
        this.gln = csvLime.getGln();
        this.name = csvLime.getName()
                           .replace(citySuffix, "");
        this.address = csvLime.getAddress();
        this.postCode = csvLime.getPostCode();
        this.city = csvLime.getCity();
        this.country = csvLime.getCountry();
        this.telephone = csvLime.getTelephone();
        this.type = "Leverantörer";
        this.organizationNumber = csvLime.getOrganizationNumber()
                                         .replace("-", "");
        this.validFrom = csvLime.getValidFrom();
    }

    public Supplier() {
    }

    public OrganizationAPI toApi(Map<String, CVCountryAPI> countryMap) {
        OrganizationAPI organization = new OrganizationAPI();
        Objects.requireNonNull(id, "id must be non-null");
        organization.setId(id);

        organization.setOrganizationName(name);
        organization.setOrganisationType(ORGANIZATION_TYPE_SUPPLIER);
        organization.setGln(gln);
        organization.setOrganizationNumber(organizationNumber);
        organization.setCountry(countryMap.get(country));
        ElectronicAddressAPI electronicAddress = new ElectronicAddressAPI();

        if (MIGRATION_SET_EMAILS) {
            electronicAddress.setEmail(email);
        } else {
            electronicAddress.setEmail(HjmtjUserRepository.createDummyEmail());
        }
        electronicAddress.setTelephone(telephone);
        electronicAddress.setFax(fax);
        organization.setElectronicAddress(electronicAddress);

        PostAddressAPI visitAddress = new PostAddressAPI();
        visitAddress.setAddressType(PostAddress.AddressType.VISIT);

        PostAddressAPI deliveryAddress = new PostAddressAPI();
        deliveryAddress.setPostCode(String.valueOf(postCode));
        deliveryAddress.setAddressType(PostAddress.AddressType.DELIVERY);
        deliveryAddress.setCity(city);
        deliveryAddress.setStreetAddress(address);

        organization.setPostAddresses(Arrays.asList(visitAddress, deliveryAddress));

        LocalDate validFrom = this.validFrom != null ? this.validFrom : VALID_FROM_DEFAULT_VALUE;
        organization.setValidFrom(DateTimes.toEpochMillis(validFrom));

        organization.setActive(this.validFrom != null && !this.validFrom
                .isAfter(LocalDate.now()));

        if (getGln() == null) {
            throw new IllegalArgumentException("GLN cannot be null");
        }

        return organization;
    }

    private boolean isPostBox(String streetAddress) {
        if (streetAddress == null) {
            return false;
        }

        String lowerCase = streetAddress.toLowerCase();
        return lowerCase.contains("postfach")
                || lowerCase.contains("postbox")
                || lowerCase.contains("box")
                || lowerCase.contains("p.o");
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Supplier supplier = (Supplier) o;
        return id.equals(supplier.id);
    }

}
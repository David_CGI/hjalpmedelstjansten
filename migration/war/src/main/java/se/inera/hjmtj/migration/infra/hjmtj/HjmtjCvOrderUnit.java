/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.hjmtj;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.model.api.cv.CVOrderUnitAPI;

import javax.annotation.PostConstruct;
import javax.ejb.DependsOn;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.util.function.Function.identity;

@Slf4j
@Singleton
public class HjmtjCvOrderUnit {

    private final Map<String, CVOrderUnitAPI> codes = new HashMap<>();

    private CVOrderUnitAPI pieces;

    @PersistenceContext(unitName = "HjmtjPU")
    private EntityManager em;

    public void initializeCache() {
        try {
            List<Object[]> rs = em.createNativeQuery("SELECT uniqueId,code FROM CVOrderUnit")
                                  .getResultList();
            codes.putAll(rs.stream()
                           .map(this::map)
                           .collect(Collectors.toMap(CVOrderUnitAPI::getCode, identity())));

            pieces = findByCode("PCE");
        } catch (Exception t) {
            log.error("{}", t);
        }
    }

    public CVOrderUnitAPI findByCode(String code) {
        Objects.requireNonNull(code, "code must be non-null");

        return codes.get(code);
    }

    private CVOrderUnitAPI map(Object[] arr) {
        CVOrderUnitAPI cvOrderUnitAPI = new CVOrderUnitAPI();
        cvOrderUnitAPI.setId(((BigInteger) arr[0]).longValue());
        cvOrderUnitAPI.setCode((String) arr[1]);

        return cvOrderUnitAPI;
    }

    public CVOrderUnitAPI pieces() {
        return pieces;
    }

}

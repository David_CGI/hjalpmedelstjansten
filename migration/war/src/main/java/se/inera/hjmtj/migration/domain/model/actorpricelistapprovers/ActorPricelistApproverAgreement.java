/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.actorpricelistapprovers;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementHead;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.Agreement;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PersistenceUnit;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Set;

@PersistenceUnit(unitName = "local")
@Entity
@Slf4j
@XmlRootElement(name = "Agreement")
@Table(indexes = {
        @Index(columnList = "identifier"),
})
@RequiredArgsConstructor
@Getter
@Setter
public class ActorPricelistApproverAgreement {

    @Id
    private long id;

    @Column(nullable = false, unique = true)
    @XmlElement(name = "Identifier")
    private String identifier;

    @XmlElement(name = "Head")
    private AgreementHead head;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @XmlElement(name = "Buyer")
    private ActorPricelistApproverAgreementParty buyer;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @XmlElement(name = "Seller")
    private ActorPricelistApproverAgreementParty seller;

    @OneToOne(fetch = FetchType.EAGER)
    private Agreement agreement;

    @XmlAttribute(name = "id")
    @XmlID
    public String getIdAsString() {
        return String.valueOf(id);
    }

    public void setIdAsString(String xmlId) {
        id = Long.parseLong(xmlId);
    }

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "approves")
    private Set<ActorPricelistApproverActorRole> approvers;

}

/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.agreements;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.domain.xml.TrimStringAdapter;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.MappedSuperclass;
import javax.persistence.PersistenceUnit;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import static javax.persistence.InheritanceType.TABLE_PER_CLASS;

@PersistenceUnit(unitName = "local")
@Inheritance(strategy = TABLE_PER_CLASS)
@MappedSuperclass
@Slf4j
@XmlTransient
@RequiredArgsConstructor
@Getter
@Setter
public abstract class AgreementRowBase<P extends AgreementProductBase, S extends AgreementStatusBase, W extends AgreementWarrantyBase> implements Serializable {

    public static final boolean USE_GENERATED_VALUE_FOR_PRICE = Boolean.valueOf(System.getenv()
                                                                                      .getOrDefault("USE_GENERATED_VALUE_FOR_PRICE", "true"));

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long generatedId;

    @XmlJavaTypeAdapter(TrimStringAdapter.class)
    @XmlElement(name = "SupplierPriceListNo")
    private String supplierPriceListNumber;

    @XmlElement(name = "LeadTimeDays")
    private int leadTimeDays;

    @XmlElement(name = "MinOrderQty")
    private Long minOrderQuantity;

    private BigDecimal price;

    @XmlElement(name = "PriceValidFromTime")
    private LocalDateTime validFrom;

    @XmlElement(name = "QtyStep")
    private int incrementBy;

    public abstract P getAgreementArticle();

    public abstract S getStatus();

    public abstract W getWarranty();

    @XmlElement(name = "Price")
    public void setPrice(BigDecimal price) {
        if (USE_GENERATED_VALUE_FOR_PRICE) {
            try {
                this.price = new BigDecimal(Math.random()).multiply(new BigDecimal(1000))
                                                          .setScale(2, BigDecimal.ROUND_HALF_UP);
            } catch (NumberFormatException e) {
                log.error("{}", e);
            }
        } else {
            this.price = price;
        }
    }

}

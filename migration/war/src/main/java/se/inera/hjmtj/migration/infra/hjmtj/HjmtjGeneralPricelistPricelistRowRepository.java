/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.hjmtj;


import lombok.extern.slf4j.Slf4j;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistPricelistRowMapper;
import se.inera.hjalpmedelstjansten.business.product.controller.CVPreventiveMaintenanceController;
import se.inera.hjalpmedelstjansten.business.product.controller.GuaranteeUnitController;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelist;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelistRow;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;
import se.inera.hjmtj.migration.domain.DateTimes;
import se.inera.hjmtj.migration.domain.exceptions.save.PriceListRowArticleNotFound;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.Date;

import static java.util.concurrent.TimeUnit.HOURS;

@Slf4j
@Stateless
@TransactionTimeout(unit = HOURS, value = 5)
public class HjmtjGeneralPricelistPricelistRowRepository {

    @PersistenceContext(unitName = "HjmtjPU")
    private EntityManager em;

    @Inject
    private CVPreventiveMaintenanceController cVPreventiveMaintenanceController;

    @Inject
    private GuaranteeUnitController guaranteeUnitController;

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    public SaveResult<Long> save(GeneralPricelistPricelistAPI generalPricelistPricelistAPI, GeneralPricelistPricelistRowAPI generalPricelistPricelistRowAPI, LocalDateTime created) throws PriceListRowArticleNotFound {
        GeneralPricelistPricelist generalPricelistPricelist = em.getReference(GeneralPricelistPricelist.class, generalPricelistPricelistAPI.getId());
        GeneralPricelistPricelistRow generalPricelistPricelistRow = GeneralPricelistPricelistRowMapper.map(generalPricelistPricelistRowAPI, new Date(generalPricelistPricelistRowAPI.getValidFrom()));
        generalPricelistPricelistRow.setPricelist(generalPricelistPricelist);

        // set timestamps
        Long createdMillis = DateTimes.toEpochMillis(created);
        generalPricelistPricelistRow.setCreated(createdMillis != null ? new Date(createdMillis) : null);
        generalPricelistPricelistRow.setLastUpdated(createdMillis != null ? new Date(createdMillis) : null);

        // article
        ArticleAPI articleAPI = generalPricelistPricelistRowAPI.getArticle();
        Article article = em.find(Article.class, articleAPI
                .getId());
        if (article == null) {
            // This is a cascade failure
            throw new PriceListRowArticleNotFound(articleAPI);

        }
        generalPricelistPricelistRow.setArticle(article);

        Article.Type articleType = generalPricelistPricelistRow.getArticle()
                                                               .getBasedOnProduct() == null ? generalPricelistPricelistRow.getArticle()
                                                                                                                          .getCategory()
                                                                                                                          .getArticleType() : generalPricelistPricelistRow.getArticle()
                                                                                                                                                                          .getBasedOnProduct()
                                                                                                                                                                          .getCategory()
                                                                                                                                                                          .getArticleType();
        GeneralPricelistPricelistRowMapper.mapUpdatableFields(generalPricelistPricelistRowAPI, generalPricelistPricelistRow, articleType, true);
        setOverrideWarrantyValidFrom(generalPricelistPricelistRowAPI, generalPricelistPricelistRow, articleType, new UserAPI(), true);

        // set overridden warranty quantity unit (if any)
        setOverriddenWarrantyQuantityUnit(generalPricelistPricelistRow, generalPricelistPricelistRowAPI, true);

        // set approved
        generalPricelistPricelistRow.setStatus(GeneralPricelistPricelistRow.Status.valueOf(generalPricelistPricelistRowAPI.getStatus()));

        em.persist(generalPricelistPricelistRow);

        log.info("Saved GP pricelist row with article {} status {} ", articleAPI
                .getId(), generalPricelistPricelistRow.getStatus());


        return SaveResult.of(generalPricelistPricelistRow.getUniqueId(), generalPricelistPricelistRowAPI);
    }

    private void setOverrideWarrantyValidFrom(GeneralPricelistPricelistRowAPI pricelistRowAPI, GeneralPricelistPricelistRow pricelistRow, Article.Type articleType, UserAPI userAPI, boolean isCreate) {
        if (isCreate && pricelistRowAPI.getWarrantyValidFrom() == null) {
            // do nothing, default to inherit
        } else {
            if (pricelistRow.isWarrantyValidFromOverridden()) {
                CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(pricelistRowAPI.getWarrantyValidFrom()
                                                                                                                            .getCode());
                if (preventiveMaintenance != null) {
                    pricelistRow.setWarrantyValidFrom(preventiveMaintenance);
                }
            } else {
                boolean overridden = false;
                if (pricelistRowAPI.getWarrantyValidFrom() != null) {
                    CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(pricelistRowAPI.getWarrantyValidFrom()
                                                                                                                                .getCode());
                    if (articleType == Article.Type.H) {
                        if (pricelistRow.getPricelist()
                                        .getGeneralPricelist()
                                        .getWarrantyValidFromH() == null) {
                            overridden = true;
                        } else {
                            if (!preventiveMaintenance.getCode()
                                                      .equals(pricelistRow.getPricelist()
                                                                          .getGeneralPricelist()
                                                                          .getWarrantyValidFromH()
                                                                          .getCode())) {
                                overridden = true;
                            }
                        }
                    } else if (articleType == Article.Type.I) {
                        if (pricelistRow.getPricelist()
                                        .getGeneralPricelist()
                                        .getWarrantyValidFromI() == null) {
                            overridden = true;
                        } else {
                            if (!preventiveMaintenance.getCode()
                                                      .equals(pricelistRow.getPricelist()
                                                                          .getGeneralPricelist()
                                                                          .getWarrantyValidFromI()
                                                                          .getCode())) {
                                overridden = true;
                            }
                        }
                    } else if (articleType == Article.Type.R) {
                        if (pricelistRow.getPricelist()
                                        .getGeneralPricelist()
                                        .getWarrantyValidFromR() == null) {
                            overridden = true;
                        } else {
                            if (!preventiveMaintenance.getCode()
                                                      .equals(pricelistRow.getPricelist()
                                                                          .getGeneralPricelist()
                                                                          .getWarrantyValidFromR()
                                                                          .getCode())) {
                                overridden = true;
                            }
                        }
                    } else if (articleType == Article.Type.T) {
                        if (pricelistRow.getPricelist()
                                        .getGeneralPricelist()
                                        .getWarrantyValidFromT() == null) {
                            overridden = true;
                        } else {
                            if (!preventiveMaintenance.getCode()
                                                      .equals(pricelistRow.getPricelist()
                                                                          .getGeneralPricelist()
                                                                          .getWarrantyValidFromT()
                                                                          .getCode())) {
                                overridden = true;
                            }
                        }
                    } else if (articleType == Article.Type.Tj) {
                        if (pricelistRow.getPricelist()
                                        .getGeneralPricelist()
                                        .getWarrantyValidFromTJ() == null) {
                            overridden = true;
                        } else {
                            if (!preventiveMaintenance.getCode()
                                                      .equals(pricelistRow.getPricelist()
                                                                          .getGeneralPricelist()
                                                                          .getWarrantyValidFromTJ()
                                                                          .getCode())) {
                                overridden = true;
                            }
                        }
                    }
                    if (overridden) {
                        pricelistRow.setWarrantyValidFrom(preventiveMaintenance);
                        pricelistRow.setWarrantyValidFromOverridden(true);
                    }
                } else {
                    if (articleType == Article.Type.H && pricelistRow.getPricelist()
                                                                     .getGeneralPricelist()
                                                                     .getWarrantyValidFromH() != null) {
                        overridden = true;
                    } else if (articleType == Article.Type.I && pricelistRow.getPricelist()
                                                                            .getGeneralPricelist()
                                                                            .getWarrantyValidFromI() != null) {
                        overridden = true;
                    } else if (articleType == Article.Type.R && pricelistRow.getPricelist()
                                                                            .getGeneralPricelist()
                                                                            .getWarrantyValidFromR() != null) {
                        overridden = true;
                    } else if (articleType == Article.Type.T && pricelistRow.getPricelist()
                                                                            .getGeneralPricelist()
                                                                            .getWarrantyValidFromT() != null) {
                        overridden = true;
                    } else if (articleType == Article.Type.Tj && pricelistRow.getPricelist()
                                                                             .getGeneralPricelist()
                                                                             .getWarrantyValidFromTJ() != null) {
                        overridden = true;
                    }
                    if (overridden) {
                        pricelistRow.setWarrantyValidFrom(null);
                        pricelistRow.setWarrantyValidFromOverridden(true);
                    }
                }
            }
        }
    }

    private void setOverriddenWarrantyQuantityUnit(GeneralPricelistPricelistRow pricelistRow, GeneralPricelistPricelistRowAPI pricelistRowAPI, boolean isCreate) {
        if (isCreate && pricelistRowAPI.getWarrantyQuantityUnit() == null) {
            // do nothing, default to inherit
        } else {
            if (pricelistRow.isWarrantyQuantityUnitOverridden()) {
                if (pricelistRowAPI.getWarrantyQuantityUnit() != null) {
                    CVGuaranteeUnit unit = guaranteeUnitController.getGuaranteeUnit(pricelistRowAPI.getWarrantyQuantityUnit()
                                                                                                   .getId());
                    if (unit != null) {
                        pricelistRow.setWarrantyQuantityUnit(unit);
                    }
                } else {
                    pricelistRow.setWarrantyQuantityUnit(null);
                }
            } else {
                Article article = pricelistRow.getArticle();
                Article.Type articleType = article.getBasedOnProduct() == null ? article.getCategory()
                                                                                        .getArticleType() : article.getBasedOnProduct()
                                                                                                                   .getCategory()
                                                                                                                   .getArticleType();
                boolean overridden = false;
                if (pricelistRowAPI.getWarrantyQuantityUnit() == null || pricelistRowAPI.getWarrantyQuantityUnit()
                                                                                        .getId() == null) {
                    // no unit sent in API, check whether it is different from agreement
                    if (articleType == Article.Type.H && pricelistRow.getPricelist()
                                                                     .getGeneralPricelist()
                                                                     .getWarrantyQuantityHUnit() != null) {
                        overridden = true;
                    } else if (articleType == Article.Type.I && pricelistRow.getPricelist()
                                                                            .getGeneralPricelist()
                                                                            .getWarrantyQuantityIUnit() != null) {
                        overridden = true;
                    } else if (articleType == Article.Type.R && pricelistRow.getPricelist()
                                                                            .getGeneralPricelist()
                                                                            .getWarrantyQuantityRUnit() != null) {
                        overridden = true;
                    } else if (articleType == Article.Type.T && pricelistRow.getPricelist()
                                                                            .getGeneralPricelist()
                                                                            .getWarrantyQuantityTUnit() != null) {
                        overridden = true;
                    } else if (articleType == Article.Type.Tj && pricelistRow.getPricelist()
                                                                             .getGeneralPricelist()
                                                                             .getWarrantyQuantityTJUnit() != null) {
                        overridden = true;
                    }
                    if (overridden) {
                        pricelistRow.setWarrantyQuantityUnit(null);
                        pricelistRow.setWarrantyQuantityUnitOverridden(true);
                    }
                } else {
                    // unit sent in API
                    if (articleType == Article.Type.H) {
                        if (pricelistRow.getPricelist()
                                        .getGeneralPricelist()
                                        .getWarrantyQuantityHUnit() == null ||
                                !pricelistRow.getPricelist()
                                             .getGeneralPricelist()
                                             .getWarrantyQuantityHUnit()
                                             .getUniqueId()
                                             .equals(pricelistRowAPI.getWarrantyQuantityUnit()
                                                                    .getId())) {
                            overridden = true;
                        }
                    } else if (articleType == Article.Type.R) {
                        if (pricelistRow.getPricelist()
                                        .getGeneralPricelist()
                                        .getWarrantyQuantityRUnit() == null ||
                                !pricelistRow.getPricelist()
                                             .getGeneralPricelist()
                                             .getWarrantyQuantityRUnit()
                                             .getUniqueId()
                                             .equals(pricelistRowAPI.getWarrantyQuantityUnit()
                                                                    .getId())) {
                            overridden = true;
                        }
                    } else if (articleType == Article.Type.I) {
                        if (pricelistRow.getPricelist()
                                        .getGeneralPricelist()
                                        .getWarrantyQuantityIUnit() == null ||
                                !pricelistRow.getPricelist()
                                             .getGeneralPricelist()
                                             .getWarrantyQuantityIUnit()
                                             .getUniqueId()
                                             .equals(pricelistRowAPI.getWarrantyQuantityUnit()
                                                                    .getId())) {
                            overridden = true;
                        }
                    } else if (articleType == Article.Type.T) {
                        if (pricelistRow.getPricelist()
                                        .getGeneralPricelist()
                                        .getWarrantyQuantityTUnit() == null ||
                                !pricelistRow.getPricelist()
                                             .getGeneralPricelist()
                                             .getWarrantyQuantityTUnit()
                                             .getUniqueId()
                                             .equals(pricelistRowAPI.getWarrantyQuantityUnit()
                                                                    .getId())) {
                            overridden = true;
                        }
                    } else if (articleType == Article.Type.Tj) {
                        if (pricelistRow.getPricelist()
                                        .getGeneralPricelist()
                                        .getWarrantyQuantityTJUnit() == null ||
                                !pricelistRow.getPricelist()
                                             .getGeneralPricelist()
                                             .getWarrantyQuantityTJUnit()
                                             .getUniqueId()
                                             .equals(pricelistRowAPI.getWarrantyQuantityUnit()
                                                                    .getId())) {
                            overridden = true;
                        }
                    }
                    if (overridden) {
                        CVGuaranteeUnit unit = guaranteeUnitController.getGuaranteeUnit(pricelistRowAPI.getWarrantyQuantityUnit()
                                                                                                       .getId());
                        if (unit != null) {
                            pricelistRow.setWarrantyQuantityUnit(unit);
                            pricelistRow.setWarrantyQuantityUnitOverridden(true);
                        }
                    }
                }
            }
        }
    }
}

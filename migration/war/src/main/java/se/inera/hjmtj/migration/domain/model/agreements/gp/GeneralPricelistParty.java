/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.agreements.gp;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import se.inera.hjmtj.migration.domain.model.Reference;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementPartyBase;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PersistenceUnit;
import javax.xml.bind.annotation.XmlElement;
import java.util.Collection;
import java.util.Set;

@PersistenceUnit(unitName = "local")
@Entity
@RequiredArgsConstructor
@Getter
@Setter
public class GeneralPricelistParty extends AgreementPartyBase<GeneralPricelistOrganization> {

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private GeneralPricelistOrganization agreementOrganization;

    @OneToMany(mappedBy = "seller")
    private Set<GeneralPricelist> generalPricelist;

    @XmlElement(name = "Organization")
    public void setXmlOrganization(Reference<GeneralPricelistOrganization> reference) {
        agreementOrganization = reference.getReference();
    }

}

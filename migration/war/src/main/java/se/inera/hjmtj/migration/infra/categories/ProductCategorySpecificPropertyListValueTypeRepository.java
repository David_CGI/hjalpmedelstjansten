/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.categories;

import lombok.extern.slf4j.Slf4j;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.domain.model.categories.ProductCategorySpecificPropertyListValueType;
import se.inera.hjmtj.migration.domain.xml.DepthFirstVisitor;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static javax.ejb.TransactionAttributeType.REQUIRES_NEW;

@Slf4j
@Stateless
@Interceptors({PerformanceLogInterceptor.class})
public class ProductCategorySpecificPropertyListValueTypeRepository {

    private static final String CODE = "Code";

    private static final String XS_COMPLEX_TYPE = "xs:complexType";

    private final DepthFirstVisitor visitor = new DepthFirstVisitor("/data.xsd");

    private final Set<String> ignoredTags = Stream.of("Enhet", "Classification", "ProdMarkStandard", "ProdMarkDirective", "_x0039_aa56166cb7942018e809ea09653ee1e")
                                                  .collect(Collectors.toSet());

    @PersistenceContext(unitName = "local")
    private EntityManager em;

    public Set<String> names() {
        return new HashSet<>(em.createQuery("SELECT DISTINCT name FROM ProductCategorySpecificPropertyListValueType ", String.class)
                               .getResultList());
    }

    private ProductCategorySpecificPropertyListValueType map(Node node) {
        String name = DepthFirstVisitor.attribute(node, "name");

        if (name != null && name.equals(CODE)) {
            Node grandParent = node.getParentNode()
                                   .getParentNode();

            if (grandParent.getNodeName()
                           .equals(XS_COMPLEX_TYPE)) {

                String grandParentName = DepthFirstVisitor.attribute(grandParent, "name");
                Objects.requireNonNull(grandParentName, "grandParentName must be non-null");

                // We are only interested in complex types that can be referenced
                if (hasId(grandParent) && !isIgnored(grandParentName)) {
                    return new ProductCategorySpecificPropertyListValueType(grandParentName);
                }
            }
        }

        return null;
    }

    private boolean hasId(Node grandParent) {
        NodeList uncleNodes = grandParent.getChildNodes();
        for (int j = 0; j < uncleNodes.getLength(); j++) {
            Node item = uncleNodes.item(j);
            String uncleNameAttributeValue = DepthFirstVisitor.attribute(item, "name");

            if (uncleNameAttributeValue != null && uncleNameAttributeValue.equals("id")) {
                return true;
            }
        }

        return false;
    }

    private boolean isIgnored(String grandParentName) {
        return ignoredTags.contains(grandParentName);
    }

    @TransactionAttribute(REQUIRES_NEW)
    public void loadFromXSD() {
        Set<ProductCategorySpecificPropertyListValueType> categorySpecificEnumerations = visitor.visit(this::map);
        log.info("Found {} ProductCategorySpecificPropertyListValueType in XSD (with duplicates removed)", categorySpecificEnumerations.size());

        for (ProductCategorySpecificPropertyListValueType xmlCategorySpecificPropertyListValueType : categorySpecificEnumerations) {
            em.persist(xmlCategorySpecificPropertyListValueType);
        }
    }

}

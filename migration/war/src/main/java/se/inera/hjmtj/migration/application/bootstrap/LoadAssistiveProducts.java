/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.application.bootstrap;

import lombok.extern.slf4j.Slf4j;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjmtj.migration.domain.HeapUsageInterceptor;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MessageTemplate;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.domain.model.product.entities.Product;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductCategoryProduct;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductCategorySpecificProperty;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductCategorySpecificPropertyListValue;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductClassification;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductDocumentType;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductMarkingDirective;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductMarkingStandard;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductOrganization;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductUnit;
import se.inera.hjmtj.migration.domain.model.result.AssistiveProductReport;
import se.inera.hjmtj.migration.domain.xml.ElementContextComparator;
import se.inera.hjmtj.migration.domain.xml.XMLFile;
import se.inera.hjmtj.migration.infra.categories.CategoryRepository;
import se.inera.hjmtj.migration.infra.categories.CategorySpecificPropertyRepository;
import se.inera.hjmtj.migration.infra.categories.ProductCategorySpecificPropertyListValueTypeRepository;
import se.inera.hjmtj.migration.infra.categories.ProductCategorySpecificPropertyTypeRepository;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;
import se.inera.hjmtj.migration.infra.io.BatchPersistence;
import se.inera.hjmtj.migration.infra.organizations.SupplierRepository;
import se.inera.hjmtj.migration.infra.products.ProductRepository;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.concurrent.TimeUnit.HOURS;

/**
 * Loads products file into H2.
 */
@Slf4j
@Singleton
@TransactionTimeout(unit = HOURS, value = 5)
@Interceptors({PerformanceLogInterceptor.class, HeapUsageInterceptor.class})
public class LoadAssistiveProducts {

    public static final ElementContextComparator PERSIST_ORDER = new ElementContextComparator(
            ProductDocumentType.class,
            ProductMarkingStandard.class,
            ProductMarkingDirective.class,
            ProductOrganization.class,
            ProductUnit.class,
            ProductClassification.class,
            ProductCategoryProduct.class,
            ProductCategorySpecificPropertyListValue.class,
            Product.class);

    private static final String PRODUCT_URL_FILE = System.getenv()
                                                         .getOrDefault("PRODUCT_URL_FILE", "<file>");

    @PersistenceContext(unitName = "local")
    private EntityManager em;

    @Inject
    private ProductCategorySpecificPropertyListValueTypeRepository productCategorySpecificPropertyListValueTypeRepository;

    @Inject
    private CategoryRepository categoryRepository;

    @Inject
    private CategorySpecificPropertyRepository categorySpecificPropertyRepository;

    @Inject
    private ProductCategorySpecificPropertyTypeRepository productCategorySpecificPropertyTypeRepository;

    @Inject
    private ProductRepository productRepository;

    @Inject
    private BatchPersistence batchPersistence;

    @Inject
    private SupplierRepository supplierRepository;

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    public void run() {
        if (productRepository.isEmpty()) {
            productRepository.clear();

            saveAll();

            setFlags();
        }

        // If enabled generate source
        productCategorySpecificPropertyTypeRepository.generateSourceCode(productCategorySpecificPropertyTypeRepository.values());
    }

    private void saveAll() {
        categoryRepository.loadFromCsv();
        categorySpecificPropertyRepository.loadFromCsv();
        productCategorySpecificPropertyTypeRepository.loadFromXSD();
        productCategorySpecificPropertyListValueTypeRepository.loadFromXSD();

        XMLFile xmlFile = new XMLFile(PERSIST_ORDER)
                .register(ProductCategorySpecificPropertyListValue.class, productCategorySpecificPropertyListValueTypeRepository.names());

        List<Object> xmlObjects = xmlFile.loadFromFTP(PRODUCT_URL_FILE)
                                         .stream()
                                         .map(this::decorate)
                                         .collect(Collectors.toList());

        batchPersistence.save(xmlObjects, em);
    }

    private void setFlags() {
        for (Product product : getProducts()) {
            boolean templateProduct = Optional.ofNullable(product.getIsTemplateProduct())
                                              .orElse(false);
            boolean basedOnTemplateProduct = product.getBasedOnTemplateProduct() != null;

            if (templateProduct) {
                product.setType(AssistiveProductReport.Type.PRODUCT);
            } else if (basedOnTemplateProduct) {
                product.setType(AssistiveProductReport.Type.ARTICLE);
            } else {
                product.setType(AssistiveProductReport.Type.ARTICLE);
            }

            if (product.getType()
                       .equals(AssistiveProductReport.Type.ARTICLE) &&
                    product.getBasedOnTemplateProduct() == null &&
                    product.countFits() == 0) {
                product.setNoArticleType(true);
            }
        }
    }

    public void fixDuplicateNumbers() {
        log.info("Fixing any duplicate assistive product numbers...");

        List<Object[]> resultList = em.createNativeQuery("SELECT trim(lower(p.supplierProductNumber)), group_concat(p.VGRPRODNO)\n" +
                "FROM PRODUCT p\n" +
                "group by p.SUPPLIER_ID, p.TYPE, trim(lower(p.SUPPLIERPRODUCTNUMBER))\n" +
                "having count(*) > 1")
                                      .getResultList();
        log.info("Found {} duplicate numbers", resultList.size());

        for (Object[] array : resultList) {
            String supplierProductNumber = array[0].toString()
                                                   .trim();
            List<String> catalogueUniqueNumbers = array[1] != null ? Arrays.asList(array[1].toString()
                                                                                           .split(",")) : Collections.emptyList();

            for (int j = 0; j < catalogueUniqueNumbers.size(); j++) {
                String catalogueUniqueNumber = catalogueUniqueNumbers.get(j);
                String newNumber = supplierProductNumber + "-" + (j + 1);

                em.createQuery("UPDATE Product SET supplierProductNumber = :supplierProductNumber WHERE vgrProdNo = :vgrProdNo")
                  .setParameter("supplierProductNumber", newNumber)
                  .setParameter("vgrProdNo", Long.valueOf(catalogueUniqueNumber))
                  .executeUpdate();

                MigrationError migrationError = new MigrationError();
                migrationError.setSeverity(MigrationError.Severity.WARNING);
                migrationError.setCatalogueUniqueNumber(Long.valueOf(catalogueUniqueNumber));
                migrationError.setClassifier(Classifier.HJALPMEDEL_ARTICLE_NUMBER);
                migrationError.setMessage(MessageTemplate.ARTICLE_NUMBER_WAS_CHANGED, supplierProductNumber, newNumber);
                migrationErrorRepository.add(migrationError);
                log.info("Product number {} changed to {}", supplierProductNumber, newNumber);
            }
        }
    }

    private Object decorate(Object xmlObject) {
        if (xmlObject instanceof Product) {
            setCategorySpecificListValue((Product) xmlObject);
        } else if (xmlObject instanceof ProductOrganization) {
            setOrganization((ProductOrganization) xmlObject);
        }

        return xmlObject;
    }

    public List<Product> getProducts() {
        return em.createQuery("SELECT p FROM Product p", Product.class)
                 .getResultList();
    }

    private void setCategorySpecificListValue(Product product) {
        for (ProductCategorySpecificProperty productCategorySpecificProperty : product.getCategorySpecificProperties()) {
            ProductCategorySpecificPropertyListValue listValue = productCategorySpecificProperty.getListValue();
            if (listValue != null) {
                Long listValueId = listValue.getId();
                Objects.requireNonNull(listValueId, "listValueId must be non-null");
                listValue = em.getReference(ProductCategorySpecificPropertyListValue.class, listValueId);
                if (listValue == null) {
                    throw new IllegalStateException("ProductCategorySpecificPropertyListValue with id " + listValueId + " not found");
                }
                productCategorySpecificProperty.setListValue(listValue);
            }
        }
    }

    private void setOrganization(ProductOrganization productOrganization) {
        // Smith-Nephew AB are not exported because they have no assistive products as of May,
        // but if they are included in the future we need to patch their details to migrate their inventory
        String name = productOrganization.getName();
        if (name.equalsIgnoreCase("Smith & Nephew AB")) {
            productOrganization.setGln("7381000022401");
        }

        // 1. Mediq Sverige Exclusive AB will be removed
        // 2. Niklassons Gummi- o Mekaniska Verkstads AB has same GLN as Maxgrepp Handrims AB
        // and is not a subscriber. We have to remove it to prevent non-unique matches.
        if (!name.equalsIgnoreCase("Mediq Sverige Exclusive AB") &&
                !name.equalsIgnoreCase("Niklassons Gummi- o Mekaniska Verkstads AB")) {
            productOrganization.setSupplier(supplierRepository.getSupplier(productOrganization));
        }


    }

    public void fixCategorySpecificPropertyListValues() {
        replaceListValue("Urmarkarlupp", "Urmakarlupp");
        replaceListValue("Linshuvud för handtag", "Linshuvud med handtag");
        replaceListValue("Typ \"Standardttangentbord\"", "Typ \"Standardtangentbord\"");
        replaceListValue("variabelt antal tryckytor", "variabelt med antal tryckytor");
        replaceListValue("Av brukaren med drivringar", "Av brukare med drivringar");
    }

    private void replaceListValue(String oldValue, String newValue) {
        if (em.createNativeQuery("UPDATE PRODUCTCATEGORYSPECIFICPROPERTYLISTVALUE SET VALUE = :newValue WHERE VALUE = :oldValue")
              .setParameter("oldValue", oldValue)
              .setParameter("newValue", newValue)
              .executeUpdate() > 0) {
            log.info("Changed CategorySpecificPropertyListValue '{}' to '{}'", oldValue, newValue);
        } else {
            log.info("CategorySpecificPropertyListValue '{}' was not updated to '{}' (only to be expected on redeploy)", oldValue, newValue);
        }
    }

}

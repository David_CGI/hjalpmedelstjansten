/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.product;

import lombok.experimental.Accessors;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedAcyclicGraph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.CategoryAPI;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCEDirectiveAPI;
import se.inera.hjmtj.migration.domain.exceptions.save.UnsupportedFitException;

@Accessors(fluent = true)
public interface AssistiveProduct {

    Logger log = LoggerFactory.getLogger(AssistiveProduct.class);

    default void addDependency(ArticleAPI articleAPI) {
        try {
            if (articleAPI != null && articleAPI instanceof ArticleAPINode) {
                graph().addVertex((AssistiveProduct) articleAPI);
                graph().addEdge((AssistiveProduct) articleAPI, this);
            }
        } catch (IllegalArgumentException e) {
            log.debug("Cycles cannot be migrated");
        }
    }

    DirectedAcyclicGraph<AssistiveProduct, DefaultEdge> graph();

    default void addDependency(ProductAPI productAPI) {
        try {
            if (productAPI != null && productAPI instanceof ProductAPINode) {
                graph().addVertex((AssistiveProduct) productAPI);
                graph().addEdge((AssistiveProduct) productAPI, this);
            }
        } catch (IllegalArgumentException e) {
            log.debug("Cycles cannot be migrated");
        }
    }

    Long getId();

    String getOrganizationName();

    String getNumber();

    CategoryAPI getCategory();

    Long getCatalogueUniqueNumber();

    void setCatalogueUniqueNumber(Long catalogueUniqueNumber);

    void addFit(AssistiveProduct to) throws UnsupportedFitException;

    void setCeDirective(CVCEDirectiveAPI ceDirective);

    void setCeMarked(boolean ceMarked);

}

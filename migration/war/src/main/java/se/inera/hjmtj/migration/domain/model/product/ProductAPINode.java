/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.product;

import lombok.extern.slf4j.Slf4j;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedAcyclicGraph;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjmtj.migration.domain.exceptions.save.UnsupportedFitException;

import java.util.LinkedList;
import java.util.Objects;

@Slf4j
public class ProductAPINode extends ProductAPI implements AssistiveProduct {

    private transient final DirectedAcyclicGraph<AssistiveProduct, DefaultEdge> graph;

    private Long catalogueUniqueNumber;

    public ProductAPINode(DirectedAcyclicGraph<AssistiveProduct, DefaultEdge> graph, long id) {
        setId(id);
        this.graph = graph;
        setReplacedByProducts(new LinkedList<>());
        graph.addVertex(this);
    }

    @Override
    public DirectedAcyclicGraph<AssistiveProduct, DefaultEdge> graph() {
        return graph;
    }

    @Override
    public String getNumber() {
        return getProductNumber();
    }

    @Override
    public Long getCatalogueUniqueNumber() {
        return catalogueUniqueNumber;
    }

    @Override
    public void setCatalogueUniqueNumber(Long catalogueUniqueNumber) {
        this.catalogueUniqueNumber = catalogueUniqueNumber;
    }

    @Override
    public void addFit(AssistiveProduct to) throws UnsupportedFitException {
        Objects.requireNonNull(to, "to must be non-null");

        throw new UnsupportedFitException("Product " + getCatalogueUniqueNumber() + " cannot fit " + to.getCatalogueUniqueNumber());
    }

    public void addReplacedByProductsItem(ProductAPI replacedByProductsItem) {
        addDependency(replacedByProductsItem);
        getReplacedByProducts().add(replacedByProductsItem);
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ArticleAPINode articleAPINode = (ArticleAPINode) o;
        return Objects.equals(getId(), articleAPINode.getId());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<");
        sb.append("Product ");
        sb.append(getCatalogueUniqueNumber());
        sb.append("(");
        if (getCategory().getCode() != null) {
            sb.append(getCategory().getCode());
            sb.append(" ");
        }
        sb.append(getCategory().getName());
        sb.append(" ");
        sb.append("[");
        sb.append(getCategory().getArticleType()
                               .toString());
        sb.append("]");
        sb.append(")");
        sb.append(">");

        return sb.toString();
    }

}

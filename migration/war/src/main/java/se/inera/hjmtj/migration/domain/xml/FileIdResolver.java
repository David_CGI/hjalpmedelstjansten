/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.xml;

import com.sun.xml.bind.IDResolver;
import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

/**
 * Resolves a {@link javax.xml.bind.annotation.XmlIDREF} to instance of target type was annotated with {@link javax.xml.bind.annotation.XmlID}.
 */
@Slf4j
public class FileIdResolver extends IDResolver {

    public static final String DISABLE_VALIDATION = "se.inera.hjmtj.migration.domain.xml.FileIdResolver.validation";

    private final Map<String, Object> idMap = new HashMap<>();

    private final Map<String, Object> usedIds = new HashMap<>();

    private final Set<String> failedIds = new HashSet<>();

    private final Boolean disableValidation = Boolean.getBoolean(DISABLE_VALIDATION);

    public void bind(String id, Object obj) {
        idMap.put(id, obj);
    }

    public Callable resolve(final String id, Class targetType) {
        return () -> resolve(id);
    }

    public Object resolve(String id) {
        Object instance = idMap.get(id);

        if (instance == null) {
            failedIds.add(id);
        }

        usedIds.put(id, instance);

        return instance;
    }

    public void clear() {
        if (!disableValidation && failedIds.size() > 0) {
            String failedIds = String.join(", ", this.failedIds);
            throw new IllegalStateException("The following XML IDREFs could not be resolved: " + failedIds + " - check that the associated JAXB bindings are correct.");
        } else if (!disableValidation && idMap.size() != usedIds.size()) {
            logWarningForUnusedIds();
        }

        idMap.clear();
        usedIds.clear();
        failedIds.clear();
    }

    private void logWarningForUnusedIds() {
        Map<String, Object> unused = new HashMap<>(idMap);
        usedIds.forEach(unused::remove);
        Map<Class<?>, List<Map.Entry<String, Object>>> classMap = unused.entrySet()
                                                                                 .stream()
                                                                                 .collect(Collectors.groupingBy(e -> e.getValue()
                                                                                                                      .getClass()));
        for (Class<?> clazz : classMap.keySet()) {
            String unusedIds = classMap.getOrDefault(clazz, Collections.emptyList())
                                      .stream()
                                      .map(Map.Entry::getKey)
                                      .collect(Collectors.joining(", "));
            log.warn("Found unused XML IDs type {}. {}", clazz.getSimpleName(), unusedIds);
        }
    }
}

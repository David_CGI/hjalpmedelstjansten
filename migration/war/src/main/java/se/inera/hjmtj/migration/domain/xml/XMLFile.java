/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.xml;

import lombok.Value;
import se.inera.hjmtj.migration.infra.io.FtpConnection;

import java.io.InputStream;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toList;
import static se.inera.hjmtj.migration.application.MigrationApplication.FTP_URL_HOST;
import static se.inera.hjmtj.migration.application.MigrationApplication.FTP_URL_PASSWORD;
import static se.inera.hjmtj.migration.application.MigrationApplication.FTP_URL_USERNAME;

@Value
public class XMLFile {

    public static final String DATA_XML = "data.xml";

    private final XMLFileDeserialization.Builder xmlFileDeserializationBuilder = new XMLFileDeserialization.Builder();

    private final ElementContextComparator contextComparator;

    public List<Object> loadFromFTP(String fileUrl) {
        FtpConnection.Parameters ftpParameters = new FtpConnection.Parameters(FTP_URL_HOST, fileUrl, DATA_XML, FTP_URL_USERNAME, FTP_URL_PASSWORD);
        InputStream ftpStream = FtpConnection.of(ftpParameters);

        return load(ftpStream);
    }

    public List<Object> load(InputStream is) {
        for (Class<?> clazz : contextComparator.classes()) {
            xmlFileDeserializationBuilder.register(clazz);
        }

        XMLFileDeserialization xmlFileDeserialization = xmlFileDeserializationBuilder.build();

        try {
            return XMLResource.of(xmlFileDeserialization, is)
                              .stream()
                              .sorted(contextComparator)
                              .map(XMLFileDeserialization.ElementContext::get)
                              .collect(toList());
        } finally {
            // Free up memory
            xmlFileDeserialization.clear();
        }
    }

    public XMLFile register(Class<?> resultClass, Set<String> tags) {
        xmlFileDeserializationBuilder.register(resultClass, tags);
        return this;
    }

}

package se.inera.hjmtj.migration.domain.model.actors;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import se.inera.hjmtj.migration.domain.xml.TrimStringAdapter;

import javax.persistence.Embeddable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@Embeddable
@Getter
@Setter
public class ActorContactInformation {

    @XmlJavaTypeAdapter(TrimStringAdapter.class)
    @XmlElement(name = "ContactInformation_Cellphone")
    private String cellphone;

    @XmlJavaTypeAdapter(TrimStringAdapter.class)
    @XmlElement(name = "ContactInformation_Email")
    private String email;

    @XmlJavaTypeAdapter(TrimStringAdapter.class)
    @XmlElement(name = "ContactInformation_Workphone")
    private String workPhone;

    /**
     * Copy constructor.
     */
    public ActorContactInformation(ActorContactInformation contactInformation) {
        this();
        this.cellphone = contactInformation.getCellphone();
        this.email = contactInformation.getEmail();
        this.workPhone = contactInformation.getWorkPhone();
    }

    public ActorContactInformation() {
    }
}

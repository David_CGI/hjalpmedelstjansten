/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import se.inera.hjmtj.migration.domain.xml.TrimStringAdapter;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Inheritance;
import javax.persistence.MappedSuperclass;
import javax.persistence.PersistenceUnit;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Objects;

import static javax.persistence.InheritanceType.TABLE_PER_CLASS;

@PersistenceUnit(unitName = "local")
@Inheritance(strategy = TABLE_PER_CLASS)
@MappedSuperclass
@Table(indexes = {
        @Index(columnList = "identifier"),
        @Index(columnList = "name"),
})
@XmlTransient
@RequiredArgsConstructor
@Getter
@Setter
public abstract class OrganizationBase {

    @Id
    private Long id;

    @Column(nullable = false, unique = true)
    @XmlElement(name = "Identifier")
    private String identifier;

    @XmlJavaTypeAdapter(TrimStringAdapter.class)
    @XmlElement(name = "Name")
    @Column(nullable = false, unique = true)
    private String name;

    @XmlAttribute(name = "id")
    @XmlID
    public String getIdAsString() {
        return String.valueOf(id);
    }

    public void setIdAsString(String xmlId) {
        id = Long.parseLong(xmlId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrganizationBase that = (OrganizationBase) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

package se.inera.hjmtj.migration.application.bootstrap;

import lombok.extern.slf4j.Slf4j;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjmtj.migration.domain.HeapUsageInterceptor;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.domain.UserCopyInfo;
import se.inera.hjmtj.migration.domain.model.actors.Actor;
import se.inera.hjmtj.migration.domain.model.actors.ActorContactInformation;
import se.inera.hjmtj.migration.domain.model.actors.ActorOrganization;
import se.inera.hjmtj.migration.domain.model.actors.ActorRole;
import se.inera.hjmtj.migration.domain.model.actors.Role;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MessageTemplate;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.domain.xml.ElementContextComparator;
import se.inera.hjmtj.migration.domain.xml.XMLFile;
import se.inera.hjmtj.migration.infra.actors.ActorsRepository;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;
import se.inera.hjmtj.migration.infra.io.BatchPersistence;
import se.inera.hjmtj.migration.infra.organizations.CustomerRepository;
import se.inera.hjmtj.migration.infra.organizations.SupplierRepository;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Loads user accounts into H2.
 */
@Slf4j
@Singleton
@TransactionTimeout(unit = TimeUnit.MINUTES, value = 30)
@Interceptors({PerformanceLogInterceptor.class, HeapUsageInterceptor.class})
public class LoadActors {

    public static final ElementContextComparator PERSIST_ORDER = new ElementContextComparator(
            ActorOrganization.class,
            Actor.class,
            ActorRole.class);

    private final String ACTORS_URL_FILE = System.getenv()
                                                 .getOrDefault("ACTORS_URL_FILE", "<file>");

    private final Map<String, String> changeEmails = new HashMap<>();

    private final Set<String> removeUsernames = new HashSet<>();

    private final Map<String, String> changeOrganizations = new HashMap<>();

    private final Map<String, UserCopyInfo> copyUsernames = new HashMap<>();

    @Inject
    private ActorsRepository actorsRepository;

    @Inject
    private CustomerRepository customerRepository;

    @Inject
    private SupplierRepository supplierRepository;

    @PersistenceContext(unitName = "local")
    private EntityManager em;

    @Inject
    private BatchPersistence batchPersistence;

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    public LoadActors() {
        hjal1531();
        hjal1404();

    }

    public void hjal1404() {
        // Users to remove
        removeUsernames.add("jim.nilsson@widex.se");
        removeUsernames.add("kpetkov@cochlear.com");
        removeUsernames.add("mbidersten@cochlear.com");
        removeUsernames.add("maria.liljhammar@sanicare.se");

        // Users to copy
        copyUsernames.put("ola64@mac.com", UserCopyInfo.builder()
                                                       .username("Hearu")
                                                       .email("info@hearu.se")
                                                       .organization("Hearu AB")
                                                       .build());

        copyUsernames.put("caroline.andersson@careofsweden.se", UserCopyInfo.builder()
                                                                            .username("mcaroline.andersson@gmail.com")
                                                                            .email("mcaroline.andersson@gmail.com")
                                                                            .organization("Parir AB")
                                                                            .build());


        copyUsernames.put("mari.hidgard@careofsweden.se", UserCopyInfo.builder()
                                                                      .username("Mari.hidgard@hotmail.com")
                                                                      .email("Mari.hidgard@hotmail.com ")
                                                                      .organization("Parir AB")
                                                                      .build());

        copyUsernames.put("sara.gadell@careofsweden.se", UserCopyInfo.builder()
                                                                     .username("Sara.gadell@hotmail.com")
                                                                     .email("Sara.gadell@hotmail.com")
                                                                     .organization("Parir AB")
                                                                     .build());

        copyUsernames.put("jleinonen@cochlear.com", UserCopyInfo.builder()
                                                                .username("jonnaleinonen@hotmail.com")
                                                                .email("jonnaleinonen@hotmail.com")
                                                                .organization("Cochlear Nordic AB")
                                                                .build());

        copyUsernames.put("roy.jacobsson@icap.nu", UserCopyInfo.builder()
                                                               .username("Roy")
                                                               .email("info@frolundadata.se")
                                                               .organization("Frölunda Data Försäljnings AB")
                                                               .build());

        copyUsernames.put("sara.nilsson@icap.nu", UserCopyInfo.builder()
                                                              .username("Sara")
                                                              .email("frolundadata@gmail.com")
                                                              .organization("Frölunda Data Försäljnings AB")
                                                              .build());

        copyUsernames.put("tomas.mardsjo@icap.nu", UserCopyInfo.builder()
                                                               .username("Tomas")
                                                               .email("tomas.mardsjo@frolundadata.se")
                                                               .organization("Frölunda Data Försäljnings AB")
                                                               .build());

        copyUsernames.put("jamie.johansson@kronoberg.se", UserCopyInfo.builder()
                                                                      .username("Jamie J")
                                                                      .email("inkop-hjalmedelscentralen@kronoberg.se")
                                                                      .organization("Växjö Kommun")
                                                                      .build());

        copyUsernames.put("joakim.t.karlsson@kronoberg.se", UserCopyInfo.builder()
                                                                        .username("Joakim K")
                                                                        .email("rhc-hjalmedelscentralen@kronoberg.se")
                                                                        .organization("Växjö Kommun")
                                                                        .build());

//        copyUsernames.put("janine.pinter@sanicare.se", UserCopyInfo.builder()
//                                                                   .username("yvonne.svensson@sanicare.se")
//                                                                   .email("yvonne.svensson@sanicare.se")
//                                                                   .organization("Sanicare AB")
//                                                                   .build());

        copyUsernames.put("helen.benamore@phonak.com", UserCopyInfo.builder()
                                                                   .username("helen.benamore@sonova.com")
                                                                   .email("helen.benamore@sonova.com")
                                                                   .organization("Sonova Nordic AB")
                                                                   .build());

        copyUsernames.put("anna@tiarrow.com", UserCopyInfo.builder()
                                                          .username("Anna")
                                                          .email("anna@eloflex.se")
                                                          .organization("Eloflex AB")
                                                          .build());

        // Users to change organization on
        changeOrganizations.put("erik.nerback@ltdalarna.se", "Landstinget Dalarna");

        changeOrganizations.put("anders@maxgrepp.com", "Maxgrepp Handrims AB");

        changeOrganizations.put("jonathan.svedelius@gotland.se", "Region Gotland");

        changeOrganizations.put("ann-kristine.holm@regiongavleborg.se", "Region Gävleborg");

        changeOrganizations.put("anna.h.andersson@regionhalland.se", "Region Halland");

        changeOrganizations.put("Daniel.Vikstrom@nll.se", "Region Norrbotten");

        changeOrganizations.put("Inger.Karkiainen@nll.se", "Region Norrbotten");

        changeOrganizations.put("kerstin.berg-nilsson@norrbotten.se", "Region Norrbotten");

        changeOrganizations.put("Lena.Nordgren-Hansson@nll.se", "Region Norrbotten");

        changeOrganizations.put("134763", "Region Skåne");

        changeOrganizations.put("157716", "Region Skåne");

        changeOrganizations.put("172740", "Region Skåne");

        changeOrganizations.put("189986", "Region Skåne");

        changeOrganizations.put("209590", "Region Skåne");

        changeOrganizations.put("hakan.hallstrom@rvn.se", "Region Västernorrland");

        changeOrganizations.put("thore.englund@rvn.se", "Region Västernorrland");

        changeOrganizations.put("lars.mosslind@regionvastmanland.se", "Region Västmanland");

        changeOrganizations.put("michael.leyser@regionvastmanland.se", "Region Västmanland");

        changeOrganizations.put("anna.brors.ulvemark@regionvastmanland.se", "Region Västmanland");

        changeOrganizations.put("ann-sofie.andertoft@regionvastmanland.se", "Region Västmanland");

        changeOrganizations.put("elin.humleback@regionvastmanland.se", "Region Västmanland");

        changeOrganizations.put("christina.hassel@regionvastmanland.se", "Region Västmanland");

        changeOrganizations.put("anita.arnqvist@regionjh.se", "Region Jämtland Härjedalen");
    }

    public void hjal1531() {
        // Users to change emails on
        changeEmails.put("barbara@bratanis-zurcher@skane.se", "barbara.bratanis-zurcher@skane.se");
        changeEmails.put("cd@alurehab.com; christina@my-netti.com", "cd@alurehab.com");
        changeEmails.put("Charlotte Sjödin", "charlotte.sjodin@entral.se");
        changeEmails.put("hans.grenfeldt@kalmar.", "hans.grenfeldt@kalmar.se");
        changeEmails.put("helene,hagg@rvn.se", "helene.hagg@rvn.se");

        // Users to remove
        removeUsernames.add("anders.carlson@vgregion.se");
        removeUsernames.add("ewa.olofsson@sll.se");
        removeUsernames.add("Lillvor Ovesson");
        removeUsernames.add("Elin Swenning");
        removeUsernames.add("info@protecta.nu");
        removeUsernames.add("Annelie Stigsson");
        removeUsernames.add("ingrid.mattsson@sll.se");
        removeUsernames.add("andre.nilsson@medelpunkten.se");
        removeUsernames.add("test1");
        removeUsernames.add("kristina.hansson@ge.com");
        removeUsernames.add("tekniker");
        removeUsernames.add("bot");
        removeUsernames.add("marie.nygren@sunrisemedical.se");
        removeUsernames.add("roy.jacobsson");
        removeUsernames.add("Sanni Jørgensen");
        removeUsernames.add("ulrik@bdibatterier.se");
        removeUsernames.add("unilsson");
        removeUsernames.add("Vita Mazure");
        removeUsernames.add("ingrid");

        // Users to change organization on
        changeOrganizations.put("B84N", "Stockholms läns landsting");
        changeOrganizations.put("3G6J", "Stockholms läns landsting");
        changeOrganizations.put("kriek6", "Västra Götalands läns landsting");
        changeOrganizations.put("jane.maindal@vendlet.dk", "Vendlet AB");
        changeOrganizations.put("madeleine.svadling@mediq.com", "Mediq Sverige AB");
        changeOrganizations.put("marje32", "maria.jenseus@vgregion.se");
    }

    public void run() {
        Map<String, ActorOrganization> actorOrganizationMap = new HashMap<>();

        if (actorsRepository.isEmpty()) {
            List<Object> xmlObjects = new XMLFile(PERSIST_ORDER).loadFromFTP(ACTORS_URL_FILE);

            for (Object xmlObject : xmlObjects) {
                if (xmlObject instanceof Actor) {
                    Actor actor = (Actor) xmlObject;
                    ActorContactInformation contactInformation = actor.getContactInformation();
                    String oldEmail = contactInformation.getEmail();

                    if (changeEmails.containsKey(oldEmail)) {
                        String newEmail = changeEmails.getOrDefault(oldEmail, oldEmail);
                        contactInformation.setEmail(newEmail);

                        logEmailChange(actor, oldEmail, newEmail);
                    }
                }
                if (xmlObject instanceof ActorOrganization) {
                    ActorOrganization organization = (ActorOrganization) xmlObject;

                    organization.setCustomer(customerRepository.getCustomer(organization));
                    organization.setSupplier(supplierRepository.getSupplier(organization));
                    actorOrganizationMap.put(organization.actualName(), organization);
                }
            }

            batchPersistence.save(xmlObjects, em);

            for (Object o : xmlObjects) {
                if (o instanceof Actor) {
                    Actor actor = (Actor) o;

                    hjal1404_CopyUser(actor, actorOrganizationMap);
                    hjal1531(actor, actorOrganizationMap);
                    hjal1644(actor);
                }
            }
        }

        listDuplicateEmails();

        log.info("Done saving users!");
    }

    private void hjal1644(Actor actor) {
        String userName = actor.getUserName();
        String title = actor.getTitle() != null ? actor.getTitle()
                                                       .toLowerCase() : null;

        if ((title != null &&
                title.contains("funktionsinloggning")) ||
                userName.equalsIgnoreCase("funktion@vgrhjm.se")) {
            removeActor(actor);
        }
    }

    private void logEmailChange(Actor actor, String oldEmail, String newEmail) {
        log.info("User {} email changed from '{}' to '{}'", actor.getUserName(), oldEmail, newEmail);

        MigrationError migrationError = new MigrationError();
        migrationError.setSeverity(MigrationError.Severity.WARNING);
        migrationError.setOrganizationName(actor.getOrganization()
                                                .actualName());
        migrationError.setClassifier(Classifier.USER_ACCOUNT_EMAIL);
        migrationError.setUserName(actor.getUserName());
        migrationError.setMessage(MessageTemplate.USER_EMAIL_CHANGED, actor.getUserName(), oldEmail, newEmail);
        migrationErrorRepository.add(migrationError);
    }

    private void hjal1404_CopyUser(Actor actor, Map<String, ActorOrganization> actorOrganizationMap) {
        String userName = actor.getUserName();
        UserCopyInfo userCopyInfo = copyUsernames.get(userName);

        if (userCopyInfo != null) {
            String newUsername = userCopyInfo.getUsername()
                                             .trim();
            String email = userCopyInfo.getEmail()
                                       .trim();
            String organizationName = userCopyInfo.getOrganization()
                                                  .trim();
            ActorOrganization actorOrganization = actorOrganizationMap.get(organizationName);

            if (actorOrganization == null) {
                throw new IllegalArgumentException("No such ActorOrganization: " + organizationName);
            }

            Actor copiedActor = new Actor(actor);
            copiedActor.setId(maxActorId() + 1);
            copiedActor.setUserName(newUsername);
            copiedActor.setOrganization(actorOrganization);
            ActorContactInformation contactInformation = copiedActor.getContactInformation();
            contactInformation.setEmail(email);
            em.persist(copiedActor);

            // Copy roles on destination organization
            for (ActorRole actorRole : new HashSet<>(actor.getRoles())) {
                for (ActorOrganization roleOrganization : actorRole.getOrganizations()) {
                    if (roleOrganization.equals(copiedActor.getOrganization())) {
                        Set<Actor> actors = actorRole.getActors();
                        actors.add(copiedActor);

                        log.info("{} is {} for {}", toString(copiedActor), actorRole.getRoleType()
                                                                                    .toRoleDescriptionName()
                                                                                    .stream()
                                                                                    .map(Role::getName)
                                                                                    .collect(Collectors.joining(", ")), roleOrganization.actualName());

                        // The reverse must also be true i.e. the original actor should not have this role anymore
                        // since it is on a different organization
                        // this will also let us see the remainder in reports
                        actors.remove(actor);

                        em.merge(actorRole);
                    }
                }
            }

            logUserCopied(actor, copiedActor);
        }
    }

    private void logUserCopied(Actor actor, Actor copiedActor) {
        log.info("USER {} was copied to {}", toString(actor), toString(copiedActor));

        MigrationError migrationError = new MigrationError();
        migrationError.setSeverity(MigrationError.Severity.WARNING);
        migrationError.setOrganizationName(copiedActor.getOrganization()
                                                      .actualName());
        migrationError.setClassifier(Classifier.USER_ACCOUNT);
        migrationError.setUserName(copiedActor.getUserName());
        migrationError.setMessage(MessageTemplate.USER_COPIED, toString(actor));
        migrationErrorRepository.add(migrationError);
    }

    public String toString(Actor actor) {
        StringBuilder sb = new StringBuilder();

        sb.append("(");
        sb.append(actor.getUserName());

        sb.append(", ");
        sb.append(actor.getContactInformation()
                       .getEmail());

        sb.append(", ");
        sb.append(actor.getOrganization()
                       .actualName());
        sb.append(")");

        return sb.toString();
    }

    private void hjal1531(Actor actor, Map<String, ActorOrganization> actorOrganizationMap) {
        String userName = actor.getUserName();
        String oldOrganizationName = actor.getOrganization()
                                          .actualName();

        if (removeUsernames.contains(userName)) {
            removeActor(actor);
        } else if (changeOrganizations.containsKey(userName)) {
            String newOrganizationName = changeOrganizations.get(userName);
            ActorOrganization actorOrganization = actorOrganizationMap.get(newOrganizationName);

            if (actorOrganization == null) {
                log.info("Create missing ActorOrganization {}", newOrganizationName);

                actorOrganization = new ActorOrganization();
                actorOrganization.setId(maxOrganizationId() + 1);
                actorOrganization.setIdentifier(UUID.nameUUIDFromBytes(newOrganizationName.getBytes())
                                                    .toString());
                actorOrganization.setName(newOrganizationName);
                actorOrganization.setCustomer(customerRepository.getCustomer(actorOrganization));
                actorOrganization.setSupplier(supplierRepository.getSupplier(actorOrganization));
                actorOrganizationMap.put(newOrganizationName, actorOrganization);  // Transaction appears to be READ-COMMITTED
                em.persist(actorOrganization);
            }

            actor.setOrganization(actorOrganization);
            em.merge(actor);

            logOrganizationChanged(actor, oldOrganizationName, actorOrganization);
        }
    }

    private void removeActor(Actor actor) {
        if (em.createQuery("DELETE FROM Actor a " +
                "WHERE a.userName = :username")
              .setParameter("username", actor.getUserName())
              .executeUpdate() != 1) {
            throw new IllegalArgumentException("No row was deleted for username" + actor.getUserName());
        }

        logAccountRemoved(actor);
    }

    private void logAccountRemoved(Actor actor) {
        log.info("USER {} was removed", toString(actor));

        MigrationError migrationError = new MigrationError();
        migrationError.setSeverity(MigrationError.Severity.WARNING);
        migrationError.setOrganizationName(actor.getOrganization()
                                                .actualName());
        migrationError.setClassifier(Classifier.USER_ACCOUNT);
        migrationError.setUserName(actor.getUserName());
        migrationError.setMessage(MessageTemplate.USER_REMOVED, toString(actor));
        migrationErrorRepository.add(migrationError);
    }

    private void logOrganizationChanged(Actor actor, String oldOrganizationName, ActorOrganization
            actorOrganization) {
        log.info("USER {} organization was changed from {} to {}", toString(actor), oldOrganizationName, actorOrganization.actualName());

        MigrationError migrationError = new MigrationError();
        migrationError.setSeverity(MigrationError.Severity.WARNING);
        migrationError.setOrganizationName(actor.getOrganization()
                                                .actualName());
        migrationError.setClassifier(Classifier.USER_ACCOUNT);
        migrationError.setUserName(actor.getUserName());
        migrationError.setMessage(MessageTemplate.USER_ORGANIZATION_CHANGED, toString(actor), oldOrganizationName, actorOrganization.actualName());
        migrationErrorRepository.add(migrationError);
    }

    private void listDuplicateEmails() {
        List<Actor> resultList = em.createNativeQuery("SELECT a.*\n" +
                "FROM ACTOR a\n" +
                "            JOIN (SELECT email, COUNT(*)\n" +
                "                  FROM ACTOR\n" +
                "                  GROUP BY email\n" +
                "                  HAVING count(*) > 1 ) b\n" +
                "                        WHERE a.email = b.email\n" +
                "ORDER BY a.email", Actor.class)
                                   .getResultList();

        if (resultList.size() > 0) {
            log.error("Database contains {} users with non-unique email: {}", resultList.size(), resultList.stream()
                                                                                                           .map(a -> a.getUserName() + " [" + a.getContactInformation()
                                                                                                                                               .getEmail() + "]")
                                                                                                           .collect(Collectors.joining(", ")));
        }
    }

    public Long maxActorId() {
        List<Long> resultList = em.createQuery("SELECT max(o.id) FROM Actor o")
                                  .getResultList();
        return resultList.size() > 0 ? resultList.get(0) : null;
    }

    public Long maxOrganizationId() {
        List<Long> resultList = em.createQuery("SELECT max(o.id) FROM ActorOrganization o")
                                  .getResultList();
        return resultList.size() > 0 ? resultList.get(0) : null;
    }

}

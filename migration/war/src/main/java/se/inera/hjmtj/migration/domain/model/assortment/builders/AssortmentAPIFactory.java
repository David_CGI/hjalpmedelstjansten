/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.assortment.builders;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.business.DateUtils;
import se.inera.hjalpmedelstjansten.model.api.AssortmentAPI;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.api.RoleAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.UserEngagementAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCountyAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVMunicipalityAPI;
import se.inera.hjmtj.migration.domain.model.actors.ActorRole;
import se.inera.hjmtj.migration.domain.model.agreements.builders.Unique;
import se.inera.hjmtj.migration.domain.model.assortment.Assortment;
import se.inera.hjmtj.migration.domain.model.assortment.AssortmentAPIWithArticles;
import se.inera.hjmtj.migration.domain.model.assortment.AssortmentCountyMunicipality;
import se.inera.hjmtj.migration.domain.model.assortment.AssortmentOrganization;
import se.inera.hjmtj.migration.domain.model.assortment.AssortmentProduct;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MessageTemplate;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.domain.model.organization.entities.Customer;
import se.inera.hjmtj.migration.domain.model.product.ArticleAPINode;
import se.inera.hjmtj.migration.domain.model.product.entities.Product;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;
import se.inera.hjmtj.migration.infra.hjmtj.CVCountyAPIWithShowMunicipalities;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjCountyRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjCustomerRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjUserRepository;
import se.inera.hjmtj.migration.infra.products.ProductRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;
import static se.inera.hjalpmedelstjansten.model.entity.UserRole.RoleName;

@Slf4j
@ApplicationScoped
public class AssortmentAPIFactory {

    private final Map<String, CVCountyAPIWithShowMunicipalities> codeToCountyAPI = new HashMap<>();

    private final Map<String, CVMunicipalityAPI> codeToMunicipalityAPI = new HashMap<>();

    private final Map<String, String> transformCodeMap = new HashMap<>();

    private final Map<String, String> municipalityCodeToCountyCode = new HashMap<>();

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    @Inject
    private HjmtjUserRepository hjmtjUserRepository;

    @Inject
    private HjmtjCustomerRepository hjmtjCustomerRepository;

    @Inject
    private ProductRepository productRepository;

    public AssortmentAPIFactory() {
        // Stockholm city areas have been updated
        transformCodeMap.put("0180-07", "0180-09"); // 0180-07 Stockholm (Kista)			    ->	 0180-09 Stockholm (Rinkeby-Kista)
        transformCodeMap.put("0180-09", "0180-04"); // 0180-09 Stockholm (Liljeholmen)			->	 0180-04 Stockholm (Hägersten-Liljeholmen)
        transformCodeMap.put("0180-10", "0180-06"); // 0180-10 Stockholm (Maria-Gamla Stan)		->	 0180-06 Stockholm (Södermalm)
        transformCodeMap.put("0180-11", "0180-08"); // 0180-11 Stockholm (Norrmalm)			    ->	 0180-08 Stockholm (Norrmalm)
        transformCodeMap.put("0180-12", "0180-09"); // 0180-12 Stockholm (Rinkeby)			    ->	 0180-09 Stockholm (Rinkeby-Kista)
        transformCodeMap.put("0180-13", "0180-10"); // 0180-13 Stockholm (Skarpnäck)			->	 0180-10 Stockholm (Skarpnäck)
        transformCodeMap.put("0180-14", "0180-11"); // 0180-14 Stockholm (Skärholmen)			->	 0180-11 Stockholm (Skärholmen)
        transformCodeMap.put("0180-15", "0180-12"); // 0180-15 Stockholm (Spånga-Tensta)		->	 0180-12 Stockholm (Spånga-Tensta)
        transformCodeMap.put("0180-16", "0180-02"); // 0180-16 Stockholm (Vantör)			    ->	 0180-02 Stockholm (Enskede-Årsta-Vantör)
        transformCodeMap.put("0180-17", "0180-13"); // 0180-17 Stockholm (Älvsjö)		    	->	 0180-13 Stockholm (Älvsjö)
        transformCodeMap.put("0180-18", "0180-14"); // 0180-18 Stockholm (Östermalm)			->	 0180-14 Stockholm (Östermalm)
    }

    @Inject
    public void init(HjmtjCountyRepository hjmtjCountyRepository) {
        this.codeToCountyAPI.putAll(hjmtjCountyRepository.codeToCounty());
        this.codeToMunicipalityAPI.putAll(hjmtjCountyRepository.codeToMunicipality());

        for (CVCountyAPIWithShowMunicipalities cvCountyAPI : hjmtjCountyRepository.counties()) {
            for (CVMunicipalityAPI cvMunicipalityAPI : cvCountyAPI.getMunicipalities()) {
                municipalityCodeToCountyCode.put(cvMunicipalityAPI.getCode(), cvCountyAPI.getCode());
            }
        }
    }

    public AssortmentAPIWithArticles of(Assortment assortment) {
        AssortmentOrganization organization = assortment.getOrganization();
        Customer customer = organization.getCustomer();

        String assortmentName = assortment.getName();
        if (customer == null) {
            MigrationError migrationError = new MigrationError();
            migrationError.setOrganizationName(organization.getName());
            migrationError.setAssortmentName(assortmentName);
            migrationError.setSeverity(MigrationError.Severity.INFO);
            migrationError.setClassifier(Classifier.ASSORTMENT_CUSTOMER);
            migrationError.setMessage(MessageTemplate.ORGANIZATION_IS_NOT_A_SUBSCRIBER);
            migrationErrorRepository.add(migrationError);

            return null;
        } else if (assortmentName.toUpperCase()
                                 .startsWith("T1177")) {
            MigrationError migrationError = new MigrationError();
            migrationError.setOrganizationName(organization.getName());
            migrationError.setAssortmentName(assortmentName);
            migrationError.setSeverity(MigrationError.Severity.WARNING);
            migrationError.setClassifier(Classifier.ASSORTMENT);
            migrationError.setMessage(MessageTemplate.T1177);
            migrationErrorRepository.add(migrationError);

            return null;
        }

        OrganizationAPI organizationAPI = hjmtjCustomerRepository.findByGln(customer.getGln());
        AssortmentAPIWithArticles assortmentAPI = new AssortmentAPIWithArticles();
        assortmentAPI.setId(assortment.getId());
        assortmentAPI.setName(assortmentName);
        assortmentAPI.setCustomer(organizationAPI);
        assortmentAPI.setValidFrom(DateUtils.beginningOfDay(new Date())
                                            .getTime());

        setArticles(assortment, assortmentAPI);
        setCountyAndMunicipalities(assortment, assortmentAPI);
        setManagers(assortment, assortmentAPI);

        return assortmentAPI;
    }

    private void setManagers(Assortment assortment, AssortmentAPI assortmentAPI) {
        Long assortmentOrganizationId = assortmentAPI.getCustomer()
                                                     .getId();
        String organizationName = assortmentAPI.getCustomer()
                                               .getOrganizationName();

        // Select unique
        List<UserAPI> assignedAssortmentManagers = assortment.getAssortmentOwners()
                                                             .stream()
                                                             .map(ActorRole::getActors)
                                                             .flatMap(Collection::stream)
                                                             .map(hjmtjUserRepository::findBy)
                                                             .filter(Objects::nonNull)
                                                             .map(t -> new Unique<>(t, UserAPI::getId))
                                                             .distinct()
                                                             .map(Unique::data)
                                                             .filter(this::isCustomerAssignedAssortmentManager)
                                                             .collect(Collectors.toList());

        List<UserAPI> managers = new LinkedList<>();
        for (UserAPI assignedAssortmentManager : assignedAssortmentManagers) {
            Set<Long> userOrganizationIds = assignedAssortmentManager.getUserEngagements()
                                                                     .stream()
                                                                     .map(UserEngagementAPI::getOrganizationId)
                                                                     .filter(assortmentOrganizationId::equals)
                                                                     .collect(Collectors.toSet());

            // Ignore business level on assortments
            if (userOrganizationIds.contains(assortmentOrganizationId)) {
                managers.add(assignedAssortmentManager);
            }
        }

        if (managers.isEmpty()) {
            List<UserAPI> customerAssortmentManagers = hjmtjUserRepository.getUsersInRolesOnOrganization(assortmentOrganizationId, Collections.singletonList(RoleName.CustomerAssortmentManager));

            // Since assortments are only on top level, this means
            if (customerAssortmentManagers.isEmpty()) {
                MigrationError migrationError = new MigrationError();
                migrationError.setOrganizationName(organizationName);
                migrationError.setAssortmentName(assortmentAPI.getName());
                migrationError.setSeverity(MigrationError.Severity.INFO);
                migrationError.setClassifier(Classifier.ASSORTMENT_OWNER);
                migrationError.setMessage(MessageTemplate.ORGANIZATION_IS_NOT_A_SUBSCRIBER);
                migrationErrorRepository.add(migrationError);
            }
        }

        assortmentAPI.setManagers(managers);
    }

    private boolean isCustomerAssignedAssortmentManager(UserAPI userAPI) {
        Set<String> userRoles = userAPI.getUserEngagements()
                                       .stream()
                                       .map(UserEngagementAPI::getRoles)
                                       .flatMap(Collection::stream)
                                       .map(RoleAPI::getName)
                                       .collect(Collectors.toSet());

        return userRoles.contains(RoleName.CustomerAssignedAssortmentManager.toString());
    }

    private void setArticles(Assortment assortment, AssortmentAPIWithArticles assortmentAPI) {
        for (AssortmentProduct assortmentProduct : assortment.getProducts()) {
            Product article = assortmentProduct.getArticle();

            if (assortmentProduct.getXmlCategories()
                                 .isIso9999()) {
                if (article == null) {
                    Product product = productRepository.findByVgrProductNumber(assortmentProduct.getVgrProdNo());

                    if (product == null) {
                        MigrationError migrationError = new MigrationError();
                        migrationError.setSeverity(MigrationError.Severity.ERROR);
                        migrationError.setAssortmentName(assortmentAPI.getName());
                        migrationError.setOrganizationName(assortmentAPI.getCustomer()
                                                                        .getOrganizationName());
                        migrationError.setCatalogueUniqueNumber(Long.valueOf(assortmentProduct.getVgrProdNo()));
                        migrationError.setClassifier(Classifier.ASSORTMENT_ARTICLE);
                        migrationError.setMessage(MessageTemplate.ROW_WILL_NOT_BE_MIGRATED_BECAUSE_ARTICLE_WAS_NOT_RECEIVED);
                        migrationErrorRepository.add(migrationError);
                    } else {
                        MigrationError migrationError = new MigrationError();
                        migrationError.setSeverity(MigrationError.Severity.ERROR);
                        migrationError.setAssortmentName(assortmentAPI.getName());
                        migrationError.setOrganizationName(assortmentAPI.getCustomer()
                                                                        .getOrganizationName());
                        migrationError.setCatalogueUniqueNumber(Long.valueOf(assortmentProduct.getVgrProdNo()));
                        migrationError.setClassifier(Classifier.ASSORTMENT_ARTICLE);
                        migrationError.setMessage(MessageTemplate.ROW_WILL_NOT_BE_MIGRATED_BECAUSE_ARTICLE_HAS_NOT_BEEN_MIGRATED);
                        migrationErrorRepository.add(migrationError);
                    }
                } else if (!article.getXmlCategories()
                                   .isIso9999()) {
                    MigrationError migrationError = new MigrationError();
                    migrationError.setSeverity(MigrationError.Severity.INFO);
                    migrationError.setAssortmentName(assortmentAPI.getName());
                    migrationError.setOrganizationName(assortmentAPI.getCustomer()
                                                                    .getOrganizationName());
                    migrationError.setCatalogueUniqueNumber(Long.valueOf(assortmentProduct.getVgrProdNo()));
                    migrationError.setClassifier(Classifier.ASSORTMENT_ARTICLE);
                    migrationError.setMessage(MessageTemplate.NOT_ISO9999);
                    migrationErrorRepository.add(migrationError);
                } else if (article.isNoArticleType()) {
                    MigrationError migrationError = new MigrationError();
                    migrationError.setSeverity(MigrationError.Severity.WARNING);
                    migrationError.setAssortmentName(assortmentAPI.getName());
                    migrationError.setOrganizationName(assortmentAPI.getCustomer()
                                                                    .getOrganizationName());
                    migrationError.setCatalogueUniqueNumber(Long.valueOf(assortmentProduct.getVgrProdNo()));
                    migrationError.setClassifier(Classifier.ASSORTMENT_ARTICLE);
                    migrationError.setMessage(MessageTemplate.ARTICLE_WITHOUT_TYPE);
                    migrationErrorRepository.add(migrationError);
                } else {
                    ArticleAPINode articleAPI = new ArticleAPINode(null, article.getId());
                    articleAPI.setCatalogueUniqueNumber(Long.valueOf(article.getVgrProdNo()));

                    assortmentAPI.addArticle(articleAPI);
                }
            }
            // Ignore non ISO-9999
        }
    }

    private void setCountyAndMunicipalities(Assortment assortment, AssortmentAPIWithArticles assortmentAPI) {
        CVCountyAPIWithShowMunicipalities county = county(assortment);
        Map<CVCountyAPI, List<CVMunicipalityAPI>> countyToMunicipality = assortment.getCountyMunicipalities()
                                                                                   .stream()
                                                                                   .map(this::toCVMunicipalityAPI)
                                                                                   .filter(Objects::nonNull)
                                                                                   .collect(Collectors.groupingBy(this::getCounty));
        List<CVMunicipalityAPI> cvMunicipalityAPIList = county != null ? countyToMunicipality.getOrDefault(county, emptyList()) : emptyList();

        if (countyToMunicipality.size() > 1) {
            MigrationError migrationError = new MigrationError();
            migrationError.setClassifier(Classifier.ASSORTMENT_MUNICIPALITY);
            migrationError.setSeverity(MigrationError.Severity.ERROR);
            migrationError.setAssortmentName(assortmentAPI.getName());
            migrationError.setOrganizationName(assortmentAPI.getCustomer()
                                                            .getOrganizationName());
            migrationError.setMessage(MessageTemplate.COUNTY_TO_MUNICIPALITY_OVERFLOW);
            migrationErrorRepository.add(migrationError);
        } else if (county == null &&
                countyToMunicipality.size() == 1) {
            // HJAL-1328
            Iterator<List<CVMunicipalityAPI>> it = countyToMunicipality.values()
                                                                       .iterator();
            cvMunicipalityAPIList = it.hasNext() ? it.next() : null;
            CVMunicipalityAPI municipalityAPI = cvMunicipalityAPIList != null ? cvMunicipalityAPIList.get(0) : null;
            String countyCode = municipalityAPI != null ? municipalityCodeToCountyCode.get(municipalityAPI.getCode()) : null;
            CVCountyAPIWithShowMunicipalities countyAPI = countyCode != null ? codeToCountyAPI.get(countyCode) : null;

            if (countyAPI != null) {
                assortmentAPI.setCounty(countyAPI);
                assortmentAPI.setMunicipalities(countyAPI.isShowMunicipalities() ? cvMunicipalityAPIList : null);

                MigrationError migrationError = new MigrationError();
                migrationError.setClassifier(Classifier.ASSORTMENT_COUNTY);
                migrationError.setSeverity(MigrationError.Severity.WARNING);
                migrationError.setAssortmentName(assortmentAPI.getName());
                migrationError.setOrganizationName(assortmentAPI.getCustomer()
                                                                .getOrganizationName());
                migrationError.setMessage(MessageTemplate.EMPTY_COUNTY_CHANGED, countyAPI.getName());
                migrationErrorRepository.add(migrationError);
            }
        } else if (county != null &&
                county.isShowMunicipalities() &&
                cvMunicipalityAPIList.size() == 0) {
            // HJAL-1329
            String countyName = county.getName();

            if (countyName.equalsIgnoreCase("Kalmar kommun")) {
                // 0880 Kalmar
                CVMunicipalityAPI kalmar = codeToMunicipalityAPI.get("0880");
                assortmentAPI.setCounty(county);
                assortmentAPI.setMunicipalities(Collections.singletonList(kalmar));

                MigrationError migrationError = new MigrationError();
                migrationError.setClassifier(Classifier.ASSORTMENT_COUNTY);
                migrationError.setSeverity(MigrationError.Severity.WARNING);
                migrationError.setAssortmentName(assortmentAPI.getName());
                migrationError.setOrganizationName(assortmentAPI.getCustomer()
                                                                .getOrganizationName());
                migrationError.setMessage(MessageTemplate.EMPTY_MUNICIPALITY_CHANGED);
                migrationErrorRepository.add(migrationError);
            } else {
                MigrationError migrationError = new MigrationError();
                migrationError.setClassifier(Classifier.ASSORTMENT_MUNICIPALITY);
                migrationError.setSeverity(MigrationError.Severity.ERROR);
                migrationError.setAssortmentName(assortmentAPI.getName());
                migrationError.setOrganizationName(assortmentAPI.getCustomer()
                                                                .getOrganizationName());
                migrationError.setMessage(MessageTemplate.MUNICIPALITY_NOT_SET, countyName);
                migrationErrorRepository.add(migrationError);
            }
        } else if (county != null &&
                county.isShowMunicipalities() &&
                cvMunicipalityAPIList.size() > 0) {
            assortmentAPI.setCounty(county);
            assortmentAPI.setMunicipalities(cvMunicipalityAPIList);
        } else if (county != null &&
                !county.isShowMunicipalities()) {
            assortmentAPI.setCounty(county);
        }
    }

    private CVCountyAPIWithShowMunicipalities county(Assortment assortment) {
        List<CVCountyAPIWithShowMunicipalities> cvCountyAPIList = assortment.getCounties()
                                                                            .stream()
                                                                            .map(this::toCvCountyAPI)
                                                                            .filter(Objects::nonNull)
                                                                            .collect(Collectors.toList());
        return cvCountyAPIList.size() > 0 ? cvCountyAPIList.get(0) : null;
    }

    private CVMunicipalityAPI toCVMunicipalityAPI(AssortmentCountyMunicipality municipality) {
        // We must map from 1.0 to 2.0
        String code = transformCodeMap.getOrDefault(municipality.getCode(), municipality.getCode());
        CVMunicipalityAPI cvMunicipalityAPI = codeToMunicipalityAPI.get(code);

        if (cvMunicipalityAPI == null) {
            MigrationError migrationError = new MigrationError();
            migrationError.setSeverity(MigrationError.Severity.ERROR);
            migrationError.setClassifier(Classifier.CV_MUNICIPALITY);
            migrationError.setMessage(municipality.getCode() + " " + municipality.getName() + " hittas ej");
            migrationErrorRepository.add(migrationError);

            return null;
        }

        return cvMunicipalityAPI;
    }

    private CVCountyAPIWithShowMunicipalities getCounty(CVMunicipalityAPI cvMunicipalityAPI) {
        String code = municipalityCodeToCountyCode.get(cvMunicipalityAPI.getCode());
        Objects.requireNonNull("CVMunicipalityAPI " + cvMunicipalityAPI.getName());

        CVCountyAPIWithShowMunicipalities cvCountyAPI = codeToCountyAPI.get(code);
        Objects.requireNonNull("CVCountyAPI " + code);

        return cvCountyAPI;
    }

    private CVCountyAPIWithShowMunicipalities toCvCountyAPI(AssortmentCountyMunicipality county) {
        String code = transformCodeMap.getOrDefault(county.getCode(), county.getCode());
        CVCountyAPIWithShowMunicipalities cvCountyAPI = codeToCountyAPI.get(code);

        if (cvCountyAPI == null) {
            MigrationError migrationError = new MigrationError();
            migrationError.setSeverity(MigrationError.Severity.ERROR);
            migrationError.setClassifier(Classifier.CV_COUNTY);
            migrationError.setMessage(MessageTemplate.COUNTY_NOT_FOUND, code, county.getName());
            migrationErrorRepository.add(migrationError);

            return null;
        }

        return cvCountyAPI;
    }

}

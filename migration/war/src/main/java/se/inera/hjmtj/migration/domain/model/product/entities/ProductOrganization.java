/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.product.entities;

import lombok.Getter;
import lombok.Setter;
import se.inera.hjmtj.migration.domain.model.OrganizationBase;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.domain.xml.TrimStringAdapter;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PersistenceUnit;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Set;

@PersistenceUnit(unitName = "local")
@Entity
@NamedQueries({
        @NamedQuery(name = ProductOrganization.FIND_BY_ID, query = "SELECT o FROM ProductOrganization o WHERE o.id = :id"),
        @NamedQuery(name = ProductOrganization.FIND_ALL, query = "SELECT o FROM ProductOrganization o"),
        @NamedQuery(name = ProductOrganization.FIND_BY_IDENTIFIER, query = "SELECT po FROM ProductOrganization po " +
                "JOIN FETCH po.supplier " +
                "WHERE po.identifier = :identifier"),
})
@Table(indexes = {
        @Index(columnList = "identifier"),
        @Index(columnList = "gln"),
        @Index(columnList = "organizationNo"),
        @Index(columnList = "name"),
})
@XmlRootElement(name = "Organization")
@Getter
@Setter
public class ProductOrganization extends OrganizationBase {

    public static final String FIND_BY_ID = "ProductOrganization.FIND_BY_ID";

    public static final String FIND_ALL = "ProductOrganization.FIND_ALL";

    public static final String FIND_BY_IDENTIFIER = "ProductOrganization.FIND_BY_IDENTIFIER";

    @XmlJavaTypeAdapter(TrimStringAdapter.class)
    @XmlElement(name = "GLN")
    private String gln;

    @Embedded
    private ProductOrganizationEconomicInfo economicInformation;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "supplier")
    private Set<Product> products;

    @OneToOne(fetch = FetchType.LAZY, optional = true)
    private Supplier supplier;

}

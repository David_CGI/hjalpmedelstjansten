/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.errors;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
@Getter
@Setter
public class MigrationError implements Serializable, Comparable<MigrationError> {

    private transient static final AtomicLong nextId = new AtomicLong();

    private final long id = nextId.getAndIncrement();

    private Severity severity = Severity.ERROR;

    private Classifier classifier;

    private String organizationName;

    private Long catalogueUniqueNumber;

    private String assortmentName;

    private String agreementNumber;

    private String pricelistNumber;

    private String userName;

    private String message;

    private MigrationError cause;

    private MessageTemplate messageTemplate;

    public static List<MigrationError> of(Classifier classifier, String organizationName, Collection<? extends ErrorMessageAPI> errorMessageAPIs) {
        return errorMessageAPIs.stream()
                               .map(e -> MigrationError.of(classifier, organizationName, e))
                               .collect(Collectors.toList());
    }

    public static MigrationError of(Classifier classifier, String organizationName, ErrorMessageAPI errorMessageAPI) {
        MigrationError migrationError = new MigrationError();
        migrationError.setClassifier(classifier);
        migrationError.setOrganizationName(organizationName);

        String message = Stream.of(errorMessageAPI.getField(), errorMessageAPI.getMessage())
                               .filter(Objects::nonNull)
                               .collect(Collectors.joining(", "));
        migrationError.setMessage(message.substring(0, Math.min(255, message.length())));

        return migrationError;
    }

    public void setMessage(String errorMessage) {
        this.message = errorMessage;
    }

    public void setMessage(MessageTemplate messageTemplate) {
        this.messageTemplate=messageTemplate;
        this.message=messageTemplate.toString();
    }

    public void setMessage(MessageTemplate messageTemplate, Object... args) {
        try {
            this.message = String.format(messageTemplate.toString(), args);
            this.messageTemplate = messageTemplate;
        } catch (Exception e) {
            this.message = e.getMessage();
        }
    }

    @Override
    public int compareTo(MigrationError o) {
        return Long.compare(id, o.getId());
    }

    public enum Severity {
        INFO("Information"),
        WARNING("Ändring"),
        ERROR("Fel");

        private final String name;

        Severity(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }

}

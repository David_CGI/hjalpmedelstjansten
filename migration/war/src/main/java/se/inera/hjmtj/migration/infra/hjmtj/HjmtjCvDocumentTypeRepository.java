package se.inera.hjmtj.migration.infra.hjmtj;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVDocumentType;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.Optional;

@Slf4j
@Stateless
public class HjmtjCvDocumentTypeRepository {

    @PersistenceContext(unitName = "HjmtjPU")
    private EntityManager em;

    private final LoadingCache<String, Optional<CVDocumentType>> cacheLoader = Caffeine.newBuilder()
                                                                                       .build(this::findByValueInternal);

    public Optional<CVDocumentType> findByValue(String value) {
        return cacheLoader.get(value);
    }

    private Optional<CVDocumentType> findByValueInternal(String value) {
        try {
            CVDocumentType article = (CVDocumentType) em.createQuery("SELECT d FROM CVDocumentType d WHERE trim(lower(d.value)) = :value")
                                                        .setParameter("value", value.trim()
                                                                                    .toLowerCase())
                                                        .setHint("org.hibernate.readOnly", true)
                                                        .getSingleResult();
            return Optional.of(article);
        } catch (NoResultException ex) {
            return Optional.empty();
        }
    }
}

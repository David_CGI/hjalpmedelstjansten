/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.agreements.builders;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.api.BusinessLevelAPI;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.UserEngagementAPI;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjmtj.migration.domain.exceptions.MigrationException;
import se.inera.hjmtj.migration.domain.exceptions.save.BusinessLevelNotFound;
import se.inera.hjmtj.migration.domain.model.actorpricelistapprovers.ActorPricelistApproverActorRole;
import se.inera.hjmtj.migration.domain.model.actorpricelistapprovers.ActorPricelistApproverAgreement;
import se.inera.hjmtj.migration.domain.model.actors.Actor;
import se.inera.hjmtj.migration.domain.model.actors.ActorRole;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.Agreement;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.AgreementOrganization;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.AgreementParty;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MessageTemplate;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.domain.model.organization.entities.Customer;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjUserRepository;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@Slf4j
public class AgreementAPIBuilder extends AgreementAPIBuilderBase<AgreementAPIBuilderBase.AgreementAPICreated> {

    private static final List<String> IGNORED_CUSTOMERS = Arrays.asList("Kunder", "Region/Landsting");


    private static boolean isValidOrganization(String name) {
        return !IGNORED_CUSTOMERS.contains(name);
    }

    public static Customer customer(Agreement agreement) {
        return Optional.ofNullable(agreement.getBuyer())
                       .map(AgreementParty::getAgreementOrganization)
                       .map(AgreementOrganization::getCustomer)
                       .orElse(null);
    }

    @Override
    public AgreementAPICreated build() throws MigrationException {
        Agreement agreement = (Agreement) agreement();
        AgreementAPICreated agreementApi = super.doBuild();

        if (agreementApi == null) {
            return null;
        }

        boolean hasRows = priceLists().stream()
                                      .map(AgreementPricelistAPI::getRows)
                                      .flatMap(Collection::stream)
                                      .map(AgreementPricelistRowAPI::getArticle)
                                      .anyMatch(Objects::nonNull);

        if (!hasRows) {
            // "För avtal som saknar rader skall de inte migreras"

            if (agreementApi.getCustomerOrganization() != null) {
                MigrationError migrationError = new MigrationError();
                migrationError.setSeverity(MigrationError.Severity.WARNING);
                migrationError.setClassifier(getAgreementClassifier());
                migrationError.setMessage(MessageTemplate.AGREEMENT_HAS_NO_ROWS);
                migrationError.setOrganizationName(agreementApi.getCustomerOrganization()
                                                               .getOrganizationName());
                migrationError.setAgreementNumber(agreementApi.getAgreementNumber());
                migrationErrorRepository().add(migrationError);
            }

            if (agreementApi.getSupplierOrganization() != null) {
                MigrationError migrationError2 = new MigrationError();
                migrationError2.setSeverity(MigrationError.Severity.WARNING);
                migrationError2.setClassifier(getAgreementClassifier());
                migrationError2.setMessage(MessageTemplate.AGREEMENT_HAS_NO_ROWS);
                migrationError2.setOrganizationName(agreementApi.getSupplierOrganization()
                                                                .getOrganizationName());
                migrationError2.setAgreementNumber(agreementApi.getAgreementNumber());
                migrationErrorRepository().add(migrationError2);
            }

            return null;
        }

        AgreementOrganization agreementBuyer = agreement.getBuyer()
                                                        .getAgreementOrganization();

        Customer customer = agreementBuyer.getCustomer();
        Objects.requireNonNull(customer);
        Objects.requireNonNull(customer.getGln(), "Customer " + customer.getOrganizationName() + " must have non-null GLN");

        OrganizationAPI buyer = hjmtjCustomerRepository().findByGln(customer.getGln());
        BusinessLevelAPI businessLevel = hjmtjCustomerRepository().findBusinessLevel(customer);

        if (customer.isBusinessLevel() && businessLevel == null) {
            throw new BusinessLevelNotFound(customer.getOrganizationName());
        }

        agreementApi.setCustomerOrganization(buyer);
        agreementApi.setCustomerBusinessLevel(businessLevel);

        List<UserAPI> approvers = getPricelistApprovers(agreement);

        if (approvers.isEmpty()) {
            approvers = hjmtjUserRepository().findByOrganization(buyer, HjmtjUserRepository.AGREEMENT_APPROVERS)
                                             .stream()
                                             .filter(userAPI -> isValidPricelistApprover(userAPI, agreement))
                                             .collect(toList());

            if (approvers.isEmpty()) {
                MigrationError migrationError = new MigrationError();
                migrationError.setSeverity(MigrationError.Severity.ERROR);
                migrationError.setClassifier(getAgreementClassifier());
                migrationError.setMessage(MessageTemplate.AGREEMENT_HAS_NO_USERS_TO_APPROVE_PRICELISTS);
                migrationError.setOrganizationName(agreementApi.getCustomerOrganization()
                                                               .getOrganizationName());
                migrationError.setAgreementNumber(agreementApi.getAgreementNumber());
                migrationErrorRepository().add(migrationError);
            } else {
                MigrationError migrationError = new MigrationError();
                migrationError.setSeverity(MigrationError.Severity.WARNING);
                migrationError.setClassifier(getAgreementClassifier());
                migrationError.setMessage(MessageTemplate.AGREEMENT_HAS_NO_PRICELIST_APPROVERS_FALLBACK);
                migrationError.setOrganizationName(agreementApi.getCustomerOrganization()
                                                               .getOrganizationName());
                migrationError.setAgreementNumber(agreementApi.getAgreementNumber());
                migrationErrorRepository().add(migrationError);
            }
        }

        agreementApi.setCustomerPricelistApprovers(approvers.stream()
                                                            .map(t -> new Unique<>(t, UserAPI::getUsername))
                                                            .distinct()
                                                            .map(Unique::data)
                                                            .collect(toList()));

        Set<Customer> sharedWithCustomers = agreement.getSharedWithOrganizations()
                                                     .stream()
                                                     .map(AgreementOrganization::getCustomer)
                                                     .filter(Objects::nonNull)
                                                     .collect(toSet());

        setSharedWith(agreementApi, sharedWithCustomers);

        return agreementApi;
    }

    @Override
    protected Classifier getAgreementDeliveryTimeClassifier() {
        return Classifier.AGREEMENT_DELIVERY_TIME;
    }

    @Override
    protected Classifier getAgreementWarrantyUnitClassifier() {
        return Classifier.AGREEMENT_WARRANTY_UNIT;
    }

    @Override
    protected Classifier getAgreementWarrantyQuantityClassifier() {
        return Classifier.AGREEMENT_WARRANTY_QUANTITY;
    }

    @Override
    protected Classifier getAgreementClassifier() {
        return Classifier.AGREEMENT;
    }

    protected void setSharedWith(AgreementAPICreated agreementApi, Set<Customer> sharedWithCustomers) {
        setSharedWithCustomers(agreementApi, sharedWithCustomers);
        setSharedWithBusinessLevels(agreementApi, sharedWithCustomers);
    }

    private void setSharedWithBusinessLevels(AgreementAPICreated agreementApi, Set<Customer> sharedWithCustomers) {
        List<String> sharedWithRootGlnList = agreementApi.getSharedWithCustomers()
                                                         .stream()
                                                         .map(OrganizationAPI::getGln)
                                                         .collect(toList());

        agreementApi.setSharedWithCustomersBusinessLevels(sharedWithCustomers.stream()
                                                                             .filter(Customer::isBusinessLevel)
                                                                             .filter(customer -> !sharedWithRootGlnList.contains(customer.getGln()))
                                                                             .map(hjmtjCustomerRepository()::findBusinessLevel)
                                                                             .filter(Objects::nonNull)
                                                                             .map(t -> new Unique<>(t, BusinessLevelAPI::getName))
                                                                             .distinct()
                                                                             .map(Unique::data)
                                                                             .collect(Collectors.toList()));
    }

    private void setSharedWithCustomers(AgreementAPICreated agreementApi, Set<Customer> sharedWithCustomers) {
        agreementApi.setSharedWithCustomers(sharedWithCustomers.stream()
                                                               .filter(customer -> !customer.isBusinessLevel())
                                                               .map(hjmtjCustomerRepository()::find)
                                                               .filter(Objects::nonNull)
                                                               .map(t -> new Unique<>(t, OrganizationAPI::getGln))
                                                               .distinct()
                                                               .map(Unique::data)
                                                               .collect(Collectors.toList()));
    }

    protected List<UserAPI> getPricelistApprovers(Agreement agreement) {
        ActorPricelistApproverAgreement actorPricelistApproverAgreement = agreement.getActorPricelistApproverAgreement();

        if (actorPricelistApproverAgreement == null) {
            log.warn("No pricelist approvers for agreement {}", agreement.getHead()
                                                                         .getSellerAgreementNo());
            return Collections.emptyList();
        }

        Set<ActorPricelistApproverActorRole> approvers = actorPricelistApproverAgreement.getApprovers();
        if (approvers == null) {
            log.warn("No pricelist approvers for agreement {}", agreement.getHead()
                                                                         .getSellerAgreementNo());
            return Collections.emptyList();
        }

        List<ActorRole> actorRoles = approvers.stream()
                                              .map(ActorPricelistApproverActorRole::getActorRole)
                                              .filter(Objects::nonNull)
                                              .collect(toList());

        if (actorRoles.isEmpty()) {
            log.warn("No actor roles for agreement {}", agreement.getHead()
                                                                 .getSellerAgreementNo());
            return Collections.emptyList();
        }

        List<Actor> actors = actorRoles.stream()
                                       .map(ActorRole::getActors)
                                       .filter(Objects::nonNull)
                                       .flatMap(Collection::stream)
                                       .collect(toList());


        if (actors.isEmpty()) {
            log.warn("No actors for agreement {}", agreement.getHead()
                                                            .getSellerAgreementNo());
            return Collections.emptyList();
        }

        List<UserAPI> users = actors.stream()
                                    .map(hjmtjUserRepository()::findBy)
                                    .filter(Objects::nonNull)
                                    .filter(userAPI -> isValidPricelistApprover(userAPI, agreement))
                                    .collect(toList());

        if (users.isEmpty()) {
            log.warn("No users that are pricelist approvers for agreement {}", agreement.getHead()
                                                                                        .getSellerAgreementNo());
            return Collections.emptyList();
        }

        return users;
    }

    private boolean isValidPricelistApprover(UserAPI userAPI, Agreement agreement) {
        Organization userOrganization = organizationController().getOrganizationFromLoggedInUser(userAPI);
        if (userOrganization == null) {
            // User should not be migrated
            return false;
        }
        String userGln = userOrganization.getGln();
        List<String> userBusinessLevelNames = userAPI.getUserEngagements() != null ? userAPI.getUserEngagements()
                                                                                            .stream()
                                                                                            .map(UserEngagementAPI::getBusinessLevels)
                                                                                            .filter(Objects::nonNull)
                                                                                            .flatMap(Collection::stream)
                                                                                            .filter(Objects::nonNull)
                                                                                            .map(BusinessLevelAPI::getName)
                                                                                            .collect(toList()) : Collections.emptyList();

        Customer agreementBuyer = customer(agreement);
        if (agreementBuyer == null) {
            // Agreement should not be migrated
            return false;
        }
        String agreementBuyerGln = agreementBuyer.getGln();
        String agreementBuyerOrganizationName = agreementBuyer.getOrganizationName();

        // If the user is restricted to a business level he or she can only approve agreements that are restricted to
        // their business level
        if (userBusinessLevelNames.size() > 0) {
            return agreementBuyerGln.equalsIgnoreCase(userGln) && userBusinessLevelNames.contains(agreementBuyerOrganizationName);
        }

        // Otherwise the user is part of the root organization and the only question is whether the agreement buyer is the same
        return agreementBuyerGln.equalsIgnoreCase(userGln);
    }

}

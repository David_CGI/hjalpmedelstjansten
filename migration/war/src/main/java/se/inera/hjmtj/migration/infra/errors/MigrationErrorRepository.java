/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.errors;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;

import javax.ejb.Singleton;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Slf4j
@Singleton
public class MigrationErrorRepository {

    private final Set<MigrationError> migrationErrors = ConcurrentHashMap.newKeySet();

    private final Map<Long, Set<MigrationError>> catalogueUniqueNumberToAssistiveProductMigrationErrors = new ConcurrentHashMap<>();

    public Set<MigrationError> values() {
        return migrationErrors;
    }

    public List<MigrationError> get(MigrationError.Severity severity, long catalogueUniqueNumber) {
        return catalogueUniqueNumberToAssistiveProductMigrationErrors.getOrDefault(catalogueUniqueNumber, Collections.emptySet())
                                                                     .stream()
                                                                     .filter(severityEquals(severity))
                                                                     .filter(this::isAssistiveProductClassifier)
                                                                     .collect(Collectors.toList());
    }

    private Predicate<MigrationError> severityEquals(MigrationError.Severity severity) {
        return t -> t.getSeverity() != null && t.getSeverity()
                                                .equals(severity);
    }

    private boolean isAssistiveProductClassifier(MigrationError migrationError) {
        Classifier classifier = migrationError.getClassifier();

        if (classifier == null) {
            return false;
        }

        return classifier.equals(Classifier.HJALPMEDEL) || (classifier.parent() != null && classifier.parent()
                                                                                                     .equals(Classifier.HJALPMEDEL));
    }

    public void add(MigrationError migrationError) {
        migrationErrors.add(migrationError);

        if (migrationError.getCatalogueUniqueNumber() != null) {
            MigrationError cause = findFirst(MigrationError.Severity.ERROR, migrationError.getCatalogueUniqueNumber());
            migrationError.setCause(cause);

            catalogueUniqueNumberToAssistiveProductMigrationErrors.computeIfAbsent(migrationError.getCatalogueUniqueNumber(), nil -> new LinkedHashSet<>())
                                                                  .add(migrationError);
        }
    }

    private MigrationError findFirst(MigrationError.Severity severity, Long catalogueUniqueNumber) {
        return catalogueUniqueNumber == null ? null : catalogueUniqueNumberToAssistiveProductMigrationErrors.getOrDefault(catalogueUniqueNumber, Collections.emptySet())
                                                                                                            .stream()
                                                                                                            .filter(severityEquals(severity))
                                                                                                            .findAny()
                                                                                                            .orElse(null);
    }

}

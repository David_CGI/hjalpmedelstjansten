/*
 *
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 *  Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.hjmtj;

import lombok.extern.slf4j.Slf4j;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.domain.model.image.entity.ImageMigration;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static java.util.concurrent.TimeUnit.HOURS;

@Slf4j
@Stateless
@TransactionTimeout(unit = HOURS, value = 5)
@Interceptors({PerformanceLogInterceptor.class})
public class HjmtjImageMigrationRepository {

    @PersistenceContext(unitName = "HjmtjPU")
    private EntityManager em;

    public void persist(Object entity) {
        try {
            em.persist(entity);
        } catch (Exception t) {
            log.info("exception e for persists of entity" + t.getMessage());
            Class<?> entityClass = entity.getClass();
            throw new IllegalStateException("Encountered exception while saving entity of type: " + entityClass.getSimpleName(), t);
        }
    }

}

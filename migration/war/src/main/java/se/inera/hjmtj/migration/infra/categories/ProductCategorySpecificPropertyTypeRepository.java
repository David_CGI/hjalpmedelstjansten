/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.categories;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.domain.model.Indexed;
import se.inera.hjmtj.migration.domain.model.categories.CategorySpecificProperty;
import se.inera.hjmtj.migration.domain.model.categories.ProductCategorySpecificPropertyType;
import se.inera.hjmtj.migration.domain.xml.DepthFirstVisitor;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.joining;
import static javax.ejb.TransactionAttributeType.REQUIRES_NEW;

/**
 * Repository that is initialized from data.xsd.
 * Contains metadata for category-specific properties found in the product XML.
 */
@Slf4j
@Stateless
public class ProductCategorySpecificPropertyTypeRepository {

    private static final String CATEGORYTYPE_SQL = "/META-INF/local/categorytype.sql";

    private static final boolean GENERATE_SOURCE = false;

    private final DepthFirstVisitor visitor = new DepthFirstVisitor("/data.xsd");

    @PersistenceContext(unitName = "local")
    private EntityManager em;

    @TransactionAttribute(REQUIRES_NEW)
    public void loadFromXSD() {
        Set<ProductCategorySpecificPropertyType> productCategorySpecificPropertyTypes = visitor.visit(ProductCategorySpecificPropertyType::map);
        log.info("Found {} ProductCategorySpecificPropertyType in XSD (with duplicates removed)", productCategorySpecificPropertyTypes.size());
        generateSourceCode(productCategorySpecificPropertyTypes);

        for (ProductCategorySpecificPropertyType productCategorySpecificPropertyType : productCategorySpecificPropertyTypes) {
            fixName(productCategorySpecificPropertyType);
            productCategorySpecificPropertyType.setMatchesCategorySpecificProperties(findMatchingCategorySpecificProperties(productCategorySpecificPropertyType));
            log(productCategorySpecificPropertyType, productCategorySpecificPropertyType.getMatchesCategorySpecificProperties());
            em.persist(productCategorySpecificPropertyType);
            em.flush();
        }
    }

    private void fixName(ProductCategorySpecificPropertyType productCategorySpecificPropertyType) {
        String name = productCategorySpecificPropertyType.getName();


    }

    public void generateSourceCode(Collection<ProductCategorySpecificPropertyType> categorySpecificProperties) {
        if (GENERATE_SOURCE) {
            log.debug(" source code:\n\n");
            String code = categorySpecificProperties.stream()
                                                    .map(this::sourceCode)
                                                    .collect(joining("\n\n"));
            log.info(code);
            log.info(String.format("\nprivate final Set<ProductCategorySpecificProperty> properties  = new HashSet<>();\n",
                    ""));
        }
    }

    public Set<CategorySpecificProperty> findMatchingCategorySpecificProperties(ProductCategorySpecificPropertyType productCategorySpecificPropertyType) {
        // We must take into account that ProductCategorySpecificProperty can be placed at a higher level e.g. category code 12
        // and that there are no ProductCategorySpecificProperty on the 4th level e.g. 120103-01
        String categoryCode = productCategorySpecificPropertyType.getCategoryCode() == null ? "" : productCategorySpecificPropertyType.getCategoryCode();
        List<CategorySpecificProperty> resultList = em.createQuery("SELECT csp FROM CategorySpecificProperty csp " +
                "WHERE lower(trim(csp.name)) = lower(:name) AND " +
                "      csp.category.code LIKE CONCAT(:code, '%') AND " +
                "      csp.type IN :valueTypes", CategorySpecificProperty.class)
                                                      .setParameter("name", productCategorySpecificPropertyType.getName()
                                                                                                               .trim()
                                                                                                               .toLowerCase())
                                                      .setParameter("code", categoryCode)
                                                      .setParameter("valueTypes", productCategorySpecificPropertyType.getValueType()
                                                                                                                     .getCategorySpecificPropertyTypes())
                                                      .getResultList();

        // We are attempting to map the old tree structure onto a flat structure.
        // Bizarre and paradoxical cases emerge e.g.
        // Case 1) We have to migrate true duplicates:
        //      _x0032_698504e72304a77b9c3c186ae115780   =   159093 Kalorier (kcal/ml) <ID=X>
        //      ac317bc16efd4b8785814a2406b57092         =   159093 Kalorier (kcal/ml) <ID=Y>
        //
        // Solution: Both can be migrated as long as each is assigned one unique CategorySpecificProperty.
        //
        // Case 2) We have to migrate duplicates caused by inheritance:
        //      isoattr_22002    =   <ALL> Total vikt (kg)  <ID=Z>
        //      isoattr_22012    =   <ALL> Total vikt (kg)  <ID=Z>
        //      isoattr_22602    =   220318 Total vikt (kg) <ID=Z>
        //
        // Solution: These cannot be migrated if they occur simultaneously on a single assistive product (!)
        //
        //

        return new HashSet<>(resultList);
    }

    public void log(ProductCategorySpecificPropertyType productCategorySpecificPropertyType, Collection<CategorySpecificProperty> categorySpecificProperties) {
        Indexed.Builder<String> builder = Indexed.builder();
        String matchesString = categorySpecificProperties.stream()
                                                         .map(this::stringValue)
                                                         .map(builder::build)
                                                         .map(Indexed::toString)
                                                         .collect(joining(", "));
        log.debug("{} matches {}", productCategorySpecificPropertyType.toString(), matchesString);
    }

    private String stringValue(CategorySpecificProperty categorySpecificProperty) {
        StringBuilder sb = new StringBuilder();
        sb.append(categorySpecificProperty.getUniqueId());
        sb.append(" ");

        if (categorySpecificProperty.getCategory() != null) {
            String code = categorySpecificProperty.getCategory()
                                                  .getCode();
            if (code != null) {
                sb.append(code);
                sb.append(" ");
            }
        }

        sb.append(categorySpecificProperty.getName());
        sb.append(" ");
        sb.append(categorySpecificProperty.getType()
                                          .toString());

        return sb.toString();
    }

    private String sourceCode(ProductCategorySpecificPropertyType propertyType) {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("@XmlElement(name = \"%s\")\n", propertyType.getXmlName()));

        ProductCategorySpecificPropertyType.ValueType dataType = propertyType.getValueType();

        if (dataType.equals(ProductCategorySpecificPropertyType.ValueType.REFERENCE)) {
            sb.append(String.format("public void set%s(BlindReference xmlReference) {\n", propertyType.getXmlName()));
            sb.append(String.format("    categorySpecificProperties.add(ProductCategorySpecificProperty.of(\"%s\", xmlReference));\n", propertyType.getXmlName()));
        } else if (dataType.equals(ProductCategorySpecificPropertyType.ValueType.DECIMAL)) {
            sb.append(String.format("public void set%s(BigDecimal value) {\n", propertyType.getXmlName()));
            sb.append(String.format("    categorySpecificProperties.add(ProductCategorySpecificProperty.of(\"%s\", value));\n", propertyType.getXmlName()));
        } else if (dataType.equals(ProductCategorySpecificPropertyType.ValueType.BOOLEAN)) {
            sb.append(String.format("public void set%s(Boolean value) {\n", propertyType.getXmlName()));
            sb.append(String.format("    categorySpecificProperties.add(ProductCategorySpecificProperty.of(\"%s\", value));\n", propertyType.getXmlName()));
        } else if (dataType.equals(ProductCategorySpecificPropertyType.ValueType.STRING)) {
            sb.append(String.format("public void set%s(String value) {\n", propertyType.getXmlName()));
            sb.append(String.format("    categorySpecificProperties.add(ProductCategorySpecificProperty.of(\"%s\", value));\n", propertyType.getXmlName()));
        } else if (dataType.equals(ProductCategorySpecificPropertyType.ValueType.INTERVAL_DECIMAL)) {
            sb.append(String.format("public void set%s(ProductIntervalDecimal value) {\n", propertyType.getXmlName()));
            sb.append(String.format("    categorySpecificProperties.add(ProductCategorySpecificProperty.of(\"%s\", value));\n", propertyType.getXmlName()));
        }

        sb.append("}");

        return sb.toString();
    }

    public List<ProductCategorySpecificPropertyType> values() {
        return em.createQuery("SELECT pcsp FROM ProductCategorySpecificPropertyType pcsp ", ProductCategorySpecificPropertyType.class)
                 .getResultList();
    }

}

/*
 *
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 *  Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.media;

import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.domain.model.product.entities.Product;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductDocument;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductFile;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductImage;

@Slf4j
@Value
public class SaveParameters {

    private final Supplier supplier;

    private final Product product;

    private final String url;

    private final String description;

    private final String altText;

    private final String _srcContentUrl;

    private final boolean isDoc;

    private final boolean isMainImage;

    private final String contentType;

    private final String docType;

    private final ProductFile mainImage;

    private final Product templateProduct;

    private final boolean inherited;

    public SaveParameters(Supplier supplier, Product product, String url, String description, String altText, String _srcContentUrl, boolean isDoc, boolean isMainImage, String contentType, String docType, ProductFile mainImage, Product templateProduct) {
        this.supplier = supplier;
        this.product = product;
        this.url = url;
        this.description = description;
        this.altText = altText;
        this._srcContentUrl = _srcContentUrl;
        this.isDoc = isDoc;
        this.isMainImage = isMainImage;
        this.contentType = contentType;
        this.docType = docType;
        this.mainImage = mainImage;
        this.templateProduct = templateProduct;
        this.inherited = computeInherited(templateProduct, url, isMainImage, isDoc);
    }

    protected static boolean computeInherited(Product templateProduct, String url, boolean mainImage, boolean doc) {
        if (templateProduct == null) {
            return false;
        }

        if (doc) {
            for (ProductDocument productDocument : templateProduct.getDocuments()) {
                ProductFile file = productDocument.getFile();

                boolean inherited = file != null &&
                        file.getOriginalContentUrl()
                            .equalsIgnoreCase(url);

                if (inherited) {
                    return inherited;
                }
            }
        } else if (mainImage) {
            ProductFile mainImageFile = templateProduct.getMainImage();

            return mainImageFile != null &&
                    mainImageFile.getOriginalContentUrl()
                                 .equalsIgnoreCase(url);
        } else {
            // additional images
            for (ProductImage productImage : templateProduct.getImages()) {
                ProductFile file = productImage.getFile();

                boolean inherited = file != null &&
                        file.getOriginalContentUrl()
                            .equalsIgnoreCase(url);

                if (inherited) {
                    return inherited;
                }
            }
        }

        return false;
    }

}

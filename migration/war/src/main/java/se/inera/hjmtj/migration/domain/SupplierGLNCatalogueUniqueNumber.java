/*
 *
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 *  Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain;

import lombok.Value;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelistRow;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelistRow;

@Value
public class SupplierGLNCatalogueUniqueNumber {

    private String gln;

    private Long catalogueUniqueNumber;

    public static SupplierGLNCatalogueUniqueNumber of(AgreementPricelistRow row) {
        String gln = row.getPricelist() != null &&
                row.getPricelist()
                   .getAgreement() != null &&
                row.getPricelist()
                   .getAgreement()
                   .getSupplier() != null ? row.getPricelist()
                                               .getAgreement()
                                               .getSupplier()
                                               .getGln() : null;
        Long catalogueUniqueNumber = row.getArticle() != null &&
                row.getArticle()
                   .getCatalogUniqueNumber() != null ? row.getArticle()
                                                          .getCatalogUniqueNumber()
                                                          .getUniqueId() : null;
        return new SupplierGLNCatalogueUniqueNumber(gln, catalogueUniqueNumber);
    }

    public static SupplierGLNCatalogueUniqueNumber of(GeneralPricelistPricelistRow row) {
        String gln = row.getPricelist() != null &&
                row.getPricelist()
                   .getGeneralPricelist() != null &&
                row.getPricelist()
                   .getGeneralPricelist()
                   .getOwnerOrganization() != null ? row.getPricelist()
                                                        .getGeneralPricelist()
                                                        .getOwnerOrganization()
                                                        .getGln() : null;
        Long catalogueUniqueNumber = row.getArticle() != null && row.getArticle()
                                                                    .getCatalogUniqueNumber() != null ? row.getArticle()
                                                                                                           .getCatalogUniqueNumber()
                                                                                                           .getUniqueId() : null;
        return new SupplierGLNCatalogueUniqueNumber(gln, catalogueUniqueNumber);
    }
}

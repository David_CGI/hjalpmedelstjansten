/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.categories;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.domain.model.categories.Category;
import se.inera.hjmtj.migration.infra.io.CsvPersistence;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.function.Function.*;
import static java.util.stream.Collectors.*;
import static javax.ejb.TransactionAttributeType.REQUIRES_NEW;

/**
 * Populates Category using csv.
 */
@Slf4j
@Stateless
public class CategoryRepository {

    private static final String COLUMN_NAMES = "CODE;NAME;DESCRIPTION;Artikeltyper i dagens lösning;Möjliga val i dagens lösning;ARTICLETYPE;Möjliga val i Hjälpmedelstjänsten 2.0;UNKNOWN";

    private static final String fileName = "/categories.csv";

    @PersistenceContext(unitName = "local")
    private EntityManager em;

    @TransactionAttribute(REQUIRES_NEW)
    public void loadFromCsv() {
        em.createNativeQuery("DELETE FROM PRODUCT_PRODUCTCATEGORYSPECIFICPROPERTY")
          .executeUpdate();
        em.createNativeQuery("DELETE FROM PRODUCTCATEGORYSPECIFICPROPERTYTYPE_MATCHES_CATEGORYSPECIFICPROPERTY")
          .executeUpdate();
        em.createNativeQuery("DELETE FROM PRODUCTCATEGORYSPECIFICPROPERTYLISTVALUETYPE")
          .executeUpdate();
        em.createNativeQuery("DELETE FROM ProductCategorySpecificPropertyType")
          .executeUpdate();
        em.createQuery("DELETE FROM CategorySpecificPropertyListValue ")
          .executeUpdate();
        em.createQuery("DELETE FROM CategorySpecificProperty ")
          .executeUpdate();
        em.createQuery("DELETE FROM Category")
          .executeUpdate();

        Set<Category> categories = CsvPersistence.csvRead(em, fileName, COLUMN_NAMES, true)
                                                 .stream()
                                                 .map(this::map)
                                                 .collect(toCollection(LinkedHashSet::new));
        Map<String, Category> codeToCategory = categories.stream()
                                                         .filter(t -> t.getCode() != null)
                                                         .collect(toMap(Category::getCode, identity()));
        log.info("Found {} Category in CSV", categories.size());
        for (Category category : categories) {
            Category parent = category.getParent();

            if (parent != null) {
                String parentCode = parent.getCode();
                parent = codeToCategory.get(parentCode);
                category.setParent(parent);
            }

            em.persist(category);
            log.debug("Saved '{} {} with parent {}'", category.getCode(), category.getName(), category.getParent() != null ? category.getParent().getCode() : null);
        }
    }

    public Category map(Object[] object) {
        Category category = category(object);
        fixName(category);
        fixArticleType(category);
        return category;
    }

    private Category category(Object[] object) {
        Category category = new Category();
        String code = (String) object[0];
        category.setCode(code == null ? null : code.trim());

        String name = (String) object[1];
        category.setName(name.trim());

        String description = (String) object[2];
        category.setDescription(description == null ? null : description.trim());

        String articleType = (String) object[3];
        category.setArticleType(articleType);

        if (code != null && code.length() != 2) {
            String withoutDash = code.replace("-", "");
            String parentCode = withoutDash.substring(0, withoutDash.length() - 2);

            Category parentCategory = new Category();
            parentCategory.setCode(parentCode);
            category.setParent(parentCategory);
        }

        return category;
    }

    private void fixName(Category category) {
        category.setName(category.getName()
                                 .trim());
    }

    private void fixArticleType(Category category) {
        String articleType = category.getArticleType();

        if (articleType != null) {
            String[] split = articleType.split(",");

            if (split.length > 1) {
                category.setArticleType(split[1].trim());
            }
        }
    }

    public Category findByCode(String code) {
        try {
            return em.createQuery("SELECT c FROM Category c WHERE c.code = :code", Category.class)
                     .setParameter("code", code)
                     .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<Category> values() {
        return em.createQuery("SELECT DISTINCT c FROM Category c " +
                "LEFT JOIN FETCH c.parent" +
                " ORDER BY c.uniqueId", Category.class)
                 .getResultList();
    }
}

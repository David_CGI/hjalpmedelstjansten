package se.inera.hjmtj.migration.httpclient;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Optional;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectionPoolTimeoutException;
import org.apache.http.impl.client.CloseableHttpClient;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;
import se.inera.hjmtj.migration.infra.images.ImageData;

@Slf4j
@Stateless
public class ImageHttpClient {
	private static final String USER_AGENT = "Mozilla/5.0";
	@Inject
	private HttpClientConfig config;
	@Inject
	private MigrationErrorRepository migrationErrorRepository;

	public ImageHttpClient() {
	}

	public ImageHttpClient(HttpClientConfig config) {
		this.config = config;
	}

	public Optional<ImageData> getImage(String url) {
		try (CloseableHttpClient httpClient = config.httpClient()) {
			HttpGet httpGet = new HttpGet(url);
			httpGet.addHeader("User-Agent", USER_AGENT);
			CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
			if (httpResponse.getStatusLine().getStatusCode() == 200) {
				String contentType = getContentTypeFromImage(httpResponse);
				HttpEntity entity = httpResponse.getEntity();
				InputStream content = entity.getContent();
				ByteArrayOutputStream baos = new ByteArrayOutputStream(1024 * 1024);
				IOUtils.copy(content, baos);
				ImageData data = new ImageData();
				data.setContentType(contentType);
				data.setData(baos.toByteArray());
				return Optional.of(data);
			}
			
		} 
		catch (ConnectionPoolTimeoutException e) {
			// close the connection manager to release all resources
			config.getConnectionMgr().close();
			// init connection manager with connection pool
			config.init();
		}
		catch (Exception e) {
			log.error(e.getMessage());
			ErrorMessageAPI errorMessageAPI = new ErrorMessageAPI();
			errorMessageAPI.setField("bildOriginalContentUrl");
			errorMessageAPI.setMessage("BildOriginalContentUrl '" + url + "' cannot be loaded");
			migrationErrorRepository.add(MigrationError.of(Classifier.HJALPMEDEL_IMAGE, url, errorMessageAPI));
		}
		return Optional.empty();
	}

	private String getContentTypeFromImage(CloseableHttpResponse httpResponse) {
		Header[] allHeaders = httpResponse.getAllHeaders();
		Optional<Header> header = Arrays.stream(allHeaders).filter(f -> f.getName().equals(HttpHeaders.CONTENT_TYPE))
				.findFirst();
		if (header.isPresent()) {
			return header.get().getValue();
		}
		return null;
	}
}

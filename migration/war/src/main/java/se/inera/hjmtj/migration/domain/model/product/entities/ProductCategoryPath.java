/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.product.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.PersistenceUnit;
import javax.xml.bind.annotation.XmlValue;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * May contain non-iso coded category
 */
@PersistenceUnit(unitName = "local")
@Embeddable
@Getter
@Setter
public class ProductCategoryPath {

    public static final String ASSISTIVE_PRODUCT = "ISO-klassificerade hjälpmedel (ISO 9999)";

    @Column(columnDefinition = "VARCHAR", length = 1024)
    private String categoryPath;

    private boolean iso9999;

    private String lastCategoryCode;

    private String lastCategoryName;

    private boolean fourthLevel;

    public List<ProductCategory> categories() {
        if (categoryPath == null) {
            return Collections.emptyList();
        } else if (!categoryPath.contains(ASSISTIVE_PRODUCT)) {
            return Collections.emptyList();
        }

        LinkedList<ProductCategory> categories = new LinkedList<>();
        String[] split = categoryPath.trim()
                                     .split("\\\\");

        ProductCategory prev = null;
        for (String text : split) {
            ProductCategory category = new ProductCategory(text, prev);
            categories.add(category);
            prev = category;
        }

        return categories;
    }

    @XmlValue
    public void setCategoryPath(String categoryPath) {
        this.categoryPath = categoryPath;
        this.iso9999 = !categories().isEmpty();
        ProductCategory leaf = leaf();
        this.lastCategoryCode = leaf != null ? leaf.getCode() : null;
        this.lastCategoryName = leaf != null ? leaf.getName() : null;
        this.fourthLevel = lastCategoryCode != null;
    }

    public ProductCategory leaf() {
        List<ProductCategory> categories = categories();
        return categories.size() > 0 ? categories.get(categories.size() - 1) : null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductCategoryPath that = (ProductCategoryPath) o;
        return Objects.equals(categoryPath, that.categoryPath);
    }

    @Override
    public int hashCode() {
        return Objects.hash(categoryPath);
    }
}

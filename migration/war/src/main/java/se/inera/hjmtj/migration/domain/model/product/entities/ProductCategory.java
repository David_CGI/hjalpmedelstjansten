/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.product.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.PersistenceUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@PersistenceUnit(unitName = "local")
@Getter
@Setter
@EqualsAndHashCode(of = {"code", "name"})
public class ProductCategory {

    private static final Pattern PATTERN = Pattern.compile("(\\d+)\\s(.+)");

    private String code;

    private String name;

    private ProductCategory parent;

    public ProductCategory() {
        this.code = null;
        this.name = null;
        this.parent = null;
    }

    public ProductCategory(String text, ProductCategory parent) {
        this.parent = parent;

        Matcher matcher = PATTERN.matcher(text);

        if (!matcher.matches()) {
            name = text;
            code = null;
        } else {
            code = matcher.group(1);
            name = matcher.group(2);
        }
    }

    public ProductCategory(String code, String name, ProductCategory parent) {
        this.code = code;
        this.name = name;
        this.parent = parent;
    }
}

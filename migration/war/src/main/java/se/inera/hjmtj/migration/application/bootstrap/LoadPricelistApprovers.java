/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.application.bootstrap;

import lombok.extern.slf4j.Slf4j;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjmtj.migration.domain.HeapUsageInterceptor;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.domain.model.actorpricelistapprovers.ActorPricelistApproverActorRole;
import se.inera.hjmtj.migration.domain.model.actorpricelistapprovers.ActorPricelistApproverAgreement;
import se.inera.hjmtj.migration.domain.model.actorpricelistapprovers.ActorPricelistApproverOrganization;
import se.inera.hjmtj.migration.domain.model.actors.ActorRole;
import se.inera.hjmtj.migration.domain.xml.ElementContextComparator;
import se.inera.hjmtj.migration.domain.xml.XMLFile;
import se.inera.hjmtj.migration.infra.actors.ActorPricelistApproverActorRoleRepository;
import se.inera.hjmtj.migration.infra.actors.ActorsRepository;
import se.inera.hjmtj.migration.infra.agreements.AgreementRepository;
import se.inera.hjmtj.migration.infra.io.BatchPersistence;
import se.inera.hjmtj.migration.infra.organizations.CustomerRepository;
import se.inera.hjmtj.migration.infra.organizations.SupplierRepository;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Loads user accounts into H2.
 */
@Slf4j
@Singleton
@TransactionTimeout(unit = TimeUnit.MINUTES, value = 30)
@Interceptors({PerformanceLogInterceptor.class, HeapUsageInterceptor.class})
public class LoadPricelistApprovers {

    public static final ElementContextComparator PERSIST_ORDER = new ElementContextComparator(
            ActorPricelistApproverOrganization.class,
            ActorPricelistApproverAgreement.class,
            ActorPricelistApproverActorRole.class);

    private final String AGREEMENT_APPROVERS_URL_FILE = System.getenv()
                                                              .getOrDefault("AGREEMENT_APPROVERS_URL_FILE", "<file>");

    @Inject
    private ActorPricelistApproverActorRoleRepository actorPricelistApproverActorRoleRepository;

    @Inject
    private CustomerRepository customerRepository;

    @Inject
    private SupplierRepository supplierRepository;

    @PersistenceContext(unitName = "local")
    private EntityManager em;

    @Inject
    private BatchPersistence batchPersistence;

    @Inject
    private ActorsRepository actorsRepository;

    @Inject
    private AgreementRepository agreementRepository;

    public void run() {
        if (actorPricelistApproverActorRoleRepository.isEmpty()) {
            loadAgreementApprovers();
        }
    }

    private void loadAgreementApprovers() {
        List<Object> xmlObjects = new XMLFile(PERSIST_ORDER).loadFromFTP(AGREEMENT_APPROVERS_URL_FILE);

        for (Object xmlObject : xmlObjects) {
            if (xmlObject instanceof ActorPricelistApproverOrganization) {
                ActorPricelistApproverOrganization organization = (ActorPricelistApproverOrganization) xmlObject;

                organization.setCustomer(customerRepository.getCustomer(organization));
                organization.setSupplier(supplierRepository.getSupplier(organization));
            }else if (xmlObject instanceof ActorPricelistApproverActorRole) {
                ActorPricelistApproverActorRole actorPricelistApproverActorRole = (ActorPricelistApproverActorRole) xmlObject;
                actorPricelistApproverActorRole.setActorRole(actorsRepository.findByIdentifier(actorPricelistApproverActorRole.getIdentifier()));
            } else if(xmlObject instanceof ActorPricelistApproverAgreement){
                ActorPricelistApproverAgreement agreement = (ActorPricelistApproverAgreement) xmlObject;
                agreement.setAgreement(agreementRepository.findByIdentifier(agreement.getIdentifier()));
            }
        }

        batchPersistence.save(xmlObjects, em);
        log.info("Done loading pricelist approvers!");
    }

}

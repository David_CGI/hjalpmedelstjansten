/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.application.services;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.product.controller.OrderUnitController;
import se.inera.hjalpmedelstjansten.business.product.controller.PackageUnitController;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVOrderUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPackageUnit;
import se.inera.hjmtj.migration.domain.HeapUsageInterceptor;
import se.inera.hjmtj.migration.domain.MigrationLock;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.domain.csv.Utf8Csv;
import se.inera.hjmtj.migration.domain.exceptions.save.FailedToFindUserWithRole;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.domain.model.product.AssistiveProduct;
import se.inera.hjmtj.migration.domain.model.result.AssistiveProductMigrationResult;
import se.inera.hjmtj.migration.domain.model.result.AssistiveProductMigrationResultRepository;
import se.inera.hjmtj.migration.domain.model.result.AssistiveProductReport;
import se.inera.hjmtj.migration.domain.model.result.AssistiveProductReportRepository;
import se.inera.hjmtj.migration.infra.BulkController;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjUserRepository;
import se.inera.hjmtj.migration.infra.products.ProductOrganizationRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@ApplicationScoped
@Path("/hjalpmedel")
@Interceptors({PerformanceLogInterceptor.class, HeapUsageInterceptor.class})
public class AssistiveProductsService implements ReportServiceBase {

    @Inject
    private MigrationLock migrationLock;

    @Inject
    private HjmtjUserRepository hjmtjUserRepository;

    @Inject
    private ProductOrganizationRepository productOrganizationRepository;

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    @Inject
    private BulkController bulkController;

    @Inject
    private OrganizationController organizationController;

    @Inject
    private UserController userController;

    @Inject
    private OrderUnitController orderUnitController;

    @Context
    private HttpServletRequest request;

    @Inject
    private AssistiveProductReportRepository assistiveProductReportRepository;

    @Inject
    private AssistiveProductMigrationResultRepository assistiveProductMigrationResultRepository;

    @Inject
    private S3MediaService mediaService;

    public void saveAssistiveProducts(List<SuppliersService.SupplierOrganizationAPI> organizations) {
        log.info("Started migrating assistive products...");

        for (SuppliersService.SupplierOrganizationAPI entry : organizations) {
            Supplier supplier = entry.getSupplier();
            Objects.requireNonNull(supplier, "supplier must be non-null");

            OrganizationAPI organizationAPI = entry.getOrganizationAPI();
            Objects.requireNonNull(organizationAPI, "organizationAPI must be non-null");

            try {
                migrateAssistiveProducts(supplier, organizationAPI);
            } catch (FailedToFindUserWithRole e) {
                MigrationError migrationError = new MigrationError();
                migrationError.setClassifier(Classifier.SUPPLIER);
                migrationError.setMessage(e.getMessage());
                migrationError.setOrganizationName(supplier.getName());
                migrationErrorRepository.add(migrationError);
            } catch (Exception e) {
                MigrationError migrationError = new MigrationError();
                migrationError.setSeverity(MigrationError.Severity.ERROR);
                migrationError.setClassifier(Classifier.HJALPMEDEL);
                migrationError.setOrganizationName(entry.getOrganizationAPI()
                                                        .getOrganizationName());
                migrationError.setMessage(e.getMessage());
                migrationErrorRepository.add(migrationError);
            }
        }

        log.info("Done migrating assistive products!");
    }

    @Inject
    PackageUnitController packageUnitController;

    private void migrateAssistiveProducts(Supplier supplier, OrganizationAPI organizationAPI) throws FailedToFindUserWithRole {
        List<AssistiveProduct> sortedAssistiveProducts = productOrganizationRepository.findAssistiveProducts(supplier, organizationAPI);

        Organization organization = organizationController.getOrganization(organizationAPI.getId());
        Map<Long, CVOrderUnit> allOrderUnits = orderUnitController.findAllAsIdMap();
        Map<Long, CVPackageUnit> allPackageUnits = packageUnitController.findAllAsIdMap();

        for (AssistiveProduct assistiveProduct : sortedAssistiveProducts) {
            try {
                if (assistiveProduct instanceof ProductAPI) {
                    ProductAPI productAPI = (ProductAPI) assistiveProduct;
                    bulkController.createProduct(assistiveProduct.getCatalogueUniqueNumber(), organization, productAPI, allOrderUnits, allPackageUnits);
                } else if (assistiveProduct instanceof ArticleAPI) {
                    ArticleAPI articleAPI = (ArticleAPI) assistiveProduct;
                    bulkController.createArticle(assistiveProduct.getCatalogueUniqueNumber(), organization, articleAPI, allOrderUnits);
                }
            } catch (HjalpmedelstjanstenValidationException e) {
                for (ErrorMessageAPI errorMessageAPI : e.getValidationMessages()) {
                    MigrationError migrationError = new MigrationError();
                    migrationError.setOrganizationName(organization.getOrganizationName());
                    migrationError.setCatalogueUniqueNumber(assistiveProduct.getCatalogueUniqueNumber());
                    migrationError.setClassifier(Classifier.field(Classifier.HJALPMEDEL, errorMessageAPI.getField()));
                    migrationError.setMessage(errorMessageAPI.getMessage());
                    migrationErrorRepository.add(migrationError);
                }
            }

        }

        log.info("Saved {} assistive products for {}", sortedAssistiveProducts.size(), organizationAPI.getOrganizationName());
    }

    @GET
    @Path("/hjalpmedel.csv")
    @Produces("text/csv;charset=utf-8")
    public byte[] resultCsv() {
        return generateReport("hjalpmedel.csv", this::buildAssistiveProductsReport);
    }

    private byte[] buildAssistiveProductsReport() {
        migrationLock.tryAcquire();

        try {
            log.info("Retrieving report from h2...");
            List<AssistiveProductReport> assistiveProductReports = assistiveProductReportRepository.all();

            log.info("Retrieving results from mysql...");
            List<AssistiveProductMigrationResult> results = assistiveProductMigrationResultRepository.get(assistiveProductReports);
            return toCsv(results);
        } finally {
            migrationLock.release();
        }
    }

    private byte[] toCsv(List<AssistiveProductMigrationResult> results) {
        StringBuilder sb = new StringBuilder();
        sb.append("Leverantör;Finns leverantören i Lime?;Databas-ID;Sökbart nummer;Typ;Är ISO9999?;Finns i avtal?;Finns i GP?;Finns i assortment?;Migrerades OK?;Följdfel?;Varningar;Feltyp;Felmeddelanden;Kräver för att migreras (Databas-ID:n)\n");

        for (AssistiveProductMigrationResult result : results) {
            AssistiveProductReport report = result.getReport();

            sb.append(report.getOrganizationName());
            sb.append(";");

            sb.append(booleanString(report.isExistsInLime()));
            sb.append(";");

            sb.append(report.getId());
            sb.append(";");

            sb.append(report.getSearchableNumber());
            sb.append(";");

            sb.append(report.getType() == AssistiveProductReport.Type.PRODUCT ? "Produkt" : "Artikel");
            sb.append(";");

            sb.append(booleanString(report.isISO9999()));
            sb.append(";");

            sb.append(booleanString(report.isExistsInAgreement()));
            sb.append(";");

            sb.append(booleanString(report.isExistsInGP()));
            sb.append(";");

            sb.append(booleanString(report.isExistsInAssortment()));
            sb.append(";");

            sb.append(booleanString(result.isSuccess()));
            sb.append(";");

            sb.append(booleanString(result.isCascadeFailure()));
            sb.append(";");

            sb.append(result.getRemarks());
            sb.append(";");

            sb.append(result.getErrorType());
            sb.append(";");

            sb.append(result.getErrors());
            sb.append(";");

            sb.append(report.getRequiresIds()
                            .stream()
                            .map(Object::toString)
                            .collect(Collectors.joining(", ")));

            sb.append("\n");
        }

        return new Utf8Csv(sb.toString()).toBytes();
    }

}

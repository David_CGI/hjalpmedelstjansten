/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package se.inera.hjmtj.migration.infra;

import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.media.controller.MediaUploadAmazonS3Controller;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;

import javax.ejb.ApplicationException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.HOURS;

@Stateless
@TransactionTimeout(unit = HOURS, value = 5)
public class BulkService {

    @Inject
    private MediaUploadAmazonS3Controller awsMigrationController;


    public boolean checkMediaRepositoryIfImageExist(String externalKey) throws ErrorMessageAPIException {
        try {
            return awsMigrationController.checkIfExist(externalKey);
        } catch (HjalpmedelstjanstenValidationException e) {
            throw new ErrorMessageAPIException(e.getValidationMessages());
        }
    }


    public String updateMediaRepository(String externalKey, InputStream ios) throws ErrorMessageAPIException {
        try {
            return awsMigrationController.uploadFile(externalKey, ios);
        } catch (HjalpmedelstjanstenValidationException e) {
            throw new ErrorMessageAPIException(e.getValidationMessages());
        }
    }
    
    public String updateMediaRepositoryWithoutCheckExist(String externalKey, String contentType, InputStream ios) throws ErrorMessageAPIException {
        try {
            return awsMigrationController.uploadFileNoCheckIfExist(externalKey, contentType, ios);
        } catch (HjalpmedelstjanstenValidationException e) {
            throw new ErrorMessageAPIException(e.getValidationMessages());
        }
    }


    @ApplicationException
    public class ErrorMessageAPIException extends Exception {

        private Set<ErrorMessageAPI> validationMessages = new HashSet<>();

        public ErrorMessageAPIException(Collection<ErrorMessageAPI> validationMessages) {
            super();
            this.validationMessages.addAll(validationMessages);
        }

        public Set<ErrorMessageAPI> getValidationMessages() {
            return validationMessages;
        }

    }
}

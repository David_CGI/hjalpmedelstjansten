/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.application.services;

import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjmtj.migration.domain.csv.Utf8Csv;
import se.inera.hjmtj.migration.domain.exceptions.save.SupplierSaveFailed;
import se.inera.hjmtj.migration.domain.exceptions.save.UserAccountSaveFailed;
import se.inera.hjmtj.migration.domain.model.Indexed;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.domain.model.organization.api.MigratedOrganizationStatistics;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.domain.model.product.api.Account;
import se.inera.hjmtj.migration.domain.model.result.SupplierReport;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjOrganizationRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjSupplierRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjUserRepository;
import se.inera.hjmtj.migration.infra.organizations.SupplierRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

@Slf4j
@ApplicationScoped
@Path("/suppliers")
public class SuppliersService implements ReportServiceBase {

    @Inject
    private SupplierRepository supplierRepository;

    @Inject
    private HjmtjOrganizationRepository hjmtjOrganizationRepository;

    @Context
    private HttpServletRequest request;

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    @Inject
    private HjmtjUserRepository hjmtjUserRepository;

    @Inject
    private HjmtjSupplierRepository hjmtjSuppliersRepository;

    @GET
    @Path("/")
    public List<SupplierOrganizationAPI> migrateSuppliers() {
        log.info("Started migrating suppliers...");

        List<SupplierOrganizationAPI> returnValue = new LinkedList<>();

        Indexed.Builder<Supplier> builder = Indexed.builder();
        List<Indexed<Supplier>> indexedSuppliers = supplierRepository.suppliers()
                                                                     .stream()
                                                                     .map(builder::build)
                                                                     .collect(Collectors.toList());
        for (Indexed<Supplier> supplier : indexedSuppliers) {
            try {
                returnValue.add(SupplierOrganizationAPI.of(supplier.id(), supplier.value(), saveSupplier(supplier)));
            } catch (SupplierSaveFailed e) {
                MigrationError migrationError = new MigrationError();
                migrationError.setClassifier(Classifier.SUPPLIER);
                migrationError.setMessage(e.getMessage());
                migrationError.setOrganizationName(supplier.value()
                                                           .getName());
                migrationErrorRepository.add(migrationError);

                // Already exists?
                OrganizationAPI organizationAPI = hjmtjSuppliersRepository.find(supplier.value());
                returnValue.add(new SupplierOrganizationAPI(supplier.id(), supplier.value(), organizationAPI));
            } catch (Exception e) {
                log.error("{}", e);
            }
        }

        log.info("Done migrating suppliers!");

        return returnValue;
    }

    private OrganizationAPI saveSupplier(Indexed<Supplier> indexedSupplier) throws SupplierSaveFailed {
        Supplier value = indexedSupplier.value();
        OrganizationAPI supplierApi = hjmtjSuppliersRepository.save(value);

        log.debug("Saved {}", supplierApi.getOrganizationName());
        return supplierApi;
    }

    public void migrateTestUsers(List<SupplierOrganizationAPI> organizations) {
        Indexed.Builder<SupplierOrganizationAPI> builder = Indexed.builder();
        List<Indexed<SupplierOrganizationAPI>> indexedOrganizations = organizations.stream()
                                                                                   .map(builder::build)
                                                                                   .collect(Collectors.toList());

        for (Indexed<SupplierOrganizationAPI> supplierOrganizationAPIIndexed : indexedOrganizations) {
            SupplierOrganizationAPI supplierOrganizationAPI = supplierOrganizationAPIIndexed.value();
            OrganizationAPI organizationAPI = supplierOrganizationAPI.getOrganizationAPI();

            Long id = supplierOrganizationAPIIndexed.id();
            Supplier supplier = supplierOrganizationAPI.getSupplier();
            Account account = Account.supplier(id, supplier.getSecret());

            try {
                hjmtjUserRepository.saveUser(account, organizationAPI, emptyList());
                hjmtjUserRepository.setPassword(account.name(), account.secret()
                                                                       .getSecretValue());
            } catch (UserAccountSaveFailed e) {
                MigrationError migrationError = new MigrationError();
                migrationError.setClassifier(Classifier.SUPPLIER);
                migrationError.setMessage(e.getMessage());
                migrationError.setOrganizationName(supplier.getName());
                migrationErrorRepository.add(migrationError);
            } catch (Exception e) {
                log.error("{}", e);
            }
        }
    }

    @GET
    @Path("/suppliers.csv")
    @Produces("text/csv;charset=utf-8")
    public byte[] suppliers() {
        return generateReport("suppliers", this::buildSuppliersReport);
    }

    private byte[] buildSuppliersReport() {
        Map<String, SupplierReport> h2Reports = supplierRepository.reports();
        Map<String, SupplierReport> mysqlReports = hjmtjSuppliersRepository.supplierReports();
        StringBuilder sb = new StringBuilder();
        sb.append("Leverantör;Ansluten till tjänsten?;Migrerade produkter;Migrerade artiklar;Andel (%)\n");

        for (SupplierReport supplierReport : h2Reports.values()) {
            SupplierReport mysqlReport = mysqlReports.getOrDefault(supplierReport.getOrganizationName(), new SupplierReport(null, false, 0, 0));

            sb.append(supplierReport.getOrganizationName());
            sb.append(";");

            sb.append(booleanString(supplierReport.isSubscribes()));
            sb.append(";");

            sb.append(String.format("%s av %s", mysqlReport.getProducts(), supplierReport.getProducts()));
            sb.append(";");

            sb.append(String.format("%s av %s", mysqlReport.getArticles(), supplierReport.getArticles()));
            sb.append(";");

            DecimalFormat df = new DecimalFormat("#.##");
            double result = 100 * (((double) mysqlReport.getProducts() + mysqlReport.getArticles()) / ((double) supplierReport.getProducts() + supplierReport.getArticles()));
            sb.append(df.format(result));
            sb.append("\n");
        }

        return new Utf8Csv(sb.toString()).toBytes();
    }

    @GET
    @Path("/progress.csv")
    @Produces("text/csv;charset=utf-8")
    public byte[] progressCsv() {
        return buildProgressReport();
    }

    private byte[] buildProgressReport() {
        StringBuilder sb = new StringBuilder();

        Map<String, Supplier> nameToOrg = supplierRepository.suppliers()
                                                            .stream()
                                                            .collect(toMap(Supplier::getName, identity()));

        sb.append("Id;Namn;Migrerad total;Total i XML;P;Pt;Ah;At;Ar;Atj;Ai;Inköpsavtal;Prislistor;Prislisterader\n");

        List<MigratedOrganizationStatistics> statistics = hjmtjOrganizationRepository.statistics();

        int ph = 0, pt = 0;
        int ah = 0, at = 0, ar = 0, atj = 0, ai = 0;
        long agreements = 0;
        long priceLists = 0;
        long priceListRows = 0;

        for (MigratedOrganizationStatistics organization : statistics) {
            ph += organization.productMap()
                              .get(Article.Type.H);
            pt += organization.productMap()
                              .get(Article.Type.T);

            ah += organization.articleMap()
                              .get(Article.Type.H);
            at += organization.articleMap()
                              .get(Article.Type.T);
            ar += organization.articleMap()
                              .get(Article.Type.R);
            atj += organization.articleMap()
                               .get(Article.Type.Tj);
            ai += organization.articleMap()
                              .get(Article.Type.I);

            agreements += organization.agreementRows();
            priceLists += organization.priceLists();
            priceListRows += organization.agreementRows();

        }
        int total = ph + pt + ah + at + ar + atj + ai;
        sb.append("0;");
        sb.append("Total");
        sb.append(";"); // user
        sb.append(total)
          .append(";"); //total
        sb.append(ph)
          .append(";");
        sb.append(pt)
          .append(";");
        sb.append(ah)
          .append(";");
        sb.append(at)
          .append(";");
        sb.append(ar)
          .append(";");
        sb.append(atj)
          .append(";");
        sb.append(ai)
          .append(";");
        sb.append(agreements)
          .append(";");
        sb.append(priceLists)
          .append(";");
        sb.append(priceListRows)
          .append(";");
        sb.append("\n");

        int index = 1;
        for (MigratedOrganizationStatistics organization : statistics) {
            Supplier supplier = nameToOrg.get(organization.organizationName());
            sb.append(index++);
            sb.append(";");

            sb.append(organization.organizationName()
                                  .replace(",", ""));
            sb.append(";");

            sb.append(organization.total());
            sb.append(";");

            // produkter
            sb.append(organization.productMap()
                                  .get(Article.Type.H));
            sb.append(";");

            sb.append(organization.productMap()
                                  .get(Article.Type.T));
            sb.append(";");

            // artiklar
            sb.append(organization.articleMap()
                                  .get(Article.Type.H));
            sb.append(";");

            sb.append(organization.articleMap()
                                  .get(Article.Type.T));
            sb.append(";");

            sb.append(organization.articleMap()
                                  .get(Article.Type.R));
            sb.append(";");

            sb.append(organization.articleMap()
                                  .get(Article.Type.Tj));
            sb.append(";");

            sb.append(organization.articleMap()
                                  .get(Article.Type.I));
            sb.append(";");

            sb.append(organization.agreements());
            sb.append(";");

            sb.append(organization.priceLists());
            sb.append(";");

            sb.append(organization.agreementRows());
            sb.append(";");

            sb.append("\n");
        }

        return new Utf8Csv(sb.toString()).toBytes();
    }


@Value(staticConstructor = "of")
public static class SupplierOrganizationAPI {

    private final Long id;

    private final Supplier supplier;

    private final OrganizationAPI organizationAPI;
}

}

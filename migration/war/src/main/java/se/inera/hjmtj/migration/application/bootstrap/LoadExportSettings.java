/*
 *
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 *  Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.application.bootstrap;

import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjmtj.migration.domain.HeapUsageInterceptor;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementHead;
import se.inera.hjmtj.migration.domain.model.agreements.gp.ExportSetting;
import se.inera.hjmtj.migration.domain.model.agreements.gp.GeneralPricelist;
import se.inera.hjmtj.migration.domain.model.agreements.gp.GeneralPricelistOrganization;
import se.inera.hjmtj.migration.domain.model.agreements.gp.GeneralPricelistParty;
import se.inera.hjmtj.migration.domain.model.organization.entities.Customer;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.infra.io.CsvPersistence;
import se.inera.hjmtj.migration.infra.organizations.CustomerRepository;
import se.inera.hjmtj.migration.infra.organizations.SupplierRepository;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static java.util.function.Function.identity;

@Slf4j
@Singleton
@TransactionTimeout(unit = TimeUnit.MINUTES, value = 30)
@Interceptors({PerformanceLogInterceptor.class, HeapUsageInterceptor.class})
public class LoadExportSettings {

    private final String EXPORT_SETTINGS_FILE = "/Speglat avtal_190107.csv";

    @Inject
    private SupplierRepository supplierRepository;

    @Inject
    private CustomerRepository customerRepository;

    @PersistenceContext(unitName = "local")
    private EntityManager em;

    public void run() {
        if (isEmpty()) {
            em.createNativeQuery("DELETE FROM ExportSetting ")
              .executeUpdate();

            CsvPersistence.csvRead(em, EXPORT_SETTINGS_FILE, "ExportSetting", "title;agreementNumber;versionlist;seller;buyer;validFrom;validEnds;createdTime;autoCreated");

            Map<String, Supplier> suppliers = supplierRepository.suppliers()
                                                                .stream()
                                                                .collect(Collectors.toMap(Supplier::getName, identity()));
            Map<String, Customer> customers = customerRepository.values()
                                                                .stream()
                                                                .collect(Collectors.toMap(Customer::getOrganizationName, identity()));

            Map<GeneralPricelistOwner, GeneralPricelist> generalPricelists = new HashMap<>();
            for (GeneralPricelist pricelist : generalPricelists()) {
                GeneralPricelistOwner generalPricelistOwner = GeneralPricelistOwner.of(pricelist);

                if (generalPricelistOwner != null) {
                    generalPricelists.put(generalPricelistOwner, pricelist);
                }
            }

            for (ExportSetting exportSetting : exportSettings()) {
                GeneralPricelist generalPricelist = generalPricelists.get(GeneralPricelistOwner.of(exportSetting));
                exportSetting.setGeneralPricelist(generalPricelist);
                exportSetting.setSupplier(suppliers.get(exportSetting.getSeller()));
                exportSetting.setCustomer(customers.get(exportSetting.getBuyer()));

                if (generalPricelist != null) {
                    log.info("Added export setting for GP {}", generalPricelist.getIdentifier());
                }
            }
        }
    }

    private boolean isEmpty() {
        return size() == 0;
    }

    private List<GeneralPricelist> generalPricelists() {
        return em.createQuery("SELECT gp from GeneralPricelist gp ", GeneralPricelist.class)
                 .setHint("org.hibernate.readOnly", true)
                 .getResultList();
    }

    private List<ExportSetting> exportSettings() {
        return em.createNamedQuery(ExportSetting.FIND_ALL, ExportSetting.class)
                 .getResultList();
    }

    private long size() {
        return em.createQuery("SELECT COUNT(a) FROM ExportSetting a", Long.class)
                 .setMaxResults(1)
                 .getSingleResult();
    }

    @Value
    private static class GeneralPricelistOwner {

        private final String seller;

        public static GeneralPricelistOwner of(GeneralPricelist generalPricelist) {
            AgreementHead head = generalPricelist.getHead();
            GeneralPricelistParty seller = head != null ? generalPricelist.getSeller() : null;
            GeneralPricelistOrganization agreementOrganization = seller != null ? seller.getAgreementOrganization() : null;
            Supplier supplier = agreementOrganization != null ? agreementOrganization.getSupplier() : null;
            String supplierName = supplier != null ? supplier.getName() : null;

            if(supplierName == null){
                return null;
            }

            return new GeneralPricelistOwner(supplierName);
        }

        public static GeneralPricelistOwner of(ExportSetting exportSetting) {
            String seller = exportSetting.getSeller();

            return new GeneralPricelistOwner(seller);
        }
    }

}

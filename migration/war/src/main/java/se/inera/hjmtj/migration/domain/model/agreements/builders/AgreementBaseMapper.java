/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.agreements.builders;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjmtj.migration.domain.exceptions.MigrationException;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementBase;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementRowBase;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementVersion;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjCustomerRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjCvGuaranteeUnitRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjOrganizationRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjProductRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjSupplierRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjUserRepository;
import se.inera.hjmtj.migration.infra.organizations.CustomerRepository;
import se.inera.hjmtj.migration.infra.products.ProductOrganizationRepository;
import se.inera.hjmtj.migration.infra.products.ProductRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Slf4j
@ApplicationScoped
public class AgreementBaseMapper {

    @Inject
    private HjmtjOrganizationRepository hjmtjOrganizationRepository;

    @Inject
    private HjmtjProductRepository hjmtjProductRepository;

    @Inject
    private ProductRepository productRepository;

    @Inject
    private HjmtjCvGuaranteeUnitRepository hjmtjGuaranteeUnitRepository;

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    @Inject
    private CustomerRepository customerRepository;

    @Inject
    private HjmtjUserRepository hjmtjUserRepository;

    @Inject
    private ProductOrganizationRepository productOrganizationRepository;

    @Inject
    private HjmtjCustomerRepository hjmtjCustomerRepository;

    @Inject
    private HjmtjSupplierRepository hjmtjSupplierRepository;

    @Inject
    private OrganizationController organizationController;

    public Optional<GeneralPricelistAPIWithMultiplePricelists> toGeneralPricelist(AgreementBase agreementBase, OrganizationAPI supplierAPI) {
        return map(agreementBase, supplierAPI, new GeneralPricelistAPIBuilder());
    }

    private <R> Optional<R> map(AgreementBase agreementBase, OrganizationAPI supplierAPI, AgreementAPIBuilderBase<R> builder) {
        try {
            Set<AgreementRowBase> rows = agreementBase.getRows();
            Set<AgreementVersion> agreementVersionList = agreementBase.getHead()
                                                                      .getAgreementVersionList();
            List<AgreementPricelistAPI> priceLists = new PriceListAPIBuilder().rows(rows)
                                                                              .hjmtjProductRepository(hjmtjProductRepository)
                                                                              .productRepository(productRepository)
                                                                              .hjmtjGuaranteeUnitRepository(hjmtjGuaranteeUnitRepository)
                                                                              .supplier(supplierAPI)
                                                                              .agreementVersionList(agreementVersionList)
                                                                              .migrationErrorRepository(migrationErrorRepository)
                                                                              .agreementBase(agreementBase)
                                                                              .buildAll();

            R build = builder
                    .agreement(agreementBase)
                    .supplier(supplierAPI)
                    .hjmtjOrganizationRepository(hjmtjOrganizationRepository)
                    .hjmtjUserRepository(hjmtjUserRepository)
                    .productOrganizationRepository(productOrganizationRepository)
                    .customerRepository(customerRepository)
                    .hjmtjCvGuaranteeUnitRepository(hjmtjGuaranteeUnitRepository)
                    .migrationErrorRepository(migrationErrorRepository)
                    .priceLists(priceLists)
                    .hjmtjCustomerRepository(hjmtjCustomerRepository)
                    .hjmtjSupplierRepository(hjmtjSupplierRepository)
                    .organizationController(organizationController)
                    .build();
            return Optional.ofNullable(build);
        } catch (MigrationException e) {
            MigrationError migrationError = new MigrationError();
            migrationError.setClassifier(Classifier.AGREEMENT);
            migrationError.setOrganizationName(supplierAPI.getOrganizationName());
            migrationError.setAgreementNumber(agreementBase.getHead()
                                                           .getSellerAgreementNo());
            migrationError.setMessage(e.getMessage());
            migrationErrorRepository.add(migrationError);
        } catch (Exception e) {
            log.error("{}", e);
        }

        return Optional.empty();
    }

    public Optional<AgreementAPIBuilderBase.AgreementAPICreated> toAgreement(AgreementBase agreementBase, OrganizationAPI supplierAPI) {
        return map(agreementBase, supplierAPI, new AgreementAPIBuilder());
    }


}

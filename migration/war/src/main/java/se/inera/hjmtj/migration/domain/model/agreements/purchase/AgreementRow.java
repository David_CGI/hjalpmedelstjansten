/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.agreements.purchase;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import se.inera.hjmtj.migration.domain.model.Reference;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementRowBase;

import javax.inject.Named;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PersistenceUnit;
import javax.xml.bind.annotation.XmlElement;

@PersistenceUnit(unitName = "local")
@Entity
@RequiredArgsConstructor
@Getter
@Setter
@NamedNativeQuery(name=AgreementRow.COUNT_ALL, query="SELECT COUNT(*)\n" +
                "FROM AGREEMENTROW r\n" +
                "       JOIN AGREEMENTPRODUCT A on r.AGREEMENTARTICLE_VGRPRODNO = A.VGRPRODNO\n" +
                "       JOIN PRODUCT P on A.ARTICLE_VGRPRODNO = P.VGRPRODNO\n" +
                "WHERE p.ISO9999 = TRUE AND \n" +
                "      p.NOARTICLETYPE = FALSE")
public class AgreementRow extends AgreementRowBase<AgreementProduct, AgreementStatus, AgreementWarranty> {

    public static final String COUNT_ALL = "AgreementRow.COUNT_ALL";

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private AgreementProduct agreementArticle;

    @OneToOne(fetch = FetchType.EAGER)
    private AgreementStatus status;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @XmlElement(name = "e1ca9861c0b0442dba7d9d3230f36289")
    private AgreementWarranty warranty;

    @XmlElement(name = "Product")
    public void setXmlProduct(Reference<AgreementProduct> reference) {
        agreementArticle = reference.getReference();
    }

    @XmlElement(name = "Status")
    public void setXmlStatus(Reference<AgreementStatus> reference) {
        status = reference.getReference();
    }

}

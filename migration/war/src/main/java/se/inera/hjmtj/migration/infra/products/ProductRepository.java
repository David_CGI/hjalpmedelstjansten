/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package se.inera.hjmtj.migration.infra.products;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.domain.HeapUsageInterceptor;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.domain.model.product.entities.Product;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductOrganization;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Stateless
@Interceptors({PerformanceLogInterceptor.class, HeapUsageInterceptor.class})
public class ProductRepository {

    @PersistenceContext(unitName = "local")
    private EntityManager em;

    public ProductRepository() {
    }

    public ProductRepository(EntityManager myEntityManager) {
        this.em = myEntityManager;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public void clear() {
        em.createNativeQuery("DELETE FROM PRODUCTORGANIZATION")
          .executeUpdate();
        em.createNativeQuery("DELETE FROM PRODUCT_ACCESSORYTO")
          .executeUpdate();
        em.createNativeQuery("DELETE FROM PRODUCT_SETTINGFOR")
          .executeUpdate();
        em.createNativeQuery("DELETE FROM PRODUCT_SERVICEFOR")
          .executeUpdate();
        em.createNativeQuery("DELETE FROM PRODUCT_SPAREPARTFOR")
          .executeUpdate();
        em.createNativeQuery("DELETE FROM PRODUCT_REPLACESLIST")
          .executeUpdate();
        em.createNativeQuery("DELETE FROM PRODUCT_PRODUCTCATEGORYSPECIFICPROPERTY")
          .executeUpdate();
        em.createNativeQuery("DELETE FROM PRODUCT")
          .executeUpdate();
        em.createNativeQuery("DELETE FROM PRODUCTCATEGORYPRODUCT")
          .executeUpdate();
        em.createNativeQuery("DELETE FROM PRODUCTCLASSIFICATION")
          .executeUpdate();
        em.createNativeQuery("DELETE FROM PRODUCTUNIT")
          .executeUpdate();
    }

    public void clearImageMigration() {
        em.createNativeQuery("DELETE FROM IMAGEMIGRATION")
          .executeUpdate();
    }

    private long size() {
        return em.createQuery("SELECT COUNT(p) FROM Product p ", Long.class)
                 .setMaxResults(1)
                 .getSingleResult();
    }

    public Product findByVgrProductNumber(String vgrProdNo) {
        try {
            return em.createQuery("SELECT p FROM Product p WHERE p.vgrProdNo = :vgrProdNo", Product.class)
                     .setParameter("vgrProdNo", Long.valueOf(vgrProdNo))
                     .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public Optional<List<ProductOrganization>> findAllProductsWithImageUrls() {
        try {
            return Optional.of(em
                    .createQuery("SELECT distinct po FROM ProductOrganization po " +
                            "LEFT JOIN FETCH po.products p " +
                            "LEFT JOIN FETCH p.images " +
                            "LEFT JOIN FETCH p.documents " +
                            "LEFT JOIN FETCH p.supplier s where s.gln = po.gln OR s.name = po.name ", ProductOrganization.class)
                    .getResultList());
        } catch (NoResultException e) {
            return Optional.empty();
        }
    }

    public Product findById(long id) {
        List<Product> product = em.createQuery("SELECT p FROM Product p WHERE p.id = :id", Product.class)
                                  .setParameter("id", id)
                                  .getResultList();
        return product.size() > 0 ? product.get(0) : null;
    }

    public Map<Long, String> catalogueUniqueNumberToSupplierNumbers() {
        List<Object[]> resultList = em.createQuery("SELECT p.vgrProdNo, p.supplierProductNumber FROM Product p")
                                      .setHint("org.hibernate.readOnly", true)
                                      .getResultList();

        HashMap<Long, String> catalogueUniqueNumberToSupplierNumbers = new HashMap<>();
        for (Object[] array : resultList) {
            Long catalogueUniqueNumber = (Long) array[0];
            String supplierNumber = (String) array[1];
            catalogueUniqueNumberToSupplierNumbers.put(catalogueUniqueNumber, supplierNumber);
        }

        return catalogueUniqueNumberToSupplierNumbers;
    }
}

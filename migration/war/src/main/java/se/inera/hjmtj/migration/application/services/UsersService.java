/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.application.services;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.business.DateUtils;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.user.controller.RoleController;
import se.inera.hjalpmedelstjansten.model.api.*;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCountryAPI;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.PostAddress;
import se.inera.hjalpmedelstjansten.model.entity.UserAccount;
import se.inera.hjmtj.migration.domain.DateTimes;
import se.inera.hjmtj.migration.domain.csv.Utf8Csv;
import se.inera.hjmtj.migration.domain.model.Indexed;
import se.inera.hjmtj.migration.domain.model.actors.Actor;
import se.inera.hjmtj.migration.domain.model.actors.ActorContactInformation;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.domain.model.organization.entities.Customer;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.domain.model.product.api.Account;
import se.inera.hjmtj.migration.domain.model.result.UserReport;
import se.inera.hjmtj.migration.domain.model.result.UserResult;
import se.inera.hjmtj.migration.infra.actors.ActorsRepository;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjCountryRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjOrganizationRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjUserRepository;
import se.inera.hjmtj.migration.infra.organizations.CustomerRepository;
import se.inera.hjmtj.migration.infra.organizations.SupplierRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@ApplicationScoped
@Path("/users")
public class UsersService implements ReportServiceBase {

    public static final String SVERIGE = "Sverige";

    private final long now = DateUtils.beginningOfDay(DateTimes.toEpochMillis(LocalDateTime.now()))
                                      .getTime();

    private final boolean MIGRATION_SET_EMAILS = Boolean.valueOf(System.getenv()
                                                                       .getOrDefault("MIGRATION_SET_EMAILS", "false"));

    @Inject
    private SupplierRepository supplierRepository;

    @Context
    private HttpServletRequest request;

    @Inject
    private CustomerRepository customerRepository;

    @Inject
    private ActorsRepository actorsRepository;

    @Inject
    private HjmtjUserRepository hjmtjUserRepository;

    @Inject
    private HjmtjCountryRepository hjmtjCountryRepository;

    @Inject
    private HjmtjOrganizationRepository hjmtjOrganizationRepository;

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    @Inject
    private RoleController roleController;

    @Inject
    private String testPassword;

    @GET
    @Path("/users.csv")
    @Produces("application/vnd.ms-excel;charset=utf-8")
    public byte[] users() {
        return generateReport("users", this::buildUsersReport);
    }

    private byte[] buildUsersReport() {
        StringBuilder sb = new StringBuilder();
        sb.append("Användarnamn;Titel;Förnamn;Efternamn;Epost;Organisation (1.0);Organisation (2.0);Affärsområde (2.0);Har roller i flera organisationer (1.0)?;Rollorganisationer (1.0);Rollnamn (1.0);Rolltyper (1.0);Roller (2.0);Godkänner prislista i avtalnr;Migrerades OK?\n");

        for (UserResult userResult : hjmtjUserRepository.results(actorsRepository.report())) {
            UserReport report = userResult.report();
            UserAPI userAPI = userResult.userAPI();

            sb.append(report.username());
            sb.append(";");

            sb.append(report.title());
            sb.append(";");

            sb.append(report.firstName());
            sb.append(";");

            sb.append(report.lastName());
            sb.append(";");

            sb.append(report.email()
                            .replace(";", ","));
            sb.append(";");

            sb.append(report.organisationName1_0());
            sb.append(";");

            List<String> organisations = userAPI != null ? userAPI.getUserEngagements()
                                                                  .stream()
                                                                  .map(UserEngagementAPI::getOrganizationId)
                                                                  .map(hjmtjOrganizationRepository::findById)
                                                                  .filter(Objects::nonNull)
                                                                  .map(OrganizationAPI::getOrganizationName)
                                                                  .collect(Collectors.toList()) : Collections.emptyList();
            sb.append(organisations);
            sb.append(";");

            List<String> businessLevels = userAPI != null ? userAPI.getUserEngagements()
                                                                   .stream()
                                                                   .map(UserEngagementAPI::getBusinessLevels)
                                                                   .filter(Objects::nonNull)
                                                                   .flatMap(Collection::stream)
                                                                   .filter(Objects::nonNull)
                                                                   .map(BusinessLevelAPI::getName)
                                                                   .filter(Objects::nonNull)
                                                                   .collect(Collectors.toList()) : Collections.emptyList();
            sb.append(businessLevels);
            sb.append(";");

            sb.append(booleanString(report.hasRolesInMultipleOrganizations()));
            sb.append(";");

            sb.append(report.roleOrganizations());
            sb.append(";");

            sb.append(report.roleNames1_0());
            sb.append(";");

            sb.append(report.roleType1_0());
            sb.append(";");

            List<String> roles2_0 = userAPI != null ? userAPI.getUserEngagements()
                                                             .stream()
                                                             .map(UserEngagementAPI::getRoles)
                                                             .filter(Objects::nonNull)
                                                             .flatMap(Collection::stream)
                                                             .filter(Objects::nonNull)
                                                             .map(RoleAPI::getDescription)
                                                             .filter(Objects::nonNull)
                                                             .collect(Collectors.toList()) : Collections.emptyList();
            sb.append(roles2_0);
            sb.append(";");

            sb.append(report.approvesPriceListsForAgreementNumbers());
            sb.append(";");

            sb.append(booleanString(userAPI != null));

            sb.append("\n");
        }

        return new Utf8Csv(sb.toString()).toBytes();
    }

    @GET
    @Path("/testusers.csv")
    @Produces("application/vnd.ms-excel;charset=utf-8")
    public byte[] testUsers() {
        return generateReport("testusers", this::buildTestUsersReport);
    }

    private byte[] buildTestUsersReport() {
        boolean allow = Boolean.valueOf(System.getenv()
                                              .getOrDefault("ALLOW_LIST_TESTUSERS", "false"));
        if (!allow) {
            throw new RuntimeException("Not allowed!");
        }

        StringBuilder sb = new StringBuilder();
        sb.append("Organisation;Användarnamn;Lösenord\n");

        for (Indexed<Customer> organization : customerRepository.rootOrganizations()) {
            sb.append(organization.value()
                                  .getOrganizationName());
            sb.append(";");

            Account account = Account.customer(organization.id(), organization.value()
                                                                              .getSecret());
            sb.append(account.name());
            sb.append(";");

            sb.append(organization.value()
                                  .getSecret()
                                  .getSecretValue());
            sb.append(";");
            sb.append("\n");
        }

        Indexed.Builder<Supplier> builder = Indexed.builder();
        List<Indexed<Supplier>> indexedSuppliers = supplierRepository.suppliers()
                                                                     .stream()
                                                                     .map(builder::build)
                                                                     .collect(Collectors.toList());
        for (Indexed<Supplier> supplierIndexed : indexedSuppliers) {
            Supplier organization = supplierIndexed.value();

            sb.append(organization.getName());
            sb.append(";");

            Account account = Account.supplier(supplierIndexed.id(), organization.getSecret());
            sb.append(account.name());
            sb.append(";");

            sb.append(organization.getSecret()
                                  .getSecretValue());
            sb.append(";");
            sb.append("\n");
        }

        return new Utf8Csv(sb.toString()).toBytes();
    }

    public void migrateUsers() {
        log.info("Creating Inera AB...");
        OrganizationAPI organizationAPI = createInera();

        try {
            organizationAPI = hjmtjOrganizationRepository.save(organizationAPI);
            createSuperAdmins(organizationAPI);
        } catch (HjalpmedelstjanstenValidationException e) {
            for (ErrorMessageAPI errorMessageAPI : e.getValidationMessages()) {
                MigrationError migrationError = new MigrationError();
                migrationError.setClassifier(Classifier.field(Classifier.INERA, errorMessageAPI.getField()));
                migrationError.setMessage(e.getMessage());
                migrationError.setOrganizationName(organizationAPI.getOrganizationName());
                migrationErrorRepository.add(migrationError);
            }
        }

        log.info("Done creating Inera AB!");

        log.info("Started migrating users...");

        for (Actor actor : actorsRepository.values()) {
            String userName = actor.getUserName();
            ActorContactInformation contactInformation = actor.getContactInformation();

            try {
                hjmtjUserRepository.saveUser(actor);
                log.info("Saved {}", contactInformation != null ? contactInformation
                        .getEmail() : userName);
            } catch (HjalpmedelstjanstenValidationException e) {
                for (ErrorMessageAPI errorMessageAPI : e.getValidationMessages()) {
                    MigrationError migrationError = new MigrationError();
                    migrationError.setClassifier(Classifier.field(Classifier.USER_ACCOUNT, errorMessageAPI.getField()));
                    migrationError.setSeverity(MigrationError.Severity.ERROR);
                    migrationError.setMessage(errorMessageAPI.getMessage());
                    migrationError.setUserName(actor.getUserName());
                    setOrganizationName(actor, migrationError);

                    migrationErrorRepository.add(migrationError);
                }
            }
        }

        log.info("Done migrating users!");
    }

    private OrganizationAPI createInera() {
        ElectronicAddressAPI electronicAddress = new ElectronicAddressAPI();

        if (MIGRATION_SET_EMAILS) {
            electronicAddress.setEmail("info@inera.se");
        } else {
            electronicAddress.setEmail(HjmtjUserRepository.createDummyEmail());
        }
        electronicAddress.setTelephone("0771-25 10 10");
        electronicAddress.setWeb("https://www.inera.se/");


        CVCountryAPI sweden = hjmtjCountryRepository.all()
                                                    .get(SVERIGE);

        PostAddressAPI deliveryAddress = new PostAddressAPI();
        deliveryAddress.setAddressType(PostAddress.AddressType.DELIVERY);
        deliveryAddress.setCity("Stockholm");
        deliveryAddress.setPostCode("118 93");
        deliveryAddress.setStreetAddress("Box 17703");

        PostAddressAPI visitAddress = new PostAddressAPI();
        visitAddress.setAddressType(PostAddress.AddressType.VISIT);

        OrganizationAPI organizationAPI = new OrganizationAPI();
        organizationAPI.setActive(true);
        organizationAPI.setCountry(sweden);
        organizationAPI.setElectronicAddress(electronicAddress);
        organizationAPI.setGln("0000000000000");
        organizationAPI.setOrganisationType(Organization.OrganizationType.SERVICE_OWNER.toString());
        organizationAPI.setOrganizationName("Inera AB");
        organizationAPI.setOrganizationNumber("556559-4230");
        organizationAPI.setValidFrom(now);
        organizationAPI.setPostAddresses(Arrays.asList(deliveryAddress, visitAddress));

        return organizationAPI;
    }

    private void createSuperAdmins(OrganizationAPI organizationAPI) throws HjalpmedelstjanstenValidationException {

        List<RoleAPI> rolesForOrganization = roleController.getRolesForOrganization(organizationAPI.getId());

        UserEngagementAPI userEngagementAPI = new UserEngagementAPI();
        userEngagementAPI.setOrganizationId(organizationAPI.getId());
        userEngagementAPI.setRoles(rolesForOrganization);
        userEngagementAPI.setValidFrom(now);

        ElectronicAddressAPI electronicAddress = new ElectronicAddressAPI();

        UserAPI userAPI = new UserAPI();
        userAPI.setUserEngagementAPIs(Collections.singletonList(userEngagementAPI));
        userAPI.setActive(true);
        userAPI.setLoginType(UserAccount.LoginType.PASSWORD.toString());

        electronicAddress.setTelephone("072-564 83 89");
        if (MIGRATION_SET_EMAILS) {
            electronicAddress.setEmail("aida.toromanovicfisic@inera.se");
        } else {
            electronicAddress.setEmail(HjmtjUserRepository.createDummyEmail());
        }
        userAPI.setUsername("Aida");
        userAPI.setFirstName("Aida");
        userAPI.setLastName("Toromanovic Fisic");
        userAPI.setTitle("Verksamhetsspecialist");
        userAPI.setElectronicAddress(electronicAddress);
        hjmtjUserRepository.saveUser(organizationAPI, userAPI);

        electronicAddress.setTelephone("072-884 56 60");
        if (MIGRATION_SET_EMAILS) {
            electronicAddress.setEmail("helena.ahlm@inera.se");
        } else {
            electronicAddress.setEmail(HjmtjUserRepository.createDummyEmail());
        }
        userAPI.setUsername("Helena");
        userAPI.setFirstName("Helena");
        userAPI.setLastName("Ahlm");
        userAPI.setTitle("Verksamhetsspecialist");
        userAPI.setElectronicAddress(electronicAddress);
        hjmtjUserRepository.saveUser(organizationAPI, userAPI);

        // Fake superuser for testing
        if (testPassword != null) {
            electronicAddress.setTelephone(null);
            electronicAddress.setEmail(HjmtjUserRepository.createDummyEmail());
            userAPI.setUsername("admin");
            userAPI.setFirstName("Admin");
            userAPI.setLastName("Adminsson");
            userAPI.setTitle("Testanvändare");
            userAPI.setElectronicAddress(electronicAddress);
            hjmtjUserRepository.saveUser(organizationAPI, userAPI);
            hjmtjUserRepository.setPassword(userAPI.getUsername(), testPassword);
        }
    }

    public void setOrganizationName(Actor actor, MigrationError migrationError) {
        if (actor.getOrganization() != null) {
            if (actor.getOrganization()
                     .getCustomer() != null) {
                migrationError.setOrganizationName(actor.getOrganization()
                                                        .getCustomer()
                                                        .getOrganizationName());
            } else if (actor.getOrganization()
                            .getSupplier() != null) {
                migrationError.setOrganizationName(actor.getOrganization()
                                                        .getSupplier()
                                                        .getName());
            } else {
                migrationError.setOrganizationName(actor.getOrganization()
                                                        .getName());
            }
        }
    }
}

package se.inera.hjmtj.migration.domain.model.actors;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.domain.model.Reference;
import se.inera.hjmtj.migration.domain.model.assortment.Assortment;
import se.inera.hjmtj.migration.domain.xml.TrimStringAdapter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.PersistenceUnit;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@PersistenceUnit(unitName = "local")
@Entity
@Slf4j
@Data
@XmlRootElement(name = "Role")
@Table(indexes = {
        @Index(columnList = "identifier"),
})
@NamedQuery(name = ActorRole.FIND_BY_IDENTIFIER, query = "SELECT DISTINCT r FROM ActorRole r WHERE r.identifier = :identifier")
public class ActorRole {

    public static final String FIND_BY_IDENTIFIER = "ActorRole.FIND_BY_IDENTIFIER";

    @Column(nullable = false, unique = true)
    @XmlElement(name = "Identifier")
    private String identifier;

    @Id
    @XmlAttribute(name = "id")
    private Long id;

    @Enumerated(EnumType.STRING)
    @XmlElement(name = "CategoryId", required = true)
    private ActorRoleType roleType;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<Actor> actors;

    @XmlJavaTypeAdapter(TrimStringAdapter.class)
    @XmlElement(name = "RoleName")
    private String roleName;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<ActorOrganization> organizations;

    @ManyToMany(fetch = FetchType.EAGER)
    private final Set<Assortment> assortmentsOwned;

    public ActorRole() {
        actors = new HashSet<>();
        organizations = new HashSet<>();
        assortmentsOwned = new HashSet<>();
    }

    @XmlElement(name = "ActorList")
    public void setXmlActorList(Reference<Actor> reference) {
        Actor actor = reference.getReference();
        actors.add(actor);
        actor.getRoles().add(this);
    }

    @XmlElement(name = "Organization")
    public void setXmlOrganization(Reference<ActorOrganization> reference) {
        organizations.add(reference.getReference());
    }

    @XmlElement(name = "AssortmentList")
    public void setXmlAssortmentList(Reference<Assortment> reference) {
        assortmentsOwned.add(reference.getReference());
    }

    @Override
    public String toString() {
        return "ActorRole " + id + ", " + roleName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActorRole actorRole = (ActorRole) o;
        return id.equals(actorRole.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

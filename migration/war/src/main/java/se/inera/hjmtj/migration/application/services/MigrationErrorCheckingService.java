/*
 *
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 *  Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.application.services;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjmtj.migration.application.bootstrap.Bootstrap;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.domain.model.product.AssistiveProduct;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjSupplierRepository;
import se.inera.hjmtj.migration.infra.organizations.SupplierRepository;
import se.inera.hjmtj.migration.infra.products.ProductOrganizationRepository;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
@ApplicationScoped
@Path("/validate")
public class MigrationErrorCheckingService {

    private final DecimalFormat df = new DecimalFormat("#.###");

    private final Validator validator = Validation.buildDefaultValidatorFactory()
                                                  .getValidator();

    @Inject
    private ProductOrganizationRepository productOrganizationRepository;

    @Inject
    private SupplierRepository supplierRepository;

    @Inject
    private Bootstrap bootstrap;

    @Inject
    private SuppliersService suppliersService;

    @Inject
    private HjmtjSupplierRepository hjmtjSupplierRepository;

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    @PostConstruct
    public void postConstruct() {
        bootstrap.start();
        suppliersService.migrateSuppliers();
    }

    @GET
    @Path("/")
    public void start() {
        Result result = new Result();

        for (Supplier supplier : supplierRepository.suppliers()) {
            OrganizationAPI organization = hjmtjSupplierRepository.find(supplier);
            List<AssistiveProduct> assistiveProducts = productOrganizationRepository.findAssistiveProducts(supplier, organization);
            result = result.add(assertCount(assistiveProducts, organization));
        }

        log.warn("Finished with {} of {} ({}%)", result.getSuccessful(), result.getTotal(), percentage(result)); // 457176
    }

    private Result assertCount(List<AssistiveProduct> assistiveProducts, OrganizationAPI organization) {
        int successful = 0;
        int total = 0;

        for (AssistiveProduct assistiveProduct : assistiveProducts) {
            Set<ErrorMessageAPI> errorMessageAPIs = validate(assistiveProduct);

            for (ErrorMessageAPI errorMessageAPI : errorMessageAPIs) {
                MigrationError migrationError = new MigrationError();
                migrationError.setOrganizationName(organization.getOrganizationName());
                migrationError.setCatalogueUniqueNumber(assistiveProduct.getCatalogueUniqueNumber());
                migrationError.setClassifier(Classifier.field(Classifier.HJALPMEDEL, errorMessageAPI.getField()));
                migrationError.setMessage(errorMessageAPI.getMessage());
                migrationErrorRepository.add(migrationError);
            }

            total++;
            successful = errorMessageAPIs.isEmpty() ? successful + 1 : successful;
        }

        Result result = new Result(successful, total);
        if (result.getTotal() == result.getSuccessful()) {
            log.info("{} : {} of {} ({}%)", organization.getOrganizationName(), successful, total, percentage(result));
        } else {
            log.warn("{} : {} of {} ({}%)", organization.getOrganizationName(), successful, total, percentage(result));
        }
        return result;
    }

    private String percentage(Result result) {
        return result.getTotal() == 0 ? "100" : df.format(100 * ((double) result.getSuccessful() / (double) result.getTotal()));
    }

    private Set<ErrorMessageAPI> validate(AssistiveProduct assistiveProduct) {
        Set<ErrorMessageAPI> errorMessageAPIs = new HashSet<>();

        Set<ConstraintViolation<AssistiveProduct>> constraintViolations = validator.validate(assistiveProduct, Default.class);
        if (constraintViolations != null && !constraintViolations.isEmpty()) {
            for (ConstraintViolation constraintViolation : constraintViolations) {
                errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage(constraintViolation.getPropertyPath()
                                                                                                                         .toString(), constraintViolation.getMessage()));
            }
        }

        return errorMessageAPIs;
    }

    @Getter
    class Result {

        int successful;

        int total;

        Result() {
            successful = 0;
            total = 0;
        }

        Result(int successful, int total) {
            this.successful = successful;
            this.total = total;
        }

        public Result add(Result result) {
            return new Result(successful + result.getSuccessful(), total + result.getTotal());
        }
    }

}

/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.hjmtj;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.business.organization.controller.CountryController;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCountryAPI;
import se.inera.hjmtj.migration.infra.BulkService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static javax.transaction.Transactional.TxType.NOT_SUPPORTED;

@Slf4j
@Stateless
public class HjmtjCountryRepository {

    @Inject
    private CountryController countryController;

    @Transactional(NOT_SUPPORTED)
    public Map<String, CVCountryAPI> all()  {
        return countryController.getAllCountries()
                                            .stream()
                                            .collect(Collectors.toMap(CVCountryAPI::getName, Function.identity()));
    }

}

/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.sql;

import lombok.Value;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Value
@Accessors(fluent = true)
public class SQLMultiLineFile {

    private final List<String> queryList;

    public SQLMultiLineFile(String path) {
        StringBuilder sb = new StringBuilder();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(path)))) {
            while (br.ready()) {
                String line = br.readLine();

                if (!line.startsWith("--") && !line.trim()
                                                   .isEmpty()) {
                    sb.append(line);
                    sb.append("\n");
                }
            }
        } catch (IOException e) {
            throw new IllegalStateException("Failed to read file: " + path, e);
        }

        String[] split = sb.toString()
                           .split(";");
        this.queryList = Arrays.stream(split)
                               .filter(this::nonEmpty)
                               .collect(Collectors.toList());
    }

    private boolean nonEmpty(String s) {
        return !s.trim()
                 .isEmpty();
    }

    public int executeUpdate(EntityManager em) {
        int updateCount = 0;

        for (String queryString : queryList) {
            Query query = em.createNativeQuery(queryString);
            updateCount += query.executeUpdate();
        }

        return updateCount;
    }

}

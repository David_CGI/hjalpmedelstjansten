/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.application.services;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.business.indexing.controller.ElasticSearchController;
import se.inera.hjmtj.migration.application.bootstrap.Bootstrap;
import se.inera.hjmtj.migration.domain.MigrationLock;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.ServiceUnavailableException;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@ApplicationScoped
@Path("/migration")
public class MigrationService {

    @Inject
    private MigrationLock migrationLock;

    @Inject
    private Bootstrap bootstrap;

    @Inject
    private CustomersService customersService;

    @Inject
    private SuppliersService suppliersService;

    @Inject
    private AssistiveProductsService assistiveProductsService;

    @Inject
    private AgreementsService agreementsService;

    @Inject
    private GeneralPricelistsService generalPricelistsService;

    @Inject
    private AssortmentsService assortmentsService;

    @Inject
    private ElasticSearchController elasticSearchController;

    @Inject
    private UsersService usersService;

    @Inject
    private S3MediaService mediaService;

    @Inject
    private ExportSettingsService exportSettingsService;

    @PostConstruct
    public void postConstruct() {
        bootstrap.start();
    }

    @GET
    @Path("/nop")
    public Response nop() {
        return Response.ok()
                       .build();
    }

    @GET
    @Path("/")
    public void start(@QueryParam("products") @DefaultValue("true") boolean products,
                      @QueryParam("agreements") @DefaultValue("true") boolean agreements,
                      @QueryParam("gps") @DefaultValue("true") boolean gp,
                      @QueryParam("assortments") @DefaultValue("true") boolean assortments,
                      @QueryParam("testusers") @DefaultValue("true") boolean testUsers,
                      @QueryParam("users") @DefaultValue("true") boolean users,
                      @QueryParam("media") @DefaultValue("true") boolean media,
                      @QueryParam("exportsettings") @DefaultValue("true") boolean exportSettings,
                      @QueryParam("offset") @DefaultValue("0") int offset,
                      @QueryParam("limit") Integer limit) throws ServiceUnavailableException {
        migrationLock.tryAcquire();
        log.info("Started migrating data!");
        long startMillis = System.currentTimeMillis();

        log.info("Users = {}", users);
        log.info("Products = {}", products);
        log.info("Agreements = {}", agreements);
        log.info("GPs = {}", gp);
        log.info("Assortments = {}", assortments);
        log.info("Test users = {}", testUsers);
        log.info("Media = {}", media);
        log.info("Offset = {}", offset);
        log.info("Limit = {}", limit);

        try {
            initializeElasticSearch();

            List<CustomersService.IdCustomerOrganizationAPI> customers = customersService.migrateCustomers();
            List<SuppliersService.SupplierOrganizationAPI> suppliers = suppliersService.migrateSuppliers();

            int min = Math.min(offset, suppliers.size());
            int max = limit == null ? suppliers.size() : Math.min(limit + offset, suppliers.size());
            suppliers = suppliers.subList(min, max);

            log.info("Supplier(s) {} to {}", min, max);

            if (users) {
                usersService.migrateUsers();
            }
            if (products) {
                assistiveProductsService.saveAssistiveProducts(suppliers);
            }
            if (agreements) {
                agreementsService.migrateAgreements(suppliers);
            }
            if (gp) {
                generalPricelistsService.migrateGeneralPricelists(suppliers);
            }
            if (assortments) {
                assortmentsService.migrateAssortments();
            }
            if (testUsers) {
                customersService.migrateTestUsers(customers);
                suppliersService.migrateTestUsers(suppliers);
            }
            if (media) {
                List<Supplier> suppliersList = suppliers.stream()
                                                        .map(SuppliersService.SupplierOrganizationAPI::getSupplier)
                                                        .collect(Collectors.toList());
                mediaService.migrateSupplierImages(suppliersList);
            }
            if (exportSettings) {
                exportSettingsService.migrateExportSettings();
            }
        } finally {
            migrationLock.release();
        }

        long elapsedMillis = System.currentTimeMillis() - startMillis;
        long elapsedSeconds = elapsedMillis / 1000;
        long elapsedMinutes = elapsedSeconds / 60;

        log.info("Done migrating data after {} minutes!", elapsedMinutes);
    }

    private void initializeElasticSearch() {
        log.info("Add elastic index and search mapping");
        if (elasticSearchController.indexExists()) {
            elasticSearchController.deleteIndex();
        }
        if (!elasticSearchController.createIndex()) {
            log.warn("Failed to create elastic search index. Search will not work properly");
        }
    }
}

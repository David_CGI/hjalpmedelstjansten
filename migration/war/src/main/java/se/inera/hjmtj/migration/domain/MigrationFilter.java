/*
 *
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 *  Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain;

import se.inera.hjalpmedelstjansten.model.api.AgreementAPI;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjmtj.migration.domain.model.agreements.builders.GeneralPricelistAPIWithMultiplePricelists;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MessageTemplate;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;

import javax.ejb.Singleton;
import javax.inject.Inject;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * HJAL-1259
 *
 * Rules regarding what data to migrate and what not to migrate.
 *
 */
@Singleton
public class MigrationFilter {

    private final LocalDate AGREEMENT_PRICELIST_VALID_ENDS_MIN = LocalDate.of(2017, 7, 1);

    private final LocalDate ASSISTIVE_PRODUCT_EXPIRED_MIN = LocalDate.of(1994, 12, 31);

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    public MigrationFilter() {
    }

    public MigrationFilter(MigrationErrorRepository migrationErrorRepository) {
        this.migrationErrorRepository = migrationErrorRepository;
    }

    public boolean isDeprecated(ProductAPI productAPI, Long catalogueUniqueNumber) {
        if (productAPI.getReplacementDate() == null) {
            return false;
        } else if (DateTimes.epochMillisToDate(productAPI.getReplacementDate())
                            .isBefore(ASSISTIVE_PRODUCT_EXPIRED_MIN)) {
            MigrationError migrationError = new MigrationError();
            migrationError.setSeverity(MigrationError.Severity.WARNING);
            migrationError.setOrganizationName(productAPI.getOrganizationName());
            migrationError.setCatalogueUniqueNumber(catalogueUniqueNumber);
            migrationError.setClassifier(Classifier.HJALPMEDEL_CUSTOMER_UNIQUE);
            migrationError.setMessage(MessageTemplate.EXPIRED_ASSISTIVE_PRODUCT_IS_DEPRECATED, ASSISTIVE_PRODUCT_EXPIRED_MIN.toString());
            migrationErrorRepository.add(migrationError);

            return true;
        }

        return false;
    }

    public boolean isDeprecated(ArticleAPI articleAPI, Long catalogueUniqueNumber) {
        if (articleAPI.getReplacementDate() == null) {
            return false;
        } else if (DateTimes.epochMillisToDate(articleAPI.getReplacementDate())
                            .isBefore(ASSISTIVE_PRODUCT_EXPIRED_MIN)) {
            MigrationError migrationError = new MigrationError();
            migrationError.setSeverity(MigrationError.Severity.WARNING);
            migrationError.setOrganizationName(articleAPI.getOrganizationName());
            migrationError.setCatalogueUniqueNumber(catalogueUniqueNumber);
            migrationError.setClassifier(Classifier.HJALPMEDEL_CUSTOMER_UNIQUE);
            migrationError.setMessage(MessageTemplate.EXPIRED_ASSISTIVE_PRODUCT_IS_DEPRECATED, ASSISTIVE_PRODUCT_EXPIRED_MIN.toString());
            migrationErrorRepository.add(migrationError);

            return true;
        }

        return false;
    }

    public boolean isDeprecated(AgreementAPI agreementAPI, AgreementPricelistAPI pricelistAPI) {
        Long validFrom = pricelistAPI.getValidFrom();

        if (validFrom == null) {
            // Invalid pricelist that will be migrated as future
            return false;
        }

        Iterator<AgreementPricelistAPI> it = agreementAPI.getPricelists() != null ? agreementAPI.getPricelists()
                                                                                                .iterator() : null;
        Long validEnds = null;
        while (it != null && it.hasNext()) {
            AgreementPricelistAPI tmp = it.next();

            if (pricelistAPI.equals(tmp)) {
                if (it.hasNext()) {
                    validEnds = it.next()
                                  .getValidFrom();
                }
                break;
            }
        }

        if (validEnds == null) {
            // Still active
            return false;
        }

        if (DateTimes.epochMillisToDate(validEnds)
                     .isBefore(AGREEMENT_PRICELIST_VALID_ENDS_MIN)) {
            MigrationError migrationError = new MigrationError();
            migrationError.setSeverity(MigrationError.Severity.WARNING);
            migrationError.setOrganizationName(agreementAPI.getSupplierOrganization() != null ? agreementAPI.getSupplierOrganization()
                                                                                                            .getOrganizationName() : null);
            migrationError.setClassifier(Classifier.PRICELIST_VALID_FROM);
            migrationError.setMessage(MessageTemplate.INACTIVE_PRICELIST_IS_DEPRECATED, AGREEMENT_PRICELIST_VALID_ENDS_MIN.toString());
            migrationErrorRepository.add(migrationError);

            return true;
        }

        return false;
    }

    public boolean isDeprecated(GeneralPricelistAPIWithMultiplePricelists generalPricelistPricelistAPI, GeneralPricelistPricelistAPI pricelistAPI) {
        // HJAL-1248 if the pricelist has null validFrom: copy the validFrom from the GP header
        if (pricelistAPI.getValidFrom() == null) {
            pricelistAPI.setValidFrom(generalPricelistPricelistAPI.getValidFrom());
        }

        List<GeneralPricelistPricelistAPI> pricelists = generalPricelistPricelistAPI.getPricelistAPIs();

        // If there is one pricelist: migrate it
        if (pricelists.size() == 1) {
            return false;
        }

        // If there are several pricelists: migrate only the active pricelist (i.e. the largest validFrom that is not in the future)
        List<GeneralPricelistPricelistAPI> sortedPriceLists = pricelists.stream()
                                                                        .filter(this::pricelistIsNonFuture)
                                                                        .sorted(Comparator.comparingLong(GeneralPricelistPricelistAPI::getValidFrom))
                                                                        .collect(Collectors.toList());

        GeneralPricelistPricelistAPI priceListToMigrate = sortedPriceLists.get(sortedPriceLists.size() - 1);
        boolean priceListAPIIsDeprecated = !priceListToMigrate.equals(pricelistAPI);

        if (priceListAPIIsDeprecated) {
            // The pricelist passed to this method is not active and should not be migrated
            MigrationError migrationError = new MigrationError();
            migrationError.setSeverity(MigrationError.Severity.WARNING);
            migrationError.setOrganizationName(generalPricelistPricelistAPI.getOwnerOrganization() != null ? generalPricelistPricelistAPI.getOwnerOrganization()
                                                                                                                                         .getOrganizationName() : null);
            migrationError.setClassifier(Classifier.GP_PRICELIST);
            migrationError.setMessage(MessageTemplate.INACTIVE_GP);
            migrationErrorRepository.add(migrationError);

            return true;
        }

        return false;
    }

    private boolean pricelistIsNonFuture(GeneralPricelistPricelistAPI generalPricelistPricelistAPI) {
        return generalPricelistPricelistAPI.getValidFrom() <= System.currentTimeMillis();
    }

}

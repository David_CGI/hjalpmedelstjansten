/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.organization.entities;

import lombok.Data;
import se.inera.hjalpmedelstjansten.model.api.BusinessLevelAPI;
import se.inera.hjalpmedelstjansten.model.api.ElectronicAddressAPI;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.api.PostAddressAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCountryAPI;
import se.inera.hjalpmedelstjansten.model.entity.PostAddress;
import se.inera.hjmtj.migration.domain.DateTimes;
import se.inera.hjmtj.migration.domain.model.agreements.gp.ExportSetting;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.AgreementOrganization;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjOrganizationRepository;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PersistenceUnit;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;

@PersistenceUnit(unitName = "local")
@Data
@Entity
@Table(indexes = {
        @Index(columnList = "organizationName"),
        @Index(columnList = "gln"),
})
@NamedQueries({
        @NamedQuery(name = Customer.FIND_BY_NAME, query = "SELECT c FROM Customer c WHERE c.organizationName = :name"),
        @NamedQuery(name = Customer.FIND_BY_TYPE_AND_GLN, query = "SELECT o FROM Customer o WHERE o.type = :type AND o.gln = :gln")
})
public class Customer {

    public static final LocalDate DEFAULT_VALID_FROM = LocalDate.of(2015, 4, 1);

    public static final String FIND_BY_NAME = "Customer.FIND_BY_NAME";

    public static final String FIND_BY_TYPE_AND_GLN = "Customer.FIND_BY_TYPE_AND_GLN";

    public static final String BUSINESS_AREA = "Verksamhetsområde";

    private static final String ACTIVE = "ACTIVE";

    private static final String CUSTOMER = "CUSTOMER";

    @Id
    private String organizationName;

    private String type;

    private String gln;

    private String addressCity;

    private String organizationNumber;

    private LocalDate validFrom;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    private Secret secret;

    @OneToMany(mappedBy = "customer")
    private Set<AgreementOrganization> agreementOrganization;

    @OneToMany(mappedBy = "customer")
    private Set<ExportSetting> exportSettings;

    private String exportFileName;

    public boolean isBusinessLevel() {
        return type != null && type.equals(BUSINESS_AREA);
    }

    public OrganizationAPI toApi() {
        OrganizationAPI organization = new OrganizationAPI();

        organization.setOrganizationName(organizationName);
        organization.setOrganisationType(CUSTOMER);
        organization.setGln(gln);
        organization.setOrganizationNumber(organizationNumber);
        CVCountryAPI country = new CVCountryAPI();
        country.setId((long) HjmtjOrganizationRepository.SWEDEN);
        organization.setCountry(country);

        ElectronicAddressAPI electronicAddress = new ElectronicAddressAPI();
        organization.setElectronicAddress(electronicAddress);

        PostAddressAPI deliveryAddress = new PostAddressAPI();
        deliveryAddress.setAddressType(PostAddress.AddressType.DELIVERY);

        PostAddressAPI visitAddress = new PostAddressAPI();
        visitAddress.setAddressType(PostAddress.AddressType.DELIVERY);

        organization.setPostAddresses(Arrays.asList(deliveryAddress, visitAddress));

        LocalDate validFromToUse = validFrom != null ? validFrom : DEFAULT_VALID_FROM;
        organization.setValidFrom(DateTimes.toEpochMillis(validFromToUse));
        organization.setActive(true);

        return organization;
    }

    public BusinessLevelAPI toApi(OrganizationAPI rootOrganization) {
        BusinessLevelAPI businessLevelAPI = new BusinessLevelAPI();
        businessLevelAPI.setName(organizationName);
        businessLevelAPI.setStatus(ACTIVE);
        businessLevelAPI.setOrganization(rootOrganization);

        return businessLevelAPI;
    }

    @Override
    public int hashCode() {
        return Objects.hash(organizationName);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return organizationName.equals(customer.organizationName);
    }

}

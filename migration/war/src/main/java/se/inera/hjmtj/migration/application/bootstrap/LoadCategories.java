/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.application.bootstrap;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.domain.HeapUsageInterceptor;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.domain.model.categories.Category;
import se.inera.hjmtj.migration.domain.model.categories.CategorySpecificProperty;
import se.inera.hjmtj.migration.domain.model.categories.CategorySpecificPropertyListValue;
import se.inera.hjmtj.migration.infra.categories.CategoryRepository;
import se.inera.hjmtj.migration.infra.categories.CategorySpecificPropertyRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjCategoryRepository;

import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import static javax.ejb.TransactionAttributeType.NOT_SUPPORTED;


/**
 * Loads categories into mysql.
 */
@Slf4j
@Singleton
@TransactionAttribute(NOT_SUPPORTED)
@Interceptors({PerformanceLogInterceptor.class, HeapUsageInterceptor.class})
public class LoadCategories {

    @Inject
    private CategoryRepository categoryRepository;

    @Inject
    private CategorySpecificPropertyRepository categorySpecificPropertyRepository;

    @Inject
    private HjmtjCategoryRepository hjmtjCategoryRepository;

    public void run() {
        if (hjmtjCategoryRepository.isEmpty()) {
            int categories = 0;
            for (Category category : categoryRepository.values()) {
                categories += hjmtjCategoryRepository.add(category);
            }
            log.debug("Saved {} Category to HJMTJ 2.0", categories);

            int csp = 0;
            for (CategorySpecificProperty categorySpecificProperty : categorySpecificPropertyRepository.values()) {
                csp += hjmtjCategoryRepository.add(categorySpecificProperty);
            }
            log.debug("Saved {} CategorySpecificProperty to HJMTJ 2.0", csp);

            int cspvl = 0;
            for (CategorySpecificPropertyListValue categorySpecificPropertyListValue : categorySpecificPropertyRepository.listValues()) {
                cspvl += hjmtjCategoryRepository.add(categorySpecificPropertyListValue);
            }
            log.debug("Saved {} CategorySpecificPropertyListValue to HJMTJ 2.0", cspvl);
        }

        hjmtjCategoryRepository.initializeCache();
    }

}

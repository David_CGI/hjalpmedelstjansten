/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.application.services;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.domain.MigrationLock;
import se.inera.hjmtj.migration.domain.SupplierGLNCatalogueUniqueNumber;
import se.inera.hjmtj.migration.domain.csv.Utf8Csv;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementRowBase;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementStatusBase;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementVersion;
import se.inera.hjmtj.migration.domain.model.agreements.builders.AgreementAPIBuilderBase;
import se.inera.hjmtj.migration.domain.model.agreements.builders.AgreementBaseMapper;
import se.inera.hjmtj.migration.domain.model.agreements.builders.PriceListRowAPIBuilder;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.Agreement;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.domain.model.organization.entities.Customer;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.domain.model.product.entities.Product;
import se.inera.hjmtj.migration.infra.agreements.AgreementRepository;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjAgreementRepository;
import se.inera.hjmtj.migration.infra.organizations.SupplierRepository;
import se.inera.hjmtj.migration.infra.products.ProductRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@ApplicationScoped
@Path("/agreements")
public class AgreementsService implements ReportServiceBase {

    @Context
    private HttpServletRequest request;

    @Inject
    private MigrationLock migrationLock;

    @Inject
    private AgreementRepository agreementRepository;

    @Inject
    private HjmtjAgreementRepository hjmtjAgreementRepository;


    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    @Inject
    private AgreementBaseMapper mapper;

    @Inject
    private SupplierRepository supplierRepository;

    @Inject
    private ProductRepository productRepository;

    @POST
    @Path("/")
    public void migrateAgreements(List<SuppliersService.SupplierOrganizationAPI> organizations) {
        log.info("Started migrating agreements...");

        for (SuppliersService.SupplierOrganizationAPI entry : organizations) {
            Supplier supplier = entry.getSupplier();
            List<Agreement> agreementList = agreementRepository.findBy(supplier);
            log.info("{} has {} agreements", supplier.getName(), agreementList.size());
            List<AgreementAPIBuilderBase.AgreementAPICreated> agreements = agreementList.stream()
                                                                                        .map(agreement -> mapper.toAgreement(agreement, entry.getOrganizationAPI()))
                                                                                        .filter(Optional::isPresent)
                                                                                        .map(Optional::get)
                                                                                        .collect(Collectors.toList());

            for (AgreementAPIBuilderBase.AgreementAPICreated agreementAPI : agreements) {
                try {
                    hjmtjAgreementRepository.save(agreementAPI, agreementAPI.getCreated());
                } catch (Exception e) {
                    MigrationError migrationError = new MigrationError();
                    migrationError.setSeverity(MigrationError.Severity.ERROR);
                    migrationError.setClassifier(Classifier.AGREEMENT);
                    migrationError.setOrganizationName(entry.getOrganizationAPI()
                                                            .getOrganizationName());
                    migrationError.setMessage(e.getMessage());
                    migrationErrorRepository.add(migrationError);
                }
            }
        }

        log.info("Started migrating agreements!");
    }

    @GET
    @Path("/agreement_rows.csv")
    @Produces("text/csv;charset=utf-8")
    public byte[] rowsCsv() {
        return generateReport("agreement_rows.csv", this::buildRowsCsv);
    }

    private byte[] buildRowsCsv() {
        migrationLock.tryAcquire();
        StringBuilder sb = new StringBuilder();

        try {
            Set<SupplierGLNCatalogueUniqueNumber> result = hjmtjAgreementRepository.result();
            sb.append("Köpare;Leverantör;Avtalsnummer;Versionslistor;Är ISO9999?;Är artikel utan typ?;Prislistenummer på rad;Giltigt från på rad;Katalogunikt nummer på rad;Artikelnummer;Aktiv?;Migrerades OK?\n");

            for (Supplier supplier : supplierRepository.suppliers()) {
                List<Agreement> agreements = agreementRepository.findBy(supplier);
                log.info("Size = {}", agreements.size());

                for (Agreement agreement : agreements) {
                    if (agreement.getRows() != null) {
                        Customer customer = agreement.getBuyer()
                                                     .getAgreementOrganization()
                                                     .getCustomer();

                        for (AgreementRowBase row : agreement.getRows()) {
                            if (customer != null && supplier != null) {
                                sb.append(customer.getOrganizationName());
                                sb.append(";");

                                sb.append(supplier.getName());
                                sb.append(";");

                                sb.append(agreement.getHead()
                                                   .getSellerAgreementNo());
                                sb.append(";");

                                sb.append(agreement.getHead()
                                                   .getAgreementVersionList()
                                                   .stream()
                                                   .map(this::stringValue)
                                                   .collect(Collectors.joining(", ")));
                                sb.append(";");

                                Product article = row.getAgreementArticle() != null ? productRepository.findByVgrProductNumber(row.getAgreementArticle()
                                                                                                                                  .getVgrProdNo()) : null;
                                sb.append(booleanString(article != null && article.getXmlCategories().isIso9999()));
                                sb.append(";");

                                sb.append(booleanString(article != null && article.isNoArticleType()));
                                sb.append(";");

                                sb.append(row.getSupplierPriceListNumber());
                                sb.append(";");

                                sb.append(row.getValidFrom());
                                sb.append(";");

                                sb.append(row.getAgreementArticle()
                                             .getVgrProdNo());
                                sb.append(";");

                                sb.append(article != null ? article.getSupplierProductNumber() : "");
                                sb.append(";");

                                AgreementStatusBase rowStatus = row.getStatus();
                                String status = rowStatus != null ? rowStatus.getValue() : "";
                                sb.append(booleanString(status.equalsIgnoreCase(PriceListRowAPIBuilder.VISUERA_AKTIV)));
                                sb.append(";");

                                sb.append(booleanString(result.contains(new SupplierGLNCatalogueUniqueNumber(supplier.getGln(), Long.valueOf(row.getAgreementArticle()
                                                                                                                                                .getVgrProdNo())))));
                                sb.append(";");

                                sb.append("\n");
                            }
                        }
                    }
                }
            }

            return new Utf8Csv(sb.toString()).toBytes();
        } finally {
            migrationLock.release();
        }
    }

    private String stringValue(AgreementVersion agreementVersion) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append("Nummer = ");
        sb.append(agreementVersion.getVersionNumber() != null ? agreementVersion.getVersionNumber() : "NULL");
        sb.append(", ");
        sb.append("Giltig från = ");
        sb.append(agreementVersion.getValidFrom() != null ? agreementVersion.getValidFrom() : "NULL");
        sb.append("]");
        return sb.toString();
    }

    @GET
    @Path("/agreements.csv")
    @Produces("text/csv;charset=utf-8")
    public byte[] agreementsCsv() {
        return generateReport("agreements.csv", this::buildAgreementsReport);
    }

    private byte[] buildAgreementsReport() {
        migrationLock.tryAcquire();

        try {
            List<Agreement> agreements = agreementRepository.values();
            log.info("Size = {}", agreements.size());

            Map<String, List<se.inera.hjalpmedelstjansten.model.entity.Agreement>> supplierAgreements = hjmtjAgreementRepository.values()
                                                                                                                                .stream()
                                                                                                                                .collect(Collectors.groupingBy(this::organizationName));

            StringBuilder sb = new StringBuilder();
            sb.append("Leverantör;Leverantör matchas?;Köpare;Köpare matchas?;Titel;Avtalsnummer;Migrerades OK?\n");

            for (Agreement agreement : agreements) {
                Supplier supplier = agreement.getSeller()
                                             .getAgreementOrganization()
                                             .getSupplier();

                sb.append(supplier != null ? supplier.getName() : agreement.getSeller()
                                                                           .getAgreementOrganization()
                                                                           .getName());
                sb.append(";");

                sb.append(booleanString(supplier != null));
                sb.append(";");

                Customer customer = agreement.getBuyer()
                                             .getAgreementOrganization()
                                             .getCustomer();
                sb.append(customer != null ? customer.getOrganizationName() : agreement.getBuyer()
                                                                                       .getAgreementOrganization()
                                                                                       .getName());
                sb.append(";");

                sb.append(booleanString(customer != null));
                sb.append(";");

                sb.append(agreement.getHead()
                                   .getTitle());
                sb.append(";");

                String agrementNumber = agreement.getHead()
                                                 .getSellerAgreementNo();
                sb.append(agrementNumber);
                sb.append(";");

                boolean migrated = false;
                if (supplier != null) {
                    for (se.inera.hjalpmedelstjansten.model.entity.Agreement hjmtjAgreement : supplierAgreements.getOrDefault(supplier.getName(), Collections.emptyList())) {
                        if (hjmtjAgreement.getAgreementNumber()
                                          .equalsIgnoreCase(agrementNumber)) {
                            migrated = true;
                            break;
                        }
                    }
                }
                sb.append(booleanString(migrated));

                sb.append("\n");
            }

            return new Utf8Csv(sb.toString()).toBytes();
        } finally {
            migrationLock.release();
        }
    }

    private String organizationName(se.inera.hjalpmedelstjansten.model.entity.Agreement agreement) {
        return agreement.getSupplier() == null ? "-" : agreement.getSupplier()
                                                                .getOrganizationName();
    }


}

/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.application.bootstrap;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.domain.HeapUsageInterceptor;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

/**
 * Prepares H2 and MySQL database with data from input files e.g. CSVs, XMLs etc.
 */
@Slf4j
@ApplicationScoped
@Interceptors({PerformanceLogInterceptor.class, HeapUsageInterceptor.class})
public class Bootstrap {

    @Inject
    private LoadAgreements loadAgreements;

    @Inject
    private LoadAssistiveProducts loadAssistiveProducts;

    @Inject
    private LoadCategories loadCategories;

    @Inject
    private LoadGeneralPricelist loadGeneralPricelist;

    @Inject
    private LoadMisc loadMisc;

    @Inject
    private LoadSecrets loadSecrets;

    @Inject
    private LoadOrganizations loadOrganizations;

    @Inject
    private LoadAssortments loadAssortments;

    @Inject
    private LoadActors loadActors;

    @Inject
    private LoadPricelistApprovers loadPricelistApprovers;

    @Inject
    private LoadAssortmentOwners loadAssortmentOwners;

    @Inject
    private boolean tokenCleanupTimerEnabled;

    @Inject
    private boolean productDiscontinuedTimerEnabled;

    @Inject
    private boolean articleDiscontinuedTimerEnabled;

    @Inject
    private boolean agreementDiscontinuedTimerEnabled;

    @Inject
    private boolean internalAuditCleanerTimerEnabled;

    @Inject
    private boolean reindexProductsAndArticlesTimerEnabled;

    @Inject
    private LoadMedia prepareModel;
    
    @Inject
    private LoadExportSettings loadExportSettings;

    public synchronized void start() {
        log.info("tokenCleanupTimerEnabled = {}", tokenCleanupTimerEnabled);
        log.info("productDiscontinuedTimerEnabled = {}", productDiscontinuedTimerEnabled);
        log.info("articleDiscontinuedTimerEnabled = {}", articleDiscontinuedTimerEnabled);
        log.info("agreementDiscontinuedTimerEnabled = {}", agreementDiscontinuedTimerEnabled);
        log.info("internalAuditCleanerTimerEnabled = {}", internalAuditCleanerTimerEnabled);
        log.info("reindexProductsAndArticlesTimerEnabled = {}", reindexProductsAndArticlesTimerEnabled);

        log.info("=== (1/13) Loading CVs ===");
        loadMisc.run();

        log.info("=== (2/13) Loading organizations ===");
        loadOrganizations.run();

        log.info("=== (3/13) Loading secrets ===");
        loadSecrets.run();

        log.info("=== (4/13) Loading assistive products ===");
        loadAssistiveProducts.run();
        loadAssistiveProducts.fixDuplicateNumbers();
        loadAssistiveProducts.fixCategorySpecificPropertyListValues();

        log.info("=== (5/13) Loading categories ===");
        loadCategories.run();

        log.info("=== (6/13) Loading assortments ===");
        loadAssortments.run();

        log.info("=== (7/13) Loading general pricelists ===");
        loadGeneralPricelist.run();

        log.info("=== (8/13) Loading agreements ===");
        loadAgreements.run();

        log.info("=== (9/13) Loading actors ===");
        loadActors.run();

        log.info("=== (10/13) Loading pricelist approvers ===");
        loadPricelistApprovers.run();

        log.info("=== (11/13) Loading assortment owners ===");
        loadAssortmentOwners.run();
        
        log.info("=== (12/13) Loading media ===");
        prepareModel.startImageMigrationPerSupplierPrepareForModel();

        log.info("=== (13/13) Loading export settings ===");
        loadExportSettings.run();
    }

}

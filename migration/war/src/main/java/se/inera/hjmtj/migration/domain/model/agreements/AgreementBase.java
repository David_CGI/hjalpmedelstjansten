/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.agreements;


import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.MappedSuperclass;
import javax.persistence.PersistenceUnit;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

import static javax.persistence.InheritanceType.TABLE_PER_CLASS;

@PersistenceUnit(unitName = "local")
@Inheritance(strategy = TABLE_PER_CLASS)
@MappedSuperclass
@Slf4j
@XmlTransient
@RequiredArgsConstructor
@Getter
@Setter
public abstract class AgreementBase<P extends AgreementPartyBase, R extends AgreementRowBase, W extends AgreementWarrantyBase> {

    @Column(nullable = false, unique = true)
    @XmlElement(name = "Identifier")
    private String identifier;

    @Id
    @XmlAttribute(name = "id")
    private Long agreementId;

    @XmlElement(name = "CategoryPath")
    private String categoryPath;

    @XmlElement(name = "Head")
    private AgreementHead head;

    @Embedded
    @XmlElement(name = "_x0031_5447a823cd64d8c91ef8f2552219e2b")
    private AgreementExtensionDate extension;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "begins", column = @Column(name = "valid_begins")),
            @AttributeOverride(name = "ends", column = @Column(name = "valid_ends")),
    })
    @XmlElement(name = "_x0034_e7e56aebebb40df80ed49c8e9a58b88")
    private AgreementValidDate valid;

    @XmlElement(name = "UpdatedTime")
    private LocalDateTime updated;

    @XmlElement(name = "CreatedTime")
    private LocalDateTime created;

    @XmlElement(name = "AgreementExpirationReminderDate")
    private LocalDateTime expirationReminderOn;

    @XmlElement(name = "LeadTimeDays")
    private Long leadTimeDays;

    public abstract P getSeller();

    public abstract void setSeller(P product);

    public abstract Set<R> getRows();

    public abstract void setRows(Set<R> rows);

    public abstract W getWarranty();

    public abstract void setWarranty(W warranty);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AgreementBase<?, ?, ?> that = (AgreementBase<?, ?, ?>) o;
        return agreementId.equals(that.agreementId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(agreementId);
    }
}

package se.inera.hjmtj.migration.domain.model.actors;

import lombok.Value;
import se.inera.hjalpmedelstjansten.model.api.RoleAPI;

@Value
public class Role {

    private final String name;

    public static Role of(RoleAPI roleAPI) {
        return new Role(roleAPI.getName());
    }

    @Override
    public String toString() {
        return name;
    }
}
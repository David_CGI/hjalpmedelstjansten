/*
 *
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 *  Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.application.bootstrap;

import lombok.extern.slf4j.Slf4j;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjmtj.migration.domain.media.SaveParameters;
import se.inera.hjmtj.migration.domain.media.SaveParametersBuilder;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.domain.model.image.entity.ImageMigration;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.domain.model.product.entities.Product;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductDocument;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductImage;
import se.inera.hjmtj.migration.infra.BulkService;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;
import se.inera.hjmtj.migration.infra.io.BatchPersistence;
import se.inera.hjmtj.migration.infra.organizations.SupplierRepository;
import se.inera.hjmtj.migration.infra.products.ProductOrganizationRepository;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.function.Function.identity;

@Singleton
@Slf4j
@TransactionTimeout(unit = TimeUnit.MINUTES, value = 60000)
public class LoadMedia {

    @EJB(lookup = "java:global/hjmtj/BulkService")
    private BulkService bulkService;

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    @PersistenceContext(unitName = "local")
    private EntityManager em;

    @Inject
    private SupplierRepository supplierRepository;

    @Inject
    private ProductOrganizationRepository productOrgRepos;

    private List<ImageMigration> listOfImages;

    @Inject
    private BatchPersistence batchPersistence;

    private AtomicInteger count = new AtomicInteger(0);


    public LoadMedia() {
    }

    public static Predicate<Product> mainImage() {
        return product -> product.getMainImage() != null;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    private long size() {
        return em.createNamedQuery(ImageMigration.COUNT, Long.class)
                 .setMaxResults(1)
                 .getSingleResult();
    }

    public void startImageMigrationPerSupplierPrepareForModel() {
        if (isEmpty()) {
            em.createQuery("DELETE FROM ImageMigration ")
              .executeUpdate();

            listOfImages = new ArrayList<>();
            count.set(0);
            // pick all
            for (Supplier supplier : supplierRepository.suppliers()) {
                try {
                    supplier = em.find(Supplier.class, supplier.getId());
                    saveImagePerSupplier(supplier);
                    batchPersistence.save(listOfImages, em);
                    listOfImages.clear();
                } catch (Exception e) {
                    MigrationError migrationError = new MigrationError();
                    migrationError.setClassifier(Classifier.HJALPMEDEL_IMAGE);
                    migrationError.setMessage(e.getMessage());
                    migrationError.setOrganizationName(supplier.getName());
                    migrationErrorRepository.add(migrationError);
                }

            }
            log.info("total number of images saved in table " + count.get());
        }
    }

    private void saveImageMigration(SaveParameters saveParameters) {
        List<String> externalKey = buildExternalKey(saveParameters.getProduct());
        try {
            count.incrementAndGet();
            String fileName = saveParameters.getMainImage()
                                            .fullFileName();
            String filePath = generateFilePath(externalKey.get(0), externalKey.get(1), fileName);

            // Load data
            ImageMigration migrationImage = new ImageMigration();
            migrationImage.setAltText(saveParameters.getAltText() != null ? saveParameters.getAltText()
                                                                                          .substring(0, Math.min(255, saveParameters.getAltText()
                                                                                                                                    .length())) : null);
            migrationImage.setSupplier(saveParameters.getSupplier());
            migrationImage.setCatalogueUniqueNumber(Long.valueOf(saveParameters.getProduct()
                                                                               .getVgrProdNo()));
            migrationImage.setGln(saveParameters.getProduct()
                                                .getSupplier()
                                                .getGln());
            migrationImage.setFilePath(filePath);
            migrationImage.setFileName(saveParameters.getMainImage()
                                                     .fullFileName());
            migrationImage.setDescription(saveParameters.getDescription());
            migrationImage.setOriginalContentUrl(saveParameters.get_srcContentUrl());
            migrationImage.setUrl(saveParameters.getUrl());
            migrationImage.setDocumentImage(saveParameters.isDoc());
            migrationImage.setMainImage(saveParameters.isMainImage());
            migrationImage.setFileExtension(saveParameters.getMainImage()
                                                          .getFileExtension());
            migrationImage.setInherited(saveParameters.isInherited());
            migrationImage.setType(saveParameters.getProduct()
                                                 .getType());

            if (saveParameters.isDoc()) {
                migrationImage.setDocumentType(saveParameters.getDocType());
            }

            listOfImages.add(migrationImage);
        } catch (Exception e) {
            log.error("exception thrown " + e.getMessage());
            ErrorMessageAPI errorMessageAPI = new ErrorMessageAPI();
            errorMessageAPI.setField("bildOriginalContentUrl");
            errorMessageAPI.setMessage("BildOriginalContentUrl '" + saveParameters.getProduct()
                                                                                  .getMainImage()
                                                                                  .getOriginalContentUrl() + "' cannot be loaded");
            createMigrationError(Long.valueOf(saveParameters.getProduct()
                                                            .getVgrProdNo()));
        }
    }

    public void saveImagePerSupplier(Supplier value) {
        AtomicInteger countOfImage = new AtomicInteger(0);
        Map<Long, Product> products = value.getProductOrganization() != null ? value.getProductOrganization()
                                                                                    .getProducts()
                                                                                    .stream()
                                                                                    .collect(Collectors.toMap(Product::getId, identity())) : Collections.emptyMap();
        for (Product assistiveProduct : products.values()) {
            Product templateProduct = assistiveProduct.getBasedOnTemplateProduct() != null ? products.get(assistiveProduct.getBasedOnTemplateProduct()
                                                                                                                          .getIdRef()) : null;


            if (assistiveProduct.getMainImage() != null) {
                if (assistiveProduct.getXmlCategories()
                                    .isIso9999()) {
                    try {
                        SaveParameters saveParameters = new SaveParametersBuilder().setSupplier(value)
                                                                                   .setProduct(assistiveProduct)
                                                                                   .setUrl(assistiveProduct.getMainImage()
                                                                                                           .getOriginalContentUrl())
                                                                                   .setDescription("")
                                                                                   .setAltText(null)
                                                                                   .set_srcContentUrl(assistiveProduct.getMainImage()
                                                                                                                      .getSourceURL())
                                                                                   .setIsDoc(false)
                                                                                   .setIsMainImage(true)
                                                                                   .setContentType(assistiveProduct.getMainImage()
                                                                                                                   .getContentType())
                                                                                   .setDocType("")
                                                                                   .setMainImage(assistiveProduct.getMainImage())
                                                                                   .setTemplateProduct(templateProduct)
                                                                                   .createSaveParameters();
                        saveImageMigration(saveParameters);
                        createAdditionalImageContent(value, assistiveProduct, templateProduct);
                        createAdditionalDocContent(value, assistiveProduct, templateProduct);
                    } catch (Exception e) {
                        if (Optional.ofNullable(assistiveProduct.getMainImage())
                                    .isPresent()) {
                            log.error("exception in image save per product with productnumber {} and main image url {}",
                                    assistiveProduct.getVgrProdNo(), assistiveProduct.getMainImage()
                                                                                     .getOriginalContentUrl());
                            log.error(e.getMessage());
                        } else {
                            log.error("exception in image save per product with productnumber {}", assistiveProduct.getVgrProdNo());
                        }
                    }
                }
            }
        }
    }

    private boolean createAdditionalImageContent(Supplier value, Product product, Product templateProduct) {
        if (Optional.ofNullable(product.getImages())
                    .isPresent()) {
            for (ProductImage prodImage : product.getImages()) {
                try {
                    if (Optional.ofNullable(prodImage.getFile())
                                .isPresent()) {
                        String url = prodImage.getFile()
                                              .getOriginalContentUrl();
                        SaveParameters saveParameters = new SaveParametersBuilder().setSupplier(value)
                                                                                   .setProduct(product)
                                                                                   .setUrl(url)
                                                                                   .setDescription(prodImage.getFileDescription())
                                                                                   .setAltText(prodImage.getAltText())
                                                                                   .set_srcContentUrl(prodImage.getFile()
                                                                                                               .getSourceURL())
                                                                                   .setIsDoc(false)
                                                                                   .setIsMainImage(false)
                                                                                   .setContentType(prodImage.getFile()
                                                                                                            .getContentType())
                                                                                   .setDocType("")
                                                                                   .setMainImage(prodImage.getFile())
                                                                                   .setTemplateProduct(templateProduct)
                                                                                   .createSaveParameters();
                        saveImageMigration(saveParameters);
                    }
                } catch (Exception e) {
                    log.error("Exception thrown in create additional image content {} ", e.getMessage());
                }
            }
        }
        return true;
    }

    private boolean createAdditionalDocContent(Supplier value, Product product, Product templateProduct) {
        if (Optional.ofNullable(product.getDocuments())
                    .isPresent()) {
            for (ProductDocument prodDoc : product.getDocuments()) {
                try {
                    if (Optional.ofNullable(prodDoc.getFile())
                                .isPresent()) {
                        String url = prodDoc.getFile()
                                            .getOriginalContentUrl();
                        String docType = "";
                        if (Optional.ofNullable(prodDoc.getDocumentType())
                                    .isPresent()) {
                            docType = prodDoc.getDocumentType()
                                             .getCode();
                        }
                        SaveParameters saveParameters = new SaveParametersBuilder().setSupplier(value)
                                                                                   .setProduct(product)
                                                                                   .setUrl(url)
                                                                                   .setDescription(prodDoc.getFileDescription())
                                                                                   .setAltText(null)
                                                                                   .set_srcContentUrl(prodDoc.getFile()
                                                                                                             .getSourceURL())
                                                                                   .setIsDoc(true)
                                                                                   .setIsMainImage(false)
                                                                                   .setContentType(prodDoc.getFile()
                                                                                                          .getContentType())
                                                                                   .setDocType(docType)
                                                                                   .setMainImage(prodDoc.getFile())
                                                                                   .setTemplateProduct(templateProduct)
                                                                                   .createSaveParameters();
                        saveImageMigration(saveParameters);
                    }
                } catch (Exception e) {
                    log.error("Exception thrown in create additional docment content {} ", e.getMessage());
                }
            }
        }
        return true;
    }

    private List<String> buildExternalKey(Product product) {
        String gln = product.getSupplier()
                            .getGln();
        String productNumberSupplier = product.getSupplierProductNumber();
        StringBuilder folderOrg;
        if (Optional.ofNullable(product.getSupplier()
                                       .getEconomicInformation())
                    .isPresent()) {
            String orgNr = product.getSupplier()
                                  .getEconomicInformation()
                                  .getOrganizationNo();
            folderOrg = new StringBuilder(gln).append(orgNr);
        } else {
            String id = product.getSupplier()
                               .getIdentifier();
            folderOrg = new StringBuilder(gln).append(id);
        }
        String uuidFolder = UUID.nameUUIDFromBytes(folderOrg.toString()
                                                            .getBytes())
                                .toString();
        StringBuilder folderProd = new StringBuilder(folderOrg).append(productNumberSupplier);
        String artFolder = UUID.nameUUIDFromBytes(folderProd.toString()
                                                            .getBytes())
                               .toString();
        List<String> valueList = new ArrayList<>();
        valueList.add(uuidFolder);
        valueList.add(artFolder);
        return valueList;
    }

    private void createMigrationError(Long articleUniqueId) {
        MigrationError migrationError = new MigrationError();
        migrationError.setClassifier(Classifier.HJALPMEDEL_IMAGE);
        migrationError.setCatalogueUniqueNumber(articleUniqueId);
        migrationErrorRepository.add(migrationError);
    }

    private String generateFilePath(String organization, String product, String filename) {
        return organization + "/" + product + "/" + filename;
    }

}

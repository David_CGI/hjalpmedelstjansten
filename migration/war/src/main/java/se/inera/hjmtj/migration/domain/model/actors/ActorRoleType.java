package se.inera.hjmtj.migration.domain.model.actors;

import se.inera.hjalpmedelstjansten.model.entity.UserRole;

import javax.xml.bind.annotation.XmlEnum;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static se.inera.hjalpmedelstjansten.model.entity.UserRole.RoleName.*;

@XmlEnum
public enum ActorRoleType {

    RoleAppointer_withappointment_seller("Rolltillsättare säljande part"),

    AssortmentOwnerSpecific("Utbudsägare (vissa utbud)"),

    AssortmentOwner("Utbudsägare (samtliga utbud)"),

    ProductOwner("Produktägare (Leverantör)"),

    AgreementOwnerSpecific_Seller("Avtalsägare säljande part (vissa avtal)"),

    RoleAppointer_withappointment_buyer("Rolltillsättare köpande part"),

    ProductCategoryOwner("Produktkategoriägare"),

    agreementsearch_seller("Produkt- och avtalssökning säljande part"),

    AgreementOwnerSpecific("Avtalsägare köpande part (vissa avtal)"),

    AgreementOwnerForOrganization("Avtalsägare säljande part (samtliga avtal)"),

    productsearch_role("Produktsökning"),

    aid_consultant("Produkt- och avtalssökning köpande part"),

    AgreementOwner_OrganizationNode_Buyer("Avtalsägare köpande part (samtliga avtal)"),

    FunctionOwner("Funktionsägare"),

    ProductAndAgreementFileOwner("Produkt- och avtalsradsfilsägare");


    private static Map<String, List<Role>> TRANSLATION = new HashMap<>();

    static {
        // https://riv-ta.atlassian.net/wiki/spaces/HJAL/pages/900433380/Anv+ndare+akt+rer
        TRANSLATION.put("Rolltillsättare säljande part", Arrays.asList(new Role(Supplieradmin.toString())));
        TRANSLATION.put("Utbudsägare (vissa utbud)", Arrays.asList(new Role(CustomerAssignedAssortmentManager.toString()), new Role(CustomerAgreementViewer.toString())));
        TRANSLATION.put("Utbudsägare (samtliga utbud)", Arrays.asList(new Role(CustomerAssortmentManager.toString()), new Role(CustomerAgreementViewer.toString())));
        TRANSLATION.put("Produktägare (Leverantör)", Arrays.asList(new Role(SupplierProductAndArticleHandler.toString()), new Role(SupplierAgreementViewer.toString())));
        TRANSLATION.put("Avtalsägare säljande part (vissa avtal)", Arrays.asList(new Role(SupplierAgreementManager.toString())));
        TRANSLATION.put("Rolltillsättare köpande part", Arrays.asList(new Role(Customeradmin.toString())));
        TRANSLATION.put("Produktkategoriägare", Collections.emptyList());
        TRANSLATION.put("Produkt- och avtalssökning säljande part", Arrays.asList(new Role(SupplierAgreementViewer.toString())));
        TRANSLATION.put("Avtalsägare köpande part (vissa avtal)", Arrays.asList(new Role(CustomerAgreementViewer.toString()))); // Users with this role should be present in "approves pricelist" for agreements
        TRANSLATION.put("Avtalsägare säljande part (samtliga avtal)", Arrays.asList(new Role(SupplierAgreementManager.toString())));
        TRANSLATION.put("Produktsökning", Collections.emptyList());
        TRANSLATION.put("Produkt- och avtalssökning köpande part", Arrays.asList(new Role(CustomerAgreementViewer.toString())));
        TRANSLATION.put("Avtalsägare köpande part (samtliga avtal)", Arrays.asList(new Role(CustomerAgreementManager.toString()),  new Role(CustomerAgreementViewer.toString())));
        TRANSLATION.put("Funktionsägare", Collections.emptyList());
        TRANSLATION.put("Produkt- och avtalsradsfilsägare", Collections.emptyList());
    }

    private final String value;

    ActorRoleType(String value) {
        this.value = value;
    }

    public static ActorRoleType fromValue(String value) {
        return Arrays.stream(values())
                     .filter(t -> t.value.equals(value))
                     .findFirst()
                     .orElseThrow(() -> new IllegalArgumentException(value));
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }

    public Set<Role> toRoleDescriptionName() {
        if (!TRANSLATION.containsKey(value)) {
            throw new IllegalStateException("No translation defined for " + value);
        }

        return new HashSet<>(TRANSLATION.get(value));
    }

}

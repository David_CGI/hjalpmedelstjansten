/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.assortment;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import se.inera.hjmtj.migration.domain.model.product.entities.Product;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductCategoryPath;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQuery;
import javax.persistence.PersistenceUnit;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;

@PersistenceUnit(unitName = "local")
@Entity
@XmlRootElement(name = "Product")
@RequiredArgsConstructor
@Getter
@Setter
@NamedNativeQuery(name = AssortmentProduct.COUNT_ALL, query = "SELECT COUNT(*) FROM ASSORTMENTPRODUCT\n" +
        "JOIN ASSORTMENT_ASSORTMENTPRODUCT AA on ASSORTMENTPRODUCT.VGRPRODNO = AA.PRODUCTS_VGRPRODNO\n" +
        "JOIN PRODUCT P on ASSORTMENTPRODUCT.ARTICLE_VGRPRODNO = P.VGRPRODNO\n" +
        "WHERE P.ISO9999 = TRUE AND\n" +
        "      P.NOARTICLETYPE = FALSE")
public class AssortmentProduct {

    public static final String COUNT_ALL = "AssortmentProduct.COUNT_ALL";

    private Long id;

    @Embedded
    @XmlElement(name = "CategoryPath")
    private ProductCategoryPath xmlCategories;

    @Id
    @XmlElement(name = "VGRProdNo")
    private String vgrProdNo;

    @ManyToOne(fetch = FetchType.EAGER, optional = true)
    private Product article;

    @XmlAttribute(name = "id")
    @XmlID
    public String getIdAsString() {
        return String.valueOf(id);
    }

    public void setIdAsString(String xmlId) {
        id = Long.parseLong(xmlId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AssortmentProduct that = (AssortmentProduct) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}

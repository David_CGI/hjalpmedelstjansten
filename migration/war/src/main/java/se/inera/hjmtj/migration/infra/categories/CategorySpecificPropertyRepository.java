/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.categories;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.domain.model.categories.Category;
import se.inera.hjmtj.migration.domain.model.categories.CategorySpecificProperty;
import se.inera.hjmtj.migration.domain.model.categories.CategorySpecificPropertyListValue;
import se.inera.hjmtj.migration.infra.io.CsvPersistence;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static javax.ejb.TransactionAttributeType.REQUIRES_NEW;

/**
 * Populates CategorySpecificProperty using csv.
 */
@Slf4j
@Stateless
public class CategorySpecificPropertyRepository {

    private static final String COLUMN_NAMES = "CODE;FIELD_NAME;DESCRIPTION;TYPE;LIST";

    private static final String fileName = "/categoryproperties.csv";

    @PersistenceContext(unitName = "local")
    private EntityManager em;

    @Inject
    private CategoryRepository categoryRepository;

    @TransactionAttribute(REQUIRES_NEW)
    public void loadFromCsv() {
        Set<CategorySpecificProperty> categorySpecificProperties = CsvPersistence.csvRead(em, fileName, COLUMN_NAMES, true)
                                                                                 .stream()
                                                                                 .map(this::categorySpecificProperty)
                                                                                 .collect(Collectors.toSet());
        log.info("Found {} CategorySpecificProperty in CSV (with duplicates removed)", categorySpecificProperties.size());

        for (CategorySpecificProperty categorySpecificProperty : categorySpecificProperties) {
            em.persist(categorySpecificProperty);
        }
    }

    private CategorySpecificProperty categorySpecificProperty(Object[] o) {
        CategorySpecificProperty categorySpecificProperty = new CategorySpecificProperty();
        String name = (String) o[1];
        categorySpecificProperty.setName(name.trim());

        String description = (String) o[2];
        categorySpecificProperty.setDescription(description == null ? null : description.trim());

        CategorySpecificProperty.Type type = CategorySpecificProperty.Type.valueOf((String) o[3]);
        categorySpecificProperty.setType(type);

        String code = (String) o[0];
        Category category = categoryRepository.findByCode(code);
        Objects.requireNonNull(category, "category must be non-null");
        categorySpecificProperty.setCategory(category);

        LinkedList<CategorySpecificPropertyListValue> categorySpecificPropertyListValues = new LinkedList<>();
        categorySpecificProperty.setCategorySpecificPropertyListValues(categorySpecificPropertyListValues);
        String possibleValues = (String) o[4];

        if (possibleValues != null) {
            String[] array = possibleValues.split(":");

            for (String value : array) {
                CategorySpecificPropertyListValue specificPropertyListValue = new CategorySpecificPropertyListValue();
                specificPropertyListValue.setCategorySpecificProperty(categorySpecificProperty);
                specificPropertyListValue.setCode(value.trim());
                specificPropertyListValue.setValue(value.trim());

                categorySpecificPropertyListValues.add(specificPropertyListValue);
            }
        }

        return categorySpecificProperty;
    }

    public List<CategorySpecificProperty> values() {
        return em.createQuery("SELECT DISTINCT csp FROM CategorySpecificProperty csp " +
                "LEFT JOIN FETCH csp.category " +
                "LEFT JOIN FETCH csp.categorySpecificPropertyListValues " +
                "order by csp.uniqueId", CategorySpecificProperty.class)
                 .getResultList();
    }

    public List<CategorySpecificPropertyListValue> listValues() {
        return em.createQuery("SELECT DISTINCT csplv FROM CategorySpecificPropertyListValue csplv  " +
                "LEFT JOIN FETCH csplv.categorySpecificProperty " +
                "order by csplv.uniqueId", CategorySpecificPropertyListValue.class)
                 .getResultList();
    }

}

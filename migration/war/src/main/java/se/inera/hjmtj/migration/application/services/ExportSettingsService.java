/*
 *
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 *  Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.application.services;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.domain.model.agreements.gp.ExportSetting;
import se.inera.hjmtj.migration.domain.model.organization.entities.Customer;
import se.inera.hjmtj.migration.infra.agreements.ExportSettingsRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjGeneralPricelistRepository;
import se.inera.hjmtj.migration.infra.organizations.CustomerRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import java.util.HashSet;
import java.util.Set;

@Slf4j
@ApplicationScoped
@Path("/exportSettings")
public class ExportSettingsService {

    @Inject
    private CustomerRepository customerRepository;

    @Inject
    private HjmtjGeneralPricelistRepository hjmtjGeneralPricelistRepository;

    @Inject
    private ExportSettingsRepository exportSettingsRepository;

    @GET
    @Path("/")
    public void migrateExportSettings() {
        for (Customer customer : customerRepository.customersAndExportSettings()) {
            try {
                Set<ExportSetting> exportSettings;

                if (customer.isBusinessLevel()) {
                    // Include GPs only for the business level itself
                    exportSettings = customer.getExportSettings();
                } else {
                    // Include GPs for top level and all business levels on the organization
                    exportSettings = new HashSet<>(exportSettingsRepository.findByCustomerGLN(customer.getGln()));
                }

                hjmtjGeneralPricelistRepository.saveExportSettings(customer, exportSettings);
            } catch (Exception e) {
                log.error("Failed to save export settings for customer {}, reason: {}", customer.getOrganizationName(), e.getMessage());
            }
        }
    }
}

/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.hjmtj;

import lombok.Value;
import lombok.experimental.Accessors;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;
import java.util.HashSet;
import java.util.Set;

@Value
@Accessors(fluent = true)
public class SaveResult<R> {

    private static SaveResult EMPTY = new SaveResult<>(null, null);

    private final R data;

    private final Set<ErrorMessageAPI> errorMessages;

    public static <R, T> SaveResult<R> of(R data, T api) {
        Validator validator = Validation.buildDefaultValidatorFactory()
                                        .getValidator();
        Set<ConstraintViolation<T>> constraintViolations = validator.validate(api, Default.class);
        Set<ErrorMessageAPI> errorMessages = new HashSet<>();
        if (constraintViolations != null && !constraintViolations.isEmpty()) {
            for (ConstraintViolation constraintViolation : constraintViolations) {
                errorMessages.add(HjalpmedelstjanstenValidationException.generateValidationMessage(constraintViolation.getPropertyPath()
                                                                                                                      .toString(), constraintViolation.getMessage()));
            }
        }

        return new SaveResult<>(data, errorMessages);
    }

    public static <R> SaveResult<R> empty() {
        return EMPTY;
    }

    public boolean isEmpty() {
        return this == EMPTY;
    }

}

/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.agreements;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import se.inera.hjmtj.migration.domain.model.OrganizationBase;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.MappedSuperclass;
import javax.persistence.PersistenceUnit;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;

import static javax.persistence.InheritanceType.TABLE_PER_CLASS;

@PersistenceUnit(unitName = "local")
@Inheritance(strategy = TABLE_PER_CLASS)
@MappedSuperclass
@XmlTransient
@RequiredArgsConstructor
@Getter
@Setter
public abstract class AgreementPartyBase<O extends OrganizationBase> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long generatedId;

    public abstract O getAgreementOrganization();

    public abstract void setAgreementOrganization(O organization);

}
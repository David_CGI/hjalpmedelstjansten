/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.product.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.PersistenceUnit;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Note: This class must be deserialized using jackson because it matches multiple elements.
 */
@PersistenceUnit(unitName = "local")
@Entity
@Table(indexes = {
        @Index(name = "CodeIndex", columnList = "code"),
        @Index(name = "ValueIndex", columnList = "value")
})
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlTransient
@RequiredArgsConstructor
@Getter
@Setter
public class ProductCategorySpecificPropertyListValue {

    @Id
    @JsonProperty("Id")
    private Long id;

    @JsonProperty("Code")
    private String code;

    @JsonProperty("Value")
    private String value;

}

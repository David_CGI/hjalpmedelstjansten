/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.csv;

import lombok.Value;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Builds a CSV that is displayed correctly in Excel >2007.
 */
@Value
public class Utf8Csv {

    private static byte[] BOM = new byte[]{(byte) 239, (byte) 187, (byte) 191};

    private final byte[] HEADER = "\nSEP=;\n".getBytes();

    private final byte[] data;

    public Utf8Csv(String data) {
        this.data = data.getBytes();
    }

    public byte[] toBytes() {
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            bos.write(BOM);
            //bos.write(HEADER);
            bos.write(data);

            return bos.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}

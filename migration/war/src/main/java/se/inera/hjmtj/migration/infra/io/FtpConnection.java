/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.io;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@Slf4j
public class FtpConnection {

    public static InputStream of(Parameters parameters) {
        log.info("Loading from {}...", parameters.getHost());

        if (parameters.getHost()
                      .equals("localhost") || parameters.getHost()
                                                        .equals("ftp")) {
            log.info("Using ftp to retrieve {}", parameters.getFile());
            return ftp(parameters.getHost(), parameters.getFile(), parameters.getZipEntry(), parameters.getUsername(), parameters.getPassword());
        } else {
            log.info("Using sftp to retrieve {}", parameters.getFile());
            return sftp(parameters.getHost(), parameters.getFile(), parameters.getZipEntry(), parameters.getUsername(), parameters.getPassword()
                                                                                                                                  .trim());
        }
    }

    private static InputStream ftp(String host, String file, String zipEntry, String username, String password) {
        String urlString;
        try {
            String user = URLEncoder.encode(username, "UTF-8");
            String pass = URLEncoder.encode(password, "UTF-8");

            if (zipEntry != null) {
                urlString = String.format("jar:ftp://%s:%s@%s/%s!/%s", user, pass, host, file, zipEntry);
            } else {
                urlString = String.format("ftp://%s:%s@%s/%s", user, pass, host, file);
            }

            log.info(urlString);
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException("URL encoding failed", e);
        }

        try {
            URL url = new URL(urlString);
            URLConnection urlConnection = url.openConnection();
            InputStream inputStream = urlConnection.getInputStream();
            log.info("FTP inputStream is " + inputStream.getClass());
            return inputStream;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static InputStream sftp(String host, String file, String zipEntry, String username, String password) {
        Session session;
        try {
            session = new JSch().getSession(username, host);
            Properties config = new Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.setPassword(password);
            session.connect();
        } catch (JSchException e) {
            throw new IllegalArgumentException(e);
        }

        ChannelSftp sftpChannel;
        try {
            sftpChannel = (ChannelSftp) session.openChannel("sftp");
            sftpChannel.connect();
        } catch (JSchException e) {
            throw new RuntimeException(e);
        }

        InputStream is;
        try {
            is = sftpChannel.get(file);
        } catch (SftpException e) {
            throw new RuntimeException(file, e);
        }

        if (zipEntry == null) {
            return is;
        }

        return unzip(file, zipEntry, is);
    }

    private static InputStream unzip(String file, String zipEntry, InputStream is) {
        try {
            ZipInputStream zis = new ZipInputStream(is);

            ZipEntry entry;
            while ((entry = zis.getNextEntry()) != null) {
                String entryName = entry.getName();
                if (entryName.equals(zipEntry)) {
                    return zis;
                }
            }

            throw new RuntimeException("Entry " + zipEntry + " not found in " + file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static class Parameters {

        private final String host;

        private final String file;

        private final String zipEntry;

        private final String username;

        private final String password;

        public Parameters(String host, String file, String zipEntry, String username, String password) {
            this.host = host;
            this.file = file;
            this.zipEntry = zipEntry;
            this.username = username;
            this.password = password;
        }

        public String getHost() {
            return host;
        }

        String getFile() {
            return file;
        }

        String getZipEntry() {
            return zipEntry;
        }

        String getUsername() {
            return username;
        }

        String getPassword() {
            return password;
        }
    }
}

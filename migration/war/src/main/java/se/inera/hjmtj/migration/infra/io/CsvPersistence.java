/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.io;

import lombok.extern.slf4j.Slf4j;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * Created by jahwag on 2018-06-04.
 */
@Slf4j
public final class CsvPersistence {

    private static final String BASE_PATH = "/tmp";

    private CsvPersistence() {
    }

    public static void csvRead(EntityManager em, String fileName, String tableName, String columnNames) {
        String path = writeTmpCsv(fileName, false);

        Query namedQuery = em.createNativeQuery("INSERT INTO " + tableName + " "
                + "(" + columnNames.replace(";", ",") + ") "
                + "SELECT * FROM CSVREAD('" + path + "', "
                + "'" + columnNames + "', "
                + "'charset=UTF-8 fieldSeparator=;')");
        namedQuery.executeUpdate();
    }

    private static String writeTmpCsv(String fileName, boolean removeHeader) {
        InputStream is = CsvPersistence.class.getResourceAsStream(fileName);
        Objects.requireNonNull(is, "CSV not found");
        Path tmpFile = Paths.get(BASE_PATH + "/" + UUID.randomUUID()
                                                       .toString()
                                                       .substring(0, 8) + ".csv");

        StringBuilder content = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
            while (br.ready()) {
                String line = br.readLine();

                if (removeHeader) {
                    removeHeader = false;
                } else {
                    content.append(line);
                    content.append("\n");
                }
            }
            byte[] bytes = content.toString()
                                  .getBytes();
            Files.write(tmpFile, bytes);

        } catch (IOException ex) {
            throw new RuntimeException("Import CSV failed", ex);
        }

        return tmpFile.toString();
    }

    public static List<Object[]> csvRead(EntityManager em, String fileName, String columnNames, boolean removeHeader) {
        String path = writeTmpCsv(fileName, removeHeader);
        String options = "charset=UTF-8 fieldSeparator=;";

        return em.createNativeQuery("SELECT * FROM CSVREAD('" + path + "', '" + columnNames + "', '" + options + "')")
                 .getResultList();
    }

}

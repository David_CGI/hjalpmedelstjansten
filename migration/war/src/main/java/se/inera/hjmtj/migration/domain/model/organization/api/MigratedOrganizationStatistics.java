/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.organization.api;

import lombok.Value;
import lombok.experimental.Accessors;
import se.inera.hjalpmedelstjansten.model.entity.Article;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

@Value
@Accessors(fluent = true)
public class MigratedOrganizationStatistics {

    private final String organizationName;

    private final long total;

    private final Map<Article.Type, Long> productMap = new HashMap<>();

    private final Map<Article.Type, Long> articleMap = new HashMap<>();

    private final long agreements = 0;

    private final long priceLists = 0;

    private final long agreementRows = 0;

    public MigratedOrganizationStatistics(Object[] array) {
        organizationName = (String) array[0];
        total = ((BigInteger) array[1]).longValue();
        productMap.put(Article.Type.H, ((BigInteger) array[2]).longValue());
        productMap.put(Article.Type.T, ((BigInteger) array[3]).longValue());

        articleMap.put(Article.Type.H, ((BigInteger) array[4]).longValue());
        articleMap.put(Article.Type.T, ((BigInteger) array[5]).longValue());
        articleMap.put(Article.Type.R, ((BigInteger) array[6]).longValue());
        articleMap.put(Article.Type.Tj, ((BigInteger) array[7]).longValue());
        articleMap.put(Article.Type.I, ((BigInteger) array[8]).longValue());

//        agreements = ((BigInteger) array[9]).longValue();
//        priceLists = ((BigInteger) array[10]).longValue();
//        agreementRows = ((BigInteger) array[11]).longValue();
    }
}
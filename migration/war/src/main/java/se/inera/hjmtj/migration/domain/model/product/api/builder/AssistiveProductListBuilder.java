/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.product.api.builder;

import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedAcyclicGraph;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.CategoryAPI;
import se.inera.hjalpmedelstjansten.model.api.CategorySpecificPropertyAPI;
import se.inera.hjalpmedelstjansten.model.api.CategorySpecificPropertyListValueAPI;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjalpmedelstjansten.model.api.ResourceSpecificPropertyAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCEDirectiveAPI;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjmtj.migration.domain.exceptions.save.ArticleCannotBeBasedOnArticle;
import se.inera.hjmtj.migration.domain.exceptions.save.ArticleHIsNotBasedOnAnyProduct;
import se.inera.hjmtj.migration.domain.exceptions.save.ArticleHasNoType;
import se.inera.hjmtj.migration.domain.exceptions.save.CategorySpecificPropertyListValueNotFound;
import se.inera.hjmtj.migration.domain.exceptions.save.CategorySpecificPropertyTypeNotFound;
import se.inera.hjmtj.migration.domain.exceptions.save.NotAnISO9999Article;
import se.inera.hjmtj.migration.domain.exceptions.save.NotAnISO9999Product;
import se.inera.hjmtj.migration.domain.exceptions.save.UnsupportedFitException;
import se.inera.hjmtj.migration.domain.model.BlindReference;
import se.inera.hjmtj.migration.domain.model.agreements.builders.Unique;
import se.inera.hjmtj.migration.domain.model.categories.CategorySpecificProperty;
import se.inera.hjmtj.migration.domain.model.categories.CategorySpecificPropertyListValue;
import se.inera.hjmtj.migration.domain.model.categories.ProductCategorySpecificPropertyType;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MessageTemplate;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.domain.model.product.ArticleAPINode;
import se.inera.hjmtj.migration.domain.model.product.AssistiveProduct;
import se.inera.hjmtj.migration.domain.model.product.ProductAPINode;
import se.inera.hjmtj.migration.domain.model.product.entities.Product;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductCategory;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductCategoryPath;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductCategoryProduct;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductCategorySpecificProperty;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductCategorySpecificPropertyListValue;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductClassification;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductIntervalDecimal;
import se.inera.hjmtj.migration.domain.model.result.AssistiveProductReport;
import se.inera.hjmtj.migration.infra.categories.ProductCategorySpecificPropertyTypeRepository;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjCategoryRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjCvCeDirectiveRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjCvCeStandardRepository;
import se.inera.hjmtj.migration.infra.products.ProductRepository;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static se.inera.hjmtj.migration.domain.DateTimes.toEpochMillis;

/**
 * Constructs an inventory of products and articles. Handles cross-cutting concerns such as filtering and recovery/
 * repair of bad data so that the returned dataset is to be considered fully valid.
 */
@Slf4j
@Value
public class AssistiveProductListBuilder {

    public static final String HJMTJ_ZONE_ID = "Europe/Stockholm";

    public static final String CE_DIRECTIVE_MEDICAL = "Medicintekniska direktivet 93/42/EEG";

    public static final String CE_DIRECTIVE_OTHER_APPLICABLE = "Annat tilllämpligt direktiv";

    private final InventoryBuilderHelper inventoryBuilderHelper;

    private final ProductRepository productRepository;

    private final Map<Long, Product> idToProduct;

    private final HjmtjCategoryRepository hjmtjCategoryRepository;

    private final ProductCategorySpecificPropertyTypeRepository productCategorySpecificPropertyTypeRepository;

    private final MigrationErrorRepository migrationErrorRepository;

    private final OrganizationAPI supplier;

    private final DirectedAcyclicGraph<AssistiveProduct, DefaultEdge> graph;

    private final Validator validator;

    private HjmtjCvCeDirectiveRepository hjmtjCvCeDirectiveRepository;

    private HjmtjCvCeStandardRepository hjmtjCvCeStandardRepository;

    public AssistiveProductListBuilder(Context context) {
        this.graph = context.getGraph();
        this.inventoryBuilderHelper = context.getInventoryBuilderHelper();
        this.productRepository = context.getProductRepository();
        this.idToProduct = context.getIdToProduct();
        this.hjmtjCategoryRepository = context.getHjmtjCategoryRepository();
        this.productCategorySpecificPropertyTypeRepository = context.getProductCategorySpecificPropertyTypeRepository();
        this.migrationErrorRepository = context.getMigrationErrorRepository();
        this.supplier = context.getSupplier();
        this.validator = context.getValidator();
        this.hjmtjCvCeDirectiveRepository = context.getHjmtjCvCeDirectiveRepository();
        this.hjmtjCvCeStandardRepository = context.getHjmtjCvCeStandardRepository();
    }

    public List<AssistiveProduct> build() {
        Map<Long, ProductWithAssistiveProduct> aidMap = new HashMap<>();

        for (Product product : idToProduct.values()) {
            try {
                ProductWithAssistiveProduct productWithAssistiveProduct = map(product);
                Product mappedProduct = productWithAssistiveProduct.product();
                aidMap.put(mappedProduct.getId(), productWithAssistiveProduct);
            } catch (NotAnISO9999Article | NotAnISO9999Product e) {
                MigrationError migrationError = new MigrationError();
                migrationError.setSeverity(MigrationError.Severity.INFO);
                migrationError.setClassifier(Classifier.HJALPMEDEL_CATEGORY);
                migrationError.setOrganizationName(supplier.getOrganizationName());
                migrationError.setCatalogueUniqueNumber(Long.valueOf(product.getVgrProdNo()));
                migrationError.setMessage(MessageTemplate.NOT_ISO9999);
                migrationErrorRepository.add(migrationError);
            } catch (ArticleHasNoType e) {
                MigrationError migrationError = new MigrationError();
                migrationError.setSeverity(MigrationError.Severity.WARNING);
                migrationError.setClassifier(Classifier.HJALPMEDEL_CATEGORY);
                migrationError.setOrganizationName(supplier.getOrganizationName());
                migrationError.setCatalogueUniqueNumber(Long.valueOf(product.getVgrProdNo()));
                migrationError.setMessage(MessageTemplate.ARTICLE_WITHOUT_TYPE);
                migrationErrorRepository.add(migrationError);
            } catch (Exception ex) {
                log.error("{}", ex);
            }
        }

        List<AssistiveProduct> returnValue = new LinkedList<>();

        for (ProductWithAssistiveProduct aid : aidMap.values()) {
            try {
                ProductWithAssistiveProduct connected = connect(aidMap, aid);

                if (connected != null) {
                    returnValue.add(connected.assistiveProduct());
                }
            } catch (ArticleHIsNotBasedOnAnyProduct e) {
                MigrationError migrationError = new MigrationError();
                migrationError.setSeverity(MigrationError.Severity.WARNING);
                migrationError.setClassifier(Classifier.HJALPMEDEL_FIT);
                migrationError.setOrganizationName(supplier.getOrganizationName());
                migrationError.setCatalogueUniqueNumber(aid.assistiveProduct()
                                                           .getCatalogueUniqueNumber());
                migrationError.setMessage(MessageTemplate.ARTICLE_H_MUST_BE_BASED_ON_A_PRODUCT, e.getArticle()
                                                                                                 .getCatalogueUniqueNumber());
                migrationErrorRepository.add(migrationError);
            } catch (ArticleCannotBeBasedOnArticle e) {
                MigrationError migrationError = new MigrationError();
                migrationError.setSeverity(MigrationError.Severity.WARNING);
                migrationError.setClassifier(Classifier.HJALPMEDEL_FIT);
                migrationError.setOrganizationName(supplier.getOrganizationName());
                migrationError.setCatalogueUniqueNumber(aid.assistiveProduct()
                                                           .getCatalogueUniqueNumber());
                migrationError.setMessage(MessageTemplate.ARTICLE_CANNOT_BE_BASED_ON_ARTICLE, e.getArticle()
                                                                                               .getCatalogueUniqueNumber(), e.getBasedOn()
                                                                                                                             .getCatalogueUniqueNumber());
                migrationErrorRepository.add(migrationError);
            }
        }

        return returnValue;
    }

    private ProductWithAssistiveProduct map(Product product) throws NotAnISO9999Article, ArticleHasNoType, NotAnISO9999Product {
        AssistiveProductReport.Type type = product.getType();

        switch (type) {
            case PRODUCT:
                return createProduct(product);
            case ARTICLE:
                return createArticle(product);
        }

        return null;
    }

    private ProductWithAssistiveProduct connect(Map<Long, ProductWithAssistiveProduct> aidMap, ProductWithAssistiveProduct productWithAssistiveProduct) throws ArticleHIsNotBasedOnAnyProduct, ArticleCannotBeBasedOnArticle {
        AssistiveProduct assistiveProduct = productWithAssistiveProduct.assistiveProduct();
        Product xmlProduct = productWithAssistiveProduct.product();
        BlindReference basedOnTemplateProduct = xmlProduct.getBasedOnTemplateProduct();

        if (assistiveProduct instanceof ArticleAPINode) {
            addFits(aidMap, productWithAssistiveProduct);
        }

        if (basedOnTemplateProduct != null) {
            ProductWithAssistiveProduct basedOnProduct = aidMap.get(basedOnTemplateProduct.getIdRef());

            if (basedOnProduct == null) {
                throw new ArticleHIsNotBasedOnAnyProduct(assistiveProduct);
            } else if (basedOnProduct.assistiveProduct() instanceof ArticleAPINode) {
                throw new ArticleCannotBeBasedOnArticle(assistiveProduct, basedOnProduct);
            } else if (assistiveProduct instanceof ArticleAPINode) {
                ProductAPI product = (ProductAPI) basedOnProduct.assistiveProduct();
                ArticleAPINode article = (ArticleAPINode) assistiveProduct;

                CategoryAPI articleCategory = article.getCategory();
                Article.Type articleCategoryType = articleCategory.getArticleType();
                List<ProductAPI> fitsToProducts = article.getFitsToProducts() != null ? article.getFitsToProducts() : Collections.emptyList();
                List<ArticleAPI> fitsToArticles = article.getFitsToArticles() != null ? article.getFitsToArticles() : Collections.emptyList();

                if (articleCategoryType != null &&
                        !articleCategoryType.equals(Article.Type.H) &&
                        (fitsToArticles.size() > 0 || fitsToProducts.size() > 0)) {
                    // This article had multiple types in the old system
                    // A decision was made to only migrate the primary category which in this context
                    // means not setting based on product as that would cause backend to assume it is type H
                    // rather than the non-ISO code category
                    MigrationError migrationError = new MigrationError();
                    migrationError.setSeverity(MigrationError.Severity.WARNING);
                    migrationError.setClassifier(Classifier.HJALPMEDEL_BASED_ON);
                    migrationError.setOrganizationName(supplier.getOrganizationName());
                    migrationError.setCatalogueUniqueNumber(article.getCatalogueUniqueNumber());
                    migrationError.setMessage(MessageTemplate.ARTICLE_HAS_MULTIPLE_TYPES);
                    migrationErrorRepository.add(migrationError);
                } else {
                    // If this article is based on a product then it should inherit the category of the product
                    // There exists cases e.g. 102917788 of articles H having a different category than their product
                    // which is plainly wrong and must be handled here
                    article.setCategory(product.getCategory());
                    article.setBasedOnProduct(product);
                }
            }
        }

        // Now after article category may have changed, perform things related to category
        if (assistiveProduct instanceof ArticleAPINode) {
            // Category properties
            ArticleAPINode articleAPINode = (ArticleAPINode) assistiveProduct;
            CategoryAPI category = articleAPINode.getCategory();
            articleAPINode.setCategoryPropertys(categoryProperties(xmlProduct, category));

            // Extended categories
            if (category.getCode() != null) {
                List<CategoryAPI> extendedCategories = categoryOf(xmlProduct.getClassifications()).stream()
                                                                                               .filter(extendedCategory -> !extendedCategory.getId()
                                                                                                                                            .equals(category.getId()))
                                                                                               .collect(Collectors.toList());
                articleAPINode.setExtendedCategories(extendedCategories);
            }
        }

        Set<AssistiveProduct> assistiveProductsToReplace = productWithAssistiveProduct.product()
                                                                                      .getReplacesList()
                                                                                      .stream()
                                                                                      .map(BlindReference::getIdRef)
                                                                                      .filter(Objects::nonNull)
                                                                                      .map(aidMap::get)
                                                                                      .filter(Objects::nonNull)
                                                                                      .map(ProductWithAssistiveProduct::assistiveProduct)
                                                                                      .filter(Objects::nonNull)
                                                                                      .collect(Collectors.toSet());

        for (AssistiveProduct replacedAid : assistiveProductsToReplace) {
            if (!assistiveProduct.getClass()
                                 .equals(replacedAid.getClass())) {
                MigrationError migrationError = new MigrationError();
                migrationError.setSeverity(MigrationError.Severity.WARNING);
                migrationError.setClassifier(Classifier.HJALPMEDEL_REPLACED_BY);
                migrationError.setOrganizationName(supplier.getOrganizationName());
                migrationError.setCatalogueUniqueNumber(replacedAid.getCatalogueUniqueNumber());

                if (assistiveProduct instanceof ArticleAPINode) {
                    migrationError.setMessage(MessageTemplate.ARTICLE_CANNOT_REPLACE_PRODUCT, assistiveProduct.getCatalogueUniqueNumber(), replacedAid.getCatalogueUniqueNumber());
                } else {
                    migrationError.setMessage(MessageTemplate.PRODUCT_CANNOT_REPLACE_ARTICLE, assistiveProduct.getCatalogueUniqueNumber(), replacedAid.getCatalogueUniqueNumber());
                }
                migrationErrorRepository.add(migrationError);
            } else if (assistiveProduct instanceof ArticleAPINode && replacedAid instanceof ArticleAPINode) {
                ((ArticleAPINode) replacedAid).addReplacedByArticlesItem((ArticleAPI) assistiveProduct);
            } else if (assistiveProduct instanceof ProductAPINode && replacedAid instanceof ProductAPINode) {
                ((ProductAPINode) replacedAid).addReplacedByProductsItem((ProductAPI) assistiveProduct);
            }
        }

        return productWithAssistiveProduct;
    }

    private Optional<CategoryAPI> category(Product product) {
        // ReferencedCategory
        CategoryAPI categoryUsingCategoryProduct = Optional.ofNullable(product.getReferencedCategory())
                                                           .map(ProductCategoryProduct::getClassification)
                                                           .map(ProductClassification::getCode)
                                                           .map(hjmtjCategoryRepository::findByCode)
                                                           .orElse(null);
        if (categoryUsingCategoryProduct != null &&
                hjmtjCategoryRepository.isLeaf(categoryUsingCategoryProduct)) {
            return Optional.of(categoryUsingCategoryProduct);
        }

        // CategoryPath for 4th level category
        ProductCategoryPath xmlCategories = product.getXmlCategories();
        CategoryAPI categoryUsingPath = hjmtjCategoryRepository.findBy(xmlCategories);
        if (categoryUsingPath != null &&
                hjmtjCategoryRepository.isLeaf(categoryUsingPath)) {
            return Optional.of(categoryUsingPath);
        }

        if (xmlCategories != null &&
                xmlCategories.leaf() != null &&
                Objects.equals(xmlCategories.leaf()
                                            .getCode(), "222490")) {
            // Fix assistive products with category 222490 Totalkonversationsenheter,
            // migrate them as having sub-category Totalkonversationsenhet
            xmlCategories.setCategoryPath(xmlCategories.getCategoryPath() + "\\Totalkonversationsenhet");
            categoryUsingPath = hjmtjCategoryRepository.findBy(xmlCategories);

            if (categoryUsingPath != null &&
                    hjmtjCategoryRepository.isLeaf(categoryUsingPath)) {
                return Optional.of(categoryUsingPath);
            }
        }

        return Optional.empty();
    }

    private Optional<CategoryAPI> articleCategory(Product product) {
        Optional<CategoryAPI> category = category(product);

        if (category.isPresent()) {
            return category;
        }

        // Check if this is a non-ISO category article
        ProductCategory leafCategory = product.getXmlCategories()
                                              .leaf();
        if (leafCategory != null) {
            String categoryName = leafCategory.getName();

            switch (categoryName) {
                case "Tillbehör":
                    return Optional.of(hjmtjCategoryRepository.accessoryId());
                case "Reservdelar":
                    return Optional.of(hjmtjCategoryRepository.sparePartId());
                case "Tjänster":
                    return Optional.of(hjmtjCategoryRepository.serviceId());
                case "Inställningar":
                    return Optional.of(hjmtjCategoryRepository.settingId());
            }
        }

        return Optional.empty();
    }

    private ProductWithAssistiveProduct createProduct(Product product) throws NotAnISO9999Product {
        CategoryAPI category = category(product).orElseThrow(() -> new NotAnISO9999Product(product));

        ProductAPINode productAPINodeAPI = new ProductAPINode(graph, product.getId());
        productAPINodeAPI.setProductName(inventoryBuilderHelper.getArticleOrProductName(product));
        productAPINodeAPI.setProductNumber(inventoryBuilderHelper.getArticleOrProductNumber(product));
        productAPINodeAPI.setTrademark(product.getBrand());
        productAPINodeAPI.setPackageLevelBase(inventoryBuilderHelper.getPackageLevelBase(product));
        productAPINodeAPI.setPackageLevelMiddle(inventoryBuilderHelper.getPackageLevelMiddle(product));
        productAPINodeAPI.setPackageLevelTop(inventoryBuilderHelper.getPackageLevelTop(product));
        productAPINodeAPI.setManufacturerElectronicAddress(inventoryBuilderHelper.getManufacturerElectronicAddress(product));
        productAPINodeAPI.setCeStandard(inventoryBuilderHelper.getCeStandard(product));
        setCE(product, productAPINodeAPI);

        productAPINodeAPI.setSupplementedInformation(inventoryBuilderHelper.getSupplementalInformation(product));
        productAPINodeAPI.setManufacturerProductNumber(inventoryBuilderHelper.getManufacturerProductNumber(product));
        productAPINodeAPI.setManufacturer(product.getManufacturerName());
        productAPINodeAPI.setCategory(category);
        productAPINodeAPI.setColor(product.getColor());

        productAPINodeAPI.setStatus(inventoryBuilderHelper.getStatus());
        productAPINodeAPI.setOrganizationName(supplier.getOrganizationName());

        Long replacementDate = toEpochMillis(product.getExpireTime());
        productAPINodeAPI.setReplacementDate(replacementDate);

        //------------------------------------------------------------------------------
        productAPINodeAPI.setOrderUnit(inventoryBuilderHelper.getOrderUnit(supplier, product));

        productAPINodeAPI.setPackageContentUnit(inventoryBuilderHelper.getPackageContentUnit(product));

        productAPINodeAPI.setArticleQuantityInOuterPackage(product.getPackQty() == null ? null : product.getPackQty()
                                                                                                        .doubleValue());
        productAPINodeAPI.setArticleQuantityInOuterPackageUnit(inventoryBuilderHelper.getQuantityInOuterPackageUnit(product));

        productAPINodeAPI.setPackageLevelBase(inventoryBuilderHelper.getPackageLevelBase(product));
        productAPINodeAPI.setPackageLevelMiddle(inventoryBuilderHelper.getPackageLevelMiddle(product));
        productAPINodeAPI.setPackageLevelTop(inventoryBuilderHelper.getPackageLevelTop(product));
        //------------------------------------------------------------------------------

        productAPINodeAPI.setCustomerUnique(product.getCustomerUnique() == null ? false : product.getCustomerUnique());

        productAPINodeAPI.setCategoryPropertys(categoryProperties(product, category));

        setCatalogueUniqueNumber(product, productAPINodeAPI);

        List<CategoryAPI> extendedCategories = categoryOf(product.getClassifications()).stream()
                                                                                       .filter(extendedCategory -> !extendedCategory.getId()
                                                                                                                                    .equals(category.getId()))
                                                                                       .collect(Collectors.toList());
        productAPINodeAPI.setExtendedCategories(extendedCategories);

        return new ProductWithAssistiveProduct(product, productAPINodeAPI);
    }


    private List<CategoryAPI> categoryOf(Collection<ProductClassification> productClassifications) {
        return productClassifications.stream()
                                     .map(this::categoryOf)
                                     .filter(Objects::nonNull)
                                     .filter(hjmtjCategoryRepository::isLeaf)
                                     .map(t -> new Unique<>(t, CategoryAPI::getId))
                                     .distinct()
                                     .map(Unique::data)
                                     .collect(Collectors.toList());
    }

    private CategoryAPI categoryOf(ProductClassification productClassification) {
        if (productClassification == null) {
            return null;
        }

        CategoryAPI categoryAPI = hjmtjCategoryRepository.findByCode(productClassification.getCode());

        if (categoryAPI != null) {
            // If this is a non-leaf category we need to expand it
            if (!hjmtjCategoryRepository.isLeaf(categoryAPI)) {
                return hjmtjCategoryRepository.findBy(productCategoryPathOf(categoryAPI));
            }
        }

        return categoryAPI;
    }

    private ProductCategoryPath productCategoryPathOf(CategoryAPI categoryAPI) {
        // Create an artificial "CategoryPath" - the intermediate levels are not necessary
        StringBuilder sb = new StringBuilder();
        sb.append("Produkt\\Vara\\ISO-klassificerade hjälpmedel (ISO 9999)\\Hjälpmedelsområden\\");
        sb.append(categoryAPI.getCode());
        sb.append(" ");
        sb.append(categoryAPI.getName());
        sb.append("\\");
        sb.append(categoryAPI.getName());

        ProductCategoryPath productCategoryPath = new ProductCategoryPath();
        productCategoryPath.setCategoryPath(sb.toString());
        return productCategoryPath;
    }

    private void setCE(Product product, AssistiveProduct assistiveProduct) {
        Boolean ceMarked = inventoryBuilderHelper.isCeMarked(product);
        CVCEDirectiveAPI ceDirective = inventoryBuilderHelper.getCeDirective(product);
        String ceDirectiveName = ceDirective != null ? ceDirective.getName() : null;

        // https://riv-ta.atlassian.net/browse/HJAL-1134
        if (ceMarked == null) {
            if (ceDirectiveName != null && ceDirectiveName.equalsIgnoreCase(CE_DIRECTIVE_MEDICAL)) {
                assistiveProduct.setCeMarked(true);
                assistiveProduct.setCeDirective(ceDirective);
            } else {
                assistiveProduct.setCeMarked(false);
                assistiveProduct.setCeDirective(null);
            }
        } else {
            assistiveProduct.setCeMarked(ceMarked);
            assistiveProduct.setCeDirective(ceMarked ? ceDirective : null);
        }
    }

    private void setCatalogueUniqueNumber(Product product, AssistiveProduct assistiveProduct) {
        try {
            assistiveProduct.setCatalogueUniqueNumber(Long.valueOf(product.getVgrProdNo()));
        } catch (NumberFormatException e) {
            MigrationError migrationError = new MigrationError();
            migrationError.setSeverity(MigrationError.Severity.ERROR);
            migrationError.setOrganizationName(supplier.getOrganizationName());
            migrationError.setClassifier(Classifier.HJALPMEDEL_CATALOGUE_UNIQUE_NUMBER);
            migrationError.setMessage(MessageTemplate.CATALOGUE_UNIQUE_NUMBER_NAN, product.getVgrProdNo());
            migrationErrorRepository.add(migrationError);
        }
    }

    private ProductWithAssistiveProduct createArticle(Product product) throws ArticleHasNoType, NotAnISO9999Article {
        CategoryAPI category = articleCategory(product).orElseThrow(() -> new NotAnISO9999Article(product));

        if (product.isNoArticleType()) {
            throw new ArticleHasNoType(product);
        }

        ArticleAPINode articleAPINodeAPI = new ArticleAPINode(graph, product.getId());
        articleAPINodeAPI.setArticleName(inventoryBuilderHelper.getArticleOrProductName(product));
        articleAPINodeAPI.setArticleNumber(inventoryBuilderHelper.getArticleOrProductNumber(product));
        articleAPINodeAPI.setSupplementedInformation(inventoryBuilderHelper.getSupplementalInformation(product));
        articleAPINodeAPI.setManufacturerArticleNumber(inventoryBuilderHelper.getManufacturerProductNumber(product));
        articleAPINodeAPI.setManufacturer(product.getManufacturerName());
        articleAPINodeAPI.setTrademark(product.getBrand());

        //------------------------------------------------------------------------------
        articleAPINodeAPI.setOrderUnit(inventoryBuilderHelper.getOrderUnit(supplier, product));

        articleAPINodeAPI.setPackageContentUnit(inventoryBuilderHelper.getPackageContentUnit(product));

        articleAPINodeAPI.setArticleQuantityInOuterPackage(product.getPackQty() == null ? null : product.getPackQty()
                                                                                                        .doubleValue());
        articleAPINodeAPI.setArticleQuantityInOuterPackageUnit(articleAPINodeAPI.getArticleQuantityInOuterPackageUnit());

        articleAPINodeAPI.setPackageLevelBase(inventoryBuilderHelper.getPackageLevelBase(product));
        articleAPINodeAPI.setPackageLevelMiddle(inventoryBuilderHelper.getPackageLevelMiddle(product));
        articleAPINodeAPI.setPackageLevelTop(inventoryBuilderHelper.getPackageLevelTop(product));
        //------------------------------------------------------------------------------

        articleAPINodeAPI.setManufacturerElectronicAddress(inventoryBuilderHelper.getManufacturerElectronicAddress(product));
        articleAPINodeAPI.setCeStandard(inventoryBuilderHelper.getCeStandard(product));
        setCE(product, articleAPINodeAPI);

        articleAPINodeAPI.setOrganizationName(supplier.getOrganizationName());
        articleAPINodeAPI.setCategory(category);
        articleAPINodeAPI.setStatus(inventoryBuilderHelper.getStatus());
        articleAPINodeAPI.setColor(product.getColor());
        articleAPINodeAPI.setCustomerUnique(product.getCustomerUnique() == null ? false : product.getCustomerUnique());

        Long replacementDate = toEpochMillis(product.getExpireTime());
        articleAPINodeAPI.setReplacementDate(replacementDate);

        setCatalogueUniqueNumber(product, articleAPINodeAPI);

        // https://riv-ta.atlassian.net/browse/HJAL-1041
        Gtin13 gtin = new Gtin13(product.getGtin());
        Set<ConstraintViolation<Gtin13>> constraintViolations = validator.validate(gtin);

        if (constraintViolations.size() > 0) {
            MigrationError migrationError = new MigrationError();
            migrationError.setSeverity(MigrationError.Severity.WARNING);
            migrationError.setClassifier(Classifier.HJALPMEDEL_GTIN13);
            migrationError.setOrganizationName(supplier.getOrganizationName());
            migrationError.setCatalogueUniqueNumber(articleAPINodeAPI.getCatalogueUniqueNumber());
            migrationError.setMessage(MessageTemplate.INVALID_GTIN13, gtin.stringValue());
            migrationErrorRepository.add(migrationError);
        } else {
            articleAPINodeAPI.setGtin(gtin.stringValue());
        }

        return new ProductWithAssistiveProduct(product, articleAPINodeAPI);
    }

    private void addFits(Map<Long, ProductWithAssistiveProduct> aidMap, ProductWithAssistiveProduct productWithAssistiveProduct) {
        AssistiveProduct assistiveProduct = productWithAssistiveProduct.assistiveProduct();

        Article.Type articleType = assistiveProduct
                .getCategory()
                .getArticleType();
        switch (articleType) {
            case H:
                break;
            case I:
                addFits(aidMap, productWithAssistiveProduct, productWithAssistiveProduct.product()
                                                                                        .getSettingFor());
                break;
            case R:
                addFits(aidMap, productWithAssistiveProduct, productWithAssistiveProduct.product()
                                                                                        .getSparePartFor());
                break;
            case T:
                addFits(aidMap, productWithAssistiveProduct, productWithAssistiveProduct.product()
                                                                                        .getAccessoryTo());
                break;
            case Tj:
                addFits(aidMap, productWithAssistiveProduct, productWithAssistiveProduct.product()
                                                                                        .getServiceFor());
                break;
        }
    }

    private void addFits(Map<Long, ProductWithAssistiveProduct> aidMap, ProductWithAssistiveProduct productWithAssistiveProduct, Set<BlindReference> fits) {
        AssistiveProduct from = productWithAssistiveProduct.assistiveProduct();
        Product product = productWithAssistiveProduct.product();

        for (BlindReference xmlReference : fits) {
            addFit(aidMap, from, product, xmlReference);
        }
    }

    private void addFit(Map<Long, ProductWithAssistiveProduct> aidMap, AssistiveProduct assistiveProduct, Product product, BlindReference xmlReference) {
        ProductWithAssistiveProduct toProductWithAssistiveProduct = aidMap.get(xmlReference.getIdRef());

        if (toProductWithAssistiveProduct == null || toProductWithAssistiveProduct.assistiveProduct() == null) {
            Product fitsProduct = productRepository.findById(xmlReference.getIdRef());

            if (fitsProduct == null) {
                MigrationError migrationError = new MigrationError();
                migrationError.setSeverity(MigrationError.Severity.ERROR);
                migrationError.setClassifier(Classifier.HJALPMEDEL_FIT);
                migrationError.setOrganizationName(supplier.getOrganizationName());
                migrationError.setCatalogueUniqueNumber(assistiveProduct.getCatalogueUniqueNumber());
                migrationError.setMessage(MessageTemplate.ASSISTIVE_PRODUCT_WITH_ID_DOES_NOT_EXIST, xmlReference.getIdRef());
                migrationErrorRepository.add(migrationError);

            } else if (!fitsProduct.getSupplier()
                                   .equals(product.getSupplier())) {
                MigrationError migrationError = new MigrationError();
                migrationError.setSeverity(MigrationError.Severity.WARNING);
                migrationError.setClassifier(Classifier.HJALPMEDEL_FIT);
                migrationError.setOrganizationName(supplier.getOrganizationName());
                migrationError.setCatalogueUniqueNumber(assistiveProduct.getCatalogueUniqueNumber());
                migrationError.setMessage(MessageTemplate.ASSISTIVE_PRODUCT_CANNOT_FIT_ASSISTIVE_PRODUCT_ON_DIFFERENT_SUPPLIER,
                        fitsProduct.getVgrProdNo(),
                        fitsProduct.getSupplier()
                                   .getName(),
                        product.getVgrProdNo(),
                        product.getSupplier()
                               .getSupplier() != null ? product.getSupplier()
                                                               .getSupplier()
                                                               .getName() : product.getSupplier()
                                                                                   .getName());
                migrationErrorRepository.add(migrationError);
            }
        } else {
            AssistiveProduct to = toProductWithAssistiveProduct.assistiveProduct();

            if (assistiveProduct instanceof ArticleAPINode &&
                    ((ArticleAPINode) assistiveProduct).isTIso() &&
                    to instanceof ProductAPINode) {
                // TIso in data will have a fit to product rather than being based on that product
                // We must change so that is based on the product instead
                ((ArticleAPINode) assistiveProduct).setBasedOnProduct((ProductAPINode) to);
            } else {
                try {
                    assistiveProduct.addFit(to);
                } catch (UnsupportedFitException e) {
                    MigrationError migrationError = new MigrationError();
                    migrationError.setSeverity(MigrationError.Severity.WARNING);
                    migrationError.setClassifier(Classifier.HJALPMEDEL_FIT);
                    migrationError.setOrganizationName(supplier.getOrganizationName());
                    migrationError.setCatalogueUniqueNumber(assistiveProduct.getCatalogueUniqueNumber());
                    migrationError.setMessage(e.getMessage());
                    migrationErrorRepository.add(migrationError);
                }
            }
        }
    }

    private List<ResourceSpecificPropertyAPI> categoryProperties(Product product, CategoryAPI category) {
        List<ResourceSpecificPropertyAPI> resourceSpecificProperties = new LinkedList<>();

        Set<ProductCategorySpecificProperty> categorySpecificProperties = product.getCategorySpecificProperties()
                                                                                 .stream()
                                                                                 .filter(ProductCategorySpecificProperty::isValuePresent)
                                                                                 .collect(Collectors.toSet());
        for (ProductCategorySpecificProperty categorySpecificProperty : categorySpecificProperties) {
            try {
                ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = map(category, categorySpecificProperty);
                resourceSpecificProperties.add(resourceSpecificPropertyAPI);
            } catch (CategorySpecificPropertyTypeNotFound e) {
                MigrationError migrationError = new MigrationError();
                migrationError.setClassifier(Classifier.CATEGORY_SPECIFIC_PROPERTY);
                migrationError.setMessage(e.getMessage());
                migrationError.setCatalogueUniqueNumber(product.getVgrProdNo());
                migrationError.setOrganizationName(supplier.getOrganizationName());
                migrationErrorRepository.add(migrationError);
            } catch (CategorySpecificPropertyListValueNotFound e) {
                MigrationError migrationError = new MigrationError();
                migrationError.setClassifier(Classifier.CATEGORY_SPECIFIC_PROPERTY_LISTVALUE);
                migrationError.setMessage(e.getMessage());
                migrationError.setCatalogueUniqueNumber(product.getVgrProdNo());
                migrationError.setOrganizationName(supplier.getOrganizationName());
                migrationErrorRepository.add(migrationError);
            }
        }

        // Combine VALUELIST_MULTIPLE
        Map<Long, List<ResourceSpecificPropertyAPI>> idToDuplicates = resourceSpecificProperties.stream()
                                                                                                .collect(Collectors.groupingBy(this::categorySpecificPropertyId));

        resourceSpecificProperties = new LinkedList<>();
        for (List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIList : idToDuplicates.values()) {
            ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = resourceSpecificPropertyAPIList.get(0);

            Set<Long> listValues = resourceSpecificPropertyAPIList.stream()
                                                                  .map(ResourceSpecificPropertyAPI::getMultipleListValue)
                                                                  .filter(Objects::nonNull)
                                                                  .flatMap(Collection::stream)
                                                                  .collect(Collectors.toSet());

            if (listValues.size() > 0) {
                resourceSpecificPropertyAPI.setMultipleListValue(new LinkedList<>(listValues));
            }

            resourceSpecificProperties.add(resourceSpecificPropertyAPI);
        }

        // Handle duplicates
        Map<Long, List<ResourceSpecificPropertyAPI>> categorySpecificPropertyIdMap = resourceSpecificProperties.stream()
                                                                                                               .filter(this::isValueSet)
                                                                                                               .collect(Collectors.groupingBy(t -> t.getProperty()
                                                                                                                                                    .getId()));
        resourceSpecificProperties = new LinkedList<>();
        for (List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIList : categorySpecificPropertyIdMap.values()) {
            if (resourceSpecificPropertyAPIList.size() > 0) {
                ResourceSpecificPropertyAPI firstResourceSpecificPropertyAPI = resourceSpecificPropertyAPIList.get(0);
                resourceSpecificProperties.add(firstResourceSpecificPropertyAPI);

                // Warn if values will be lost
                if (resourceSpecificPropertyAPIList.size() > 1) {
                    MigrationError migrationError = new MigrationError();
                    migrationError.setSeverity(MigrationError.Severity.ERROR);
                    migrationError.setClassifier(Classifier.CATEGORY_SPECIFIC_PROPERTY_LISTVALUE);
                    migrationError.setOrganizationName(supplier.getOrganizationName());
                    migrationError.setCatalogueUniqueNumber(Long.valueOf(product.getVgrProdNo()));
                    CategorySpecificPropertyAPI property = firstResourceSpecificPropertyAPI.getProperty();
                    migrationError.setMessage(MessageTemplate.CATEGORY_SPECIFIC_PROPERTY_LISTVALUE, product.getVgrProdNo(), property.getId(), property.getName());
                    migrationErrorRepository.add(migrationError);
                }
            }
        }

        return resourceSpecificProperties;
    }

    private boolean isValueSet(ResourceSpecificPropertyAPI resourceSpecificPropertyAPI) {
        return getValueAsString(resourceSpecificPropertyAPI) != null;
    }

    private String getValueAsString(ResourceSpecificPropertyAPI resourceSpecificPropertyAPI) {
        CategorySpecificProperty.Type type = CategorySpecificProperty.Type.valueOf(resourceSpecificPropertyAPI.getProperty()
                                                                                                              .getType());

        switch (type) {
            case TEXTFIELD:
                return resourceSpecificPropertyAPI.getTextValue();
            case DECIMAL:
                return resourceSpecificPropertyAPI.getDecimalValue()
                                                  .toString();
            case VALUELIST_SINGLE:
                return resourceSpecificPropertyAPI.getSingleListValue()
                                                  .toString();
            case VALUELIST_MULTIPLE:
                return resourceSpecificPropertyAPI.getMultipleListValue()
                                                  .isEmpty() ? null : resourceSpecificPropertyAPI.getMultipleListValue()
                                                                                                 .stream()
                                                                                                 .map(String::valueOf)
                                                                                                 .collect(Collectors.joining(", "));
            case INTERVAL:
                if (resourceSpecificPropertyAPI.getIntervalFromValue() != null) {
                    return resourceSpecificPropertyAPI.getIntervalFromValue()
                                                      .toString();
                } else if (resourceSpecificPropertyAPI.getIntervalToValue() != null) {
                    return resourceSpecificPropertyAPI.getIntervalToValue()
                                                      .toString();
                }

                return null;
        }

        return null;
    }

    private ResourceSpecificPropertyAPI map(CategoryAPI category, ProductCategorySpecificProperty
            productCategorySpecificProperty) throws
            CategorySpecificPropertyTypeNotFound, CategorySpecificPropertyListValueNotFound {
        ProductCategorySpecificPropertyType productCategorySpecificPropertyType = productCategorySpecificProperty.getProductCategorySpecificPropertyType();
        Set<CategorySpecificProperty> matchesCategorySpecificProperties = productCategorySpecificPropertyType.getMatchesCategorySpecificProperties();

        if (matchesCategorySpecificProperties.isEmpty()) {
            throw new CategorySpecificPropertyTypeNotFound(productCategorySpecificPropertyType, " has no matches");
        }

        // Find the one for this product category
        CategorySpecificProperty categorySpecificProperty = null;
        for (CategorySpecificProperty candidate : matchesCategorySpecificProperties) {
            if (candidate.getCategory()
                         .getUniqueId()
                         .equals(category.getId())) {
                categorySpecificProperty = candidate;
                break;
            }
        }

        if (categorySpecificProperty == null) {
            throw new CategorySpecificPropertyTypeNotFound(productCategorySpecificPropertyType, " has no match for Category " + category.getCode() + " " + category.getName());
        }

        return resourceSpecificProperty(productCategorySpecificProperty, categorySpecificProperty);
    }

    private Long categorySpecificPropertyId(ResourceSpecificPropertyAPI resourceSpecificPropertyAPI) {
        return resourceSpecificPropertyAPI.getProperty()
                                          .getId();
    }

    private CategorySpecificPropertyAPI toAPI(CategorySpecificProperty categorySpecificProperty) {
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();
        categorySpecificPropertyAPI.setId(categorySpecificProperty.getUniqueId());
        categorySpecificPropertyAPI.setType(categorySpecificProperty.getType()
                                                                    .toString());
        categorySpecificPropertyAPI.setDescription(categorySpecificProperty.getDescription());
        categorySpecificPropertyAPI.setName(categorySpecificProperty.getName());
        categorySpecificPropertyAPI.setValues(categorySpecificProperty.getCategorySpecificPropertyListValues()
                                                                      .stream()
                                                                      .map(this::toAPI)
                                                                      .collect(Collectors.toList()));

        return categorySpecificPropertyAPI;
    }

    private CategorySpecificPropertyListValueAPI toAPI(CategorySpecificPropertyListValue categorySpecificPropertyListValue) {
        CategorySpecificPropertyListValueAPI categorySpecificPropertyListValueAPI = new CategorySpecificPropertyListValueAPI();
        categorySpecificPropertyListValueAPI.setId(categorySpecificPropertyListValue.getUniqueId());
        categorySpecificPropertyListValueAPI.setValue(categorySpecificPropertyListValue.getValue());

        return categorySpecificPropertyListValueAPI;
    }

    private ResourceSpecificPropertyAPI resourceSpecificProperty(ProductCategorySpecificProperty
                                                                         productCategorySpecificProperty, CategorySpecificProperty categorySpecificProperty) throws
            CategorySpecificPropertyListValueNotFound {
        CategorySpecificProperty.Type categorySpecificPropertyType = categorySpecificProperty.getType();
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = toAPI(categorySpecificProperty);

        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
        resourceSpecificPropertyAPI.setProperty(categorySpecificPropertyAPI);

        ProductCategorySpecificPropertyListValue listValue = productCategorySpecificProperty.getListValue();
        Boolean booleanValue = productCategorySpecificProperty.getBooleanValue();
        if (booleanValue != null) {
            String expectedValue = booleanValue ? "Ja" : "Nej";
            resourceSpecificPropertyAPI.setSingleListValue(getListValue(categorySpecificProperty, expectedValue));
            return resourceSpecificPropertyAPI;
        }

        switch (categorySpecificPropertyType) {
            case TEXTFIELD:
                resourceSpecificPropertyAPI.setTextValue(productCategorySpecificProperty.getStringValue());
                break;
            case DECIMAL:
                if (productCategorySpecificProperty.getDecimalValue() != null) {
                    resourceSpecificPropertyAPI.setDecimalValue(productCategorySpecificProperty.getDecimalValue()
                                                                                               .doubleValue());
                }
                break;
            case VALUELIST_SINGLE:
                if (listValue != null) {
                    resourceSpecificPropertyAPI.setSingleListValue(getListValue(categorySpecificProperty, listValue.getValue()));
                }
                break;
            case VALUELIST_MULTIPLE:
                if (listValue != null) {
                    Long listValueUniqueId = getListValue(categorySpecificProperty, listValue.getValue());
                    resourceSpecificPropertyAPI.setMultipleListValue(Collections.singletonList(listValueUniqueId));
                }
                break;
            case INTERVAL:
                ProductIntervalDecimal interval = productCategorySpecificProperty.getIntervalDecimalValue();

                if (interval != null && interval.min() != null) {
                    BigDecimal min = interval.min();
                    resourceSpecificPropertyAPI.setIntervalFromValue(min.doubleValue());
                }
                if (interval != null && interval.max() != null) {
                    BigDecimal max = interval.max();
                    resourceSpecificPropertyAPI.setIntervalToValue(max.doubleValue());
                }
                break;
        }

        return resourceSpecificPropertyAPI;
    }

    private Long getListValue(CategorySpecificProperty categorySpecificProperty, String expectedValue) throws
            CategorySpecificPropertyListValueNotFound {
        for (CategorySpecificPropertyListValue categorySpecificPropertyListValue : categorySpecificProperty.getCategorySpecificPropertyListValues()) {
            if (categorySpecificPropertyListValue.getValue()
                                                 .trim()
                                                 .toLowerCase()
                                                 .equalsIgnoreCase(expectedValue.trim()
                                                                                .toLowerCase())) {
                return categorySpecificPropertyListValue.getUniqueId();
            }
        }

        throw new CategorySpecificPropertyListValueNotFound(categorySpecificProperty, expectedValue);
    }

    @Value
    public static class Context {

        private final Map<Long, Product> idToProduct;

        private final InventoryBuilderHelper inventoryBuilderHelper;

        private final ProductRepository productRepository;

        private final HjmtjCategoryRepository hjmtjCategoryRepository;

        private final ProductCategorySpecificPropertyTypeRepository productCategorySpecificPropertyTypeRepository;

        private final MigrationErrorRepository migrationErrorRepository;

        private final OrganizationAPI supplier;

        private final DirectedAcyclicGraph<AssistiveProduct, DefaultEdge> graph;

        private final Validator validator;

        private final HjmtjCvCeDirectiveRepository hjmtjCvCeDirectiveRepository;

        private final HjmtjCvCeStandardRepository hjmtjCvCeStandardRepository;

        public Context(DirectedAcyclicGraph<AssistiveProduct, DefaultEdge> graph, Map<Long, Product> idToProduct, InventoryBuilderHelper inventoryBuilderHelper, ProductRepository productRepository, HjmtjCategoryRepository hjmtjCategoryRepository, ProductCategorySpecificPropertyTypeRepository productCategorySpecificPropertyTypeRepository, MigrationErrorRepository migrationErrorRepository, OrganizationAPI supplier, Validator validator,
                       HjmtjCvCeDirectiveRepository hjmtjCvCeDirectiveRepository, HjmtjCvCeStandardRepository hjmtjCvCeStandardRepository) {
            this.graph = graph;
            this.idToProduct = idToProduct;
            this.inventoryBuilderHelper = inventoryBuilderHelper;
            this.productRepository = productRepository;
            this.hjmtjCategoryRepository = hjmtjCategoryRepository;
            this.productCategorySpecificPropertyTypeRepository = productCategorySpecificPropertyTypeRepository;
            this.migrationErrorRepository = migrationErrorRepository;
            this.supplier = supplier;
            this.validator = validator;
            this.hjmtjCvCeDirectiveRepository = hjmtjCvCeDirectiveRepository;
            this.hjmtjCvCeStandardRepository = hjmtjCvCeStandardRepository;
        }

    }
}

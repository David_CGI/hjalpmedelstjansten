/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.product.api.builder;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.business.product.controller.PackageUnitController;
import se.inera.hjalpmedelstjansten.business.product.controller.PackageUnitMapper;
import se.inera.hjalpmedelstjansten.model.api.ElectronicAddressAPI;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCEDirectiveAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCEStandardAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVOrderUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPackageUnitAPI;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MessageTemplate;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.domain.model.product.entities.Product;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductMarking;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductUnit;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjCvCeDirectiveRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjCvCeStandardRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjCvOrderUnit;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Singleton
public class InventoryBuilderHelper {

    private static final String STATUS_PUBLISHED = "PUBLISHED";

    @Inject
    private HjmtjCvOrderUnit hjmtjCvOrderUnit;

    @Inject
    private HjmtjCvCeDirectiveRepository hjmtjCvCeDirectiveRepository;

    @Inject
    private HjmtjCvCeStandardRepository hjmtjCvCeStandardRepository;

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    private final Map<String, CVPackageUnitAPI> codeToCvPackageUnitAPI = new HashMap<>();

    public InventoryBuilderHelper() {
    }

    @Inject
    public void setPackageUnitController(PackageUnitController packageUnitController) {
        Map<String, CVPackageUnitAPI> codeToCvPackageUnitAPI = packageUnitController.findAll()
                                                                                    .stream()
                                                                                    .map(PackageUnitMapper::map)
                                                                                    .filter(Objects::nonNull)
                                                                                    .collect(Collectors.toMap(CVPackageUnitAPI::getCode, Function.identity()));
        this.codeToCvPackageUnitAPI.putAll(codeToCvPackageUnitAPI);
    }

    public ElectronicAddressAPI getManufacturerElectronicAddress(Product xmlProduct) {
        ElectronicAddressAPI manufacturerElectronicAddress = new ElectronicAddressAPI();

        // TODO I have found EXTREMELY long URLs - the longest is 300
        // We need to make a decision if these should be shortened or if we should bump the limit
        manufacturerElectronicAddress.setWeb(limit(xmlProduct.getManufacturerWebUrl(), 255));
        return manufacturerElectronicAddress;
    }

    private String limit(String value, int max) {
        if (value == null) {
            return null;
        }

        return value.substring(0, Math.min(max, value.length()));
    }

    public Integer getPackageLevelTop(Product xmlProduct) {
        if (xmlProduct.getPackagePallet() == null) {
            return null;
        }

        return xmlProduct.getPackagePallet()
                         .intValue();
    }

    public Integer getPackageLevelMiddle(Product xmlProduct) {
        if (xmlProduct.getPackage2() == null) {
            return null;
        }

        return xmlProduct.getPackage2()
                         .intValue();
    }

    public Integer getPackageLevelBase(Product xmlProduct) {
        if (xmlProduct.getPackage1() == null) {
            return null;
        }

        return xmlProduct.getPackage1()
                         .intValue();
    }

    public String getSupplementalInformation(Product xmlProduct) {
        return limit(xmlProduct.getSupplementalInformation(), 512);
    }

    public String getArticleOrProductName(Product xmlProduct) {
        return limit(xmlProduct.getSupplierProductName(), 255);
    }

    public Boolean isCeMarked(Product xmlProduct) {
        ProductMarking productMarking = xmlProduct.getProductMarking();

        if (productMarking == null || productMarking.getCeMark() == null) {
            return null;
        }

        // https://riv-ta.atlassian.net/browse/HJAL-1116
        return productMarking.getCeMark();
    }

    public String getManufacturerProductNumber(Product xmlProduct) {
        return limit(xmlProduct.getManufacturerProductNumber(), 255);
    }

    public String getArticleOrProductNumber(Product xmlProduct) {
        return limit(xmlProduct.getSupplierProductNumber(), 35);
    }

    public String getStatus() {
        return STATUS_PUBLISHED;
    }

    public CVOrderUnitAPI getOrderUnit(OrganizationAPI supplier, Product xmlProduct) {
        ProductUnit xmlUnit = xmlProduct.getOrderUnit();
        log.debug("orderUnit {}", xmlUnit);

        if (xmlUnit == null) {
            xmlUnit = new ProductUnit();
            xmlUnit.setCode("PCE");

            MigrationError migrationError = new MigrationError();
            migrationError.setSeverity(MigrationError.Severity.WARNING);
            migrationError.setClassifier(Classifier.HJALPMEDEL_ORDER_UNIT);
            migrationError.setOrganizationName(supplier.getOrganizationName());
            migrationError.setCatalogueUniqueNumber(xmlProduct.getVgrProdNo());
            migrationError.setMessage(MessageTemplate.ORDER_UNIT_NULL);
            migrationErrorRepository.add(migrationError);
        }

        return getUnitByCode(xmlUnit);
    }

    private CVOrderUnitAPI getUnitByCode(ProductUnit xmlUnit) {
        if (xmlUnit != null) {
            CVOrderUnitAPI cvOrderUnit = hjmtjCvOrderUnit.findByCode(xmlUnit.getCode());
            log.debug("cvOrderUnit {}", cvOrderUnit);

            return cvOrderUnit;
        }

        return null;
    }

    public CVPackageUnitAPI getPackageContentUnit(Product xmlProduct) {
        ProductUnit xmlUnit = xmlProduct.getPackUnit();
        log.debug("packUnit {}", xmlUnit);

        if (xmlUnit == null || xmlUnit.getCode() == null) {
            return null;
        }

        return codeToCvPackageUnitAPI.get(xmlUnit.getCode());
    }

    public CVOrderUnitAPI getQuantityInOuterPackageUnit(Product xmlProduct) {
        ProductUnit xmlUnit = xmlProduct.getPackUnit();
        log.debug("packUnit {}", xmlUnit);

        return getUnitByCode(xmlUnit);
    }


    public CVCEDirectiveAPI getCeDirective(Product product) {
        if (product.getProductMarking() != null && product.getProductMarking()
                                                          .getProductMarkingDirective() != null) {
            String value = product.getProductMarking()
                                  .getProductMarkingDirective()
                                  .getValue();
            CVCEDirectiveAPI cvceDirectiveAPI = hjmtjCvCeDirectiveRepository.findByName(value);

            if (cvceDirectiveAPI == null) {
                MigrationError migrationError = new MigrationError();
                migrationError.setClassifier(Classifier.CV_CE_DIRECTIVE);
                migrationError.setMessage(MessageTemplate.CVDIRECTIVE_VALUE_NOT_FOUND, value);
                migrationErrorRepository.add(migrationError);
            }

            return cvceDirectiveAPI;
        }

        return null;
    }

    public CVCEStandardAPI getCeStandard(Product product) {
        if (product.getProductMarking() != null && product.getProductMarking()
                                                          .getProductMarkingStandard() != null) {
            String value = product.getProductMarking()
                                  .getProductMarkingStandard()
                                  .getValue();
            CVCEStandardAPI cvceStandardAPI = hjmtjCvCeStandardRepository.findByName(value);

            if (cvceStandardAPI == null) {
                MigrationError migrationError = new MigrationError();
                migrationError.setClassifier(Classifier.CV_CE_STANDARD);
                migrationError.setMessage(MessageTemplate.CVCESTANDARD_VALUE_NOT_FOUND, value);
                migrationErrorRepository.add(migrationError);
            }

            return cvceStandardAPI;
        }

        return null;
    }

}
/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.product.entities;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQuery;
import javax.persistence.PersistenceUnit;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;

@PersistenceUnit(unitName = "local")
@Slf4j
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@NamedQuery(name = ProductClassification.XML_CLASSIFICATION_FIND_BY_ID, query = "SELECT new ProductClassification(id, code) FROM ProductClassification WHERE id = :classificationId")
@Table(indexes = {
        @Index(name = "ProductClassificationCodeIndex", columnList = "code")
})
@XmlRootElement(name = "Classification")
public class ProductClassification {

    public static final String XML_CLASSIFICATION_FIND_BY_ID = "ProductClassification.findById";

    @Id
    private Long id;

    @XmlElement(name = "Code")
    private String code;

    @XmlAttribute(name = "id")
    @XmlID
    public String getIdAsString() {
        return String.valueOf(id);
    }

    public void setIdAsString(String xmlId) {
        id = Long.parseLong(xmlId);
    }
}

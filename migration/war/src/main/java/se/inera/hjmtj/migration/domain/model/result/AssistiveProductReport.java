/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.result;

import lombok.Builder;
import lombok.Value;

import java.io.Serializable;
import java.util.Set;

/**
 * A report on an assistive product from H2.
 */
@Builder
@Value
public class AssistiveProductReport implements Serializable, Comparable<AssistiveProductReport> {

    private final Long id;

    private final String organizationName;

    private final boolean existsInLime;

    private final long catalogueUniqueNumber;

    private final String searchableNumber;

    private boolean isISO9999;

    private Type type;

    private final Set<Long> requiresIds;

    private final boolean existsInAgreement;

    private final boolean existsInGP;

    private final boolean existsInAssortment;

    @Override
    public int compareTo(AssistiveProductReport o) {
        if(requiresIds.contains(o.getId())){
            return 1;
        } else if(o.requiresIds.contains(id)){
            return -1;
        }

        return 0;
    }

    public enum Type {
        PRODUCT,
        ARTICLE
    }

}

/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.application.services;

import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.model.api.BusinessLevelAPI;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjmtj.migration.domain.exceptions.save.BusinessLevelSaveFailed;
import se.inera.hjmtj.migration.domain.exceptions.save.CustomerSaveFailed;
import se.inera.hjmtj.migration.domain.exceptions.save.UserAccountSaveFailed;
import se.inera.hjmtj.migration.domain.model.Indexed;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.domain.model.organization.entities.Customer;
import se.inera.hjmtj.migration.domain.model.product.api.Account;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjCustomerRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjUserRepository;
import se.inera.hjmtj.migration.infra.organizations.CustomerRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import java.util.LinkedList;
import java.util.List;

@Slf4j
@ApplicationScoped
@Path("/customers")
public class CustomersService {

    @Inject
    private CustomerRepository customerRepository;

    @Inject
    private HjmtjCustomerRepository hjmtjCustomerRepository;

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    @Inject
    private HjmtjUserRepository hjmtjUserRepository;

    @GET
    @Path("/")
    public List<IdCustomerOrganizationAPI> migrateCustomers() {
        log.info("Started migrating customers...");

        LinkedList<IdCustomerOrganizationAPI> savedCustomers = new LinkedList<>();

        for (Indexed<Customer> indexedCustomer : customerRepository.rootOrganizations()) {
            try {
                IdCustomerOrganizationAPI organizationAPI = saveCustomer(indexedCustomer);
                savedCustomers.add(organizationAPI);
            } catch (CustomerSaveFailed | BusinessLevelSaveFailed e) {
                MigrationError migrationError = new MigrationError();
                migrationError.setClassifier(Classifier.CUSTOMER);
                migrationError.setMessage(e.getMessage());
                migrationError.setOrganizationName(indexedCustomer.value()
                                                                  .getOrganizationName());
                migrationErrorRepository.add(migrationError);
            } catch (Exception e) {
                log.error("{}", e);
            }
        }

        log.info("Done migrating customers!");

        return savedCustomers;
    }

    private IdCustomerOrganizationAPI saveCustomer(Indexed<Customer> customerIndexed) throws CustomerSaveFailed, BusinessLevelSaveFailed {
        Customer customer = customerIndexed.value();
        OrganizationAPI savedOrganization = hjmtjCustomerRepository.save(customer);

        List<BusinessLevelAPI> businessLevelAPIList = new LinkedList<>();
        for (Indexed<Customer> indexedBusinessAreaCustomer : customerRepository.businessAreas(customer)) {
            Customer businessAreaCustomer = indexedBusinessAreaCustomer.value();
            BusinessLevelAPI businessLevelAPI = businessAreaCustomer.toApi(savedOrganization);
            businessLevelAPIList.add(hjmtjCustomerRepository.save(businessLevelAPI));
        }

        log.debug("Saved {}", customer.getOrganizationName());

        return new IdCustomerOrganizationAPI(customerIndexed.id(), customer, savedOrganization, businessLevelAPIList);
    }

    public void migrateTestUsers(List<IdCustomerOrganizationAPI> customers) {
        for (IdCustomerOrganizationAPI idCustomerOrganizationAPI : customers) {
            try {
                Account account = Account.customer(idCustomerOrganizationAPI.getId(), idCustomerOrganizationAPI.getCustomer()
                                                                                                               .getSecret());
                hjmtjUserRepository.saveUser(account, idCustomerOrganizationAPI.getOrganizationAPI(), idCustomerOrganizationAPI.getBusinessLevels());
                hjmtjUserRepository.setPassword(account.name(), account.secret().getSecretValue());
            } catch (UserAccountSaveFailed e) {
                MigrationError migrationError = new MigrationError();
                migrationError.setClassifier(Classifier.CUSTOMER);
                migrationError.setMessage(e.getMessage());
                migrationError.setOrganizationName(idCustomerOrganizationAPI.getCustomer()
                                                                            .getOrganizationName());
                migrationErrorRepository.add(migrationError);
            } catch (Exception e) {
                log.error("{}", e);
            }
        }
    }

    @Value(staticConstructor = "of")
    public static class IdCustomerOrganizationAPI {

        private final Long id;

        private final Customer customer;

        private final OrganizationAPI organizationAPI;

        private final List<BusinessLevelAPI> businessLevels;
    }
}

/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.exceptions.save;

import se.inera.hjmtj.migration.domain.exceptions.MigrationException;
import se.inera.hjmtj.migration.domain.model.product.entities.Product;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductCategory;

import javax.ejb.ApplicationException;

@ApplicationException
public class NotAnISO9999Article extends MigrationException {

    public NotAnISO9999Article(Product product) {
        super("Article with catalogue number = " + product.getVgrProdNo() + " is not ISO-9999.");
    }

    private static String stringValue(ProductCategory productCategory) {
        StringBuilder sb = new StringBuilder();

        if (productCategory.getCode() != null) {
            sb.append(productCategory.getCode());
            sb.append(" ");
        }

        sb.append(productCategory.getName());

        return sb.toString();
    }
}

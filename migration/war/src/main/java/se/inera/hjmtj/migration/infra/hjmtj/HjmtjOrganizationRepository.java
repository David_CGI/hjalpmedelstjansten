/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package se.inera.hjmtj.migration.infra.hjmtj;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.organization.controller.BusinessLevelController;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationMapper;
import se.inera.hjalpmedelstjansten.model.api.BusinessLevelAPI;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjmtj.migration.domain.exceptions.save.BusinessLevelSaveFailed;
import se.inera.hjmtj.migration.domain.model.organization.api.MigratedOrganizationStatistics;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.math.BigInteger;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Slf4j
@Stateless
public class HjmtjOrganizationRepository {

    public static final int SWEDEN = 120;

    private static final String DELIVERY = "DELIVERY";

    private static final String VISIT = "VISIT";

    @PersistenceContext(unitName = "HjmtjPU")
    private EntityManager em;

    @Inject
    private OrganizationController organizationController;

    @EJB
    private BusinessLevelController businessLevelController;

    public Optional<Organization> findOrganisationByGln(String gln) {
        Objects.requireNonNull(gln, "gln must be non-null");
        log.trace("findByGln {}", gln);
        try {
            Organization org = (Organization) em.createQuery("SELECT o FROM Organization o WHERE o.gln = :gln")
                                                .setParameter("gln", gln)
                                                .getSingleResult();
            return Optional.of(org);
        } catch (NoResultException ex) {
            return Optional.empty();
        }
    }

    public List<MigratedOrganizationStatistics> statistics() {
        List<Object[]> resultList = em.createNativeQuery("SELECT\n" + "  r.organizationName,\n" + "  (r.Product_H +\n"
                + "   r.Product_T +\n" + "   r.Article_H +\n" + "   r.Article_T +\n" + "   r.Article_R +\n" + "    r.Article_Tj +\n"
                + "    r.Article_I) AS Total,\n" + "  r.Product_H,\n" + "  r.Product_T,\n" + "  r.Article_H,\n" + "  r.Article_T,\n"
                + "  r.Article_R,\n" + "  r.Article_Tj,\n" + "  r.Article_I\n" + "FROM (\n" + "       SELECT\n"
                + "         o.organizationName,\n" + "         (SELECT COUNT(*)\n" + "          FROM Product p\n"
                + "            JOIN Category c on p.categoryId = c.uniqueId\n"
                + "          WHERE p.organizationId = o.uniqueId AND c.articleType = 'H')  AS Product_H,\n" + "         (SELECT COUNT(*)\n"
                + "          FROM Product p\n" + "            JOIN Category c on p.categoryId = c.uniqueId\n"
                + "          WHERE p.organizationId = o.uniqueId AND c.articleType = 'T')  AS Product_T,\n" + "         (SELECT COUNT(*)\n"
                + "          FROM Article a\n" + "            LEFT JOIN Product p on a.basedOnProductId = p.uniqueId\n"
                + "            JOIN Category c on a.categoryId = c.uniqueId OR p.categoryId = c.uniqueId\n"
                + "          WHERE a.organizationId = o.uniqueId AND c.articleType = 'H')  AS Article_H,\n" + "         (SELECT COUNT(*)\n"
                + "          FROM Article a\n" + "            LEFT JOIN Product p on a.basedOnProductId = p.uniqueId\n"
                + "            JOIN Category c on a.categoryId = c.uniqueId OR p.categoryId = c.uniqueId\n"
                + "          WHERE a.organizationId = o.uniqueId AND c.articleType = 'T')  AS Article_T,\n" + "         (SELECT COUNT(*)\n"
                + "          FROM Article a\n" + "            JOIN Category c on a.categoryId = c.uniqueId\n"
                + "          WHERE a.organizationId = o.uniqueId AND c.articleType = 'R')  AS Article_R,\n" + "         (SELECT COUNT(*)\n"
                + "          FROM Article a\n" + "            JOIN Category c on a.categoryId = c.uniqueId\n"
                + "          WHERE a.organizationId = o.uniqueId AND c.articleType = 'Tj') AS Article_Tj,\n" + "         (SELECT COUNT(*)\n"
                + "          FROM Article a\n" + "            JOIN Category c on a.categoryId = c.uniqueId\n"
                + "          WHERE a.organizationId = o.uniqueId AND c.articleType = 'I')  AS Article_I\n" + "       FROM Organization o\n"
                + "       WHERE o.organizationType = 'SUPPLIER'\n" + "     ) r\n" + "ORDER BY Total DESC;\n")
                                      .getResultList();
        return resultList.stream()
                         .map(MigratedOrganizationStatistics::new)
                         .collect(toList());
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    private long size() {
        BigInteger result = (BigInteger) em.createNativeQuery("SELECT COUNT(*) FROM Organization")
                                           .setMaxResults(1)
                                           .getSingleResult();
        return result.longValue();
    }

    public BusinessLevelAPI save(BusinessLevelAPI businessLevel) throws BusinessLevelSaveFailed {
        try {
            OrganizationAPI organization = businessLevel.getOrganization();
            return businessLevelController.addBusinessLevelToOrganization(organization.getId(), businessLevel);
        } catch (Exception e) {
            throw new BusinessLevelSaveFailed(businessLevel, e);
        }
    }

    public OrganizationAPI save(OrganizationAPI organization) throws HjalpmedelstjanstenValidationException {
        return organizationController.createOrganization(organization, new UserAPI(), "", null);
    }

    public BusinessLevelAPI findBusinessLevelByName(String name) {
        try {
            Object[] array = (Object[]) em
                    .createNativeQuery("SELECT bl.uniqueId,bl.name,bl.organizationId FROM BusinessLevel bl WHERE bl.name = :name")
                    .setParameter("name", name)
                    .getSingleResult();
            return mapBusinessLevel(array);
        } catch (NoResultException ex) {
            return null;
        }
    }

    private BusinessLevelAPI mapBusinessLevel(Object[] array) {
        BusinessLevelAPI businessLevelAPI = new BusinessLevelAPI();
        businessLevelAPI.setId(((BigInteger) array[0]).longValue());
        businessLevelAPI.setName((String) array[1]);
        OrganizationAPI organization = new OrganizationAPI();
        organization.setId(((BigInteger) array[2]).longValue());
        businessLevelAPI.setOrganization(organization);
        return businessLevelAPI;
    }

    public OrganizationAPI findById(long organizationId) {
        Organization organization = em.find(Organization.class, organizationId);
        return organization != null ? OrganizationMapper.map(organization, false) : null;
    }
}

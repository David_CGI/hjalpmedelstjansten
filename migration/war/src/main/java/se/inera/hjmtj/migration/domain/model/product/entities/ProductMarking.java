/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.product.entities;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import se.inera.hjmtj.migration.domain.model.Reference;
import se.inera.hjmtj.migration.domain.xml.TrimStringAdapter;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceUnit;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;

@PersistenceUnit(unitName = "local")
@Embeddable
@XmlRootElement(name = "_x0031_7fa973736524d658b7b944561195907")
@RequiredArgsConstructor
@Getter
@Setter
public class ProductMarking implements Serializable {

    @XmlElement(name = "CEmark")
    private Boolean ceMark;

    @XmlJavaTypeAdapter(TrimStringAdapter.class)
    @XmlElement(name = "isoattr_3007")
    private String isoattr_3007;

    @XmlJavaTypeAdapter(TrimStringAdapter.class)
    @XmlElement(name = "isoattr_3008")
    private String isoattr_3008;

    @ManyToOne(fetch = FetchType.EAGER)
    private ProductMarkingDirective productMarkingDirective;

    @ManyToOne(fetch = FetchType.EAGER)
    private ProductMarkingStandard productMarkingStandard;

    @XmlElement(name = "Directive")
    public void setXmlDirective(Reference<ProductMarkingDirective> reference) {
        productMarkingDirective = reference.getReference();
    }

    @XmlElement(name = "Standard")
    public void setXmlStandard(Reference<ProductMarkingStandard> reference) {
        productMarkingStandard = reference.getReference();
    }
}

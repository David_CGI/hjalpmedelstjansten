/*
 *
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 *  Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package se.inera.hjmtj.migration.application.services;

import lombok.extern.slf4j.Slf4j;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.media.ArticleMediaDocument;
import se.inera.hjalpmedelstjansten.model.entity.media.ArticleMediaImage;
import se.inera.hjalpmedelstjansten.model.entity.media.MediaDocument;
import se.inera.hjalpmedelstjansten.model.entity.media.MediaImage;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.domain.exceptions.save.CloudfrontURLTooLong;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.domain.model.image.entity.ImageMigration;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.domain.model.result.AssistiveProductReport;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjArticleRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjCvDocumentTypeRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjImageMigrationRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjProductRepository;
import se.inera.hjmtj.migration.infra.images.ImageMigrationRepository;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.concurrent.TimeUnit.HOURS;
import static java.util.function.Function.identity;

@Slf4j
@Stateless
@Interceptors({PerformanceLogInterceptor.class})
@TransactionTimeout(unit = HOURS, value = 5)
@TransactionManagement(TransactionManagementType.BEAN)
public class S3MediaService {

    @Inject
    private HjmtjProductRepository hjmtjProductRepository;

    @Inject
    private HjmtjArticleRepository hjmtjArticleRepository;

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    @Inject
    private HjmtjCvDocumentTypeRepository hjmtjCvDocumentTypeRepository;

    @Inject
    private ImageMigrationRepository imageMigrationRepository;

    @Inject
    private HjmtjImageMigrationRepository hjmtjImageMigrationRepository;

    public void migrateSupplierImages(List<Supplier> suppliers) {
        int mediaSaved = 0;

        for (Supplier supplier : suppliers) {
            List<ImageMigration> listOfImagesToMigrate = imageMigrationRepository.findAllImageMigrationBasedOnSupplier(supplier.getId());
            mediaSaved = saveSupplierMedia(mediaSaved, supplier, listOfImagesToMigrate);

            log.info("Media total {}", mediaSaved);
        }
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public int saveSupplierMedia(int mediaSaved, Supplier supplier, List<ImageMigration> listOfImagesToMigrate) {
        // Fetch all assistive products for supplier
        Map<Long, Product> catalogueUniqueNumberToProduct = hjmtjProductRepository.findByGLN(supplier.getGln())
                                                                                  .stream()
                                                                                  .collect(Collectors.toMap(a -> a.getCatalogUniqueNumber()
                                                                                                                  .getUniqueId(), identity()));
        Map<Long, Article> catalogueUniqueNumberToArticle = new HashMap<>();
        Map<Long, List<Article>> productCatalogueUniqueNumberToArticlesBasedOn = new HashMap<>();

        for (Map.Entry<Article, Long> entry : hjmtjArticleRepository.findByGLN(supplier.getGln())
                                                                    .entrySet()) {
            Article article = entry.getKey();
            Long productCatalogueUniqueId = entry.getValue();
            catalogueUniqueNumberToArticle.put(article.getCatalogUniqueNumber()
                                                      .getUniqueId(), article);
            productCatalogueUniqueNumberToArticlesBasedOn.computeIfAbsent(productCatalogueUniqueId, nil -> new LinkedList<>())
                                                         .add(article);
        }


        for (ImageMigration image : listOfImagesToMigrate) {
            Long catalogueUniqueNumber = image.getCatalogueUniqueNumber();

            try {
                AssistiveProductReport.Type type = image.getType();
                if (type.equals(AssistiveProductReport.Type.ARTICLE)) {
                    Article article = catalogueUniqueNumberToArticle.get(catalogueUniqueNumber);

                    // Do not migrate inherited media as they will be migrated when the product is
                    if (article != null && !image.isInherited()) {
                        migrateArticleMedia(image, article);
                    }
                } else {
                    Product product = catalogueUniqueNumberToProduct.get(catalogueUniqueNumber);

                    if (product != null) {
                        migrateProductMedia(image, product, productCatalogueUniqueNumberToArticlesBasedOn.getOrDefault(catalogueUniqueNumber, Collections.emptyList()));
                    }
                }

                mediaSaved++;
            } catch (CloudfrontURLTooLong e) {
                MigrationError migrationError = new MigrationError();
                migrationError.setClassifier(Classifier.HJALPMEDEL_IMAGE_URL);
                migrationError.setOrganizationName(supplier.getName());
                migrationError.setCatalogueUniqueNumber(catalogueUniqueNumber);
                migrationError.setMessage("CDN URL length > 255 characters: " + e.getUrl());
                migrationErrorRepository.add(migrationError);
            } catch (Exception e) {
                log.error("", e);
            }
        }

        return mediaSaved;
    }

    private void migrateArticleMedia(ImageMigration imageToMigrate, Article article) throws CloudfrontURLTooLong {
        if (imageToMigrate.isDocumentImage()) {
            migrateArticleDocument(imageToMigrate, article);
        } else {
            migrateArticleImage(imageToMigrate, article);
        }
    }

    private void migrateProductMedia(ImageMigration imageToMigrate, Product product, List<Article> articlesBasedOnProduct) throws CloudfrontURLTooLong {
        if (imageToMigrate.isDocumentImage()) {
            MediaDocument mediaDoc = imageToMigrate.toMediaDocument(hjmtjCvDocumentTypeRepository);
            mediaDoc.setProduct(product);
            hjmtjImageMigrationRepository.persist(mediaDoc);

            for (Article article : articlesBasedOnProduct) {
                if (imageMigrationRepository.inherits(article.getCatalogUniqueNumber(), imageToMigrate)) {
                    ArticleMediaDocument articleMediaDocument = new ArticleMediaDocument();
                    articleMediaDocument.setInherited(true);
                    articleMediaDocument.setMediaDocument(mediaDoc);
                    articleMediaDocument.setArticle(article);
                    hjmtjImageMigrationRepository.persist(articleMediaDocument);
                }
            }
        } else {
            MediaImage mediaImage = imageToMigrate.toMediaImage();
            mediaImage.setProduct(product);
            hjmtjImageMigrationRepository.persist(mediaImage);

            for (Article article : articlesBasedOnProduct) {
                if (imageMigrationRepository.inherits(article.getCatalogUniqueNumber(), imageToMigrate)) {
                    ArticleMediaImage articleMediaImage = toArticleMediaImage(article, mediaImage, true);
                    hjmtjImageMigrationRepository.persist(articleMediaImage);
                }
            }
        }
    }

    private void migrateArticleDocument(ImageMigration imageToMigrate, Article article) throws CloudfrontURLTooLong {
        MediaDocument mediaDoc = imageToMigrate.toMediaDocument(hjmtjCvDocumentTypeRepository);
        hjmtjImageMigrationRepository.persist(mediaDoc);

        ArticleMediaDocument articleMediaDocument = new ArticleMediaDocument();
        articleMediaDocument.setInherited(imageToMigrate.isInherited());
        articleMediaDocument.setMediaDocument(mediaDoc);
        articleMediaDocument.setArticle(article);
        hjmtjImageMigrationRepository.persist(articleMediaDocument);
    }

    private void migrateArticleImage(ImageMigration imageToMigrate, Article article) throws CloudfrontURLTooLong {
        MediaImage mediaImage = imageToMigrate.toMediaImage();
        hjmtjImageMigrationRepository.persist(mediaImage);

        createArticleMediaImage(article, mediaImage, imageToMigrate.isInherited());
    }

    private ArticleMediaImage toArticleMediaImage(Article article, MediaImage mediaImage, boolean inherited) {
        ArticleMediaImage articleMediaImage = new ArticleMediaImage();
        articleMediaImage.setInherited(inherited);
        articleMediaImage.setMediaImage(mediaImage);
        articleMediaImage.setArticle(article);
        return articleMediaImage;
    }

    private void createArticleMediaImage(Article article, MediaImage mediaImage, boolean inherited) {
        ArticleMediaImage articleMediaImage = toArticleMediaImage(article, mediaImage, inherited);
        hjmtjImageMigrationRepository.persist(articleMediaImage);
    }

}

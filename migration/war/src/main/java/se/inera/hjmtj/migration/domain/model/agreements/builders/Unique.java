/*
 *
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 *  Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.agreements.builders;

import lombok.Value;
import lombok.experimental.Accessors;

import java.util.Objects;
import java.util.function.Function;

@Value
@Accessors(fluent = true)
public class Unique<T> {

    private final Function<T, Object> function;

    private final T data;

    public Unique(T data, Function<T, Object> function) {
        this.data = data;
        this.function = function;
    }

    public static <T> Unique<T> of(T data, Function<T, Object> hashCodeFunction) {
        return new Unique<>(data, hashCodeFunction);
    }

    @Override
    public int hashCode() {
        return Objects.hash(function.apply(data)
                                    .hashCode());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Unique<T> that = (Unique<T>) o;
        return Objects.equals(function.apply(data), function.apply(that.data()));
    }

}

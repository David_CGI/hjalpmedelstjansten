package se.inera.hjmtj.migration.domain.model.image.entity;

import lombok.Data;
import se.inera.hjalpmedelstjansten.model.entity.media.MediaDocument;
import se.inera.hjalpmedelstjansten.model.entity.media.MediaImage;
import se.inera.hjmtj.migration.domain.exceptions.save.CloudfrontURLTooLong;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.domain.model.result.AssistiveProductReport;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjCvDocumentTypeRepository;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PersistenceUnit;
import javax.persistence.Table;

@PersistenceUnit(unitName = "local")
@Entity
@NamedQueries({@NamedQuery(name = ImageMigration.INHERITS, query = "select i " +
        "from ImageMigration i " +
        "where i.catalogueUniqueNumber = :catalogueUniqueNumber AND " +
        "i.inherited = true AND " +
        "i.url = :url AND " +
        "i.mainImage = :mainImage "),
        @NamedQuery(name = ImageMigration.COUNT, query = "SELECT COUNT(a) FROM ImageMigration a")
})
@Data
@Table(indexes = {
        @Index(columnList = "catalogueUniqueNumber,inherited,url")
})
public class ImageMigration {

    public static final String COUNT = "ImageMigration.COUNT";

    public static final String INHERITS = "ImageMigration.INHERITS";

    private static final String mediaBaseUrl = System.getenv("MEDIA_BASE_URL");

    private String description;

    @Column(length = 500)
    private String filePath;

    @Column(length = 300)
    private String fileName;

    private Long catalogueUniqueNumber;

    private String gln;

    private boolean mainImage = false;

    private boolean documentImage = false;

    private String originalContentUrl;

    private String url;

    private String documentType;

    private String fileExtension;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long imageId;

    @OneToOne
    private Supplier supplier;

    private String altText;

    private boolean inherited;

    @Enumerated(EnumType.STRING)
    private AssistiveProductReport.Type type;

    public MediaImage toMediaImage() throws CloudfrontURLTooLong {
        String altText = getAltText();
        String shortenedAltText = altText == null ? null : altText.substring(0, Math.min(altText.length(), 255));

        String cloudFrontMediaUrl = getCloudFrontMediaUrl(getFilePath());
        if (cloudFrontMediaUrl.length() > 255) {
            throw new CloudfrontURLTooLong(cloudFrontMediaUrl, getCatalogueUniqueNumber(), cloudFrontMediaUrl);
        }

        MediaImage mediaImage = new MediaImage();
        mediaImage.setAlternativeText(shortenedAltText);
        mediaImage.setDescription(getDescription());
        mediaImage.setMainImage(isMainImage());
        mediaImage.setFileName(getFileName());
        mediaImage.setExternalKey(getFilePath());
        mediaImage.setUrl(cloudFrontMediaUrl);

        return mediaImage;
    }

    private String getCloudFrontMediaUrl(String externalKey) {
        return mediaBaseUrl + (mediaBaseUrl.endsWith("/") ? "" : "/") + externalKey;
    }

    public MediaDocument toMediaDocument(HjmtjCvDocumentTypeRepository hjmtjCvDocumentTypeRepository) throws CloudfrontURLTooLong {
        String cloudFrontMediaUrl = getCloudFrontMediaUrl(getFilePath());
        if (cloudFrontMediaUrl.length() > 255) {
            throw new CloudfrontURLTooLong(cloudFrontMediaUrl, getCatalogueUniqueNumber(), cloudFrontMediaUrl);
        }

        MediaDocument mediaDocument = new MediaDocument();
        mediaDocument.setDescription(getDescription());
        mediaDocument.setExternalKey(getFilePath());
        mediaDocument.setOriginalUrl(getOriginalContentUrl());
        mediaDocument.setUrl(cloudFrontMediaUrl);
        mediaDocument.setFileName(getFileName());
        mediaDocument.setFileType(resolveDocFileType(this));

        String documentType = getDocumentType();

        // HJAL-1554
        if (documentType != null &&
                documentType.trim()
                            .equalsIgnoreCase("Rekonditionerings, tvätt-rengöringsanvisning")) {
            documentType = "Rekonditionerings-, tvätt-rengöringsanvisning";
        }

        hjmtjCvDocumentTypeRepository.findByValue(documentType)
                                     .ifPresent(mediaDocument::setDocumentType);

        return mediaDocument;
    }

    private MediaDocument.FileType resolveDocFileType(ImageMigration imageToMigrate) {
        String fileExtension = imageToMigrate.getFileExtension();

        if (fileExtension.equalsIgnoreCase(".pdf")) {
            return MediaDocument.FileType.PDF;
        } else if (fileExtension.equalsIgnoreCase(".xls") ||
                fileExtension.equalsIgnoreCase(".xlsx")) {
            return MediaDocument.FileType.EXCEL;
        } else if (fileExtension.equalsIgnoreCase(".doc") ||
                fileExtension.equalsIgnoreCase(".docx")) {
            return MediaDocument.FileType.WORD;
        }

        // Default to PDF as field is non-nullable
        return MediaDocument.FileType.PDF;
    }
}

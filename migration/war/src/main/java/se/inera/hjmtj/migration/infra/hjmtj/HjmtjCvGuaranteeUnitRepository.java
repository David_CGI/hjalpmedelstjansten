/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.hjmtj;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.model.api.cv.CVGuaranteeUnitAPI;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.util.function.Function.identity;

@Slf4j
@Singleton
public class HjmtjCvGuaranteeUnitRepository {

    private final Map<String, CVGuaranteeUnitAPI> codes = new HashMap<>();

    private CVGuaranteeUnitAPI days;

    @PersistenceContext(unitName = "HjmtjPU")
    private EntityManager em;

    public boolean isEmpty() {
        return size() == 0;
    }

    private long size() {
        return ((BigInteger) em.createNativeQuery("SELECT COUNT(*) FROM hjmtj.CVGuaranteeUnit ")
                               .setMaxResults(1)
                               .getSingleResult()).longValue();
    }

    public void initializeCache() {
        try {
            List<Object[]> rs = em.createNativeQuery("SELECT uniqueId,code FROM CVGuaranteeUnit")
                                  .getResultList();
            codes.putAll(rs.stream()
                           .map(this::map)
                           .collect(Collectors.toMap(CVGuaranteeUnitAPI::getCode, identity())));

            days = findByCode("02");
        } catch (Exception t) {
            log.error("{}", t);
        }
    }

    public CVGuaranteeUnitAPI findByCode(String code) {
        Objects.requireNonNull(code, "code must be non-null");

        return codes.get(code);
    }

    private CVGuaranteeUnitAPI map(Object[] arr) {
        CVGuaranteeUnitAPI cvOrderUnitAPI = new CVGuaranteeUnitAPI();
        cvOrderUnitAPI.setId(((BigInteger) arr[0]).longValue());
        cvOrderUnitAPI.setCode((String) arr[1]);

        return cvOrderUnitAPI;
    }

    public CVGuaranteeUnitAPI days() {
        return days;
    }

}

package se.inera.hjmtj.migration.httpclient;

import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.net.ssl.SSLContext;

import org.apache.http.HeaderElement;
import org.apache.http.HeaderElementIterator;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Singleton
public class HttpClientConfig {
	private static final int CONNECT_TIMEOUT = 60000;
	private static final int REQUEST_TIMEOUT = 30000;
	private static final int SOCKET_TIMEOUT = 60000;
	private static final int MAX_TOTAL_CONNECTIONS = 50;
	private static final int DEFAULT_KEEP_ALIVE_TIME_MILLIS = 20 * 1000;
	private static final int CLOSE_IDLE_CONNECTION_WAIT_TIME_SECS = 10;
	private PoolingHttpClientConnectionManager connectionMgr;

	@PostConstruct
	public void init() {
		connectionMgr = poolingConnectionManager();
	}

	public PoolingHttpClientConnectionManager poolingConnectionManager() {
		TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;
		SSLContext sslcontext = SSLContexts.createSystemDefault();
		Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
			.register("http", PlainConnectionSocketFactory.INSTANCE).register("https", new SSLConnectionSocketFactory(sslcontext)).build();
		PoolingHttpClientConnectionManager poolingConnectionManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
		poolingConnectionManager.setValidateAfterInactivity(2000);
		poolingConnectionManager.setMaxTotal(MAX_TOTAL_CONNECTIONS);
		idleConnectionMonitor(poolingConnectionManager);
		return poolingConnectionManager;
	}

	public ConnectionKeepAliveStrategy connectionKeepAliveStrategy() {
		return new ConnectionKeepAliveStrategy() {
			@Override
			public long getKeepAliveDuration(HttpResponse response, HttpContext context) {
				HeaderElementIterator it = new BasicHeaderElementIterator(response.headerIterator(HTTP.CONN_KEEP_ALIVE));
				while (it.hasNext()) {
					HeaderElement he = it.nextElement();
					String param = he.getName();
					String value = he.getValue();
					if (value != null && param.equalsIgnoreCase("timeout")) {
						return Long.parseLong(value) * 1000;
					}
				}
				return DEFAULT_KEEP_ALIVE_TIME_MILLIS;
			}
		};
	}

	public CloseableHttpClient httpClient() {
		RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(REQUEST_TIMEOUT).setConnectTimeout(CONNECT_TIMEOUT)
			.setSocketTimeout(SOCKET_TIMEOUT).build();
		return HttpClients.custom().setDefaultRequestConfig(requestConfig).setConnectionManager(connectionMgr)
				.setConnectionManagerShared(true).build();
	}


	public Runnable idleConnectionMonitor(final PoolingHttpClientConnectionManager connectionManager) {
		return new Runnable() {
			@Override
			public void run() {
				try {
					if (connectionManager != null) {
						connectionManager.closeExpiredConnections();
						connectionManager.closeIdleConnections(CLOSE_IDLE_CONNECTION_WAIT_TIME_SECS, TimeUnit.SECONDS);
					} else {
					}
				} catch (Exception e) {
				}
			}
		};
	}

	public PoolingHttpClientConnectionManager getConnectionMgr() {
		return connectionMgr;
	}

	public void setConnectionMgr(PoolingHttpClientConnectionManager connectionMgr) {
		this.connectionMgr = connectionMgr;
	}

}

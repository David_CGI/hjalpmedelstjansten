/*
 *
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 *  Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain;

import lombok.Value;
import org.apache.commons.io.FilenameUtils;
import org.apache.tika.mime.MediaType;
import org.apache.tika.mime.MimeType;
import org.apache.tika.mime.MimeTypeException;
import org.apache.tika.mime.MimeTypes;

import java.net.MalformedURLException;
import java.net.URL;

@Value
public class FileExtension {

    private final String fileExtension;

    public static FileExtension fromContentType(String contentType) {
        if(contentType.contains(";")){
            contentType = contentType.split(";")[0];
        }

        // Unsupported and obsolete mime-type
        if(contentType.equalsIgnoreCase("image/x-png")){
            return new FileExtension(".png");
        }
        // Tikka does not know what image/jpg is only image/jpeg
        else if(contentType.equalsIgnoreCase("image/jpg")){
            return new FileExtension(".jpg");
        }

        MimeTypes fullTypes = MimeTypes.getDefaultMimeTypes();
        try {
            MimeType mimeType = fullTypes.forName(contentType);
            String extension = mimeType != null ? mimeType.getExtension() : null;
            return extension != null && extension.trim()
                                                 .length() > 0 ? new FileExtension(extension) : null;
        } catch (MimeTypeException e) {
            return null;
        }
    }

    public static FileExtension fromURL(String urlString) {
        try {
            URL url = new URL(urlString);
            String fileName = url.getFile();
            return fileName != null ? fromFileName(fileName) : null;
        } catch (MalformedURLException e) {
            return null;
        }
    }

    public static FileExtension fromFileName(String fileName) {
        String extension = FilenameUtils.getExtension(fileName);
        return extension != null && extension.trim()
                                             .length() > 0 ? new FileExtension("." + extension) : null;
    }


}

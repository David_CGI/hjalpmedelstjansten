/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain;

import lombok.extern.slf4j.Slf4j;

import javax.interceptor.AroundInvoke;
import javax.interceptor.AroundTimeout;
import javax.interceptor.InvocationContext;

@Slf4j
public class HeapUsageInterceptor {

    private Long lastLogTime;

    @AroundInvoke
    @AroundTimeout
    public Object logHeap(InvocationContext invocationContext) throws Exception {
        try {
            return invocationContext.proceed();
        } finally {
            // Do not log more often that every 10 seconds
            if (lastLogTime == null || (lastLogTime - System.currentTimeMillis()) > 10) {
                log.info("Heap {} / {} mb", (Runtime.getRuntime()
                                                    .totalMemory() / 1000 / 1000), (Runtime.getRuntime()
                                                                                           .maxMemory() / 1000 / 1000));
            }
            lastLogTime = System.currentTimeMillis();
        }
    }

}

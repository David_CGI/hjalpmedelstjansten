/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.agreements.builders;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVGuaranteeUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPreventiveMaintenanceAPI;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelistRow;
import se.inera.hjmtj.migration.domain.DateTimes;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementCalculatedFromBase;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementProductBase;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementRowBase;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementWarrantyBase;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MessageTemplate;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.domain.model.product.ArticleAPINode;
import se.inera.hjmtj.migration.domain.model.product.entities.Product;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;
import se.inera.hjmtj.migration.infra.products.ProductRepository;

import java.util.Objects;

@RequiredArgsConstructor
@Getter
@Setter
@Accessors(fluent = true)
public class PriceListRowAPIBuilder {

    public static final String VISUERA_AKTIV = "Aktiv";

    private AgreementRowBase agreementRow;

    private CVGuaranteeUnitAPI defaultUnit;

    private AgreementPricelistAPI priceList;

    private ProductRepository productRepository;

    private MigrationErrorRepository migrationErrorRepository;

    private OrganizationAPI organizationAPI;

    public AgreementPricelistRowAPI build() {
        Objects.requireNonNull(agreementRow, "agreementRow must be non-null");
        Objects.requireNonNull(defaultUnit, "defaultUnit must be non-null");
        Objects.requireNonNull(priceList, "priceList must be non-null");
        Objects.requireNonNull(migrationErrorRepository, "migrationErrorRepository must be non-null");
        Objects.requireNonNull(productRepository, "productRepository must be non-null");

        AgreementPricelistRowAPI row = new AgreementPricelistRowAPI();
        row.setPrice(agreementRow.getPrice());
        row.setDeliveryTime(agreementRow.getLeadTimeDays());
        setStatus(row);
        row.setValidFrom(DateTimes.toEpochMillis(agreementRow.getValidFrom()));
        setLeastOrderQuantity(row, agreementRow.getMinOrderQuantity());
        setWarranty(row);

        AgreementProductBase agreementArticle = agreementRow.getAgreementArticle();
        Product article = agreementArticle.getArticle();

        if (article == null) {
            if (agreementArticle.getXmlCategories()
                                .isIso9999()) {
                Product product = productRepository.findByVgrProductNumber(agreementArticle.getVgrProdNo());

                if (product == null) {
                    MigrationError migrationError = new MigrationError();
                    migrationError.setSeverity(MigrationError.Severity.ERROR);
                    migrationError.setOrganizationName(organizationAPI.getOrganizationName());
                    migrationError.setCatalogueUniqueNumber(Long.valueOf(agreementArticle.getVgrProdNo()));
                    migrationError.setClassifier(Classifier.PRICELIST_ROW_ARTICLE);
                    migrationError.setAgreementNumber(priceList != null && priceList.getAgreement() != null ? priceList.getAgreement()
                                                                                                                       .getAgreementNumber() : null);
                    migrationError.setPricelistNumber(priceList != null ? priceList.getNumber() : null);
                    migrationError.setMessage(MessageTemplate.ROW_WILL_NOT_BE_MIGRATED_BECAUSE_ARTICLE_WAS_NOT_RECEIVED);
                    migrationErrorRepository.add(migrationError);
                } else {
                    MigrationError migrationError = new MigrationError();
                    migrationError.setSeverity(MigrationError.Severity.ERROR);
                    migrationError.setOrganizationName(organizationAPI.getOrganizationName());
                    migrationError.setCatalogueUniqueNumber(Long.valueOf(agreementArticle.getVgrProdNo()));
                    migrationError.setClassifier(Classifier.PRICELIST_ROW_ARTICLE);
                    migrationError.setAgreementNumber(priceList != null && priceList.getAgreement() != null ? priceList.getAgreement()
                                                                                                                       .getAgreementNumber() : null);
                    migrationError.setPricelistNumber(priceList != null ? priceList.getNumber() : null);
                    migrationError.setMessage(MessageTemplate.ROW_WILL_NOT_BE_MIGRATED_BECAUSE_ARTICLE_HAS_NOT_BEEN_MIGRATED);
                    migrationErrorRepository.add(migrationError);
                }
            }
            // Ignore those that are not ISO9999

            return null;
        }
        if (!article.getXmlCategories()
                    .isIso9999()) {
            MigrationError migrationError = new MigrationError();
            migrationError.setSeverity(MigrationError.Severity.INFO);
            migrationError.setOrganizationName(organizationAPI.getOrganizationName());
            migrationError.setCatalogueUniqueNumber(Long.valueOf(agreementRow.getAgreementArticle()
                                                                             .getVgrProdNo()));
            migrationError.setClassifier(Classifier.PRICELIST_ROW_ARTICLE);
            migrationError.setAgreementNumber(priceList != null && priceList.getAgreement() != null ? priceList.getAgreement()
                                                                                                               .getAgreementNumber() : null);
            migrationError.setPricelistNumber(priceList != null ? priceList.getNumber() : null);
            migrationError.setMessage(MessageTemplate.NOT_ISO9999);
            migrationErrorRepository.add(migrationError);

            return null;
        } else if (article.isNoArticleType()) {
            MigrationError migrationError = new MigrationError();
            migrationError.setSeverity(MigrationError.Severity.WARNING);
            migrationError.setOrganizationName(organizationAPI.getOrganizationName());
            migrationError.setCatalogueUniqueNumber(Long.valueOf(agreementRow.getAgreementArticle()
                                                                             .getVgrProdNo()));
            migrationError.setClassifier(Classifier.PRICELIST_ROW_ARTICLE);
            migrationError.setAgreementNumber(priceList != null && priceList.getAgreement() != null ? priceList.getAgreement()
                                                                                                               .getAgreementNumber() : null);
            migrationError.setPricelistNumber(priceList != null ? priceList.getNumber() : null);
            migrationError.setMessage(MessageTemplate.ARTICLE_WITHOUT_TYPE);
            migrationErrorRepository.add(migrationError);

            return null;
        } else {
            setArticle(row, agreementArticle, article);
        }


        return row;
    }

    public void setStatus(AgreementPricelistRowAPI row) {
        if (agreementRow.getStatus() == null || agreementRow.getStatus()
                                                            .getValue()
                                                            .equals(VISUERA_AKTIV)) {
            row.setStatus(AgreementPricelistRow.Status.ACTIVE.toString());
        } else {
            row.setStatus(AgreementPricelistRow.Status.INACTIVE.toString());
        }
    }

    private void setLeastOrderQuantity(AgreementPricelistRowAPI row, Long minOrderQuantity) {
        if (minOrderQuantity == null || minOrderQuantity == 0) {
            MigrationError migrationError = new MigrationError();
            migrationError.setSeverity(MigrationError.Severity.WARNING);
            migrationError.setOrganizationName(organizationAPI.getOrganizationName());
            migrationError.setCatalogueUniqueNumber(Long.valueOf(agreementRow.getAgreementArticle()
                                                                             .getVgrProdNo()));
            migrationError.setClassifier(Classifier.PRICELIST_ROW_LEAST_ORDER_QUANTITY);
            migrationError.setAgreementNumber(priceList != null && priceList.getAgreement() != null ? priceList.getAgreement()
                                                                                                               .getAgreementNumber() : null);
            migrationError.setPricelistNumber(priceList != null ? priceList.getNumber() : null);
            migrationError.setMessage(MessageTemplate.LEAST_ORDER_QUANTITY_CHANGED, minOrderQuantity, 1);
            migrationErrorRepository.add(migrationError);

            minOrderQuantity = 1L;
        }

        row.setLeastOrderQuantity(minOrderQuantity.intValue());
    }

    public void setWarranty(AgreementPricelistRowAPI row) {
        AgreementWarrantyBase warranty = agreementRow.getWarranty();
        if (warranty != null) {
            setWarrantyQuantity(row, warranty.getNumericValue());
            setWarrantyTerms(row);
            setWarrantyCalculatedFrom(row, warranty.getCalculatedFrom());
        }
    }

    public void setArticle(AgreementPricelistRowAPI row, AgreementProductBase agreementArticle, Product article) {
        ArticleAPINode articleAPI = new ArticleAPINode(null, article.getId());
        articleAPI.setArticleNumber(article.getSupplierProductNumber());
        articleAPI.setCatalogueUniqueNumber(Long.valueOf(article.getVgrProdNo()));
        row.setArticle(articleAPI);
    }

    private void setWarrantyQuantity(AgreementPricelistRowAPI row, Long warrantyQty) {
        if (warrantyQty != null) {
            if (warrantyQty == 0) {
                MigrationError migrationError = new MigrationError();
                migrationError.setClassifier(Classifier.PRICELIST_ROW_WARRANTY_QUANTITY);
                migrationError.setSeverity(MigrationError.Severity.WARNING);
                migrationError.setOrganizationName(organizationAPI.getOrganizationName());
                migrationError.setAgreementNumber(priceList != null && priceList.getAgreement() != null ? priceList.getAgreement()
                                                                                                                   .getAgreementNumber() : null);
                migrationError.setPricelistNumber(priceList != null ? priceList.getNumber() : null);
                migrationError.setMessage(MessageTemplate.WARRANTY_QUANTITY_CHANGED, warrantyQty, 1);
                migrationErrorRepository.add(migrationError);

                warrantyQty = 1L;
            }

            row.setWarrantyQuantity(warrantyQty.intValue());
            row.setWarrantyQuantityUnit(defaultUnit);
        }
    }

    private void setWarrantyTerms(AgreementPricelistRowAPI row) {
        String termsAndConditions = agreementRow.getWarranty()
                                                .getTermsAndConditions();
        if (termsAndConditions != null) {
            String termsAndConditionsShortened = termsAndConditions.substring(0, Math.min(255, termsAndConditions.length()));
            row.setWarrantyTerms(termsAndConditionsShortened);
        }
    }

    private void setWarrantyCalculatedFrom(AgreementPricelistRowAPI row, AgreementCalculatedFromBase calculatedFrom) {
        if (calculatedFrom != null) {
            CVPreventiveMaintenanceAPI preventiveMaintenanceAPI = new CVPreventiveMaintenanceAPI();
            preventiveMaintenanceAPI.setCode(calculatedFrom.getCode());
            row.setWarrantyValidFrom(preventiveMaintenanceAPI);
        }
    }

}

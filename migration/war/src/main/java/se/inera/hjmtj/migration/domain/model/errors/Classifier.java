/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.errors;

import lombok.Value;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Properties;

@Slf4j
@Value
@Accessors(fluent = true)
public class Classifier {

    public static final Classifier CUSTOMER = new Classifier(null, "CUSTOMER");

    public static final Classifier SUPPLIER = new Classifier(null, "SUPPLIER");

    public static final Classifier SUPPLIER_IMAGES = new Classifier(SUPPLIER, "SUPPLIER_IMAGES");

    // Agreement
    public static final Classifier AGREEMENT = new Classifier(null, "AGREEMENT");

    public static final Classifier AGREEMENT_DELIVERY_TIME = new Classifier(AGREEMENT, "AGREEMENT_DELIVERY_TIME");

    public static final Classifier AGREEMENT_WARRANTY_UNIT = new Classifier(AGREEMENT, "AGREEMENT_WARRANTY_UNIT");

    public static final Classifier AGREEMENT_WARRANTY_QUANTITY = new Classifier(AGREEMENT, "AGREEMENT_WARRANTY_QUANTITY");

    // Pricelist
    public static final Classifier PRICELIST = new Classifier(AGREEMENT, "PRICELIST");

    public static final Classifier PRICELIST_VALID_FROM = new Classifier(AGREEMENT, "PRICELIST_VALID_FROM");

    public static final Classifier PRICELIST_ROW = new Classifier(AGREEMENT, "PRICELIST_ROW");

    public static final Classifier PRICELIST_ROW_WARRANTY_QUANTITY = new Classifier(AGREEMENT, "PRICELIST_ROW_WARRANTY_QUANTITY");

    public static final Classifier PRICELIST_ROW_LEAST_ORDER_QUANTITY = new Classifier(AGREEMENT, "PRICELIST_ROW_LEAST_ORDER_QUANTITY");

    public static final Classifier PRICELIST_ROW_ARTICLE = new Classifier(AGREEMENT, "PRICELIST_ROW_ARTICLE");

    // GP
    public static final Classifier GP = new Classifier(null, "GP");

    public static final Classifier GP_DELIVERY_TIME = new Classifier(GP, "GP_DELIVERY_TIME");

    public static final Classifier GP_WARRANTY_UNIT = new Classifier(GP, "GP_WARRANTY_UNIT");

    public static final Classifier GP_WARRANTY_QUANTITY = new Classifier(GP, "GP_WARRANTY_QUANTITY");

    // GP Pricelist
    public static final Classifier GP_PRICELIST = new Classifier(GP, "GP_PRICELIST");

    public static final Classifier GP_PRICELIST_ROW = new Classifier(GP, "GP_PRICELIST_ROW");

    public static final Classifier GP_PRICELIST_ROW_WARRANTY_QUANTITY = new Classifier(GP, "GP_PRICELIST_ROW_WARRANTY_QUANTITY");

    public static final Classifier GP_PRICELIST_ROW_LEAST_ORDER_QUANTITY = new Classifier(GP, "GP_PRICELIST_ROW_LEAST_ORDER_QUANTITY");

    public static final Classifier GP_PRICELIST_ROW_ARTICLE = new Classifier(GP, "GP_PRICELIST_ROW_ARTICLE");

    // Hjalpmedel
    public static final Classifier HJALPMEDEL = new Classifier(null, "HJALPMEDEL");

    public static final Classifier HJALPMEDEL_CATALOGUE_UNIQUE_NUMBER = new Classifier(HJALPMEDEL, "HJALPMEDEL_CATALOGUE_UNIQUE_NUMBER");

    public static final Classifier HJALPMEDEL_FIT = new Classifier(HJALPMEDEL, "HJALPMEDEL_FIT");

    public static final Classifier HJALPMEDEL_BASED_ON = new Classifier(HJALPMEDEL, "HJALPMEDEL_BASED_ON");

    public static final Classifier HJALPMEDEL_REPLACES = new Classifier(HJALPMEDEL, "HJALPMEDEL_REPLACES");

    public static final Classifier HJALPMEDEL_REPLACED_BY = new Classifier(HJALPMEDEL, "HJALPMEDEL_REPLACED_BY");

    public static final Classifier HJALPMEDEL_GTIN13 = new Classifier(HJALPMEDEL, "HJALPMEDEL_GTIN13");

    public static final Classifier HJALPMEDEL_CATEGORY = new Classifier(HJALPMEDEL, "HJALPMEDEL_CATEGORY");

    public static final Classifier HJALPMEDEL_ARTICLE_NUMBER = new Classifier(HJALPMEDEL, "HJALPMEDEL_ARTICLE_NUMBER");

    public static final Classifier HJALPMEDEL_PRODUCT_NUMBER = new Classifier(HJALPMEDEL, "HJALPMEDEL_PRODUCT_NUMBER");

    public static final Classifier HJALPMEDEL_IMAGE = new Classifier(null, "HJALPMEDEL_IMAGE");

    public static final Classifier HJALPMEDEL_IMAGE_URL = new Classifier(HJALPMEDEL_IMAGE, "HJALPMEDEL_IMAGE_URL");

    public static final Classifier HJALPMEDEL_CUSTOMER_UNIQUE = new Classifier(HJALPMEDEL, "HJALPMEDEL_CUSTOMER_UNIQUE");

    public static final Classifier HJALPMEDEL_ORDER_UNIT = new Classifier(HJALPMEDEL, "HJALPMEDEL_ORDER_UNIT"); ;

    // Category-specific property
    public static final Classifier CATEGORY_SPECIFIC_PROPERTY = new Classifier(null, "CATEGORY_SPECIFIC_PROPERTY");

    public static final Classifier CATEGORY_SPECIFIC_PROPERTY_LISTVALUE = new Classifier(CATEGORY_SPECIFIC_PROPERTY, "CATEGORY_SPECIFIC_PROPERTY_LISTVALUE");

    // Assortments
    public static final Classifier ASSORTMENT = new Classifier(null, "ASSORTMENT");

    public static final Classifier ASSORTMENT_NAME = new Classifier(ASSORTMENT, "ASSORTMENT_NAME");

    public static final Classifier ASSORTMENT_CUSTOMER = new Classifier(ASSORTMENT, "ASSORTMENT_CUSTOMER");

    public static final Classifier ASSORTMENT_OWNER = new Classifier(ASSORTMENT, "ASSORTMENT_OWNER");

    public static final Classifier ASSORTMENT_ARTICLE = new Classifier(ASSORTMENT, "ASSORTMENT_ARTICLE");

    public static final Classifier ASSORTMENT_COUNTY = new Classifier(ASSORTMENT, "ASSORTMENT_COUNTY");

    public static final Classifier ASSORTMENT_MUNICIPALITY = new Classifier(ASSORTMENT, "ASSORTMENT_MUNICIPALITY");

    // Users
    public static final Classifier INERA = new Classifier(null, "INERA");

    // Users
    public static final Classifier USER_ACCOUNT = new Classifier(null, "USER_ACCOUNT");

    public static final Classifier USER_ACCOUNT_ROLE = new Classifier(USER_ACCOUNT, "USER_ACCOUNT_ROLE");

    public static final Classifier USER_ACCOUNT_EMAIL = new Classifier(USER_ACCOUNT,"USER_ACCOUNT_EMAIL");

    // Kodverk
    public static final Classifier CV = new Classifier(null, "CV");

    public static final Classifier CV_CE_DIRECTIVE = new Classifier(CV, "CV_CE_DIRECTIVE");

    public static final Classifier CV_CE_STANDARD = new Classifier(CV, "CV_CE_STANDARD");

    public static final Classifier CV_COUNTY = new Classifier(CV, "CV_COUNTY");

    public static final Classifier CV_MUNICIPALITY = new Classifier(CV, "CV_MUNICIPALITY");

    private static Properties PROPERTIES;

    private final Classifier parent;

    private final String propertyKey;

    public Classifier(Classifier parent, String propertyKey) {
        this.parent = parent;
        this.propertyKey = propertyKey;
        Objects.requireNonNull(propertyKey, "key must be non-null");
        Objects.requireNonNull(toString(), "message must be non-null");
    }

    @Override
    public String toString() {
        return properties().getProperty(propertyKey.toUpperCase(), propertyKey);
    }

    private synchronized Properties properties() {
        if (PROPERTIES == null) {
            try {
                PROPERTIES = new Properties();
                InputStream input = Classifier.class.getResourceAsStream("/classifier.properties");
                PROPERTIES.load(new InputStreamReader(input, StandardCharsets.UTF_8));
            } catch (IOException e) {
                log.error("Failed to load resource bundle", e);
            }
        }

        return PROPERTIES;
    }

    public static Classifier field(Classifier parent, String field) {
        return new Classifier(parent, field == null || field.isEmpty() ? "?" : field);
    }

}

/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.hjmtj;


import lombok.extern.slf4j.Slf4j;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementPricelistMapper;
import se.inera.hjalpmedelstjansten.model.api.AgreementAPI;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistAPI;
import se.inera.hjalpmedelstjansten.model.entity.Agreement;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelist;
import se.inera.hjmtj.migration.domain.DateTimes;
import se.inera.hjmtj.migration.domain.MigrationFilter;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MessageTemplate;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

import static java.util.concurrent.TimeUnit.HOURS;

@Slf4j
@Stateless
@TransactionTimeout(unit = HOURS, value = 5)
public class HjmtjPricelistRepository {

    @PersistenceContext(unitName = "HjmtjPU")
    private EntityManager em;

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    @Inject
    private MigrationFilter migrationFilter;

    public SaveResult<Long> save(AgreementAPI agreementAPI, AgreementPricelistAPI pricelistAPI, LocalDateTime created) {
        if (migrationFilter.isDeprecated(agreementAPI, pricelistAPI)) {
            return SaveResult.empty();
        }

        // "Avtal som saknar gäller från på prislista ersätts med 3018-01-01"
        if (pricelistAPI.getValidFrom() == null) {
            LocalDate date = getValidFrom(agreementAPI, pricelistAPI);
            pricelistAPI.setValidFrom(DateTimes.toEpochMillis(date));

            MigrationError migrationError = new MigrationError();
            migrationError.setSeverity(MigrationError.Severity.WARNING);
            migrationError.setOrganizationName(agreementAPI.getSupplierOrganization()
                                                           .getOrganizationName());
            migrationError.setClassifier(Classifier.PRICELIST);
            migrationError.setAgreementNumber(agreementAPI.getAgreementNumber());
            migrationError.setPricelistNumber(pricelistAPI.getNumber());
            migrationError.setMessage(MessageTemplate.PRICELIST_EMPTY_VALIDFROM, date.toString());
            migrationErrorRepository.add(migrationError);
        }

        AgreementPricelist priceList = AgreementPricelistMapper.map(pricelistAPI);
        Agreement agreement = em.getReference(Agreement.class, agreementAPI.getId());
        priceList.setAgreement(agreement);

        // set timestamps
        Long createdMillis = DateTimes.toEpochMillis(created);
        priceList.setCreated(createdMillis != null ? new Date(createdMillis) : Calendar.getInstance()
                                                                                       .getTime());
        priceList.setLastUpdated(createdMillis != null ? new Date(createdMillis) : Calendar.getInstance()
                                                                                           .getTime());

        em.persist(priceList);

        log.info("Saved agreement pricelist {} ", pricelistAPI.getNumber());

        return SaveResult.of(priceList.getUniqueId(), pricelistAPI);
    }

    public LocalDate getValidFrom(AgreementAPI agreementAPI, AgreementPricelistAPI pricelistAPI) {
        // Work-around database constraint "validFromIsUniquePerAgreement"
        // There is no guarantee that the order will be consistent across migration attempts
        int index = agreementAPI.getPricelists()
                                .indexOf(pricelistAPI);

        int month = (int) Math.floor((double) index / 28) + 1;
        int day = (index % 28) + 1;

        LocalDate date = LocalDate.of(3018, month, day);
        log.warn("Null validFrom on agreement {} pricelist {} was set to {}", agreementAPI.getAgreementNumber(), pricelistAPI.getNumber(), date.toString());
        return date;
    }

}

/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.hjmtj;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationMapper;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjmtj.migration.domain.exceptions.save.SupplierSaveFailed;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.domain.model.result.SupplierReport;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.function.Function.identity;

@Slf4j
@Stateless
public class HjmtjSupplierRepository {

    @PersistenceContext(unitName = "HjmtjPU")
    private EntityManager em;

    @Inject
    private HjmtjOrganizationRepository hjmtjOrganizationRepository;

    @Inject
    private HjmtjCountryRepository hjmtjCountryRepository;

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    public boolean isEmpty() {
        return size() == 0;
    }

    public long size() {
        return ((BigInteger) em.createNativeQuery("SELECT COUNT(*) FROM Organization " +
                "WHERE Organization.organizationType = :organizationType")
                               .setParameter("organizationType", Organization.OrganizationType.SUPPLIER.toString())
                               .setMaxResults(1)
                               .getSingleResult()).longValue();
    }

    public Map<String,SupplierReport> supplierReports() {
        List<Object[]> resultList = em.createNativeQuery("select o.organizationName  ,\n" +
                "       count(distinct p.uniqueId),\n" +
                "       count(distinct a.uniqueId) " +
                "from Organization o,\n" +
                "     Article a,\n" +
                "     Product p\n" +
                "WHERE a.organizationId = o.uniqueId\n" +
                "  AND p.organizationId = o.uniqueId\n" +
                "group by a.organizationId\n")
                                      .getResultList();
        return resultList
                .stream()
                .map(SupplierReport::mysql)
                .collect(Collectors.toMap(SupplierReport::getOrganizationName, identity()));
    }

    public OrganizationAPI save(Supplier supplier) throws SupplierSaveFailed {
        try {
            OrganizationAPI organizationApi = supplier.toApi(hjmtjCountryRepository.all());
            return hjmtjOrganizationRepository.save(organizationApi);
        } catch (HjalpmedelstjanstenValidationException e) {
            for (ErrorMessageAPI errorMessageAPI : e.getValidationMessages()) {
                MigrationError migrationError = new MigrationError();
                migrationError.setClassifier(Classifier.field(Classifier.SUPPLIER, errorMessageAPI.getField()));
                migrationError.setMessage(e.getMessage());
                migrationError.setOrganizationName(supplier.getName());
                migrationErrorRepository.add(migrationError);
            }

            throw new SupplierSaveFailed(supplier, e);
        }
    }

    public OrganizationAPI find(Supplier supplier) {
        try {
            Organization organization = em.createQuery("SELECT o FROM Organization o WHERE o.organizationType = :type AND " +
                    "(o.gln = :gln OR " +
                    "o.organizationName = :name) ", Organization.class)
                                          .setParameter("type", Organization.OrganizationType.SUPPLIER)
                                          .setParameter("gln", supplier.getGln())
                                          .setParameter("name", supplier.getName())
                                          .getSingleResult();
            return OrganizationMapper.map(organization, true);
        } catch (NoResultException ex) {
            return null;
        }
    }

}

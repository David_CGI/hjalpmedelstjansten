/*
 *
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 *  Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.application.bootstrap;


import lombok.extern.slf4j.Slf4j;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjmtj.migration.domain.HeapUsageInterceptor;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.domain.model.actors.Actor;
import se.inera.hjmtj.migration.domain.model.actors.ActorOrganization;
import se.inera.hjmtj.migration.domain.model.actors.ActorRole;
import se.inera.hjmtj.migration.domain.model.assortment.Assortment;
import se.inera.hjmtj.migration.domain.xml.ElementContextComparator;
import se.inera.hjmtj.migration.domain.xml.XMLFile;
import se.inera.hjmtj.migration.infra.actors.ActorsRepository;
import se.inera.hjmtj.migration.infra.assortments.AssortmentsRepository;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.concurrent.TimeUnit.MINUTES;

/**
 * Loads assortment owners into H2.
 */
@Slf4j
@Singleton
@TransactionTimeout(unit = MINUTES, value = 30)
@Interceptors({PerformanceLogInterceptor.class, HeapUsageInterceptor.class})
public class LoadAssortmentOwners {

    public static final ElementContextComparator PERSIST_ORDER = new ElementContextComparator(
            Assortment.class,
            ActorOrganization.class,
            Actor.class,
            ActorRole.class);

    private static final String ASSORTMENT_OWNERS_URL_FILE = System.getenv()
                                                                   .getOrDefault("ASSORTMENT_OWNERS_URL_FILE", "<file>");

    @PersistenceContext(unitName = "local")
    private EntityManager em;

    @Inject
    private ActorsRepository actorsRepository;

    @Inject
    private AssortmentsRepository assortmentsRepository;

    public void run() {
        XMLFile xmlFile = new XMLFile(PERSIST_ORDER);

        xmlFile.loadFromFTP(ASSORTMENT_OWNERS_URL_FILE)
               .stream()
               .filter(ActorRole.class::isInstance)
               .map(ActorRole.class::cast)
               .forEach(this::merge);
    }

    private void merge(ActorRole actorRole) {
        ActorRole persistedActorRole = actorsRepository.findByIdentifier(actorRole.getIdentifier());

        if (persistedActorRole != null) {
            Set<Assortment> persistedAssortments = actorRole.getAssortmentsOwned()
                                                            .stream()
                                                            .map(Assortment::getIdentifier)
                                                            .map(assortmentsRepository::findByIdentifier)
                                                            .filter(Objects::nonNull)
                                                            .collect(Collectors.toSet());

            Set<Assortment> persistedAssortmentsOwned = persistedActorRole.getAssortmentsOwned();

            if (persistedAssortmentsOwned.size() != actorRole.getAssortmentsOwned()
                                                             .size()) {
                persistedAssortmentsOwned.addAll(persistedAssortments);
                em.merge(persistedActorRole);

                log.info("Role {} is owner for {} assortments", actorRole.getRoleName(), actorRole.getAssortmentsOwned().size());
            }
        }
    }

}

/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.hjmtj;


import lombok.extern.slf4j.Slf4j;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementMapper;
import se.inera.hjalpmedelstjansten.business.product.controller.CVPreventiveMaintenanceController;
import se.inera.hjalpmedelstjansten.model.api.AgreementAPI;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.api.BusinessLevelAPI;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.entity.Agreement;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelistRow;
import se.inera.hjalpmedelstjansten.model.entity.BusinessLevel;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.UserEngagement;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;
import se.inera.hjmtj.migration.domain.DateTimes;
import se.inera.hjmtj.migration.domain.SupplierGLNCatalogueUniqueNumber;
import se.inera.hjmtj.migration.domain.exceptions.save.PriceListRowArticleNotFound;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.domain.model.product.ArticleAPINode;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.concurrent.TimeUnit.HOURS;

@Slf4j
@Stateless
@TransactionTimeout(unit = HOURS, value = 5)
public class HjmtjAgreementRepository {

    @Inject
    private CVPreventiveMaintenanceController cVPreventiveMaintenanceController;

    @PersistenceContext(unitName = "HjmtjPU")
    private EntityManager em;

    @Inject
    private HjmtjPricelistRepository hjmtjPriceListRepository;

    @Inject
    private HjmtjPricelistRowRepository hjmtjPricelistRowRepository;

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    public void save(AgreementAPI agreementAPI, LocalDateTime created) {
        String supplierName = agreementAPI.getSupplierOrganization()
                                          .getOrganizationName();


        SaveResult<Long> agreementSaveResult = saveAgreement(agreementAPI, created);
        MigrationError.of(Classifier.AGREEMENT, agreementAPI.getSupplierOrganization()
                                                            .getOrganizationName(), agreementSaveResult.errorMessages())
                      .stream()
                      .peek(e -> e.setAgreementNumber(agreementAPI.getAgreementNumber()))
                      .forEach(migrationErrorRepository::add);

        agreementAPI.setId(agreementSaveResult.data());

        for (AgreementPricelistAPI pricelistAPI : agreementAPI.getPricelists()) {
            SaveResult<Long> priceListSaveResult = hjmtjPriceListRepository.save(agreementAPI, pricelistAPI, created);

            if (priceListSaveResult.isEmpty()) {
                continue;
            }

            MigrationError.of(Classifier.PRICELIST, agreementAPI.getSupplierOrganization()
                                                                .getOrganizationName(), priceListSaveResult.errorMessages())
                          .stream()
                          .peek(e -> e.setAgreementNumber(agreementAPI.getAgreementNumber()))
                          .peek(e -> e.setPricelistNumber(pricelistAPI.getNumber()))
                          .forEach(migrationErrorRepository::add);
            pricelistAPI.setId(priceListSaveResult.data());

            for (AgreementPricelistRowAPI rowAPI : pricelistAPI.getRows()) {
                try {
                    SaveResult<Long> rowSaveResult = hjmtjPricelistRowRepository.save(pricelistAPI, rowAPI, created);
                    MigrationError.of(Classifier.PRICELIST_ROW, agreementAPI.getSupplierOrganization()
                                                                            .getOrganizationName(), rowSaveResult.errorMessages())
                                  .stream()
                                  .peek(e -> e.setAgreementNumber(agreementAPI.getAgreementNumber()))
                                  .peek(e -> e.setPricelistNumber(pricelistAPI.getNumber()))
                                  .forEach(migrationErrorRepository::add);
                    rowAPI.setId(rowSaveResult.data());
                } catch (PriceListRowArticleNotFound e) {
                    MigrationError migrationError = new MigrationError();
                    migrationError.setSeverity(MigrationError.Severity.ERROR);
                    migrationError.setOrganizationName(supplierName);
                    migrationError.setClassifier(Classifier.PRICELIST_ROW);
                    migrationError.setAgreementNumber(pricelistAPI.getAgreement() != null ? pricelistAPI.getAgreement()
                                                                                                        .getAgreementNumber() : null);
                    migrationError.setPricelistNumber(pricelistAPI.getNumber());
                    migrationError.setMessage(e.getMessage());

                    if (rowAPI.getArticle() instanceof ArticleAPINode) {
                        migrationError.setCatalogueUniqueNumber(((ArticleAPINode) rowAPI.getArticle()).getCatalogueUniqueNumber());
                    }

                    migrationErrorRepository.add(migrationError);
                }
            }
            log.info("Size = {} rows", pricelistAPI.getRows()
                                                   .size());
        }
    }

    public SaveResult<Long> saveAgreement(AgreementAPI agreementAPI, LocalDateTime created) {
        Agreement agreement = AgreementMapper.map(agreementAPI);

        // add supplier organization
        setCustomer(agreementAPI, agreement);

        // add supplier organization
        setSupplier(agreementAPI, agreement);

        // set business level on customer
        setCustomerBusinessLevel(agreementAPI, agreement);

        // set pricelist approvers
        setAgreementPricelistApprovers(agreementAPI, agreement);

        // set shared with customers
        setSharedWithCustomers(agreementAPI, agreement);

        // set warranty quantity units
        setWarrantyQuantityUnits(agreement, agreementAPI);

        // set warranty valid from
        setWarrantyValidFroms(agreementAPI, agreement);

        // set timestamps
        Long createdMillis = DateTimes.toEpochMillis(created);
        agreement.setCreated(createdMillis != null ? new Date(createdMillis) : Calendar.getInstance()
                                                                                       .getTime());
        agreement.setLastUpdated(createdMillis != null ? new Date(createdMillis) : Calendar.getInstance()
                                                                                           .getTime());

        // save it
        em.persist(agreement);

        log.info("Saved agreement {}, buyer {}, seller {}", agreementAPI.getAgreementNumber(), agreementAPI.getCustomerOrganization()
                                                                                                           .getOrganizationName(), agreementAPI.getSupplierOrganization()
                                                                                                                                               .getOrganizationName());

        return SaveResult.of(agreement.getUniqueId(), agreementAPI);
    }

    private void setCustomer(AgreementAPI agreementAPI, Agreement agreement) {
        Organization supplier = em.getReference(Organization.class, agreementAPI.getCustomerOrganization()
                                                                                .getId());
        agreement.setCustomer(supplier);
    }

    private void setSupplier(AgreementAPI agreementAPI, Agreement agreement) {
        Organization supplier = em.getReference(Organization.class, agreementAPI.getSupplierOrganization()
                                                                                .getId());
        agreement.setSupplier(supplier);
    }

    private void setCustomerBusinessLevel(AgreementAPI agreementAPI, Agreement agreement) {
        if (agreementAPI.getCustomerBusinessLevel() != null) {
            BusinessLevel businessLevel = em.getReference(BusinessLevel.class, agreementAPI.getCustomerBusinessLevel()
                                                                                           .getId());
            agreement.setCustomerBusinessLevel(businessLevel);
        }
    }

    private void setAgreementPricelistApprovers(AgreementAPI agreementAPI, Agreement agreement) {
        LinkedList<UserEngagement> customerPricelistApprovers = new LinkedList<>();
        agreement.setCustomerPricelistApprovers(customerPricelistApprovers);
        for (UserAPI userAPI : agreementAPI.getCustomerPricelistApprovers()) {
            UserEngagement userEngagement = em.getReference(UserEngagement.class, userAPI.getId());
            customerPricelistApprovers.add(userEngagement);
        }
    }

    private void setSharedWithCustomers(AgreementAPI agreementAPI, Agreement agreement) {
        LinkedList<Organization> sharedWithCustomers = new LinkedList<>();
        agreement.setSharedWithCustomers(sharedWithCustomers);
        for (OrganizationAPI organizationAPI : agreementAPI.getSharedWithCustomers()) {
            Organization organization = em.getReference(Organization.class, organizationAPI.getId());
            sharedWithCustomers.add(organization);
        }

        LinkedList<BusinessLevel> sharedWithBusinessLevels = new LinkedList<>();
        agreement.setSharedWithCustomerBusinessLevels(sharedWithBusinessLevels);
        for (BusinessLevelAPI businessLevelAPI : agreementAPI.getSharedWithCustomersBusinessLevels()) {
            BusinessLevel businessLevel = em.getReference(BusinessLevel.class, businessLevelAPI.getId());
            sharedWithBusinessLevels.add(businessLevel);
        }
    }

    private void setWarrantyQuantityUnits(Agreement agreement, AgreementAPI agreementAPI) {
        // H
        if (agreementAPI.getWarrantyQuantityHUnit() != null) {
            CVGuaranteeUnit unit = em.getReference(CVGuaranteeUnit.class, agreementAPI.getWarrantyQuantityHUnit()
                                                                                      .getId());
            agreement.setWarrantyQuantityHUnit(unit);
        }

        // T
        if (agreementAPI.getWarrantyQuantityTUnit() != null) {
            CVGuaranteeUnit unit = em.getReference(CVGuaranteeUnit.class, agreementAPI.getWarrantyQuantityTUnit()
                                                                                      .getId());
            agreement.setWarrantyQuantityTUnit(unit);
        }

        // I
        if (agreementAPI.getWarrantyQuantityIUnit() != null) {
            CVGuaranteeUnit unit = em.getReference(CVGuaranteeUnit.class, agreementAPI.getWarrantyQuantityIUnit()
                                                                                      .getId());
            agreement.setWarrantyQuantityIUnit(unit);
        }

        // R
        if (agreementAPI.getWarrantyQuantityRUnit() != null) {
            CVGuaranteeUnit unit = em.getReference(CVGuaranteeUnit.class, agreementAPI.getWarrantyQuantityRUnit()
                                                                                      .getId());
            agreement.setWarrantyQuantityRUnit(unit);
        }

        // TJ
        if (agreementAPI.getWarrantyQuantityTJUnit() != null) {
            CVGuaranteeUnit unit = em.getReference(CVGuaranteeUnit.class, agreementAPI.getWarrantyQuantityTJUnit()
                                                                                      .getId());
            agreement.setWarrantyQuantityTJUnit(unit);
        }
    }

    private void setWarrantyValidFroms(AgreementAPI agreementAPI, Agreement agreement) {
        // warranty valid from
        if (agreementAPI.getWarrantyValidFromH() != null) {
            CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(agreementAPI.getWarrantyValidFromH()
                                                                                                                     .getCode());
            if (preventiveMaintenance != null) {
                agreement.setWarrantyValidFromH(preventiveMaintenance);
            }
        }
        if (agreementAPI.getWarrantyValidFromT() != null) {
            CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(agreementAPI.getWarrantyValidFromT()
                                                                                                                     .getCode());
            if (preventiveMaintenance != null) {
                agreement.setWarrantyValidFromT(preventiveMaintenance);
            }
        }
        if (agreementAPI.getWarrantyValidFromI() != null) {
            CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(agreementAPI.getWarrantyValidFromI()
                                                                                                                     .getCode());
            if (preventiveMaintenance != null) {
                agreement.setWarrantyValidFromI(preventiveMaintenance);
            }
        }
        if (agreementAPI.getWarrantyValidFromR() != null) {
            CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(agreementAPI.getWarrantyValidFromR()
                                                                                                                     .getCode());
            if (preventiveMaintenance != null) {
                agreement.setWarrantyValidFromR(preventiveMaintenance);
            }
        }
        if (agreementAPI.getWarrantyValidFromTJ() != null) {
            CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(agreementAPI.getWarrantyValidFromTJ()
                                                                                                                     .getCode());
            if (preventiveMaintenance != null) {
                agreement.setWarrantyValidFromTJ(preventiveMaintenance);
            }
        }

    }

    public List<Agreement> values() {
        return em.createQuery("SELECT DISTINCT a FROM Agreement a " +
                "LEFT JOIN FETCH a.supplier", Agreement.class)
                 .getResultList();
    }

    public Set<SupplierGLNCatalogueUniqueNumber> result() {
        List<AgreementPricelistRow> resultList = em.createQuery("SELECT r FROM AgreementPricelistRow r ", AgreementPricelistRow.class)
                                                   .getResultList();

        return resultList.stream()
                         .map(SupplierGLNCatalogueUniqueNumber::of)
                         .collect(Collectors.toSet());
    }

}

/*
 *
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 *  Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.result;

import lombok.Value;

import java.math.BigInteger;

@Value
public class SupplierReport {

    private final String organizationName;

    private final boolean subscribes;

    private final long products;

    private final long articles;

    public static SupplierReport mysql(Object[] array) {
        return new SupplierReport((String) array[0], true,((BigInteger) array[1]).longValue(), ((BigInteger) array[2]).longValue());
    }
    public static SupplierReport h2(Object[] array) {
        return new SupplierReport((String) array[0],(boolean)array[1], ((BigInteger) array[2]).longValue(), ((BigInteger) array[3]).longValue());
    }
}

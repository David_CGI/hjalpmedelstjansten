/*
 *
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 *  Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.agreements.gp;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import se.inera.hjmtj.migration.domain.model.organization.entities.Customer;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PersistenceUnit;
import javax.xml.bind.annotation.XmlRootElement;
import java.time.LocalDateTime;

@PersistenceUnit(unitName = "local")
@Entity
@XmlRootElement(name = "Agreement")
@RequiredArgsConstructor
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = ExportSetting.FIND_ALL, query = "SELECT e FROM ExportSetting e "),

        @NamedQuery(name = ExportSetting.FIND_BY_CUSTOMER_GLN, query = "SELECT e FROM ExportSetting e " +
                "WHERE e.customer.gln = :gln")
})

public class ExportSetting {

    public static final String FIND_ALL = "ExportSettings.FIND_ALL";

    public static final String FIND_BY_CUSTOMER_GLN = "ExportSettings.FIND_BY_CUSTOMER_GLN";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    private String agreementNumber;

    private String seller;

    private String buyer;

    @ManyToOne
    private Supplier supplier;

    @ManyToOne
    private Customer customer;

    @Column(columnDefinition = "VARCHAR", length = 1024)
    private String versionlist;

    private LocalDateTime validFrom;

    private LocalDateTime validEnds;

    private LocalDateTime createdTime;

    @Enumerated(EnumType.STRING)
    private YesNo autoCreated;

    @ManyToOne
    private GeneralPricelist generalPricelist;

    public enum YesNo {
        Ja,
        Nej
    }
}

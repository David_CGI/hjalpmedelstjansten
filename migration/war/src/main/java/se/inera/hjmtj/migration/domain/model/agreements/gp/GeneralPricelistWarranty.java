/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.agreements.gp;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import se.inera.hjmtj.migration.domain.model.Reference;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementWarrantyBase;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceUnit;
import javax.xml.bind.annotation.XmlElement;

@PersistenceUnit(unitName = "local")
@Entity
@RequiredArgsConstructor
@Getter
@Setter
public class GeneralPricelistWarranty extends AgreementWarrantyBase<GeneralPricelistWarrantyUnit, GeneralPricelistCalculatedFrom> {

    @ManyToOne(fetch = FetchType.EAGER)
    private GeneralPricelistWarrantyUnit unit;

    @ManyToOne(fetch = FetchType.EAGER)
    private GeneralPricelistCalculatedFrom calculatedFrom;

    @XmlElement(name = "_x0035_731d6d603a34eaf89b9c4dbc9678e31")
    public void setXmlUnit(Reference<GeneralPricelistWarrantyUnit> reference) {
        unit = reference.getReference();
    }

    @XmlElement(name = "d5c4e7d32f0a43b3a5341ce63c0be1ed")
    public void setXmlCalculatedFrom(Reference<GeneralPricelistCalculatedFrom> reference) {
        calculatedFrom = reference.getReference();
    }
}

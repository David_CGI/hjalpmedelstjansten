/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.product.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import se.inera.hjmtj.migration.domain.xml.TrimStringAdapter;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PersistenceUnit;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;

@PersistenceUnit(unitName = "local")
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductImage implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long generatedId;

    @Embedded
    @XmlElement(name = "File")
    private ProductFile file;

    /**
     * Alt-text - Beskrivning: Alternativ text som förmedlar samma information som bilden
     */
    private String altText;

    private String fileDescription;

    @XmlJavaTypeAdapter(TrimStringAdapter.class)
    @XmlElement(name = "Description")
    public void setXmlDescription(String description) {
        this.fileDescription = description != null ? description.substring(0, Math.min(255, description.length())) : null;
    }

    @XmlJavaTypeAdapter(TrimStringAdapter.class)
    @XmlElement(name = "cc19119cfc904d3b94201336130b2d0b")
    public void setXmlAltText(String altText) {
        this.altText = altText != null ? altText.substring(0, Math.min(255, altText.length())) : null;
    }

    void afterUnmarshal(Unmarshaller u, Object parent) {
        // Fix bad data
        if (file != null) {
            if (file.getFileName() == null) {
                file.setFileName("noname");
            }
            if (file.getFileExtension() == null) {
                file.setFileExtension(".jpg");
            }
        }
    }

}

/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.product.entities;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import se.inera.hjmtj.migration.domain.model.Reference;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.PersistenceUnit;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;

@PersistenceUnit(unitName = "local")
@NamedQuery(name = ProductCategoryProduct.FIND_BY_ID, query = "SELECT c FROM ProductCategoryProduct c WHERE c.id = :categoryProductId")
@Entity
@XmlRootElement(name = "CATEGORY_Product")
@RequiredArgsConstructor
@Getter
@Setter
public class ProductCategoryProduct {

    public static final String FIND_BY_ID = "XmlCategoryProduct.FindById";

    @Id
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    private ProductClassification classification;

    @XmlElement(name = "RelatedClassifications")
    public void setXmlClassification(Reference<ProductClassification> reference) {
        classification = reference.getReference();
    }

    @XmlAttribute(name = "id")
    @XmlID
    public String getIdAsString() {
        return String.valueOf(id);
    }

    public void setIdAsString(String xmlId) {
        id = Long.parseLong(xmlId);
    }
}

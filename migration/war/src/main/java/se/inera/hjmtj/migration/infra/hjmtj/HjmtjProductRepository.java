/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.hjmtj;

import lombok.extern.slf4j.Slf4j;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.domain.model.product.AssistiveProduct;
import se.inera.hjmtj.migration.domain.model.product.ProductAPINode;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import static java.util.concurrent.TimeUnit.HOURS;
import static javax.ejb.TransactionAttributeType.REQUIRES_NEW;

@Slf4j
@Stateless
@Interceptors({PerformanceLogInterceptor.class})
@TransactionTimeout(unit = HOURS, value = 5)
public class HjmtjProductRepository {

    @PersistenceContext(unitName = "HjmtjPU")
    private EntityManager em;

    public void removeByOrganization(long organizationId) {
        // delete not replaced products first
        em.createNativeQuery("DELETE FROM Product WHERE Product.organizationId = :id " +
                "AND Product.replacedByProductId IS NOT NULL")
          .setParameter("id", organizationId)
          .executeUpdate();

        int no = em.createNativeQuery("DELETE FROM Product WHERE Product.organizationId = :id")
                   .setParameter("id", organizationId)
                   .executeUpdate();

        em.createNativeQuery("DELETE FROM UserEngagement WHERE UserEngagement.uniqueId IN " +
                "(SELECT p.createdById FROM Product p WHERE p.organizationId = :id)")
          .setParameter("id", organizationId)
          .executeUpdate();

    }

    @TransactionAttribute(REQUIRES_NEW)
    public Optional<Article> findArticleByCatalogNumber(Long catalogueUniqueNumber) {
        try {
            Article article = (Article) em.createQuery("SELECT a FROM Article a " +
                    "LEFT JOIN FETCH a.organization " +
                    "LEFT JOIN FETCH a.catalogUniqueNumber " +
                    "WHERE a.catalogUniqueNumber.uniqueId = :catalogueUniqueNumber")
                                          .setParameter("catalogueUniqueNumber", catalogueUniqueNumber)
                                          .setHint("org.hibernate.readOnly", true)
                                          .getSingleResult();

            return Optional.of(article);
        } catch (NoResultException ex) {
            return Optional.empty();
        }
    }

    @TransactionAttribute(REQUIRES_NEW)
    public Optional<Product> findProductByCatalogNumber(Long catalogueUniqueNumber) {
        try {
            Product product = (Product) em.createQuery("SELECT p FROM Product p " +
                    "LEFT JOIN FETCH p.organization " +
                    "LEFT JOIN FETCH p.catalogUniqueNumber " +
                    "WHERE p.catalogUniqueNumber.uniqueId= :catalogueUniqueNumber")
                                          .setParameter("catalogueUniqueNumber", catalogueUniqueNumber)
                                          .setHint("org.hibernate.readOnly", true)
                                          .getSingleResult();
            return Optional.of(product);
        } catch (NoResultException ex) {
            return Optional.empty();
        }
    }


    public long size() {
        return ((BigInteger) em.createNativeQuery("SELECT COUNT(*) FROM PRODUCT ")
                               .setMaxResults(1)
                               .getSingleResult()).longValue();
    }

    public void setVgrProdNo(AssistiveProduct assistiveProduct) {
        Long catalogueUniqueNumber = Long.valueOf(assistiveProduct.getCatalogueUniqueNumber());

        em.createNativeQuery("INSERT INTO CatalogueUniqueNumber (uniqueId) VALUES (:catalogueUniqueNumber)")
          .setParameter("catalogueUniqueNumber", catalogueUniqueNumber)
          .executeUpdate();

        if (assistiveProduct instanceof ProductAPINode) {
            em.createNativeQuery("UPDATE Product SET catalogUniqueNumberId = :catalogueUniqueNumber " +
                    "WHERE uniqueId = :id")
              .setParameter("id", assistiveProduct.getId())
              .setParameter("catalogueUniqueNumber", catalogueUniqueNumber)
              .executeUpdate();
        } else {
            em.createNativeQuery("UPDATE Article SET catalogUniqueNumberId = :catalogueUniqueNumber " +
                    "WHERE uniqueId = :id")
              .setParameter("id", assistiveProduct.getId())
              .setParameter("catalogueUniqueNumber", catalogueUniqueNumber)
              .executeUpdate();
        }
    }

    public Optional<Long> findIdBy(String articleNumber, String gln) {
        try {
            BigInteger result = (BigInteger) em.createNativeQuery("SELECT a.uniqueId FROM Article a " +
                    "JOIN Organization O on a.organizationId = O.uniqueId " +
                    "WHERE a.articleNumber = :articleNumber AND " +
                    "O.gln = :gln ")
                                               .setParameter("articleNumber", articleNumber)
                                               .setParameter("gln", gln)
                                               .getSingleResult();
            return Optional.of(result.longValue());
        } catch (NoResultException e) {
            log.warn("No product entity found using params (articleNumber = {}, gln = {})", articleNumber, gln);
            return Optional.empty();
        }
    }

    public boolean containsCatalogueUniqueNumber(String catalogueUniqueNumber) {
        return !em.createNativeQuery("SELECT p.uniqueId FROM Product p WHERE p.catalogUniqueNumberId = :catalogUniqueNumber")
                  .setParameter("catalogUniqueNumber", catalogueUniqueNumber)
                  .getResultList()
                  .isEmpty();
    }

    public List<Product> findByGLN(String gln) {
        return em.createQuery("SELECT a FROM Product a " +
                "LEFT JOIN FETCH a.catalogUniqueNumber " +
                "WHERE a.organization.gln = :gln", Product.class)
                 .setParameter("gln", gln)
                 .setHint("org.hibernate.readOnly", true)
                 .getResultList();
    }
}

/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.product.entities;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import se.inera.hjmtj.migration.domain.model.BlindReference;
import se.inera.hjmtj.migration.domain.model.categories.ProductCategorySpecificPropertyType;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PersistenceUnit;
import java.io.Serializable;
import java.math.BigDecimal;

@PersistenceUnit(unitName = "local")
@Entity
@RequiredArgsConstructor
@Getter
@Setter
public class ProductCategorySpecificProperty implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    private ProductCategorySpecificPropertyType productCategorySpecificPropertyType;

    @ManyToOne(fetch = FetchType.EAGER)
    private ProductCategorySpecificPropertyListValue listValue;

    private String stringValue;

    private Boolean booleanValue;

    private BigDecimal decimalValue;

    @Embedded
    private ProductIntervalDecimal intervalDecimalValue;

    public static ProductCategorySpecificProperty of(String propertyName, Boolean value) {
        ProductCategorySpecificProperty xmlCategorySpecificProperty = new ProductCategorySpecificProperty();
        xmlCategorySpecificProperty.setProductCategorySpecificPropertyType(ProductCategorySpecificPropertyType.of(propertyName));
        xmlCategorySpecificProperty.setBooleanValue(value);

        return xmlCategorySpecificProperty;
    }

    public static ProductCategorySpecificProperty of(String propertyName, BigDecimal value) {
        ProductCategorySpecificProperty xmlCategorySpecificProperty = new ProductCategorySpecificProperty();
        xmlCategorySpecificProperty.setProductCategorySpecificPropertyType(ProductCategorySpecificPropertyType.of(propertyName));
        xmlCategorySpecificProperty.setDecimalValue(value);

        return xmlCategorySpecificProperty;
    }

    public static ProductCategorySpecificProperty of(String propertyName, ProductIntervalDecimal value) {
        ProductCategorySpecificProperty xmlCategorySpecificProperty = new ProductCategorySpecificProperty();
        xmlCategorySpecificProperty.setProductCategorySpecificPropertyType(ProductCategorySpecificPropertyType.of(propertyName));
        xmlCategorySpecificProperty.setIntervalDecimalValue(value);

        return xmlCategorySpecificProperty;
    }

    public static ProductCategorySpecificProperty of(String propertyName, String value) {
        ProductCategorySpecificProperty xmlCategorySpecificProperty = new ProductCategorySpecificProperty();
        xmlCategorySpecificProperty.setProductCategorySpecificPropertyType(ProductCategorySpecificPropertyType.of(propertyName));
        xmlCategorySpecificProperty.setStringValue(value);

        return xmlCategorySpecificProperty;
    }

    public static ProductCategorySpecificProperty of(String propertyName, BlindReference xmlReference) {
        ProductCategorySpecificProperty xmlCategorySpecificProperty = new ProductCategorySpecificProperty();
        xmlCategorySpecificProperty.setProductCategorySpecificPropertyType(ProductCategorySpecificPropertyType.of(propertyName));
        ProductCategorySpecificPropertyListValue listValue = new ProductCategorySpecificPropertyListValue();
        listValue.setId(xmlReference.getIdRef());
        xmlCategorySpecificProperty.setListValue(listValue);

        return xmlCategorySpecificProperty;
    }

    public boolean isValuePresent() {
        return getValue() != null;
    }

    private Object getValue() {
        if (decimalValue != null) {
            return decimalValue;
        } else if (intervalDecimalValue != null && (intervalDecimalValue.min() != null || intervalDecimalValue.max() != null)) {
            return intervalDecimalValue;
        } else if (listValue != null) {
            return listValue;
        } else if (stringValue != null) {
            return stringValue;
        }

        return null;
    }

}

/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.agreements;

import lombok.extern.slf4j.Slf4j;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjmtj.migration.domain.HeapUsageInterceptor;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.domain.model.agreements.gp.GeneralPricelist;
import se.inera.hjmtj.migration.domain.model.agreements.gp.GeneralPricelistRow;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.infra.io.BatchPersistence;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import static java.util.concurrent.TimeUnit.HOURS;

@Slf4j
@Stateless
@Interceptors({PerformanceLogInterceptor.class, HeapUsageInterceptor.class})
@TransactionTimeout(unit = HOURS, value = 5)
public class GeneralPricelistRepository {


    @PersistenceContext(unitName = "local")
    private EntityManager em;

    @Inject
    private BatchPersistence batchPersistence;

    public boolean isEmpty() {
        return size() == 0;
    }

    private long size() {
        return em.createNamedQuery(GeneralPricelist.COUNT_GP, Long.class)
                 .setMaxResults(1)
                 .getSingleResult();
    }

    public void save(List<Object> objects) {
        batchPersistence.save(objects, em);
    }


    public List<GeneralPricelist> values() {
        return em.createNamedQuery(GeneralPricelist.FIND_ALL, GeneralPricelist.class)
                 .setHint("org.hibernate.readOnly", true)
                 .getResultList();
    }

    public GeneralPricelist findBy(Supplier supplier) {
        return em.createNamedQuery(GeneralPricelist.FIND_BY_SUPPLIER, GeneralPricelist.class)
                 .setParameter("supplier", supplier)
                 .setHint("org.hibernate.readOnly", true)
                 .getResultList()
                 .stream()
                 .findFirst()
                 .map(this::setRows)
                 .orElse(null);
    }

    private GeneralPricelist setRows(GeneralPricelist generalPricelist) {
        LinkedList<GeneralPricelistRow> rows = new LinkedList<>(generalPricelist.getRows());
        generalPricelist.setRows(new HashSet<>(rows));
        return generalPricelist;
    }

}

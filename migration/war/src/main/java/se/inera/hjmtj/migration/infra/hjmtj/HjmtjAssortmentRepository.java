/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.hjmtj;

import lombok.extern.slf4j.Slf4j;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.assortment.controller.AssortmentController;
import se.inera.hjalpmedelstjansten.model.api.AssortmentAPI;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.entity.Assortment;
import se.inera.hjmtj.migration.domain.HeapUsageInterceptor;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.domain.model.assortment.AssortmentAPIWithArticles;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MessageTemplate;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.domain.model.product.ArticleAPINode;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static java.util.concurrent.TimeUnit.HOURS;

@SuppressWarnings("JpaQueryApiInspection")
@Slf4j
@Stateless
@Interceptors({PerformanceLogInterceptor.class, HeapUsageInterceptor.class})
@TransactionTimeout(unit = HOURS, value = 5)
public class HjmtjAssortmentRepository {

    @PersistenceContext(unitName = "HjmtjPU")
    private EntityManager em;

    @Inject
    private AssortmentController assortmentController;

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    @Inject
    private HjmtjArticleRepository hjmtjArticleRepository;

    public void save(AssortmentAPIWithArticles assortmentAPI) {
        OrganizationAPI customer = assortmentAPI.getCustomer();

        try {
            long savedAssortmentId = saveAssortment(assortmentAPI, customer);
            saveAssortmentArticles(assortmentAPI, savedAssortmentId);

            log.info("Saved assortment {}", assortmentAPI.getName());
        } catch (HjalpmedelstjanstenValidationException e) {
            for (ErrorMessageAPI errorMessageAPI : e.getValidationMessages()) {
                MigrationError migrationError = new MigrationError();
                migrationError.setClassifier(Classifier.field(Classifier.ASSORTMENT, errorMessageAPI.getField()));
                migrationError.setAssortmentName(assortmentAPI.getName());
                migrationError.setOrganizationName(assortmentAPI.getCustomer()
                                                                .getOrganizationName());
                migrationError.setMessage(errorMessageAPI.getMessage());
                migrationErrorRepository.add(migrationError);
            }

            log.warn("Failed to save assortment {}, reason: {}", assortmentAPI.getName(), e.getMessage());
        }
    }

    private long saveAssortment(AssortmentAPIWithArticles assortmentAPI, OrganizationAPI organizationAPI) throws HjalpmedelstjanstenValidationException {
        UserAPI userAPI = new UserAPI();
        userAPI.setId(-1l);

        AssortmentAPI savedAssortmentAPI = assortmentController.createAssortment(organizationAPI.getId(), assortmentAPI, userAPI, "", "");

        // Update with county and municipalities
        if (assortmentAPI.getCounty() != null) {
            savedAssortmentAPI.setCounty(assortmentAPI.getCounty());
            savedAssortmentAPI.setMunicipalities(assortmentAPI.getCounty() != null ? assortmentAPI.getMunicipalities() : null);
            assortmentController.updateAssortment(organizationAPI.getId(), savedAssortmentAPI.getId(), savedAssortmentAPI, userAPI, "", "", true);
        }

        return savedAssortmentAPI.getId();
    }

    private void saveAssortmentArticles(AssortmentAPIWithArticles assortmentAPI, long savedAssortmentId) {
        for (ArticleAPINode articleAPI : assortmentAPI.getArticles()) {
            boolean articleExists = hjmtjArticleRepository.contains(articleAPI.getId());

            if (articleExists) {
                saveAssortmentArticle(savedAssortmentId, articleAPI);
            } else {
                MigrationError migrationError = new MigrationError();
                migrationError.setOrganizationName(assortmentAPI.getCustomer()
                                                                .getOrganizationName());
                migrationError.setAssortmentName(assortmentAPI.getName());
                migrationError.setSeverity(MigrationError.Severity.ERROR);
                migrationError.setClassifier(Classifier.ASSORTMENT_ARTICLE);
                migrationError.setCatalogueUniqueNumber(articleAPI.getCatalogueUniqueNumber());
                migrationError.setMessage(MessageTemplate.ROW_WILL_NOT_BE_MIGRATED_BECAUSE_ARTICLE_HAS_NOT_BEEN_MIGRATED);
                migrationErrorRepository.add(migrationError);
            }
        }
    }

    private void saveAssortmentArticle(long savedAssortmentId, ArticleAPINode articleAPI) {
        em.createNativeQuery("INSERT INTO AssortmentArticle (assortmentId, articleId) values (:assortmentId, :articleId)")
          .setParameter("assortmentId", savedAssortmentId)
          .setParameter("articleId", articleAPI.getId())
          .executeUpdate();
    }

    public boolean contains(String organizationName, String assortmentName) {
        return em.createQuery("SELECT a.id FROM Assortment a " +
                "WHERE a.customer.organizationName = :organizationName AND " +
                "a.name = :assortmentName")
                 .setParameter("organizationName", organizationName)
                 .setParameter("assortmentName", assortmentName)
                 .getResultList()
                 .size() > 0;
    }

    public int count(String organizationName, String assortmentName) {
        return em.createQuery("SELECT a.articles.size FROM Assortment a " +
                "WHERE a.customer.organizationName = :organizationName AND " +
                "a.name = :assortmentName", Integer.class)
                 .setParameter("organizationName", organizationName)
                 .setParameter("assortmentName", assortmentName)
                 .getResultList()
                 .stream()
                 .mapToInt(Integer::intValue)
                 .sum();
    }

    public List<Assortment> values() {
        return em.createQuery("SELECT a FROM Assortment a", Assortment.class)
                 .getResultList();
    }
}

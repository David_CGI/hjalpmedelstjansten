/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.result;

import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.domain.model.agreements.gp.GeneralPricelistRow;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.AgreementRow;
import se.inera.hjmtj.migration.domain.model.assortment.AssortmentProduct;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.domain.model.product.entities.Product;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static java.util.concurrent.TimeUnit.HOURS;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@Stateless
@Interceptors({PerformanceLogInterceptor.class})
@TransactionTimeout(unit = HOURS, value = 5)
public class AssistiveProductReportRepository {

    @PersistenceContext(unitName = "local")
    private EntityManager em;

    public List<AssistiveProductReport> get(Supplier supplier) {
        List<Object[]> resultList = em.createNativeQuery("SELECT\n" +
                "  o.NAME                                                       AS \"Leverantör\",\n" +
                "  o.SUPPLIER_ID IS NOT NULL                                    AS \"Finns i Lime?\",\n" +
                "  p.ID                                                         AS \"ID\",\n" +
                "  p.VGRPRODNO                                                  AS \"Katalogunikt nummer\",\n" +
                "  p.SUPPLIERPRODUCTNUMBER,\n" +
                "  trim(p.CATEGORYPATH) LIKE\n" +
                "  'Produkt\\\\Vara\\\\ISO-klassificerade hjälpmedel (ISO 9999)\\\\%' AS \"Är ISO9999-hjälpmedel\",\n" +
                "  COALESCE(p.ISTEMPLATEPRODUCT, FALSE) =\n" +
                "  FALSE                                                        AS \"Är artikel\",\n" +
                "  concat_ws(',', group_concat(F1.IDREF), group_concat(F2.IDREF), group_concat(F3.IDREF), group_concat(F4.IDREF),\n" +
                "            group_concat(R.IDREF), p.BASEDONTEMPLATEPRODUCT)   AS \"Kräver (ID)\",\n" +
                "  (SELECT TRUE\n" +
                "   FROM AGREEMENTROW r\n" +
                "   WHERE r.PRODUCT_VGRPRODNO = p.VGRPRODNO\n" +
                "   HAVING COUNT(*) >\n" +
                "          0)                                                   AS \"Förekommer i inköpsavtal\"\n" +
                "FROM PRODUCT p\n" +
                "  LEFT JOIN PRODUCT_ACCESSORYTO F1 on p.VGRPRODNO = F1.PRODUCT_VGRPRODNO\n" +
                "  LEFT JOIN PRODUCT_SPAREPARTFOR F2 on p.VGRPRODNO = F2.PRODUCT_VGRPRODNO\n" +
                "  LEFT JOIN PRODUCT_SERVICEFOR F3 on p.VGRPRODNO = F3.PRODUCT_VGRPRODNO\n" +
                "  LEFT JOIN PRODUCT_SETTINGFOR F4 on p.VGRPRODNO = F4.PRODUCT_VGRPRODNO\n" +
                "  LEFT JOIN PRODUCT_REPLACESLIST R on p.VGRPRODNO = R.PRODUCT_VGRPRODNO\n" +
                "  LEFT JOIN PRODUCTORGANIZATION o on p.SUPPLIER_ID = o.ID\n" +
                "   WHERE lower(trim(o.NAME)) = :orgName OR o.GLN = :gln " +
                "GROUP BY P.ID")
                                      .setParameter("orgName", supplier.getName()
                                                                       .trim()
                                                                       .toLowerCase())
                                      .setParameter("gln", supplier.getGln())
                                      .getResultList();

        return resultList
                .stream()
                .map(this::result)
                .sorted()
                .collect(toList());
    }

    public List<AssistiveProductReport> all() {
        List<Object[]> resultList = em.createNativeQuery("SELECT\n" +
                "  coalesce(s.NAME, o.NAME)                                           AS \"Leverantör\",\n" +
                "  o.SUPPLIER_ID IS NOT NULL                                          AS \"Finns i Lime?\",\n" +
                "  p.ID                                                               AS \"ID\",\n" +
                "  p.VGRPRODNO                                                        AS \"Katalogunikt nummer\",\n" +
                "  p.SUPPLIERPRODUCTNUMBER,\n" +
                "  trim(p.CATEGORYPATH) LIKE\n" +
                "  'Produkt\\\\Vara\\\\ISO-klassificerade hjälpmedel (ISO 9999)\\\\%' AS \"Är ISO9999-hjälpmedel\",\n" +
                "  COALESCE(p.ISTEMPLATEPRODUCT, FALSE) =\n" +
                "  FALSE                                                              AS \"Är artikel\",\n" +
                "  concat_ws(',', group_concat(F1.IDREF), group_concat(F2.IDREF), group_concat(F3.IDREF),\n" +
                "            group_concat(F4.IDREF),\n" +
                "            group_concat(R.IDREF), p.BASEDONTEMPLATEPRODUCT)         AS \"Kräver (ID)\",\n" +
                "  A.VGRPRODNO IS NOT NULL                                            AS \"Förekommer i inköpsavtal\",\n" +
                "  G2.VGRPRODNO IS NOT NULL                                           AS \"Förekommer i GP\",\n" +
                "  A2.VGRPRODNO IS NOT NULL                                           AS \"Förekommer i utbud\"\n" +
                "FROM PRODUCT p\n" +
                "       LEFT JOIN PRODUCT_ACCESSORYTO F1 on p.VGRPRODNO = F1.PRODUCT_VGRPRODNO\n" +
                "       LEFT JOIN PRODUCT_SPAREPARTFOR F2 on p.VGRPRODNO = F2.PRODUCT_VGRPRODNO\n" +
                "       LEFT JOIN PRODUCT_SERVICEFOR F3 on p.VGRPRODNO = F3.PRODUCT_VGRPRODNO\n" +
                "       LEFT JOIN PRODUCT_SETTINGFOR F4 on p.VGRPRODNO = F4.PRODUCT_VGRPRODNO\n" +
                "       LEFT JOIN PRODUCT_REPLACESLIST R on p.VGRPRODNO = R.PRODUCT_VGRPRODNO\n" +
                "       LEFT JOIN PRODUCTORGANIZATION o on p.SUPPLIER_ID = o.ID\n" +
                "       LEFT JOIN SUPPLIER s ON o.SUPPLIER_ID = s.ID\n" +
                "       LEFT JOIN AGREEMENTPRODUCT A on p.VGRPRODNO = A.ARTICLE_VGRPRODNO\n" +
                "       LEFT JOIN GENERALPRICELISTPRODUCT G2 on p.VGRPRODNO = G2.ARTICLE_VGRPRODNO\n" +
                "       LEFT JOIN ASSORTMENTPRODUCT A2 on p.VGRPRODNO = A2.ARTICLE_VGRPRODNO\n" +
                "GROUP BY P.ID\n")
                                      .getResultList();

        return resultList
                .stream()
                .map(this::result)
                .sorted()
                .collect(toList());
    }

    private AssistiveProductReport result(Object[] array) {
        return AssistiveProductReport.builder()
                                     .organizationName(array[0].toString())
                                     .existsInLime(array[1] != null && (boolean) array[1])
                                     .id(Long.valueOf(array[2].toString()))
                                     .catalogueUniqueNumber(Long.valueOf(array[3].toString()))
                                     .searchableNumber(array[4].toString())
                                     .isISO9999((boolean) array[5])
                                     .type((boolean) array[6] ? AssistiveProductReport.Type.ARTICLE : AssistiveProductReport.Type.PRODUCT)
                                     .requiresIds(new HashSet<>(Arrays.asList(array[7].toString()
                                                                                      .split(",")))
                                             .stream()
                                             .map(String::trim)
                                             .filter(t -> !t.isEmpty())
                                             .map(Long::valueOf)
                                             .collect(toSet()))
                                     .existsInAgreement(array[8] != null && (boolean) array[8])
                                     .existsInGP(array[9] != null && (boolean) array[9])
                                     .existsInAssortment(array[10] != null && (boolean) array[10])
                                     .build();
    }

    public GeneralReport quickStats() {
        BigInteger iso9999 = (BigInteger) em.createNamedQuery(Product.COUNT_ALL)
                                            .getSingleResult();
        BigInteger agreements = (BigInteger) em.createNativeQuery("SELECT COUNT(*) FROM AGREEMENT")
                                               .getSingleResult();
        BigInteger agreementRows = (BigInteger) em.createNamedQuery(AgreementRow.COUNT_ALL)
                                                  .getSingleResult();
        BigInteger gp = (BigInteger) em.createNativeQuery("SELECT COUNT(*) FROM GENERALPRICELIST")
                                       .getSingleResult();
        BigInteger gpRows = (BigInteger) em.createNamedQuery(GeneralPricelistRow.COUNT_ALL)
                                           .getSingleResult();
        BigInteger assortments = (BigInteger) em.createNativeQuery("SELECT COUNT(*) FROM ASSORTMENT")
                                                .getSingleResult();
        BigInteger assortmentProducts = (BigInteger) em.createNamedQuery(AssortmentProduct.COUNT_ALL)
                                                       .getSingleResult();
        BigInteger actors = (BigInteger) em.createNativeQuery("SELECT COUNT(*) FROM ACTOR")
                                           .getSingleResult();
        BigInteger suppliers = (BigInteger) em.createNativeQuery("SELECT COUNT(*) FROM SUPPLIER")
                                              .getSingleResult();
        BigInteger customers = (BigInteger) em.createNativeQuery("SELECT COUNT(*) FROM CUSTOMER WHERE CUSTOMER.TYPE IS NULL ")
                                              .getSingleResult();
        BigInteger businessLevels = (BigInteger) em.createNativeQuery("SELECT COUNT(*) FROM CUSTOMER WHERE CUSTOMER.TYPE IS NOT NULL ")
                                                   .getSingleResult();


        GeneralReport.GeneralReportBuilder builder = GeneralReport.builder();
        builder.iso9999(iso9999.longValue());
        builder.agreements(agreements.longValue());
        builder.agreementRows(agreementRows.longValue());
        builder.gp(gp.longValue());
        builder.gpRows(gpRows.longValue());
        builder.assortments(assortments.longValue());
        builder.assortmentProducts(assortmentProducts.longValue());
        builder.users(actors.longValue());
        builder.suppliers(suppliers.longValue());
        builder.customers(customers.longValue());
        builder.businessLevels(businessLevels.longValue());

        return builder.build();
    }

    public GeneralReport expired() {
        BigInteger iso9999 = (BigInteger) em.createNativeQuery("SELECT COUNT(DISTINCT PRODUCT.ID)\n" +
                "FROM Product\n" +
                "WHERE trim(Product.CATEGORYPATH) LIKE\n" +
                "      'Produkt\\\\Vara\\\\ISO-klassificerade hjälpmedel (ISO 9999)\\\\%' AND " +
                "Product.EXPIRETIME <= parsedatetime('2014-01-14', 'yyyy-MM-dd')")
                                            .getSingleResult();

        BigInteger agreements = (BigInteger) em.createNativeQuery("SELECT COUNT(*) FROM AGREEMENT WHERE " +
                "AGREEMENT.VALID_ENDS <= parsedatetime('2017-07-01', 'yyyy-MM-dd')")
                                               .getSingleResult();

        BigInteger agreementRows = (BigInteger) em.createNativeQuery("SELECT COUNT(DISTINCT AGREEMENTROW.GENERATEDID) FROM AGREEMENTROW " +
                "JOIN AGREEMENTPRODUCT ON AGREEMENTROW.AGREEMENTARTICLE_VGRPRODNO = AGREEMENTPRODUCT.VGRPRODNO " +
                "JOIN PRODUCT ON AGREEMENTPRODUCT.ARTICLE_VGRPRODNO = PRODUCT.VGRPRODNO " +
                "JOIN AGREEMENT_AGREEMENTROW AA on AGREEMENTROW.GENERATEDID = AA.ROWS_GENERATEDID " +
                "JOIN AGREEMENT A on AA.AGREEMENT_AGREEMENTID = A.AGREEMENTID " +
                "WHERE " +
                "Product.EXPIRETIME <= parsedatetime('2014-01-14', 'yyyy-MM-dd') OR " +
                "A.VALID_ENDS <= parsedatetime('2017-07-01', 'yyyy-MM-dd')")
                                                  .getSingleResult();
        BigInteger gp = (BigInteger) em.createNativeQuery("SELECT COUNT(DISTINCT GENERALPRICELIST.AGREEMENTID) FROM GENERALPRICELIST WHERE " +
                "GENERALPRICELIST.VALID_ENDS <= parsedatetime('2017-07-01', 'yyyy-MM-dd')")
                                       .getSingleResult();

        BigInteger gpRows = (BigInteger) em.createNativeQuery("SELECT COUNT(*) FROM GENERALPRICELISTROW " +
                "JOIN GENERALPRICELISTPRODUCT ON GENERALPRICELISTROW.AGREEMENTARTICLE_VGRPRODNO = GENERALPRICELISTPRODUCT.VGRPRODNO " +
                "JOIN PRODUCT ON GENERALPRICELISTPRODUCT.ARTICLE_VGRPRODNO = PRODUCT.VGRPRODNO " +
                "JOIN AGREEMENTPRODUCT A on PRODUCT.VGRPRODNO = A.ARTICLE_VGRPRODNO " +
                "JOIN GENERALPRICELIST G2 on GENERALPRICELISTROW.GENERALPRICELISTS_AGREEMENTID = G2.AGREEMENTID " +
                "WHERE " +
                "Product.EXPIRETIME <= parsedatetime('2014-01-14', 'yyyy-MM-dd') OR " +
                "G2.VALID_ENDS <= parsedatetime('2017-07-01', 'yyyy-MM-dd')")
                                           .getSingleResult();


        GeneralReport.GeneralReportBuilder builder = GeneralReport.builder();
        builder.iso9999(iso9999.longValue());
        builder.agreements(agreements.longValue());
        builder.agreementRows(agreementRows.longValue());
        builder.gp(gp.longValue());
        builder.gpRows(gpRows.longValue());

        return builder.build();
    }

}

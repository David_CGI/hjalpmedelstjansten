/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.hjmtj;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCountyAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVMunicipalityAPI;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCounty;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVMunicipality;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

@Slf4j
@Stateless
public class HjmtjCountyRepository {

    @PersistenceContext(unitName = "HjmtjPU")
    private EntityManager em;

    public Map<String, CVCountyAPIWithShowMunicipalities> codeToCounty() {
        return counties().stream()
                         .collect(toMap(CVCountyAPI::getCode, identity()));
    }

    public List<CVCountyAPIWithShowMunicipalities> counties() {
        return em.createNamedQuery(CVCounty.FIND_ALL, CVCounty.class)
                 .getResultList()
                 .stream()
                 .map(this::map)
                 .collect(Collectors.toList());
    }

    private CVCountyAPIWithShowMunicipalities map(CVCounty cvCounty) {
        CVCountyAPIWithShowMunicipalities cvCountyAPI = new CVCountyAPIWithShowMunicipalities();
        cvCountyAPI.setId(cvCounty.getUniqueId());
        cvCountyAPI.setCode(cvCounty.getCode());
        cvCountyAPI.setName(cvCounty.getName());
        cvCountyAPI.setMunicipalities(cvCounty.getMunicipalities()
                                              .stream()
                                              .map(this::map)
                                              .collect(Collectors.toList()));
        cvCountyAPI.setShowMunicipalities(cvCounty.getShowMunicipalities());

        return cvCountyAPI;
    }

    private CVMunicipalityAPI map(CVMunicipality cvMunicipality) {
        CVMunicipalityAPI cvMunicipalityAPI = new CVMunicipalityAPI();
        cvMunicipalityAPI.setId(cvMunicipality.getUniqueId());
        cvMunicipalityAPI.setCode(cvMunicipality.getCode());
        cvMunicipalityAPI.setName(cvMunicipality.getName());

        return cvMunicipalityAPI;
    }

    public Map<? extends String, ? extends CVMunicipalityAPI> codeToMunicipality() {
        return municipalities()
                .stream()
                .collect(toMap(CVMunicipalityAPI::getCode, identity()));
    }

    public Collection<CVMunicipalityAPI> municipalities() {
        return em.createQuery("SELECT m FROM CVMunicipality m", CVMunicipality.class)
                 .getResultList()
                 .stream()
                 .map(this::map)
                 .collect(Collectors.toList());
    }
}
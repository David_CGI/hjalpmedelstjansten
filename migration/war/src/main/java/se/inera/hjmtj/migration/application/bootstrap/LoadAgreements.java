/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.application.bootstrap;

import lombok.extern.slf4j.Slf4j;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjmtj.migration.domain.HeapUsageInterceptor;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.Agreement;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.AgreementCalculatedFrom;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.AgreementOrganization;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.AgreementProduct;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.AgreementRow;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.AgreementStatus;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.AgreementWarrantyUnit;
import se.inera.hjmtj.migration.domain.model.product.entities.Product;
import se.inera.hjmtj.migration.domain.xml.ElementContextComparator;
import se.inera.hjmtj.migration.domain.xml.XMLFile;
import se.inera.hjmtj.migration.infra.agreements.AgreementRepository;
import se.inera.hjmtj.migration.infra.io.BatchPersistence;
import se.inera.hjmtj.migration.infra.organizations.CustomerRepository;
import se.inera.hjmtj.migration.infra.organizations.SupplierRepository;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Loads agreements file into H2.
 */
@Slf4j
@Singleton
@TransactionTimeout(unit = TimeUnit.MINUTES, value = 30)
@Interceptors({PerformanceLogInterceptor.class, HeapUsageInterceptor.class})
public class LoadAgreements {

    public static final ElementContextComparator PERSIST_ORDER = new ElementContextComparator(
            AgreementWarrantyUnit.class,
            AgreementCalculatedFrom.class,
            AgreementOrganization.class,
            AgreementStatus.class,
            AgreementProduct.class,
            Agreement.class);

    private final String AGREEMENT_URL_FILE = System.getenv()
                                                    .getOrDefault("AGREEMENT_URL_FILE", "<file>");

    @PersistenceContext(unitName = "local")
    private EntityManager em;

    @Inject
    private AgreementRepository agreements;

    @Inject
    private BatchPersistence batchPersistence;

    @Inject
    private CustomerRepository customerRepository;

    @Inject
    private SupplierRepository supplierRepository;

    public void run() {
        if (agreements.isEmpty()) {
            if (AgreementRow.USE_GENERATED_VALUE_FOR_PRICE) {
                log.info("Generated prices will be committed");
            } else {
                log.warn("Real prices will be committed - if this is unintentional please abort before transaction commits");
            }

            XMLFile xmlFile = new XMLFile(PERSIST_ORDER);

            List<Object> xmlObjects = xmlFile.loadFromFTP(AGREEMENT_URL_FILE)
                                             .stream()
                                             .map(this::decorate)
                                             .collect(Collectors.toList());

            batchPersistence.save(xmlObjects, em);
        }
    }

    private Object decorate(Object xmlObject) {
        if (xmlObject instanceof AgreementProduct) {
            setArticle((AgreementProduct) xmlObject);
        } else if (xmlObject instanceof AgreementOrganization) {
            AgreementOrganization organization = (AgreementOrganization) xmlObject;

            organization.setCustomer(customerRepository.getCustomer(organization));
            organization.setSupplier(supplierRepository.getSupplier(organization));
        }

        return xmlObject;
    }

    private void setArticle(AgreementProduct agreementArticle) {
        Product article = em.find(Product.class, Long.valueOf(agreementArticle.getVgrProdNo()));
        agreementArticle.setArticle(article);
    }

}

/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model;

import lombok.Value;
import lombok.experimental.Accessors;

import java.util.concurrent.atomic.AtomicLong;

@Value
@Accessors(fluent = true)
public class Indexed<T> {

    private final Long id;

    private final T value;

    public static <T> Builder<T> builder() {
        return new Builder<>();
    }

    public static class Builder<T> {

        private final AtomicLong nextId = new AtomicLong(0);

        public Indexed<T> build(T data) {
            return new Indexed<>(nextId.incrementAndGet(), data);
        }
    }

    @Override
    public String toString() {
        return "[(" + id + ") " + value.toString() + "]";
    }
}
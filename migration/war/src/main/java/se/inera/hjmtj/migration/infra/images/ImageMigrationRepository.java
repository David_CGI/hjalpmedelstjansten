package se.inera.hjmtj.migration.infra.images;

import lombok.extern.slf4j.Slf4j;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.model.entity.CatalogueUniqueNumber;
import se.inera.hjmtj.migration.domain.model.image.entity.ImageMigration;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static java.util.concurrent.TimeUnit.HOURS;
import static javax.ejb.TransactionAttributeType.REQUIRES_NEW;

@Slf4j
@Stateless
@TransactionTimeout(unit = HOURS, value = 5)
public class ImageMigrationRepository {

    @PersistenceContext(unitName = "local")
    private EntityManager em;

    @TransactionAttribute(REQUIRES_NEW)
    public List<ImageMigration> findAllImageMigrationBasedOnSupplier(Long supplierId) {
        return em.createQuery("SELECT b FROM ImageMigration b where b.supplier.id = :supplierId", ImageMigration.class)
                 .setParameter("supplierId", supplierId)
                 .setHint("org.hibernate.readOnly", true)
                 .getResultList();
    }

    public boolean inherits(CatalogueUniqueNumber catalogueUniqueNumber, ImageMigration imageToMigrate) {
        return em.createNamedQuery(ImageMigration.INHERITS, ImageMigration.class)
                 .setParameter("catalogueUniqueNumber", catalogueUniqueNumber.getUniqueId())
                 .setParameter("url", imageToMigrate.getUrl())
                 .setParameter("mainImage", imageToMigrate.isMainImage())
                 .setHint("org.hibernate.readOnly", true)
                 .getResultList()
                 .size() > 0;
    }

}
/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.hjmtj;

import lombok.extern.slf4j.Slf4j;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistController;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistMapper;
import se.inera.hjalpmedelstjansten.business.product.controller.CVPreventiveMaintenanceController;
import se.inera.hjalpmedelstjansten.model.api.BusinessLevelAPI;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.entity.BusinessLevel;
import se.inera.hjalpmedelstjansten.model.entity.ExportSettings;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelist;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelistRow;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;
import se.inera.hjmtj.migration.domain.DateTimes;
import se.inera.hjmtj.migration.domain.SupplierGLNCatalogueUniqueNumber;
import se.inera.hjmtj.migration.domain.exceptions.save.PriceListRowArticleNotFound;
import se.inera.hjmtj.migration.domain.model.agreements.builders.GeneralPricelistAPIWithMultiplePricelists;
import se.inera.hjmtj.migration.domain.model.agreements.builders.GeneralPricelistPricelistAPIWithRows;
import se.inera.hjmtj.migration.domain.model.agreements.builders.Unique;
import se.inera.hjmtj.migration.domain.model.agreements.gp.ExportSetting;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.domain.model.organization.entities.Customer;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.domain.model.product.ArticleAPINode;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.concurrent.TimeUnit.HOURS;

@Slf4j
@Stateless
@TransactionTimeout(unit = HOURS, value = 5)
public class HjmtjGeneralPricelistRepository {

    @PersistenceContext(unitName = "HjmtjPU")
    private EntityManager em;

    @Inject
    private CVPreventiveMaintenanceController cVPreventiveMaintenanceController;

    @Inject
    private HjmtjGeneralPricelistPricelistRepository hjmtjGeneralPricelistPricelistRepository;

    @Inject
    private HjmtjGeneralPricelistPricelistRowRepository hjmtjGeneralPricelistPricelistRowRepository;

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    @Inject
    private HjmtjCustomerRepository hjmtjCustomerRepository;

    @Inject
    private GeneralPricelistController generalPricelistController;

    @Inject
    private HjmtjSupplierRepository hjmtjSupplierRepository;

    public void save(GeneralPricelistAPIWithMultiplePricelists generalPricelistAPI, LocalDateTime created) {
        String supplierName = generalPricelistAPI.getOwnerOrganization()
                                                 .getOrganizationName();

        SaveResult<Long> agreementSaveResult = saveGP(generalPricelistAPI, created);
        MigrationError.of(Classifier.GP, generalPricelistAPI.getOwnerOrganization()
                                                            .getOrganizationName(), agreementSaveResult.errorMessages())
                      .stream()
                      .peek(e -> e.setAgreementNumber(generalPricelistAPI.getGeneralPricelistNumber()))
                      .forEach(migrationErrorRepository::add);

        generalPricelistAPI.setId(agreementSaveResult.data());

        for (GeneralPricelistPricelistAPI pricelistAPI : generalPricelistAPI.getPricelistAPIs()) {
            SaveResult<Long> priceListSaveResult = hjmtjGeneralPricelistPricelistRepository.save(generalPricelistAPI, pricelistAPI, created);

            if (priceListSaveResult.isEmpty()) {
                continue;
            }

            MigrationError.of(Classifier.GP_PRICELIST, generalPricelistAPI.getOwnerOrganization()
                                                                          .getOrganizationName(), agreementSaveResult.errorMessages())
                          .stream()
                          .peek(e -> e.setAgreementNumber(generalPricelistAPI.getGeneralPricelistNumber()))
                          .peek(e -> e.setPricelistNumber(pricelistAPI.getNumber()))
                          .forEach(migrationErrorRepository::add);
            pricelistAPI.setId(priceListSaveResult.data());

            GeneralPricelistPricelistAPIWithRows pricelistAPIWithRows = (GeneralPricelistPricelistAPIWithRows) pricelistAPI;
            for (GeneralPricelistPricelistRowAPI rowAPI : pricelistAPIWithRows.getRows()) {
                try {
                    SaveResult<Long> rowSaveResult = hjmtjGeneralPricelistPricelistRowRepository.save(pricelistAPI, rowAPI, created);
                    MigrationError.of(Classifier.GP_PRICELIST_ROW, generalPricelistAPI.getOwnerOrganization()
                                                                                      .getOrganizationName(), agreementSaveResult.errorMessages())
                                  .stream()
                                  .peek(e -> e.setAgreementNumber(generalPricelistAPI.getGeneralPricelistNumber()))
                                  .peek(e -> e.setPricelistNumber(pricelistAPI.getNumber()))
                                  .forEach(migrationErrorRepository::add);
                    rowAPI.setId(rowSaveResult.data());
                } catch (PriceListRowArticleNotFound e) {
                    MigrationError migrationError = new MigrationError();
                    migrationError.setSeverity(MigrationError.Severity.ERROR);
                    migrationError.setOrganizationName(supplierName);
                    migrationError.setClassifier(Classifier.GP_PRICELIST_ROW);
                    migrationError.setMessage(e.getMessage());

                    if (rowAPI.getArticle() instanceof ArticleAPINode) {
                        migrationError.setCatalogueUniqueNumber(((ArticleAPINode) rowAPI.getArticle()).getCatalogueUniqueNumber());
                    }

                    migrationErrorRepository.add(migrationError);
                }
            }

            log.info("Size = {} rows", pricelistAPIWithRows.getRows()
                                                           .size());
        }
    }

    public SaveResult<Long> saveGP(GeneralPricelistAPI generalPricelistAPI, LocalDateTime created) {
        GeneralPricelist generalPricelist = GeneralPricelistMapper.map(generalPricelistAPI);

        // add supplier organization
        setSupplier(generalPricelistAPI, generalPricelist);

        // set warranty quantity units
        setWarrantyQuantityUnits(generalPricelist, generalPricelistAPI);

        // set warranty valid from
        setWarrantyValidFroms(generalPricelistAPI, generalPricelist);

        // set timestamps
        Long createdMillis = DateTimes.toEpochMillis(created);
        generalPricelist.setCreated(createdMillis != null ? new Date(createdMillis) : Calendar.getInstance()
                                                                                              .getTime());
        generalPricelist.setLastUpdated(createdMillis != null ? new Date(createdMillis) : Calendar.getInstance()
                                                                                                  .getTime());

        // save it
        em.persist(generalPricelist);

        log.info("Saved general pricelist {}, seller {}", generalPricelistAPI.getGeneralPricelistNumber(), generalPricelistAPI.getOwnerOrganization()
                                                                                                                              .getOrganizationName());

        return SaveResult.of(generalPricelist.getUniqueId(), generalPricelistAPI);
    }

    private void setSupplier(GeneralPricelistAPI generalPricelistAPI, GeneralPricelist generalPricelist) {
        Organization supplier = em.getReference(Organization.class, generalPricelistAPI.getOwnerOrganization()
                                                                                       .getId());
        generalPricelist.setOwnerOrganization(supplier);
    }

    private void setWarrantyQuantityUnits(GeneralPricelist generalPricelist, GeneralPricelistAPI generalPricelistAPI) {
        // H
        if (generalPricelistAPI.getWarrantyQuantityHUnit() != null) {
            CVGuaranteeUnit unit = em.getReference(CVGuaranteeUnit.class, generalPricelistAPI.getWarrantyQuantityHUnit()
                                                                                             .getId());
            generalPricelist.setWarrantyQuantityHUnit(unit);
        }

        // T
        if (generalPricelistAPI.getWarrantyQuantityTUnit() != null) {
            CVGuaranteeUnit unit = em.getReference(CVGuaranteeUnit.class, generalPricelistAPI.getWarrantyQuantityTUnit()
                                                                                             .getId());
            generalPricelist.setWarrantyQuantityTUnit(unit);
        }

        // I
        if (generalPricelistAPI.getWarrantyQuantityIUnit() != null) {
            CVGuaranteeUnit unit = em.getReference(CVGuaranteeUnit.class, generalPricelistAPI.getWarrantyQuantityIUnit()
                                                                                             .getId());
            generalPricelist.setWarrantyQuantityIUnit(unit);
        }

        // R
        if (generalPricelistAPI.getWarrantyQuantityRUnit() != null) {
            CVGuaranteeUnit unit = em.getReference(CVGuaranteeUnit.class, generalPricelistAPI.getWarrantyQuantityRUnit()
                                                                                             .getId());
            generalPricelist.setWarrantyQuantityRUnit(unit);
        }

        // TJ
        if (generalPricelistAPI.getWarrantyQuantityTJUnit() != null) {
            CVGuaranteeUnit unit = em.getReference(CVGuaranteeUnit.class, generalPricelistAPI.getWarrantyQuantityTJUnit()
                                                                                             .getId());
            generalPricelist.setWarrantyQuantityTJUnit(unit);
        }
    }

    private void setWarrantyValidFroms(GeneralPricelistAPI generalPricelistAPI, GeneralPricelist generalPricelist) {
        // warranty valid from
        if (generalPricelistAPI.getWarrantyValidFromH() != null) {
            CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(generalPricelistAPI.getWarrantyValidFromH()
                                                                                                                            .getCode());
            if (preventiveMaintenance != null) {
                generalPricelist.setWarrantyValidFromH(preventiveMaintenance);
            }
        }
        if (generalPricelistAPI.getWarrantyValidFromT() != null) {
            CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(generalPricelistAPI.getWarrantyValidFromT()
                                                                                                                            .getCode());
            if (preventiveMaintenance != null) {
                generalPricelist.setWarrantyValidFromT(preventiveMaintenance);
            }
        }
        if (generalPricelistAPI.getWarrantyValidFromI() != null) {
            CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(generalPricelistAPI.getWarrantyValidFromI()
                                                                                                                            .getCode());
            if (preventiveMaintenance != null) {
                generalPricelist.setWarrantyValidFromI(preventiveMaintenance);
            }
        }
        if (generalPricelistAPI.getWarrantyValidFromR() != null) {
            CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(generalPricelistAPI.getWarrantyValidFromR()
                                                                                                                            .getCode());
            if (preventiveMaintenance != null) {
                generalPricelist.setWarrantyValidFromR(preventiveMaintenance);
            }
        }
        if (generalPricelistAPI.getWarrantyValidFromTJ() != null) {
            CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(generalPricelistAPI.getWarrantyValidFromTJ()
                                                                                                                            .getCode());
            if (preventiveMaintenance != null) {
                generalPricelist.setWarrantyValidFromTJ(preventiveMaintenance);
            }
        }

    }

    public Set<SupplierGLNCatalogueUniqueNumber> result() {
        List<GeneralPricelistPricelistRow> resultList = em.createQuery("SELECT r FROM GeneralPricelistPricelistRow r ", GeneralPricelistPricelistRow.class)
                                                          .getResultList();

        return resultList.stream()
                         .map(SupplierGLNCatalogueUniqueNumber::of)
                         .collect(Collectors.toSet());
    }

    public void saveExportSettings(Customer customer, Set<ExportSetting> exportSettingsToMigrate) {
        List<GeneralPricelist> generalPricelists = exportSettingsToMigrate.stream()
                                                                          .map(ExportSetting::getSupplier)
                                                                          .filter(Objects::nonNull)
                                                                          .map(gp -> Unique.of(gp, Supplier::getId))
                                                                          .distinct()
                                                                          .map(Unique::data)
                                                                          .map(this::findBy)
                                                                          .filter(Objects::nonNull)
                                                                          .collect(Collectors.toList());
        Organization organization;
        BusinessLevel businessLevel = null;

        if (customer.isBusinessLevel()) {
            BusinessLevelAPI businessLevelAPI = hjmtjCustomerRepository.findBusinessLevel(customer);
            Objects.requireNonNull(businessLevelAPI, "businessLevelAPI must be non-null");

            businessLevel = new BusinessLevel();
            businessLevel.setUniqueId(businessLevelAPI.getId());

            organization = new Organization();
            organization.setUniqueId(businessLevelAPI.getOrganization()
                                                     .getId());
        } else {
            OrganizationAPI organizationAPI = hjmtjCustomerRepository.find(customer);
            Objects.requireNonNull(organizationAPI, "organizationAPI must be non-null");

            organization = new Organization();
            organization.setUniqueId(organizationAPI.getId());
        }

        if (customer.getExportFileName() != null) {
            // Important that GP is not duplicated
            List<GeneralPricelist> uniqueGP = generalPricelists.stream()
                                                               .map(gp -> Unique.of(gp, GeneralPricelist::getUniqueId))
                                                               .distinct()
                                                               .map(Unique::data)
                                                               .collect(Collectors.toList());

            ExportSettings exportSettings = new ExportSettings();
            exportSettings.setLastExported(null);
            exportSettings.setCreated(Calendar.getInstance()
                                              .getTime());
            exportSettings.setLastUpdated(Calendar.getInstance()
                                                  .getTime());
            exportSettings.setNumberOfExports(0);
            exportSettings.setFilename(customer.getExportFileName());
            exportSettings.setOrganization(organization);
            exportSettings.setBusinessLevel(businessLevel);
            exportSettings.setGeneralPricelists(uniqueGP);

            log.info("{} = {}, exported GPs = {}", customer.getOrganizationName(), customer.getExportFileName(), uniqueGP.size());
            em.persist(exportSettings);
        }
    }

    private GeneralPricelist findBy(Supplier supplier) {
        OrganizationAPI organizationAPI = hjmtjSupplierRepository.find(supplier);

        if (organizationAPI == null) {
            return null;
        }

        List<GeneralPricelist> resultList = em.createQuery("SELECT gp FROM GeneralPricelist gp " +
                "WHERE gp.ownerOrganization.uniqueId = :organizationId ", GeneralPricelist.class)
                                              .setParameter("organizationId", organizationAPI.getId())
                                              .getResultList();

        return resultList.size() > 0 ? resultList.get(0) : null;
    }

}

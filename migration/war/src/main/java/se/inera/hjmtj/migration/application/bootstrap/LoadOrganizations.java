/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.application.bootstrap;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.domain.HeapUsageInterceptor;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.domain.model.organization.entities.Customer;
import se.inera.hjmtj.migration.domain.model.organization.entities.LimeOrganization;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.domain.model.organization.entities.VisueraOrganization;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjUserRepository;
import se.inera.hjmtj.migration.infra.io.CsvPersistence;
import se.inera.hjmtj.migration.infra.organizations.CustomerRepository;
import se.inera.hjmtj.migration.infra.organizations.SupplierRepository;

import javax.ejb.PrePassivate;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * Loads suppliers into h2.
 */
@Slf4j
@Singleton
@Interceptors({PerformanceLogInterceptor.class, HeapUsageInterceptor.class})
public class LoadOrganizations {

    @Inject
    private HjmtjUserRepository hjmtjUsers;

    @Inject
    private SupplierRepository supplierRepository;

    @PersistenceContext(unitName = "local")
    private EntityManager em;

    @Inject
    private CustomerRepository customerRepository;

    private final Map<String, String> exportFileNames = new LinkedHashMap<>();

    public LoadOrganizations() {
        exportFileNames.put("Alvesta Kommun", "alvesta1");
        exportFileNames.put("Hälso- och sjukvårdsförvaltningen", "k_sthlm2");
        exportFileNames.put("Landstinget Blekinge, Hjälpmedelscenter", "blekinge1");
        exportFileNames.put("Landstinget Dalarna, LD Hjälpmedel", "dalarna1");
        exportFileNames.put("Region Gävleborg, Hjälpmedel SAM", "gavleborg1");
        exportFileNames.put("Region Gotland", "gotland1");
        exportFileNames.put("Region Halland", "halland1");
        exportFileNames.put("Region Jämtland Härjedalen", "jamtland1");
        exportFileNames.put("Landstinget Jönköpings län", "jonkoping1");
        exportFileNames.put("Kalmar kommun, Kommunal hjälpmedelssamverkan", "kalmar1");
        exportFileNames.put("Region Kronoberg, Hjälpmedelscentralen", "kronoberg1");
        exportFileNames.put("Ljungby Kommun", "ljungby1");
        exportFileNames.put("Malmö Stad", "malmo1");
        exportFileNames.put("Kommunalförbundet Medelpunkten", "medelpunkten1");
        exportFileNames.put("Region Norrbotten Länsservice Hjälpmedel", "norrbotten1");
        exportFileNames.put("Region Örebro län", "orebro1");
        exportFileNames.put("Hjälpmedelscentrum Östra Skåne Kristianstad kommun", "skane1");
        exportFileNames.put("Region Skåne, Hjälpmedel", "skane2");
        exportFileNames.put("Sodexo AB", "sodexo1");
        exportFileNames.put("Sodexo AB, Hjälpmedelsservice 10-gruppen", "sodexo2");
        exportFileNames.put("HjälpmedelsCentrum i Östergötland AB", "sodexo3");
        exportFileNames.put("Region Sörmland", "sormland1");
        exportFileNames.put("Hjälpmedel Stockholm", "sthlm1");
        exportFileNames.put("Förbrukningshjälpmedel i hemmet", "sthlm3");
        exportFileNames.put("Uppsala kommun", "uppsala1");
        exportFileNames.put("Uppvidinge kommun", "uppvidinge1");
        exportFileNames.put("Landstinget i Värmland", "varmland1");
        exportFileNames.put("Västerbottens läns landsting", "vasterbotten1");
        exportFileNames.put("Region Västernorrland, Hjälpmedel", "vasternorrland1");
        exportFileNames.put("Region Västmanland, Hjälpmedelscentrum", "vastmanland1");
        exportFileNames.put("Hjälpmedelscentralen Västra Götaland", "vastragotaland1");
        exportFileNames.put("Växjö Kommun", "vaxjo1");
    }

    @PrePassivate
    public void cleanUp() {
        //hjmtjOrganizations.removeAdmin();
        hjmtjUsers.delete(1);
    }

    public void run() {
        if (supplierRepository.isEmpty()) {
            // Delete any existing data
            em.createNativeQuery("DELETE FROM LimeOrganization")
              .executeUpdate();
            em.createNativeQuery("DELETE FROM VisueraOrganization")
              .executeUpdate();
            em.createNativeQuery("DELETE FROM CUSTOMER")
              .executeUpdate();
            em.createNativeQuery("DELETE FROM SUPPLIER")
              .executeUpdate();

            CsvPersistence.csvRead(em, "/kunder.csv", "Customer", "OrganizationName;Type;GLN;AddressCity;OrganizationNumber;ValidFrom");
            CsvPersistence.csvRead(em, "/organisation.csv", "VisueraOrganization", "Name;Type;GLN;Address;PostCode;City;Country;Telephone;Fax;Email;OrganizationNumber;Comment");
            CsvPersistence.csvRead(em, "/organisationlime.csv", "LimeOrganization", "ServiceName;ValidFrom;ConnectionType;Connection;Status;Name;NameInvoice;Category;OrganizationNumber;GLN;Address;PostCode;City;Country;Telephone");

            // Fix multiple ws in address and name from Lime
            requireSuccess(em.createNativeQuery(
                    "UPDATE LimeOrganization SET ADDRESS = REGEXP_REPLACE(ADDRESS, '\\s+', ' '), NAME = REGEXP_REPLACE(NAME, '\\s+,', ', ')")
                             .executeUpdate());
            // Fix multiple ws in address and name from Visuera
            requireSuccess(em.createNativeQuery(
                    "UPDATE VisueraOrganization SET ADDRESS = REGEXP_REPLACE(ADDRESS, '\\s+', ' '), NAME = REGEXP_REPLACE(NAME, '\\s+,', ', ')")
                             .executeUpdate());
            requireSuccess(em.createNativeQuery(
                    "UPDATE VisueraOrganization SET ADDRESS = REGEXP_REPLACE(ADDRESS, '\\s+,', ', ')")
                             .executeUpdate());

            List<LimeOrganization> limeOrganizations = em.createNativeQuery("SELECT * FROM LimeOrganization", LimeOrganization.class)
                                                         .getResultList();

            for (LimeOrganization csvLime : limeOrganizations) {
                Supplier supplier = new Supplier(csvLime);

                List<VisueraOrganization> visueraOrganizations = em.createNativeQuery("SELECT * FROM VisueraOrganization WHERE TYPE = 'Leverantörer' AND (GLN = :gln OR ORGANIZATIONNUMBER = :orgNr)", VisueraOrganization.class)
                                                                   .setParameter("gln", supplier.getGln())
                                                                   .setParameter("orgNr", supplier.getOrganizationNumber())
                                                                   .getResultList();

                if (visueraOrganizations.size() == 1) {
                    VisueraOrganization csvVisuera = visueraOrganizations.get(0);
                    supplier = new Supplier(csvVisuera, csvLime);
                } else if (visueraOrganizations.size() > 1) {
                    log.info("Several matches for {} in Visuera file. Any fields that are empty in Lime will remain empty.", supplier.getName());
                }

                em.persist(supplier);
            }
        }

        // Set export filenames
        if(exportFileNames.size() != 32){
            throw new IllegalStateException("Number of export files should be 32");
        }

        Map<String, Customer> organizationNameToCustomer = customerRepository.values()
                                                          .stream()
                                                          .collect(Collectors.toMap(Customer::getOrganizationName, Function.identity()));
        for (Map.Entry<String, String> entry : exportFileNames.entrySet()) {
            String organizationName = entry.getKey();
            String exportFileName = entry.getValue();
            Customer customer = organizationNameToCustomer.get(organizationName);

            if(customer == null){
                throw new IllegalStateException("Could not find any organization named " + organizationName);
            }

            customer.setExportFileName(exportFileName);
            em.merge(customer);
            log.info("{} = {}", organizationName, exportFileName);
        }
    }

    private void requireSuccess(int value) {
        if (value == 0) {
            throw new RuntimeException("Execute update did not update any row(s)");
        }
    }

}

/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.product.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.domain.model.BlindReference;
import se.inera.hjmtj.migration.domain.model.Reference;
import se.inera.hjmtj.migration.domain.model.result.AssistiveProductReport;
import se.inera.hjmtj.migration.domain.xml.TrimStringAdapter;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PersistenceUnit;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;

@PersistenceUnit(unitName = "local")
@Slf4j
@Entity
@Table(indexes = {
        @Index(columnList = "id"),
        @Index(columnList = "isTemplateProduct"),
        @Index(columnList = "basedOnTemplateProduct"),
        @Index(columnList = "id,basedOnTemplateProduct"),
        @Index(columnList = "id,basedOnTemplateProduct,vgrProdNo"),
        @Index(columnList = "fourthLevel"),
        @Index(columnList = "lastCategoryCode"),
        @Index(columnList = "lastCategoryName"),
})
@XmlRootElement(name = "Product")
@Getter
@Setter
@NamedNativeQuery(name = Product.COUNT_ALL, query = "select count(*)\n" +
        "from PRODUCT p\n" +
        "       JOIN PRODUCTORGANIZATION o on p.SUPPLIER_ID = o.ID\n" +
        "       JOIN SUPPLIER s on o.SUPPLIER_ID = S.ID\n" +
        "WHERE  p.ISO9999 = TRUE AND\n" +
        "  p.NOARTICLETYPE = FALSE\n")
@NamedQueries({@NamedQuery(name = Product.FIND_DUPLICATE_NUMBERS,query = "SELECT p FROM Product p " +
                "group by p.supplier, p.type, lower(p.supplierProductNumber) " +
                "having count(p) > 1"),
        @NamedQuery(name = Product.FIND_ALL, query = "SELECT p FROM Product p " +
                "WHERE p.supplier IS NOT NULL AND " +
                "p.xmlCategories.iso9999 = TRUE AND " +
                "p.noArticleType = FALSE")
})
public class Product {

    public static final String FIND_DUPLICATE_NUMBERS = "Product.FIND_DUPLICATE_NUMBERS";

    public static final String COUNT_ALL = "Product.COUNT_ALL";

    public static final String FIND_ALL = "Product.FIND_ALL";

    @ElementCollection(fetch = FetchType.LAZY)
    @XmlElement(name = "ReplacesList")
    private List<BlindReference> replacesList;

    @ElementCollection(fetch = FetchType.LAZY)
    @XmlElement(name = "_x0035_c128a39d932418d93e4c63576ae67d6")
    private Set<BlindReference> accessoryTo;

    @ElementCollection(fetch = FetchType.LAZY)
    @XmlElement(name = "SettingFor")
    private Set<BlindReference> settingFor;

    @ElementCollection(fetch = FetchType.LAZY)
    @XmlElement(name = "ServiceFor")
    private Set<BlindReference> serviceFor;

    @ElementCollection(fetch = FetchType.LAZY)
    @XmlElement(name = "_x0036_fc8b90656504d6aac2274cffb074fff")
    private Set<BlindReference> sparePartFor;

    @Embedded
    @AttributeOverride(name = "idRef", column = @Column(name = "basedOnTemplateProduct"))
    @XmlElement(name = "BasedOnTemplateProduct")
    private BlindReference basedOnTemplateProduct;

    @XmlAttribute(name = "id")
    private Long id;

    @Id
    @XmlElement(name = "VGRProdNo")
    private Long vgrProdNo;

    @ManyToMany(fetch = FetchType.LAZY)
    private Set<ProductClassification> classifications;

    @Embedded
    @XmlElement(name = "CategoryPath")
    private ProductCategoryPath xmlCategories;

    @ManyToOne(fetch = FetchType.LAZY)
    private ProductCategoryProduct referencedCategory;

    @Embedded
    @XmlElement(name = "_x0031_7fa973736524d658b7b944561195907")
    private ProductMarking productMarking;

    @XmlElement(name = "_x0031_1fd813b4adb47f58d1d2e4bf67ce658")
    private String isoCategory;

    @Embedded
    @XmlElement(name = "Bild")
    private ProductFile mainImage;

    @XmlElement(name = "ExpireTime")
    private String expireTime;

    @ManyToOne(fetch = FetchType.LAZY)
    private ProductUnit orderUnit;

    @XmlElement(name = "Package1")
    private BigDecimal package1;

    @XmlElement(name = "Package2")
    private BigDecimal package2;

    @ManyToOne(fetch = FetchType.LAZY)
    private ProductOrganization supplier;

    @XmlJavaTypeAdapter(TrimStringAdapter.class)
    @Column(columnDefinition = "VARCHAR", length = 1024)
    @XmlElement(name = "SupplierProductName")
    private String supplierProductName;

    @XmlJavaTypeAdapter(TrimStringAdapter.class)
    @XmlElement(name = "SupplierProductNumber")
    private String supplierProductNumber;

    @XmlElement(name = "UpdatedTime")
    private String updatedTime;

    @Column(columnDefinition = "VARCHAR", length = 1024)
    @XmlElement(name = "d0823a697d0f40dc97c13f2bda60c6d9")
    private String supplementalInformation;

    @OneToMany(fetch = FetchType.LAZY, cascade = ALL)
    private Set<ProductImage> images;

    @XmlJavaTypeAdapter(TrimStringAdapter.class)
    @XmlElement(name = "GTIN")
    private String gtin;

    @XmlJavaTypeAdapter(TrimStringAdapter.class)
    @XmlElement(name = "Brand")
    private String brand;

    @XmlElement(name = "CreatedTime")
    private LocalDateTime createdTime;

    @XmlElement(name = "VGRMinOrderQty")
    private Integer vgrMinOrderQty;

    @OneToMany(fetch = FetchType.LAZY, cascade = ALL)
    private Set<ProductDocument> documents;

    @XmlElement(name = "IsTemplateProduct")
    private Boolean isTemplateProduct;

    @XmlElement(name = "PackagePallet")
    private BigDecimal packagePallet;

    @XmlElement(name = "PackQty")
    private BigDecimal packQty;

    @ManyToOne(fetch = FetchType.LAZY)
    private ProductUnit packUnit;

    @XmlJavaTypeAdapter(TrimStringAdapter.class)
    @Column(columnDefinition = "VARCHAR", length = 1024)
    @XmlElement(name = "ba8f9f24d28a4ff9ab01899d24fab837")
    private String manufacturerWebUrl;

    @XmlJavaTypeAdapter(TrimStringAdapter.class)
    @XmlElement(name = "Manufacture")
    private String manufacturerName;

    @XmlJavaTypeAdapter(TrimStringAdapter.class)
    @XmlElement(name = "ManufacturerProductNumber")
    private String manufacturerProductNumber;

    @OneToMany(fetch = FetchType.LAZY, cascade = ALL)
    private Set<ProductCategorySpecificProperty> categorySpecificProperties;

    @XmlJavaTypeAdapter(TrimStringAdapter.class)
    @XmlElement(name = "_x0034_2220c59dd674226bdd8488b73f5837c")
    private String color;

    @XmlElement(name = "IsCustom")
    private Boolean customerUnique;

    @Enumerated(EnumType.STRING)
    private AssistiveProductReport.Type type;

    private boolean noArticleType;

    public Product() {
        categorySpecificProperties = new HashSet<>();
        this.serviceFor = new HashSet<>();
        this.settingFor = new HashSet<>();
        this.sparePartFor = new HashSet<>();
        this.accessoryTo = new HashSet<>();
        images = new HashSet<>();
        documents = new HashSet<>();
        classifications = new HashSet<>();
    }

    @XmlElement(name = "Documents")
    private void setXmlDocuments(ProductDocument productDocument) {
        if (productDocument != null &&
                productDocument.getFile() != null &&
                productDocument.getFile()
                               .getOriginalContentUrl() != null) {
            documents.add(productDocument);
        }
    }

    @XmlElement(name = "Images")
    private void setXmlOtherImages(ProductImage productImage) {
        if (productImage != null &&
                productImage.getFile() != null &&
                productImage.getFile()
                            .getOriginalContentUrl() != null) {
            images.add(productImage);
        }
    }

    @XmlElement(name = "OrderUnit")
    public void setXmlOrderUnit(Reference<ProductUnit> reference) {
        orderUnit = reference.getReference();
    }

    @XmlElement(name = "Supplier")
    public void setXmlSupplier(Reference<ProductOrganization> reference) {
        supplier = reference.getReference();
    }

    public void setSupplier(ProductOrganization supplier) {
        this.supplier = supplier;
    }

    @XmlElement(name = "Classification")
    public void setXmlClassifications(Reference<ProductClassification> reference) {
        classifications.add(reference.getReference());
    }

    @XmlElement(name = "ReferencedCategory")
    public void setXmlReferencedClassification(Reference<ProductCategoryProduct> reference) {
        referencedCategory = reference.getReference();
    }

    @XmlElement(name = "PackUnit")
    public void setXmlPackUnit(Reference<ProductUnit> reference) {
        packUnit = reference.getReference();
    }

    public int countFits() {
        return accessoryTo.size() + sparePartFor.size() + settingFor.size() + serviceFor.size();
    }

    @Override
    public int hashCode() {
        return Objects.hash(vgrProdNo);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(vgrProdNo, product.vgrProdNo);
    }

    @XmlElement(name = "_x0030_1f1945644754551b13c0ce2c0da5d29")
    public void set_x0030_1f1945644754551b13c0ce2c0da5d29(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0030_1f1945644754551b13c0ce2c0da5d29", xmlReference));
        }
    }

    @XmlElement(name = "_x0030_2844d426f9142678d59e6efdcb1f43e")
    public void set_x0030_2844d426f9142678d59e6efdcb1f43e(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0030_2844d426f9142678d59e6efdcb1f43e", xmlReference));
        }
    }

    @XmlElement(name = "_x0030_3645facacad4643819fa52a374e319c")
    public void set_x0030_3645facacad4643819fa52a374e319c(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0030_3645facacad4643819fa52a374e319c", xmlReference));
        }
    }

    @XmlElement(name = "_x0030_4a8eb135db44fa5ae194bbfaccc1b7d")
    public void set_x0030_4a8eb135db44fa5ae194bbfaccc1b7d(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0030_4a8eb135db44fa5ae194bbfaccc1b7d", value));
        }
    }

    @XmlElement(name = "_x0030_534bc83eda44314a0e645ea117cd9e8")
    public void set_x0030_534bc83eda44314a0e645ea117cd9e8(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0030_534bc83eda44314a0e645ea117cd9e8", value));
        }
    }

    @XmlElement(name = "_x0030_5f2bf48d68740959015d01ca2efb0c7")
    public void set_x0030_5f2bf48d68740959015d01ca2efb0c7(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0030_5f2bf48d68740959015d01ca2efb0c7", value));
        }
    }

    @XmlElement(name = "_x0030_70448179a594149907c41a4cf4bb61d")
    public void set_x0030_70448179a594149907c41a4cf4bb61d(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0030_70448179a594149907c41a4cf4bb61d", xmlReference));
        }
    }

    @XmlElement(name = "_x0030_9e0e0da4c264b8a98bb5ee90705e71c")
    public void set_x0030_9e0e0da4c264b8a98bb5ee90705e71c(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0030_9e0e0da4c264b8a98bb5ee90705e71c", value));
        }
    }

    @XmlElement(name = "_x0030_a27f8c251544fc8abff3309c695821f")
    public void set_x0030_a27f8c251544fc8abff3309c695821f(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0030_a27f8c251544fc8abff3309c695821f", xmlReference));
        }
    }

    @XmlElement(name = "_x0030_beccd1cd7a84581b0651a90f4f98ac0")
    public void set_x0030_beccd1cd7a84581b0651a90f4f98ac0(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0030_beccd1cd7a84581b0651a90f4f98ac0", xmlReference));
        }
    }

    @XmlElement(name = "_x0030_c59bad7af694344a6fe244ca09bdda9")
    public void set_x0030_c59bad7af694344a6fe244ca09bdda9(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0030_c59bad7af694344a6fe244ca09bdda9", xmlReference));
        }
    }

    @XmlElement(name = "_x0030_c86d047cf0d4119a3c14e418f54ff46")
    public void set_x0030_c86d047cf0d4119a3c14e418f54ff46(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0030_c86d047cf0d4119a3c14e418f54ff46", value));
        }
    }

    @XmlElement(name = "_x0030_c89c1a5d10d4e1a87ed80473e63b121")
    public void set_x0030_c89c1a5d10d4e1a87ed80473e63b121(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0030_c89c1a5d10d4e1a87ed80473e63b121", xmlReference));
        }
    }

    @XmlElement(name = "_x0030_d9596b6e4c24c1c9a8bc30477d0b697")
    public void set_x0030_d9596b6e4c24c1c9a8bc30477d0b697(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0030_d9596b6e4c24c1c9a8bc30477d0b697", value));
        }
    }

    @XmlElement(name = "_x0030_dac8a48458a490487e35d050f5cced2")
    public void set_x0030_dac8a48458a490487e35d050f5cced2(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0030_dac8a48458a490487e35d050f5cced2", value));
        }
    }

    @XmlElement(name = "_x0031_31d9b7ab19f40658fa6d523539e4dde")
    public void set_x0031_31d9b7ab19f40658fa6d523539e4dde(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0031_31d9b7ab19f40658fa6d523539e4dde", xmlReference));
        }
    }

    @XmlElement(name = "_x0031_38ec53206b640fd8a88960c68d261c9")
    public void set_x0031_38ec53206b640fd8a88960c68d261c9(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0031_38ec53206b640fd8a88960c68d261c9", value));
        }
    }

    @XmlElement(name = "_x0031_6d46168ad5b425ea3839146b16dadc2")
    public void set_x0031_6d46168ad5b425ea3839146b16dadc2(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0031_6d46168ad5b425ea3839146b16dadc2", value));
        }
    }

    @XmlElement(name = "_x0031_78541f1c0dc4682b34b73763e950470")
    public void set_x0031_78541f1c0dc4682b34b73763e950470(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0031_78541f1c0dc4682b34b73763e950470", xmlReference));
        }
    }

    @XmlElement(name = "_x0031_7dbb38728454989972412485fe270ed")
    public void set_x0031_7dbb38728454989972412485fe270ed(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0031_7dbb38728454989972412485fe270ed", xmlReference));
        }
    }

    @XmlElement(name = "_x0031_97d1e7447cf472abecb53df436e1c7d")
    public void set_x0031_97d1e7447cf472abecb53df436e1c7d(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0031_97d1e7447cf472abecb53df436e1c7d", xmlReference));
        }
    }

    @XmlElement(name = "_x0031_af5f30226cf4a1e835baac9084db28b")
    public void set_x0031_af5f30226cf4a1e835baac9084db28b(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0031_af5f30226cf4a1e835baac9084db28b", xmlReference));
        }
    }

    @XmlElement(name = "_x0031_d121eac640d4eecbb0bb7c868499cc6")
    public void set_x0031_d121eac640d4eecbb0bb7c868499cc6(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0031_d121eac640d4eecbb0bb7c868499cc6", value));
        }
    }

    @XmlElement(name = "_x0031_f95e20f997f45d0b6c33ba41e7185b4")
    public void set_x0031_f95e20f997f45d0b6c33ba41e7185b4(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0031_f95e20f997f45d0b6c33ba41e7185b4", xmlReference));
        }
    }

    @XmlElement(name = "_x0032_0e4559b107947d8a7eda7f93744a97a")
    public void set_x0032_0e4559b107947d8a7eda7f93744a97a(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0032_0e4559b107947d8a7eda7f93744a97a", value));
        }
    }

    @XmlElement(name = "_x0032_698504e72304a77b9c3c186ae115780")
    public void set_x0032_698504e72304a77b9c3c186ae115780(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0032_698504e72304a77b9c3c186ae115780", value));
        }
    }

    @XmlElement(name = "_x0032_764199885f0495d9b765a437bbd10b8")
    public void set_x0032_764199885f0495d9b765a437bbd10b8(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0032_764199885f0495d9b765a437bbd10b8", xmlReference));
        }
    }

    @XmlElement(name = "_x0032_767659a64bc46bd9f295d18ca601f4b")
    public void set_x0032_767659a64bc46bd9f295d18ca601f4b(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0032_767659a64bc46bd9f295d18ca601f4b", value));
        }
    }

    @XmlElement(name = "_x0032_8a3b60ff287405e9cb67252181c632e")
    public void set_x0032_8a3b60ff287405e9cb67252181c632e(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0032_8a3b60ff287405e9cb67252181c632e", xmlReference));
        }
    }

    @XmlElement(name = "_x0032_948f7620d2e492095fa6cc7efae40fa")
    public void set_x0032_948f7620d2e492095fa6cc7efae40fa(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0032_948f7620d2e492095fa6cc7efae40fa", xmlReference));
        }
    }

    @XmlElement(name = "_x0032_964e748f78849a986d498600df82b78")
    public void set_x0032_964e748f78849a986d498600df82b78(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0032_964e748f78849a986d498600df82b78", xmlReference));
        }
    }

    @XmlElement(name = "_x0032_af30bb3808f474bb81f88dd78112897")
    public void set_x0032_af30bb3808f474bb81f88dd78112897(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0032_af30bb3808f474bb81f88dd78112897", value));
        }
    }

    @XmlElement(name = "_x0032_b6b56a6c064435f9f513e33a9866e99")
    public void set_x0032_b6b56a6c064435f9f513e33a9866e99(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0032_b6b56a6c064435f9f513e33a9866e99", value));
        }
    }

    @XmlElement(name = "_x0032_f8bbcffe6ff422391c90f498785160e")
    public void set_x0032_f8bbcffe6ff422391c90f498785160e(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0032_f8bbcffe6ff422391c90f498785160e", value));
        }
    }

    @XmlElement(name = "_x0032_fb3c1645fe54dab9ba9c229c1a9e1ed")
    public void set_x0032_fb3c1645fe54dab9ba9c229c1a9e1ed(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0032_fb3c1645fe54dab9ba9c229c1a9e1ed", value));
        }
    }

    @XmlElement(name = "_x0033_39cf774c0844769aee9c18dd8b7d788")
    public void set_x0033_39cf774c0844769aee9c18dd8b7d788(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0033_39cf774c0844769aee9c18dd8b7d788", xmlReference));
        }
    }

    @XmlElement(name = "_x0033_678692735b64f069684171ce9fd3001")
    public void set_x0033_678692735b64f069684171ce9fd3001(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0033_678692735b64f069684171ce9fd3001", value));
        }
    }

    @XmlElement(name = "_x0033_789ce602e9a4f70a05ed34e889e89d3")
    public void set_x0033_789ce602e9a4f70a05ed34e889e89d3(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0033_789ce602e9a4f70a05ed34e889e89d3", xmlReference));
        }
    }

    @XmlElement(name = "_x0033_7f6b0bced4f44e1be182b14c6220f95")
    public void set_x0033_7f6b0bced4f44e1be182b14c6220f95(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0033_7f6b0bced4f44e1be182b14c6220f95", xmlReference));
        }
    }

    @XmlElement(name = "_x0033_a4f15add3e2434eade71533bff488cb")
    public void set_x0033_a4f15add3e2434eade71533bff488cb(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0033_a4f15add3e2434eade71533bff488cb", value));
        }
    }

    @XmlElement(name = "_x0033_b331befca4e432a9f7ffd08d5e8bbbd")
    public void set_x0033_b331befca4e432a9f7ffd08d5e8bbbd(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0033_b331befca4e432a9f7ffd08d5e8bbbd", xmlReference));
        }
    }

    @XmlElement(name = "_x0033_b7a9ae2076542c8acdc6bde946db4a2")
    public void set_x0033_b7a9ae2076542c8acdc6bde946db4a2(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0033_b7a9ae2076542c8acdc6bde946db4a2", value));
        }
    }

    @XmlElement(name = "_x0033_c01f742716e49fa9d28e7c51e0ab1be")
    public void set_x0033_c01f742716e49fa9d28e7c51e0ab1be(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0033_c01f742716e49fa9d28e7c51e0ab1be", xmlReference));
        }
    }

    @XmlElement(name = "_x0033_edb620fdf1d44df96130dc522a1fac1")
    public void set_x0033_edb620fdf1d44df96130dc522a1fac1(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0033_edb620fdf1d44df96130dc522a1fac1", xmlReference));
        }
    }

    @XmlElement(name = "_x0033_f5f45631e1a421788a1cdd810fa30f6")
    public void set_x0033_f5f45631e1a421788a1cdd810fa30f6(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0033_f5f45631e1a421788a1cdd810fa30f6", xmlReference));
        }
    }

    @XmlElement(name = "_x0033_f6029c4a6014f60a69eb629d731904f")
    public void set_x0033_f6029c4a6014f60a69eb629d731904f(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0033_f6029c4a6014f60a69eb629d731904f", value));
        }
    }

    @XmlElement(name = "_x0033_fcfde5307dc41af9dd1f18250cfc121")
    public void set_x0033_fcfde5307dc41af9dd1f18250cfc121(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0033_fcfde5307dc41af9dd1f18250cfc121", xmlReference));
        }
    }

    @XmlElement(name = "_x0034_0d2c3d671884b86877a78f0680a4e77")
    public void set_x0034_0d2c3d671884b86877a78f0680a4e77(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0034_0d2c3d671884b86877a78f0680a4e77", value));
        }
    }

    @XmlElement(name = "_x0034_4a54d78125f49ff84ff490d825f7368")
    public void set_x0034_4a54d78125f49ff84ff490d825f7368(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0034_4a54d78125f49ff84ff490d825f7368", xmlReference));
        }
    }

    @XmlElement(name = "_x0034_63c20ad823c4ba68c7945603847b323")
    public void set_x0034_63c20ad823c4ba68c7945603847b323(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0034_63c20ad823c4ba68c7945603847b323", value));
        }
    }

    @XmlElement(name = "_x0034_74d14b5c3e849eaa0c8b105084569fc")
    public void set_x0034_74d14b5c3e849eaa0c8b105084569fc(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0034_74d14b5c3e849eaa0c8b105084569fc", value));
        }
    }

    @XmlElement(name = "_x0034_76ad04d0f35427594624ab43b89c0d6")
    public void set_x0034_76ad04d0f35427594624ab43b89c0d6(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0034_76ad04d0f35427594624ab43b89c0d6", xmlReference));
        }
    }

    @XmlElement(name = "_x0034_86a7648fb7c4de58c411ad33b956d2d")
    public void set_x0034_86a7648fb7c4de58c411ad33b956d2d(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0034_86a7648fb7c4de58c411ad33b956d2d", value));
        }
    }

    @XmlElement(name = "_x0034_910109d968e4ff8b375aa8028d54b4e")
    public void set_x0034_910109d968e4ff8b375aa8028d54b4e(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0034_910109d968e4ff8b375aa8028d54b4e", value));
        }
    }

    @XmlElement(name = "_x0034_a1645d308cf4903a953045880a9a3f7")
    public void set_x0034_a1645d308cf4903a953045880a9a3f7(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0034_a1645d308cf4903a953045880a9a3f7", xmlReference));
        }
    }

    @XmlElement(name = "_x0034_b72ab5795564382a958d132167cec51")
    public void set_x0034_b72ab5795564382a958d132167cec51(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0034_b72ab5795564382a958d132167cec51", value));
        }
    }

    @XmlElement(name = "_x0035_1b836c796c74877acdfc72845e23d53")
    public void set_x0035_1b836c796c74877acdfc72845e23d53(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0035_1b836c796c74877acdfc72845e23d53", value));
        }
    }

    @XmlElement(name = "_x0035_2255f718efe47f59ff91d8b0398703d")
    public void set_x0035_2255f718efe47f59ff91d8b0398703d(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0035_2255f718efe47f59ff91d8b0398703d", value));
        }
    }

    @XmlElement(name = "_x0035_2c25d817cc74e60b4c1b437fca29bd3")
    public void set_x0035_2c25d817cc74e60b4c1b437fca29bd3(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0035_2c25d817cc74e60b4c1b437fca29bd3", value));
        }
    }

    @XmlElement(name = "_x0035_2d28337489541a3ad271f90f01db014")
    public void set_x0035_2d28337489541a3ad271f90f01db014(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0035_2d28337489541a3ad271f90f01db014", value));
        }
    }

    @XmlElement(name = "_x0035_5e45f8bcbcc44fb8e24e45608bc9c66")
    public void set_x0035_5e45f8bcbcc44fb8e24e45608bc9c66(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0035_5e45f8bcbcc44fb8e24e45608bc9c66", xmlReference));
        }
    }

    @XmlElement(name = "_x0035_74c36bf4ae94090a85989a520e3f72f")
    public void set_x0035_74c36bf4ae94090a85989a520e3f72f(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0035_74c36bf4ae94090a85989a520e3f72f", xmlReference));
        }
    }

    @XmlElement(name = "_x0035_7efb1ad2aee4daa8cc24a5145c2e4a3")
    public void set_x0035_7efb1ad2aee4daa8cc24a5145c2e4a3(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0035_7efb1ad2aee4daa8cc24a5145c2e4a3", xmlReference));
        }
    }

    @XmlElement(name = "_x0035_978af2a729f461a994dbbbbd1493553")
    public void set_x0035_978af2a729f461a994dbbbbd1493553(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0035_978af2a729f461a994dbbbbd1493553", xmlReference));
        }
    }

    @XmlElement(name = "_x0035_99dddd3d2d04a33840fe145c6c6f51b")
    public void set_x0035_99dddd3d2d04a33840fe145c6c6f51b(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0035_99dddd3d2d04a33840fe145c6c6f51b", value));
        }
    }

    @XmlElement(name = "_x0035_9bc7b5884374a2faa92ac65a32403c0")
    public void set_x0035_9bc7b5884374a2faa92ac65a32403c0(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0035_9bc7b5884374a2faa92ac65a32403c0", xmlReference));
        }
    }

    @XmlElement(name = "_x0035_9efa5788fa2434c912a23891e7db280")
    public void set_x0035_9efa5788fa2434c912a23891e7db280(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0035_9efa5788fa2434c912a23891e7db280", value));
        }
    }

    @XmlElement(name = "_x0035_af103c8114540ff9e920e36688b2aa6")
    public void set_x0035_af103c8114540ff9e920e36688b2aa6(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0035_af103c8114540ff9e920e36688b2aa6", value));
        }
    }

    @XmlElement(name = "_x0035_afea9d19741401a8c700a123bdb9ce2")
    public void set_x0035_afea9d19741401a8c700a123bdb9ce2(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0035_afea9d19741401a8c700a123bdb9ce2", value));
        }
    }

    @XmlElement(name = "_x0035_dba5093746548d3b488c3c818642d6f")
    public void set_x0035_dba5093746548d3b488c3c818642d6f(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0035_dba5093746548d3b488c3c818642d6f", value));
        }
    }

    @XmlElement(name = "_x0035_e0b934657444397853b9775e570bbcf")
    public void set_x0035_e0b934657444397853b9775e570bbcf(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0035_e0b934657444397853b9775e570bbcf", value));
        }
    }

    @XmlElement(name = "_x0035_eb62431cab04344b19b662aea0b550b")
    public void set_x0035_eb62431cab04344b19b662aea0b550b(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0035_eb62431cab04344b19b662aea0b550b", value));
        }
    }

    @XmlElement(name = "_x0035_fa7de7eab8b402898a2b99e8d986369")
    public void set_x0035_fa7de7eab8b402898a2b99e8d986369(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0035_fa7de7eab8b402898a2b99e8d986369", xmlReference));
        }
    }

    @XmlElement(name = "_x0035_ff648b2c1504d9097b47394c3cd96bc")
    public void set_x0035_ff648b2c1504d9097b47394c3cd96bc(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0035_ff648b2c1504d9097b47394c3cd96bc", value));
        }
    }

    @XmlElement(name = "_x0036_06c230df14f411da5dafb3231b6a90c")
    public void set_x0036_06c230df14f411da5dafb3231b6a90c(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0036_06c230df14f411da5dafb3231b6a90c", xmlReference));
        }
    }

    @XmlElement(name = "_x0036_2790d2fff2e4e82acbe383829a83f7a")
    public void set_x0036_2790d2fff2e4e82acbe383829a83f7a(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0036_2790d2fff2e4e82acbe383829a83f7a", value));
        }
    }

    @XmlElement(name = "_x0036_29e71a3aa304f0ba4a5b62b72a8a260")
    public void set_x0036_29e71a3aa304f0ba4a5b62b72a8a260(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0036_29e71a3aa304f0ba4a5b62b72a8a260", value));
        }
    }

    @XmlElement(name = "_x0036_32e3ffdc89a48489caeafb56dced69c")
    public void set_x0036_32e3ffdc89a48489caeafb56dced69c(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0036_32e3ffdc89a48489caeafb56dced69c", xmlReference));
        }
    }

    @XmlElement(name = "_x0036_3a313f0173348e1aaa37abab6abd114")
    public void set_x0036_3a313f0173348e1aaa37abab6abd114(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0036_3a313f0173348e1aaa37abab6abd114", xmlReference));
        }
    }

    @XmlElement(name = "_x0036_3bf73a58d75471da6d6dd703c82dc3b")
    public void set_x0036_3bf73a58d75471da6d6dd703c82dc3b(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0036_3bf73a58d75471da6d6dd703c82dc3b", value));
        }
    }

    @XmlElement(name = "_x0036_40e7cf89a7e480192e2bff583d2b80f")
    public void set_x0036_40e7cf89a7e480192e2bff583d2b80f(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0036_40e7cf89a7e480192e2bff583d2b80f", xmlReference));
        }
    }

    @XmlElement(name = "_x0036_49a6393ea554e15a2633eb1e7602ec5")
    public void set_x0036_49a6393ea554e15a2633eb1e7602ec5(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0036_49a6393ea554e15a2633eb1e7602ec5", value));
        }
    }

    @XmlElement(name = "_x0036_5e011412a614eb5b2b9e0f26066aee8")
    public void set_x0036_5e011412a614eb5b2b9e0f26066aee8(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0036_5e011412a614eb5b2b9e0f26066aee8", xmlReference));
        }
    }

    @XmlElement(name = "_x0036_7ad17ef66674dc1a04ac99d270510fd")
    public void set_x0036_7ad17ef66674dc1a04ac99d270510fd(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0036_7ad17ef66674dc1a04ac99d270510fd", value));
        }
    }

    @XmlElement(name = "_x0036_899bdafabac45de9370f1fedb8d6d7a")
    public void set_x0036_899bdafabac45de9370f1fedb8d6d7a(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0036_899bdafabac45de9370f1fedb8d6d7a", value));
        }
    }

    @XmlElement(name = "_x0036_ae1ace7937642b08f1cfc43f59862ba")
    public void set_x0036_ae1ace7937642b08f1cfc43f59862ba(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0036_ae1ace7937642b08f1cfc43f59862ba", value));
        }
    }

    @XmlElement(name = "_x0036_c5669e4b85a445c8f9c905b0c39626b")
    public void set_x0036_c5669e4b85a445c8f9c905b0c39626b(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0036_c5669e4b85a445c8f9c905b0c39626b", xmlReference));
        }
    }

    @XmlElement(name = "_x0037_123607ed5b94afda7b672e2ab89a40f")
    public void set_x0037_123607ed5b94afda7b672e2ab89a40f(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0037_123607ed5b94afda7b672e2ab89a40f", value));
        }
    }

    @XmlElement(name = "_x0037_22e437dc5ae4a3b9a4a94a9f8848d8d")
    public void set_x0037_22e437dc5ae4a3b9a4a94a9f8848d8d(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0037_22e437dc5ae4a3b9a4a94a9f8848d8d", value));
        }
    }

    @XmlElement(name = "_x0037_48845bbddd1422188c452b5dc16ff31")
    public void set_x0037_48845bbddd1422188c452b5dc16ff31(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0037_48845bbddd1422188c452b5dc16ff31", xmlReference));
        }
    }

    @XmlElement(name = "_x0037_63e271e78d54ad3a2bdfab6bc82ef48")
    public void set_x0037_63e271e78d54ad3a2bdfab6bc82ef48(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0037_63e271e78d54ad3a2bdfab6bc82ef48", xmlReference));
        }
    }

    @XmlElement(name = "_x0037_93988d24004468b91cdb3a85a3f34dc")
    public void set_x0037_93988d24004468b91cdb3a85a3f34dc(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0037_93988d24004468b91cdb3a85a3f34dc", xmlReference));
        }
    }

    @XmlElement(name = "_x0037_9af68b9fd7c4bbbae503474d10f7573")
    public void set_x0037_9af68b9fd7c4bbbae503474d10f7573(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0037_9af68b9fd7c4bbbae503474d10f7573", xmlReference));
        }
    }

    @XmlElement(name = "_x0037_c41b8b30f7b4c7a880b6169440c70aa")
    public void set_x0037_c41b8b30f7b4c7a880b6169440c70aa(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0037_c41b8b30f7b4c7a880b6169440c70aa", value));
        }
    }

    @XmlElement(name = "_x0037_dd684f343cb415bb5319c2dbe3253ae")
    public void set_x0037_dd684f343cb415bb5319c2dbe3253ae(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0037_dd684f343cb415bb5319c2dbe3253ae", value));
        }
    }

    @XmlElement(name = "_x0037_fc4510efec940d69c0d2f26e39fc29f")
    public void set_x0037_fc4510efec940d69c0d2f26e39fc29f(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0037_fc4510efec940d69c0d2f26e39fc29f", xmlReference));
        }
    }

    @XmlElement(name = "_x0038_05820b3fc164ffe9f75e99beb24a0b6")
    public void set_x0038_05820b3fc164ffe9f75e99beb24a0b6(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0038_05820b3fc164ffe9f75e99beb24a0b6", value));
        }
    }

    @XmlElement(name = "_x0038_16f8a6507914108bfc923afb5afe2c2")
    public void set_x0038_16f8a6507914108bfc923afb5afe2c2(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0038_16f8a6507914108bfc923afb5afe2c2", value));
        }
    }

    @XmlElement(name = "_x0038_3d44276339749e6b581f90a6482de87")
    public void set_x0038_3d44276339749e6b581f90a6482de87(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0038_3d44276339749e6b581f90a6482de87", value));
        }
    }

    @XmlElement(name = "_x0038_772a2cb7e4141b49886afd51dcdc3b6")
    public void set_x0038_772a2cb7e4141b49886afd51dcdc3b6(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0038_772a2cb7e4141b49886afd51dcdc3b6", xmlReference));
        }
    }

    @XmlElement(name = "_x0038_8e1d8a78b9049e89a15fc2226896111")
    public void set_x0038_8e1d8a78b9049e89a15fc2226896111(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0038_8e1d8a78b9049e89a15fc2226896111", value));
        }
    }

    @XmlElement(name = "_x0038_a25b335beab474eab1790f1a52cf01c")
    public void set_x0038_a25b335beab474eab1790f1a52cf01c(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0038_a25b335beab474eab1790f1a52cf01c", xmlReference));
        }
    }

    @XmlElement(name = "_x0038_ac5b3e820024daca312dada9146a687")
    public void set_x0038_ac5b3e820024daca312dada9146a687(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0038_ac5b3e820024daca312dada9146a687", xmlReference));
        }
    }

    @XmlElement(name = "_x0038_dde2145ab9a4f5db2c511a2b699f566")
    public void set_x0038_dde2145ab9a4f5db2c511a2b699f566(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0038_dde2145ab9a4f5db2c511a2b699f566", value));
        }
    }

    @XmlElement(name = "_x0039_090d93b29be43b9a15eacea556dc835")
    public void set_x0039_090d93b29be43b9a15eacea556dc835(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0039_090d93b29be43b9a15eacea556dc835", xmlReference));
        }
    }

    @XmlElement(name = "_x0039_0c24a6b9742473f8b613a6256cbf436")
    public void set_x0039_0c24a6b9742473f8b613a6256cbf436(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0039_0c24a6b9742473f8b613a6256cbf436", xmlReference));
        }
    }

    @XmlElement(name = "_x0039_12c75c69a2c4e369aa8478d7f34da1b")
    public void set_x0039_12c75c69a2c4e369aa8478d7f34da1b(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0039_12c75c69a2c4e369aa8478d7f34da1b", value));
        }
    }

    @XmlElement(name = "_x0039_26ed244084a4bb68847adc1ab63f976")
    public void set_x0039_26ed244084a4bb68847adc1ab63f976(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0039_26ed244084a4bb68847adc1ab63f976", value));
        }
    }

    @XmlElement(name = "_x0039_34e82f4509c4d81a5b4f908f1db8d7b")
    public void set_x0039_34e82f4509c4d81a5b4f908f1db8d7b(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0039_34e82f4509c4d81a5b4f908f1db8d7b", xmlReference));
        }
    }

    @XmlElement(name = "_x0039_3dc3c0954864ed4bebcd6cb9406dd06")
    public void set_x0039_3dc3c0954864ed4bebcd6cb9406dd06(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0039_3dc3c0954864ed4bebcd6cb9406dd06", xmlReference));
        }
    }

    @XmlElement(name = "_x0039_596ff26c6094eb89b4fcd01773dd668")
    public void set_x0039_596ff26c6094eb89b4fcd01773dd668(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0039_596ff26c6094eb89b4fcd01773dd668", xmlReference));
        }
    }

    @XmlElement(name = "_x0039_6d9d1da8ffc4a8e9d08605b2c719512")
    public void set_x0039_6d9d1da8ffc4a8e9d08605b2c719512(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0039_6d9d1da8ffc4a8e9d08605b2c719512", value));
        }
    }

    @XmlElement(name = "_x0039_7926641f16c4125b2ff8f4167431852")
    public void set_x0039_7926641f16c4125b2ff8f4167431852(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0039_7926641f16c4125b2ff8f4167431852", xmlReference));
        }
    }

    @XmlElement(name = "_x0039_8ac3a22d99346f3a216609f1fa98c1d")
    public void set_x0039_8ac3a22d99346f3a216609f1fa98c1d(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0039_8ac3a22d99346f3a216609f1fa98c1d", xmlReference));
        }
    }

    @XmlElement(name = "_x0039_8c2318760844b5cadc30ba255f95609")
    public void set_x0039_8c2318760844b5cadc30ba255f95609(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0039_8c2318760844b5cadc30ba255f95609", xmlReference));
        }
    }

    @XmlElement(name = "_x0039_a7e9163ae69409f9b849d4c640aa2cc")
    public void set_x0039_a7e9163ae69409f9b849d4c640aa2cc(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0039_a7e9163ae69409f9b849d4c640aa2cc", value));
        }
    }

    @XmlElement(name = "_x0039_ab9927a1e22407eb14e1e3def2ed5c0")
    public void set_x0039_ab9927a1e22407eb14e1e3def2ed5c0(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0039_ab9927a1e22407eb14e1e3def2ed5c0", xmlReference));
        }
    }

    @XmlElement(name = "_x0039_b30d1fcb56940da872c428dc9e45de1")
    public void set_x0039_b30d1fcb56940da872c428dc9e45de1(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0039_b30d1fcb56940da872c428dc9e45de1", value));
        }
    }

    @XmlElement(name = "_x0039_cc68bb806ba4c00bfab3a7390fbff6f")
    public void set_x0039_cc68bb806ba4c00bfab3a7390fbff6f(Boolean value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0039_cc68bb806ba4c00bfab3a7390fbff6f", value));
        }

    }

    @XmlElement(name = "_x0039_dafb15a1ca946febf95596b818896d5")
    public void set_x0039_dafb15a1ca946febf95596b818896d5(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("_x0039_dafb15a1ca946febf95596b818896d5", value));
        }
    }

    @XmlElement(name = "a17b06b2e4a44e62adf7d31bd5178f58")
    public void seta17b06b2e4a44e62adf7d31bd5178f58(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("a17b06b2e4a44e62adf7d31bd5178f58", value));
        }
    }

    @XmlElement(name = "a183d1cd3a5a4f5b937575e32619943e")
    public void seta183d1cd3a5a4f5b937575e32619943e(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("a183d1cd3a5a4f5b937575e32619943e", xmlReference));
        }
    }

    @XmlElement(name = "a1dd4a2fcb9e4226a79bc567c72cdc1a")
    public void seta1dd4a2fcb9e4226a79bc567c72cdc1a(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("a1dd4a2fcb9e4226a79bc567c72cdc1a", value));
        }
    }

    @XmlElement(name = "a2b70cd3470048098aa36596e47d4837")
    public void seta2b70cd3470048098aa36596e47d4837(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("a2b70cd3470048098aa36596e47d4837", value));
        }
    }

    @XmlElement(name = "a36941a1a6f04987a35507611a23bf2c")
    public void seta36941a1a6f04987a35507611a23bf2c(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("a36941a1a6f04987a35507611a23bf2c", xmlReference));
        }
    }

    @XmlElement(name = "a43308d18a0243d49554eeae7380ca61")
    public void seta43308d18a0243d49554eeae7380ca61(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("a43308d18a0243d49554eeae7380ca61", xmlReference));
        }
    }

    @XmlElement(name = "a775ff2c3d6e414ebad7d38d597d5f43")
    public void seta775ff2c3d6e414ebad7d38d597d5f43(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("a775ff2c3d6e414ebad7d38d597d5f43", value));
        }
    }

    @XmlElement(name = "a8d658b7495c44e4b522a3b59700b56e")
    public void seta8d658b7495c44e4b522a3b59700b56e(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("a8d658b7495c44e4b522a3b59700b56e", value));
        }
    }

    @XmlElement(name = "a902a7d997a6477da972e149a84c6971")
    public void seta902a7d997a6477da972e149a84c6971(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("a902a7d997a6477da972e149a84c6971", value));
        }
    }

    @XmlElement(name = "a9781cc29deb47648486d5e382d5a781")
    public void seta9781cc29deb47648486d5e382d5a781(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("a9781cc29deb47648486d5e382d5a781", xmlReference));
        }
    }

    @XmlElement(name = "aa4ea9d0272645b8bc6033d2649678aa")
    public void setaa4ea9d0272645b8bc6033d2649678aa(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("aa4ea9d0272645b8bc6033d2649678aa", xmlReference));
        }
    }

    @XmlElement(name = "aa9673ea3bfa43b7bb17030996f79f2a")
    public void setaa9673ea3bfa43b7bb17030996f79f2a(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("aa9673ea3bfa43b7bb17030996f79f2a", value));
        }
    }

    @XmlElement(name = "ab26eff21a8d4b8c941e97aad8755a82")
    public void setab26eff21a8d4b8c941e97aad8755a82(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("ab26eff21a8d4b8c941e97aad8755a82", value));
        }
    }

    @XmlElement(name = "ac317bc16efd4b8785814a2406b57092")
    public void setac317bc16efd4b8785814a2406b57092(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("ac317bc16efd4b8785814a2406b57092", value));
        }
    }

    @XmlElement(name = "ad327233f2414f8ab88e8fd81c6bceef")
    public void setad327233f2414f8ab88e8fd81c6bceef(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("ad327233f2414f8ab88e8fd81c6bceef", value));
        }
    }

    @XmlElement(name = "afb9d0f0db584de6a3e5db45160f207f")
    public void setafb9d0f0db584de6a3e5db45160f207f(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("afb9d0f0db584de6a3e5db45160f207f", xmlReference));
        }
    }

    @XmlElement(name = "b0415ef44b9e4dc498cc3b7d97c6de3c")
    public void setb0415ef44b9e4dc498cc3b7d97c6de3c(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("b0415ef44b9e4dc498cc3b7d97c6de3c", value));
        }
    }

    @XmlElement(name = "b18ddfd38d3045f8a8aae46179511575")
    public void setb18ddfd38d3045f8a8aae46179511575(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("b18ddfd38d3045f8a8aae46179511575", value));
        }
    }

    @XmlElement(name = "b2ddf05b8ed34b7d9abc1d3f0e45e834")
    public void setb2ddf05b8ed34b7d9abc1d3f0e45e834(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("b2ddf05b8ed34b7d9abc1d3f0e45e834", xmlReference));
        }
    }

    @XmlElement(name = "b3ae54d1fa144405ad58b010b54f165f")
    public void setb3ae54d1fa144405ad58b010b54f165f(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("b3ae54d1fa144405ad58b010b54f165f", value));
        }
    }

    @XmlElement(name = "b4454dfa6f22479eb440e8ce74b2810e")
    public void setb4454dfa6f22479eb440e8ce74b2810e(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("b4454dfa6f22479eb440e8ce74b2810e", value));
        }
    }

    @XmlElement(name = "b8985c4e304f4249a278379052727f71")
    public void setb8985c4e304f4249a278379052727f71(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("b8985c4e304f4249a278379052727f71", value));
        }
    }

    @XmlElement(name = "b8ac4b039b98412eb931a4be1807cb9b")
    public void setb8ac4b039b98412eb931a4be1807cb9b(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("b8ac4b039b98412eb931a4be1807cb9b", xmlReference));
        }
    }

    @XmlElement(name = "b8b7d4bc2f2e45a78693758329394c1c")
    public void setb8b7d4bc2f2e45a78693758329394c1c(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("b8b7d4bc2f2e45a78693758329394c1c", value));
        }
    }

    @XmlElement(name = "b8d89b36ed3a4587805d68ec6bb12990")
    public void setb8d89b36ed3a4587805d68ec6bb12990(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("b8d89b36ed3a4587805d68ec6bb12990", value));
        }
    }

    @XmlElement(name = "b96fd4561f6740ff9a0fec170a505e42")
    public void setb96fd4561f6740ff9a0fec170a505e42(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("b96fd4561f6740ff9a0fec170a505e42", value));
        }
    }

    @XmlElement(name = "ba2cf37eb5724fd58f3a57b64f90bea4")
    public void setba2cf37eb5724fd58f3a57b64f90bea4(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("ba2cf37eb5724fd58f3a57b64f90bea4", value));
        }
    }

    @XmlElement(name = "bb7f523d4ae3449783379ff3f06681fc")
    public void setbb7f523d4ae3449783379ff3f06681fc(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("bb7f523d4ae3449783379ff3f06681fc", value));
        }
    }

    @XmlElement(name = "bbafd4a46c074ea697dcb42755193907")
    public void setbbafd4a46c074ea697dcb42755193907(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("bbafd4a46c074ea697dcb42755193907", xmlReference));
        }
    }

    @XmlElement(name = "bbdfe006bfc14ce9a4c1d1116b67f132")
    public void setbbdfe006bfc14ce9a4c1d1116b67f132(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("bbdfe006bfc14ce9a4c1d1116b67f132", xmlReference));
        }
    }

    @XmlElement(name = "bd448cee25c247fa81ee192a29d890e3")
    public void setbd448cee25c247fa81ee192a29d890e3(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("bd448cee25c247fa81ee192a29d890e3", value));
        }
    }

    @XmlElement(name = "be3e4d924b244335bc413771fa9008b0")
    public void setbe3e4d924b244335bc413771fa9008b0(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("be3e4d924b244335bc413771fa9008b0", xmlReference));
        }
    }

    @XmlElement(name = "becbe5ec8cb342b6b90d67614a514964")
    public void setbecbe5ec8cb342b6b90d67614a514964(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("becbe5ec8cb342b6b90d67614a514964", xmlReference));
        }
    }

    @XmlElement(name = "bf7bf2d8f673453aa5a2bef7e9c401f9")
    public void setbf7bf2d8f673453aa5a2bef7e9c401f9(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("bf7bf2d8f673453aa5a2bef7e9c401f9", value));
        }
    }

    @XmlElement(name = "c0f4b2060e4244c7ba7402e65fa441ea")
    public void setc0f4b2060e4244c7ba7402e65fa441ea(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("c0f4b2060e4244c7ba7402e65fa441ea", value));
        }
    }

    @XmlElement(name = "c100941f8ddd469ea275795deecaa416")
    public void setc100941f8ddd469ea275795deecaa416(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("c100941f8ddd469ea275795deecaa416", value));
        }
    }

    @XmlElement(name = "c100c4f3687f46679e9d40e14dc2c956")
    public void setc100c4f3687f46679e9d40e14dc2c956(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("c100c4f3687f46679e9d40e14dc2c956", value));
        }
    }

    @XmlElement(name = "c1139d120f014389a57c619152646123")
    public void setc1139d120f014389a57c619152646123(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("c1139d120f014389a57c619152646123", value));
        }
    }

    @XmlElement(name = "c2235311159a4d0b807e751a415f7fd3")
    public void setc2235311159a4d0b807e751a415f7fd3(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("c2235311159a4d0b807e751a415f7fd3", xmlReference));
        }
    }

    @XmlElement(name = "c29865a7dd4045d4b2a2bfb2b515518b")
    public void setc29865a7dd4045d4b2a2bfb2b515518b(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("c29865a7dd4045d4b2a2bfb2b515518b", xmlReference));
        }
    }

    @XmlElement(name = "c447767f64fc4b7cad73e14fee18105b")
    public void setc447767f64fc4b7cad73e14fee18105b(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("c447767f64fc4b7cad73e14fee18105b", value));
        }
    }

    @XmlElement(name = "c4a74966ef9345d8993db4df64b0a4d7")
    public void setc4a74966ef9345d8993db4df64b0a4d7(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("c4a74966ef9345d8993db4df64b0a4d7", xmlReference));
        }
    }

    @XmlElement(name = "c5cc010454734e5794055f5267a61246")
    public void setc5cc010454734e5794055f5267a61246(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("c5cc010454734e5794055f5267a61246", value));
        }
    }

    @XmlElement(name = "c5f9b44fcb9740e6ab2f1c0c39798dc0")
    public void setc5f9b44fcb9740e6ab2f1c0c39798dc0(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("c5f9b44fcb9740e6ab2f1c0c39798dc0", xmlReference));
        }
    }

    @XmlElement(name = "c73fb80e7584416cb8d8a2fb43a747c5")
    public void setc73fb80e7584416cb8d8a2fb43a747c5(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("c73fb80e7584416cb8d8a2fb43a747c5", value));
        }
    }

    @XmlElement(name = "c8b5e688d10d4e249aa3b29a50fcb36b")
    public void setc8b5e688d10d4e249aa3b29a50fcb36b(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("c8b5e688d10d4e249aa3b29a50fcb36b", xmlReference));
        }
    }

    @XmlElement(name = "c979cc011a1940c5bff2e53005149766")
    public void setc979cc011a1940c5bff2e53005149766(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("c979cc011a1940c5bff2e53005149766", xmlReference));
        }
    }

    @XmlElement(name = "c9cbd736e1754eeea4c383ce148061a0")
    public void setc9cbd736e1754eeea4c383ce148061a0(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("c9cbd736e1754eeea4c383ce148061a0", value));
        }
    }

    @XmlElement(name = "ca7f19af63c04193844df66077f38086")
    public void setca7f19af63c04193844df66077f38086(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("ca7f19af63c04193844df66077f38086", xmlReference));
        }
    }

    @XmlElement(name = "cb0afe64d5a34e2a8323a5f3eb91b9ed")
    public void setcb0afe64d5a34e2a8323a5f3eb91b9ed(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("cb0afe64d5a34e2a8323a5f3eb91b9ed", value));
        }
    }

    @XmlElement(name = "d0354df1b777482e8232b2a175f45469")
    public void setd0354df1b777482e8232b2a175f45469(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("d0354df1b777482e8232b2a175f45469", value));
        }
    }

    @XmlElement(name = "d0a1acf0079d41c5901463a6f16fb248")
    public void setd0a1acf0079d41c5901463a6f16fb248(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("d0a1acf0079d41c5901463a6f16fb248", value));
        }
    }

    @XmlElement(name = "d13e5ea31e68454cbaecdc998edd6ce3")
    public void setd13e5ea31e68454cbaecdc998edd6ce3(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("d13e5ea31e68454cbaecdc998edd6ce3", xmlReference));
        }
    }

    @XmlElement(name = "d1ae8608ba3744b1a4afd0861d58a7dc")
    public void setd1ae8608ba3744b1a4afd0861d58a7dc(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("d1ae8608ba3744b1a4afd0861d58a7dc", xmlReference));
        }
    }

    @XmlElement(name = "d33fedf04c164db4b7e83fd71c10929c")
    public void setd33fedf04c164db4b7e83fd71c10929c(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("d33fedf04c164db4b7e83fd71c10929c", xmlReference));
        }
    }

    @XmlElement(name = "d75d8d7c644443ebb49ff7b36a84f118")
    public void setd75d8d7c644443ebb49ff7b36a84f118(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("d75d8d7c644443ebb49ff7b36a84f118", xmlReference));
        }
    }

    @XmlElement(name = "d93d37d8b3e442d0b2cdad933741cb30")
    public void setd93d37d8b3e442d0b2cdad933741cb30(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("d93d37d8b3e442d0b2cdad933741cb30", xmlReference));
        }
    }

    @XmlElement(name = "d9db96a1188d467e8d010270c6ede225")
    public void setd9db96a1188d467e8d010270c6ede225(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("d9db96a1188d467e8d010270c6ede225", xmlReference));
        }
    }

    @XmlElement(name = "dcd75e9d3b414f9ca9662ca684455ba4")
    public void setdcd75e9d3b414f9ca9662ca684455ba4(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("dcd75e9d3b414f9ca9662ca684455ba4", value));
        }
    }

    @XmlElement(name = "dd2adf1874884138bac09e7de39b0d14")
    public void setdd2adf1874884138bac09e7de39b0d14(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("dd2adf1874884138bac09e7de39b0d14", xmlReference));
        }
    }

    @XmlElement(name = "e105417dcfe843c09c666c401f8a9c30")
    public void sete105417dcfe843c09c666c401f8a9c30(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("e105417dcfe843c09c666c401f8a9c30", xmlReference));
        }
    }

    @XmlElement(name = "e11fd4f7cc95468c93c04a2edba10f67")
    public void sete11fd4f7cc95468c93c04a2edba10f67(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("e11fd4f7cc95468c93c04a2edba10f67", value));
        }
    }

    @XmlElement(name = "e2e0320beb244129b9602187d1406362")
    public void sete2e0320beb244129b9602187d1406362(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("e2e0320beb244129b9602187d1406362", xmlReference));
        }
    }

    @XmlElement(name = "e427aaacdbc14c86890d4a7661c0e713")
    public void sete427aaacdbc14c86890d4a7661c0e713(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("e427aaacdbc14c86890d4a7661c0e713", value));
        }
    }

    @XmlElement(name = "e42bbb9e5aa3405f8a08a37893bd90ed")
    public void sete42bbb9e5aa3405f8a08a37893bd90ed(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("e42bbb9e5aa3405f8a08a37893bd90ed", value));
        }
    }

    @XmlElement(name = "e4812061e92649daacc8843e547845ed")
    public void sete4812061e92649daacc8843e547845ed(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("e4812061e92649daacc8843e547845ed", value));
        }
    }

    @XmlElement(name = "e67501dc19204f85af65b42e81b1dfd4")
    public void sete67501dc19204f85af65b42e81b1dfd4(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("e67501dc19204f85af65b42e81b1dfd4", xmlReference));
        }
    }

    @XmlElement(name = "e710ebf5406140eb9c50770ae7e1fae6")
    public void sete710ebf5406140eb9c50770ae7e1fae6(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("e710ebf5406140eb9c50770ae7e1fae6", xmlReference));
        }
    }

    @XmlElement(name = "e7bf85dad48f4770952a9deca8dd53e8")
    public void sete7bf85dad48f4770952a9deca8dd53e8(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("e7bf85dad48f4770952a9deca8dd53e8", value));
        }
    }

    @XmlElement(name = "e83afdfeca1045fe907ce89c0f84f488")
    public void sete83afdfeca1045fe907ce89c0f84f488(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("e83afdfeca1045fe907ce89c0f84f488", value));
        }
    }

    @XmlElement(name = "eb6f004e996f408c912f4ca33c0df76b")
    public void seteb6f004e996f408c912f4ca33c0df76b(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("eb6f004e996f408c912f4ca33c0df76b", value));
        }
    }

    @XmlElement(name = "eca945f1763142958297882bbace9dbc")
    public void seteca945f1763142958297882bbace9dbc(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("eca945f1763142958297882bbace9dbc", xmlReference));
        }
    }

    @XmlElement(name = "f0ba6869676c46ff8d994a82f7455880")
    public void setf0ba6869676c46ff8d994a82f7455880(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("f0ba6869676c46ff8d994a82f7455880", xmlReference));
        }
    }

    @XmlElement(name = "f113634920d14df1953a33d5a0aea213")
    public void setf113634920d14df1953a33d5a0aea213(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("f113634920d14df1953a33d5a0aea213", value));
        }
    }

    @XmlElement(name = "f1329a474bf3407ba2a65ede24c53a4f")
    public void setf1329a474bf3407ba2a65ede24c53a4f(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("f1329a474bf3407ba2a65ede24c53a4f", xmlReference));
        }
    }

    @XmlElement(name = "f197ed7060284bb79b6c9740b570bc9f")
    public void setf197ed7060284bb79b6c9740b570bc9f(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("f197ed7060284bb79b6c9740b570bc9f", value));
        }
    }

    @XmlElement(name = "f1be7f7c1c704ad9bdb0e56b8ab47666")
    public void setf1be7f7c1c704ad9bdb0e56b8ab47666(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("f1be7f7c1c704ad9bdb0e56b8ab47666", xmlReference));
        }
    }

    @XmlElement(name = "f49ccf17f54d4cefaf56d0646fd2a487")
    public void setf49ccf17f54d4cefaf56d0646fd2a487(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("f49ccf17f54d4cefaf56d0646fd2a487", value));
        }
    }

    @XmlElement(name = "f5365511d8fe4c66897fed965828451d")
    public void setf5365511d8fe4c66897fed965828451d(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("f5365511d8fe4c66897fed965828451d", value));
        }
    }

    @XmlElement(name = "f5d6b6e9dc1b4bdea4c34115258de7e4")
    public void setf5d6b6e9dc1b4bdea4c34115258de7e4(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("f5d6b6e9dc1b4bdea4c34115258de7e4", xmlReference));
        }
    }

    @XmlElement(name = "f5e2c754e75345e7a2be3c86e38c647b")
    public void setf5e2c754e75345e7a2be3c86e38c647b(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("f5e2c754e75345e7a2be3c86e38c647b", value));
        }
    }

    @XmlElement(name = "f7a3e0f1c8db4203b5b6d4a34b01c72f")
    public void setf7a3e0f1c8db4203b5b6d4a34b01c72f(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("f7a3e0f1c8db4203b5b6d4a34b01c72f", xmlReference));
        }
    }

    @XmlElement(name = "fa9f379608384136b5a9582958d79706")
    public void setfa9f379608384136b5a9582958d79706(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("fa9f379608384136b5a9582958d79706", value));
        }
    }

    @XmlElement(name = "fb35399fe0ab45a98e8bad33931ef1e1")
    public void setfb35399fe0ab45a98e8bad33931ef1e1(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("fb35399fe0ab45a98e8bad33931ef1e1", xmlReference));
        }
    }

    @XmlElement(name = "fdcdf4d1d28a434298964da529fe3213")
    public void setfdcdf4d1d28a434298964da529fe3213(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("fdcdf4d1d28a434298964da529fe3213", value));
        }
    }

    @XmlElement(name = "ffbf522933674395bbaa6748584be057")
    public void setffbf522933674395bbaa6748584be057(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("ffbf522933674395bbaa6748584be057", xmlReference));
        }
    }

    @XmlElement(name = "ffc5777c7c094891aa8c3be8277ca20b")
    public void setffc5777c7c094891aa8c3be8277ca20b(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("ffc5777c7c094891aa8c3be8277ca20b", value));
        }
    }

    @XmlElement(name = "isoattr_22001")
    public void setisoattr_22001(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22001", value));
        }
    }

    @XmlElement(name = "isoattr_22002")
    public void setisoattr_22002(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22002", value));
        }
    }

    /**
     * Visas inte i gränssnittet idag. Ska inte visas i 2.0.
     @XmlElement(name = "isoattr_22003")
     public void setisoattr_22003(BlindReference xmlReference) { if(xmlReference != null){
     categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22003", xmlReference)); }
     }
     **/

    /**
     * Visas inte i gränssnittet idag. Ska inte visas i 2.0.
     @XmlElement(name = "isoattr_22004")
     public void setisoattr_22004(String value) { if(value != null) {
     categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22004",value)); }
     }
     **/

    @XmlElement(name = "isoattr_22005")
    public void setisoattr_22005(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22005", value));
        }
    }

    @XmlElement(name = "isoattr_22006")
    public void setisoattr_22006(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22006", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22007")
    public void setisoattr_22007(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22007", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22008")
    public void setisoattr_22008(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22008", value));
        }
    }

    @XmlElement(name = "isoattr_22009")
    public void setisoattr_22009(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22009", value));
        }
    }

    @XmlElement(name = "isoattr_22010")
    public void setisoattr_22010(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22010", value));
        }
    }

    @XmlElement(name = "isoattr_22011")
    public void setisoattr_22011(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22011", value));
        }
    }

    @XmlElement(name = "isoattr_22012")
    public void setisoattr_22012(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22012", value));
        }
    }

    @XmlElement(name = "isoattr_22013")
    public void setisoattr_22013(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22013", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22014")
    public void setisoattr_22014(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22014", value));
        }
    }

    @XmlElement(name = "isoattr_22015")
    public void setisoattr_22015(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22015", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22016")
    public void setisoattr_22016(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22016", value));
        }
    }

    @XmlElement(name = "isoattr_22017")
    public void setisoattr_22017(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22017", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22018")
    public void setisoattr_22018(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22018", value));
        }
    }

    @XmlElement(name = "isoattr_22019")
    public void setisoattr_22019(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22019", value));
        }
    }

    @XmlElement(name = "isoattr_22020")
    public void setisoattr_22020(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22020", value));
        }
    }

    @XmlElement(name = "isoattr_22021")
    public void setisoattr_22021(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22021", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22022")
    public void setisoattr_22022(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22022", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22023")
    public void setisoattr_22023(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22023", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22024")
    public void setisoattr_22024(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22024", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22025")
    public void setisoattr_22025(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22025", value));
        }
    }

    @XmlElement(name = "isoattr_22026")
    public void setisoattr_22026(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22026", value));
        }
    }

    @XmlElement(name = "isoattr_22027")
    public void setisoattr_22027(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22027", value));
        }
    }

    @XmlElement(name = "isoattr_22028")
    public void setisoattr_22028(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22028", value));
        }
    }

    @XmlElement(name = "isoattr_22029")
    public void setisoattr_22029(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22029", value));
        }
    }

    @XmlElement(name = "isoattr_22030")
    public void setisoattr_22030(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22030", value));
        }
    }

    @XmlElement(name = "isoattr_22031")
    public void setisoattr_22031(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22031", value));
        }
    }

    @XmlElement(name = "isoattr_22032")
    public void setisoattr_22032(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22032", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22033")
    public void setisoattr_22033(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22033", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22034")
    public void setisoattr_22034(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22034", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22035")
    public void setisoattr_22035(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22035", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22036")
    public void setisoattr_22036(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22036", value));
        }
    }

    @XmlElement(name = "isoattr_22037")
    public void setisoattr_22037(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22037", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22038")
    public void setisoattr_22038(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22038", value));
        }
    }

    @XmlElement(name = "isoattr_22039")
    public void setisoattr_22039(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22039", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22040")
    public void setisoattr_22040(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22040", value));
        }
    }

    @XmlElement(name = "isoattr_22041")
    public void setisoattr_22041(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22041", value));
        }
    }

    @XmlElement(name = "isoattr_22042")
    public void setisoattr_22042(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22042", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22043")
    public void setisoattr_22043(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22043", value));
        }
    }

    @XmlElement(name = "isoattr_22044")
    public void setisoattr_22044(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22044", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22045")
    public void setisoattr_22045(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22045", value));
        }
    }

    @XmlElement(name = "isoattr_22046")
    public void setisoattr_22046(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22046", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22047")
    public void setisoattr_22047(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22047", value));
        }
    }

    @XmlElement(name = "isoattr_22048")
    public void setisoattr_22048(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22048", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22049")
    public void setisoattr_22049(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22049", value));
        }
    }

    @XmlElement(name = "isoattr_22050")
    public void setisoattr_22050(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22050", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22051")
    public void setisoattr_22051(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22051", value));
        }
    }

    @XmlElement(name = "isoattr_22052")
    public void setisoattr_22052(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22052", value));
        }
    }

    @XmlElement(name = "isoattr_22053")
    public void setisoattr_22053(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22053", value));
        }
    }

    @XmlElement(name = "isoattr_22054")
    public void setisoattr_22054(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22054", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22055")
    public void setisoattr_22055(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22055", value));
        }
    }

    @XmlElement(name = "isoattr_22056")
    public void setisoattr_22056(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22056", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22057")
    public void setisoattr_22057(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22057", value));
        }
    }

    @XmlElement(name = "isoattr_22058")
    public void setisoattr_22058(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22058", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22059")
    public void setisoattr_22059(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22059", value));
        }
    }

    @XmlElement(name = "isoattr_22060")
    public void setisoattr_22060(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22060", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22061")
    public void setisoattr_22061(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22061", value));
        }
    }

    @XmlElement(name = "isoattr_22062")
    public void setisoattr_22062(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22062", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22063")
    public void setisoattr_22063(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22063", value));
        }
    }

    @XmlElement(name = "isoattr_22064")
    public void setisoattr_22064(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22064", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22065")
    public void setisoattr_22065(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22065", value));
        }
    }

    @XmlElement(name = "isoattr_22066")
    public void setisoattr_22066(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22066", value));
        }
    }

    @XmlElement(name = "isoattr_22067")
    public void setisoattr_22067(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22067", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22068")
    public void setisoattr_22068(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22068", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22069")
    public void setisoattr_22069(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22069", value));
        }
    }

    @XmlElement(name = "isoattr_22070")
    public void setisoattr_22070(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22070", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22071")
    public void setisoattr_22071(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22071", value));
        }
    }

    @XmlElement(name = "isoattr_22072")
    public void setisoattr_22072(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22072", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22073")
    public void setisoattr_22073(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22073", value));
        }
    }

    @XmlElement(name = "isoattr_22074")
    public void setisoattr_22074(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22074", value));
        }
    }

    @XmlElement(name = "isoattr_22075")
    public void setisoattr_22075(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22075", value));
        }
    }

    @XmlElement(name = "isoattr_22076")
    public void setisoattr_22076(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22076", value));
        }
    }

    @XmlElement(name = "isoattr_22077")
    public void setisoattr_22077(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22077", value));
        }
    }

    @XmlElement(name = "isoattr_22078")
    public void setisoattr_22078(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22078", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22079")
    public void setisoattr_22079(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22079", value));
        }
    }

    @XmlElement(name = "isoattr_22080")
    public void setisoattr_22080(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22080", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22081")
    public void setisoattr_22081(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22081", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22082")
    public void setisoattr_22082(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22082", value));
        }
    }

    @XmlElement(name = "isoattr_22083")
    public void setisoattr_22083(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22083", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22084")
    public void setisoattr_22084(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22084", value));
        }
    }

    @XmlElement(name = "isoattr_22085")
    public void setisoattr_22085(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22085", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22086")
    public void setisoattr_22086(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22086", value));
        }
    }

    @XmlElement(name = "isoattr_22087")
    public void setisoattr_22087(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22087", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22088")
    public void setisoattr_22088(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22088", value));
        }
    }

    @XmlElement(name = "isoattr_22089")
    public void setisoattr_22089(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22089", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22090")
    public void setisoattr_22090(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22090", value));
        }
    }

    @XmlElement(name = "isoattr_22091")
    public void setisoattr_22091(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22091", value));
        }
    }

    @XmlElement(name = "isoattr_22092")
    public void setisoattr_22092(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22092", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22093")
    public void setisoattr_22093(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22093", value));
        }
    }

    @XmlElement(name = "isoattr_22094")
    public void setisoattr_22094(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22094", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22095")
    public void setisoattr_22095(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22095", value));
        }
    }

    @XmlElement(name = "isoattr_22096")
    public void setisoattr_22096(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22096", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22097")
    public void setisoattr_22097(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22097", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22098")
    public void setisoattr_22098(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22098", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22099")
    public void setisoattr_22099(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22099", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22100")
    public void setisoattr_22100(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22100", value));
        }
    }

    @XmlElement(name = "isoattr_22101")
    public void setisoattr_22101(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22101", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22102")
    public void setisoattr_22102(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22102", value));
        }
    }

    @XmlElement(name = "isoattr_22103")
    public void setisoattr_22103(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22103", value));
        }
    }

    @XmlElement(name = "isoattr_22104")
    public void setisoattr_22104(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22104", value));
        }
    }

    @XmlElement(name = "isoattr_22105")
    public void setisoattr_22105(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22105", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22106")
    public void setisoattr_22106(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22106", value));
        }
    }

    @XmlElement(name = "isoattr_22107")
    public void setisoattr_22107(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22107", value));
        }
    }

    @XmlElement(name = "isoattr_22108")
    public void setisoattr_22108(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22108", value));
        }
    }

    @XmlElement(name = "isoattr_22109")
    public void setisoattr_22109(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22109", value));
        }
    }

    @XmlElement(name = "isoattr_22110")
    public void setisoattr_22110(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22110", value));
        }
    }

    @XmlElement(name = "isoattr_22111")
    public void setisoattr_22111(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22111", value));
        }
    }

    @XmlElement(name = "isoattr_22112")
    public void setisoattr_22112(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22112", value));
        }
    }

    @XmlElement(name = "isoattr_22113")
    public void setisoattr_22113(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22113", value));
        }
    }

    @XmlElement(name = "isoattr_22114")
    public void setisoattr_22114(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22114", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22115")
    public void setisoattr_22115(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22115", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22116")
    public void setisoattr_22116(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22116", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22117")
    public void setisoattr_22117(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22117", value));
        }
    }

    @XmlElement(name = "isoattr_22118")
    public void setisoattr_22118(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22118", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22119")
    public void setisoattr_22119(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22119", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22120")
    public void setisoattr_22120(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22120", value));
        }
    }

    @XmlElement(name = "isoattr_22121")
    public void setisoattr_22121(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22121", value));
        }
    }

    @XmlElement(name = "isoattr_22122")
    public void setisoattr_22122(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22122", value));
        }
    }

    @XmlElement(name = "isoattr_22123")
    public void setisoattr_22123(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22123", value));
        }
    }

    @XmlElement(name = "isoattr_22124")
    public void setisoattr_22124(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22124", value));
        }
    }

    @XmlElement(name = "isoattr_22125")
    public void setisoattr_22125(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22125", value));
        }
    }

    @XmlElement(name = "isoattr_22126")
    public void setisoattr_22126(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22126", value));
        }
    }

    @XmlElement(name = "isoattr_22127")
    public void setisoattr_22127(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22127", value));
        }
    }

    @XmlElement(name = "isoattr_22128")
    public void setisoattr_22128(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22128", value));
        }
    }

    @XmlElement(name = "isoattr_22129")
    public void setisoattr_22129(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22129", value));
        }
    }

    @XmlElement(name = "isoattr_22130")
    public void setisoattr_22130(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22130", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22131")
    public void setisoattr_22131(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22131", value));
        }
    }

    @XmlElement(name = "isoattr_22132")
    public void setisoattr_22132(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22132", value));
        }
    }

    @XmlElement(name = "isoattr_22133")
    public void setisoattr_22133(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22133", value));
        }
    }

    @XmlElement(name = "isoattr_22134")
    public void setisoattr_22134(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22134", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22135")
    public void setisoattr_22135(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22135", value));
        }
    }

    @XmlElement(name = "isoattr_22136")
    public void setisoattr_22136(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22136", value));
        }
    }

    @XmlElement(name = "isoattr_22137")
    public void setisoattr_22137(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22137", value));
        }
    }

    @XmlElement(name = "isoattr_22138")
    public void setisoattr_22138(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22138", value));
        }
    }

    @XmlElement(name = "isoattr_22139")
    public void setisoattr_22139(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22139", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22140")
    public void setisoattr_22140(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22140", value));
        }
    }

    @XmlElement(name = "isoattr_22141")
    public void setisoattr_22141(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22141", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22142")
    public void setisoattr_22142(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22142", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22143")
    public void setisoattr_22143(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22143", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22144")
    public void setisoattr_22144(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22144", value));
        }
    }

    @XmlElement(name = "isoattr_22145")
    public void setisoattr_22145(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22145", value));
        }
    }

    @XmlElement(name = "isoattr_22146")
    public void setisoattr_22146(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22146", value));
        }
    }

    @XmlElement(name = "isoattr_22147")
    public void setisoattr_22147(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22147", value));
        }
    }

    @XmlElement(name = "isoattr_22148")
    public void setisoattr_22148(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22148", value));
        }
    }

    @XmlElement(name = "isoattr_22149")
    public void setisoattr_22149(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22149", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22150")
    public void setisoattr_22150(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22150", value));
        }
    }

    @XmlElement(name = "isoattr_22151")
    public void setisoattr_22151(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22151", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22152")
    public void setisoattr_22152(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22152", value));
        }
    }

    @XmlElement(name = "isoattr_22153")
    public void setisoattr_22153(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22153", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22154")
    public void setisoattr_22154(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22154", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22155")
    public void setisoattr_22155(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22155", value));
        }
    }

    @XmlElement(name = "isoattr_22156")
    public void setisoattr_22156(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22156", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22157")
    public void setisoattr_22157(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22157", value));
        }
    }

    @XmlElement(name = "isoattr_22158")
    public void setisoattr_22158(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22158", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22159")
    public void setisoattr_22159(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22159", value));
        }
    }

    @XmlElement(name = "isoattr_22160")
    public void setisoattr_22160(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22160", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22161")
    public void setisoattr_22161(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22161", value));
        }
    }

    @XmlElement(name = "isoattr_22162")
    public void setisoattr_22162(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22162", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22163")
    public void setisoattr_22163(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22163", value));
        }
    }

    @XmlElement(name = "isoattr_22164")
    public void setisoattr_22164(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22164", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22165")
    public void setisoattr_22165(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22165", value));
        }
    }

    @XmlElement(name = "isoattr_22166")
    public void setisoattr_22166(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22166", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22167")
    public void setisoattr_22167(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22167", value));
        }
    }

    @XmlElement(name = "isoattr_22168")
    public void setisoattr_22168(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22168", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22169")
    public void setisoattr_22169(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22169", value));
        }
    }

    @XmlElement(name = "isoattr_22170")
    public void setisoattr_22170(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22170", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22171")
    public void setisoattr_22171(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22171", value));
        }
    }

    @XmlElement(name = "isoattr_22172")
    public void setisoattr_22172(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22172", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22173")
    public void setisoattr_22173(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22173", value));
        }
    }

    @XmlElement(name = "isoattr_22174")
    public void setisoattr_22174(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22174", value));
        }
    }

    @XmlElement(name = "isoattr_22175")
    public void setisoattr_22175(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22175", value));
        }
    }

    @XmlElement(name = "isoattr_22176")
    public void setisoattr_22176(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22176", value));
        }
    }

    @XmlElement(name = "isoattr_22177")
    public void setisoattr_22177(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22177", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22178")
    public void setisoattr_22178(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22178", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22179")
    public void setisoattr_22179(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22179", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22180")
    public void setisoattr_22180(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22180", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22181")
    public void setisoattr_22181(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22181", value));
        }
    }

    @XmlElement(name = "isoattr_22182")
    public void setisoattr_22182(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22182", value));
        }
    }

    @XmlElement(name = "isoattr_22183")
    public void setisoattr_22183(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22183", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22184")
    public void setisoattr_22184(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22184", value));
        }
    }

    @XmlElement(name = "isoattr_22185")
    public void setisoattr_22185(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22185", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22186")
    public void setisoattr_22186(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22186", value));
        }
    }

    @XmlElement(name = "isoattr_22187")
    public void setisoattr_22187(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22187", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22188")
    public void setisoattr_22188(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22188", value));
        }
    }

    @XmlElement(name = "isoattr_22189")
    public void setisoattr_22189(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22189", value));
        }
    }

    @XmlElement(name = "isoattr_22190")
    public void setisoattr_22190(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22190", value));
        }
    }

    @XmlElement(name = "isoattr_22191")
    public void setisoattr_22191(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22191", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22192")
    public void setisoattr_22192(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22192", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22193")
    public void setisoattr_22193(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22193", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22194")
    public void setisoattr_22194(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22194", value));
        }
    }

    @XmlElement(name = "isoattr_22195")
    public void setisoattr_22195(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22195", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22196")
    public void setisoattr_22196(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22196", value));
        }
    }

    @XmlElement(name = "isoattr_22197")
    public void setisoattr_22197(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22197", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22198")
    public void setisoattr_22198(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22198", value));
        }
    }

    @XmlElement(name = "isoattr_22199")
    public void setisoattr_22199(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22199", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22200")
    public void setisoattr_22200(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22200", value));
        }
    }

    @XmlElement(name = "isoattr_22201")
    public void setisoattr_22201(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22201", value));
        }
    }

    @XmlElement(name = "isoattr_22202")
    public void setisoattr_22202(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22202", value));
        }
    }

    @XmlElement(name = "isoattr_22203")
    public void setisoattr_22203(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22203", value));
        }
    }

    @XmlElement(name = "isoattr_22204")
    public void setisoattr_22204(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22204", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22205")
    public void setisoattr_22205(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22205", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22206")
    public void setisoattr_22206(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22206", value));
        }
    }

    @XmlElement(name = "isoattr_22207")
    public void setisoattr_22207(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22207", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22208")
    public void setisoattr_22208(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22208", value));
        }
    }

    @XmlElement(name = "isoattr_22209")
    public void setisoattr_22209(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22209", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22210")
    public void setisoattr_22210(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22210", value));
        }
    }

    @XmlElement(name = "isoattr_22211")
    public void setisoattr_22211(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22211", value));
        }
    }

    @XmlElement(name = "isoattr_22212")
    public void setisoattr_22212(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22212", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22213")
    public void setisoattr_22213(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22213", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22214")
    public void setisoattr_22214(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22214", value));
        }
    }

    @XmlElement(name = "isoattr_22215")
    public void setisoattr_22215(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22215", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22216")
    public void setisoattr_22216(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22216", value));
        }
    }

    @XmlElement(name = "isoattr_22217")
    public void setisoattr_22217(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22217", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22218")
    public void setisoattr_22218(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22218", value));
        }
    }

    @XmlElement(name = "isoattr_22219")
    public void setisoattr_22219(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22219", value));
        }
    }

    @XmlElement(name = "isoattr_22220")
    public void setisoattr_22220(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22220", value));
        }
    }

    @XmlElement(name = "isoattr_22221")
    public void setisoattr_22221(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22221", value));
        }
    }

    @XmlElement(name = "isoattr_22222")
    public void setisoattr_22222(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22222", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22223")
    public void setisoattr_22223(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22223", value));
        }
    }

    @XmlElement(name = "isoattr_22224")
    public void setisoattr_22224(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22224", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22225")
    public void setisoattr_22225(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22225", value));
        }
    }

    @XmlElement(name = "isoattr_22226")
    public void setisoattr_22226(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22226", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22227")
    public void setisoattr_22227(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22227", value));
        }
    }

    @XmlElement(name = "isoattr_22228")
    public void setisoattr_22228(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22228", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22229")
    public void setisoattr_22229(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22229", value));
        }
    }

    @XmlElement(name = "isoattr_22230")
    public void setisoattr_22230(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22230", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22231")
    public void setisoattr_22231(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22231", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22232")
    public void setisoattr_22232(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22232", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22233")
    public void setisoattr_22233(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22233", value));
        }
    }

    @XmlElement(name = "isoattr_22234")
    public void setisoattr_22234(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22234", value));
        }
    }

    @XmlElement(name = "isoattr_22235")
    public void setisoattr_22235(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22235", value));
        }
    }

    @XmlElement(name = "isoattr_22236")
    public void setisoattr_22236(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22236", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22237")
    public void setisoattr_22237(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22237", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22238")
    public void setisoattr_22238(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22238", value));
        }
    }

    @XmlElement(name = "isoattr_22239")
    public void setisoattr_22239(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22239", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22240")
    public void setisoattr_22240(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22240", value));
        }
    }

    @XmlElement(name = "isoattr_22241")
    public void setisoattr_22241(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22241", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22242")
    public void setisoattr_22242(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22242", value));
        }
    }

    @XmlElement(name = "isoattr_22243")
    public void setisoattr_22243(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22243", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22244")
    public void setisoattr_22244(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22244", value));
        }
    }

    @XmlElement(name = "isoattr_22245")
    public void setisoattr_22245(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22245", value));
        }
    }

    @XmlElement(name = "isoattr_22246")
    public void setisoattr_22246(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22246", value));
        }
    }

    @XmlElement(name = "isoattr_22247")
    public void setisoattr_22247(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22247", value));
        }
    }

    @XmlElement(name = "isoattr_22248")
    public void setisoattr_22248(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22248", value));
        }
    }

    @XmlElement(name = "isoattr_22249")
    public void setisoattr_22249(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22249", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22250")
    public void setisoattr_22250(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22250", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22251")
    public void setisoattr_22251(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22251", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22252")
    public void setisoattr_22252(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22252", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22253")
    public void setisoattr_22253(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22253", value));
        }
    }

    @XmlElement(name = "isoattr_22254")
    public void setisoattr_22254(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22254", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22255")
    public void setisoattr_22255(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22255", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22256")
    public void setisoattr_22256(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22256", value));
        }
    }

    @XmlElement(name = "isoattr_22257")
    public void setisoattr_22257(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22257", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22258")
    public void setisoattr_22258(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22258", value));
        }
    }

    @XmlElement(name = "isoattr_22259")
    public void setisoattr_22259(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22259", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22260")
    public void setisoattr_22260(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22260", value));
        }
    }

    @XmlElement(name = "isoattr_22261")
    public void setisoattr_22261(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22261", value));
        }
    }

    @XmlElement(name = "isoattr_22262")
    public void setisoattr_22262(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22262", value));
        }
    }

    @XmlElement(name = "isoattr_22263")
    public void setisoattr_22263(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22263", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22264")
    public void setisoattr_22264(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22264", value));
        }
    }

    @XmlElement(name = "isoattr_22265")
    public void setisoattr_22265(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22265", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22266")
    public void setisoattr_22266(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22266", value));
        }
    }

    @XmlElement(name = "isoattr_22267")
    public void setisoattr_22267(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22267", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22268")
    public void setisoattr_22268(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22268", value));
        }
    }

    @XmlElement(name = "isoattr_22269")
    public void setisoattr_22269(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22269", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22270")
    public void setisoattr_22270(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22270", value));
        }
    }

    @XmlElement(name = "isoattr_22271")
    public void setisoattr_22271(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22271", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22272")
    public void setisoattr_22272(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22272", value));
        }
    }

    @XmlElement(name = "isoattr_22273")
    public void setisoattr_22273(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22273", value));
        }
    }

    @XmlElement(name = "isoattr_22274")
    public void setisoattr_22274(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22274", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22275")
    public void setisoattr_22275(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22275", value));
        }
    }

    @XmlElement(name = "isoattr_22276")
    public void setisoattr_22276(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22276", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22277")
    public void setisoattr_22277(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22277", value));
        }
    }

    @XmlElement(name = "isoattr_22278")
    public void setisoattr_22278(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22278", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22279")
    public void setisoattr_22279(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22279", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22280")
    public void setisoattr_22280(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22280", value));
        }
    }

    @XmlElement(name = "isoattr_22281")
    public void setisoattr_22281(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22281", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22282")
    public void setisoattr_22282(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22282", value));
        }
    }

    @XmlElement(name = "isoattr_22283")
    public void setisoattr_22283(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22283", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22284")
    public void setisoattr_22284(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22284", value));
        }
    }

    @XmlElement(name = "isoattr_22285")
    public void setisoattr_22285(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22285", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22286")
    public void setisoattr_22286(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22286", value));
        }
    }

    @XmlElement(name = "isoattr_22287")
    public void setisoattr_22287(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22287", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22288")
    public void setisoattr_22288(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22288", value));
        }
    }

    @XmlElement(name = "isoattr_22289")
    public void setisoattr_22289(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22289", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22290")
    public void setisoattr_22290(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22290", value));
        }
    }

    @XmlElement(name = "isoattr_22291")
    public void setisoattr_22291(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22291", value));
        }
    }

    @XmlElement(name = "isoattr_22292")
    public void setisoattr_22292(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22292", value));
        }
    }

    @XmlElement(name = "isoattr_22293")
    public void setisoattr_22293(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22293", value));
        }
    }

    @XmlElement(name = "isoattr_22294")
    public void setisoattr_22294(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22294", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22295")
    public void setisoattr_22295(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22295", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22296")
    public void setisoattr_22296(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22296", value));
        }
    }

    @XmlElement(name = "isoattr_22297")
    public void setisoattr_22297(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22297", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22298")
    public void setisoattr_22298(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22298", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22299")
    public void setisoattr_22299(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22299", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22300")
    public void setisoattr_22300(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22300", value));
        }
    }

    @XmlElement(name = "isoattr_22301")
    public void setisoattr_22301(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22301", value));
        }
    }

    @XmlElement(name = "isoattr_22302")
    public void setisoattr_22302(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22302", value));
        }
    }

    @XmlElement(name = "isoattr_22303")
    public void setisoattr_22303(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22303", value));
        }
    }

    @XmlElement(name = "isoattr_22304")
    public void setisoattr_22304(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22304", value));
        }
    }

    @XmlElement(name = "isoattr_22305")
    public void setisoattr_22305(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22305", value));
        }
    }

    @XmlElement(name = "isoattr_22306")
    public void setisoattr_22306(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22306", value));
        }
    }

    @XmlElement(name = "isoattr_22307")
    public void setisoattr_22307(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22307", value));
        }
    }

    @XmlElement(name = "isoattr_22308")
    public void setisoattr_22308(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22308", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22309")
    public void setisoattr_22309(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22309", value));
        }
    }

    @XmlElement(name = "isoattr_22310")
    public void setisoattr_22310(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22310", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22311")
    public void setisoattr_22311(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22311", value));
        }
    }

    @XmlElement(name = "isoattr_22312")
    public void setisoattr_22312(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22312", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22313")
    public void setisoattr_22313(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22313", value));
        }
    }

    @XmlElement(name = "isoattr_22314")
    public void setisoattr_22314(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22314", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22315")
    public void setisoattr_22315(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22315", value));
        }
    }

    @XmlElement(name = "isoattr_22316")
    public void setisoattr_22316(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22316", value));
        }
    }

    @XmlElement(name = "isoattr_22317")
    public void setisoattr_22317(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22317", value));
        }
    }

    @XmlElement(name = "isoattr_22318")
    public void setisoattr_22318(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22318", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22319")
    public void setisoattr_22319(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22319", value));
        }
    }

    @XmlElement(name = "isoattr_22320")
    public void setisoattr_22320(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22320", value));
        }
    }

    @XmlElement(name = "isoattr_22321")
    public void setisoattr_22321(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22321", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22322")
    public void setisoattr_22322(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22322", value));
        }
    }

    @XmlElement(name = "isoattr_22323")
    public void setisoattr_22323(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22323", value));
        }
    }

    @XmlElement(name = "isoattr_22324")
    public void setisoattr_22324(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22324", value));
        }
    }

    @XmlElement(name = "isoattr_22325")
    public void setisoattr_22325(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22325", value));
        }
    }

    @XmlElement(name = "isoattr_22326")
    public void setisoattr_22326(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22326", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22327")
    public void setisoattr_22327(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22327", value));
        }
    }

    @XmlElement(name = "isoattr_22328")
    public void setisoattr_22328(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22328", value));
        }
    }

    @XmlElement(name = "isoattr_22329")
    public void setisoattr_22329(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22329", value));
        }
    }

    @XmlElement(name = "isoattr_22330")
    public void setisoattr_22330(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22330", value));
        }
    }

    @XmlElement(name = "isoattr_22331")
    public void setisoattr_22331(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22331", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22332")
    public void setisoattr_22332(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22332", value));
        }
    }

    @XmlElement(name = "isoattr_22333")
    public void setisoattr_22333(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22333", value));
        }
    }

    @XmlElement(name = "isoattr_22334")
    public void setisoattr_22334(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22334", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22335")
    public void setisoattr_22335(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22335", value));
        }
    }

    @XmlElement(name = "isoattr_22336")
    public void setisoattr_22336(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22336", value));
        }
    }

    @XmlElement(name = "isoattr_22337")
    public void setisoattr_22337(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22337", value));
        }
    }

    @XmlElement(name = "isoattr_22338")
    public void setisoattr_22338(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22338", value));
        }
    }

    @XmlElement(name = "isoattr_22339")
    public void setisoattr_22339(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22339", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22340")
    public void setisoattr_22340(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22340", value));
        }
    }

    @XmlElement(name = "isoattr_22341")
    public void setisoattr_22341(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22341", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22342")
    public void setisoattr_22342(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22342", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22343")
    public void setisoattr_22343(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22343", value));
        }
    }

    @XmlElement(name = "isoattr_22344")
    public void setisoattr_22344(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22344", value));
        }
    }

    @XmlElement(name = "isoattr_22345")
    public void setisoattr_22345(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22345", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22346")
    public void setisoattr_22346(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22346", value));
        }
    }

    @XmlElement(name = "isoattr_22347")
    public void setisoattr_22347(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22347", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22348")
    public void setisoattr_22348(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22348", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22349")
    public void setisoattr_22349(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22349", value));
        }
    }

    @XmlElement(name = "isoattr_22350")
    public void setisoattr_22350(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22350", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22351")
    public void setisoattr_22351(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22351", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22352")
    public void setisoattr_22352(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22352", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22353")
    public void setisoattr_22353(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22353", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22354")
    public void setisoattr_22354(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22354", value));
        }
    }

    @XmlElement(name = "isoattr_22355")
    public void setisoattr_22355(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22355", value));
        }
    }

    @XmlElement(name = "isoattr_22356")
    public void setisoattr_22356(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22356", value));
        }
    }

    @XmlElement(name = "isoattr_22357")
    public void setisoattr_22357(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22357", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22358")
    public void setisoattr_22358(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22358", value));
        }
    }

    @XmlElement(name = "isoattr_22359")
    public void setisoattr_22359(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22359", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22360")
    public void setisoattr_22360(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22360", value));
        }
    }

    @XmlElement(name = "isoattr_22361")
    public void setisoattr_22361(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22361", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22362")
    public void setisoattr_22362(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22362", value));
        }
    }

    @XmlElement(name = "isoattr_22363")
    public void setisoattr_22363(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22363", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22364")
    public void setisoattr_22364(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22364", value));
        }
    }

    @XmlElement(name = "isoattr_22365")
    public void setisoattr_22365(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22365", value));
        }
    }

    @XmlElement(name = "isoattr_22366")
    public void setisoattr_22366(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22366", value));
        }
    }

    @XmlElement(name = "isoattr_22367")
    public void setisoattr_22367(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22367", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22368")
    public void setisoattr_22368(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22368", value));
        }
    }

    @XmlElement(name = "isoattr_22369")
    public void setisoattr_22369(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22369", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22370")
    public void setisoattr_22370(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22370", value));
        }
    }

    @XmlElement(name = "isoattr_22371")
    public void setisoattr_22371(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22371", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22372")
    public void setisoattr_22372(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22372", value));
        }
    }

    @XmlElement(name = "isoattr_22373")
    public void setisoattr_22373(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22373", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22374")
    public void setisoattr_22374(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22374", value));
        }
    }

    @XmlElement(name = "isoattr_22375")
    public void setisoattr_22375(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22375", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22376")
    public void setisoattr_22376(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22376", value));
        }
    }

    @XmlElement(name = "isoattr_22377")
    public void setisoattr_22377(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22377", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22378")
    public void setisoattr_22378(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22378", value));
        }
    }

    @XmlElement(name = "isoattr_22379")
    public void setisoattr_22379(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22379", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22380")
    public void setisoattr_22380(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22380", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22381")
    public void setisoattr_22381(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22381", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22382")
    public void setisoattr_22382(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22382", value));
        }
    }

    @XmlElement(name = "isoattr_22383")
    public void setisoattr_22383(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22383", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22384")
    public void setisoattr_22384(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22384", value));
        }
    }

    @XmlElement(name = "isoattr_22385")
    public void setisoattr_22385(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22385", value));
        }
    }

    @XmlElement(name = "isoattr_22386")
    public void setisoattr_22386(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22386", value));
        }
    }

    @XmlElement(name = "isoattr_22387")
    public void setisoattr_22387(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22387", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22388")
    public void setisoattr_22388(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22388", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22389")
    public void setisoattr_22389(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22389", value));
        }
    }

    @XmlElement(name = "isoattr_22390")
    public void setisoattr_22390(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22390", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22391")
    public void setisoattr_22391(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22391", value));
        }
    }

    @XmlElement(name = "isoattr_22392")
    public void setisoattr_22392(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22392", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22393")
    public void setisoattr_22393(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22393", value));
        }
    }

    @XmlElement(name = "isoattr_22394")
    public void setisoattr_22394(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22394", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22395")
    public void setisoattr_22395(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22395", value));
        }
    }

    @XmlElement(name = "isoattr_22396")
    public void setisoattr_22396(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22396", value));
        }
    }

    @XmlElement(name = "isoattr_22397")
    public void setisoattr_22397(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22397", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22398")
    public void setisoattr_22398(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22398", value));
        }
    }

    @XmlElement(name = "isoattr_22399")
    public void setisoattr_22399(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22399", value));
        }
    }

    @XmlElement(name = "isoattr_22400")
    public void setisoattr_22400(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22400", value));
        }
    }

    @XmlElement(name = "isoattr_22401")
    public void setisoattr_22401(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22401", value));
        }
    }

    @XmlElement(name = "isoattr_22402")
    public void setisoattr_22402(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22402", value));
        }
    }

    @XmlElement(name = "isoattr_22403")
    public void setisoattr_22403(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22403", value));
        }
    }

    @XmlElement(name = "isoattr_22404")
    public void setisoattr_22404(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22404", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22405")
    public void setisoattr_22405(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22405", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22406")
    public void setisoattr_22406(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22406", value));
        }
    }

    @XmlElement(name = "isoattr_22407")
    public void setisoattr_22407(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22407", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22408")
    public void setisoattr_22408(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22408", value));
        }
    }

    @XmlElement(name = "isoattr_22409")
    public void setisoattr_22409(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22409", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22410")
    public void setisoattr_22410(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22410", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22411")
    public void setisoattr_22411(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22411", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22412")
    public void setisoattr_22412(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22412", value));
        }
    }

    @XmlElement(name = "isoattr_22413")
    public void setisoattr_22413(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22413", value));
        }
    }

    @XmlElement(name = "isoattr_22414")
    public void setisoattr_22414(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22414", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22415")
    public void setisoattr_22415(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22415", value));
        }
    }

    @XmlElement(name = "isoattr_22416")
    public void setisoattr_22416(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22416", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22417")
    public void setisoattr_22417(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22417", value));
        }
    }

    @XmlElement(name = "isoattr_22418")
    public void setisoattr_22418(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22418", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22419")
    public void setisoattr_22419(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22419", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22420")
    public void setisoattr_22420(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22420", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22421")
    public void setisoattr_22421(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22421", value));
        }
    }

    @XmlElement(name = "isoattr_22422")
    public void setisoattr_22422(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22422", value));
        }
    }

    @XmlElement(name = "isoattr_22423")
    public void setisoattr_22423(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22423", value));
        }
    }

    @XmlElement(name = "isoattr_22424")
    public void setisoattr_22424(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22424", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22425")
    public void setisoattr_22425(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22425", value));
        }
    }

    @XmlElement(name = "isoattr_22426")
    public void setisoattr_22426(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22426", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22427")
    public void setisoattr_22427(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22427", value));
        }
    }

    @XmlElement(name = "isoattr_22428")
    public void setisoattr_22428(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22428", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22429")
    public void setisoattr_22429(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22429", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22430")
    public void setisoattr_22430(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22430", value));
        }
    }

    @XmlElement(name = "isoattr_22431")
    public void setisoattr_22431(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22431", value));
        }
    }

    @XmlElement(name = "isoattr_22432")
    public void setisoattr_22432(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22432", value));
        }
    }

    @XmlElement(name = "isoattr_22433")
    public void setisoattr_22433(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22433", value));
        }
    }

    @XmlElement(name = "isoattr_22434")
    public void setisoattr_22434(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22434", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22435")
    public void setisoattr_22435(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22435", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22436")
    public void setisoattr_22436(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22436", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22437")
    public void setisoattr_22437(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22437", value));
        }
    }

    @XmlElement(name = "isoattr_22438")
    public void setisoattr_22438(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22438", value));
        }
    }

    @XmlElement(name = "isoattr_22439")
    public void setisoattr_22439(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22439", value));
        }
    }

    @XmlElement(name = "isoattr_22440")
    public void setisoattr_22440(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22440", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22441")
    public void setisoattr_22441(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22441", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22442")
    public void setisoattr_22442(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22442", value));
        }
    }

    @XmlElement(name = "isoattr_22443")
    public void setisoattr_22443(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22443", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22444")
    public void setisoattr_22444(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22444", value));
        }
    }

    @XmlElement(name = "isoattr_22445")
    public void setisoattr_22445(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22445", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22446")
    public void setisoattr_22446(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22446", value));
        }
    }

    @XmlElement(name = "isoattr_22447")
    public void setisoattr_22447(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22447", value));
        }
    }

    @XmlElement(name = "isoattr_22448")
    public void setisoattr_22448(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22448", value));
        }
    }

    @XmlElement(name = "isoattr_22449")
    public void setisoattr_22449(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22449", value));
        }
    }

    @XmlElement(name = "isoattr_22450")
    public void setisoattr_22450(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22450", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22451")
    public void setisoattr_22451(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22451", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22452")
    public void setisoattr_22452(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22452", value));
        }
    }

    @XmlElement(name = "isoattr_22453")
    public void setisoattr_22453(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22453", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22454")
    public void setisoattr_22454(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22454", value));
        }
    }

    @XmlElement(name = "isoattr_22455")
    public void setisoattr_22455(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22455", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22456")
    public void setisoattr_22456(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22456", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22457")
    public void setisoattr_22457(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22457", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22458")
    public void setisoattr_22458(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22458", value));
        }
    }

    @XmlElement(name = "isoattr_22459")
    public void setisoattr_22459(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22459", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22460")
    public void setisoattr_22460(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22460", value));
        }
    }

    @XmlElement(name = "isoattr_22461")
    public void setisoattr_22461(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22461", value));
        }
    }

    @XmlElement(name = "isoattr_22462")
    public void setisoattr_22462(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22462", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22463")
    public void setisoattr_22463(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22463", value));
        }
    }

    @XmlElement(name = "isoattr_22464")
    public void setisoattr_22464(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22464", value));
        }
    }

    @XmlElement(name = "isoattr_22465")
    public void setisoattr_22465(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22465", value));
        }
    }

    @XmlElement(name = "isoattr_22466")
    public void setisoattr_22466(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22466", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22467")
    public void setisoattr_22467(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22467", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22468")
    public void setisoattr_22468(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22468", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22469")
    public void setisoattr_22469(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22469", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22470")
    public void setisoattr_22470(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22470", value));
        }
    }

    @XmlElement(name = "isoattr_22471")
    public void setisoattr_22471(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22471", value));
        }
    }

    @XmlElement(name = "isoattr_22472")
    public void setisoattr_22472(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22472", value));
        }
    }

    @XmlElement(name = "isoattr_22473")
    public void setisoattr_22473(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22473", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22474")
    public void setisoattr_22474(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22474", value));
        }
    }

    @XmlElement(name = "isoattr_22475")
    public void setisoattr_22475(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22475", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22476")
    public void setisoattr_22476(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22476", value));
        }
    }

    @XmlElement(name = "isoattr_22477")
    public void setisoattr_22477(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22477", value));
        }
    }

    @XmlElement(name = "isoattr_22478")
    public void setisoattr_22478(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22478", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22479")
    public void setisoattr_22479(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22479", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22480")
    public void setisoattr_22480(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22480", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22481")
    public void setisoattr_22481(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22481", value));
        }
    }

    @XmlElement(name = "isoattr_22482")
    public void setisoattr_22482(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22482", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22483")
    public void setisoattr_22483(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22483", value));
        }
    }

    @XmlElement(name = "isoattr_22484")
    public void setisoattr_22484(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22484", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22485")
    public void setisoattr_22485(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22485", value));
        }
    }

    @XmlElement(name = "isoattr_22486")
    public void setisoattr_22486(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22486", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22487")
    public void setisoattr_22487(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22487", value));
        }
    }

    @XmlElement(name = "isoattr_22488")
    public void setisoattr_22488(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22488", value));
        }
    }

    @XmlElement(name = "isoattr_22489")
    public void setisoattr_22489(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22489", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22490")
    public void setisoattr_22490(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22490", value));
        }
    }

    @XmlElement(name = "isoattr_22491")
    public void setisoattr_22491(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22491", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22492")
    public void setisoattr_22492(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22492", value));
        }
    }

    @XmlElement(name = "isoattr_22493")
    public void setisoattr_22493(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22493", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22494")
    public void setisoattr_22494(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22494", value));
        }
    }

    @XmlElement(name = "isoattr_22495")
    public void setisoattr_22495(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22495", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22496")
    public void setisoattr_22496(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22496", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22497")
    public void setisoattr_22497(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22497", value));
        }
    }

    @XmlElement(name = "isoattr_22498")
    public void setisoattr_22498(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22498", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22499")
    public void setisoattr_22499(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22499", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22500")
    public void setisoattr_22500(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22500", value));
        }
    }

    @XmlElement(name = "isoattr_22501")
    public void setisoattr_22501(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22501", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22502")
    public void setisoattr_22502(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22502", value));
        }
    }

    @XmlElement(name = "isoattr_22503")
    public void setisoattr_22503(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22503", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22504")
    public void setisoattr_22504(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22504", value));
        }
    }

    @XmlElement(name = "isoattr_22505")
    public void setisoattr_22505(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22505", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22506")
    public void setisoattr_22506(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22506", value));
        }
    }

    @XmlElement(name = "isoattr_22507")
    public void setisoattr_22507(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22507", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22508")
    public void setisoattr_22508(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22508", value));
        }
    }

    @XmlElement(name = "isoattr_22509")
    public void setisoattr_22509(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22509", value));
        }
    }

    @XmlElement(name = "isoattr_22510")
    public void setisoattr_22510(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22510", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22511")
    public void setisoattr_22511(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22511", value));
        }
    }

    @XmlElement(name = "isoattr_22512")
    public void setisoattr_22512(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22512", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22513")
    public void setisoattr_22513(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22513", value));
        }
    }

    @XmlElement(name = "isoattr_22514")
    public void setisoattr_22514(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22514", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22515")
    public void setisoattr_22515(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22515", value));
        }
    }

    @XmlElement(name = "isoattr_22516")
    public void setisoattr_22516(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22516", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22517")
    public void setisoattr_22517(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22517", value));
        }
    }

    @XmlElement(name = "isoattr_22518")
    public void setisoattr_22518(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22518", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22519")
    public void setisoattr_22519(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22519", value));
        }
    }

    @XmlElement(name = "isoattr_22520")
    public void setisoattr_22520(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22520", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22521")
    public void setisoattr_22521(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22521", value));
        }
    }

    @XmlElement(name = "isoattr_22522")
    public void setisoattr_22522(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22522", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22523")
    public void setisoattr_22523(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22523", value));
        }
    }

    @XmlElement(name = "isoattr_22524")
    public void setisoattr_22524(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22524", value));
        }
    }

    @XmlElement(name = "isoattr_22525")
    public void setisoattr_22525(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22525", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22526")
    public void setisoattr_22526(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22526", value));
        }
    }

    @XmlElement(name = "isoattr_22527")
    public void setisoattr_22527(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22527", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22528")
    public void setisoattr_22528(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22528", value));
        }
    }

    @XmlElement(name = "isoattr_22529")
    public void setisoattr_22529(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22529", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22530")
    public void setisoattr_22530(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22530", value));
        }
    }

    @XmlElement(name = "isoattr_22531")
    public void setisoattr_22531(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22531", value));
        }
    }

    @XmlElement(name = "isoattr_22532")
    public void setisoattr_22532(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22532", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22533")
    public void setisoattr_22533(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22533", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22534")
    public void setisoattr_22534(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22534", value));
        }
    }

    @XmlElement(name = "isoattr_22535")
    public void setisoattr_22535(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22535", value));
        }
    }

    @XmlElement(name = "isoattr_22536")
    public void setisoattr_22536(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22536", value));
        }
    }

    @XmlElement(name = "isoattr_22537")
    public void setisoattr_22537(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22537", value));
        }
    }

    @XmlElement(name = "isoattr_22538")
    public void setisoattr_22538(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22538", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22539")
    public void setisoattr_22539(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22539", value));
        }
    }

    @XmlElement(name = "isoattr_22540")
    public void setisoattr_22540(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22540", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22541")
    public void setisoattr_22541(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22541", value));
        }
    }

    @XmlElement(name = "isoattr_22542")
    public void setisoattr_22542(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22542", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22543")
    public void setisoattr_22543(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22543", value));
        }
    }

    @XmlElement(name = "isoattr_22544")
    public void setisoattr_22544(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22544", value));
        }
    }

    @XmlElement(name = "isoattr_22545")
    public void setisoattr_22545(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22545", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22546")
    public void setisoattr_22546(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22546", value));
        }
    }

    @XmlElement(name = "isoattr_22547")
    public void setisoattr_22547(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22547", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22548")
    public void setisoattr_22548(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22548", value));
        }
    }

    @XmlElement(name = "isoattr_22549")
    public void setisoattr_22549(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22549", value));
        }
    }

    @XmlElement(name = "isoattr_22550")
    public void setisoattr_22550(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22550", value));
        }
    }

    @XmlElement(name = "isoattr_22551")
    public void setisoattr_22551(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22551", value));
        }
    }

    @XmlElement(name = "isoattr_22552")
    public void setisoattr_22552(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22552", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22553")
    public void setisoattr_22553(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22553", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22554")
    public void setisoattr_22554(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22554", value));
        }
    }

    @XmlElement(name = "isoattr_22555")
    public void setisoattr_22555(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22555", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22556")
    public void setisoattr_22556(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22556", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22557")
    public void setisoattr_22557(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22557", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22558")
    public void setisoattr_22558(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22558", value));
        }
    }

    @XmlElement(name = "isoattr_22559")
    public void setisoattr_22559(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22559", value));
        }
    }

    @XmlElement(name = "isoattr_22560")
    public void setisoattr_22560(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22560", value));
        }
    }

    @XmlElement(name = "isoattr_22561")
    public void setisoattr_22561(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22561", value));
        }
    }

    @XmlElement(name = "isoattr_22562")
    public void setisoattr_22562(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22562", value));
        }
    }

    @XmlElement(name = "isoattr_22563")
    public void setisoattr_22563(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22563", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22564")
    public void setisoattr_22564(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22564", value));
        }
    }

    @XmlElement(name = "isoattr_22565")
    public void setisoattr_22565(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22565", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22566")
    public void setisoattr_22566(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22566", value));
        }
    }

    /**
     * Visas inte i gränssnittet idag. Ska inte visas i 2.0.
     @XmlElement(name = "isoattr_22567")
     public void setisoattr_22567(BlindReference xmlReference) { if(xmlReference != null){
     categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22567", xmlReference)); }
     }
     **/

    @XmlElement(name = "isoattr_22568")
    public void setisoattr_22568(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22568", value));
        }
    }

    @XmlElement(name = "isoattr_22569")
    public void setisoattr_22569(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22569", value));
        }
    }

    @XmlElement(name = "isoattr_22570")
    public void setisoattr_22570(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22570", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22571")
    public void setisoattr_22571(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22571", value));
        }
    }

    @XmlElement(name = "isoattr_22572")
    public void setisoattr_22572(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22572", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22573")
    public void setisoattr_22573(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22573", value));
        }
    }

    @XmlElement(name = "isoattr_22574")
    public void setisoattr_22574(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22574", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22575")
    public void setisoattr_22575(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22575", value));
        }
    }

    @XmlElement(name = "isoattr_22576")
    public void setisoattr_22576(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22576", value));
        }
    }

    @XmlElement(name = "isoattr_22577")
    public void setisoattr_22577(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22577", value));
        }
    }

    @XmlElement(name = "isoattr_22578")
    public void setisoattr_22578(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22578", value));
        }
    }

    @XmlElement(name = "isoattr_22579")
    public void setisoattr_22579(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22579", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22580")
    public void setisoattr_22580(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22580", value));
        }
    }

    @XmlElement(name = "isoattr_22581")
    public void setisoattr_22581(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22581", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22582")
    public void setisoattr_22582(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22582", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22583")
    public void setisoattr_22583(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22583", value));
        }
    }

    @XmlElement(name = "isoattr_22584")
    public void setisoattr_22584(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22584", value));
        }
    }

    @XmlElement(name = "isoattr_22585")
    public void setisoattr_22585(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22585", value));
        }
    }

    @XmlElement(name = "isoattr_22586")
    public void setisoattr_22586(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22586", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22587")
    public void setisoattr_22587(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22587", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22588")
    public void setisoattr_22588(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22588", value));
        }
    }

    @XmlElement(name = "isoattr_22589")
    public void setisoattr_22589(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22589", value));
        }
    }

    @XmlElement(name = "isoattr_22590")
    public void setisoattr_22590(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22590", value));
        }
    }

    @XmlElement(name = "isoattr_22591")
    public void setisoattr_22591(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22591", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22592")
    public void setisoattr_22592(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22592", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22593")
    public void setisoattr_22593(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22593", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22594")
    public void setisoattr_22594(ProductIntervalDecimal value) {
        if (value != null && !value.isEmpty()) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22594", value));
        }
    }

    @XmlElement(name = "isoattr_22595")
    public void setisoattr_22595(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22595", value));
        }
    }

    @XmlElement(name = "isoattr_22596")
    public void setisoattr_22596(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22596", value));
        }
    }

    @XmlElement(name = "isoattr_22597")
    public void setisoattr_22597(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22597", value));
        }
    }

    @XmlElement(name = "isoattr_22598")
    public void setisoattr_22598(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22598", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22599")
    public void setisoattr_22599(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22599", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22600")
    public void setisoattr_22600(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22600", value));
        }
    }

    @XmlElement(name = "isoattr_22601")
    public void setisoattr_22601(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22601", value));
        }
    }

    @XmlElement(name = "isoattr_22602")
    public void setisoattr_22602(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22602", value));
        }
    }

    @XmlElement(name = "isoattr_22603")
    public void setisoattr_22603(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22603", value));
        }
    }

    @XmlElement(name = "isoattr_22604")
    public void setisoattr_22604(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22604", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22605")
    public void setisoattr_22605(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22605", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22606")
    public void setisoattr_22606(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22606", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22607")
    public void setisoattr_22607(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22607", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22608")
    public void setisoattr_22608(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22608", value));
        }
    }

    @XmlElement(name = "isoattr_22609")
    public void setisoattr_22609(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22609", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22610")
    public void setisoattr_22610(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22610", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22611")
    public void setisoattr_22611(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22611", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22612")
    public void setisoattr_22612(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22612", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22613")
    public void setisoattr_22613(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22613", value));
        }
    }

    @XmlElement(name = "isoattr_22614")
    public void setisoattr_22614(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22614", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22615")
    public void setisoattr_22615(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22615", value));
        }
    }

    @XmlElement(name = "isoattr_22616")
    public void setisoattr_22616(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22616", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22617")
    public void setisoattr_22617(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22617", value));
        }
    }

    @XmlElement(name = "isoattr_22618")
    public void setisoattr_22618(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22618", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22619")
    public void setisoattr_22619(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22619", value));
        }
    }

    @XmlElement(name = "isoattr_22620")
    public void setisoattr_22620(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22620", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22621")
    public void setisoattr_22621(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22621", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22622")
    public void setisoattr_22622(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22622", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22623")
    public void setisoattr_22623(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22623", value));
        }
    }

    @XmlElement(name = "isoattr_22624")
    public void setisoattr_22624(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22624", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22625")
    public void setisoattr_22625(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22625", value));
        }
    }

    @XmlElement(name = "isoattr_22626")
    public void setisoattr_22626(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22626", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22627")
    public void setisoattr_22627(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22627", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22628")
    public void setisoattr_22628(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22628", value));
        }
    }

    @XmlElement(name = "isoattr_22629")
    public void setisoattr_22629(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22629", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22630")
    public void setisoattr_22630(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22630", value));
        }
    }

    @XmlElement(name = "isoattr_22631")
    public void setisoattr_22631(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22631", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22632")
    public void setisoattr_22632(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22632", value));
        }
    }

    @XmlElement(name = "isoattr_22633")
    public void setisoattr_22633(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22633", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22634")
    public void setisoattr_22634(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22634", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22635")
    public void setisoattr_22635(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22635", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22636")
    public void setisoattr_22636(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22636", value));
        }
    }

    @XmlElement(name = "isoattr_22637")
    public void setisoattr_22637(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22637", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22638")
    public void setisoattr_22638(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22638", value));
        }
    }

    @XmlElement(name = "isoattr_22639")
    public void setisoattr_22639(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22639", value));
        }
    }

    @XmlElement(name = "isoattr_22640")
    public void setisoattr_22640(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22640", value));
        }
    }

    @XmlElement(name = "isoattr_22641")
    public void setisoattr_22641(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22641", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22642")
    public void setisoattr_22642(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22642", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22643")
    public void setisoattr_22643(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22643", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22644")
    public void setisoattr_22644(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22644", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22645")
    public void setisoattr_22645(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22645", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22646")
    public void setisoattr_22646(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22646", value));
        }
    }

    @XmlElement(name = "isoattr_22647")
    public void setisoattr_22647(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22647", value));
        }
    }

    @XmlElement(name = "isoattr_22648")
    public void setisoattr_22648(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22648", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22649")
    public void setisoattr_22649(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22649", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22650")
    public void setisoattr_22650(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22650", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22651")
    public void setisoattr_22651(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22651", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22652")
    public void setisoattr_22652(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22652", value));
        }
    }

    @XmlElement(name = "isoattr_22653")
    public void setisoattr_22653(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22653", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22654")
    public void setisoattr_22654(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22654", value));
        }
    }

    @XmlElement(name = "isoattr_22655")
    public void setisoattr_22655(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22655", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22656")
    public void setisoattr_22656(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22656", value));
        }
    }

    @XmlElement(name = "isoattr_22657")
    public void setisoattr_22657(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22657", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22658")
    public void setisoattr_22658(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22658", value));
        }
    }

    @XmlElement(name = "isoattr_22659")
    public void setisoattr_22659(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22659", value));
        }
    }

    @XmlElement(name = "isoattr_22660")
    public void setisoattr_22660(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22660", value));
        }
    }

    @XmlElement(name = "isoattr_22661")
    public void setisoattr_22661(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22661", value));
        }
    }

    @XmlElement(name = "isoattr_22662")
    public void setisoattr_22662(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22662", value));
        }
    }

    @XmlElement(name = "isoattr_22663")
    public void setisoattr_22663(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22663", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22664")
    public void setisoattr_22664(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22664", value));
        }
    }

    @XmlElement(name = "isoattr_22665")
    public void setisoattr_22665(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22665", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22666")
    public void setisoattr_22666(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22666", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22667")
    public void setisoattr_22667(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22667", value));
        }
    }

    @XmlElement(name = "isoattr_22668")
    public void setisoattr_22668(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22668", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22669")
    public void setisoattr_22669(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22669", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22670")
    public void setisoattr_22670(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22670", value));
        }
    }

    @XmlElement(name = "isoattr_22671")
    public void setisoattr_22671(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22671", value));
        }
    }

    @XmlElement(name = "isoattr_22672")
    public void setisoattr_22672(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22672", value));
        }
    }

    @XmlElement(name = "isoattr_22673")
    public void setisoattr_22673(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22673", value));
        }
    }

    @XmlElement(name = "isoattr_22674")
    public void setisoattr_22674(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22674", value));
        }
    }

    @XmlElement(name = "isoattr_22675")
    public void setisoattr_22675(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22675", value));
        }
    }

    @XmlElement(name = "isoattr_22676")
    public void setisoattr_22676(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22676", value));
        }
    }

    @XmlElement(name = "isoattr_22677")
    public void setisoattr_22677(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22677", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22678")
    public void setisoattr_22678(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22678", value));
        }
    }

    @XmlElement(name = "isoattr_22679")
    public void setisoattr_22679(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22679", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22680")
    public void setisoattr_22680(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22680", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22681")
    public void setisoattr_22681(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22681", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22682")
    public void setisoattr_22682(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22682", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22683")
    public void setisoattr_22683(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22683", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22684")
    public void setisoattr_22684(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22684", value));
        }
    }

    @XmlElement(name = "isoattr_22685")
    public void setisoattr_22685(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22685", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22686")
    public void setisoattr_22686(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22686", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22687")
    public void setisoattr_22687(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22687", value));
        }
    }

    @XmlElement(name = "isoattr_22688")
    public void setisoattr_22688(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22688", value));
        }
    }

    @XmlElement(name = "isoattr_22689")
    public void setisoattr_22689(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22689", value));
        }
    }

    @XmlElement(name = "isoattr_22690")
    public void setisoattr_22690(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22690", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22691")
    public void setisoattr_22691(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22691", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22692")
    public void setisoattr_22692(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22692", value));
        }
    }

    @XmlElement(name = "isoattr_22693")
    public void setisoattr_22693(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22693", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22694")
    public void setisoattr_22694(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22694", value));
        }
    }

    @XmlElement(name = "isoattr_22695")
    public void setisoattr_22695(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22695", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22696")
    public void setisoattr_22696(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22696", value));
        }
    }

    @XmlElement(name = "isoattr_22697")
    public void setisoattr_22697(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22697", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22698")
    public void setisoattr_22698(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22698", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22699")
    public void setisoattr_22699(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22699", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22700")
    public void setisoattr_22700(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22700", value));
        }
    }

    @XmlElement(name = "isoattr_22701")
    public void setisoattr_22701(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22701", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22702")
    public void setisoattr_22702(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22702", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22703")
    public void setisoattr_22703(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22703", value));
        }
    }

    @XmlElement(name = "isoattr_22704")
    public void setisoattr_22704(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22704", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22705")
    public void setisoattr_22705(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22705", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22706")
    public void setisoattr_22706(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22706", value));
        }
    }

    @XmlElement(name = "isoattr_22707")
    public void setisoattr_22707(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22707", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22708")
    public void setisoattr_22708(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22708", value));
        }
    }

    @XmlElement(name = "isoattr_22709")
    public void setisoattr_22709(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22709", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22710")
    public void setisoattr_22710(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22710", value));
        }
    }

    @XmlElement(name = "isoattr_22711")
    public void setisoattr_22711(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22711", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22712")
    public void setisoattr_22712(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22712", value));
        }
    }

    @XmlElement(name = "isoattr_22713")
    public void setisoattr_22713(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22713", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22714")
    public void setisoattr_22714(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22714", value));
        }
    }

    @XmlElement(name = "isoattr_22715")
    public void setisoattr_22715(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22715", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22716")
    public void setisoattr_22716(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22716", value));
        }
    }

    @XmlElement(name = "isoattr_22717")
    public void setisoattr_22717(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22717", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22718")
    public void setisoattr_22718(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22718", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22719")
    public void setisoattr_22719(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22719", value));
        }
    }

    @XmlElement(name = "isoattr_22720")
    public void setisoattr_22720(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22720", value));
        }
    }

    @XmlElement(name = "isoattr_22721")
    public void setisoattr_22721(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22721", value));
        }
    }

    @XmlElement(name = "isoattr_22722")
    public void setisoattr_22722(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22722", value));
        }
    }

    @XmlElement(name = "isoattr_22723")
    public void setisoattr_22723(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22723", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22724")
    public void setisoattr_22724(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22724", value));
        }
    }

    @XmlElement(name = "isoattr_22725")
    public void setisoattr_22725(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22725", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22726")
    public void setisoattr_22726(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22726", value));
        }
    }

    @XmlElement(name = "isoattr_22727")
    public void setisoattr_22727(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22727", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22728")
    public void setisoattr_22728(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22728", value));
        }
    }

    @XmlElement(name = "isoattr_22729")
    public void setisoattr_22729(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22729", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22730")
    public void setisoattr_22730(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22730", value));
        }
    }

    @XmlElement(name = "isoattr_22731")
    public void setisoattr_22731(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22731", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22732")
    public void setisoattr_22732(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22732", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22733")
    public void setisoattr_22733(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22733", value));
        }
    }

    @XmlElement(name = "isoattr_22734")
    public void setisoattr_22734(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22734", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22735")
    public void setisoattr_22735(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22735", value));
        }
    }

    @XmlElement(name = "isoattr_22736")
    public void setisoattr_22736(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22736", value));
        }
    }

    @XmlElement(name = "isoattr_22737")
    public void setisoattr_22737(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22737", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22738")
    public void setisoattr_22738(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22738", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22739")
    public void setisoattr_22739(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22739", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22740")
    public void setisoattr_22740(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22740", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22741")
    public void setisoattr_22741(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22741", value));
        }
    }

    @XmlElement(name = "isoattr_22742")
    public void setisoattr_22742(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22742", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22743")
    public void setisoattr_22743(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22743", value));
        }
    }

    @XmlElement(name = "isoattr_22744")
    public void setisoattr_22744(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22744", value));
        }
    }

    @XmlElement(name = "isoattr_22745")
    public void setisoattr_22745(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22745", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22746")
    public void setisoattr_22746(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22746", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22747")
    public void setisoattr_22747(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22747", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22748")
    public void setisoattr_22748(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22748", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22749")
    public void setisoattr_22749(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22749", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22750")
    public void setisoattr_22750(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22750", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22751")
    public void setisoattr_22751(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22751", value));
        }
    }

    @XmlElement(name = "isoattr_22752")
    public void setisoattr_22752(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22752", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22753")
    public void setisoattr_22753(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22753", value));
        }
    }

    @XmlElement(name = "isoattr_22754")
    public void setisoattr_22754(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22754", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22755")
    public void setisoattr_22755(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22755", value));
        }
    }

    @XmlElement(name = "isoattr_22756")
    public void setisoattr_22756(BigDecimal value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22756", value));
        }
    }

    @XmlElement(name = "isoattr_22757")
    public void setisoattr_22757(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22757", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22758")
    public void setisoattr_22758(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22758", value));
        }
    }

    @XmlElement(name = "isoattr_22759")
    public void setisoattr_22759(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22759", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22760")
    public void setisoattr_22760(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22760", value));
        }
    }

    @XmlElement(name = "isoattr_22761")
    public void setisoattr_22761(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22761", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22762")
    public void setisoattr_22762(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22762", value));
        }
    }

    @XmlElement(name = "isoattr_22763")
    public void setisoattr_22763(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22763", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22764")
    public void setisoattr_22764(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22764", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22765")
    public void setisoattr_22765(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22765", value));
        }
    }

    @XmlElement(name = "isoattr_22766")
    public void setisoattr_22766(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22766", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22767")
    public void setisoattr_22767(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22767", value));
        }
    }

    @XmlElement(name = "isoattr_22768")
    public void setisoattr_22768(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22768", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22769")
    public void setisoattr_22769(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22769", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22770")
    public void setisoattr_22770(BlindReference xmlReference) {
        if (xmlReference != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22770", xmlReference));
        }
    }

    @XmlElement(name = "isoattr_22771")
    public void setisoattr_22771(String value) {
        if (value != null) {
            categorySpecificProperties.add(ProductCategorySpecificProperty.of("isoattr_22771", value));
        }
    }

}

/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.agreements.builders;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.business.organization.controller.BusinessLevelController;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.model.api.AgreementAPI;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVGuaranteeUnitAPI;
import se.inera.hjmtj.migration.domain.DateTimes;
import se.inera.hjmtj.migration.domain.exceptions.MigrationException;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementBase;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementHead;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementWarrantyBase;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementWarrantyUnitBase;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MessageTemplate;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjCustomerRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjCvGuaranteeUnitRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjOrganizationRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjSupplierRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjUserRepository;
import se.inera.hjmtj.migration.infra.organizations.CustomerRepository;
import se.inera.hjmtj.migration.infra.products.ProductOrganizationRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Slf4j
@Accessors(fluent = true)
@RequiredArgsConstructor
@Getter
@Setter
public abstract class AgreementAPIBuilderBase<R> {

    private AgreementBase agreement;

    private OrganizationAPI supplier;

    private HjmtjOrganizationRepository hjmtjOrganizationRepository;

    private HjmtjCustomerRepository hjmtjCustomerRepository;

    private HjmtjSupplierRepository hjmtjSupplierRepository;

    private ProductOrganizationRepository productOrganizationRepository;

    private HjmtjUserRepository hjmtjUserRepository;

    private CustomerRepository customerRepository;

    private List<AgreementPricelistAPI> priceLists;

    private MigrationErrorRepository migrationErrorRepository;

    private HjmtjCvGuaranteeUnitRepository hjmtjCvGuaranteeUnitRepository;

    private OrganizationController organizationController;

    public abstract R build() throws MigrationException;

    protected AgreementAPICreated doBuild() throws MigrationException {
        Objects.requireNonNull(this.agreement, "agreement must be non-null");
        Objects.requireNonNull(supplier, "supplier must be non-null");
        Objects.requireNonNull(hjmtjOrganizationRepository, "hjmtjOrganizationRepository must be non-null");
        Objects.requireNonNull(productOrganizationRepository, "productOrganizationRepository must be non-null");
        Objects.requireNonNull(hjmtjUserRepository, "hjmtjUserRepository must be non-null");
        Objects.requireNonNull(customerRepository, "customerRepository must be non-null");
        Objects.requireNonNull(priceLists, "priceLists must be non-null");
        Objects.requireNonNull(migrationErrorRepository, "migrationErrorRepository must be non-null");
        Objects.requireNonNull(hjmtjCvGuaranteeUnitRepository, "hjmtjCvGuaranteeUnitRepository must be non-null");
        Objects.requireNonNull(hjmtjCustomerRepository, "hjmtjCustomerRepository must be non-null");
        Objects.requireNonNull(hjmtjSupplierRepository, "hjmtjSupplierRepository must be non-null");
        Objects.requireNonNull(organizationController, "organizationController must be non-null");

        AgreementBase baseAgreement = agreement();
        AgreementHead head = baseAgreement.getHead();
        AgreementAPICreated agreementApi = new AgreementAPICreated();
        agreementApi.setCreated(baseAgreement.getCreated());
        agreementApi.setAgreementNumber(head.getSellerAgreementNo());
        agreementApi.setSendReminderOn(DateTimes.toEpochMillis(baseAgreement.getExpirationReminderOn()));
        agreementApi.setSupplierOrganization(supplier());
        agreementApi.setAgreementName(head.getTitle());
        setValidity(baseAgreement, agreementApi);
        setExtensionOptionTo(baseAgreement, agreementApi);
        setDeliveryTime(agreementApi, baseAgreement.getLeadTimeDays());
        setWarranty(baseAgreement, agreementApi);

        agreementApi.setPricelists(priceLists);

        return agreementApi;
    }

    private void setValidity(AgreementBase baseAgreement, AgreementAPI agreementApi) {
        long validFrom = DateTimes.toEpochMillis(baseAgreement.getValid()
                                                              .getBegins());
        agreementApi.setValidFrom(validFrom);

        long validTo = DateTimes.toEpochMillis(baseAgreement.getValid()
                                                            .getEnds());
        agreementApi.setValidTo(validTo);
    }

    private void setExtensionOptionTo(AgreementBase baseAgreement, AgreementAPI agreementApi) {
        if (baseAgreement.getExtension() != null) {
            agreementApi.setExtensionOptionTo(DateTimes.toEpochMillis(baseAgreement.getExtension()
                                                                                   .getToDate()));
        }
    }

    private void setWarranty(AgreementBase baseAgreement, AgreementAPI agreementApi) {
        AgreementWarrantyBase warranty = baseAgreement.getWarranty();
        if (warranty != null) {
            setWarrantyTerms(agreementApi, warranty.getTermsAndConditions());
            setWarrantyUnit(agreementApi, warranty.getUnit());
            setWarrantyQuantity(agreementApi, warranty.getNumericValue());
        }
    }

    private void setDeliveryTime(AgreementAPI agreementApi, Long leadTimeDays) {
        if (leadTimeDays == null) {
            MigrationError migrationError = new MigrationError();
            migrationError.setSeverity(MigrationError.Severity.WARNING);
            migrationError.setOrganizationName(supplier.getOrganizationName());
            migrationError.setClassifier(getAgreementDeliveryTimeClassifier());
            migrationError.setAgreementNumber(agreementApi.getAgreementNumber());
            migrationError.setMessage(MessageTemplate.DELIVERY_TIME_CHANGED,0);
            migrationErrorRepository.add(migrationError);

            leadTimeDays = 0L;
        }

        agreementApi.setDeliveryTimeH(leadTimeDays.intValue());
        agreementApi.setDeliveryTimeR(leadTimeDays.intValue());
        agreementApi.setDeliveryTimeT(leadTimeDays.intValue());
        agreementApi.setDeliveryTimeI(leadTimeDays.intValue());
        agreementApi.setDeliveryTimeTJ(leadTimeDays.intValue());
    }

    private void setWarrantyTerms(AgreementAPI agreementApi, String termsAndConditions) {
        if (termsAndConditions != null) {
            String termsAndConditionsShortened = termsAndConditions.substring(0, Math.min(255, termsAndConditions.length()));
            agreementApi.setWarrantyTermsH(termsAndConditionsShortened);
            agreementApi.setWarrantyTermsR(termsAndConditionsShortened);
            agreementApi.setWarrantyTermsT(termsAndConditionsShortened);
            agreementApi.setWarrantyTermsI(termsAndConditionsShortened);
            agreementApi.setWarrantyTermsTJ(termsAndConditionsShortened);
        }
    }

    private void setWarrantyUnit(AgreementAPI agreementApi, AgreementWarrantyUnitBase warrantyUnit) {
        if (warrantyUnit != null) {
            CVGuaranteeUnitAPI hjmtjWarrantyUnit = hjmtjCvGuaranteeUnitRepository.findByCode(warrantyUnit.getCode());

            if (hjmtjWarrantyUnit == null) {
                MigrationError migrationError = new MigrationError();
                migrationError.setClassifier(getAgreementWarrantyUnitClassifier());
                migrationError.setMessage(MessageTemplate.CVGUARANTEE_VALUE_NOT_FOUND, warrantyUnit.getCode());
                migrationError.setAgreementNumber(agreementApi.getAgreementNumber());
                migrationErrorRepository.add(migrationError);
            } else {
                agreementApi.setWarrantyQuantityHUnit(hjmtjWarrantyUnit);
                agreementApi.setWarrantyQuantityRUnit(hjmtjWarrantyUnit);
                agreementApi.setWarrantyQuantityTUnit(hjmtjWarrantyUnit);
                agreementApi.setWarrantyQuantityIUnit(hjmtjWarrantyUnit);
                agreementApi.setWarrantyQuantityTJUnit(hjmtjWarrantyUnit);
            }
        }
    }

    private void setWarrantyQuantity(AgreementAPI agreementApi, Long warrantyQuantity) {
        if (warrantyQuantity != null) {
            if (warrantyQuantity == 0) {
                warrantyQuantity = 1L;

                MigrationError migrationError = new MigrationError();
                migrationError.setSeverity(MigrationError.Severity.WARNING);
                migrationError.setOrganizationName(supplier.getOrganizationName());
                migrationError.setClassifier(getAgreementWarrantyQuantityClassifier());
                migrationError.setAgreementNumber(agreementApi.getAgreementNumber());
                migrationError.setMessage(MessageTemplate.WARRANTY_QUANTITY_CHANGED, 0 ,1);
                migrationErrorRepository.add(migrationError);
            }

            agreementApi.setWarrantyQuantityH(warrantyQuantity.intValue());
            agreementApi.setWarrantyQuantityR(warrantyQuantity.intValue());
            agreementApi.setWarrantyQuantityT(warrantyQuantity.intValue());
            agreementApi.setWarrantyQuantityI(warrantyQuantity.intValue());
            agreementApi.setWarrantyQuantityTJ(warrantyQuantity.intValue());
        }
    }

    protected abstract Classifier getAgreementDeliveryTimeClassifier();

    protected abstract Classifier getAgreementWarrantyUnitClassifier();

    protected abstract Classifier getAgreementWarrantyQuantityClassifier();

    protected abstract Classifier getAgreementClassifier();

    public static class AgreementAPICreated extends AgreementAPI {

        private LocalDateTime created;

        public LocalDateTime getCreated() {
            return created;
        }

        public void setCreated(LocalDateTime created) {
            this.created = created;
        }
    }

}

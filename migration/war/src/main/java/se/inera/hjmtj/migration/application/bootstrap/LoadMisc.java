/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package se.inera.hjmtj.migration.application.bootstrap;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.model.entity.UserPermission;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEDirective;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEStandard;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCountry;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCounty;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVDocumentType;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVMunicipality;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVOrderUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPackageUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;
import se.inera.hjmtj.migration.domain.HeapUsageInterceptor;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjCvCeDirectiveRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjCvCeStandardRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjCvGuaranteeUnitRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjCvOrderUnit;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Loads misc CSVs into Mysql.
 */
@Slf4j
@Singleton
@Interceptors({PerformanceLogInterceptor.class, HeapUsageInterceptor.class})
public class LoadMisc {

    @PersistenceContext(unitName = "HjmtjPU")
    private EntityManager em;

    @Inject
    private HjmtjCvGuaranteeUnitRepository hjmtjCvGuaranteeUnitRepository;

    @Inject
    private HjmtjCvCeStandardRepository hjmtjCvCeStandardRepository;

    @Inject
    private HjmtjCvCeDirectiveRepository hjmtjCvCeDirectiveRepository;

    @Inject
    private HjmtjCvOrderUnit hjmtjCvOrderUnit;

    public void run() {
        if (hjmtjCvGuaranteeUnitRepository.isEmpty()) {
            loadPreventiveMaintenances();
            loadGuaranteeUnits();
            loadOrderUnits();
            loadPackageUnits();
            loadDirectives();
            loadStandards();
            loadRolesAndPermissions();
            loadDocumentTypes();
            loadCountries();
            loadCountiesAndMunicipalities();
        }

        hjmtjCvGuaranteeUnitRepository.initializeCache();
        hjmtjCvCeDirectiveRepository.initializeCache();
        hjmtjCvCeStandardRepository.initializeCache();
        hjmtjCvOrderUnit.initializeCache();
    }

    private List<CVPackageUnit> loadPackageUnits() {
        log.info("loadPackageUnits()");
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        List<CVPackageUnit> packageUnits = new ArrayList<>();
        try {
            inputStream = this.getClass()
                              .getClassLoader()
                              .getResourceAsStream("packageunits.csv");
            if (inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
                String nextLine;
                boolean first = true;
                while ((nextLine = bufferedReader.readLine()) != null) {
                    if (first) {
                        first = false;
                        continue;
                    }
                    nextLine = nextLine.trim();
                    if (nextLine != null && !nextLine.isEmpty()) {
                        String[] lineParts = nextLine.split(";");
                        int order = Integer.parseInt(lineParts[0]);
                        String code = lineParts[1];
                        String name = lineParts[2];
                        CVPackageUnit packageUnit = new CVPackageUnit();
                        packageUnit.setDisplayOrder(order);
                        packageUnit.setCode(code);
                        packageUnit.setName(name);
                        em.persist(packageUnit);
                        packageUnits.add(packageUnit);
                    }
                }
            } else {
                log.error("No packageunits.csv found");
            }
        } catch (IOException ex) {
            log.error("No packageunits.csv found", ex);
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException ex) {
                log.warn("Failed to close packageunits.csv", ex);
            }
        }

        return packageUnits;
    }

    private void loadCountiesAndMunicipalities() {
        log.info("loadCountiesAndMunicipalities()");
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        try {
            inputStream = this.getClass()
                              .getClassLoader()
                              .getResourceAsStream("countymunicipality.csv");
            if (inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
                String nextLine;
                boolean first = true;
                CVCounty lastCounty = null;
                while ((nextLine = bufferedReader.readLine()) != null) {
                    if (first) {
                        first = false;
                        continue;
                    }
                    nextLine = nextLine.trim();
                    if (nextLine != null && !nextLine.isEmpty()) {
                        String[] lineParts = nextLine.split(";", -1);
                        String code = lineParts[0];
                        String name = lineParts[1];
                        Boolean showMunicipalities = null;
                        if (code.length() == 2) {
                            String showMunicipalitiesString = lineParts[2];
                            showMunicipalities = Boolean.parseBoolean(showMunicipalitiesString);
                            CVCounty county = new CVCounty();
                            county.setCode(code);
                            county.setName(name);
                            county.setShowMunicipalities(showMunicipalities);
                            em.persist(county);
                            lastCounty = county;
                        } else {
                            CVMunicipality municipality = new CVMunicipality();
                            municipality.setCode(code);
                            municipality.setName(name);
                            municipality.setCounty(lastCounty);
                            em.persist(municipality);
                        }
                    }
                }

            } else {
                log.warn("No countymunicipality.csv found");
            }
        } catch (IOException ex) {
            log.error("No countymunicipality.csv found", ex);
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException ex) {
                log.error("Failed to close countymunicipality.csv", ex);
            }
        }
    }

    /**
     * Load countries based on ISO 3166-1
     *
     * @return
     */
    private CVCountry loadCountries() {
        log.info("loadCountries()");
        CVCountry sweden = new CVCountry();
        sweden.setCode("SE");
        sweden.setName("Sverige");
        em.persist(sweden);
        CVCountry norway = new CVCountry();
        norway.setCode("NO");
        norway.setName("Norge");
        em.persist(norway);
        CVCountry denmark = new CVCountry();
        denmark.setCode("DK");
        denmark.setName("Danmark");
        em.persist(denmark);
        CVCountry finland = new CVCountry();
        finland.setCode("FI");
        finland.setName("Finland");
        em.persist(finland);
        CVCountry germany = new CVCountry();
        germany.setCode("DE");
        germany.setName("Tyskland");
        em.persist(germany);
        CVCountry netherlands = new CVCountry();
        netherlands.setCode("NL");
        netherlands.setName("Nederländerna");
        em.persist(netherlands);
        return sweden;
    }

    private List<UserRole> loadRolesAndPermissions() {
        log.info("loadRolesAndPermissions()");
        List<UserRole> userRoles = new ArrayList<>();

        UserPermission organizationViewPermission = new UserPermission();
        organizationViewPermission.setName("organization:view");
        organizationViewPermission.setDescription("View organization details");
        em.persist(organizationViewPermission);

        UserPermission organizationCreatePermission = new UserPermission();
        organizationCreatePermission.setName("organization:create");
        organizationCreatePermission.setDescription("Create organizations");
        em.persist(organizationCreatePermission);

        UserPermission organizationUpdatePermission = new UserPermission();
        organizationUpdatePermission.setName("organization:update");
        organizationUpdatePermission.setDescription("Update organizations");
        em.persist(organizationUpdatePermission);

        UserPermission organizationUpdateContactPermission = new UserPermission();
        organizationUpdateContactPermission.setName("organization:update_contact");
        organizationUpdateContactPermission.setDescription("Update organization contact information on own organization");
        em.persist(organizationUpdateContactPermission);

        UserPermission organizationDeletePermission = new UserPermission();
        organizationDeletePermission.setName("organization:delete");
        organizationDeletePermission.setDescription("Delete organizations");
        em.persist(organizationDeletePermission);

        UserPermission businessLevelViewPermission = new UserPermission();
        businessLevelViewPermission.setName("businesslevel:view");
        businessLevelViewPermission.setDescription("View business levels on organization");
        em.persist(businessLevelViewPermission);

        UserPermission businessLevelCreatePermission = new UserPermission();
        businessLevelCreatePermission.setName("businesslevel:create");
        businessLevelCreatePermission.setDescription("Create business levels on organization");
        em.persist(businessLevelCreatePermission);

        UserPermission businessLevelInactivatePermission = new UserPermission();
        businessLevelInactivatePermission.setName("businesslevel:inactivate");
        businessLevelInactivatePermission.setDescription("Inactivate business levels on organization");
        em.persist(businessLevelInactivatePermission);

        UserPermission businessLevelDeletePermission = new UserPermission();
        businessLevelDeletePermission.setName("businesslevel:delete");
        businessLevelDeletePermission.setDescription("Delete business levels on organization");
        em.persist(businessLevelDeletePermission);

        UserPermission userViewPermission = new UserPermission();
        userViewPermission.setName("user:view");
        userViewPermission.setDescription("View user details");
        em.persist(userViewPermission);

        UserPermission userCreatePermission = new UserPermission();
        userCreatePermission.setName("user:create");
        userCreatePermission.setDescription("Create users");
        em.persist(userCreatePermission);

        UserPermission userCreateOwnPermission = new UserPermission();
        userCreateOwnPermission.setName("user:create_own");
        userCreateOwnPermission.setDescription("Create users on own organization");
        em.persist(userCreateOwnPermission);

        UserPermission userUpdatePermission = new UserPermission();
        userUpdatePermission.setName("user:update");
        userUpdatePermission.setDescription("Update users");
        em.persist(userUpdatePermission);

        UserPermission userUpdateOwnPermission = new UserPermission();
        userUpdateOwnPermission.setName("user:update_own");
        userUpdateOwnPermission.setDescription("Update users on own organization");
        em.persist(userUpdateOwnPermission);

        UserPermission userUpdateContactPermission = new UserPermission();
        userUpdateContactPermission.setName("user:update_contact");
        userUpdateContactPermission.setDescription("Update my own contact information");
        em.persist(userUpdateContactPermission);

        UserPermission userDeletePermission = new UserPermission();
        userDeletePermission.setName("user:delete");
        userDeletePermission.setDescription("Delete users");
        em.persist(userDeletePermission);

        UserPermission userDeleteOwnPermission = new UserPermission();
        userDeleteOwnPermission.setName("user:delete_own");
        userDeleteOwnPermission.setDescription("Delete users on own organization");
        em.persist(userDeleteOwnPermission);

        UserPermission profileViewPermission = new UserPermission();
        profileViewPermission.setName("profile:view");
        profileViewPermission.setDescription("View personal profile");
        em.persist(profileViewPermission);

        UserPermission rolesViewPermission = new UserPermission();
        rolesViewPermission.setName("roles:view");
        rolesViewPermission.setDescription("View available roles");
        em.persist(rolesViewPermission);

        UserPermission productViewPermission = new UserPermission();
        productViewPermission.setName("product:view");
        productViewPermission.setDescription("View product information");
        em.persist(productViewPermission);

        UserPermission productCreateOwnPermission = new UserPermission();
        productCreateOwnPermission.setName("product:create_own");
        productCreateOwnPermission.setDescription("Create product on own organization");
        em.persist(productCreateOwnPermission);

        UserPermission productUpdateOwnPermission = new UserPermission();
        productUpdateOwnPermission.setName("product:update_own");
        productUpdateOwnPermission.setDescription("Update product on own organization");
        em.persist(productUpdateOwnPermission);

        UserPermission articleViewPermission = new UserPermission();
        articleViewPermission.setName("article:view");
        articleViewPermission.setDescription("View article information");
        em.persist(articleViewPermission);

        UserPermission articleCreateOwnPermission = new UserPermission();
        articleCreateOwnPermission.setName("article:create_own");
        articleCreateOwnPermission.setDescription("Create article on own organization");
        em.persist(articleCreateOwnPermission);

        UserPermission articleUpdateOwnPermission = new UserPermission();
        articleUpdateOwnPermission.setName("article:update_own");
        articleUpdateOwnPermission.setDescription("Update article on own organization");
        em.persist(articleUpdateOwnPermission);

        UserPermission categoryViewPermission = new UserPermission();
        categoryViewPermission.setName("category:view");
        categoryViewPermission.setDescription("View categories on products");
        em.persist(categoryViewPermission);

        UserPermission ceViewPermission = new UserPermission();
        ceViewPermission.setName("ce:view");
        ceViewPermission.setDescription("View CE standards and directives");
        em.persist(ceViewPermission);

        UserPermission orderUnitViewPermission = new UserPermission();
        orderUnitViewPermission.setName("orderunit:view");
        orderUnitViewPermission.setDescription("View order units");
        em.persist(orderUnitViewPermission);

        UserPermission packageUnitViewPermission = new UserPermission();
        packageUnitViewPermission.setName("packageunit:view");
        packageUnitViewPermission.setDescription("View package units");
        em.persist(packageUnitViewPermission);

        UserPermission guaranteeUnitViewPermission = new UserPermission();
        guaranteeUnitViewPermission.setName("guaranteeunit:view");
        guaranteeUnitViewPermission.setDescription("View guarantee units");
        em.persist(guaranteeUnitViewPermission);

        UserPermission preventiveMaintenanceViewPermission = new UserPermission();
        preventiveMaintenanceViewPermission.setName("preventivemaintenance:view");
        preventiveMaintenanceViewPermission.setDescription("View preventive maintenances");
        em.persist(preventiveMaintenanceViewPermission);

        UserPermission agreementViewOwnPermission = new UserPermission();
        agreementViewOwnPermission.setName("agreement:view_own");
        agreementViewOwnPermission.setDescription("View agreement on own organization");
        em.persist(agreementViewOwnPermission);

        UserPermission agreementCreateOwnPermission = new UserPermission();
        agreementCreateOwnPermission.setName("agreement:create_own");
        agreementCreateOwnPermission.setDescription("Create agreement on own organization");
        em.persist(agreementCreateOwnPermission);

        UserPermission agreementUpdateOwnPermission = new UserPermission();
        agreementUpdateOwnPermission.setName("agreement:update_own");
        agreementUpdateOwnPermission.setDescription("Update agreement on own organization");
        em.persist(agreementUpdateOwnPermission);

        UserPermission pricelistViewOwnPermission = new UserPermission();
        pricelistViewOwnPermission.setName("pricelist:view_own");
        pricelistViewOwnPermission.setDescription("View pricelist on own organization");
        em.persist(pricelistViewOwnPermission);

        UserPermission pricelistCreateOwnPermission = new UserPermission();
        pricelistCreateOwnPermission.setName("pricelist:create_own");
        pricelistCreateOwnPermission.setDescription("Create pricelist on own organization");
        em.persist(pricelistCreateOwnPermission);

        UserPermission pricelistUpdateOwnPermission = new UserPermission();
        pricelistUpdateOwnPermission.setName("pricelist:update_own");
        pricelistUpdateOwnPermission.setDescription("Update pricelist on own organization");
        em.persist(pricelistUpdateOwnPermission);

        UserPermission pricelistRowViewExistsPermission = new UserPermission();
        pricelistRowViewExistsPermission.setName("pricelistrow:view_exists");
        pricelistRowViewExistsPermission.setDescription("View pricelist exists on row on any");
        em.persist(pricelistRowViewExistsPermission);

        UserPermission pricelistRowViewOwnPermission = new UserPermission();
        pricelistRowViewOwnPermission.setName("pricelistrow:view_own");
        pricelistRowViewOwnPermission.setDescription("View pricelist row on pricelist");
        em.persist(pricelistRowViewOwnPermission);

        UserPermission pricelistRowCreateOwnPermission = new UserPermission();
        pricelistRowCreateOwnPermission.setName("pricelistrow:create_own");
        pricelistRowCreateOwnPermission.setDescription("Create pricelist row on pricelist");
        em.persist(pricelistRowCreateOwnPermission);

        UserPermission pricelistRowUpdateOwnPermission = new UserPermission();
        pricelistRowUpdateOwnPermission.setName("pricelistrow:update_own");
        pricelistRowUpdateOwnPermission.setDescription("Update pricelist row on pricelist");
        em.persist(pricelistRowUpdateOwnPermission);

        UserPermission pricelistRowSendForCustomerApprovalPermission = new UserPermission();
        pricelistRowSendForCustomerApprovalPermission.setName("pricelistrow:sendForCustomerApproval");
        pricelistRowSendForCustomerApprovalPermission.setDescription("Send pricelist row on pricelist for customer approval");
        em.persist(pricelistRowSendForCustomerApprovalPermission);

        UserPermission pricelistRowDeclinePermission = new UserPermission();
        pricelistRowDeclinePermission.setName("pricelistrow:decline");
        pricelistRowDeclinePermission.setDescription("Decline pricelist row on pricelist");
        em.persist(pricelistRowDeclinePermission);

        UserPermission pricelistRowActivatePermission = new UserPermission();
        pricelistRowActivatePermission.setName("pricelistrow:activate");
        pricelistRowActivatePermission.setDescription("Activate pricelist row on pricelist");
        em.persist(pricelistRowActivatePermission);

        UserPermission pricelistRowInactivatePermission = new UserPermission();
        pricelistRowInactivatePermission.setName("pricelistrow:inactivate");
        pricelistRowInactivatePermission.setDescription("Inactivate pricelist row on pricelist");
        em.persist(pricelistRowInactivatePermission);

        UserPermission pricelistRowApproveInactivatePermission = new UserPermission();
        pricelistRowApproveInactivatePermission.setName("pricelistrow:approve_inactivate");
        pricelistRowApproveInactivatePermission.setDescription("Approve inactivation of pricelist row on pricelist");
        em.persist(pricelistRowApproveInactivatePermission);

        UserPermission pricelistRowDeclineInactivatePermission = new UserPermission();
        pricelistRowDeclineInactivatePermission.setName("pricelistrow:decline_inactivate");
        pricelistRowDeclineInactivatePermission.setDescription("Decline inactivation of pricelist row on pricelist");
        em.persist(pricelistRowDeclineInactivatePermission);

        UserPermission pricelistRowReopenInactivatedPermission = new UserPermission();
        pricelistRowReopenInactivatedPermission.setName("pricelistrow:reopen");
        pricelistRowReopenInactivatedPermission.setDescription("Reopen inactivated pricelist rows on pricelist");
        em.persist(pricelistRowReopenInactivatedPermission);

        UserPermission generalPricelistViewPermission = new UserPermission();
        generalPricelistViewPermission.setName("generalpricelist:view");
        generalPricelistViewPermission.setDescription("View general pricelist");
        em.persist(generalPricelistViewPermission);

        UserPermission generalPricelistViewAllPermission = new UserPermission();
        generalPricelistViewAllPermission.setName("generalpricelist:view_all");
        generalPricelistViewAllPermission.setDescription("View general pricelist any organization");
        em.persist(generalPricelistViewAllPermission);

        UserPermission generalPricelistCreateOwnPermission = new UserPermission();
        generalPricelistCreateOwnPermission.setName("generalpricelist:create_own");
        generalPricelistCreateOwnPermission.setDescription("Create general pricelist");
        em.persist(generalPricelistCreateOwnPermission);

        UserPermission generalPricelistUpdateOwnPermission = new UserPermission();
        generalPricelistUpdateOwnPermission.setName("generalpricelist:update_own");
        generalPricelistUpdateOwnPermission.setDescription("Update general pricelist");
        em.persist(generalPricelistUpdateOwnPermission);

        UserPermission generalPricelistPricelistViewOwnPermission = new UserPermission();
        generalPricelistPricelistViewOwnPermission.setName("generalpricelist_pricelist:view_own");
        generalPricelistPricelistViewOwnPermission.setDescription("View pricelist on general pricelist own organization");
        em.persist(generalPricelistPricelistViewOwnPermission);

        UserPermission generalPricelistPricelistViewAllPermission = new UserPermission();
        generalPricelistPricelistViewAllPermission.setName("generalpricelist_pricelist:view_all");
        generalPricelistPricelistViewAllPermission.setDescription("View pricelist on general pricelist any organization");
        em.persist(generalPricelistPricelistViewAllPermission);

        UserPermission generalPricelistPricelistCreateOwnPermission = new UserPermission();
        generalPricelistPricelistCreateOwnPermission.setName("generalpricelist_pricelist:create_own");
        generalPricelistPricelistCreateOwnPermission.setDescription("Create pricelist on general pricelist own organization");
        em.persist(generalPricelistPricelistCreateOwnPermission);

        UserPermission generalPricelistPricelistUpdateOwnPermission = new UserPermission();
        generalPricelistPricelistUpdateOwnPermission.setName("generalpricelist_pricelist:update_own");
        generalPricelistPricelistUpdateOwnPermission.setDescription("Update pricelist on general pricelist own organization");
        em.persist(generalPricelistPricelistUpdateOwnPermission);

        UserPermission generalPricelistPricelistRowViewOwnPermission = new UserPermission();
        generalPricelistPricelistRowViewOwnPermission.setName("generalpricelist_pricelistrow:view_own");
        generalPricelistPricelistRowViewOwnPermission.setDescription("View pricelist rows on general pricelist own organization");
        em.persist(generalPricelistPricelistRowViewOwnPermission);

        UserPermission generalPricelistPricelistRowViewAllPermission = new UserPermission();
        generalPricelistPricelistRowViewAllPermission.setName("generalpricelist_pricelistrow:view_all");
        generalPricelistPricelistRowViewAllPermission.setDescription("View pricelist rows on general pricelist any organization");
        em.persist(generalPricelistPricelistRowViewAllPermission);

        UserPermission generalPricelistPricelistRowCreateOwnPermission = new UserPermission();
        generalPricelistPricelistRowCreateOwnPermission.setName("generalpricelist_pricelistrow:create_own");
        generalPricelistPricelistRowCreateOwnPermission.setDescription("Create pricelist row on general pricelist own organization");
        em.persist(generalPricelistPricelistRowCreateOwnPermission);

        UserPermission generalPricelistPricelistRowInactivateOwnPermission = new UserPermission();
        generalPricelistPricelistRowInactivateOwnPermission.setName("generalpricelist_pricelistrow:inactivate_own");
        generalPricelistPricelistRowInactivateOwnPermission.setDescription("Inactivate pricelist row on general pricelist own organization");
        em.persist(generalPricelistPricelistRowInactivateOwnPermission);

        UserPermission generalPricelistPricelistRowUpdateOwnPermission = new UserPermission();
        generalPricelistPricelistRowUpdateOwnPermission.setName("generalpricelist_pricelistrow:update_own");
        generalPricelistPricelistRowUpdateOwnPermission.setDescription("Update pricelist row on general pricelist own organization");
        em.persist(generalPricelistPricelistRowUpdateOwnPermission);

        UserPermission generalPricelistSearchAllPermission = new UserPermission();
        generalPricelistSearchAllPermission.setName("generalpricelist:search_all");
        generalPricelistSearchAllPermission.setDescription("Search general pricelists from all suppliers");
        em.persist(generalPricelistSearchAllPermission);

        // assortment
        UserPermission assortmentViewAllPermission = new UserPermission();
        assortmentViewAllPermission.setName("assortment:view_all");
        assortmentViewAllPermission.setDescription("View all assortments");
        em.persist(assortmentViewAllPermission);

        UserPermission assortmentViewPermission = new UserPermission();
        assortmentViewPermission.setName("assortment:view");
        assortmentViewPermission.setDescription("View assortment on organization");
        em.persist(assortmentViewPermission);

        UserPermission assortmentCreatePermission = new UserPermission();
        assortmentCreatePermission.setName("assortment:create");
        assortmentCreatePermission.setDescription("Create assortment on organization");
        em.persist(assortmentCreatePermission);

        UserPermission assortmentUpdatePermission = new UserPermission();
        assortmentUpdatePermission.setName("assortment:update");
        assortmentUpdatePermission.setDescription("Update assortment on organization");
        em.persist(assortmentUpdatePermission);

        UserPermission assortmentArticleViewPermission = new UserPermission();
        assortmentArticleViewPermission.setName("assortmentarticle:view");
        assortmentArticleViewPermission.setDescription("View articles on assortment");
        em.persist(assortmentArticleViewPermission);

        UserPermission assortmentArticleCreateAllPermission = new UserPermission();
        assortmentArticleCreateAllPermission.setName("assortmentarticle:create_all");
        assortmentArticleCreateAllPermission.setDescription("Create articles on any assortment on organization");
        em.persist(assortmentArticleCreateAllPermission);

        UserPermission assortmentArticleCreateOwnPermission = new UserPermission();
        assortmentArticleCreateOwnPermission.setName("assortmentarticle:create_own");
        assortmentArticleCreateOwnPermission.setDescription("Create articles on assigned assortments on organization");
        em.persist(assortmentArticleCreateOwnPermission);

        UserPermission assortmentArticleDeleteAllPermission = new UserPermission();
        assortmentArticleDeleteAllPermission.setName("assortmentarticle:delete_all");
        assortmentArticleDeleteAllPermission.setDescription("Delete articles on any assortment on organization");
        em.persist(assortmentArticleDeleteAllPermission);

        UserPermission assortmentArticleDeleteOwnPermission = new UserPermission();
        assortmentArticleDeleteOwnPermission.setName("assortmentarticle:delete_own");
        assortmentArticleDeleteOwnPermission.setDescription("Delete articles on assigned assortments on organization");
        em.persist(assortmentArticleDeleteOwnPermission);

        UserPermission countiesViewPermission = new UserPermission();
        countiesViewPermission.setName("counties:view");
        countiesViewPermission.setDescription("View counties for assortments");
        em.persist(countiesViewPermission);

        // media
        UserPermission documentTypeViewPermission = new UserPermission();
        documentTypeViewPermission.setName("documenttype:view");
        documentTypeViewPermission.setDescription("View document types");
        em.persist(documentTypeViewPermission);

        UserPermission mediaViewPermission = new UserPermission();
        mediaViewPermission.setName("media:view");
        mediaViewPermission.setDescription("View media on article or product");
        em.persist(mediaViewPermission);

        UserPermission mediaCreatePermission = new UserPermission();
        mediaCreatePermission.setName("media:create");
        mediaCreatePermission.setDescription("Create media on article or product");
        em.persist(mediaCreatePermission);

        UserPermission mediaDeletePermission = new UserPermission();
        mediaDeletePermission.setName("media:delete");
        mediaDeletePermission.setDescription("Delete media on article or product");
        em.persist(mediaDeletePermission);

        // export/import
        UserPermission productArticleExportViewPermission = new UserPermission();
        productArticleExportViewPermission.setName("productarticleexport:view");
        productArticleExportViewPermission.setDescription("View export for products/articles");
        em.persist(productArticleExportViewPermission);

        UserPermission productArticleImportViewPermission = new UserPermission();
        productArticleImportViewPermission.setName("productarticleimport:view");
        productArticleImportViewPermission.setDescription("Upload import for products/articles");
        em.persist(productArticleImportViewPermission);

        UserPermission pricelistExportViewPermission = new UserPermission();
        pricelistExportViewPermission.setName("agreementpricelistexport:view");
        pricelistExportViewPermission.setDescription("View export for agreement pricelist");
        em.persist(pricelistExportViewPermission);

        UserPermission pricelistImportViewPermission = new UserPermission();
        pricelistImportViewPermission.setName("agreementpricelistimport:view");
        pricelistImportViewPermission.setDescription("Upload import for agreement pricelist");
        em.persist(pricelistImportViewPermission);

        UserPermission generalPricelistPricelistExportViewPermission = new UserPermission();
        generalPricelistPricelistExportViewPermission.setName("generalpricelistpricelistexport:view");
        generalPricelistPricelistExportViewPermission.setDescription("View export for general pricelist pricelist");
        em.persist(generalPricelistPricelistExportViewPermission);

        UserPermission generalPricelistPricelistImportViewPermission = new UserPermission();
        generalPricelistPricelistImportViewPermission.setName("generalpricelistpricelistimport:view");
        generalPricelistPricelistImportViewPermission.setDescription("Upload import for general pricelist pricelist");
        em.persist(generalPricelistPricelistImportViewPermission);

        // export settings (xml)
        UserPermission exportSettingsViewAllPermission = new UserPermission();
        exportSettingsViewAllPermission.setName("exportsettings:view_all");
        exportSettingsViewAllPermission.setDescription("View all export settings (admin)");
        em.persist(exportSettingsViewAllPermission);

        UserPermission exportSettingsCreateAllPermission = new UserPermission();
        exportSettingsCreateAllPermission.setName("exportsettings:create_all");
        exportSettingsCreateAllPermission.setDescription("Create export settings (admin)");
        em.persist(exportSettingsCreateAllPermission);

        UserPermission exportSettingsViewOwnPermission = new UserPermission();
        exportSettingsViewOwnPermission.setName("exportsettings:view_own");
        exportSettingsViewOwnPermission.setDescription("View own export settings");
        em.persist(exportSettingsViewOwnPermission);

        UserPermission exportSettingsUpdateOwnPermission = new UserPermission();
        exportSettingsUpdateOwnPermission.setName("exportsettings:update_own");
        exportSettingsUpdateOwnPermission.setDescription("Update own export settings");
        em.persist(exportSettingsUpdateOwnPermission);

        UserPermission textsViewPermission = new UserPermission();
        textsViewPermission.setName("texts:view");
        textsViewPermission.setDescription("View system texts");
        em.persist(textsViewPermission);

        UserPermission tokenUserViewPermission = new UserPermission();
        tokenUserViewPermission.setName("tokenuser:view");
        tokenUserViewPermission.setDescription("View token users");
        em.persist(tokenUserViewPermission);

        UserPermission tokenUserCreatePermission = new UserPermission();
        tokenUserCreatePermission.setName("tokenuser:create");
        tokenUserCreatePermission.setDescription("Create token user");
        em.persist(tokenUserCreatePermission);

        // BASE USER everyone should have this role
        List<UserPermission> baseUserPermissions = new ArrayList<>();
        baseUserPermissions.add(organizationViewPermission);
        baseUserPermissions.add(businessLevelViewPermission);
        baseUserPermissions.add(userViewPermission);
        baseUserPermissions.add(rolesViewPermission);
        baseUserPermissions.add(userUpdateContactPermission);
        baseUserPermissions.add(profileViewPermission);
        baseUserPermissions.add(categoryViewPermission);
        baseUserPermissions.add(ceViewPermission);
        baseUserPermissions.add(orderUnitViewPermission);
        baseUserPermissions.add(packageUnitViewPermission);
        baseUserPermissions.add(documentTypeViewPermission);
        baseUserPermissions.add(guaranteeUnitViewPermission);
        baseUserPermissions.add(preventiveMaintenanceViewPermission);
        baseUserPermissions.add(productViewPermission);
        baseUserPermissions.add(articleViewPermission);
        baseUserPermissions.add(mediaViewPermission);
        baseUserPermissions.add(textsViewPermission);
        baseUserPermissions.add(generalPricelistPricelistRowViewOwnPermission);
        baseUserPermissions.add(pricelistRowViewExistsPermission);
        UserRole baseUserRole = new UserRole();
        baseUserRole.setName(UserRole.RoleName.Baseuser);
        baseUserRole.setDescription("Grundläggande visning och sök");
        baseUserRole.setPermissions(baseUserPermissions);
        em.persist(baseUserRole);
        userRoles.add(baseUserRole);

        // BASE TOKEN USER for users logging in with tokens, limited permissions
        List<UserPermission> baseTokenUserPermissions = new ArrayList<>();
        baseTokenUserPermissions.add(businessLevelViewPermission);
        baseTokenUserPermissions.add(organizationViewPermission);
        baseTokenUserPermissions.add(ceViewPermission);
        baseTokenUserPermissions.add(orderUnitViewPermission);
        baseTokenUserPermissions.add(packageUnitViewPermission);
        baseTokenUserPermissions.add(documentTypeViewPermission);
        baseTokenUserPermissions.add(guaranteeUnitViewPermission);
        baseTokenUserPermissions.add(preventiveMaintenanceViewPermission);
        baseTokenUserPermissions.add(profileViewPermission);
        baseTokenUserPermissions.add(productViewPermission);
        baseTokenUserPermissions.add(articleViewPermission);
        baseTokenUserPermissions.add(categoryViewPermission);
        baseTokenUserPermissions.add(mediaViewPermission);
        baseTokenUserPermissions.add(textsViewPermission);
        UserRole baseTokenUserRole = new UserRole();
        baseTokenUserRole.setName(UserRole.RoleName.BaseTokenuser);
        baseTokenUserRole.setDescription("Grundläggande visning och sök");
        baseTokenUserRole.setPermissions(baseTokenUserPermissions);
        em.persist(baseTokenUserRole);

        // SUPER ADMIN
        List<UserPermission> superAdminPermissions = new ArrayList<>();
        superAdminPermissions.add(organizationCreatePermission);
        superAdminPermissions.add(organizationUpdatePermission);
        superAdminPermissions.add(organizationDeletePermission);
        superAdminPermissions.add(businessLevelCreatePermission);
        superAdminPermissions.add(businessLevelInactivatePermission);
        superAdminPermissions.add(businessLevelDeletePermission);
        superAdminPermissions.add(userCreatePermission);
        superAdminPermissions.add(userUpdatePermission);
        superAdminPermissions.add(userDeletePermission);
        superAdminPermissions.add(exportSettingsViewAllPermission);
        superAdminPermissions.add(exportSettingsCreateAllPermission);
        superAdminPermissions.add(tokenUserViewPermission);
        superAdminPermissions.add(tokenUserCreatePermission);
        superAdminPermissions.add(assortmentViewAllPermission);
        superAdminPermissions.add(assortmentViewPermission);
        superAdminPermissions.add(assortmentUpdatePermission);
        superAdminPermissions.add(assortmentArticleViewPermission);
        superAdminPermissions.add(countiesViewPermission);
        UserRole superAdminRole = new UserRole();
        superAdminRole.setName(UserRole.RoleName.Superadmin);
        superAdminRole.setDescription("Administratörsroll för tjänsteägare");
        superAdminRole.setPermissions(superAdminPermissions);
        em.persist(superAdminRole);
        userRoles.add(superAdminRole);

        // SUPPLIER ADMIN
        List<UserPermission> supplierAdminPermissions = new ArrayList<>();
        supplierAdminPermissions.add(organizationUpdateContactPermission);
        supplierAdminPermissions.add(userCreateOwnPermission);
        supplierAdminPermissions.add(userUpdateOwnPermission);
        supplierAdminPermissions.add(userDeleteOwnPermission);
        UserRole supplierAdminRole = new UserRole();
        supplierAdminRole.setName(UserRole.RoleName.Supplieradmin);
        supplierAdminRole.setDescription("Hantera användare och organisation (lokal admin)");
        supplierAdminRole.setPermissions(supplierAdminPermissions);
        em.persist(supplierAdminRole);
        userRoles.add(supplierAdminRole);

        // SUPPLIER AGREEMENT/GP VIEWER
        List<UserPermission> supplierAgreementViewerPermissions = new ArrayList<>();
        supplierAgreementViewerPermissions.add(agreementViewOwnPermission);
        supplierAgreementViewerPermissions.add(pricelistViewOwnPermission);
        supplierAgreementViewerPermissions.add(pricelistRowViewOwnPermission);
        supplierAgreementViewerPermissions.add(generalPricelistViewPermission);
        supplierAgreementViewerPermissions.add(generalPricelistPricelistViewOwnPermission);
        supplierAgreementViewerPermissions.add(generalPricelistPricelistRowViewOwnPermission);
        UserRole supplierAgreementViewerRole = new UserRole();
        supplierAgreementViewerRole.setName(UserRole.RoleName.SupplierAgreementViewer);
        supplierAgreementViewerRole.setDescription("Sök inköpsavtal och GP med priser");
        supplierAgreementViewerRole.setPermissions(supplierAgreementViewerPermissions);
        em.persist(supplierAgreementViewerRole);
        userRoles.add(supplierAgreementViewerRole);


        // SUPPLIER AGREEMENT/GP MANAGER
        List<UserPermission> supplierAgreementManagerPermissions = new ArrayList<>();
        supplierAgreementManagerPermissions.add(agreementViewOwnPermission);
        supplierAgreementManagerPermissions.add(pricelistViewOwnPermission);
        supplierAgreementManagerPermissions.add(pricelistExportViewPermission);
        supplierAgreementManagerPermissions.add(pricelistImportViewPermission);
        supplierAgreementManagerPermissions.add(pricelistRowViewOwnPermission);
        supplierAgreementManagerPermissions.add(pricelistRowCreateOwnPermission);
        supplierAgreementManagerPermissions.add(pricelistRowUpdateOwnPermission);
        supplierAgreementManagerPermissions.add(pricelistRowSendForCustomerApprovalPermission);
        supplierAgreementManagerPermissions.add(pricelistRowInactivatePermission);
        supplierAgreementManagerPermissions.add(pricelistRowReopenInactivatedPermission);
        supplierAgreementManagerPermissions.add(generalPricelistViewPermission);
        supplierAgreementManagerPermissions.add(generalPricelistCreateOwnPermission);
        supplierAgreementManagerPermissions.add(generalPricelistUpdateOwnPermission);
        supplierAgreementManagerPermissions.add(generalPricelistPricelistExportViewPermission);
        supplierAgreementManagerPermissions.add(generalPricelistPricelistImportViewPermission);
        supplierAgreementManagerPermissions.add(generalPricelistPricelistViewOwnPermission);
        supplierAgreementManagerPermissions.add(generalPricelistPricelistCreateOwnPermission);
        supplierAgreementManagerPermissions.add(generalPricelistPricelistUpdateOwnPermission);
        supplierAgreementManagerPermissions.add(generalPricelistPricelistRowViewOwnPermission);
        supplierAgreementManagerPermissions.add(generalPricelistPricelistRowCreateOwnPermission);
        supplierAgreementManagerPermissions.add(generalPricelistPricelistRowUpdateOwnPermission);
        supplierAgreementManagerPermissions.add(generalPricelistPricelistRowInactivateOwnPermission);
        UserRole supplierAgreementManagerRole = new UserRole();
        supplierAgreementManagerRole.setName(UserRole.RoleName.SupplierAgreementManager);
        supplierAgreementManagerRole.setDescription("Hantera prislistor och generell prislista (GP)");
        supplierAgreementManagerRole.setPermissions(supplierAgreementManagerPermissions);
        em.persist(supplierAgreementManagerRole);
        userRoles.add(supplierAgreementManagerRole);


        // SUPPLIER HANDLE PRODUCTS AND ARTICLES
        List<UserPermission> supplierProductAndArticlePermissions = new ArrayList<>();
        supplierProductAndArticlePermissions.add(productCreateOwnPermission);
        supplierProductAndArticlePermissions.add(productUpdateOwnPermission);
        supplierProductAndArticlePermissions.add(articleCreateOwnPermission);
        supplierProductAndArticlePermissions.add(articleUpdateOwnPermission);
        supplierProductAndArticlePermissions.add(mediaCreatePermission);
        supplierProductAndArticlePermissions.add(mediaDeletePermission);
        supplierProductAndArticlePermissions.add(productArticleExportViewPermission);
        supplierProductAndArticlePermissions.add(productArticleImportViewPermission);
        UserRole supplierProductAndArticleRole = new UserRole();
        supplierProductAndArticleRole.setName(UserRole.RoleName.SupplierProductAndArticleHandler);
        supplierProductAndArticleRole.setDescription("Hantera produkt och artikel");
        supplierProductAndArticleRole.setPermissions(supplierProductAndArticlePermissions);
        em.persist(supplierProductAndArticleRole);
        userRoles.add(supplierProductAndArticleRole);

        // CUSTOMER ADMIN
        List<UserPermission> customerAdminPermissions = new ArrayList<>();
        customerAdminPermissions.add(organizationUpdateContactPermission);
        customerAdminPermissions.add(userCreateOwnPermission);
        customerAdminPermissions.add(userUpdateOwnPermission);
        customerAdminPermissions.add(userDeleteOwnPermission);
        customerAdminPermissions.add(exportSettingsViewOwnPermission);
        customerAdminPermissions.add(exportSettingsUpdateOwnPermission);
        UserRole customerAdminRole = new UserRole();
        customerAdminRole.setName(UserRole.RoleName.Customeradmin);
        customerAdminRole.setDescription("Hantera användare, organisation och exportfil (lokal admin)");
        customerAdminRole.setPermissions(customerAdminPermissions);
        em.persist(customerAdminRole);
        userRoles.add(customerAdminRole);

        // CUSTOMER AGREEMENT/GP VIEWER
        List<UserPermission> customerAgreementViewerPermissions = new ArrayList<>();
        customerAgreementViewerPermissions.add(agreementViewOwnPermission);
        customerAgreementViewerPermissions.add(pricelistViewOwnPermission);
        customerAgreementViewerPermissions.add(pricelistRowViewOwnPermission);
        customerAgreementViewerPermissions.add(generalPricelistSearchAllPermission);
        customerAgreementViewerPermissions.add(generalPricelistViewAllPermission);
        customerAgreementViewerPermissions.add(generalPricelistPricelistViewAllPermission);
        customerAgreementViewerPermissions.add(generalPricelistPricelistRowViewAllPermission);
        // the below 4 row permissions is required for this role since the role can be added
        // as prielist approver and then must be able to work with rows
        customerAgreementViewerPermissions.add(pricelistRowActivatePermission);
        customerAgreementViewerPermissions.add(pricelistRowDeclinePermission);
        customerAgreementViewerPermissions.add(pricelistRowApproveInactivatePermission);
        customerAgreementViewerPermissions.add(pricelistRowDeclineInactivatePermission);
        UserRole customerAgreementViewerRole = new UserRole();
        customerAgreementViewerRole.setName(UserRole.RoleName.CustomerAgreementViewer);
        customerAgreementViewerRole.setDescription("Sök inköpsavtal och GP med priser");
        customerAgreementViewerRole.setPermissions(customerAgreementViewerPermissions);
        em.persist(customerAgreementViewerRole);
        userRoles.add(customerAgreementViewerRole);

        // CUSTOMER AGREEMENT/GP MANAGER
        List<UserPermission> customerAgreementManagerPermissions = new ArrayList<>();
        customerAgreementManagerPermissions.add(agreementViewOwnPermission);
        customerAgreementManagerPermissions.add(agreementCreateOwnPermission);
        customerAgreementManagerPermissions.add(agreementUpdateOwnPermission);
        customerAgreementManagerPermissions.add(pricelistViewOwnPermission);
        customerAgreementManagerPermissions.add(pricelistCreateOwnPermission);
        customerAgreementManagerPermissions.add(pricelistUpdateOwnPermission);
        customerAgreementManagerPermissions.add(pricelistRowViewOwnPermission);
        customerAgreementManagerPermissions.add(pricelistRowActivatePermission);
        customerAgreementManagerPermissions.add(pricelistRowDeclinePermission);
        customerAgreementManagerPermissions.add(pricelistRowApproveInactivatePermission);
        customerAgreementManagerPermissions.add(pricelistRowDeclineInactivatePermission);
        UserRole customerAgreementManagerRole = new UserRole();
        customerAgreementManagerRole.setName(UserRole.RoleName.CustomerAgreementManager);
        customerAgreementManagerRole.setDescription("Hantera inköpsavtal");
        customerAgreementManagerRole.setPermissions(customerAgreementManagerPermissions);
        em.persist(customerAgreementManagerRole);
        userRoles.add(customerAgreementManagerRole);

        // CUSTOMER ASSORTMENT MANAGER
        List<UserPermission> customerAssortmentManagerPermissions = new ArrayList<>();
        customerAssortmentManagerPermissions.add(assortmentViewPermission);
        customerAssortmentManagerPermissions.add(assortmentCreatePermission);
        customerAssortmentManagerPermissions.add(assortmentUpdatePermission);
        customerAssortmentManagerPermissions.add(assortmentArticleViewPermission);
        customerAssortmentManagerPermissions.add(assortmentArticleCreateAllPermission);
        customerAssortmentManagerPermissions.add(assortmentArticleDeleteAllPermission);
        customerAssortmentManagerPermissions.add(countiesViewPermission);
        UserRole customerAssortmentManagerRole = new UserRole();
        customerAssortmentManagerRole.setName(UserRole.RoleName.CustomerAssortmentManager);
        customerAssortmentManagerRole.setDescription("Hantera utbud");
        customerAssortmentManagerRole.setPermissions(customerAssortmentManagerPermissions);
        em.persist(customerAssortmentManagerRole);
        userRoles.add(customerAssortmentManagerRole);

        // CUSTOMER ASSIGNED ASSORTMENT MANAGER
        List<UserPermission> customerAssignedAssortmentManagerPermissions = new ArrayList<>();
        customerAssignedAssortmentManagerPermissions.add(assortmentViewPermission);
        customerAssignedAssortmentManagerPermissions.add(assortmentArticleViewPermission);
        customerAssignedAssortmentManagerPermissions.add(assortmentArticleCreateOwnPermission);
        customerAssignedAssortmentManagerPermissions.add(assortmentArticleDeleteOwnPermission);
        customerAssignedAssortmentManagerPermissions.add(agreementViewOwnPermission);
        customerAssignedAssortmentManagerPermissions.add(pricelistViewOwnPermission);
        customerAssignedAssortmentManagerPermissions.add(pricelistRowViewOwnPermission);
        customerAssignedAssortmentManagerPermissions.add(countiesViewPermission);
        UserRole customerAssignedAssortmentManagerRole = new UserRole();
        customerAssignedAssortmentManagerRole.setName(UserRole.RoleName.CustomerAssignedAssortmentManager);
        customerAssignedAssortmentManagerRole.setDescription("Hantera tilldelade utbud");
        customerAssignedAssortmentManagerRole.setPermissions(customerAssignedAssortmentManagerPermissions);
        em.persist(customerAssignedAssortmentManagerRole);
        userRoles.add(customerAssignedAssortmentManagerRole);

        return userRoles;
    }

    private UserRole getRoleByName(UserRole.RoleName roleName, List<UserRole> userRoles) {
        for (UserRole userRole : userRoles) {
            if (userRole.getName()
                        .equals(roleName)) {
                return userRole;
            }
        }
        return null;
    }

    private List<CVCEDirective> loadDirectives() {
        log.info("loadDirectives()");
        List<CVCEDirective> directives = new ArrayList<>();
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        try {
            inputStream = this.getClass()
                              .getClassLoader()
                              .getResourceAsStream("directives.csv");
            if (inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
                String nextLine;
                boolean first = true;
                while ((nextLine = bufferedReader.readLine()) != null) {
                    if (first) {
                        first = false;
                        continue;
                    }
                    nextLine = nextLine.trim();
                    if (nextLine != null && !nextLine.isEmpty()) {
                        String[] lineParts = nextLine.split(";");
                        int order = Integer.parseInt(lineParts[0]);
                        String code = lineParts[1];
                        String name = lineParts[2];
                        CVCEDirective directive = new CVCEDirective();
                        directive.setCode(code);
                        directive.setDisplayOrder(order);
                        directive.setName(name);
                        em.persist(directive);
                        directives.add(directive);
                    }
                }
            } else {
                log.info("No directives.csv found");
            }
        } catch (IOException ex) {
            log.info("No directives.csv found", ex);
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException ex) {
                log.error("Failed to close directives.csv", ex);
            }
        }
        return directives;
    }

    private List<CVCEStandard> loadStandards() {
        log.info("loadStandards()");
        List<CVCEStandard> standards = new ArrayList<>();
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        try {
            inputStream = this.getClass()
                              .getClassLoader()
                              .getResourceAsStream("standards.csv");
            if (inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
                String nextLine;
                boolean first = true;
                while ((nextLine = bufferedReader.readLine()) != null) {
                    if (first) {
                        first = false;
                        continue;
                    }
                    nextLine = nextLine.trim();
                    if (nextLine != null && !nextLine.isEmpty()) {
                        String[] lineParts = nextLine.split(";");
                        int order = Integer.parseInt(lineParts[0]);
                        String code = lineParts[1];
                        String name = lineParts[2];
                        CVCEStandard standard = new CVCEStandard();
                        standard.setDisplayOrder(order);
                        standard.setCode(code);
                        standard.setName(name);
                        em.persist(standard);
                        standards.add(standard);
                    }
                }
            } else {
                log.info("No standards.csv found");
            }
        } catch (IOException ex) {
            log.info("No standards.csv found", ex);
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException ex) {
                log.error("Failed to close standards.csv", ex);
            }
        }
        return standards;
    }

    private List<CVOrderUnit> loadOrderUnits() {
        log.info("loadOrderUnits()");
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        List<CVOrderUnit> orderUnits = new ArrayList<>();
        try {
            inputStream = this.getClass()
                              .getClassLoader()
                              .getResourceAsStream("orderunits.csv");
            if (inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
                String nextLine;
                boolean first = true;
                while ((nextLine = bufferedReader.readLine()) != null) {
                    if (first) {
                        first = false;
                        continue;
                    }
                    nextLine = nextLine.trim();
                    if (nextLine != null && !nextLine.isEmpty()) {
                        String[] lineParts = nextLine.split(";");
                        int order = Integer.parseInt(lineParts[0]);
                        String code = lineParts[1];
                        String name = lineParts[2];
                        CVOrderUnit orderUnit = new CVOrderUnit();
                        orderUnit.setDisplayOrder(order);
                        orderUnit.setCode(code);
                        orderUnit.setName(name);
                        em.persist(orderUnit);
                        orderUnits.add(orderUnit);
                    }
                }
            } else {
                log.info("No orderunits.csv found");
            }
        } catch (IOException ex) {
            log.info("No orderunits.csv found", ex);
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException ex) {
                log.error("Failed to close orderunits.csv", ex);
            }
        }

        return orderUnits;
    }

    private List<CVDocumentType> loadDocumentTypes() {
        log.info("loadDocumentTypes()");
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        List<CVDocumentType> documentTypes = new ArrayList<>();
        try {
            inputStream = this.getClass()
                              .getClassLoader()
                              .getResourceAsStream("documenttypes.csv");
            if (inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
                String nextLine;
                boolean first = true;
                while ((nextLine = bufferedReader.readLine()) != null) {
                    if (first) {
                        first = false;
                        continue;
                    }
                    nextLine = nextLine.trim();
                    if (nextLine != null && !nextLine.isEmpty()) {
                        String[] lineParts = nextLine.split(";");
                        int order = Integer.parseInt(lineParts[0]);
                        String code = lineParts[1];
                        String value = lineParts[2];
                        CVDocumentType documentType = new CVDocumentType();
                        documentType.setDisplayOrder(order);
                        documentType.setCode(code);
                        documentType.setValue(value);
                        em.persist(documentType);
                        documentTypes.add(documentType);
                    }
                }
            } else {
                log.info("No documenttypes.csv found");
            }
        } catch (IOException ex) {
            log.info("No documenttypes.csv found", ex);
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException ex) {
                log.error("Failed to close documenttypes.csv", ex);
            }
        }

        return documentTypes;
    }

    private List<CVGuaranteeUnit> loadGuaranteeUnits() {
        log.info("loadGuaranteeUnits()");
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        List<CVGuaranteeUnit> guaranteeUnits = new ArrayList<>();
        try {
            inputStream = this.getClass()
                              .getClassLoader()
                              .getResourceAsStream("guaranteeunits.csv");
            if (inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
                String nextLine;
                boolean first = true;
                while ((nextLine = bufferedReader.readLine()) != null) {
                    if (first) {
                        first = false;
                        continue;
                    }
                    nextLine = nextLine.trim();
                    if (nextLine != null && !nextLine.isEmpty()) {
                        String[] lineParts = nextLine.split(";");
                        int order = Integer.parseInt(lineParts[0]);
                        String code = lineParts[1];
                        String name = lineParts[2];
                        CVGuaranteeUnit guaranteeUnit = new CVGuaranteeUnit();
                        guaranteeUnit.setDisplayOrder(order);
                        guaranteeUnit.setCode(code);
                        guaranteeUnit.setName(name);
                        em.persist(guaranteeUnit);
                        guaranteeUnits.add(guaranteeUnit);
                    }
                }
            } else {
                log.info("No guaranteeunits.csv found");
            }
        } catch (IOException ex) {
            log.info("No guaranteeunits.csv found", ex);
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException ex) {
                log.error("Failed to close guaranteeunits.csv", ex);
            }
        }

        return guaranteeUnits;
    }

    private List<CVPreventiveMaintenance> loadPreventiveMaintenances() {
        log.info("loadPreventiveMaintenances()");
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        List<CVPreventiveMaintenance> preventiveMaintenances = new ArrayList<>();
        try {
            inputStream = this.getClass()
                              .getClassLoader()
                              .getResourceAsStream("preventivemaintenances.csv");
            if (inputStream != null) {
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream, Charset.forName("UTF-8")));
                String nextLine;
                boolean first = true;
                while ((nextLine = bufferedReader.readLine()) != null) {
                    if (first) {
                        first = false;
                        continue;
                    }
                    nextLine = nextLine.trim();
                    if (nextLine != null && !nextLine.isEmpty()) {
                        String[] lineParts = nextLine.split(";");
                        int order = Integer.parseInt(lineParts[0]);
                        String code = lineParts[1];
                        String name = lineParts[2];
                        CVPreventiveMaintenance preventiveMaintenance = new CVPreventiveMaintenance();
                        preventiveMaintenance.setDisplayOrder(order);
                        preventiveMaintenance.setCode(code);
                        preventiveMaintenance.setName(name);
                        em.persist(preventiveMaintenance);
                        preventiveMaintenances.add(preventiveMaintenance);
                    }
                }
            } else {
                log.info("No preventivemaintenances.csv found");
            }
        } catch (IOException ex) {
            log.info("No preventivemaintenances.csv found", ex);
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException ex) {
                log.error("Failed to close preventivemaintenances.csv", ex);
            }
        }

        return preventiveMaintenances;
    }


}

/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.application.services;


import lombok.extern.slf4j.Slf4j;

import javax.annotation.Resource;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import java.util.List;

@Slf4j
@ApplicationScoped
@Path("/sql")
@TransactionManagement(TransactionManagementType.BEAN)
public class ReadOnlySQLService {

    @PersistenceContext(unitName = "local")
    private EntityManager local;

    @PersistenceContext(unitName = "HjmtjPU")
    private EntityManager hjmtj;

    @Resource
    private UserTransaction tx;

    @POST
    @Path("/h2")
    public List<Object> h2(String queryString) throws SystemException, NotSupportedException {
        try {
            tx.begin();
            return local.createNativeQuery(queryString)
                        .getResultList();
        } finally {
            tx.rollback();
        }
    }

    @POST
    @Path("/mysql")
    public List<Object> mysql(String queryString) throws SystemException, NotSupportedException {
        try {
            tx.begin();
            return hjmtj.createNativeQuery(queryString)
                        .getResultList();
        } finally {
            tx.rollback();
        }
    }
}

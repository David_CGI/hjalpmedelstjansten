/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.agreements.gp;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementBase;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PersistenceUnit;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Set;

@PersistenceUnit(unitName = "local")
@Entity
@XmlRootElement(name = "Agreement")
@NamedQueries({
        @NamedQuery(name = GeneralPricelist.FIND_ALL, query = "SELECT DISTINCT A \n" +
                "FROM GeneralPricelist A " +
                "LEFT JOIN FETCH A.valid " +
                "LEFT JOIN FETCH A.warranty " +
                "LEFT JOIN FETCH A.rows r " +
                "LEFT JOIN FETCH r.warranty " +
                "LEFT JOIN FETCH r.status " +
                "LEFT JOIN FETCH r.agreementArticle ra " +
                "LEFT JOIN FETCH ra.article article " +
                "LEFT JOIN FETCH A.seller S " +
                "LEFT  JOIN FETCH S.agreementOrganization o "),
        @NamedQuery(name = GeneralPricelist.FIND_BY_SUPPLIER, query = "SELECT DISTINCT A \n" +
                "FROM GeneralPricelist A " +
                "LEFT JOIN FETCH A.valid " +
                "LEFT JOIN FETCH A.warranty " +
                "LEFT JOIN FETCH A.seller S " +
                "LEFT JOIN FETCH S.agreementOrganization o " +
                "WHERE o.supplier = :supplier "),
        @NamedQuery(name = GeneralPricelist.COUNT_GP, query = "SELECT COUNT(a) FROM GeneralPricelist a"),
        @NamedQuery(name = GeneralPricelist.FIND_BY_TITLE_NUMBER_SELLER, query = "SELECT gp FROM GeneralPricelist gp " +
                "WHERE " +
                "gp.head.sellerAgreementNo = :number AND " +
                "gp.valid.begins = :validFrom AND " +
                "gp.valid.ends = :validEnds AND " +
                "gp.seller.agreementOrganization.supplier.name = :seller ")
})
//@NamedNativeQuery(name=GeneralPricelist.FIND_BY_TITLE_NUMBER_SELLER, query = "SELECT gp.AGREEMENTID\n" +
//        "FROM GENERALPRICELIST gp\n" +
//        "join GENERALPRICELISTPARTY G2 on gp.SELLER_GENERATEDID = G2.GENERATEDID\n" +
//        "JOIN GENERALPRICELISTORGANIZATION G3 on G2.AGREEMENTORGANIZATION_ID = G3.ID\n" +
//        "WHERE G3.NAME  = :seller\n" +
//        "  AND gp.sellerAgreementNo = :number\n" +
//        "  AND gp.VALID_BEGINS = :validFrom\n" +
//        "  AND gp.VALID_ENDS = :validEnds")
@RequiredArgsConstructor
@Getter
@Setter
@Table(indexes = {
        @Index(columnList = "sellerAgreementNo"),
        @Index(columnList = "VALID_BEGINS"),
        @Index(columnList = "VALID_ENDS"),
})
public class GeneralPricelist extends AgreementBase<GeneralPricelistParty, GeneralPricelistRow, GeneralPricelistWarranty> {

    public static final String FIND_BY_TITLE_NUMBER_SELLER = "GeneralPricelist.FIND_BY_TITLE_NUMBER_SELLER";

    public static final String COUNT_GP = "GeneralPricelist.COUNT_GP";

    public static final String FIND_ALL = "GeneralPricelist.FIND_ALL";

    public static final String FIND_BY_SUPPLIER = "GeneralPricelist.FIND_BY_SUPPLIER";

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @XmlElement(name = "Entries")
    private Set<GeneralPricelistRow> rows;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @XmlElement(name = "be6470986d7744568de81ffdc8dd9edd")
    private GeneralPricelistWarranty warranty;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @XmlElement(name = "Seller")
    private GeneralPricelistParty seller;

    private boolean migrate;

    @OneToMany(mappedBy = "generalPricelist")
    private Set<ExportSetting> exportSetting;

}

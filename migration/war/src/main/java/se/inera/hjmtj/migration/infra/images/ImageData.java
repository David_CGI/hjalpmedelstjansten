package se.inera.hjmtj.migration.infra.images;

import lombok.Data;

@Data
public class ImageData {
	private byte[] data;
	private String contentType;
}

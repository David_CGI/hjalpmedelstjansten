package se.inera.hjmtj.migration.domain.model.actors;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.domain.model.Reference;
import se.inera.hjmtj.migration.domain.xml.TrimStringAdapter;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQuery;
import javax.persistence.PersistenceUnit;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@PersistenceUnit(unitName = "local")
@Entity
@Slf4j
@XmlRootElement(name = "Actor")
@NamedQuery(name = Actor.ALL, query = "SELECT distinct a FROM Actor a " +
        "LEFT JOIN FETCH a.organization o " +
        "LEFT JOIN FETCH o.supplier " +
        "LEFT JOIN FETCH o.customer " +
        "LEFT JOIN FETCH a.roles r " +
        "LEFT JOIN FETCH r.actors " +
        "LEFT JOIN FETCH r.organizations ")
@NamedNativeQuery(name = Actor.REPORT, query = "SELECT a.FIRSTNAME                                             AS \"Förnamn\",\n" +
        "       a.LASTNAME                                              AS \"Efternamn\",\n" +
        "       a.USERNAME                                              AS \"Användarnamn\",\n" +
        "       a.EMAIL                                                 AS \"Epost\",\n" +
        "       a.TITLE                                                 AS \"Titel\",\n" +
        "       o.NAME                                                  AS \"Användarens organisation (1.0)\",\n" +
        "       coalesce(c.ORGANIZATIONNAME, S.NAME)                    AS \"Användarens organisation (2.0)\",\n" +
        "       count(distinct AA2.ORGANIZATIONS_ID) > 1                AS \"Har roller i flera organisationer?\",\n" +
        "       group_concat(distinct o2.NAME SEPARATOR ', ')           AS \"Rollorganisation(er)\",\n" +
        "       group_concat(distinct r.ROLENAME SEPARATOR ', ')        AS \"Rollnamn (1.0)\",\n" +
        "       group_concat(distinct r.ROLETYPE SEPARATOR ', ')        AS \"Rolltyp (1.0)\",\n" +
        "       group_concat(trim(AG.SELLERAGREEMENTNO) SEPARATOR ', ') AS \"Godkänner prislistor på avtalsnummer\"\n" +
        "FROM ACTOR a\n" +
        "       left join ACTORROLE_ACTOR AA on a.ID = AA.ACTORS_ID\n" +
        "       left join ACTORORGANIZATION o ON a.ORGANIZATION_ID = o.ID\n" +
        "       left join CUSTOMER c ON o.CUSTOMER_ORGANIZATIONNAME = c.ORGANIZATIONNAME\n" +
        "       left join SUPPLIER S ON o.SUPPLIER_ID = S.ID\n" +
        "\n" +
        "       left join ACTORROLE r on AA.ROLES_ID = r.ID\n" +
        "       left join ACTORROLE_ACTORORGANIZATION AA2 on r.ID = AA2.ACTORROLE_ID\n" +
        "       left join ACTORORGANIZATION o2 ON AA2.ORGANIZATIONS_ID = o2.ID\n" +
        "       LEFT JOIN ACTORPRICELISTAPPROVERACTORROLE q ON q.IDENTIFIER = r.IDENTIFIER\n" +
        "       LEFT JOIN ACTORPRICELISTAPPROVERACTORROLE_ACTORPRICELISTAPPROVERAGREEMENT x ON q.GENERATEDID = x.APPROVERS_GENERATEDID\n" +
        "       LEFT JOIN ACTORPRICELISTAPPROVERAGREEMENT AGx ON x.APPROVES_ID = AGx.ID\n" +
        "       LEFT JOIN AGREEMENT AG ON AGx.IDENTIFIER = AG.IDENTIFIER\n" +
        "group by a.ID\n" +
        "\n")
@Getter
@Setter
public class Actor {

    public static final String REPORT = "ACTOR.REPORT";

    public static final String ALL = "ACTOR.ALL";

    @Id
    private Long id;

    @XmlElement(name = "Timestamp")
    private LocalDateTime timestamp;

    @XmlElement(name = "CreatedTime")
    private LocalDateTime createdTime;

    @Embedded
    @XmlElement(name = "Actor_ContactInformation")
    private ActorContactInformation contactInformation;

    @XmlJavaTypeAdapter(TrimStringAdapter.class)
    @XmlElement(name = "_x0036_661d502ebfb41368ff4d5724f813b09")
    private String title;

    @ManyToOne(fetch = FetchType.LAZY)
    private ActorOrganization organization;

    @XmlJavaTypeAdapter(TrimStringAdapter.class)
    @XmlElement(name = "Username")
    private String userName;

    @XmlJavaTypeAdapter(TrimStringAdapter.class)
    private String firstName;

    @XmlJavaTypeAdapter(TrimStringAdapter.class)
    private String lastName;

    @ManyToMany(mappedBy = "actors", cascade = CascadeType.ALL)
    private Set<ActorRole> roles;

    /**
     * Deep copy constructor.
     */
    public Actor(Actor actor) {
        this();
        this.timestamp = actor.getTimestamp();
        this.createdTime = actor.getCreatedTime();
        this.contactInformation = new ActorContactInformation(actor.getContactInformation());
        this.title = actor.getTitle();
        this.organization = actor.getOrganization();
        this.userName = actor.getUserName();
        this.firstName = actor.getFirstName();
        this.lastName = actor.getLastName();
        this.roles = new HashSet<>();
    }

    public Actor() {
        if(this.roles == null){
            this.roles = new HashSet<>();
        }
    }

    @XmlElement(name = "BelongsToOrganization")
    public void setXmlOrganization(Reference<ActorOrganization> reference) {
        organization = reference.getReference();
    }

    @XmlElement(name = "FullName")
    public void setXmlFullName(String fullName) {
        fullName = fullName.trim();

        List<String> names = Arrays.asList(fullName.split("\\s+"));
        this.firstName = names.size() > 0 ? names.get(0) : "";
        this.lastName = names.size() > 1 ? String.join(" ", names.subList(1, names.size())) : "";
    }

    @XmlAttribute(name = "id")
    @XmlID
    public String getIdAsString() {
        return String.valueOf(id);
    }

    public void setIdAsString(String xmlId) {
        id = Long.parseLong(xmlId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Actor actor = (Actor) o;
        return Objects.equals(id, actor.id);
    }

    @Override
    public String toString() {
        return userName;
    }
}

/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.agreements;

import lombok.Data;
import se.inera.hjmtj.migration.domain.xml.TrimStringAdapter;

import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.PersistenceUnit;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Set;

@PersistenceUnit(unitName = "local")
@Embeddable
@Data
public class AgreementHead implements Serializable {

    @XmlJavaTypeAdapter(TrimStringAdapter.class)
    @XmlElement(name = "SellerAgreementNo")
    private String sellerAgreementNo;

    @XmlElement(name = "VersionList")
    @ElementCollection(fetch = FetchType.EAGER)
    private Set<AgreementVersion> agreementVersionList;

    @XmlJavaTypeAdapter(TrimStringAdapter.class)
    @XmlElement(name = "Title")
    private String title;

}

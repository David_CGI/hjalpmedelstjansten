/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.categories;

import lombok.Data;
import org.w3c.dom.Node;
import se.inera.hjmtj.migration.domain.xml.DepthFirstVisitor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PersistenceUnit;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

/**
 * This is a metadata entity.
 */

@PersistenceUnit(unitName = "local")
@Entity
@Data
public class ProductCategorySpecificPropertyType implements Serializable {

    private static final String DOCUMENTATION = "xs:documentation";

    private static final String HJMTJ_CATEGORY_DESCRIPTOR = "- Används i kategori: Produkt\\Vara\\ISO-klassificerade hjälpmedel (ISO 9999)";

    private static final Pattern NAME_PATTERN = Pattern.compile("^(.*?)\\s-\\s.*\\z");

    private static final Pattern CATEGORY_CODE_PATTERN = Pattern.compile("\\\\(\\d+)");

    @Id
    private String xmlName;

    @Column(nullable = true)
    private String categoryCode;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ValueType valueType;

    @JoinTable(name = "ProductCategorySpecificPropertyType_Matches_CategorySpecificProperty")
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    private Set<CategorySpecificProperty> matchesCategorySpecificProperties;

    public static ProductCategorySpecificPropertyType of(String xmlName) {
        ProductCategorySpecificPropertyType xmlCategorySpecificPropertyType = new ProductCategorySpecificPropertyType();
        xmlCategorySpecificPropertyType.setXmlName(xmlName);
        return xmlCategorySpecificPropertyType;
    }

    public static ProductCategorySpecificPropertyType map(Node item) {
        String nodeName = item.getNodeName();

        if (nodeName.equals(DOCUMENTATION)) {
            String description = item.getTextContent();

            if (description.contains(HJMTJ_CATEGORY_DESCRIPTOR)) {
                Node grandParentNode = item.getParentNode()
                                           .getParentNode();
                String xmlName = DepthFirstVisitor.attribute(grandParentNode, "name");

                if (xmlName != null) {
                    String categoryCode = categoryCode(description);
                    String type = DepthFirstVisitor.attribute(grandParentNode, "type");
                    ProductCategorySpecificPropertyType.ValueType valueType = ProductCategorySpecificPropertyType.ValueType.of(type);
                    String name = name(description);

                    if (name != null) {
                        name = name.replace("  ", " ")
                                   .trim();
                        ProductCategorySpecificPropertyType xmlCategorySpecificPropertyType = new ProductCategorySpecificPropertyType();
                        xmlCategorySpecificPropertyType.setXmlName(xmlName);
                        xmlCategorySpecificPropertyType.setCategoryCode(categoryCode);
                        xmlCategorySpecificPropertyType.setName(name);
                        xmlCategorySpecificPropertyType.setValueType(valueType);
                        return xmlCategorySpecificPropertyType;
                    }
                }
            }
        }

        return null;
    }

    static String categoryCode(String value) {
        Matcher matcher = CATEGORY_CODE_PATTERN.matcher(value);

        int start = -1;
        int end = 0;
        while (matcher.find()) {
            start = matcher.start() + 1;
            end = matcher.end();
        }

        return start > -1 ? value.substring(start, end)
                                 .trim() : null;
    }

    static String name(String value) {
        Matcher matcher = NAME_PATTERN.matcher(value);

        if (matcher.matches()) {
            return matcher.group(1)
                          .trim();
        }

        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductCategorySpecificPropertyType that = (ProductCategorySpecificPropertyType) o;
        return Objects.equals(xmlName, that.xmlName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(xmlName);
    }

    @Override
    public String toString() {
        return "<" + xmlName + "> " + getCategoryCode() + " " + getName() + " (" + getValueType() + ")";
    }

    public enum ValueType {
        REFERENCE("Reference", CategorySpecificProperty.Type.VALUELIST_SINGLE, CategorySpecificProperty.Type.VALUELIST_MULTIPLE),
        BOOLEAN("xs:boolean", CategorySpecificProperty.Type.VALUELIST_SINGLE),
        STRING("xs:string", CategorySpecificProperty.Type.TEXTFIELD),
        DECIMAL("xs:decimal", CategorySpecificProperty.Type.DECIMAL),
        INTERVAL_DECIMAL("IntervalDecimal", CategorySpecificProperty.Type.INTERVAL);

        private final String xmlType;

        private final Set<CategorySpecificProperty.Type> categorySpecificPropertyTypes;

        ValueType(String xmlType, CategorySpecificProperty.Type... categorySpecificPropertyTypes) {
            this.xmlType = xmlType;
            this.categorySpecificPropertyTypes = new HashSet<>(Arrays.asList(categorySpecificPropertyTypes));
        }

        public static ValueType of(String xmlName) {
            ValueType valueType = Arrays.stream(values())
                                        .collect(toMap(ValueType::xmlType, identity()))
                                        .get(xmlName);
            Objects.requireNonNull(valueType, "valueType must be non-null");
            return valueType;
        }

        public Set<CategorySpecificProperty.Type> getCategorySpecificPropertyTypes() {
            return categorySpecificPropertyTypes;
        }

        String xmlType() {
            return xmlType;
        }
    }

}
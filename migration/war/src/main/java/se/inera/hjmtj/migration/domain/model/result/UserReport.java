/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.result;

import lombok.Value;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Accessors(fluent = true)
@Value
public class UserReport {

    private final String firstName;

    private final String lastName;

    private final String username;

    private final String email;

    private final String title;

    private final String organisationName1_0;

    private final String organizationName2_0;

    private final boolean hasRolesInMultipleOrganizations;

    private final List<String> roleOrganizations;

    private final List<String> roleNames1_0;

    private final List<String> roleType1_0;

    private final List<String> approvesPriceListsForAgreementNumbers;

    public UserReport(Object[] array) {
        firstName = (String) array[0];
        lastName = (String) array[1];
        username = (String) array[2];
        email = (String) array[3];
        title = (String) array[4];
        organisationName1_0 = (String) array[5];
        organizationName2_0 = (String) array[6];
        hasRolesInMultipleOrganizations = (boolean) array[7];
        roleOrganizations = array[8] == null ? null : Stream.of(array[8].toString()
                                                                        .split(","))
                                                            .map(String::trim)
                                                            .collect(Collectors.toList());
        roleNames1_0 = array[9] == null ? null : Stream.of(array[9].toString()
                                                                   .split(","))
                                                       .map(String::trim)
                                                       .collect(Collectors.toList());
        roleType1_0 = array[10] == null ? null : Stream.of(array[10].toString()
                                                                    .split(","))
                                                       .map(String::trim)
                                                       .collect(Collectors.toList());
        approvesPriceListsForAgreementNumbers = array[11] == null ? null : Stream.of(array[11].toString()
                                                                                              .split(","))
                                                                                 .map(String::trim)
                                                                                 .collect(Collectors.toList());

    }

}

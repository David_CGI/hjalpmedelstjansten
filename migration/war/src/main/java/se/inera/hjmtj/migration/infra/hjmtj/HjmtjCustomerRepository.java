/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.hjmtj;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.organization.controller.BusinessLevelMapper;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationMapper;
import se.inera.hjalpmedelstjansten.model.api.BusinessLevelAPI;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.entity.BusinessLevel;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjmtj.migration.domain.exceptions.save.BusinessLevelSaveFailed;
import se.inera.hjmtj.migration.domain.exceptions.save.CustomerSaveFailed;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.domain.model.organization.entities.Customer;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Stateless
public class HjmtjCustomerRepository {

    public static final String SVERIGE = "Sverige";

    @PersistenceContext(unitName = "HjmtjPU")
    private EntityManager em;

    @Inject
    private HjmtjOrganizationRepository hjmtjOrganizationRepository;

    @Inject
    private HjmtjCountryRepository hjmtjCountryRepository;

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    public boolean isEmpty() {
        return size() == 0;
    }

    public long size() {
        return ((BigInteger) em.createNativeQuery("SELECT COUNT(*) FROM Organization " +
                "WHERE Organization.organizationType = :type")
                               .setParameter("type", Organization.OrganizationType.CUSTOMER.toString())
                               .setMaxResults(1)
                               .getSingleResult()).longValue();
    }

    public OrganizationAPI save(Customer customer) throws CustomerSaveFailed {
        OrganizationAPI customerOrganizationApi = customer.toApi();

        try {
            customerOrganizationApi.setCountry(hjmtjCountryRepository.all()
                                                                     .get(SVERIGE));
            return hjmtjOrganizationRepository.save(customerOrganizationApi);
        } catch (HjalpmedelstjanstenValidationException e) {
            for (ErrorMessageAPI errorMessageAPI : e.getValidationMessages()) {
                MigrationError migrationError = new MigrationError();
                migrationError.setClassifier(Classifier.field(Classifier.CUSTOMER, errorMessageAPI.getField()));
                migrationError.setMessage(e.getMessage());
                migrationError.setOrganizationName(customer.getOrganizationName());
                migrationErrorRepository.add(migrationError);
            }

            throw new CustomerSaveFailed(customerOrganizationApi, e);
        }
    }

    public BusinessLevelAPI save(BusinessLevelAPI businessLevelAPI) throws BusinessLevelSaveFailed {
        try {
            return hjmtjOrganizationRepository.save(businessLevelAPI);
        } catch (BusinessLevelSaveFailed t) {
            throw new BusinessLevelSaveFailed(businessLevelAPI, t);
        }
    }

    public List<OrganizationAPI> values() {
        List<Object[]> rs = em.createNativeQuery("SELECT o.uniqueId, o.gln FROM Organization o " +
                "WHERE o.organizationType = :type")
                              .setParameter("type", Organization.OrganizationType.CUSTOMER.toString())
                              .getResultList();
        return rs.stream()
                 .map(this::map)
                 .collect(Collectors.toList());
    }

    public OrganizationAPI findByGln(String gln) {
        Object[] result = (Object[]) em.createNativeQuery("SELECT o.uniqueId, o.gln, o.organizationName FROM Organization o " +
                "WHERE o.organizationType = :type AND o.gln = :gln")
                                       .setParameter("type", Organization.OrganizationType.CUSTOMER.toString())
                                       .setParameter("gln", gln)
                                       .getSingleResult();
        return map(result);
    }

    private OrganizationAPI map(Object[] array) {
        OrganizationAPI organizationAPI = new OrganizationAPI();
        organizationAPI.setId(((BigInteger) array[0]).longValue());
        organizationAPI.setGln((String) array[1]);
        organizationAPI.setOrganizationName((String) array[2]);

        return organizationAPI;
    }

    public OrganizationAPI findByName(String name) {
        try {
            Organization organization = em.createQuery("SELECT o FROM Organization o " +
                    "WHERE o.organizationType = :type AND " +
                    "o.organizationName = :name", Organization.class)
                                          .setParameter("type", Organization.OrganizationType.CUSTOMER)
                                          .setParameter("name", name)
                                          .getSingleResult();

            return OrganizationMapper.map(organization, true);
        } catch (NoResultException e) {
            return null;
        }
    }

    public OrganizationAPI find(Customer customer) {
        try {
            Organization organization = em.createQuery("SELECT o FROM Organization o WHERE o.organizationType = :type AND " +
                    "(o.gln = :gln OR " +
                    "o.organizationName = :name) ", Organization.class)
                                          .setParameter("type", Organization.OrganizationType.CUSTOMER)
                                          .setParameter("gln", customer.getGln())
                                          .setParameter("name", customer.getOrganizationName())
                                          .getSingleResult();
            return OrganizationMapper.map(organization, true);
        } catch (NoResultException ex) {
            return null;
        }
    }

    public BusinessLevelAPI findBusinessLevel(Customer customer) {
        try {
            BusinessLevel businessLevel = em.createQuery("SELECT bl FROM BusinessLevel bl " +
                    " WHERE bl.organization.gln = :gln AND bl.name = :name", BusinessLevel.class)
                                            .setParameter("gln", customer.getGln())
                                            .setParameter("name", customer.getOrganizationName())
                                            .getSingleResult();

            return BusinessLevelMapper.map(businessLevel, true);
        } catch (NoResultException e) {
            return null;
        }
    }

}

/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.assortment;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.domain.model.Reference;
import se.inera.hjmtj.migration.domain.model.actors.ActorRole;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PersistenceUnit;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;

@PersistenceUnit(unitName = "local")
@Slf4j
@Entity
@XmlRootElement(name = "Assortment")
@Getter
@Setter
@NamedQueries({
        @NamedQuery(name = Assortment.FIND_ALL_WITH_PRODUCT, query = "SELECT distinct a FROM Assortment a " +
                "LEFT JOIN FETCH a.organization " +
                "LEFT JOIN FETCH a.countyMunicipalities " +
                "LEFT JOIN FETCH a.counties " +
                "LEFT JOIN FETCH a.assortmentOwners "),
        @NamedQuery(name = Assortment.FIND_ALL, query = "SELECT distinct a FROM Assortment a " +
                "LEFT JOIN FETCH a.organization " +
                "LEFT JOIN FETCH a.countyMunicipalities " +
                "LEFT JOIN FETCH a.counties " +
                "LEFT JOIN FETCH a.assortmentOwners "),
        @NamedQuery(name = Assortment.FIND_BY_IDENTIFIER, query = "SELECT DISTINCT r FROM Assortment r WHERE r.identifier = :identifier")
})
public class Assortment {

    public static final String FIND_ALL_WITH_PRODUCT = "Assortment.ALL_WITH_PRODUCTS";

    public static final String FIND_ALL = "Assortment.FIND_ALL";

    public static final String FIND_BY_IDENTIFIER = "Assortment.FIND_BY_IDENTIFIER";

    @Column(nullable = false, unique = true)
    @XmlElement(name = "Identifier")
    private String identifier;

    @Id
    private Long id;

    @XmlElement(name = "AssortmentName")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    private AssortmentOrganization organization;

    @XmlElement(name = "Disabled")
    private boolean disabled;

    @ManyToMany(fetch = FetchType.LAZY, cascade = ALL)
    private Set<AssortmentProduct> products;

    @JoinTable(name = "Assortment_County")
    @ManyToMany(fetch = FetchType.LAZY, cascade = ALL)
    private Set<AssortmentCountyMunicipality> counties;

    @JoinTable(name = "Assortment_Municipality")
    @ManyToMany(fetch = FetchType.LAZY, cascade = ALL)
    private Set<AssortmentCountyMunicipality> countyMunicipalities;

    @ManyToMany(mappedBy = "assortmentsOwned")
    private Set<ActorRole> assortmentOwners;

    public Assortment() {
        this.products = new HashSet<>();
        this.counties = new HashSet<>();
        this.countyMunicipalities = new HashSet<>();
    }

    @XmlElement(name = "BelongsToOrganization")
    public void setXmlBelongsToOrganization(Reference<AssortmentOrganization> reference) {
        organization = reference.getReference();
    }

    @XmlElement(name = "ProdList")
    public void setXmlProdList(Reference<AssortmentProduct> reference) {
        products.add(reference.getReference());
    }

    @XmlElement(name = "CountyList")
    public void setXmlCountyList(Reference<AssortmentCountyMunicipality> reference) {
        counties.add(reference.getReference());
    }

    @XmlElement(name = "MunicipalityList")
    public void setXmlMunicipalityList(Reference<AssortmentCountyMunicipality> reference) {
        countyMunicipalities.add(reference.getReference());
    }

    @XmlAttribute(name = "id")
    @XmlID
    public String getIdAsString() {
        return String.valueOf(id);
    }

    public void setIdAsString(String xmlId) {
        id = Long.parseLong(xmlId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Assortment that = (Assortment) o;
        return Objects.equals(id, that.id);
    }

}

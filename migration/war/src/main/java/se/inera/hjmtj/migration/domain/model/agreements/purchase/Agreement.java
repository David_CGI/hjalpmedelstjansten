/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.agreements.purchase;

import lombok.Getter;
import lombok.Setter;
import se.inera.hjmtj.migration.domain.model.Reference;
import se.inera.hjmtj.migration.domain.model.actorpricelistapprovers.ActorPricelistApproverAgreement;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementBase;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PersistenceUnit;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashSet;
import java.util.Set;

@PersistenceUnit(unitName = "local")
@Entity
@Getter
@Setter
@XmlRootElement(name = "Agreement")

@NamedQueries({
        @NamedQuery(name = Agreement.FIND_BY_IDENTIFIER, query = "SELECT p FROM Agreement p " +
                "LEFT JOIN FETCH p.seller " +
                "LEFT JOIN FETCH p.buyer " +
                "WHERE p.identifier = :identifier"),
        @NamedQuery(name = Agreement.COUNT_AGREEMENT, query = "SELECT COUNT(a) FROM Agreement a"),
        @NamedQuery(name = Agreement.FIND_ALL, query = "SELECT DISTINCT A " +
                "FROM Agreement A " +
                "LEFT JOIN FETCH A.warranty " +
                "LEFT JOIN FETCH A.seller S " +
                "LEFT JOIN FETCH A.buyer B " +
                "LEFT JOIN FETCH S.agreementOrganization so " +
                "LEFT JOIN FETCH so.supplier " +
                "LEFT JOIN FETCH so.customer " +
                "LEFT JOIN FETCH B.agreementOrganization bo " +
                "LEFT JOIN FETCH bo.supplier " +
                "LEFT JOIN FETCH bo.supplier " +
                "LEFT JOIN FETCH a.head h " +
                "LEFT JOIN FETCH h.agreementVersionList " +
                "LEFT JOIN FETCH A.actorPricelistApproverAgreement p " +
                "LEFT JOIN FETCH A.sharedWithOrganizations " +
                "LEFT JOIN FETCH A.rows r " +
                "LEFT JOIN FETCH r.status " +
                "LEFT JOIN FETCH r.warranty "
        ),
        @NamedQuery(name = Agreement.FIND_BY_SUPPLIER, query = "SELECT DISTINCT A " +
                "FROM Agreement A " +
                "LEFT JOIN FETCH A.warranty " +
                "LEFT JOIN FETCH A.seller S " +
                "LEFT JOIN FETCH A.buyer B " +
                "LEFT JOIN FETCH S.agreementOrganization so " +
                "LEFT JOIN FETCH so.supplier " +
                "LEFT JOIN FETCH so.customer " +
                "LEFT JOIN FETCH B.agreementOrganization bo " +
                "LEFT JOIN FETCH bo.supplier " +
                "LEFT JOIN FETCH bo.supplier " +
                "LEFT JOIN FETCH a.head h " +
                "LEFT JOIN FETCH h.agreementVersionList " +
                "LEFT JOIN FETCH A.actorPricelistApproverAgreement p " +
                "LEFT JOIN FETCH A.sharedWithOrganizations " +
                "WHERE so.supplier = :supplier"
        )
})
@Table(indexes = {
        @Index(columnList = "identifier"),
})
public class Agreement extends AgreementBase<AgreementParty, AgreementRow, AgreementWarranty> {

    public static final String COUNT_AGREEMENT = "Agreement.COUNT_AGREEMENT";

    public static final String FIND_BY_IDENTIFIER = "Agreement.FIND_BY_IDENTIFIER";

    public static final String FIND_ALL = "Agreement.FIND_ALL";

    public static final String FIND_BY_SUPPLIER = "Agreement.FIND_BY_SUPPLIER";

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @XmlElement(name = "Buyer")
    private AgreementParty buyer;

    @ManyToMany
    private Set<AgreementOrganization> sharedWithOrganizations;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @XmlElement(name = "Entries")
    private Set<AgreementRow> rows;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @XmlElement(name = "be6470986d7744568de81ffdc8dd9edd")
    private AgreementWarranty warranty;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @XmlElement(name = "Seller")
    private AgreementParty seller;

    public Agreement() {
        this.sharedWithOrganizations = new HashSet<>();
    }

    @XmlElement(name = "ReadableByOrganizations")
    public void setXmlReadableByOrganizations(Reference<AgreementOrganization> reference) {
        sharedWithOrganizations.add(reference.getReference());
    }

    @OneToOne(mappedBy = "agreement", optional = false)
    private ActorPricelistApproverAgreement actorPricelistApproverAgreement;

}

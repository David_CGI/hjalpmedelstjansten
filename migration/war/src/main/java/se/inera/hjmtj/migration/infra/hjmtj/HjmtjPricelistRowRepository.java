/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.hjmtj;


import lombok.extern.slf4j.Slf4j;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementPricelistRowMapper;
import se.inera.hjalpmedelstjansten.business.product.controller.CVPreventiveMaintenanceController;
import se.inera.hjalpmedelstjansten.business.product.controller.GuaranteeUnitController;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelist;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelistRow;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;
import se.inera.hjmtj.migration.domain.DateTimes;
import se.inera.hjmtj.migration.domain.exceptions.save.PriceListRowArticleNotFound;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.Date;

import static java.util.concurrent.TimeUnit.HOURS;

@Slf4j
@Stateless
@TransactionTimeout(unit = HOURS, value = 5)
public class HjmtjPricelistRowRepository {

    @PersistenceContext(unitName = "HjmtjPU")
    private EntityManager em;

    @Inject
    private CVPreventiveMaintenanceController cVPreventiveMaintenanceController;

    @Inject
    private GuaranteeUnitController guaranteeUnitController;

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    public SaveResult<Long> save(AgreementPricelistAPI pricelistAPI, AgreementPricelistRowAPI pricelistRowAPI, LocalDateTime created) throws PriceListRowArticleNotFound {
        AgreementPricelist pricelist = em.getReference(AgreementPricelist.class, pricelistAPI.getId());
        AgreementPricelistRow pricelistRow = AgreementPricelistRowMapper.map(pricelistRowAPI, new Date(pricelistAPI.getValidFrom()));
        pricelistRow.setPricelist(pricelist);

        // set timestamps
        Long createdMillis = DateTimes.toEpochMillis(created);
        pricelistRow.setCreated(createdMillis != null ? new Date(createdMillis) : null);
        pricelistRow.setLastUpdated(createdMillis != null ? new Date(createdMillis) : null);

        // article
        ArticleAPI articleAPI = pricelistRowAPI.getArticle();
        Article article = em.find(Article.class, articleAPI
                .getId());
        if (article == null) {
            // This is a cascade failure
            throw new PriceListRowArticleNotFound(articleAPI);
        }
        pricelistRow.setArticle(article);

        Article.Type articleType = pricelistRow.getArticle()
                                               .getBasedOnProduct() == null ? pricelistRow.getArticle()
                                                                                          .getCategory()
                                                                                          .getArticleType() : pricelistRow.getArticle()
                                                                                                                          .getBasedOnProduct()
                                                                                                                          .getCategory()
                                                                                                                          .getArticleType();
        AgreementPricelistRowMapper.mapUpdatableFields(pricelistRowAPI, pricelistRow, articleType, true);
        setOverrideWarrantyValidFrom(pricelistRowAPI, pricelistRow, articleType, true);

        // set overridden warranty quantity unit (if any)
        setOverriddenWarrantyQuantityUnit(pricelistRow, pricelistRowAPI, true);

        // set approved
        pricelistRow.setStatus(AgreementPricelistRow.Status.valueOf(pricelistRowAPI.getStatus()));

        em.persist(pricelistRow);

        return SaveResult.of(pricelistRow.getUniqueId(), pricelistRowAPI);
    }

    private void setOverrideWarrantyValidFrom(AgreementPricelistRowAPI pricelistRowAPI, AgreementPricelistRow pricelistRow, Article.Type articleType, boolean isCreate) {
        if (isCreate && pricelistRowAPI.getWarrantyValidFrom() == null) {
            // do nothing, default to inherit
        } else {
            if (pricelistRow.isWarrantyValidFromOverridden()) {
                CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(pricelistRowAPI.getWarrantyValidFrom()
                                                                                                                            .getCode());
                if (preventiveMaintenance != null) {
                    pricelistRow.setWarrantyValidFrom(preventiveMaintenance);
                }
            } else {
                boolean overridden = false;
                if (pricelistRowAPI.getWarrantyValidFrom() != null) {
                    CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(pricelistRowAPI.getWarrantyValidFrom()
                                                                                                                                .getCode());
                    if (articleType == Article.Type.H) {
                        if (pricelistRow.getPricelist()
                                        .getAgreement()
                                        .getWarrantyValidFromH() == null) {
                            overridden = true;
                        } else {
                            if (!preventiveMaintenance.getCode()
                                                      .equals(pricelistRow.getPricelist()
                                                                          .getAgreement()
                                                                          .getWarrantyValidFromH()
                                                                          .getCode())) {
                                overridden = true;
                            }
                        }
                    } else if (articleType == Article.Type.I) {
                        if (pricelistRow.getPricelist()
                                        .getAgreement()
                                        .getWarrantyValidFromI() == null) {
                            overridden = true;
                        } else {
                            if (!preventiveMaintenance.getCode()
                                                      .equals(pricelistRow.getPricelist()
                                                                          .getAgreement()
                                                                          .getWarrantyValidFromI()
                                                                          .getCode())) {
                                overridden = true;
                            }
                        }
                    } else if (articleType == Article.Type.R) {
                        if (pricelistRow.getPricelist()
                                        .getAgreement()
                                        .getWarrantyValidFromR() == null) {
                            overridden = true;
                        } else {
                            if (!preventiveMaintenance.getCode()
                                                      .equals(pricelistRow.getPricelist()
                                                                          .getAgreement()
                                                                          .getWarrantyValidFromR()
                                                                          .getCode())) {
                                overridden = true;
                            }
                        }
                    } else if (articleType == Article.Type.T) {
                        if (pricelistRow.getPricelist()
                                        .getAgreement()
                                        .getWarrantyValidFromT() == null) {
                            overridden = true;
                        } else {
                            if (!preventiveMaintenance.getCode()
                                                      .equals(pricelistRow.getPricelist()
                                                                          .getAgreement()
                                                                          .getWarrantyValidFromT()
                                                                          .getCode())) {
                                overridden = true;
                            }
                        }
                    } else if (articleType == Article.Type.Tj) {
                        if (pricelistRow.getPricelist()
                                        .getAgreement()
                                        .getWarrantyValidFromTJ() == null) {
                            overridden = true;
                        } else {
                            if (!preventiveMaintenance.getCode()
                                                      .equals(pricelistRow.getPricelist()
                                                                          .getAgreement()
                                                                          .getWarrantyValidFromTJ()
                                                                          .getCode())) {
                                overridden = true;
                            }
                        }
                    }
                    if (overridden) {
                        pricelistRow.setWarrantyValidFrom(preventiveMaintenance);
                        pricelistRow.setWarrantyValidFromOverridden(true);
                    }
                } else {
                    if (articleType == Article.Type.H && pricelistRow.getPricelist()
                                                                     .getAgreement()
                                                                     .getWarrantyValidFromH() != null) {
                        overridden = true;
                    } else if (articleType == Article.Type.I && pricelistRow.getPricelist()
                                                                            .getAgreement()
                                                                            .getWarrantyValidFromI() != null) {
                        overridden = true;
                    } else if (articleType == Article.Type.R && pricelistRow.getPricelist()
                                                                            .getAgreement()
                                                                            .getWarrantyValidFromR() != null) {
                        overridden = true;
                    } else if (articleType == Article.Type.T && pricelistRow.getPricelist()
                                                                            .getAgreement()
                                                                            .getWarrantyValidFromT() != null) {
                        overridden = true;
                    } else if (articleType == Article.Type.Tj && pricelistRow.getPricelist()
                                                                             .getAgreement()
                                                                             .getWarrantyValidFromTJ() != null) {
                        overridden = true;
                    }
                    if (overridden) {
                        pricelistRow.setWarrantyValidFrom(null);
                        pricelistRow.setWarrantyValidFromOverridden(true);
                    }
                }
            }
        }
    }

    private void setOverriddenWarrantyQuantityUnit(AgreementPricelistRow pricelistRow, AgreementPricelistRowAPI pricelistRowAPI, boolean isCreate) {
        if (isCreate && pricelistRowAPI.getWarrantyQuantityUnit() == null) {
            // do nothing, default to inherit
        } else {
            if (pricelistRow.isWarrantyQuantityUnitOverridden()) {
                if (pricelistRowAPI.getWarrantyQuantityUnit() != null) {
                    CVGuaranteeUnit unit = guaranteeUnitController.getGuaranteeUnit(pricelistRowAPI.getWarrantyQuantityUnit()
                                                                                                   .getId());
                    if (unit != null) {
                        pricelistRow.setWarrantyQuantityUnit(unit);
                    }
                } else {
                    pricelistRow.setWarrantyQuantityUnit(null);
                }
            } else {
                Article article = pricelistRow.getArticle();
                Article.Type articleType = article.getBasedOnProduct() == null ? article.getCategory()
                                                                                        .getArticleType() : article.getBasedOnProduct()
                                                                                                                   .getCategory()
                                                                                                                   .getArticleType();
                boolean overridden = false;
                if (pricelistRowAPI.getWarrantyQuantityUnit() == null || pricelistRowAPI.getWarrantyQuantityUnit()
                                                                                        .getId() == null) {
                    // no unit sent in API, check whether it is different from agreement
                    if (articleType == Article.Type.H && pricelistRow.getPricelist()
                                                                     .getAgreement()
                                                                     .getWarrantyQuantityHUnit() != null) {
                        overridden = true;
                    } else if (articleType == Article.Type.I && pricelistRow.getPricelist()
                                                                            .getAgreement()
                                                                            .getWarrantyQuantityIUnit() != null) {
                        overridden = true;
                    } else if (articleType == Article.Type.R && pricelistRow.getPricelist()
                                                                            .getAgreement()
                                                                            .getWarrantyQuantityRUnit() != null) {
                        overridden = true;
                    } else if (articleType == Article.Type.T && pricelistRow.getPricelist()
                                                                            .getAgreement()
                                                                            .getWarrantyQuantityTUnit() != null) {
                        overridden = true;
                    } else if (articleType == Article.Type.Tj && pricelistRow.getPricelist()
                                                                             .getAgreement()
                                                                             .getWarrantyQuantityTJUnit() != null) {
                        overridden = true;
                    }
                    if (overridden) {
                        pricelistRow.setWarrantyQuantityUnit(null);
                        pricelistRow.setWarrantyQuantityUnitOverridden(true);
                    }
                } else {
                    // unit sent in API
                    if (articleType == Article.Type.H) {
                        if (pricelistRow.getPricelist()
                                        .getAgreement()
                                        .getWarrantyQuantityHUnit() == null ||
                                !pricelistRow.getPricelist()
                                             .getAgreement()
                                             .getWarrantyQuantityHUnit()
                                             .getUniqueId()
                                             .equals(pricelistRowAPI.getWarrantyQuantityUnit()
                                                                    .getId())) {
                            overridden = true;
                        }
                    } else if (articleType == Article.Type.R) {
                        if (pricelistRow.getPricelist()
                                        .getAgreement()
                                        .getWarrantyQuantityRUnit() == null ||
                                !pricelistRow.getPricelist()
                                             .getAgreement()
                                             .getWarrantyQuantityRUnit()
                                             .getUniqueId()
                                             .equals(pricelistRowAPI.getWarrantyQuantityUnit()
                                                                    .getId())) {
                            overridden = true;
                        }
                    } else if (articleType == Article.Type.I) {
                        if (pricelistRow.getPricelist()
                                        .getAgreement()
                                        .getWarrantyQuantityIUnit() == null ||
                                !pricelistRow.getPricelist()
                                             .getAgreement()
                                             .getWarrantyQuantityIUnit()
                                             .getUniqueId()
                                             .equals(pricelistRowAPI.getWarrantyQuantityUnit()
                                                                    .getId())) {
                            overridden = true;
                        }
                    } else if (articleType == Article.Type.T) {
                        if (pricelistRow.getPricelist()
                                        .getAgreement()
                                        .getWarrantyQuantityTUnit() == null ||
                                !pricelistRow.getPricelist()
                                             .getAgreement()
                                             .getWarrantyQuantityTUnit()
                                             .getUniqueId()
                                             .equals(pricelistRowAPI.getWarrantyQuantityUnit()
                                                                    .getId())) {
                            overridden = true;
                        }
                    } else if (articleType == Article.Type.Tj) {
                        if (pricelistRow.getPricelist()
                                        .getAgreement()
                                        .getWarrantyQuantityTJUnit() == null ||
                                !pricelistRow.getPricelist()
                                             .getAgreement()
                                             .getWarrantyQuantityTJUnit()
                                             .getUniqueId()
                                             .equals(pricelistRowAPI.getWarrantyQuantityUnit()
                                                                    .getId())) {
                            overridden = true;
                        }
                    }
                    if (overridden) {
                        CVGuaranteeUnit unit = guaranteeUnitController.getGuaranteeUnit(pricelistRowAPI.getWarrantyQuantityUnit()
                                                                                                       .getId());
                        if (unit != null) {
                            pricelistRow.setWarrantyQuantityUnit(unit);
                            pricelistRow.setWarrantyQuantityUnitOverridden(true);
                        }
                    }
                }
            }
        }
    }
}

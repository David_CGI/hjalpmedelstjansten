/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.application.services;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjmtj.migration.domain.csv.Utf8Csv;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MessageTemplate;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.domain.model.organization.api.MigratedOrganizationStatistics;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.domain.model.result.AssistiveProductMigrationResultRepository;
import se.inera.hjmtj.migration.domain.model.result.AssistiveProductReportRepository;
import se.inera.hjmtj.migration.domain.model.result.GeneralReport;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjOrganizationRepository;
import se.inera.hjmtj.migration.infra.organizations.SupplierRepository;
import se.inera.hjmtj.migration.infra.products.ProductRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

@Slf4j
@ApplicationScoped
@Path("/")
public class ReportsService implements ReportServiceBase {

    @Inject
    private SupplierRepository supplierRepository;

    @Inject
    private HjmtjOrganizationRepository hjmtjOrganizationRepository;

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    @Context
    private HttpServletRequest request;

    @Inject
    private AssistiveProductReportRepository assistiveProductReportRepository;

    @Inject
    private AssistiveProductMigrationResultRepository assistiveProductMigrationResultRepository;

    @GET
    @Path("/")
    @Produces(MediaType.TEXT_HTML)
    public String index() {
        StringBuilder sb = new StringBuilder();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/index.html")))) {
            while (br.ready()) {
                sb.append(br.readLine());
                sb.append("\n");
            }
        } catch (IOException e) {
            log.error("{}", e);
        }

        return sb.toString();
    }

    @GET
    @Path("/expired")
    @Produces(MediaType.TEXT_PLAIN)
    public String expired() {
        return generateReport("expired", this::buildExpiredReport);
    }

    private String buildExpiredReport() {
        GeneralReport expiredReportH2 = assistiveProductReportRepository.expired();

        return String.format("%s ISO-9999 hjälpmedel\n" +
                        "%s avtal\n" +
                        "%s avtalsrader\n" +
                        "%s GP\n" +
                        "%s GP rader",
                expiredReportH2.getIso9999(),
                expiredReportH2.getAgreements(),
                expiredReportH2.getAgreementRows(),
                expiredReportH2.getGp(),
                expiredReportH2.getGpRows());
    }

    @GET
    @Path("/stats")
    @Produces(MediaType.TEXT_PLAIN)
    public String result() {
        return generateReport("stats", this::buildResultReport);
    }

    public String buildResultReport() {
        GeneralReport generalReportH2 = assistiveProductReportRepository.quickStats();
        GeneralReport generalReportMysql = assistiveProductMigrationResultRepository.quickStats();

        DecimalFormat decimalFormat = new DecimalFormat("#.##");

        String iso9999 = decimalFormat.format(((double) generalReportMysql.getIso9999() / (double) generalReportH2.getIso9999()) * 100);
        String agreements = decimalFormat.format(((double) generalReportMysql.getAgreements() / (double) generalReportH2.getAgreements()) * 100);
        String agreementRows = decimalFormat.format(((double) generalReportMysql.getAgreementRows() / (double) generalReportH2.getAgreementRows()) * 100);
        String gp = decimalFormat.format(((double) generalReportMysql.getGp() / (double) generalReportH2.getGp()) * 100);
        String gpRows = decimalFormat.format(((double) generalReportMysql.getGpRows() / (double) generalReportH2.getGpRows()) * 100);
        String assortments = decimalFormat.format(((double) generalReportMysql.getAssortments() / (double) generalReportH2.getAssortments()) * 100);
        String assortmentsProducts = decimalFormat.format(((double) generalReportMysql.getAssortmentProducts() / (double) generalReportH2.getAssortmentProducts()) * 100);
        String users = decimalFormat.format(((double) generalReportMysql.getUsers() / (double) generalReportH2.getUsers()) * 100);
        String suppliers = decimalFormat.format(((double) generalReportMysql.getSuppliers() / (double) generalReportH2.getSuppliers()) * 100);
        String customers = decimalFormat.format(((double) generalReportMysql.getCustomers() / (double) generalReportH2.getCustomers()) * 100);
        String businessLevels = decimalFormat.format(((double) generalReportMysql.getBusinessLevels() / (double) generalReportH2.getBusinessLevels()) * 100);

        return String.format("Status på migreringen:\n" +
                        "%s %% av alla ISO-9999 hjälpmedel (%s av %s)\n" +
                        "%s %% av alla avtal (%s av %s) \n" +
                        "%s %% av alla avtalsrader (%s av %s)\n" +
                        "%s %% av alla GP (%s av %s)\n" +
                        "%s %% av alla GP rader (%s av %s)\n" +
                        "%s %% av alla utbud (%s av %s)\n" +
                        "%s %% av alla artiklar på utbud (%s av %s)\n" + //
                        "%s %% av alla användare (%s av %s)\n" + //
                        "%s %% av alla leverantörer (%s av %s)\n" +  //
                        "%s %% av alla kunder (%s av %s)\n" +
                        "%s %% av alla affärsområden (%s av %s)\n",  //
                iso9999, generalReportMysql.getIso9999(), generalReportH2.getIso9999(), //
                agreements, generalReportMysql.getAgreements(), generalReportH2.getAgreements(), //
                agreementRows, generalReportMysql.getAgreementRows(), generalReportH2.getAgreementRows(), //
                gp, generalReportMysql.getGp(), generalReportH2.getGp(), //
                gpRows, generalReportMysql.getGpRows(), generalReportH2.getGpRows(), //
                assortments, generalReportMysql.getAssortments(), generalReportH2.getAssortments(), //
                assortmentsProducts, generalReportMysql.getAssortmentProducts(), generalReportH2.getAssortmentProducts(), //
                users, generalReportMysql.getUsers(), generalReportH2.getUsers(),
                suppliers, generalReportMysql.getSuppliers(), generalReportH2.getSuppliers(),
                customers, generalReportMysql.getCustomers(), generalReportH2.getCustomers(),
                businessLevels, generalReportMysql.getBusinessLevels(), generalReportH2.getBusinessLevels());
    }

    @GET
    @Path("/progress.csv")
    @Produces("text/csv;charset=utf-8")
    public byte[] progressCsv() {
        return generateReport("progress", this::buildProgressReport);
    }

    private byte[] buildProgressReport() {
        StringBuilder sb = new StringBuilder();

        Map<String, Supplier> nameToOrg = supplierRepository.suppliers()
                                                            .stream()
                                                            .collect(toMap(Supplier::getName, identity()));

        sb.append("Id;Namn;Total;P;Pt;Ah;At;Ar;Atj;Ai;Inköpsavtal;Prislistor;Prislisterader\n");

        List<MigratedOrganizationStatistics> statistics = hjmtjOrganizationRepository.statistics();

        int ph = 0, pt = 0;
        int ah = 0, at = 0, ar = 0, atj = 0, ai = 0;
        long agreements = 0;
        long priceLists = 0;
        long priceListRows = 0;

        for (MigratedOrganizationStatistics organization : statistics) {
            ph += organization.productMap()
                              .get(Article.Type.H);
            pt += organization.productMap()
                              .get(Article.Type.T);

            ah += organization.articleMap()
                              .get(Article.Type.H);
            at += organization.articleMap()
                              .get(Article.Type.T);
            ar += organization.articleMap()
                              .get(Article.Type.R);
            atj += organization.articleMap()
                               .get(Article.Type.Tj);
            ai += organization.articleMap()
                              .get(Article.Type.I);

            agreements += organization.agreementRows();
            priceLists += organization.priceLists();
            priceListRows += organization.agreementRows();

        }
        int total = ph + pt + ah + at + ar + atj + ai;
        sb.append("0;");
        sb.append("Total");
        sb.append(";"); // user
        sb.append(total)
          .append(";"); //total
        sb.append(ph)
          .append(";");
        sb.append(pt)
          .append(";");
        sb.append(ah)
          .append(";");
        sb.append(at)
          .append(";");
        sb.append(ar)
          .append(";");
        sb.append(atj)
          .append(";");
        sb.append(ai)
          .append(";");
        sb.append(agreements)
          .append(";");
        sb.append(priceLists)
          .append(";");
        sb.append(priceListRows)
          .append(";");
        sb.append("\n");

        int index = 1;
        for (MigratedOrganizationStatistics organization : statistics) {
            Supplier supplier = nameToOrg.get(organization.organizationName());
            sb.append(index++);
            sb.append(";");

            sb.append(organization.organizationName()
                                  .replace(",", ""));
            sb.append(";");

            sb.append(organization.total());
            sb.append(";");

            // produkter
            sb.append(organization.productMap()
                                  .get(Article.Type.H));
            sb.append(";");

            sb.append(organization.productMap()
                                  .get(Article.Type.T));
            sb.append(";");

            // artiklar
            sb.append(organization.articleMap()
                                  .get(Article.Type.H));
            sb.append(";");

            sb.append(organization.articleMap()
                                  .get(Article.Type.T));
            sb.append(";");

            sb.append(organization.articleMap()
                                  .get(Article.Type.R));
            sb.append(";");

            sb.append(organization.articleMap()
                                  .get(Article.Type.Tj));
            sb.append(";");

            sb.append(organization.articleMap()
                                  .get(Article.Type.I));
            sb.append(";");

            sb.append(organization.agreements());
            sb.append(";");

            sb.append(organization.priceLists());
            sb.append(";");

            sb.append(organization.agreementRows());
            sb.append(";");

            sb.append("\n");
        }

        return new Utf8Csv(sb.toString()).toBytes();
    }

    @GET
    @Path("/errors.csv")
    @Produces("application/vnd.ms-excel;charset=utf-8")
    public byte[] errors() {
        return buildErrorsReport();
    }

    @Inject
    private ProductRepository productRepository;

    public byte[] buildErrorsReport() {
        StringBuilder sb = new StringBuilder();
        sb.append("Fel-ID;Orsak (Fel-ID);Organisation;Nivå;Fel;Fält;Felkod;Katalogunikt nummer;Artikel- eller produktnummer;Utbudsnamn;Avtalsnummer;Prislistenummer;Användarnamn;Beskrivning\n");

        List<MigrationError> values = migrationErrorRepository.values()
                                                              .stream()
                                                              .filter(Objects::nonNull)
                                                              .sorted()
                                                              .collect(Collectors.toList());

        Map<Long, String> catalogueUniqueNumberToSupplierNumbers = productRepository.catalogueUniqueNumberToSupplierNumbers();

        for (MigrationError migrationError : values) {
            sb.append(migrationError.getId());
            sb.append(";");

            sb.append(migrationError.getCause() != null ? migrationError.getCause()
                                                                        .getId() : "");
            sb.append(";");

            sb.append(migrationError.getOrganizationName() != null ? migrationError.getOrganizationName() : "<Alla>");
            sb.append(";");

            MigrationError.Severity severity = migrationError.getSeverity();
            sb.append(severity
                    .toString());
            sb.append(";");

            Classifier classifier = migrationError.getClassifier();
            sb.append(classifier != null && classifier.parent() != null ? classifier.parent() : classifier);
            sb.append(";");

            sb.append(classifier);
            sb.append(";");

            MessageTemplate messageTemplate = migrationError.getMessageTemplate();
            sb.append(messageTemplate != null ? messageTemplate.ordinal() : "-");
            sb.append(";");

            sb.append(migrationError.getCatalogueUniqueNumber() != null ? migrationError.getCatalogueUniqueNumber() : "");
            sb.append(";");

            sb.append(migrationError.getCatalogueUniqueNumber() != null ? catalogueUniqueNumberToSupplierNumbers.getOrDefault(migrationError.getCatalogueUniqueNumber(),"") : "");
            sb.append(";");

            sb.append(migrationError.getAssortmentName() != null ? migrationError.getAssortmentName() : "");
            sb.append(";");

            sb.append(migrationError.getAgreementNumber() != null ? migrationError.getAgreementNumber() : "");
            sb.append(";");

            sb.append(migrationError.getPricelistNumber() != null ? migrationError.getPricelistNumber() : "");
            sb.append(";");

            sb.append(migrationError.getUserName() != null ? migrationError.getUserName() : "");
            sb.append(";");

            sb.append(migrationError.getMessage());

            sb.append("\n");
        }

        return new Utf8Csv(sb.toString()).toBytes();
    }

}

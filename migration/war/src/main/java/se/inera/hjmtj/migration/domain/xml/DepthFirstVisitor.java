/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.xml;

import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * W3C DOM Depth-first visitor.
 */
@Slf4j
@Value
public final class DepthFirstVisitor {

    private final Document document;

    public DepthFirstVisitor(String path) {
        DocumentBuilderFactory factory =
                DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            this.document = builder.parse(getClass().getResourceAsStream(path));
            log.debug("Done parsing XML {}", path);
        } catch (SAXException | IOException | ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
    }

    public static String attribute(Node item, String attributeName) {
        if (item.getAttributes() != null) {
            Node name = item.getAttributes()
                            .getNamedItem(attributeName);
            if (name != null) {
                return name.getTextContent();
            }
        }

        return null;
    }

    private static <T extends Node> List<Node> children(T node) {
        List<Node> nodes = new LinkedList<>();
        NodeList childNodes = node.getChildNodes();

        for (int j = 0; j < childNodes.getLength(); j++) {
            nodes.add(childNodes.item(j));
        }

        return nodes;
    }

    public <R> Set<R> visit(Function<Node, R> function) {
        return visit(function, document);
    }

    private <R> Set<R> visit(Function<Node, R> function, Node node) {
        R result = function.apply(node);

        return result != null ? new HashSet<>(Collections.singletonList(result)) : children(node).stream()
                                                                                                 .map(visitUsing(function))
                                                                                                 .flatMap(Collection::stream)
                                                                                                 .filter(Objects::nonNull)
                                                                                                 .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    private <R> Function<Node, Set<R>> visitUsing(Function<Node, R> function) {
        return child -> visit(function, child);
    }


}

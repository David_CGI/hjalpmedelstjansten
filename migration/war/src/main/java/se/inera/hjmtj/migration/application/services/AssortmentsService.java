/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.application.services;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.domain.MigrationLock;
import se.inera.hjmtj.migration.domain.csv.Utf8Csv;
import se.inera.hjmtj.migration.domain.model.assortment.Assortment;
import se.inera.hjmtj.migration.domain.model.assortment.AssortmentAPIWithArticles;
import se.inera.hjmtj.migration.domain.model.assortment.AssortmentCountyMunicipality;
import se.inera.hjmtj.migration.domain.model.assortment.builders.AssortmentAPIFactory;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.domain.model.organization.entities.Customer;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.infra.assortments.AssortmentsRepository;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjAssortmentRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.ServiceUnavailableException;
import javax.ws.rs.core.Context;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

@Slf4j
@ApplicationScoped
@Path("/assortments")
public class AssortmentsService implements ReportServiceBase {

    @Inject
    private HjmtjAssortmentRepository hjmtjAssortmentRepository;

    @Inject
    private AssortmentsRepository assortmentsRepository;

    @Context
    private HttpServletRequest request;

    @Inject
    private MigrationLock migrationLock;

    @Inject
    private AssortmentAPIFactory assortmentAPIFactory;

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    public void migrateAssortments() throws ServiceUnavailableException {
        log.info("Started migrating assortments...");

        log.info("Reading assortments from h2...");
        List<AssortmentAPIWithArticles> assortmentAPIList = assortmentsRepository.values(true)
                                                                                 .stream()
                                                                                 .map(assortmentAPIFactory::of)
                                                                                 .filter(Objects::nonNull)
                                                                                 .collect(Collectors.toList());
        log.info("Assortments size = {}", assortmentAPIList.size());

        for (AssortmentAPIWithArticles assortmentAPI : assortmentAPIList) {
            try {
                hjmtjAssortmentRepository.save(assortmentAPI);
            } catch (Exception e){
                MigrationError migrationError = new MigrationError();
                migrationError.setSeverity(MigrationError.Severity.ERROR);
                migrationError.setClassifier(Classifier.ASSORTMENT);
                migrationError.setOrganizationName(assortmentAPI.getCustomer().getOrganizationName());
                migrationError.setMessage(e.getMessage());
                migrationErrorRepository.add(migrationError);
            }
        }

        log.info("Done migrating assortments!");
    }

    @GET
    @Path("/assortments.csv")
    @Produces("text/csv;charset=utf-8")
    public byte[] assortmentsCsv() {
        return generateReport("assortments.csv", this::buildAssortmentReport);
    }

    private byte[] buildAssortmentReport() {
        migrationLock.tryAcquire();

        try {
            List<Assortment> assortments = assortmentsRepository.values(true);
            Map<String, se.inera.hjalpmedelstjansten.model.entity.Assortment> nameToAssortment = hjmtjAssortmentRepository.values()
                                                                                                                          .stream()
                                                                                                                          .collect(toMap(se.inera.hjalpmedelstjansten.model.entity.Assortment::getName, identity()));

            StringBuilder sb = new StringBuilder();
            sb.append("Organisation;Organisation finns i tjänsten?;Namn på utbud;Län;Kommuner/Stadsdelar;Migrerades OK?;Antal artiklar (före);Antal artiklar (efter);\n");

            for (Assortment assortment : assortments) {
                Customer customer = assortment.getOrganization()
                                              .getCustomer();
                Supplier supplier = assortment.getOrganization()
                                              .getSupplier();

                String organizationName;
                if (customer != null) {
                    organizationName = customer.getOrganizationName();
                } else if (supplier != null) {
                    organizationName = supplier.getName();
                } else {
                    organizationName = "<" + assortment.getOrganization()
                                                       .getName() + ">";
                }
                sb.append(organizationName);
                sb.append(";");

                sb.append(booleanString(customer != null));
                sb.append(";");

                sb.append(assortment.getName());
                sb.append(";");

                sb.append(assortment.getCounties()
                                    .stream()
                                    .map(AssortmentCountyMunicipality::getName)
                                    .collect(Collectors.joining(", ")));
                sb.append(";");

                sb.append(assortment.getCountyMunicipalities()
                                    .stream()
                                    .map(AssortmentCountyMunicipality::getName)
                                    .collect(Collectors.joining(", ")));
                sb.append(";");

                boolean contains = nameToAssortment.containsKey(assortment.getName());
                sb.append(booleanString(contains));
                sb.append(";");

                sb.append(assortment.getProducts()
                                    .size());
                sb.append(";");

                int count = customer != null ? hjmtjAssortmentRepository.count(customer.getOrganizationName(), assortment.getName()) : 0;
                sb.append(count);
                sb.append(";");

                sb.append("\n");
            }

            return new Utf8Csv(sb.toString()).toBytes();
        } finally {
            migrationLock.release();
        }
    }

}

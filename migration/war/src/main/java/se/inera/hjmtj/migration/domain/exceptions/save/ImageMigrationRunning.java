package se.inera.hjmtj.migration.domain.exceptions.save;

public class ImageMigrationRunning extends Exception {

    public ImageMigrationRunning() {
        super("Migration is already in progress");
    }
}
/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.hjmtj;


import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenException;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationMapper;
import se.inera.hjalpmedelstjansten.business.user.controller.RoleController;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.business.user.controller.UserMapper;
import se.inera.hjalpmedelstjansten.model.api.BusinessLevelAPI;
import se.inera.hjalpmedelstjansten.model.api.ElectronicAddressAPI;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.api.RoleAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.UserEngagementAPI;
import se.inera.hjalpmedelstjansten.model.entity.UserAccount;
import se.inera.hjalpmedelstjansten.model.entity.UserEngagement;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;
import se.inera.hjmtj.migration.domain.DateTimes;
import se.inera.hjmtj.migration.domain.exceptions.save.UserAccountSaveFailed;
import se.inera.hjmtj.migration.domain.model.actors.Actor;
import se.inera.hjmtj.migration.domain.model.actors.ActorOrganization;
import se.inera.hjmtj.migration.domain.model.actors.ActorRole;
import se.inera.hjmtj.migration.domain.model.actors.ActorRoleType;
import se.inera.hjmtj.migration.domain.model.actors.Role;
import se.inera.hjmtj.migration.domain.model.agreements.builders.Unique;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MessageTemplate;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.domain.model.organization.entities.Customer;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.domain.model.product.api.Account;
import se.inera.hjmtj.migration.domain.model.result.UserReport;
import se.inera.hjmtj.migration.domain.model.result.UserResult;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.time.LocalDate;
import java.util.Base64;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

@SuppressWarnings("JpaQueryApiInspection")
@Slf4j
@Stateless
public class HjmtjUserRepository {

    public static final List<String> AGREEMENT_APPROVERS = Collections.singletonList("CustomerAgreementManager");

    private static final String TESTUSER_NAME = "Test";

    private static final String TESTUSER_EMAIL = "@inera.test";

    private final boolean MIGRATION_SET_EMAILS = Boolean.valueOf(System.getenv()
                                                                       .getOrDefault("MIGRATION_SET_EMAILS", "false"));

    @PersistenceContext(unitName = "HjmtjPU")
    private EntityManager em;

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    @Inject
    private UserController userController;

    @Inject
    private RoleController roleController;

    @Inject
    private HjmtjCustomerRepository hjmtjCustomerRepository;

    @Inject
    private HjmtjSupplierRepository hjmtjSupplierRepository;

    public List<UserAPI> getUsersInRolesOnOrganization(long organizationUniqueId, List<UserRole.RoleName> roleNames) {
        List<UserAPI> usersInRolesOnOrganization = userController.getUsersInRolesOnOrganization(organizationUniqueId,
                roleNames);

        // We need the full API
        List<UserAPI> returnValue = new LinkedList<>();
        for (UserAPI userAPI : usersInRolesOnOrganization) {
            UserAPI fullUserAPI = userController.getUserAPI(organizationUniqueId, userAPI.getId(), userAPI, "", null);
            returnValue.add(fullUserAPI);
        }

        return returnValue;
    }

    public UserAPI saveUser(Actor actor) throws HjalpmedelstjanstenValidationException {
        ActorOrganization actorOrganization = actor.getOrganization();
        Supplier supplier = actorOrganization.getSupplier();
        Customer customer = actorOrganization.getCustomer();
        OrganizationAPI organizationAPI;
        BusinessLevelAPI businessLevelAPI = null;

        if (supplier != null) {
            organizationAPI = hjmtjSupplierRepository.find(supplier);
            Objects.requireNonNull(organizationAPI);
        } else if (customer != null) {
            if (customer.isBusinessLevel()) {
                businessLevelAPI = hjmtjCustomerRepository.findBusinessLevel(customer);
                Objects.requireNonNull(businessLevelAPI);

                organizationAPI = businessLevelAPI.getOrganization();
                Objects.requireNonNull(organizationAPI);
            } else {
                organizationAPI = hjmtjCustomerRepository.find(customer);
                Objects.requireNonNull(organizationAPI);
            }
        } else {
            MigrationError migrationError = new MigrationError();
            migrationError.setClassifier(Classifier.USER_ACCOUNT);
            migrationError.setOrganizationName(actorOrganization
                    .getName());
            migrationError.setSeverity(MigrationError.Severity.INFO);
            migrationError.setUserName(actor.getUserName());
            migrationError.setMessage(MessageTemplate.ORGANIZATION_IS_NOT_A_SUBSCRIBER);
            migrationErrorRepository.add(migrationError);

            return null;
        }

        Map<Role, RoleAPI> rolesForOrganization = roleController.getRolesForOrganization(organizationAPI.getId())
                                                                .stream()
                                                                .collect(toMap(Role::of, identity()));

        List<RoleAPI> roleAPIToSet = new LinkedList<>();
        for (ActorRole actorRole : actor.getRoles()) {
            // Only add user roles that apply to this user account's organization
            Set<ActorOrganization> roleOrganizations = actorRole.getOrganizations();
            if (!roleOrganizations.contains(actorOrganization)) {
                continue;
            }

            ActorRoleType actorRoleType = actorRole.getRoleType();
            Objects.requireNonNull(actorRoleType, "actorRoleType must be non-null - " + actorRole.toString());

            Set<Role> roleIdentifiers = actorRoleType.toRoleDescriptionName();

            for (Role roleIdentifier : roleIdentifiers) {
                if (roleIdentifier != null) {
                    RoleAPI roleAPI = rolesForOrganization.get(roleIdentifier);

                    if (roleAPI == null) {
                        String role = roleIdentifier.toString();
                        String availableRoles = rolesForOrganization.keySet()
                                                                    .stream()
                                                                    .map(Role::toString)
                                                                    .collect(Collectors.joining(", "));

                        MigrationError migrationError = new MigrationError();
                        migrationError.setClassifier(Classifier.USER_ACCOUNT_ROLE);
                        migrationError.setOrganizationName(organizationAPI.getOrganizationName());
                        migrationError.setSeverity(MigrationError.Severity.WARNING);
                        migrationError.setUserName(actor.getUserName());
                        migrationError.setMessage(MessageTemplate.ROLE_COULD_NOT_BE_ASSIGNED, role, availableRoles);
                        migrationErrorRepository.add(migrationError);
                    } else {
                        roleAPIToSet.add(roleAPI);
                    }
                }
            }
        }

        UserEngagementAPI userEngagement = new UserEngagementAPI();
        List<RoleAPI> distinctRoles = roleAPIToSet.stream()
                                            .map(role -> Unique.of(role, RoleAPI::getId))
                                            .distinct()
                                            .map(Unique::data)
                                            .collect(toList());
        userEngagement.setRoles(distinctRoles);

        if (businessLevelAPI != null) {
            userEngagement.setBusinessLevels(Collections.singletonList(businessLevelAPI));
        }
        userEngagement.setOrganizationId(organizationAPI.getId());

        if (actor.getCreatedTime() == null) {
            MigrationError migrationError = new MigrationError();
            migrationError.setClassifier(Classifier.USER_ACCOUNT);
            migrationError.setOrganizationName(organizationAPI.getOrganizationName());
            migrationError.setSeverity(MigrationError.Severity.WARNING);
            migrationError.setUserName(actor.getUserName());
            migrationError.setMessage(MessageTemplate.VALID_FROM_IS_MISSING);
            migrationErrorRepository.add(migrationError);
        }
        userEngagement.setValidFrom(DateTimes.toEpochMillis(actor.getCreatedTime() != null ? actor.getCreatedTime() : actor.getTimestamp()));

        UserAPI userApi = new UserAPI();
        userApi.setUserEngagementAPIs(Collections.singletonList(userEngagement));
        userApi.setUsername(actor.getUserName());
        userApi.setTitle(actor.getTitle());
        userApi.setFirstName(actor.getFirstName() != null ? actor.getFirstName() : "");
        userApi.setLastName(actor.getLastName() != null ? actor.getLastName() : "");
        userApi.setActive(true);
        userApi.setLoginType(UserAccount.LoginType.PASSWORD.toString());
        ElectronicAddressAPI electronicAddress = new ElectronicAddressAPI();
        if (MIGRATION_SET_EMAILS) {
            electronicAddress.setEmail(actor.getContactInformation()
                                            .getEmail()
                                            .trim());
        } else {
            electronicAddress.setEmail(createDummyEmail());
        }
        electronicAddress.setMobile(actor.getContactInformation()
                                         .getCellphone());
        electronicAddress.setTelephone(actor.getContactInformation()
                                            .getWorkPhone());
        userApi.setElectronicAddress(electronicAddress);

        return saveUser(organizationAPI, userApi);
    }

    public static String createDummyEmail() {
        return UUID.randomUUID()
                   .toString()
                   .substring(0, 8) + TESTUSER_EMAIL;
    }

    public UserAPI saveUser(OrganizationAPI organizationAPI, UserAPI userApi) {
        try {
            boolean isSuperAdmin = userApi.getUserEngagements()
                                          .stream()
                                          .map(UserEngagementAPI::getRoles)
                                          .flatMap(Collection::stream)
                                          .map(RoleAPI::getName)
                                          .anyMatch(UserRole.RoleName.Superadmin.toString()::equals);

            return userController.createUser(organizationAPI.getId(), userApi, userApi, "", null, true);
        } catch (HjalpmedelstjanstenValidationException e) {
            handleError(organizationAPI, userApi, e.getValidationMessages());
        } catch (HjalpmedelstjanstenException e) {
            handleError(organizationAPI, userApi, e.getValidationMessages());
        }

        return null;
    }

    private void handleError(OrganizationAPI organizationAPI, UserAPI userAPI, Set<ErrorMessageAPI> validationMessages) {
        for (ErrorMessageAPI errorMessageAPI : validationMessages) {
            MigrationError migrationError = new MigrationError();
            migrationError.setClassifier(Classifier.field(Classifier.USER_ACCOUNT, errorMessageAPI.getField()));
            migrationError.setOrganizationName(organizationAPI.getOrganizationName());
            migrationError.setSeverity(MigrationError.Severity.ERROR);
            migrationError.setMessage(errorMessageAPI.getMessage());
            migrationError.setUserName(userAPI.getUsername());
            migrationErrorRepository.add(migrationError);
        }
    }

    public UserAPI saveUser(Account account, OrganizationAPI savedOrganization,
                            List<BusinessLevelAPI> savedBusinessLevels) throws UserAccountSaveFailed {
        try {
            Objects.requireNonNull(account, "account must be non-null");
            Objects.requireNonNull(savedOrganization, "savedOrganization must be non-null");

            List<RoleAPI> roles = roleController.getRolesForOrganization(savedOrganization.getId());

            UserEngagementAPI userEngagement = new UserEngagementAPI();
            userEngagement.setRoles(roles);
            //userEngagement.setBusinessLevels(savedBusinessLevels);
            userEngagement.setOrganizationId(savedOrganization.getId());
            userEngagement.setValidFrom(LocalDate.now()
                                                 .toEpochDay());

            UserAPI userApi = new UserAPI();
            userApi.setUserEngagementAPIs(Collections.singletonList(userEngagement));
            userApi.setUsername(account.name());
            userApi.setFirstName(account.name());
            userApi.setLastName(TESTUSER_NAME);
            userApi.setActive(true);
            userApi.setLoginType(UserAccount.LoginType.PASSWORD.toString());
            ElectronicAddressAPI electronicAddress = new ElectronicAddressAPI();
            electronicAddress.setEmail(createDummyEmail());
            userApi.setElectronicAddress(electronicAddress);

            return userController.createUser(savedOrganization.getId(), userApi, new UserAPI(), "", null, true);
        } catch (HjalpmedelstjanstenException | HjalpmedelstjanstenValidationException e) {
            throw new UserAccountSaveFailed(account.name());
        }
    }

    public void delete(long id) {
        em.createNativeQuery("DELETE FROM UserEngagementBusinessLevel " +
                "WHERE userEngagementId = :id")
          .setParameter("id", id)
          .executeUpdate();

        em.createNativeQuery("DELETE FROM UserEngagementUserRole " +
                "WHERE userEngagementId = :id")
          .setParameter("id", id)
          .executeUpdate();

        em.createNativeQuery("DELETE FROM UserEngagement " +
                "WHERE uniqueId = :id")
          .setParameter("id", id)
          .executeUpdate();

        em.createNativeQuery("DELETE FROM UserAccount " +
                "WHERE uniqueId = :id")
          .setParameter("id", id)
          .executeUpdate();
    }

    public List<UserAPI> findByOrganization(OrganizationAPI organization, List<String> roles) {
        List<Object[]> result = em.createNativeQuery("SELECT u.uniqueId AS userId, E.uniqueId AS engagementId, u.username " +
                "FROM UserAccount u " +
                "JOIN UserEngagement E on u.uniqueId = E.userId " +
                "JOIN Organization O on E.orgId = O.uniqueId " +
                "JOIN UserEngagementUserRole Role on E.uniqueId = Role.userEngagementId " +
                "JOIN UserRole R on Role.roleId = R.uniqueId " +
                "WHERE O.uniqueId = :orgId AND R.name IN :roleNames")
                                  .setParameter("orgId", organization.getId())
                                  .setParameter("roleNames", roles)
                                  .getResultList();

        return result.stream()
                     .map(arr -> (String) arr[2])
                     .map(this::findByUserName)
                     .collect(toList());
    }

    public UserAPI findByUserName(String userName) {
        List<UserEngagement> userEngagements = em.createNamedQuery(UserEngagement.FIND_BY_USERNAME, UserEngagement.class)
                                                 .setParameter("username", userName)
                                                 .getResultList();

        for (UserEngagement userEngagement : userEngagements) {
            UserAPI userAPI = UserMapper.map(userEngagement, true);

            if (userAPI.getUserEngagements() != null) {
                for (UserEngagementAPI userEngagementAPI : userAPI.getUserEngagements()) {
                    if (userEngagementAPI.getBusinessLevels() != null) {
                        for (BusinessLevelAPI businessLevelAPI : userEngagementAPI.getBusinessLevels()) {
                            OrganizationAPI organizationAPI = OrganizationMapper.map(userEngagement.getOrganization(), true);
                            businessLevelAPI.setOrganization(organizationAPI);
                        }
                    }
                }
            }

            return userAPI;
        }

        return null;
    }

    public void setPassword(String username, String password) {
        // Password must be set through DB
        // This is affected by a bug where ID is off by 1, unsure if caused by tampering with hibernateSeqNo
        String salt = generateSalt();
        if (em.createNativeQuery("UPDATE UserAccount " +
                "SET password = :password, salt = :salt, iterations = :iterations " +
                "WHERE username = :username")
              .setParameter("username", username)
              .setParameter("password", hashPassword(password, salt, 1))
              .setParameter("salt", salt)
              .setParameter("iterations", 1)
              .executeUpdate() != 1) {
            throw new IllegalStateException("Failed to set password for " + username);
        }
    }

    private String generateSalt() {
        final Random random = new SecureRandom();
        byte[] salt = new byte[32];
        random.nextBytes(salt);
        return Base64.getEncoder()
                     .encodeToString(salt);
    }

    private String hashPassword(String passwordToHash, String salt, int iterations) {
        try {
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
            PBEKeySpec spec = new PBEKeySpec(passwordToHash.toCharArray(), salt.getBytes(), iterations, 256);
            SecretKey key = skf.generateSecret(spec);
            byte[] res = key.getEncoded();
            return Base64.getEncoder()
                         .encodeToString(res);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }

    public UserAPI findBy(Actor actor) {
        String userName = actor.getUserName();
        return findByUserName(userName);
    }

    public List<UserResult> results(List<UserReport> userReports) {
        Map<String, UserAPI> userNameMap = em.createQuery("SELECT ue FROM UserEngagement ue", UserEngagement.class)
                                             .getResultList()
                                             .stream()
                                             .map(ue -> UserMapper.map(ue, true))
                                             .collect(Collectors.toMap(UserAPI::getUsername, Function.identity()));

        return userReports.stream()
                          .map(r -> new UserResult(r, userNameMap.get(r.username())))
                          .collect(toList());
    }

}

/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.xml;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.Value;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.stream.events.XMLEvent;
import java.io.StringReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

@Slf4j
@Value
class XMLFileDeserialization {

    private final com.fasterxml.jackson.dataformat.xml.XmlMapper xmlMapper;

    private final Map<String, Class<?>> tagToResultClass;

    private final Set<String> dynamicTags;

    private final Unmarshaller unmarshaller;

    private final FileIdResolver idResolver = new FileIdResolver();

    private XMLFileDeserialization(Map<String, Class<?>> tagToResultClass, Set<String> dynamicTags) {
        this.tagToResultClass = tagToResultClass;
        this.dynamicTags = dynamicTags;

        xmlMapper = new com.fasterxml.jackson.dataformat.xml.XmlMapper();
        xmlMapper.registerModule(new JavaTimeModule());
        xmlMapper.registerModule(new JacksonXmlModule());
        xmlMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);

        try {
            Class<?>[] resultClassArray = tagToResultClass.values()
                                                          .toArray(new Class<?>[0]);
            JAXBContext jaxbContext = JAXBContext.newInstance(resultClassArray);
            unmarshaller = jaxbContext.createUnmarshaller();
            unmarshaller.setProperty("com.sun.xml.bind.IDResolver", idResolver);
        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    public void clear() {
        idResolver.clear();
    }

    public ElementFilter get(XMLEvent xmlEvent) {
        String name;

        if (xmlEvent.isStartElement()) {
            name = xmlEvent.asStartElement()
                           .getName()
                           .toString();
        } else if (xmlEvent.isEndElement()) {
            name = xmlEvent.asEndElement()
                           .getName()
                           .toString();
        } else {
            return null;
        }

        Class<?> resultClass = tagToResultClass.get(name);

        if (resultClass != null) {
            return new ElementFilter<>(name, resultClass);
        }

        return null;
    }

    public static class Builder {

        private final Map<String, Class<?>> tagToResultClass = new HashMap<>();

        private final Set<String> dynamicTag = new HashSet<>();

        public Builder() {
        }

        public Builder register(Class<?> resultClass, Set<String> tags) {
            for (String tag : tags) {
                if (register(resultClass, tag)) {
                    dynamicTag.add(tag);
                }
            }

            return this;
        }

        private boolean register(Class<?> resultClass, String tag) {
            if (tagToResultClass.containsKey(tag)) {
                Class<?> otherResultClass = tagToResultClass.get(tag);
                String otherResultClassName = otherResultClass.getSimpleName();
                throw new IllegalStateException("Tag " + tag + " has already been assigned to " + otherResultClassName);
            } else {
                tagToResultClass.put(tag, resultClass);
                return true;
            }
        }

        public Builder register(Class<?> resultClass) {
            XmlRootElement rootElement = resultClass.getDeclaredAnnotation(XmlRootElement.class);
            XmlTransient xmlTransient = resultClass.getDeclaredAnnotation(XmlTransient.class);

            if (xmlTransient == null) {
                Objects.requireNonNull(rootElement, resultClass + " must be annotated with @XmlRootElement");
                String tag = rootElement.name();
                register(resultClass, tag);
            }

            return this;
        }

        public XMLFileDeserialization build() {
            return new XMLFileDeserialization(tagToResultClass, dynamicTag);
        }
    }

    @Value
    @Accessors(fluent = true)
    public class ElementContext<R> {

        private final ElementFilter<R> mapper;

        private final String xml;

        public R get() {
            try {
                if (dynamicTags.contains(mapper.name())) {
                    return xmlMapper.readValue(xml, mapper.resultClass());
                }

                return deserialize();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        private R deserialize() throws JAXBException {
            return (R) unmarshaller.unmarshal(new StringReader(xml));
        }

        @Override
        public String toString() {
            return mapper.resultClass.getSimpleName();
        }
    }

    @Value
    @Accessors(fluent = true)
    public class ElementFilter<R> {

        private final String name;

        private final Class<R> resultClass;

        public boolean isStart(XMLEvent xmlEvent) {
            if (xmlEvent.isStartElement()) {
                String elementName = xmlEvent.asStartElement()
                                             .getName()
                                             .toString();

                return name.equals(elementName);
            }

            return false;
        }

        public boolean isEnd(XMLEvent xmlEvent) {
            if (xmlEvent.isEndElement()) {
                String elementName = xmlEvent.asEndElement()
                                             .getName()
                                             .toString();

                return name.equals(elementName);
            }

            return false;
        }

        public ElementContext<R> context(String xml) {
            return new ElementContext<>(this, xml);
        }

    }

}

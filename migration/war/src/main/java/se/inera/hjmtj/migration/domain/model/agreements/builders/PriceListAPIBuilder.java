/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.agreements.builders;


import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelistRow;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementBase;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementRowBase;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementVersion;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.Agreement;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.AgreementRow;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MessageTemplate;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjCvGuaranteeUnitRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjProductRepository;
import se.inera.hjmtj.migration.infra.products.ProductRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

import static java.util.stream.Collectors.groupingBy;
import static se.inera.hjmtj.migration.domain.DateTimes.toEpochMillis;

@Slf4j
@Accessors(fluent = true)
@RequiredArgsConstructor
@Getter
@Setter
public final class PriceListAPIBuilder {

    private final static String VISUERA_ACTIVE = "Aktiv";

    private Set<AgreementRowBase> rows;

    private OrganizationAPI supplier;

    private HjmtjProductRepository hjmtjProductRepository;

    private ProductRepository productRepository;

    private HjmtjCvGuaranteeUnitRepository hjmtjGuaranteeUnitRepository;

    private Set<AgreementVersion> agreementVersionList;

    private MigrationErrorRepository migrationErrorRepository;

    private AgreementBase agreementBase;

    public List<AgreementPricelistAPI> buildAll() {
        Objects.requireNonNull(rows, "rows must be non-null");
        Objects.requireNonNull(supplier, "supplier must be non-null");
        Objects.requireNonNull(hjmtjProductRepository, "hjmtjProductRepository must be non-null");
        Objects.requireNonNull(productRepository, "productRepository must be non-null");
        Objects.requireNonNull(hjmtjGuaranteeUnitRepository, "hjmtjGuaranteeUnitRepository must be non-null");
        Objects.requireNonNull(agreementVersionList, "agreementVersionList must be non-null");
        Objects.requireNonNull(migrationErrorRepository, "migrationErrorRepository must be non-null");
        Objects.requireNonNull(agreementBase, "agreementBase must be non-null");

        // Filter out rows that have pricelistnumber = null
        Map<String, List<AgreementRowBase>> priceListNumbers = rows.stream()
                                                                   .filter(this::rowPriceListNumberNotNull)
                                                                   .collect(groupingBy(getGetSupplierPriceListNumber()));

        List<AgreementPricelistAPI> priceLists = new ArrayList<>();

        for (Map.Entry<String, List<AgreementRowBase>> entry : priceListNumbers.entrySet()) {
            String priceListNumber = entry.getKey();
            List<AgreementRowBase> priceListRows = entry.getValue();

            AgreementPricelistAPI agreementPricelistAPI = build(priceListNumber, priceListRows);

            if (agreementPricelistAPI != null) {
                priceLists.add(agreementPricelistAPI);
            }
        }

        return priceLists;
    }

    private boolean rowPriceListNumberNotNull(AgreementRowBase agreementRowBase) {
        return agreementRowBase.getSupplierPriceListNumber() != null;
    }

    public Function<AgreementRowBase, String> getGetSupplierPriceListNumber() {
        return row -> row.getSupplierPriceListNumber()
                         .trim();
    }

    private AgreementPricelistAPI build(String priceListNumber, List<AgreementRowBase> agreementRowBases) {
        AgreementPricelistAPI priceListApi = new AgreementPricelistAPI();
        priceListApi.setNumber(priceListNumber);

        boolean associated = false;
        for (AgreementVersion version : agreementVersionList) {
            if (isAssociated(priceListNumber, version)) {
                LocalDateTime validFrom = version.getValidFrom();
                priceListApi.setValidFrom(toEpochMillis(validFrom));
                associated = true;
                break;
            }
        }

        // If the rows have a priceListNumber but the version list does not contain that version number
        // we will not migrate the pricelist
        if (!associated) {
            MigrationError migrationError = new MigrationError();
            migrationError.setSeverity(MigrationError.Severity.WARNING);
            migrationError.setClassifier(agreementBase instanceof Agreement ? Classifier.PRICELIST_ROW : Classifier.GP_PRICELIST);
            migrationError.setOrganizationName(supplier.getOrganizationName());
            migrationError.setAgreementNumber(priceListApi.getAgreement() != null ? priceListApi.getAgreement()
                                                                                                .getAgreementNumber() : null);
            migrationError.setPricelistNumber(priceListApi.getNumber());
            migrationError.setMessage(MessageTemplate.MISSING_VERSION_LIST_ENTRY, priceListApi.getNumber());
            migrationErrorRepository.add(migrationError);

            return null;
        }

        List<AgreementPricelistRowAPI> rowsToSave = new ArrayList<>();

        for (AgreementRowBase agreementRow : agreementRowBases) {
            AgreementPricelistRowAPI priceListRowAPI = new PriceListRowAPIBuilder().agreementRow(agreementRow)
                                                                                   .defaultUnit(hjmtjGuaranteeUnitRepository.days())
                                                                                   .priceList(priceListApi)
                                                                                   .migrationErrorRepository(migrationErrorRepository)
                                                                                   .productRepository(productRepository)
                                                                                   .organizationAPI(supplier)
                                                                                   .build();

            if (priceListRowAPI != null && priceListRowAPI.getArticle() != null) {
                if (!priceListRowAPI.getStatus()
                                    .equals(AgreementPricelistRow.Status.INACTIVE.name())) {
                    rowsToSave.add(priceListRowAPI);
                } else {
                    MigrationError migrationError = new MigrationError();
                    migrationError.setSeverity(MigrationError.Severity.WARNING);
                    migrationError.setClassifier(agreementRow instanceof AgreementRow ? Classifier.PRICELIST_ROW : Classifier.GP_PRICELIST_ROW);
                    migrationError.setOrganizationName(supplier.getOrganizationName());
                    migrationError.setCatalogueUniqueNumber(Long.valueOf(agreementRow.getAgreementArticle()
                                                                                     .getVgrProdNo()));
                    migrationError.setAgreementNumber(priceListApi.getAgreement() != null ? priceListApi.getAgreement()
                                                                                                        .getAgreementNumber() : null);
                    migrationError.setPricelistNumber(priceListApi.getNumber());
                    migrationError.setMessageTemplate(MessageTemplate.INACTIVE_PRICELIST_ROW);
                    migrationError.setMessage(MessageTemplate.INACTIVE_PRICELIST_ROW.toString());
                    migrationErrorRepository.add(migrationError);
                }
            }
        }

        priceListApi.setRows(rowsToSave);

        return priceListApi;
    }

    private boolean isAssociated(String rowPriceListNumber, AgreementVersion version) {
        if (rowPriceListNumber == null) {
            return false;
        }

        String headVersionPriceListNumber = version != null ? version.getVersionNumber() : null;

        if (headVersionPriceListNumber == null) {
            return false;
        }

        return rowPriceListNumber.trim()
                                 .equalsIgnoreCase(headVersionPriceListNumber.trim());
    }

}

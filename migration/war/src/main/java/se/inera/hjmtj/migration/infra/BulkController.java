/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra;


import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.indexing.controller.ElasticSearchController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleController;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleMapper;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleValidation;
import se.inera.hjalpmedelstjansten.business.product.controller.CategoryController;
import se.inera.hjalpmedelstjansten.business.product.controller.ProductController;
import se.inera.hjalpmedelstjansten.business.product.controller.ProductMapper;
import se.inera.hjalpmedelstjansten.business.product.controller.ProductValidation;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjalpmedelstjansten.model.api.ResourceSpecificPropertyAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.UserEngagementAPI;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.CatalogueUniqueNumber;
import se.inera.hjalpmedelstjansten.model.entity.Category;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificProperty;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificPropertyListValue;
import se.inera.hjalpmedelstjansten.model.entity.ElectronicAddress;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValue;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueDecimal;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueInterval;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueTextField;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueValueListMultiple;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueValueListSingle;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEDirective;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEStandard;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVOrderUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPackageUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;
import se.inera.hjmtj.migration.domain.MigrationFilter;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MessageTemplate;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.logging.Level;

import static java.util.Optional.ofNullable;

/**
 * Specialized controller for persisting large volumes of data quickly using native queries and postphoned validation.
 */
@SuppressWarnings("JpaQueryApiInspection")
@Stateless
public class BulkController {

    @Inject
    HjmtLogger LOG;

    @PersistenceContext(unitName = "HjmtjPU")
    EntityManager em;

    @Inject
    ArticleValidation articleValidation;

    @Inject
    ProductValidation productValidation;

    @Inject
    ValidationMessageService validationMessageService;

    @Inject
    ProductController productController;

    @Inject
    CategoryController categoryController;

    @Inject
    ElasticSearchController elasticSearchController;

    @Inject
    private BulkController bulkController;

    @Inject
    private ArticleController articleController;

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    @Inject
    private MigrationFilter migrationFilter;

    public void createArticle(long catalogueUniqueNumber,
                              Organization organization,
                              ArticleAPI articleAPI,
                              Map<Long, CVOrderUnit> allOrderUnits) throws HjalpmedelstjanstenValidationException {
        if (migrationFilter.isDeprecated(articleAPI, catalogueUniqueNumber)) {
            return;
        }

        // https://riv-ta.atlassian.net/browse/HJAL-1443
        ProductAPI basedOnProductAPI = articleAPI.getBasedOnProduct();
        if (basedOnProductAPI != null) {
            if (basedOnProductAPI.isCustomerUnique() && !articleAPI.isCustomerUnique()) {
                articleAPI.setCustomerUnique(basedOnProductAPI.isCustomerUnique());

                MigrationError migrationError = new MigrationError();
                migrationError.setSeverity(MigrationError.Severity.WARNING);
                migrationError.setOrganizationName(organization.getOrganizationName());
                migrationError.setCatalogueUniqueNumber(catalogueUniqueNumber);
                migrationError.setClassifier(Classifier.HJALPMEDEL_CUSTOMER_UNIQUE);
                migrationError.setMessage(MessageTemplate.ARTICLE_CUSTOMER_UNIQUE_CHANGED);
                migrationErrorRepository.add(migrationError);
            }
        }

        // ArticleController.checkCustomerUnique requires userAPI
        UserAPI dummyUser = getDummyUserAPI(organization);

        ArticleMapper.fix(articleAPI);

        // remove any id references to category specific properties that may be
        // sent. when creating, there are no previous values. it's easier to do
        // it here in backend than to solve it in frontend and it also makes
        // more sense to have it here.
        if( articleAPI.getCategoryPropertys() != null && !articleAPI.getCategoryPropertys().isEmpty() ) {
            for( ResourceSpecificPropertyAPI resourceSpecificPropertyAPI : articleAPI.getCategoryPropertys() ) {
                resourceSpecificPropertyAPI.setId(null);
            }
        }

        articleValidation.validateForCreate(articleAPI, organization.getUniqueId());
        Article article = ArticleMapper.map(articleAPI);

        // Specific for bulk controller
        article.setUniqueId(articleAPI.getId());
        article.setCreated(articleAPI.getCreated() == null ? new Date() : new Date(articleAPI.getCreated()));
        CatalogueUniqueNumber catalogUniqueNumber = new CatalogueUniqueNumber();
        catalogUniqueNumber.setUniqueId(catalogueUniqueNumber);
        article.setCatalogUniqueNumber(catalogUniqueNumber);

        // add organization
        article.setOrganization(organization);

        // add based on product relation
        Product basedOnProduct = null;
        if (articleAPI.getBasedOnProduct() != null) {
            basedOnProduct = productController.getProduct(organization.getUniqueId(), articleAPI.getBasedOnProduct()
                                                                                                .getId(), dummyUser);
            if (basedOnProduct == null) {
                LOG.log(Level.FINEST, "product {0} does not exist", new Object[]{articleAPI.getBasedOnProduct().getId()});
                throw validationMessageService.generateValidationException("basedOnProduct", "article.product.basedOn.notExists");
            }
            article.setBasedOnProduct(basedOnProduct);
        } else {
            // add primary category, if article is based on product, category is inherited
            List<Category> categories = categoryController.getChildlessById(articleAPI.getCategory()
                                                                                      .getId());
            if (categories == null || categories.isEmpty()) {
                LOG.log(Level.FINEST, "category {0} does not exist or is not a leaf node", new Object[]{articleAPI.getCategory().getId()});
                throw validationMessageService.generateValidationException("category", "article.category.notExists");
            }
            article.setCategory(categories.get(0));

        }

        // add fits to
        articleController.setFitsTo(organization.getUniqueId(), articleAPI, article, dummyUser);

        // add extended categories
        articleController.setExtendedCategories(articleAPI, article);

        // add ce, directives and standards (if any)
        articleController.setCE(articleAPI, article);

        // add manufacturer
        articleController.setManufacturer(articleAPI, article);

        // add preventive maintainance
        articleController.setPreventiveMaintenance(articleAPI, article, dummyUser);

        // customer unique article
        articleController.setCustomerUnique(articleAPI, article);

        // order information
        articleController.checkSetOrderInformation(articleAPI, article, allOrderUnits);

        // set supplemental information
        articleController.setSupplementedInformation(articleAPI, article);

        article.setMediaFolderName(UUID.randomUUID()
                                       .toString());

        articleController.handleReplaceArticle(article, articleAPI, organization.getUniqueId(), dummyUser);

        bulkController.save(article, articleAPI);

        // Index
        articleAPI.setStatus(article.getStatus()
                                    .toString());
        articleAPI.setOrganizationId(organization.getUniqueId());
        articleAPI.setOrganizationName(organization.getOrganizationName());
        elasticSearchController.bulkIndexArticle(articleAPI);
    }

    public void save(Article article, ArticleAPI articleAPI) throws HjalpmedelstjanstenValidationException {
        if (article.getCatalogUniqueNumber() != null) {
            em.createNativeQuery("INSERT INTO CatalogueUniqueNumber (uniqueId) VALUES (:uniqueId)")
              .setParameter("uniqueId", article.getCatalogUniqueNumber()
                                               .getUniqueId())
              .executeUpdate();
        }

        em.createNativeQuery("INSERT INTO Article (uniqueId, articleName, articleNumber, articleQuantityInOuterPackage," +
                " ceDirectiveOverridden, ceMarked, ceMarkedOverridden, ceStandardOverridden, created, customerUnique, customerUniqueOverridden, gtin, " +
                "lastUpdated, manufacturer, manufacturerArticleNumber, manufacturerArticleNumberOverridden, " +
                "manufacturerElectronicAddressOverridden, manufacturerOverridden, mediaFolderName, " +
                "orderInformationOverridden, packageContent, packageLevelBase, packageLevelMiddle, packageLevelTop, " +
                "preventiveMaintenanceDescription, preventiveMaintenanceDescriptionOverridden, " +
                "preventiveMaintenanceNumberOfDays, preventiveMaintenanceNumberOfDaysOverridden, " +
                "preventiveMaintenanceValidFromOverridden, replacementDate, status, statusOverridden, " +
                "supplementedInformation, supplementedInformationOverridden, " +
                "trademark, trademarkOverridden, articleQuantityInOuterPackageUnitId, " +
                "basedOnProductId, catalogUniqueNumberId, categoryId, ceDirectiveId, " +
                "ceStandardId, manufacturerElectronicAddressId, orderUnitId, " +
                "organizationId, packageContentId, preventiveMaintenanceValidFromId, " +
                "color, colorOverridden) " +
                "VALUES (:uniqueId, :articleName, :articleNumber, :articleQuantityInOuterPackage, " +
                ":ceDirectiveOverridden, :ceMarked, :ceMarkedOverridden, :ceStandardOverridden, :created, :customerUnique, :customerUniqueOverridden, :gtin, " +
                ":lastUpdated, :manufacturer, :manufacturerArticleNumber, :manufacturerArticleNumberOverridden, " +
                ":manufacturerElectronicAddressOverridden, :manufacturerOverridden, :mediaFolderName, " +
                ":orderInformationOverridden, :packageContent, :packageLevelBase, :packageLevelMiddle, :packageLevelTop, " +
                ":preventiveMaintenanceDescription, :preventiveMaintenanceDescriptionOverridden, " +
                ":preventiveMaintenanceNumberOfDays, :preventiveMaintenanceNumberOfDaysOverridden, " +
                ":preventiveMaintenanceValidFromOverridden, :replacementDate, :articleStatus, :statusOverridden, " +
                ":supplementedInformation, :supplementedInformationOverridden, " +
                ":trademark, :trademarkOverridden, :articleQuantityInOuterPackageUnitId, " +
                ":basedOnProductId, :catalogUniqueNumberId, :categoryId, :ceDirectiveId, " +
                ":ceStandardId, :manufacturerElectronicAddressId, :orderUnitId, " +
                ":organizationId, :packageContentId, :preventiveMaintenanceValidFromId, " +
                ":color, :colorOverridden)")
          .setParameter("uniqueId", article.getUniqueId())
          .setParameter("articleName", article.getArticleName())
          .setParameter("articleNumber", article.getArticleNumber())
          .setParameter("articleQuantityInOuterPackage", article.getArticleQuantityInOuterPackage())
          .setParameter("ceDirectiveOverridden", article.isCeDirectiveOverridden())
          .setParameter("ceMarked", article.isCeMarked())
          .setParameter("ceMarkedOverridden", article.isCeMarkedOverridden())
          .setParameter("ceStandardOverridden", article.isCeStandardOverridden())
          .setParameter("created", article.getCreated())
          .setParameter("customerUnique", article.isCustomerUnique())
          .setParameter("customerUniqueOverridden", article.isCustomerUniqueOverridden())
          .setParameter("gtin", article.getGtin())
          .setParameter("lastUpdated", article.getLastUpdated())
          .setParameter("manufacturer", article.getManufacturer())
          .setParameter("manufacturerArticleNumber", article.getManufacturerArticleNumber())
          .setParameter("manufacturerArticleNumberOverridden", article.isManufacturerArticleNumberOverridden())
          .setParameter("manufacturerElectronicAddressOverridden", article.isManufacturerElectronicAddressOverridden())
          .setParameter("manufacturerOverridden", article.isManufacturerOverridden())
          .setParameter("mediaFolderName", article.getMediaFolderName())
          .setParameter("orderInformationOverridden", article.isOrderInformationOverridden())
          .setParameter("packageContent", article.getPackageContent())
          .setParameter("packageLevelBase", article.getPackageLevelBase())
          .setParameter("packageLevelMiddle", article.getPackageLevelMiddle())
          .setParameter("packageLevelTop", article.getPackageLevelTop())
          .setParameter("preventiveMaintenanceDescription", article.getPreventiveMaintenanceDescription())
          .setParameter("preventiveMaintenanceDescriptionOverridden", article.isPreventiveMaintenanceDescriptionOverridden())
          .setParameter("preventiveMaintenanceNumberOfDays", article.getPreventiveMaintenanceNumberOfDays())
          .setParameter("preventiveMaintenanceNumberOfDaysOverridden", article.isPreventiveMaintenanceNumberOfDaysOverridden())
          .setParameter("preventiveMaintenanceValidFromOverridden", article.isPreventiveMaintenanceValidFromOverridden())
          .setParameter("replacementDate", article.getReplacementDate())
          .setParameter("articleStatus", article.getStatus()
                                                .toString())
          .setParameter("statusOverridden", article.isStatusOverridden())
          .setParameter("supplementedInformation", article.getSupplementedInformation())
          .setParameter("supplementedInformationOverridden", article.isSupplementedInformationOverridden())
          .setParameter("trademark", article.getTrademark())
          .setParameter("trademarkOverridden", article.isTrademarkOverridden())
          .setParameter("articleQuantityInOuterPackageUnitId", ofNullable(article.getArticleQuantityInOuterPackageUnit()).map(CVOrderUnit::getUniqueId)
                                                                                                                         .orElse(null))
          .setParameter("basedOnProductId", article.getBasedOnProduct())
          .setParameter("catalogUniqueNumberId", article.getCatalogUniqueNumber())
          .setParameter("categoryId", ofNullable(article.getCategory()).map(Category::getUniqueId)
                                                                       .orElse(null))
          .setParameter("ceDirectiveId", ofNullable(article.getCeDirective()).map(CVCEDirective::getUniqueId)
                                                                             .orElse(null))
          .setParameter("ceStandardId", ofNullable(article.getCeStandard()).map(CVCEStandard::getUniqueId)
                                                                           .orElse(null))
          .setParameter("manufacturerElectronicAddressId", ofNullable(article.getManufacturerElectronicAddress()).map(ElectronicAddress::getUniqueId)
                                                                                                                 .orElse(null))
          .setParameter("orderUnitId", ofNullable(article.getOrderUnit()).map(CVOrderUnit::getUniqueId)
                                                                         .orElse(null))
          .setParameter("organizationId", ofNullable(article.getOrganization()).map(Organization::getUniqueId)
                                                                               .orElse(null))
          .setParameter("packageContentId", ofNullable(article.getPackageContentUnit()).map(CVPackageUnit::getUniqueId)
                                                                                       .orElse(null))
          .setParameter("preventiveMaintenanceValidFromId", ofNullable(article.getPreventiveMaintenanceValidFrom()).map(CVPreventiveMaintenance::getUniqueId)
                                                                                                                   .orElse(null))
          .setParameter("color", article.getColor())
          .setParameter("colorOverridden", article.isColorOverridden())
          .executeUpdate();

        Article attachedArticle = em.find(Article.class, article.getUniqueId());

        if (attachedArticle != null) {
            if (article.getManufacturerElectronicAddress() != null) {
                attachedArticle.setManufacturerElectronicAddress(article.getManufacturerElectronicAddress());
            }

            if (article.getFitsToProducts() != null) {
                attachedArticle.getFitsToProducts()
                               .addAll(article.getFitsToProducts());
            }

            if (article.getFitsToArticles() != null) {
                attachedArticle.getFitsToArticles()
                               .addAll(article.getFitsToArticles());
            }

            if (article.getExtendedCategories() != null) {
                attachedArticle.getExtendedCategories()
                               .addAll(article.getExtendedCategories());
            }

            if (article.getReplacedByArticles() != null) {
                List<Article> replacedByArticles = new LinkedList<>(new HashSet<>(article.getReplacedByArticles()));
                attachedArticle.getReplacedByArticles()
                               .addAll(replacedByArticles);
            }

            // category specific properties
            Category category = article.getBasedOnProduct() == null ? article.getCategory() : article.getBasedOnProduct()
                                                                                                     .getCategory();
            List<CategorySpecificProperty> categorySpecificPropertys = categoryController.getCategoryPropertys(category.getUniqueId());
            setResourceSpecificProperties(articleAPI, article, category, categorySpecificPropertys);
        }
    }

    public void setResourceSpecificProperties(ArticleAPI articleAPI, Article article, Category category, List<CategorySpecificProperty> categorySpecificPropertys) {
        LOG.log(Level.FINEST, "setResourceSpecificProperties( article->uniqueId: {1} )", new Object[]{article.getUniqueId()});
        // add/update properties
        if (articleAPI.getCategoryPropertys() != null && !articleAPI.getCategoryPropertys()
                                                                    .isEmpty()) {
            if (article.getResourceSpecificPropertyValues() == null) {
                article.setResourceSpecificPropertyValues(new ArrayList<>());
            }
            for (ResourceSpecificPropertyAPI resourceSpecificPropertyAPI : articleAPI.getCategoryPropertys()) {
                // first make sure the property exists for the article category
                CategorySpecificProperty categorySpecificProperty = productController.categorySpecificPropertysContains(resourceSpecificPropertyAPI.getProperty()
                                                                                                                                                   .getId(), categorySpecificPropertys);
                if (categorySpecificProperty == null) {
                    LOG.log(Level.WARNING, "attempt to set category specific property for article {0} but category: {1} of article does not have it", new Object[]{article.getUniqueId(), category.getUniqueId()});

                    //throw validationMessageService.generateValidationException("categoryProperties", "article.categoryProperty.notExist");
                    return;
                }
                LOG.log(Level.FINEST, "Check if category specific property: {0} is already answered", new Object[]{categorySpecificProperty.getUniqueId()});
                if (resourceSpecificPropertyAPI.getId() != null) {
                    // previously answered question, value may be updated
                    ResourceSpecificPropertyValue resourceSpecificPropertyValue = productController.getResourceSpecificPropertyValue(article.getResourceSpecificPropertyValues(), resourceSpecificPropertyAPI.getId());
                    if (resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueTextField) {
                        ResourceSpecificPropertyValueTextField resourceSpecificPropertyValueTextfield = (ResourceSpecificPropertyValueTextField) resourceSpecificPropertyValue;
                        resourceSpecificPropertyValueTextfield.setValue(resourceSpecificPropertyAPI.getTextValue());
                    } else if (resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueDecimal) {
                        ResourceSpecificPropertyValueDecimal resourceSpecificPropertyValueDecimal = (ResourceSpecificPropertyValueDecimal) resourceSpecificPropertyValue;
                        resourceSpecificPropertyValueDecimal.setValue(resourceSpecificPropertyAPI.getDecimalValue());
                    } else if (resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueInterval) {
                        ResourceSpecificPropertyValueInterval resourceSpecificPropertyValueInterval = (ResourceSpecificPropertyValueInterval) resourceSpecificPropertyValue;
                        resourceSpecificPropertyValueInterval.setFromValue(resourceSpecificPropertyAPI.getIntervalFromValue());
                        resourceSpecificPropertyValueInterval.setToValue(resourceSpecificPropertyAPI.getIntervalToValue());
                    } else if (resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueValueListSingle) {
                        CategorySpecificPropertyListValue categorySpecificPropertyListValue = resourceSpecificPropertyAPI.getSingleListValue() == null ? null : categoryController.getCategorySpecificPropertyListValue(resourceSpecificPropertyAPI.getSingleListValue());
                        ResourceSpecificPropertyValueValueListSingle resourceSpecificPropertyValueValueListSingle = (ResourceSpecificPropertyValueValueListSingle) resourceSpecificPropertyValue;
                        resourceSpecificPropertyValueValueListSingle.setValue(categorySpecificPropertyListValue);
                    } else if (resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueValueListMultiple) {
                        List<CategorySpecificPropertyListValue> categorySpecificPropertyListValues = new ArrayList<>();
                        ResourceSpecificPropertyValueValueListMultiple resourceSpecificPropertyValueValueListMultiple = (ResourceSpecificPropertyValueValueListMultiple) resourceSpecificPropertyValue;
                        if (resourceSpecificPropertyAPI.getMultipleListValue() != null) {
                            for (Long listValue : resourceSpecificPropertyAPI.getMultipleListValue()) {
                                CategorySpecificPropertyListValue categorySpecificPropertyListValue = categoryController.getCategorySpecificPropertyListValue(listValue);
                                categorySpecificPropertyListValues.add(categorySpecificPropertyListValue);
                            }
                        }
                        resourceSpecificPropertyValueValueListMultiple.setValues(categorySpecificPropertyListValues);
                    }
                } else {
                    // make sure property hasn't already been answered
                    ResourceSpecificPropertyValue alreadyAnsweredResourceSpecificPropertyValue = productController.getResourceSpecificPropertyValueByCategorySpecificProperty(article.getResourceSpecificPropertyValues(), resourceSpecificPropertyAPI.getProperty()
                                                                                                                                                                                                                                                      .getId());
                    if (alreadyAnsweredResourceSpecificPropertyValue != null) {
                        LOG.log(Level.WARNING, "attempt to set category specific property: {0} for article {1} but property has already been set in request ", new Object[]{categorySpecificProperty.getUniqueId(), article.getUniqueId()});
                        //throw validationMessageService.generateValidationException("categoryProperties", "article.categoryProperty.alreadySet");
                        return;
                    }
                    // only save new property if a value is set and the value differs
                    // from that of the product (if product has the property)
                    ResourceSpecificPropertyValue productResourceSpecificPropertyValue = productController.getResourceSpecificPropertyValueByCategorySpecificProperty(article.getBasedOnProduct()
                                                                                                                                                                             .getResourceSpecificPropertyValues(), resourceSpecificPropertyAPI.getProperty()
                                                                                                                                                                                                                                              .getId());
                    boolean saveProperty = true;
                    if (categorySpecificProperty.getType() == CategorySpecificProperty.Type.TEXTFIELD) {
                        String productValue = productResourceSpecificPropertyValue == null ? null : ((ResourceSpecificPropertyValueTextField) productResourceSpecificPropertyValue).getValue();
                        String articleValue = resourceSpecificPropertyAPI.getTextValue();
                        if (!Objects.equals(productValue, articleValue)) {
                            ResourceSpecificPropertyValueTextField resourceSpecificPropertyValueTextfield = new ResourceSpecificPropertyValueTextField();
                            resourceSpecificPropertyValueTextfield.setArticle(article);
                            resourceSpecificPropertyValueTextfield.setCategorySpecificProperty(categorySpecificProperty);
                            resourceSpecificPropertyValueTextfield.setValue(resourceSpecificPropertyAPI.getTextValue());
                            em.persist(resourceSpecificPropertyValueTextfield);
                            article.getResourceSpecificPropertyValues()
                                   .add(resourceSpecificPropertyValueTextfield);
                        }
                    } else if (categorySpecificProperty.getType() == CategorySpecificProperty.Type.DECIMAL) {
                        Double productValue = productResourceSpecificPropertyValue == null ? null : ((ResourceSpecificPropertyValueDecimal) productResourceSpecificPropertyValue).getValue();
                        Double articleValue = resourceSpecificPropertyAPI.getDecimalValue();
                        if (!Objects.equals(productValue, articleValue)) {
                            ResourceSpecificPropertyValueDecimal resourceSpecificPropertyValueDecimal = new ResourceSpecificPropertyValueDecimal();
                            resourceSpecificPropertyValueDecimal.setCategorySpecificProperty(categorySpecificProperty);
                            resourceSpecificPropertyValueDecimal.setArticle(article);
                            resourceSpecificPropertyValueDecimal.setValue(resourceSpecificPropertyAPI.getDecimalValue());
                            em.persist(resourceSpecificPropertyValueDecimal);
                            article.getResourceSpecificPropertyValues()
                                   .add(resourceSpecificPropertyValueDecimal);
                        }
                    } else if (categorySpecificProperty.getType() == CategorySpecificProperty.Type.INTERVAL) {
                        Double productFromValue = productResourceSpecificPropertyValue == null ? null : ((ResourceSpecificPropertyValueInterval) productResourceSpecificPropertyValue).getFromValue();
                        Double productToValue = productResourceSpecificPropertyValue == null ? null : ((ResourceSpecificPropertyValueInterval) productResourceSpecificPropertyValue).getToValue();
                        Double articleFromValue = resourceSpecificPropertyAPI.getIntervalFromValue();
                        Double articleToValue = resourceSpecificPropertyAPI.getIntervalToValue();
                        if (!Objects.equals(productFromValue, articleFromValue) || !Objects.equals(productToValue, articleToValue)) {
                            ResourceSpecificPropertyValueInterval resourceSpecificPropertyValueInterval = new ResourceSpecificPropertyValueInterval();
                            resourceSpecificPropertyValueInterval.setCategorySpecificProperty(categorySpecificProperty);
                            resourceSpecificPropertyValueInterval.setArticle(article);
                            resourceSpecificPropertyValueInterval.setFromValue(resourceSpecificPropertyAPI.getIntervalFromValue());
                            resourceSpecificPropertyValueInterval.setToValue(resourceSpecificPropertyAPI.getIntervalToValue());
                            em.persist(resourceSpecificPropertyValueInterval);
                            article.getResourceSpecificPropertyValues()
                                   .add(resourceSpecificPropertyValueInterval);
                        }
                    } else if (categorySpecificProperty.getType() == CategorySpecificProperty.Type.VALUELIST_SINGLE) {
                        Long productValue = productResourceSpecificPropertyValue == null ? null : ((ResourceSpecificPropertyValueValueListSingle) productResourceSpecificPropertyValue).getValue()
                                                                                                                                                                                       .getUniqueId();
                        Long articleValue = resourceSpecificPropertyAPI.getSingleListValue();
                        if (!Objects.equals(productValue, articleValue)) {
                            CategorySpecificPropertyListValue categorySpecificPropertyListValue = resourceSpecificPropertyAPI.getSingleListValue() == null ? null : categoryController.getCategorySpecificPropertyListValue(resourceSpecificPropertyAPI.getSingleListValue());
                            ResourceSpecificPropertyValueValueListSingle resourceSpecificPropertyValueValueListSingle = new ResourceSpecificPropertyValueValueListSingle();
                            resourceSpecificPropertyValueValueListSingle.setCategorySpecificProperty(categorySpecificProperty);
                            resourceSpecificPropertyValueValueListSingle.setArticle(article);
                            resourceSpecificPropertyValueValueListSingle.setValue(categorySpecificPropertyListValue);
                            em.persist(resourceSpecificPropertyValueValueListSingle);
                            article.getResourceSpecificPropertyValues()
                                   .add(resourceSpecificPropertyValueValueListSingle);
                        }
                    } else if (resourceSpecificPropertyAPI.getMultipleListValue() != null) {
                        List<CategorySpecificPropertyListValue> productListValues = productResourceSpecificPropertyValue == null ? null : ((ResourceSpecificPropertyValueValueListMultiple) productResourceSpecificPropertyValue).getValues();
                        List<Long> articleListValues = resourceSpecificPropertyAPI.getMultipleListValue();
                        int productNumberOfValues = productListValues == null ? 0 : productListValues.size();
                        int articleNumberOfValues = articleListValues.size();
                        if (productNumberOfValues != articleNumberOfValues || !articleValuesAreSameAsProduct(productListValues, articleListValues)) {
                            List<CategorySpecificPropertyListValue> categorySpecificPropertyListValues = new ArrayList<>();
                            ResourceSpecificPropertyValueValueListMultiple resourceSpecificPropertyValueValueListMultiple = new ResourceSpecificPropertyValueValueListMultiple();
                            resourceSpecificPropertyValueValueListMultiple.setCategorySpecificProperty(categorySpecificProperty);
                            resourceSpecificPropertyValueValueListMultiple.setArticle(article);
                            for (Long listValue : resourceSpecificPropertyAPI.getMultipleListValue()) {
                                CategorySpecificPropertyListValue categorySpecificPropertyListValue = categoryController.getCategorySpecificPropertyListValue(listValue);
                                categorySpecificPropertyListValues.add(categorySpecificPropertyListValue);
                            }
                            resourceSpecificPropertyValueValueListMultiple.setValues(categorySpecificPropertyListValues);
                            em.persist(resourceSpecificPropertyValueValueListMultiple);
                            article.getResourceSpecificPropertyValues()
                                   .add(resourceSpecificPropertyValueValueListMultiple);
                        }
                    }
                }
            }
        }
    }

    private boolean articleValuesAreSameAsProduct(List<CategorySpecificPropertyListValue> productValues, List<Long> articleValues) {
        // do they have the same number of values
        int productNumberOfValues = productValues == null ? 0 : productValues.size();
        int articleNumberOfValues = articleValues == null ? 0 : articleValues.size();
        LOG.log(Level.FINEST, "productNumberOfValues: {0}, articleNumberOfValues: {1}", new Object[]{productNumberOfValues, articleNumberOfValues});
        if (productNumberOfValues != articleNumberOfValues) {
            return false;
        }

        // is there anything in the list to compare? we know the lists are the same size
        if (productNumberOfValues == 0) {
            return true;
        }

        // same number of values, are they the same values?
        for (CategorySpecificPropertyListValue productValue : productValues) {
            if (!articleValues.contains(productValue.getUniqueId())) {
                return false;
            }
        }
        return true;
    }

    public void createProduct(Long catalogueUniqueNumber, Organization organization,
                              ProductAPI productAPI, Map<Long, CVOrderUnit> allOrderUnits, Map<Long, CVPackageUnit> allPackageUnits) throws HjalpmedelstjanstenValidationException {
        if (migrationFilter.isDeprecated(productAPI, catalogueUniqueNumber)) {
            return;
        }

        // ProductController.checkCustomerUnique requires userAPI
        UserAPI dummyUser = getDummyUserAPI(organization);

        ProductMapper.fix(productAPI);
        productValidation.validateForCreate(productAPI, organization.getUniqueId());
        Product product = ProductMapper.map(productAPI);

        // Specific for bulk controller
        product.setUniqueId(productAPI.getId());
        product.setCreated(productAPI.getCreated() == null ? new Date() : new Date(productAPI.getCreated()));
        CatalogueUniqueNumber catalogUniqueNumber = new CatalogueUniqueNumber();
        catalogUniqueNumber.setUniqueId(catalogueUniqueNumber);
        product.setCatalogUniqueNumber(catalogUniqueNumber);

        // add organization
        product.setOrganization(organization);

        // add primary category
        List<Category> categories = categoryController.getChildlessById(productAPI.getCategory()
                                                                                  .getId());
        if (categories == null || categories.isEmpty()) {
            LOG.log(Level.FINEST, "category {0} does not exist or is not a leaf node", new Object[]{productAPI.getCategory().getId()});
            throw validationMessageService.generateValidationException("category", "product.category.notExists");
        }
        Category category = categories.get(0);
        // only possible to create products with article type H and T with iso codes
        if (category.getArticleType() != Article.Type.T && category.getArticleType() != Article.Type.H) {
            LOG.log(Level.FINEST, "category {0} is not of article type T or H, but instead: {1} which is not allowed for products", new Object[]{category.getUniqueId(), category.getArticleType()});
            throw validationMessageService.generateValidationException("category", "product.category.wrongArticleType");
        } else {
            if (category.getCode() == null) {
                LOG.log(Level.FINEST, "category {0} is of right article type T or H, but has not iso code which is not allowed for products", new Object[]{category.getUniqueId()});
                throw validationMessageService.generateValidationException("category", "product.category.wrongArticleType");
            }
        }
        product.setCategory(category);

        // add extended categories
        productController.setExtendedCategories(productAPI, product);

        // add directives and standards (if any)
        productController.setCe(productAPI, product);

        // preventive maintenance valid from
        productController.setPreventiveMaintenanceValidFrom(productAPI, product, dummyUser);

        // order information
        productController.setOrderInformation(productAPI, product, allOrderUnits, allPackageUnits);

        product.setMediaFolderName(UUID.randomUUID()
                                       .toString());

        productController.handleReplaceProduct(product, productAPI, organization.getUniqueId(), product.getUniqueId(), dummyUser);

        bulkController.save(product, productAPI);

        // Index
        productAPI.setStatus(product.getStatus()
                                    .toString());
        productAPI.setOrganizationId(organization.getUniqueId());
        productAPI.setOrganizationName(organization.getOrganizationName());
        elasticSearchController.bulkIndexProduct(productAPI);
    }

    public UserAPI getDummyUserAPI(Organization organization) {
        UserAPI dummyUser = new UserAPI();
        dummyUser.setId(-1l);
        UserEngagementAPI userEngagementAPI = new UserEngagementAPI();
        userEngagementAPI.setOrganizationId(organization.getUniqueId());
        dummyUser.setUserEngagementAPIs(Collections.singletonList(userEngagementAPI));
        return dummyUser;
    }

    public void save(Product product, ProductAPI productAPI) throws HjalpmedelstjanstenValidationException {
        if (product.getCatalogUniqueNumber() != null) {
            em.createNativeQuery("INSERT INTO CatalogueUniqueNumber (uniqueId) VALUES (:uniqueId)")
              .setParameter("uniqueId", product.getCatalogUniqueNumber()
                                               .getUniqueId())
              .executeUpdate();
        }

        em.createNativeQuery("INSERT INTO Product (uniqueId, articleQuantityInOuterPackage, ceMarked, created, " +
                "customerUnique, lastUpdated, manufacturer, manufacturerProductNumber, mediaFolderName, packageContent," +
                " packageLevelBase, packageLevelMiddle, packageLevelTop, preventiveMaintenanceDescription," +
                " preventiveMaintenanceNumberOfDays, productName, productNumber, replacementDate, status, " +
                "supplementedInformation, trademark, articleQuantityInOuterPackageUnitId, catalogUniqueNumberId," +
                " categoryId, ceDirectiveId, ceStandardId," +
                " manufacturerElectronicAddressId, orderUnitId, organizationId, packageContentId, " +
                "preventiveMaintenanceValidFromId, color) VALUES (" +
                ":uniqueId, :articleQuantityInOuterPackage, :ceMarked, :created, " +
                ":customerUnique, :lastUpdated, :manufacturer, :manufacturerProductNumber, :mediaFolderName, :packageContent, " +
                ":packageLevelBase, :packageLevelMiddle, :packageLevelTop, :preventiveMaintenanceDescription, " +
                ":preventiveMaintenanceNumberOfDays, :productName, :productNumber, :replacementDate, :productStatus, " +
                ":supplementedInformation, :trademark, :articleQuantityInOuterPackageUnitId, :catalogUniqueNumberId, " +
                ":categoryId, :ceDirectiveId, :ceStandardId, " +
                ":manufacturerElectronicAddressId, :orderUnitId, :organizationId, :packageContentId, " +
                ":preventiveMaintenanceValidFromId, :color)")
          .setParameter("uniqueId", product.getUniqueId())
          .setParameter("articleQuantityInOuterPackage", product.getArticleQuantityInOuterPackage())
          .setParameter("ceMarked", product.isCeMarked())
          .setParameter("created", product.getCreated())
          .setParameter("customerUnique", ofNullable(product.isCustomerUnique()).orElse(false))
          .setParameter("lastUpdated", product.getLastUpdated())
          .setParameter("manufacturer", product.getManufacturer())
          .setParameter("manufacturerProductNumber", product.getManufacturerProductNumber())
          .setParameter("mediaFolderName", product.getMediaFolderName())
          .setParameter("packageContent", product.getPackageContent())
          .setParameter("packageLevelBase", product.getPackageLevelBase())
          .setParameter("packageLevelMiddle", product.getPackageLevelMiddle())
          .setParameter("packageLevelTop", product.getPackageLevelTop())
          .setParameter("preventiveMaintenanceDescription", product.getPreventiveMaintenanceDescription())
          .setParameter("preventiveMaintenanceNumberOfDays", product.getPreventiveMaintenanceNumberOfDays())
          .setParameter("productName", product.getProductName())
          .setParameter("productNumber", product.getProductNumber())
          .setParameter("replacementDate", product.getReplacementDate())
          .setParameter("productStatus", product.getStatus()
                                                .toString())
          .setParameter("supplementedInformation", product.getSupplementedInformation())
          .setParameter("trademark", product.getTrademark())
          .setParameter("articleQuantityInOuterPackageUnitId", ofNullable(product.getArticleQuantityInOuterPackageUnit()).map(CVOrderUnit::getUniqueId)
                                                                                                                         .orElse(null))
          .setParameter("catalogUniqueNumberId", ofNullable(product.getCatalogUniqueNumber()).map(CatalogueUniqueNumber::getUniqueId)
                                                                                             .orElse(null))
          .setParameter("categoryId", ofNullable(product.getCategory()).map(Category::getUniqueId)
                                                                       .orElse(null))
          .setParameter("ceDirectiveId", ofNullable(product.getCeDirective()).map(CVCEDirective::getUniqueId)
                                                                             .orElse(null))
          .setParameter("ceStandardId", ofNullable(product.getCeStandard()).map(CVCEStandard::getUniqueId)
                                                                           .orElse(null))
          .setParameter("manufacturerElectronicAddressId", ofNullable(product.getManufacturerElectronicAddress()).map(ElectronicAddress::getUniqueId)
                                                                                                                 .orElse(null))
          .setParameter("orderUnitId", ofNullable(product.getOrderUnit()).map(CVOrderUnit::getUniqueId)
                                                                         .orElse(null))
          .setParameter("organizationId", ofNullable(product.getOrganization()).map(Organization::getUniqueId)
                                                                               .orElse(null))
          .setParameter("packageContentId", ofNullable(product.getPackageContentUnit()).map(CVPackageUnit::getUniqueId)
                                                                                       .orElse(null))
          .setParameter("preventiveMaintenanceValidFromId", ofNullable(product.getPreventiveMaintenanceValidFrom()).map(CVPreventiveMaintenance::getUniqueId)
                                                                                                                   .orElse(null))
          .setParameter("color", product.getColor())
          .executeUpdate();

        Product attachedProduct = em.find(Product.class, product.getUniqueId());

        if (attachedProduct != null) {
            if (product.getManufacturerElectronicAddress() != null) {
                attachedProduct.setManufacturerElectronicAddress(product.getManufacturerElectronicAddress());
            }

            if (product.getExtendedCategories() != null) {
                attachedProduct.getExtendedCategories()
                               .addAll(product.getExtendedCategories());
            }

            if (product.getReplacedByProducts() != null) {
                List<Product> replacedByProducts = new LinkedList<>(new HashSet<>(product.getReplacedByProducts()));
                attachedProduct.getReplacedByProducts()
                               .addAll(replacedByProducts);
            }

            // category specific properties
            List<CategorySpecificProperty> categorySpecificPropertys = categoryController.getCategoryPropertys(product.getCategory()
                                                                                                                      .getUniqueId());
            setResourceSpecificProperties(productAPI, product, categorySpecificPropertys, null);
        }
    }

    public void setResourceSpecificProperties(ProductAPI productAPI, Product product, List<CategorySpecificProperty> categorySpecificPropertys, Boolean categoryWasChanged) {
        LOG.log(Level.FINEST, "setResourceSpecificProperties( product->uniqueId: {1} )", new Object[]{product.getUniqueId()});

        if (categoryWasChanged != null && categoryWasChanged) {
            product.getResourceSpecificPropertyValues()
                   .clear();
        }

        // add/update properties
        if (productAPI.getCategoryPropertys() != null && !productAPI.getCategoryPropertys()
                                                                    .isEmpty()) {
            if (product.getResourceSpecificPropertyValues() == null) {
                product.setResourceSpecificPropertyValues(new ArrayList<>());
            }
            for (ResourceSpecificPropertyAPI resourceSpecificPropertyAPI : productAPI.getCategoryPropertys()) {
                // first make sure the property exists for the product category
                CategorySpecificProperty categorySpecificProperty = categorySpecificPropertysContains(resourceSpecificPropertyAPI.getProperty()
                                                                                                                                 .getId(), categorySpecificPropertys);
                if (categorySpecificProperty == null) {
                    LOG.log(Level.WARNING, "attempt to set category specific property for product {0} but category: {1} of product does not have it", new Object[]{product.getUniqueId(), product.getCategory().getUniqueId()});
                    //throw validationMessageService.generateValidationException("categoryProperties", "product.categoryProperty.notExist");
                    return;
                }
                LOG.log(Level.FINEST, "Check if category specific property: {0} is already answered", new Object[]{categorySpecificProperty.getUniqueId()});
                if (resourceSpecificPropertyAPI.getId() != null) {
                    // previously answered question, value may be updated
                    ResourceSpecificPropertyValue resourceSpecificPropertyValue = getResourceSpecificPropertyValue(product.getResourceSpecificPropertyValues(), resourceSpecificPropertyAPI.getId());
                    if (resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueTextField) {
                        ResourceSpecificPropertyValueTextField resourceSpecificPropertyValueTextfield = (ResourceSpecificPropertyValueTextField) resourceSpecificPropertyValue;
                        resourceSpecificPropertyValueTextfield.setValue(resourceSpecificPropertyAPI.getTextValue());
                    } else if (resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueDecimal) {
                        ResourceSpecificPropertyValueDecimal resourceSpecificPropertyValueDecimal = (ResourceSpecificPropertyValueDecimal) resourceSpecificPropertyValue;
                        resourceSpecificPropertyValueDecimal.setValue(resourceSpecificPropertyAPI.getDecimalValue());
                    } else if (resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueInterval) {
                        ResourceSpecificPropertyValueInterval resourceSpecificPropertyValueInterval = (ResourceSpecificPropertyValueInterval) resourceSpecificPropertyValue;
                        resourceSpecificPropertyValueInterval.setFromValue(resourceSpecificPropertyAPI.getIntervalFromValue());
                        resourceSpecificPropertyValueInterval.setToValue(resourceSpecificPropertyAPI.getIntervalToValue());
                    } else if (resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueValueListSingle) {
                        CategorySpecificPropertyListValue categorySpecificPropertyListValue = resourceSpecificPropertyAPI.getSingleListValue() == null ? null : categoryController.getCategorySpecificPropertyListValue(resourceSpecificPropertyAPI.getSingleListValue());
                        ResourceSpecificPropertyValueValueListSingle resourceSpecificPropertyValueValueListSingle = (ResourceSpecificPropertyValueValueListSingle) resourceSpecificPropertyValue;
                        resourceSpecificPropertyValueValueListSingle.setValue(categorySpecificPropertyListValue);
                    } else if (resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueValueListMultiple) {
                        List<CategorySpecificPropertyListValue> categorySpecificPropertyListValues = new ArrayList<>();
                        ResourceSpecificPropertyValueValueListMultiple resourceSpecificPropertyValueValueListMultiple = (ResourceSpecificPropertyValueValueListMultiple) resourceSpecificPropertyValue;
                        if (resourceSpecificPropertyAPI.getMultipleListValue() != null && !resourceSpecificPropertyAPI.getMultipleListValue()
                                                                                                                      .isEmpty()) {
                            for (Long listValue : resourceSpecificPropertyAPI.getMultipleListValue()) {
                                CategorySpecificPropertyListValue categorySpecificPropertyListValue = categoryController.getCategorySpecificPropertyListValue(listValue);
                                categorySpecificPropertyListValues.add(categorySpecificPropertyListValue);
                            }
                        }
                        resourceSpecificPropertyValueValueListMultiple.setValues(categorySpecificPropertyListValues);
                    }
                } else {
                    // make sure property hasn't already been answered
                    ResourceSpecificPropertyValue alreadyAnsweredResourceSpecificPropertyValue = getResourceSpecificPropertyValueByCategorySpecificProperty(product.getResourceSpecificPropertyValues(), resourceSpecificPropertyAPI.getProperty()
                                                                                                                                                                                                                                    .getId());
                    if (alreadyAnsweredResourceSpecificPropertyValue != null) {
                        LOG.log(Level.WARNING, "attempt to set category specific property: {0} for product {1} but property has already been set in request ", new Object[]{categorySpecificProperty.getUniqueId(), product.getUniqueId()});
                        //throw validationMessageService.generateValidationException("categoryProperties", "product.categoryProperty.alreadySet");
                        return;
                    }
                    // only save new property if a value is set
                    if (resourceSpecificPropertyAPI.getTextValue() != null && !resourceSpecificPropertyAPI.getTextValue()
                                                                                                          .isEmpty()) {
                        ResourceSpecificPropertyValueTextField resourceSpecificPropertyValueTextfield = new ResourceSpecificPropertyValueTextField();
                        resourceSpecificPropertyValueTextfield.setProduct(product);
                        resourceSpecificPropertyValueTextfield.setCategorySpecificProperty(categorySpecificProperty);
                        resourceSpecificPropertyValueTextfield.setValue(resourceSpecificPropertyAPI.getTextValue());
                        em.persist(resourceSpecificPropertyValueTextfield);
                        product.getResourceSpecificPropertyValues()
                               .add(resourceSpecificPropertyValueTextfield);
                    } else if (resourceSpecificPropertyAPI.getDecimalValue() != null) {
                        ResourceSpecificPropertyValueDecimal resourceSpecificPropertyValueDecimal = new ResourceSpecificPropertyValueDecimal();
                        resourceSpecificPropertyValueDecimal.setCategorySpecificProperty(categorySpecificProperty);
                        resourceSpecificPropertyValueDecimal.setProduct(product);
                        resourceSpecificPropertyValueDecimal.setValue(resourceSpecificPropertyAPI.getDecimalValue());
                        em.persist(resourceSpecificPropertyValueDecimal);
                        product.getResourceSpecificPropertyValues()
                               .add(resourceSpecificPropertyValueDecimal);
                    } else if (resourceSpecificPropertyAPI.getIntervalFromValue() != null || resourceSpecificPropertyAPI.getIntervalToValue() != null) {
                        ResourceSpecificPropertyValueInterval resourceSpecificPropertyValueInterval = new ResourceSpecificPropertyValueInterval();
                        resourceSpecificPropertyValueInterval.setCategorySpecificProperty(categorySpecificProperty);
                        resourceSpecificPropertyValueInterval.setProduct(product);
                        resourceSpecificPropertyValueInterval.setFromValue(resourceSpecificPropertyAPI.getIntervalFromValue());
                        resourceSpecificPropertyValueInterval.setToValue(resourceSpecificPropertyAPI.getIntervalToValue());
                        em.persist(resourceSpecificPropertyValueInterval);
                        product.getResourceSpecificPropertyValues()
                               .add(resourceSpecificPropertyValueInterval);
                    } else if (resourceSpecificPropertyAPI.getSingleListValue() != null) {
                        CategorySpecificPropertyListValue categorySpecificPropertyListValue = categoryController.getCategorySpecificPropertyListValue(resourceSpecificPropertyAPI.getSingleListValue());
                        ResourceSpecificPropertyValueValueListSingle resourceSpecificPropertyValueValueListSingle = new ResourceSpecificPropertyValueValueListSingle();
                        resourceSpecificPropertyValueValueListSingle.setCategorySpecificProperty(categorySpecificProperty);
                        resourceSpecificPropertyValueValueListSingle.setProduct(product);
                        resourceSpecificPropertyValueValueListSingle.setValue(categorySpecificPropertyListValue);
                        em.persist(resourceSpecificPropertyValueValueListSingle);
                        product.getResourceSpecificPropertyValues()
                               .add(resourceSpecificPropertyValueValueListSingle);
                    } else if (resourceSpecificPropertyAPI.getMultipleListValue() != null) {
                        List<CategorySpecificPropertyListValue> categorySpecificPropertyListValues = new ArrayList<>();
                        ResourceSpecificPropertyValueValueListMultiple resourceSpecificPropertyValueValueListMultiple = new ResourceSpecificPropertyValueValueListMultiple();
                        resourceSpecificPropertyValueValueListMultiple.setCategorySpecificProperty(categorySpecificProperty);
                        resourceSpecificPropertyValueValueListMultiple.setProduct(product);
                        for (Long listValue : resourceSpecificPropertyAPI.getMultipleListValue()) {
                            CategorySpecificPropertyListValue categorySpecificPropertyListValue = categoryController.getCategorySpecificPropertyListValue(listValue);
                            categorySpecificPropertyListValues.add(categorySpecificPropertyListValue);
                        }
                        resourceSpecificPropertyValueValueListMultiple.setValues(categorySpecificPropertyListValues);
                        em.persist(resourceSpecificPropertyValueValueListMultiple);
                        product.getResourceSpecificPropertyValues()
                               .add(resourceSpecificPropertyValueValueListMultiple);

                    }
                }
            }
        }
    }

    public ResourceSpecificPropertyValue getResourceSpecificPropertyValue(List<ResourceSpecificPropertyValue> resourceSpecificPropertyValues, long resourceSpecificPropertyId) {
        if (resourceSpecificPropertyValues != null && !resourceSpecificPropertyValues.isEmpty()) {
            for (ResourceSpecificPropertyValue resourceSpecificPropertyValue : resourceSpecificPropertyValues) {
                if (resourceSpecificPropertyValue.getUniqueId()
                                                 .equals(resourceSpecificPropertyId)) {
                    return resourceSpecificPropertyValue;
                }
            }
        }
        return null;
    }

    public ResourceSpecificPropertyValue getResourceSpecificPropertyValueByCategorySpecificProperty(List<ResourceSpecificPropertyValue> resourceSpecificPropertyValues, long categorySpecificPropertyId) {
        if (resourceSpecificPropertyValues != null && !resourceSpecificPropertyValues.isEmpty()) {
            for (ResourceSpecificPropertyValue resourceSpecificPropertyValue : resourceSpecificPropertyValues) {
                if (resourceSpecificPropertyValue.getCategorySpecificProperty()
                                                 .getUniqueId()
                                                 .equals(categorySpecificPropertyId)) {
                    return resourceSpecificPropertyValue;
                }
            }
        }
        return null;
    }

    public CategorySpecificProperty categorySpecificPropertysContains(long categorySpecificPropertyId, List<CategorySpecificProperty> categorySpecificPropertys) {
        if (categorySpecificPropertys != null && !categorySpecificPropertys.isEmpty()) {
            for (CategorySpecificProperty categorySpecificProperty : categorySpecificPropertys) {
                if (categorySpecificProperty.getUniqueId()
                                            .equals(categorySpecificPropertyId)) {
                    return categorySpecificProperty;
                }
            }
        }
        return null;
    }

}

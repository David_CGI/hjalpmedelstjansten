/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.organizations;

import lombok.extern.slf4j.Slf4j;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjmtj.migration.domain.HeapUsageInterceptor;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.domain.model.OrganizationBase;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductOrganization;
import se.inera.hjmtj.migration.domain.model.result.SupplierReport;
import se.inera.hjmtj.migration.infra.products.ProductOrganizationRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.concurrent.TimeUnit.HOURS;
import static java.util.function.Function.identity;

@Slf4j
@Stateless
@Interceptors({PerformanceLogInterceptor.class})
@TransactionTimeout(unit = HOURS, value = 5)
public class SupplierRepository {

    private final Map<String, String> supplierNameMap = new HashMap<>();

    @PersistenceContext(unitName = "local")
    private EntityManager em;

    @Inject
    private ProductOrganizationRepository productOrganizationRepository;

    public SupplierRepository() {
        supplierNameMap.put("Smith & Nephew AB", "Smith-Nephew AB");
        supplierNameMap.put("Doctor Lundh AB", "Doctorlundh AB");
        supplierNameMap.put("Svart på Vitt Reklam Video & Design AB", "Svart på Vitt AB");
    }

    @Interceptors({HeapUsageInterceptor.class})
    public List<Supplier> suppliers() {
        return em.createNamedQuery(Supplier.FIND_ALL, Supplier.class)
                 .getResultList()
                 .stream()
                 .sorted(Comparator.comparing(this::productCount))
                 .collect(Collectors.toList());
    }

    private int productCount(Supplier supplier) {
        ProductOrganization productOrganization = supplier.getProductOrganization();
        return productOrganization != null && productOrganization.getProducts() != null ? productOrganization.getProducts()
                                                                                                             .size() : 0;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    private long size() {
        return em.createNamedQuery(Supplier.COUNT_ALL, Long.class)
                 .getSingleResult();
    }

    public Supplier getSupplier(OrganizationBase organization) {
        ProductOrganization productOrganization = productOrganizationRepository.findByIdentifier(organization.getIdentifier());

        if (productOrganization != null &&
                productOrganization.getSupplier() != null) {
            return productOrganization.getSupplier();
        }

        String organizationName = supplierNameMap.getOrDefault(organization.getName(), organization.getName());
        return findByName(organizationName);
    }

    private Supplier findByName(String organizationName) {
        List<Supplier> supplierList = em.createNamedQuery(Supplier.FIND_BY_NAME, Supplier.class)
                                        .setParameter("name", organizationName.toLowerCase()
                                                                              .trim())
                                        .setMaxResults(1)
                                        .getResultList();
        return supplierList.size() > 0 ? supplierList.get(0) : null;
    }

    public Supplier getSupplier(ProductOrganization productOrganization) {
        if (productOrganization.getSupplier() != null) {
            return productOrganization.getSupplier();
        }

        List<Supplier> supplier = em.createQuery("SELECT s FROM Supplier s WHERE s.gln = :gln OR " +
                "      lower(trim(s.name)) = :name", Supplier.class)
                                    .setParameter("gln", productOrganization.getGln() == null ? null : productOrganization.getGln()
                                                                                                                          .trim())
                                    .setParameter("name", productOrganization.getName() == null ? null : productOrganization.getName()
                                                                                                                            .trim()
                                                                                                                            .toLowerCase())
                                    .setMaxResults(1)
                                    .getResultList();

        return supplier.size() > 0 ? supplier.get(0) : null;
    }


    public Map<String, SupplierReport> reports() {
        List<Object[]> resultList = em.createNativeQuery("SELECT coalesce(s.NAME, po.NAME), " +
                "       s.name is not null, " +
                "       count(distinct case when p.TYPE = 'ARTICLE' THEN null ELSE p.id end), " +
                "       count(distinct case when p.TYPE = 'PRODUCT' THEN null ELSE p.id end) " +
                "FROM PRODUCT p\n" +
                "       join PRODUCTORGANIZATION po ON p.SUPPLIER_ID = po.ID\n" +
                "       left join SUPPLIER s ON po.SUPPLIER_ID = s.ID\n" +
                "WHERE p.ISO9999 = TRUE AND p.NOARTICLETYPE = false " +
                "GROUP BY s.ID, po.ID\n" +
                "order by count(p.ID) desc")
                                      .getResultList();
        return resultList
                .stream()
                .map(SupplierReport::h2)
                .collect(Collectors.toMap(SupplierReport::getOrganizationName, identity()));
    }
}

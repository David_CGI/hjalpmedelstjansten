/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

public class DateTimes {

    private static final ZoneId ZONE = ZoneId.of("Europe/Stockholm");

    private static final ZoneOffset OFFSET_STOCKHOLM = ZonedDateTime.now(ZONE)
                                                                    .getOffset();

    public static Long toEpochMillis(String dateTimeString) {
        if (dateTimeString == null) {
            return null;
        }

        return LocalDateTime.parse(dateTimeString)
                            .toInstant(OFFSET_STOCKHOLM)
                            .toEpochMilli();
    }

    public static Long toEpochMillis(LocalDateTime localDateTime) {
        if (localDateTime == null) {
            return null;
        }

        return localDateTime.toInstant(OFFSET_STOCKHOLM)
                            .toEpochMilli();
    }

    public static Long toEpochMillis(LocalDate date) {
        return date != null ? date.atStartOfDay(OFFSET_STOCKHOLM)
                                  .toEpochSecond() * 1000 : null;
    }

    public static LocalDate epochMillisToDate(Long epochMillis) {
        return Instant.ofEpochMilli(epochMillis).atZone(OFFSET_STOCKHOLM).toLocalDate();
    }
}

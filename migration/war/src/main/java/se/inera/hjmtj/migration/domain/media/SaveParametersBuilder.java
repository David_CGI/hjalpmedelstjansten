/*
 *
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 *  Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.media;

import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.domain.model.product.entities.Product;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductFile;

public class SaveParametersBuilder {

    private Supplier supplier;

    private Product product;

    private String url;

    private String description;

    private String altText;

    private String srcContentUrl;

    private boolean isDoc;

    private boolean isMainImage;

    private String contentType;

    private String docType;

    private ProductFile mainImage;

    private Product templateProduct;

    public SaveParametersBuilder setSupplier(Supplier supplier) {
        this.supplier = supplier;
        return this;
    }

    public SaveParametersBuilder setProduct(Product product) {
        this.product = product;
        return this;
    }

    public SaveParametersBuilder setUrl(String url) {
        this.url = url;
        return this;
    }

    public SaveParametersBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    public SaveParametersBuilder setAltText(String altText) {
        this.altText = altText;
        return this;
    }

    public SaveParametersBuilder set_srcContentUrl(String srcContentUrl) {
        this.srcContentUrl = srcContentUrl;
        return this;
    }

    public SaveParametersBuilder setIsDoc(boolean isDoc) {
        this.isDoc = isDoc;
        return this;
    }

    public SaveParametersBuilder setIsMainImage(boolean isMainImage) {
        this.isMainImage = isMainImage;
        return this;
    }

    public SaveParametersBuilder setContentType(String contentType) {
        this.contentType = contentType;
        return this;
    }

    public SaveParametersBuilder setDocType(String docType) {
        this.docType = docType;
        return this;
    }

    public SaveParametersBuilder setMainImage(ProductFile mainImage) {
        this.mainImage = mainImage;
        return this;
    }

    public SaveParameters createSaveParameters() {
        return new SaveParameters(supplier, product, url, description, altText, srcContentUrl, isDoc, isMainImage, contentType, docType, mainImage, templateProduct);
    }

    public SaveParametersBuilder setTemplateProduct(Product templateProduct) {
        this.templateProduct=templateProduct;
        return this;
    }
}
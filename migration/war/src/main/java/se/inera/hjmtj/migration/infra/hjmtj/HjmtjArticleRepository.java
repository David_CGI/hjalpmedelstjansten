/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package se.inera.hjmtj.migration.infra.hjmtj;

import lombok.extern.slf4j.Slf4j;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;

import javax.ejb.Stateless;
import javax.interceptor.Interceptor;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.concurrent.TimeUnit.HOURS;

@Slf4j
@Stateless
@Interceptors({PerformanceLogInterceptor.class})
@TransactionTimeout(unit = HOURS, value = 5)
public class HjmtjArticleRepository {

    @PersistenceContext(unitName = "HjmtjPU")
    private EntityManager em;

    public long size() {
        return ((BigInteger) em.createNativeQuery("SELECT COUNT(*) FROM Article ")
                               .setMaxResults(1)
                               .getSingleResult()).longValue();
    }

    public boolean contains(long uniqueId) {
        try {
            return em.createNativeQuery("SELECT a.uniqueId FROM Article a WHERE a.uniqueId = :uniqueId")
                     .setParameter("uniqueId", uniqueId)
                     .getSingleResult() != null;
        } catch (NoResultException e) {
            return false;
        }
    }

    public Map<Article, Long> findByGLN(String gln) {
        List<Article> articles = em.createQuery("SELECT a FROM Article a " +
                "LEFT JOIN FETCH a.catalogUniqueNumber " +
                "WHERE a.organization.gln = :gln", Article.class)
                                   .setParameter("gln", gln)
                                   .setHint("org.hibernate.readOnly", true)
                                   .getResultList();

        Map<Article, Long> articleToProduct = new HashMap<>();
        for (Article article : articles) {
            articleToProduct.put(article, article.getBasedOnProduct() != null ? article.getBasedOnProduct()
                                                                                       .getCatalogUniqueNumber()
                                                                                       .getUniqueId() : null);
        }

        return articleToProduct;
    }
}

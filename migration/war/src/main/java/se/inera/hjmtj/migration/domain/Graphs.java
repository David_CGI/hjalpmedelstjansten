/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain;

import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedAcyclicGraph;
import org.jgrapht.traverse.TopologicalOrderIterator;

import java.util.LinkedList;
import java.util.List;

public interface Graphs {

    static <T> DirectedAcyclicGraph<T, DefaultEdge> directedAcyclicGraph() {
        return new DirectedAcyclicGraph<>(DefaultEdge.class);
    }

    static <T> List<T> topological(DirectedAcyclicGraph<T, DefaultEdge> graph) {
        LinkedList<T> returnValue = new LinkedList<>();
        TopologicalOrderIterator<T, DefaultEdge> iterator = new TopologicalOrderIterator<>(graph);

        while (iterator.hasNext()) {
            returnValue.add(iterator.next());
        }

        return returnValue;
    }

}

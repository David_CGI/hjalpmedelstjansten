/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.agreements;

import lombok.Data;
import se.inera.hjmtj.migration.domain.model.Reference;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.PersistenceUnit;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;

import static javax.persistence.InheritanceType.TABLE_PER_CLASS;


@PersistenceUnit(unitName = "local")
@Inheritance(strategy = TABLE_PER_CLASS)
@MappedSuperclass
@Data
@XmlTransient
public abstract class AgreementWarrantyBase<U extends AgreementWarrantyUnitBase, C extends AgreementCalculatedFromBase> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long generatedId;

    @XmlElement(name = "_x0039_a6935ebf5eb4955b6c9a072dd948036")
    private Long numericValue;

    @Column(columnDefinition = "VARCHAR", length = 1024)
    @XmlElement(name = "ece01645e72c42999bb4b757d4f9112a")
    private String termsAndConditions;


    @ManyToOne(fetch = FetchType.EAGER)
    private U unit;

    @ManyToOne(fetch = FetchType.EAGER)
    private C calculatedFrom;

    @XmlElement(name = "_x0035_731d6d603a34eaf89b9c4dbc9678e31")
    public void setXmlUnit(Reference<U> reference) {
        unit = reference.getReference();
    }

    @XmlElement(name = "d5c4e7d32f0a43b3a5341ce63c0be1ed")
    public void setXmlCalculatedFrom(Reference<C> reference) {
        calculatedFrom = reference.getReference();
    }


}

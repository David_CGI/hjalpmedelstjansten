/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.application.bootstrap;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.application.MigrationApplication;
import se.inera.hjmtj.migration.domain.HeapUsageInterceptor;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.domain.model.Indexed;
import se.inera.hjmtj.migration.domain.model.organization.entities.Customer;
import se.inera.hjmtj.migration.domain.model.organization.entities.Secret;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.infra.io.FtpConnection;
import se.inera.hjmtj.migration.infra.organizations.CustomerRepository;
import se.inera.hjmtj.migration.infra.organizations.SecretsRepository;
import se.inera.hjmtj.migration.infra.organizations.SupplierRepository;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * Loads secrets into h2.
 */
@Slf4j
@Singleton
@Interceptors({PerformanceLogInterceptor.class, HeapUsageInterceptor.class})
public class LoadSecrets {

    private final FtpConnection.Parameters customersFile;

    private final FtpConnection.Parameters suppliersFile;

    @PersistenceContext(unitName = "local")
    private EntityManager em;

    @Inject
    private SecretsRepository secretsRepository;

    @Inject
    private CustomerRepository customerRepository;

    @Inject
    private SupplierRepository supplierRepository;

    public LoadSecrets() {
        String CUSTOMERS_PW_URL_FILE = System.getenv()
                                             .getOrDefault("CUSTOMERS_PW_URL_FILE", "<customers.pw>");

        customersFile = new FtpConnection.Parameters(MigrationApplication.FTP_URL_HOST, CUSTOMERS_PW_URL_FILE, null, MigrationApplication.FTP_URL_USERNAME, MigrationApplication.FTP_URL_PASSWORD);

        String SUPPLIERS_PW_URL_FILE = System.getenv()
                                             .getOrDefault("SUPPLIERS_PW_URL_FILE", "<suppliers.pw>");

        suppliersFile = new FtpConnection.Parameters(MigrationApplication.FTP_URL_HOST, SUPPLIERS_PW_URL_FILE, null, MigrationApplication.FTP_URL_USERNAME, MigrationApplication.FTP_URL_PASSWORD);
    }

    public void run() {
        if (secretsRepository.isEmpty()) {
            saveCustomerSecrets();

            saveSupplierSecrets();
        }
    }

    private void saveCustomerSecrets() {
        List<Indexed<Customer>> customers = customerRepository.rootOrganizations();
        List<Secret> customerSecrets = lines(customersFile).stream()
                                                           .map(Secret::of)
                                                           .collect(toList());

        for (int j = 0; j < customers.size(); j++) {
            Customer customer = customers.get(j)
                                         .value();
            customer.setSecret(customerSecrets.get(j));
            em.merge(customer);
        }
    }

    private void saveSupplierSecrets() {
        List<Supplier> suppliers = supplierRepository.suppliers();
        List<Secret> supplierSecrets = lines(suppliersFile).stream()
                                                           .map(Secret::of)
                                                           .collect(toList());

        for (int j = 0; j < suppliers.size(); j++) {
            Supplier supplier = suppliers.get(j);
            supplier.setSecret(supplierSecrets.get(j));
            em.merge(supplier);
        }
    }

    private List<String> lines(FtpConnection.Parameters parameters) {
        try (BufferedReader r = new BufferedReader(new InputStreamReader(FtpConnection.of(parameters)))) {
            return r.lines()
                    .collect(toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

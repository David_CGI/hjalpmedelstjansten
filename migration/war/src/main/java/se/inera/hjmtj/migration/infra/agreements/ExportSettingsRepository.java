/*
 *
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 *  Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.agreements;

import lombok.extern.slf4j.Slf4j;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjmtj.migration.domain.HeapUsageInterceptor;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.domain.model.agreements.gp.ExportSetting;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static java.util.concurrent.TimeUnit.HOURS;

@Slf4j
@Stateless
@Interceptors({PerformanceLogInterceptor.class, HeapUsageInterceptor.class})
@TransactionTimeout(unit = HOURS, value = 5)
public class ExportSettingsRepository {

    @PersistenceContext(unitName = "local")
    private EntityManager em;

    public List<ExportSetting> findByCustomerGLN(String gln) {
        return em.createNamedQuery(ExportSetting.FIND_BY_CUSTOMER_GLN, ExportSetting.class)
                 .setParameter("gln", gln)
                 .setHint("org.hibernate.readOnly", true)
                 .getResultList();
    }
}

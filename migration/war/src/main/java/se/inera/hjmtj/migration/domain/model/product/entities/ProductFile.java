/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.product.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.io.FilenameUtils;
import se.inera.hjmtj.migration.domain.FileExtension;

import javax.persistence.Embeddable;
import javax.persistence.PersistenceUnit;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

@PersistenceUnit(unitName = "local")
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ProductFile {

    public static final String JUNK = "cache_vgregion";

    @XmlElement(name = "OriginalContentUrl")
    private String originalContentUrl;

    @XmlElement(name = "OriginalFileName")
    private String originalFileName;

    @XmlElement(name = "SourceUrl")
    private String sourceURL;

    @XmlElement(name = "ContentType")
    private String contentType;

    private String fileName;

    private String fileExtension;

    void afterUnmarshal(Unmarshaller u, Object parent) {
        // fileName
        if (originalFileName != null && originalFileName.trim()
                                                        .length() > 0) {
            this.fileName = FilenameUtils.getName(originalFileName);
        }
        if (sourceURL != null && fileName == null) {
            fileName = FilenameUtils.getName(sourceURL);

            // Handle cases where sourceURL is dynamic and filename is missing e.g.
            // https://webcache.visuera.com/cache_vgregion?ref=c%2c33218930-e6de-4097-906b-a54e01031db5%2cBlobData&u=8D32EBBB6341300
            // or
            // http://www.jobstorder.se/_includes/GetFile.cfm?Id=36a16a2505369e0c922b6ea7a23a56d2&amp;FileVariant=original&amp;CoreFile=1
            if (sourceURL.contains("?")) {
                Map<String, String> queryMap = getQueryMap(originalContentUrl.trim());
                String identifier = queryMap.get("ref");
                try {
                    identifier = URLDecoder.decode(identifier, "UTF-8");
                    String[] array = identifier.split(",");
                    if (array.length == 3) {
                        String value = array[1];
                        this.fileName = value != null ? value.trim() : fileName;
                    }
                } catch (UnsupportedEncodingException e) {
                    // Nothing to do
                }
            }
        }

        if (fileName == null || fileName.trim()
                                        .isEmpty()) {
            Map<String, String> queryMap = getQueryMap(originalContentUrl.trim());
            String identifier = queryMap.get("ref");
            try {
                identifier = URLDecoder.decode(identifier, "UTF-8");
                String[] array = identifier.split(",");
                if (array.length == 3) {
                    String value = array[1];
                    this.fileName = value != null ? value.trim() : fileName;
                }
            } catch (UnsupportedEncodingException e) {
                // Nothing to do
            }
        }

        // Handle cases like remote.jpg.ashx
        if (fileName != null) {
            for (int j = 0; j < 3; j++) {
                fileName = FilenameUtils.removeExtension(fileName);
            }
        }

        this.fileName = shorten(fileName);

        // Extension
        FileExtension fileExtension = null;

        if (contentType != null) {
            fileExtension = FileExtension.fromContentType(contentType);
        }
        if (originalFileName != null && fileExtension == null) {
            fileExtension = FileExtension.fromFileName(originalFileName);
        }
        if (fileExtension != null) {
            this.fileExtension = fileExtension.getFileExtension();
        }

        // sanity check
        if (this.fileExtension != null && this.fileExtension.length() > 5) {
            throw new IllegalArgumentException("File extension is likely wrong: " + fileExtension);
        }

        // In order to prevent problems with S3 we only allow alphanumeric characters, underscore, hyphen and space
        if (this.fileName != null) {
            this.fileName = fileName.replaceAll("[^\\p{IsAlphabetic}^\\p{IsDigit}-_\\s]", "");
        }
    }

    public static Map<String, String> getQueryMap(String query) {
        String[] params = query.split("[?&]");
        Map<String, String> map = new HashMap<String, String>();
        for (String param : params) {
            String[] split = param.split("=");

            if (split.length >= 2) {
                String name = split[0];
                String value = split[1];
                map.put(name, value);
            }
        }
        return map;
    }

    private String shorten(String fileName) {
        // The maximum length of URL is 255
        // A typical URL is https://test-cdn.hjm.aws.ineratest.org/ff0c4eda-7757-33cc-877d-75ae8bf6dfcf/03f80ec8-0cac-3ad1-a67f-3d134f3c7966/
        // i.e. 114 characters
        // we limit filename to 40 characters for good measure
        return fileName != null ? fileName.substring(0, Math.min(fileName.length(), 40)) : null;
    }

    public String fullFileName() {
        if (fileName != null && fileExtension != null) {
            return fileName + fileExtension;
        }
        return fileName;
    }

}

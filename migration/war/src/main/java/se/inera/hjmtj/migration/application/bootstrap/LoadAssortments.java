/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.application.bootstrap;

import lombok.extern.slf4j.Slf4j;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjmtj.migration.domain.HeapUsageInterceptor;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.domain.model.assortment.Assortment;
import se.inera.hjmtj.migration.domain.model.assortment.AssortmentCountyMunicipality;
import se.inera.hjmtj.migration.domain.model.assortment.AssortmentOrganization;
import se.inera.hjmtj.migration.domain.model.assortment.AssortmentProduct;
import se.inera.hjmtj.migration.domain.model.product.entities.Product;
import se.inera.hjmtj.migration.domain.xml.ElementContextComparator;
import se.inera.hjmtj.migration.domain.xml.XMLFile;
import se.inera.hjmtj.migration.infra.assortments.AssortmentsRepository;
import se.inera.hjmtj.migration.infra.io.BatchPersistence;
import se.inera.hjmtj.migration.infra.organizations.CustomerRepository;
import se.inera.hjmtj.migration.infra.organizations.SupplierRepository;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.concurrent.TimeUnit.MINUTES;

/**
 * Loads assortment into H2.
 */
@Slf4j
@Singleton
@TransactionTimeout(unit = MINUTES, value = 30)
@Interceptors({PerformanceLogInterceptor.class, HeapUsageInterceptor.class})
public class LoadAssortments {

    public static final ElementContextComparator PERSIST_ORDER = new ElementContextComparator(
            AssortmentCountyMunicipality.class,
            AssortmentOrganization.class,
            AssortmentProduct.class,
            Assortment.class);

    private static final String ASSORTMENT_URL_FILE = System.getenv()
                                                            .getOrDefault("ASSORTMENT_URL_FILE", "<file>");

    @PersistenceContext(unitName = "local")
    private EntityManager em;

    @Inject
    private BatchPersistence batchPersistence;

    @Inject
    private AssortmentsRepository assortmentsRepository;

    @Inject
    private CustomerRepository customerRepository;

    @Inject
    private SupplierRepository supplierRepository;

    public void run() {
        if (assortmentsRepository.isEmpty()) {
            assortmentsRepository.clear();

            XMLFile xmlFile = new XMLFile(PERSIST_ORDER);

            List<Object> xmlObjects = xmlFile.loadFromFTP(ASSORTMENT_URL_FILE)
                                             .stream()
                                             .map(this::decorate)
                                             .collect(Collectors.toList());

            batchPersistence.save(xmlObjects, em);
        }
    }

    private Object decorate(Object xmlObject) {
        if (xmlObject instanceof AssortmentProduct) {
            setAssistiveProduct((AssortmentProduct) xmlObject);
        } else if (xmlObject instanceof AssortmentOrganization) {
            AssortmentOrganization organization = (AssortmentOrganization) xmlObject;

            organization.setCustomer(customerRepository.getCustomer(organization));
            organization.setSupplier(supplierRepository.getSupplier(organization));
        }

        return xmlObject;
    }

    private void setAssistiveProduct(AssortmentProduct agreementProductBase) {
        Product assistiveProduct = em.find(Product.class, Long.valueOf(agreementProductBase.getVgrProdNo()));
        agreementProductBase.setArticle(assistiveProduct);
    }

}

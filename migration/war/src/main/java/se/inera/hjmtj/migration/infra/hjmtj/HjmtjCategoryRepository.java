/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.hjmtj;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.model.api.CategoryAPI;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjmtj.migration.domain.model.categories.Category;
import se.inera.hjmtj.migration.domain.model.categories.CategorySpecificProperty;
import se.inera.hjmtj.migration.domain.model.categories.CategorySpecificPropertyListValue;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductCategory;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductCategoryPath;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

@Slf4j
@Stateless
public class HjmtjCategoryRepository {

    private final List<Long> leafCategoryIds = new LinkedList<>();

    private final Map<String, CategoryAPI> codeToCategory = new HashMap<>();

    private final Map<ProductCategoryPath, Optional<CategoryAPI>> categoryCache = new ConcurrentHashMap<>();

    @PersistenceContext(unitName = "HjmtjPU")
    private EntityManager em;

    private CategoryAPI settingId;

    private CategoryAPI sparePartId;

    private CategoryAPI accessoryId;

    private CategoryAPI serviceId;

    public void initializeCache() {
        try {
            this.settingId = findNonISOByArticleType(Article.Type.I, "Inställningar");
            this.sparePartId = findNonISOByArticleType(Article.Type.R, "Reservdelar");
            this.accessoryId = findNonISOByArticleType(Article.Type.T, "Tillbehör");
            this.serviceId = findNonISOByArticleType(Article.Type.Tj, "Tjänster");

            List<BigInteger> resultList = em.createNativeQuery("SELECT c.uniqueId\n" +
                    "FROM Category c\n" +
                    "WHERE c.uniqueId NOT IN (SELECT DISTINCT o.parentCategoryId\n" +
                    "                         FROM Category o\n" +
                    "                         WHERE o.parentCategoryId IS NOT NULL)" +
                    "\n")
                                            .getResultList();
            resultList
                    .stream()
                    .map(BigInteger.class::cast)
                    .map(BigInteger::longValue)
                    .forEach(leafCategoryIds::add);


            List<Object[]> rs2 = em.createNativeQuery("SELECT c.uniqueId, c.articleType, c.code, c.name FROM Category c WHERE c.code IS NOT NULL ")
                                   .getResultList();
            codeToCategory.putAll(rs2.stream()
                                     .map(Object[].class::cast)
                                     .map(this::fromArray)
                                     .collect(toMap(CategoryAPI::getCode, Function.identity())));
        } catch (Exception t) {
            log.error("{}", t);
        }
    }

    private CategoryAPI findNonISOByArticleType(Article.Type articleType, String name) {
        Query query = em.createNativeQuery("SELECT c.uniqueId, c.articleType, c.code, c.name FROM Category c WHERE c.articleType = :articleType AND name = :name")
                        .setParameter("articleType", articleType.toString())
                        .setParameter("name", name)
                        .setMaxResults(1);

        Stream<Object[]> stream = query.getResultList()
                                       .stream()
                                       .map(Object[].class::cast);

        return stream.map(this::fromArray)
                     .findFirst()
                     .orElse(null);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public CategoryAPI findBy(ProductCategoryPath xmlCategories) {
        ProductCategory leaf = xmlCategories.leaf();

        if(leaf == null){
            return null;
        }

        log.trace("{}", leaf);

        if (categoryCache.containsKey(xmlCategories)) {
            return categoryCache.get(xmlCategories)
                                .orElse(null);
        }

        Query query;
        if (leaf.getCode() != null) {
            CategoryAPI categoryAPI = codeToCategory.get(leaf.getCode());

            return categoryAPI.getName()
                              .equals(leaf.getName()) ? categoryAPI : null;
        } else {
            ProductCategory parent = leaf.getParent();

            query = em.createNativeQuery("SELECT c.uniqueId, c.articleType, c.code, c.name FROM Category c " +
                    "JOIN Category p ON c.parentCategoryId = p.uniqueId AND p.code = :parentCode " +
                    "WHERE lower(c.name) = :name ")
                      .setParameter("name", leaf.getName().toLowerCase())
                      .setParameter("parentCode", parent.getCode())
                      .setMaxResults(1);
        }

        Stream<Object[]> stream = query.getResultList()
                                       .stream()
                                       .map(Object[].class::cast);

        CategoryAPI categoryAPI = stream.map(this::fromArray)
                                        .findFirst()
                                        .orElse(null);
        categoryCache.put(xmlCategories, Optional.ofNullable(categoryAPI));
        return categoryAPI;
    }

    private CategoryAPI fromArray(Object[] arr) {
        BigInteger id = (BigInteger) arr[0];
        Objects.requireNonNull(id, "id must be non-null");

        String type = (String) arr[1];
        Article.Type articleType = type != null ? Article.Type.valueOf(type) : Article.Type.H;

        String code = (String) arr[2];

        String name = (String) arr[3];

        CategoryAPI categoryAPI = new CategoryAPI();
        categoryAPI.setId(id.longValue());
        categoryAPI.setArticleType(articleType);
        categoryAPI.setCode(code);
        categoryAPI.setName(name);

        return categoryAPI;
    }

    public CategoryAPI settingId() {
        return settingId;
    }

    public CategoryAPI sparePartId() {
        return sparePartId;
    }

    public CategoryAPI accessoryId() {
        return accessoryId;
    }

    public CategoryAPI serviceId() {
        return serviceId;
    }

    public CategoryAPI findByCode(String code) {
        log.debug("findByCode({})", code);
        return codeToCategory.get(code);

    }

    public boolean isLeaf(CategoryAPI category) {
        log.debug("isLeaf({})", category);
        return leafCategoryIds.contains(category.getId());
    }

    public int add(Category category) {
        return em.createNativeQuery("INSERT INTO hjmtj.Category (uniqueId, ARTICLETYPE, CODE, DESCRIPTION, NAME, PARENTCATEGORYID) " +
                "VALUES (:uniqueId, :articleType, :code, :description, :name, :parentCategoryId)")
                 .setParameter("uniqueId", category.getUniqueId())
                 .setParameter("articleType", category.getArticleType())
                 .setParameter("code", category.getCode())
                 .setParameter("description", category.getDescription())
                 .setParameter("name", category.getName())
                 .setParameter("parentCategoryId", category.getParent() == null ? null : category.getParent()
                                                                                                 .getUniqueId())
                 .executeUpdate();
    }

    public int add(CategorySpecificProperty categorySpecificProperty) {
        return em.createNativeQuery("INSERT INTO hjmtj.CategorySpecificProperty (uniqueId, DESCRIPTION, NAME, TYPE, CATEGORYID) " +
                "VALUES (:uniqueId, :description, :name, :type, :categoryId)")
                 .setParameter("uniqueId", categorySpecificProperty.getUniqueId())
                 .setParameter("description", categorySpecificProperty.getDescription())
                 .setParameter("name", categorySpecificProperty.getName())
                 .setParameter("type", categorySpecificProperty.getType()
                                                               .toString())
                 .setParameter("categoryId", categorySpecificProperty.getCategory()
                                                                     .getUniqueId())
                 .executeUpdate();
    }

    public int add(CategorySpecificPropertyListValue categorySpecificPropertyListValue) {
        return em.createNativeQuery("INSERT INTO hjmtj.CategorySpecificPropertyListValue " +
                "(uniqueId, code, value, categorySpecificPropertyId)" +
                "VALUES (:uniqueId, :code, :value, :categorySpecificPropertyId)")
                 .setParameter("uniqueId", categorySpecificPropertyListValue.getUniqueId())
                 .setParameter("code", categorySpecificPropertyListValue.getCode())
                 .setParameter("value", categorySpecificPropertyListValue.getValue())
                 .setParameter("categorySpecificPropertyId", categorySpecificPropertyListValue.getCategorySpecificProperty()
                                                                                              .getUniqueId())
                 .executeUpdate();
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    private long size() {
        return ((BigInteger) em.createNativeQuery("SELECT COUNT(*) FROM hjmtj.Category ")
                               .setMaxResults(1)
                               .getSingleResult()).longValue();
    }

}

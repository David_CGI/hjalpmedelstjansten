/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.application.bootstrap;

import lombok.Builder;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjmtj.migration.domain.HeapUsageInterceptor;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementHead;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementRowBase;
import se.inera.hjmtj.migration.domain.model.agreements.gp.GeneralPricelist;
import se.inera.hjmtj.migration.domain.model.agreements.gp.GeneralPricelistCalculatedFrom;
import se.inera.hjmtj.migration.domain.model.agreements.gp.GeneralPricelistOrganization;
import se.inera.hjmtj.migration.domain.model.agreements.gp.GeneralPricelistParty;
import se.inera.hjmtj.migration.domain.model.agreements.gp.GeneralPricelistProduct;
import se.inera.hjmtj.migration.domain.model.agreements.gp.GeneralPricelistStatus;
import se.inera.hjmtj.migration.domain.model.agreements.gp.GeneralPricelistWarrantyUnit;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MessageTemplate;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.domain.model.product.entities.Product;
import se.inera.hjmtj.migration.domain.xml.ElementContextComparator;
import se.inera.hjmtj.migration.domain.xml.XMLFile;
import se.inera.hjmtj.migration.infra.agreements.GeneralPricelistRepository;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;
import se.inera.hjmtj.migration.infra.io.BatchPersistence;
import se.inera.hjmtj.migration.infra.organizations.SupplierRepository;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

/**
 * Loads agreements file into H2.
 */
@Slf4j
@Singleton
@TransactionTimeout(unit = TimeUnit.MINUTES, value = 30)
@Interceptors({PerformanceLogInterceptor.class, HeapUsageInterceptor.class})
public class LoadGeneralPricelist {

    public static final ElementContextComparator PERSIST_ORDER = new ElementContextComparator(
            GeneralPricelistWarrantyUnit.class,
            GeneralPricelistCalculatedFrom.class,
            GeneralPricelistOrganization.class,
            GeneralPricelistStatus.class,
            GeneralPricelistProduct.class,
            GeneralPricelist.class);

    private final HashMap<String, GPIdentifier> supplierNameToAcceptedGPIdentifier = new HashMap<>();

    private final String GP_URL_FILE = System.getenv()
                                             .getOrDefault("GP_URL_FILE", "<file>");

    @Inject
    private GeneralPricelistRepository generalPricelistRepository;

    @PersistenceContext(unitName = "local")
    private EntityManager em;

    @Inject
    private BatchPersistence batchPersistence;

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    @Inject
    private SupplierRepository supplierRepository;


    public LoadGeneralPricelist() {
        // Since some suppliers have > 1 GP we must select only one to migrate
        supplierNameToAcceptedGPIdentifier.put("Insyn Scandinavia AB", new GPIdentifier.GPIdentifierBuilder()
                .title("Generell Prislista")
                .number("2019")
                .build());
        supplierNameToAcceptedGPIdentifier.put("Lidol AB", new GPIdentifier.GPIdentifierBuilder()
                .title(null)
                .number("GP")
                .build());
        supplierNameToAcceptedGPIdentifier.put("Multilens AB", new GPIdentifier.GPIdentifierBuilder()
                .title(null)
                .number("GP")
                .build());
        supplierNameToAcceptedGPIdentifier.put("Posifon AB", new GPIdentifier.GPIdentifierBuilder()
                .title("GP Careousel och MiniTell")
                .number("GP")
                .build());
        supplierNameToAcceptedGPIdentifier.put("Bard Norden AB", new GPIdentifier.GPIdentifierBuilder()
                .title(null)
                .number("GP")
                .build());
        supplierNameToAcceptedGPIdentifier.put("Oticon AB", new GPIdentifier.GPIdentifierBuilder()
                .title(null)
                .number("GP")
                .build());
        supplierNameToAcceptedGPIdentifier.put("Hjälpmedelsexperten Sverige AB", new GPIdentifier.GPIdentifierBuilder()
                .title("Stricker Drivaggregat")
                .number("002")
                .build());
        supplierNameToAcceptedGPIdentifier.put("GN Hearing Sverige AB", new GPIdentifier.GPIdentifierBuilder()
                .title("Generell")
                .number("Generell")
                .build());
        supplierNameToAcceptedGPIdentifier.put("Hearu AB", new GPIdentifier.GPIdentifierBuilder()
                .title("GP")
                .number("Generell Prislista")
                .build());
        supplierNameToAcceptedGPIdentifier.put("WWT Care AB", new GPIdentifier.GPIdentifierBuilder()
                .title(null)
                .number("2010-01")
                .build());
        supplierNameToAcceptedGPIdentifier.put("Caretec i Forserum AB", new GPIdentifier.GPIdentifierBuilder()
                .title("GP")
                .number("GP")
                .build());
        supplierNameToAcceptedGPIdentifier.put("Sivantos A/S", new GPIdentifier.GPIdentifierBuilder()
                .title("Generell prislista")
                .number("GP")
                .build());
        supplierNameToAcceptedGPIdentifier.put("Wolturnus A/S", new GPIdentifier.GPIdentifierBuilder()
                .title("Generell Prislista")
                .number("GP")
                .build());
        supplierNameToAcceptedGPIdentifier.put("Euforia", new GPIdentifier.GPIdentifierBuilder()
                .title(null)
                .number("GP")
                .build());
    }

    public void run() {
        if (generalPricelistRepository.isEmpty()) {
            if (AgreementRowBase.USE_GENERATED_VALUE_FOR_PRICE) {
                log.info("Generated prices will be committed");
            } else {
                log.warn("Real prices will be committed - if this is unintentional please abort before transaction commits");
            }

            XMLFile xmlFile = new XMLFile(PERSIST_ORDER);

            List<Object> xmlObjects = xmlFile.loadFromFTP(GP_URL_FILE)
                                             .stream()
                                             .map(this::decorate)
                                             .collect(Collectors.toList());

            xmlObjects = xmlObjects.stream()
                                   .filter(this::accept)
                                   .collect(toList());

            batchPersistence.save(xmlObjects, em);
        }
    }

    private Object decorate(Object xmlObject) {
        if (xmlObject instanceof GeneralPricelistOrganization) {
            GeneralPricelistOrganization organization = (GeneralPricelistOrganization) xmlObject;

            organization.setSupplier(supplierRepository.getSupplier(organization));
        } else if (xmlObject instanceof GeneralPricelistProduct) {
            setArticle((GeneralPricelistProduct) xmlObject);
        }

        return xmlObject;
    }

    private boolean accept(Object object) {
        if (object instanceof GeneralPricelist) {
            GeneralPricelist generalPricelist = (GeneralPricelist) object;

            GeneralPricelistParty seller = generalPricelist.getSeller();
            GeneralPricelistOrganization agreementOrganization = seller
                    .getAgreementOrganization();
            Supplier supplier = agreementOrganization
                    .getSupplier();

            if (supplier != null) {
                String supplierName = supplier.getName();

                if (supplierName.equalsIgnoreCase("Wolturnus A/S") &&
                        generalPricelist.getRows()
                                        .size() == 1) {
                    // Special handling of duplicate GP on Wolturnus A/S
                    return false;
                }
                if (supplierNameToAcceptedGPIdentifier.containsKey(supplierName)) {
                    GPIdentifier gpIdentifier = supplierNameToAcceptedGPIdentifier.get(supplierName);
                    AgreementHead head = generalPricelist.getHead();
                    String title = head.getTitle();
                    String gpNumber = head
                            .getSellerAgreementNo();
                    GPIdentifier currentGPIdentifier = new GPIdentifier.GPIdentifierBuilder().title(title)
                                                                                             .number(gpNumber)
                                                                                             .build();
                    boolean equals = gpIdentifier.equals(currentGPIdentifier);

                    if (equals) {
                        MigrationError migrationError = new MigrationError();
                        migrationError.setClassifier(Classifier.GP);
                        migrationError.setOrganizationName(supplierName);
                        migrationError.setSeverity(MigrationError.Severity.WARNING);
                        String message = "Supplier has multiple GP. Will only migrate " + gpIdentifier.toString();
                        migrationError.setMessage(MessageTemplate.SUPPLIER_HAS_MULTIPLE_GP, gpIdentifier.toString());
                        migrationErrorRepository.add(migrationError);
                        log.warn(supplierName + ": " + message);
                    }

                    return equals;
                }
            }
        }

        return true;
    }

    private void setArticle(GeneralPricelistProduct agreementArticle) {
        Product article = em.find(Product.class, Long.valueOf(agreementArticle.getVgrProdNo()));
        agreementArticle.setArticle(article);
    }

    @Builder
    @Value
    private static class GPIdentifier {

        private String title;

        private String number;

        @Override
        public String toString() {
            return "GP title = " + title + ", number = " + number;
        }
    }

}

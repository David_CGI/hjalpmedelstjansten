/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.application.services;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistController;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjmtj.migration.domain.MigrationLock;
import se.inera.hjmtj.migration.domain.SupplierGLNCatalogueUniqueNumber;
import se.inera.hjmtj.migration.domain.csv.Utf8Csv;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementRowBase;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementStatusBase;
import se.inera.hjmtj.migration.domain.model.agreements.AgreementVersion;
import se.inera.hjmtj.migration.domain.model.agreements.builders.AgreementBaseMapper;
import se.inera.hjmtj.migration.domain.model.agreements.builders.GeneralPricelistAPIWithMultiplePricelists;
import se.inera.hjmtj.migration.domain.model.agreements.builders.PriceListRowAPIBuilder;
import se.inera.hjmtj.migration.domain.model.agreements.gp.GeneralPricelist;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.domain.model.product.entities.Product;
import se.inera.hjmtj.migration.infra.agreements.GeneralPricelistRepository;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjGeneralPricelistRepository;
import se.inera.hjmtj.migration.infra.organizations.SupplierRepository;
import se.inera.hjmtj.migration.infra.products.ProductRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@ApplicationScoped
@Path("/gps")
public class GeneralPricelistsService implements ReportServiceBase {

    @Context
    private HttpServletRequest request;

    @Inject
    private MigrationLock migrationLock;

    @Inject
    private GeneralPricelistRepository generalPricelistRepository;

    @Inject
    private GeneralPricelistController generalPricelistController;

    @Inject
    private OrganizationController organizationController;


    @Inject
    private HjmtjGeneralPricelistRepository hjmtjGeneralPricelistRepository;

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    @Inject
    private AgreementBaseMapper mapper;

    @Inject
    private SupplierRepository supplierRepository;

    @Inject
    private ProductRepository productRepository;

    @GET
    @Path("/gp_rows.csv")
    @Produces("text/csv;charset=utf-8")
    public byte[] rowsCsv() {
        return generateReport("gp_rows.csv", this::buildRowsCsv);
    }

    private byte[] buildRowsCsv() {
        migrationLock.tryAcquire();
        StringBuilder sb = new StringBuilder();
        sb.append("Leverantör;GP-nummer;Versionslistor;Är ISO9999?;Är artikel utan typ?;Avtalsnummer på rad;Giltigt från på rad;Katalogunikt nummer på rad;Artikelnummer;Aktiv?;Migrerades OK?\n");

        try {
            Set<SupplierGLNCatalogueUniqueNumber> result = hjmtjGeneralPricelistRepository.result();

            for (Supplier supplier : supplierRepository.suppliers()) {
                GeneralPricelist agreement = generalPricelistRepository.findBy(supplier);

                if (agreement != null && agreement.getRows() != null) {
                    for (AgreementRowBase row : agreement.getRows()) {
                        if (supplier != null) {
                            sb.append(supplier.getName());
                            sb.append(";");

                            sb.append(agreement.getHead()
                                               .getSellerAgreementNo());
                            sb.append(";");

                            sb.append(agreement.getHead()
                                               .getAgreementVersionList()
                                               .stream()
                                               .map(this::stringValue)
                                               .collect(Collectors.joining(", ")));
                            sb.append(";");

                            Product article = row.getAgreementArticle() != null ? productRepository.findByVgrProductNumber(row.getAgreementArticle()
                                                                                                                              .getVgrProdNo()) : null;

                            sb.append(booleanString(article != null && article.getXmlCategories()
                                                                              .isIso9999()));
                            sb.append(";");

                            sb.append(booleanString(article != null && article.isNoArticleType()));
                            sb.append(";");

                            sb.append(row.getSupplierPriceListNumber());
                            sb.append(";");

                            sb.append(row.getValidFrom());
                            sb.append(";");

                            sb.append(row.getAgreementArticle()
                                         .getVgrProdNo());
                            sb.append(";");

                            sb.append(article != null ? article.getSupplierProductNumber() : "");
                            sb.append(";");

                            AgreementStatusBase rowStatus = row.getStatus();
                            String status = rowStatus != null ? rowStatus.getValue() : "";
                            sb.append(booleanString(status.equalsIgnoreCase(PriceListRowAPIBuilder.VISUERA_AKTIV)));
                            sb.append(";");

                            sb.append(booleanString(result.contains(new SupplierGLNCatalogueUniqueNumber(supplier.getGln(), Long.valueOf(row.getAgreementArticle()
                                                                                                                                            .getVgrProdNo())))));
                            sb.append(";");

                            sb.append("\n");
                        }
                    }
                }
            }

            return new Utf8Csv(sb.toString()).toBytes();
        } finally {
            migrationLock.release();
        }
    }

    private String stringValue(AgreementVersion agreementVersion) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        sb.append("Nummer = ");
        sb.append(agreementVersion.getVersionNumber() != null ? agreementVersion.getVersionNumber() : "NULL");
        sb.append(", ");
        sb.append("Giltig från = ");
        sb.append(agreementVersion.getValidFrom() != null ? agreementVersion.getValidFrom() : "NULL");
        sb.append("]");
        return sb.toString();
    }

    @POST
    @Path("/")
    public void migrateGeneralPricelists(List<SuppliersService.SupplierOrganizationAPI> organizations) {
        log.info("Started migrating general pricelists...");

        for (SuppliersService.SupplierOrganizationAPI entry : organizations) {
            try {
                Supplier supplier = entry.getSupplier();
                GeneralPricelist gp = generalPricelistRepository.findBy(supplier);
                GeneralPricelistAPIWithMultiplePricelists generalPricelistAPI = gp != null ? mapper.toGeneralPricelist(gp, entry.getOrganizationAPI())
                                                                                                   .orElse(null) : null;

                if (generalPricelistAPI != null) {
                    hjmtjGeneralPricelistRepository.save(generalPricelistAPI, generalPricelistAPI.getCreated());
                }
            } catch (Exception e) {
                MigrationError migrationError = new MigrationError();
                migrationError.setSeverity(MigrationError.Severity.ERROR);
                migrationError.setClassifier(Classifier.GP);
                migrationError.setOrganizationName(entry.getOrganizationAPI()
                                                        .getOrganizationName());
                migrationError.setMessage(e.getMessage());
                migrationErrorRepository.add(migrationError);
            }
        }

        log.info("Done migrating general pricelists!");
    }

    @GET
    @Path("/gp.csv")
    @Produces("text/csv;charset=utf-8")
    public byte[] gpCsv() {
        return generateReport("gp.csv", this::buildGPReport);
    }

    private byte[] buildGPReport() {
        migrationLock.tryAcquire();

        try {
            List<GeneralPricelist> gp = generalPricelistRepository.values();

            StringBuilder sb = new StringBuilder();
            sb.append("Leverantör;Leverantör matchas?;Titel;GP-nummer;Migrerades OK?\n");

            for (GeneralPricelist generalPricelist : gp) {
                Supplier supplier = generalPricelist.getSeller()
                                                    .getAgreementOrganization()
                                                    .getSupplier();

                sb.append(supplier != null ? supplier.getName() : generalPricelist.getSeller()
                                                                                  .getAgreementOrganization()
                                                                                  .getName());
                sb.append(";");

                sb.append(booleanString(supplier != null));
                sb.append(";");

                sb.append(generalPricelist.getHead()
                                          .getTitle());
                sb.append(";");

                sb.append(generalPricelist.getHead()
                                          .getSellerAgreementNo());
                sb.append(";");

                se.inera.hjalpmedelstjansten.model.entity.GeneralPricelist hjmtjGp = null;
                if (supplier != null) {
                    List<Organization> organizations = organizationController.findByGlnAndType(supplier.getGln(), Organization.OrganizationType.SUPPLIER);
                    if (!organizations.isEmpty()) {
                        Organization organization = organizations.get(0);
                        hjmtjGp = generalPricelistController.getGeneralPricelist(organization.getUniqueId());
                    }
                }
                sb.append(booleanString(hjmtjGp != null));

                sb.append("\n");
            }

            return new Utf8Csv(sb.toString()).toBytes();
        } finally {
            migrationLock.release();
        }
    }

}

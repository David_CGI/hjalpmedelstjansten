/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.hjmtj;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCEDirectiveAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVGuaranteeUnitAPI;

import javax.annotation.PostConstruct;
import javax.ejb.DependsOn;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Singleton
public class HjmtjCvCeDirectiveRepository {

    private final Map<String, CVCEDirectiveAPI> nameToCvCeDirectiveAPI = new HashMap<>();

    @PersistenceContext(unitName = "HjmtjPU")
    private EntityManager em;

    public void initializeCache() {
        try {
            List<Object[]> resultList = em.createNativeQuery("SELECT d.uniqueId, d.name FROM hjmtj.CVCEDirective d")
                                          .getResultList();

            nameToCvCeDirectiveAPI.putAll(resultList
                    .stream()
                    .map(this::map)
                    .collect(Collectors.toMap(CVCEDirectiveAPI::getName, Function.identity())));
        } catch (Exception t) {
            log.error("{}", t);
        }
    }

    private CVCEDirectiveAPI map(Object[] array) {
        BigInteger uniqueId = (BigInteger) array[0];

        CVCEDirectiveAPI cvceDirectiveAPI = new CVCEDirectiveAPI();
        cvceDirectiveAPI.setId(uniqueId.longValue());
        cvceDirectiveAPI.setName((String) array[1]);

        return cvceDirectiveAPI;
    }

    public CVCEDirectiveAPI findByName(String name) {
        return nameToCvCeDirectiveAPI.get(name);
    }
}

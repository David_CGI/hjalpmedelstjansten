/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.agreements;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Inheritance;
import javax.persistence.MappedSuperclass;
import javax.persistence.PersistenceUnit;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlTransient;

import static javax.persistence.InheritanceType.TABLE_PER_CLASS;


@PersistenceUnit(unitName = "local")
@Inheritance(strategy = TABLE_PER_CLASS)
@MappedSuperclass
@Table(indexes = {
        @Index(name = "AgreementWarrantyUnitCodeIndex", columnList = "code"),
})
@XmlTransient
@RequiredArgsConstructor
@Getter
@Setter
public abstract class AgreementWarrantyUnitBase {

    @Id
    private Long id;

    @XmlElement(name = "Code")
    private String code;

    @XmlElement(name = "Value")
    private String value;

    @XmlAttribute(name = "id")
    @XmlID
    public String getIdAsString() {
        return String.valueOf(id);
    }

    public void setIdAsString(String xmlId) {
        id = Long.parseLong(xmlId);
    }
}

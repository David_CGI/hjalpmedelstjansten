/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.result;

import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.infra.errors.MigrationErrorRepository;
import se.inera.hjmtj.migration.infra.hjmtj.HjmtjCatalogUniqueNumberRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Stateless
@Interceptors({PerformanceLogInterceptor.class})
public class AssistiveProductMigrationResultRepository {

    @Inject
    private MigrationErrorRepository migrationErrorRepository;

    @Inject
    private HjmtjCatalogUniqueNumberRepository hjmtjCatalogUniqueNumberRepository;

    @PersistenceContext(unitName = "HjmtjPU")
    private EntityManager em;

    public Stream<AssistiveProductMigrationResult> stream(List<AssistiveProductReport> reports) {
        return get(reports).stream();
    }

    public List<AssistiveProductMigrationResult> get(List<AssistiveProductReport> reports) {
        Set<Long> catalogueUniqueNumbers = hjmtjCatalogUniqueNumberRepository.catalogueUniqueNumbers();
        List<AssistiveProductMigrationResult> results = new LinkedList<>();
        Set<Long> failedIds = reports.stream()
                                     .filter(report -> !catalogueUniqueNumbers.contains(report.getCatalogueUniqueNumber()))
                                     .map(AssistiveProductReport::getId)
                                     .collect(Collectors.toSet());

        for (AssistiveProductReport report : reports) {
            boolean success = catalogueUniqueNumbers.contains(report.getCatalogueUniqueNumber());
            boolean cascadeFailure = report.getRequiresIds()
                                           .stream()
                                           .anyMatch(failedIds::contains);
            String remarks = migrationErrorRepository.get(MigrationError.Severity.WARNING, report.getCatalogueUniqueNumber())
                                                     .stream()
                                                     .map(MigrationError::getMessage)
                                                     .collect(Collectors.joining(","));
            List<MigrationError> migrationErrors = migrationErrorRepository.get(MigrationError.Severity.ERROR, report.getCatalogueUniqueNumber());
            String errors = migrationErrors
                    .stream()
                    .map(MigrationError::getMessage)
                    .collect(Collectors.joining(","));
            String errorType = migrationErrors.stream()
                                              .map(MigrationError::getClassifier)
                                              .filter(Objects::nonNull)
                                              .map(Classifier::toString)
                                              .findAny()
                                              .orElse("");

            AssistiveProductMigrationResult result = AssistiveProductMigrationResult.builder()
                                                                                    .report(report)
                                                                                    .success(success)
                                                                                    .cascadeFailure(cascadeFailure)
                                                                                    .remarks(remarks)
                                                                                    .errorType(errorType)
                                                                                    .errors(errors)
                                                                                    .build();
            results.add(result);
        }

        return results;
    }

    public GeneralReport quickStats() {
        BigInteger iso9999Product = (BigInteger) em.createNativeQuery("SELECT COUNT(*)\n" +
                "FROM hjmtj.Product\n")
                                                   .getSingleResult();
        BigInteger iso9999Article = (BigInteger) em.createNativeQuery("SELECT COUNT(*)\n" +
                "FROM hjmtj.Article\n")
                                                   .getSingleResult();
        BigInteger agreements = (BigInteger) em.createNativeQuery("SELECT COUNT(*) FROM hjmtj.Agreement")
                                               .getSingleResult();
        BigInteger agreementRows = (BigInteger) em.createNativeQuery("SELECT COUNT(*) FROM AgreementPricelistRow")
                                                  .getSingleResult();
        BigInteger gp = (BigInteger) em.createNativeQuery("SELECT COUNT(*) FROM hjmtj.GeneralPricelist")
                                       .getSingleResult();
        BigInteger gpRows = (BigInteger) em.createNativeQuery("SELECT COUNT(*) FROM hjmtj.GeneralPricelistPricelistRow")
                                           .getSingleResult();
        BigInteger assortments = (BigInteger) em.createNativeQuery("SELECT COUNT(*) FROM hjmtj.Assortment")
                                                .getSingleResult();
        BigInteger assortmentProducts = (BigInteger) em.createNativeQuery("SELECT COUNT(*) FROM hjmtj.AssortmentArticle")
                                                       .getSingleResult();
        BigInteger actors = (BigInteger) em.createNativeQuery("SELECT COUNT(*) FROM hjmtj.UserAccount WHERE hjmtj.UserAccount.password IS NULL")
                                           .getSingleResult();
        BigInteger suppliers = (BigInteger) em.createNativeQuery("SELECT COUNT(*) FROM hjmtj.Organization o WHERE o.organizationType = 'SUPPLIER'")
                                              .getSingleResult();
        BigInteger customers = (BigInteger) em.createNativeQuery("SELECT COUNT(*) FROM hjmtj.Organization o WHERE o.organizationType = 'CUSTOMER'")
                                              .getSingleResult();
        BigInteger businessLevels = (BigInteger) em.createNativeQuery("SELECT COUNT(*) FROM hjmtj.BusinessLevel o ")
                                              .getSingleResult();

        GeneralReport.GeneralReportBuilder builder = GeneralReport.builder();
        builder.iso9999(iso9999Product.longValue() + iso9999Article.longValue());
        builder.agreements(agreements.longValue());
        builder.agreementRows(agreementRows.longValue());
        builder.gp(gp.longValue());
        builder.gpRows(gpRows.longValue());
        builder.assortments(assortments.longValue());
        builder.assortmentProducts(assortmentProducts.longValue());
        builder.users(actors.longValue());
        builder.suppliers(suppliers.longValue());
        builder.customers(customers.longValue());
        builder.businessLevels(businessLevels.longValue());

        return builder.build();
    }
}

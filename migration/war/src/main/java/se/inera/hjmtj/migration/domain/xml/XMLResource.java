/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.xml;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.infra.io.FtpConnection;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.LinkedList;
import java.util.List;

@Slf4j
final class XMLResource implements AutoCloseable {

    private final XMLEventReader xml;

    private XMLResource(InputStream inputStream) {
        XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();

        try {
            this.xml = xmlInputFactory.createXMLEventReader(inputStream);
        } catch (XMLStreamException e) {
            throw new RuntimeException(e);
        }
    }

    public static List<XMLFileDeserialization.ElementContext> of(XMLFileDeserialization mapper, FtpConnection.Parameters ftpParameters) {
        log.info("Loading from {}...", ftpParameters.getHost());
        InputStream ftpStream = FtpConnection.of(ftpParameters);

        return of(mapper, ftpStream);
    }

    public static List<XMLFileDeserialization.ElementContext> of(XMLFileDeserialization mapper, InputStream ftpStream) {
        try (InputStream is = ftpStream) {
            return new XMLResource(is).accept(mapper);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private List<XMLFileDeserialization.ElementContext> accept(XMLFileDeserialization mapper) {
        List<XMLFileDeserialization.ElementContext> returnValue = new LinkedList<>();

        XmlResourceSink generator = new XmlResourceSink(this);

        while (hasNext()) {
            XMLFileDeserialization.ElementContext value = generator.next(mapper);

            if (value != null) {
                returnValue.add(value);
            }
        }

        return returnValue;
    }

    public boolean hasNext() {
        return xml.hasNext();
    }

    public XMLEvent next() {
        try {
            return xml.nextEvent();
        } catch (XMLStreamException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() throws Exception {
        xml.close();
    }

    public static final class XmlResourceSink {

        private final XMLResource xmlResource;

        public XmlResourceSink(XMLResource xmlResource) {
            this.xmlResource = xmlResource;
        }

        public XMLFileDeserialization.ElementContext next(XMLFileDeserialization mapper) {
            StringBuilder sb = new StringBuilder();
            XMLFileDeserialization.ElementFilter elementFilter = consumeStartElement(mapper, sb);

            if (elementFilter == null) {
                return null;
            }

            consumeToEndElement(sb, elementFilter);

            String xml = sb.toString();
            if (xml.isEmpty()) {
                return null;
            }

            return elementFilter.context(xml);
        }

        private XMLFileDeserialization.ElementFilter consumeStartElement(XMLFileDeserialization mapper, StringBuilder sb) {
            while (xmlResource.hasNext()) {
                XMLEvent xmlEvent = xmlResource.next();
                String xmlValue = asText(xmlEvent);
                XMLFileDeserialization.ElementFilter elementFilter = mapper.get(xmlEvent);

                if (elementFilter != null) {
                    if (elementFilter.isStart(xmlEvent)) {
                        sb.append(xmlValue);
                        return elementFilter;
                    }
                }
            }

            return null;
        }

        private void consumeToEndElement(StringBuilder sb, XMLFileDeserialization.ElementFilter elementFilter) {
            while (xmlResource.hasNext()) {
                XMLEvent xmlEvent = xmlResource.next();
                String xmlValue = asText(xmlEvent);

                sb.append(xmlValue);

                if (elementFilter.isEnd(xmlEvent)) {
                    return;
                }
            }
        }

        private String asText(XMLEvent xmlEvent) {
            StringWriter writer = new StringWriter();

            try {
                xmlEvent.writeAsEncodedUnicode(writer);
            } catch (XMLStreamException e) {
                throw new RuntimeException(e);
            }


            return writer.toString();
        }
    }


}

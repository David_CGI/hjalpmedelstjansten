package se.inera.hjmtj.migration.infra.actors;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.domain.model.actors.Actor;
import se.inera.hjmtj.migration.domain.model.actors.ActorOrganization;
import se.inera.hjmtj.migration.domain.model.actors.ActorRole;
import se.inera.hjmtj.migration.domain.model.result.UserReport;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Stateless
public class ActorsRepository {

    @PersistenceContext(unitName = "local")
    private EntityManager em;

    public boolean isEmpty() {
        return size() == 0;
    }

    private long size() {
        return ((BigInteger) em.createNativeQuery("SELECT COUNT(*) FROM Actor a")
                               .setMaxResults(1)
                               .getSingleResult()).longValue();
    }

    public List<Actor> values() {
        return em.createNamedQuery(Actor.ALL, Actor.class)
                 .setHint("org.hibernate.readOnly", true)
                 .getResultList();
    }

    public List<UserReport> report() {
        List<Object[]> resultList = em.createNamedQuery(Actor.REPORT)
                                      .setHint("org.hibernate.readOnly", true)
                                      .getResultList();

        return resultList
                .stream()
                .map(UserReport::new)
                .collect(Collectors.toList());
    }

    public ActorRole findByIdentifier(String identifier) {
        List<ActorRole> resultList = em.createNamedQuery(ActorRole.FIND_BY_IDENTIFIER, ActorRole.class)
                                       .setParameter("identifier", identifier)
                                       .getResultList();

        return resultList.size() > 0 ? resultList.get(0) : null;
    }

}

/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.hjmtj;


import lombok.extern.slf4j.Slf4j;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistPricelistMapper;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistAPI;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelist;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelist;
import se.inera.hjmtj.migration.domain.DateTimes;
import se.inera.hjmtj.migration.domain.MigrationFilter;
import se.inera.hjmtj.migration.domain.model.agreements.builders.GeneralPricelistAPIWithMultiplePricelists;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDateTime;
import java.util.Date;

import static java.util.concurrent.TimeUnit.HOURS;

@Slf4j
@Stateless
@TransactionTimeout(unit = HOURS, value = 5)
public class HjmtjGeneralPricelistPricelistRepository {

    @PersistenceContext(unitName = "HjmtjPU")
    private EntityManager em;

    @Inject
    private MigrationFilter migrationFilter;

    public SaveResult<Long> save(GeneralPricelistAPIWithMultiplePricelists generalPricelistAPI, GeneralPricelistPricelistAPI pricelistAPI, LocalDateTime created) {
        if (migrationFilter.isDeprecated(generalPricelistAPI, pricelistAPI)) {
            return SaveResult.empty();
        }

        GeneralPricelistPricelist generalPricelistPricelist = GeneralPricelistPricelistMapper.map(pricelistAPI);
        GeneralPricelist generalPricelist = em.getReference(GeneralPricelist.class, generalPricelistAPI.getId());
        generalPricelistPricelist.setGeneralPricelist(generalPricelist);

        // set timestamps
        Long createdMillis = DateTimes.toEpochMillis(created);
        generalPricelistPricelist.setCreated(createdMillis != null ? new Date(createdMillis) : null);
        generalPricelistPricelist.setLastUpdated(createdMillis != null ? new Date(createdMillis) : null);

        em.persist(generalPricelistPricelist);

        log.info("Saved GP pricelist {} ", pricelistAPI.getNumber());

        return SaveResult.of(generalPricelistPricelist.getUniqueId(), pricelistAPI);
    }
}

/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.assortments;

import lombok.extern.slf4j.Slf4j;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjmtj.migration.domain.HeapUsageInterceptor;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.domain.model.assortment.Assortment;
import se.inera.hjmtj.migration.domain.model.assortment.AssortmentProduct;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.concurrent.TimeUnit.HOURS;

@Slf4j
@Stateless
@Interceptors({PerformanceLogInterceptor.class, HeapUsageInterceptor.class})
@TransactionTimeout(unit = HOURS, value = 5)
public class AssortmentsRepository {

    @PersistenceContext(unitName = "local")
    private EntityManager em;

    public boolean isEmpty() {
        return size() == 0;
    }

    private long size() {
        return em.createQuery("SELECT COUNT(a) FROM Assortment a", Long.class)
                 .setMaxResults(1)
                 .getSingleResult();
    }

    public void clear() {
        em.createNativeQuery("DELETE FROM ASSORTMENT_ASSORTMENTPRODUCT")
          .executeUpdate();
        em.createNativeQuery("DELETE FROM ASSORTMENT")
          .executeUpdate();
        em.createNativeQuery("DELETE FROM ASSORTMENTORGANIZATION")
          .executeUpdate();
        em.createNativeQuery("DELETE FROM ASSORTMENTPRODUCT")
          .executeUpdate();
    }

    public List<Assortment> values(boolean fetchProducts) {
        if (fetchProducts) {
            return em.createNamedQuery(Assortment.FIND_ALL_WITH_PRODUCT, Assortment.class)
                     .setHint("org.hibernate.readOnly", true)
                     .getResultList()
                     .stream()
                     .map(this::setRows)
                     .collect(Collectors.toList());
        } else {
            return em.createQuery(Assortment.FIND_ALL, Assortment.class)
                     .setHint("org.hibernate.readOnly", true)
                     .getResultList();
        }
    }

    private Assortment setRows(Assortment agreement) {
        List<AssortmentProduct> rows = new LinkedList<>(agreement.getProducts());
        agreement.setProducts(new HashSet<>(rows));
        return agreement;
    }

    public Assortment findByIdentifier(String identifier) {
        List<Assortment> assortmentList = em.createNamedQuery(Assortment.FIND_BY_IDENTIFIER, Assortment.class)
                                            .setParameter("identifier", identifier)
                                            .setHint("org.hibernate.readOnly", true)
                                            .getResultList();

        return assortmentList.size() > 0 ? assortmentList.get(0) : null;
    }
}

/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.product;

import lombok.extern.slf4j.Slf4j;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DirectedAcyclicGraph;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.CategoryAPI;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjmtj.migration.domain.exceptions.save.UnsupportedFitException;

import java.util.LinkedList;
import java.util.Objects;

@Slf4j
public class ArticleAPINode extends ArticleAPI implements AssistiveProduct {

    private transient final DirectedAcyclicGraph<AssistiveProduct, DefaultEdge> graph;

    private Long catalogueUniqueNumber;

    public ArticleAPINode(DirectedAcyclicGraph<AssistiveProduct, DefaultEdge> graph, long id) {
        setId(id);
        setFitsToArticles(new LinkedList<>());
        setFitsToProducts(new LinkedList<>());
        setReplacedByArticles(new LinkedList<>());
        this.graph = graph;

        if (graph != null) {
            graph.addVertex(this);
        }
    }

    @Override
    public DirectedAcyclicGraph<AssistiveProduct, DefaultEdge> graph() {
        return graph;
    }

    @Override
    public String getNumber() {
        return getArticleNumber();
    }

    @Override
    public Long getCatalogueUniqueNumber() {
        return catalogueUniqueNumber;
    }

    @Override
    public void setCatalogueUniqueNumber(Long catalogueUniqueNumber) {
        this.catalogueUniqueNumber = catalogueUniqueNumber;
    }

    @Override
    public void addFit(AssistiveProduct to) throws UnsupportedFitException {
        Objects.requireNonNull(to, "to must be non-null");
        String code = getCategory().getCode();
        Article.Type type = getCategory().getArticleType();
        Article.Type toType = to.getCategory()
                                .getArticleType();

        if (to.equals(this)) {
            throw new UnsupportedFitException(toString() + " cannot fit itself");
        } else if (code != null && !code.isEmpty()) {
            throw new UnsupportedFitException(toString() + " with ISO-code category cannot fit any product or article, not even " + to);
        } else if (type.equals(Article.Type.H)) {
            throw new UnsupportedFitException(toString() + " cannot have fit " + to + ", since it has category with type H");
        } else if (getFitsToArticles().contains(to)) {
            throw new UnsupportedFitException(toString() + " already fits " + to);
        } else if (toType.equals(Article.Type.I) ||
                toType.equals(Article.Type.R) ||
                toType.equals(Article.Type.Tj)) {
            throw new UnsupportedFitException(toString() + " cannot have fit to " + to + " of wrong type " + toType);
        } else if (to instanceof ProductAPINode) {
            addFit((ProductAPINode) to);
        } else if (to instanceof ArticleAPINode) {
            addFit((ArticleAPINode) to);
        }
    }

    public boolean isTIso() {
        CategoryAPI category = getCategory();
        Article.Type articleType = category.getArticleType();
        String code = category.getCode();
        return code != null && !code.isEmpty() && articleType.equals(Article.Type.T);
    }

    private void addFit(ProductAPINode to) throws UnsupportedFitException {
        String toCode = to.getCategory()
                          .getCode();

        if (toCode == null || toCode.isEmpty()) {
            throw new UnsupportedFitException(toString() + " cannot fit (invalid because it has no code) " + to);
        }

        addFitsToProductsItem(to);
    }

    private void addFit(ArticleAPINode to) {
        addFitsToArticlesItem(to);
    }

    void addFitsToProductsItem(ProductAPI fitsToProductsItem) {
        addDependency(fitsToProductsItem);
        getFitsToProducts().add(fitsToProductsItem);
    }

    void addFitsToArticlesItem(ArticleAPI fitsToArticlesItem) {
        addDependency(fitsToArticlesItem);
        getFitsToArticles().add(fitsToArticlesItem);
    }

    @Override
    public void setBasedOnProduct(ProductAPI basedOnProduct) {
        addDependency(basedOnProduct);
        super.setBasedOnProduct(basedOnProduct);
    }

    public void addReplacedByArticlesItem(ArticleAPI replacedByArticlesItem) {
        addDependency(replacedByArticlesItem);
        getReplacedByArticles().add(replacedByArticlesItem);
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ArticleAPINode articleAPINode = (ArticleAPINode) o;
        return Objects.equals(getId(), articleAPINode.getId());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<");
        sb.append("Article ");
        sb.append(getCatalogueUniqueNumber());
        sb.append("(");
        if (getCategory().getCode() != null) {
            sb.append(getCategory().getCode());
            sb.append(" ");
        }
        sb.append(getCategory().getName());
        sb.append(" ");
        sb.append("[");
        sb.append(getCategory().getArticleType()
                               .toString());
        sb.append("]");
        sb.append(")");
        sb.append(">");

        return sb.toString();
    }

}

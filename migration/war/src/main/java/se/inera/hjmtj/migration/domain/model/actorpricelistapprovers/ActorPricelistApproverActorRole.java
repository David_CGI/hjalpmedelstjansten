/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.actorpricelistapprovers;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.domain.model.Reference;
import se.inera.hjmtj.migration.domain.model.actors.ActorRole;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.PersistenceUnit;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashSet;
import java.util.Set;

@PersistenceUnit(unitName = "local")
@Entity
@Slf4j
@Data
@XmlRootElement(name = "Role")
@Table(indexes = {
        @Index(columnList = "identifier"),
})
public class ActorPricelistApproverActorRole {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long generatedId;

    @Column(nullable = false, unique = true)
    @XmlElement(name = "Identifier")
    private String identifier;

    @OneToOne(fetch = FetchType.EAGER)
    private ActorRole actorRole;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<ActorPricelistApproverAgreement> approves;

    public ActorPricelistApproverActorRole() {
        approves = new HashSet<>();
    }

    @XmlElement(name = "OwnsAgreementList")
    public void setXMLApprovesAgreements(Reference<ActorPricelistApproverAgreement> reference) {
        approves.add(reference.getReference());
    }

}

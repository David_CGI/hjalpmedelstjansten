/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.agreements.builders;

import se.inera.hjalpmedelstjansten.model.api.AgreementAPI;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistRowAPI;

import java.util.List;
import java.util.stream.Collectors;

public class AgreementAPIToGeneralPricelistAPIMapper {

    public GeneralPricelistAPIWithMultiplePricelists map(AgreementAPIBuilderBase.AgreementAPICreated agreementAPI) {
        GeneralPricelistAPIWithMultiplePricelists generalPricelistAPI = new GeneralPricelistAPIWithMultiplePricelists();

        generalPricelistAPI.setCreated(agreementAPI.getCreated());
        generalPricelistAPI.setGeneralPricelistName(agreementAPI.getAgreementName());
        generalPricelistAPI.setGeneralPricelistNumber(agreementAPI.getAgreementNumber());
        generalPricelistAPI.setValidFrom(agreementAPI.getValidFrom());
        generalPricelistAPI.setValidTo(agreementAPI.getValidTo());
        generalPricelistAPI.setOwnerOrganization(agreementAPI.getSupplierOrganization());

        generalPricelistAPI.setDeliveryTimeH(agreementAPI.getDeliveryTimeH());
        generalPricelistAPI.setDeliveryTimeT(agreementAPI.getDeliveryTimeT());
        generalPricelistAPI.setDeliveryTimeR(agreementAPI.getDeliveryTimeR());
        generalPricelistAPI.setDeliveryTimeI(agreementAPI.getDeliveryTimeI());
        generalPricelistAPI.setDeliveryTimeTJ(agreementAPI.getDeliveryTimeTJ());

        generalPricelistAPI.setWarrantyTermsH(agreementAPI.getWarrantyTermsH());
        generalPricelistAPI.setWarrantyTermsT(agreementAPI.getWarrantyTermsT());
        generalPricelistAPI.setWarrantyTermsR(agreementAPI.getWarrantyTermsR());
        generalPricelistAPI.setWarrantyTermsI(agreementAPI.getWarrantyTermsI());
        generalPricelistAPI.setWarrantyTermsTJ(agreementAPI.getWarrantyTermsTJ());

        generalPricelistAPI.setWarrantyValidFromH(agreementAPI.getWarrantyValidFromH());
        generalPricelistAPI.setWarrantyValidFromT(agreementAPI.getWarrantyValidFromT());
        generalPricelistAPI.setWarrantyValidFromR(agreementAPI.getWarrantyValidFromR());
        generalPricelistAPI.setWarrantyValidFromI(agreementAPI.getWarrantyValidFromI());
        generalPricelistAPI.setWarrantyValidFromTJ(agreementAPI.getWarrantyValidFromTJ());

        generalPricelistAPI.setWarrantyTermsH(agreementAPI.getWarrantyTermsH());
        generalPricelistAPI.setWarrantyTermsT(agreementAPI.getWarrantyTermsT());
        generalPricelistAPI.setWarrantyTermsR(agreementAPI.getWarrantyTermsR());
        generalPricelistAPI.setWarrantyTermsI(agreementAPI.getWarrantyTermsI());
        generalPricelistAPI.setWarrantyTermsTJ(agreementAPI.getWarrantyTermsTJ());

        generalPricelistAPI.setWarrantyQuantityHUnit(agreementAPI.getWarrantyQuantityHUnit());
        generalPricelistAPI.setWarrantyQuantityTUnit(agreementAPI.getWarrantyQuantityTUnit());
        generalPricelistAPI.setWarrantyQuantityRUnit(agreementAPI.getWarrantyQuantityRUnit());
        generalPricelistAPI.setWarrantyQuantityIUnit(agreementAPI.getWarrantyQuantityIUnit());
        generalPricelistAPI.setWarrantyQuantityTJUnit(agreementAPI.getWarrantyQuantityTJUnit());

        generalPricelistAPI.setWarrantyQuantityH(agreementAPI.getWarrantyQuantityH());
        generalPricelistAPI.setWarrantyQuantityT(agreementAPI.getWarrantyQuantityT());
        generalPricelistAPI.setWarrantyQuantityR(agreementAPI.getWarrantyQuantityR());
        generalPricelistAPI.setWarrantyQuantityI(agreementAPI.getWarrantyQuantityI());
        generalPricelistAPI.setWarrantyQuantityTJ(agreementAPI.getWarrantyQuantityTJ());

        // Sort pricelists so that the the one with the greatest validFrom is placed first
        List<GeneralPricelistPricelistAPI> pricelistAPIList = agreementAPI.getPricelists()
                                                                          .stream()
                                                                          .map(t -> map(generalPricelistAPI, t))
                                                                          .collect(Collectors.toList());

        generalPricelistAPI.setPricelistAPIs(pricelistAPIList);

        return generalPricelistAPI;
    }

    private GeneralPricelistPricelistAPI map(GeneralPricelistAPI generalPricelistAPI, AgreementPricelistAPI pricelist) {
        GeneralPricelistPricelistAPIWithRows generalPricelistPricelistAPI = new GeneralPricelistPricelistAPIWithRows();
        generalPricelistPricelistAPI.setValidFrom(pricelist.getValidFrom());
        generalPricelistPricelistAPI.setNumber(pricelist.getNumber());
        generalPricelistPricelistAPI.setStatus(pricelist.getStatus());
        generalPricelistPricelistAPI.setRows(pricelist.getRows()
                                                      .stream()
                                                      .map(agreementPricelistRowAPI -> map(generalPricelistPricelistAPI, agreementPricelistRowAPI))
                                                      .collect(Collectors.toList()));
        generalPricelistPricelistAPI.setGeneralPricelist(generalPricelistAPI);

        return generalPricelistPricelistAPI;
    }

    private GeneralPricelistPricelistRowAPI map(GeneralPricelistPricelistAPI pricelist, AgreementPricelistRowAPI agreementPricelistRowAPI) {
        GeneralPricelistPricelistRowAPI generalPricelistPricelistRowAPI = new GeneralPricelistPricelistRowAPI();
        generalPricelistPricelistRowAPI.setArticle(agreementPricelistRowAPI.getArticle());
        generalPricelistPricelistRowAPI.setDeliveryTime(agreementPricelistRowAPI.getDeliveryTime());
        generalPricelistPricelistRowAPI.setLeastOrderQuantity(agreementPricelistRowAPI.getLeastOrderQuantity());
        generalPricelistPricelistRowAPI.setPrice(agreementPricelistRowAPI.getPrice());
        generalPricelistPricelistRowAPI.setPricelist(pricelist);
        generalPricelistPricelistRowAPI.setStatus(agreementPricelistRowAPI.getStatus());
        generalPricelistPricelistRowAPI.setValidFrom(agreementPricelistRowAPI.getValidFrom());
        generalPricelistPricelistRowAPI.setWarrantyQuantity(agreementPricelistRowAPI.getWarrantyQuantity());
        generalPricelistPricelistRowAPI.setWarrantyQuantityUnit(agreementPricelistRowAPI.getWarrantyQuantityUnit());
        generalPricelistPricelistRowAPI.setWarrantyTerms(agreementPricelistRowAPI.getWarrantyTerms());
        generalPricelistPricelistRowAPI.setWarrantyValidFrom(agreementPricelistRowAPI.getWarrantyValidFrom());

        return generalPricelistPricelistRowAPI;
    }

}

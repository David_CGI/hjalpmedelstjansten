/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.application;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.domain.model.agreements.gp.GeneralPricelist;
import se.inera.hjmtj.migration.domain.model.agreements.gp.GeneralPricelistCalculatedFrom;
import se.inera.hjmtj.migration.domain.model.agreements.gp.GeneralPricelistOrganization;
import se.inera.hjmtj.migration.domain.model.agreements.gp.GeneralPricelistProduct;
import se.inera.hjmtj.migration.domain.model.agreements.gp.GeneralPricelistStatus;
import se.inera.hjmtj.migration.domain.model.agreements.gp.GeneralPricelistWarrantyUnit;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.Agreement;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.AgreementCalculatedFrom;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.AgreementOrganization;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.AgreementProduct;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.AgreementStatus;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.AgreementWarrantyUnit;
import se.inera.hjmtj.migration.domain.model.assortment.Assortment;
import se.inera.hjmtj.migration.domain.model.assortment.AssortmentCountyMunicipality;
import se.inera.hjmtj.migration.domain.model.assortment.AssortmentOrganization;
import se.inera.hjmtj.migration.domain.model.assortment.AssortmentProduct;
import se.inera.hjmtj.migration.domain.model.product.entities.Product;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductCategoryProduct;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductCategorySpecificPropertyListValue;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductClassification;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductDocumentType;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductMarkingDirective;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductMarkingStandard;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductOrganization;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductUnit;
import se.inera.hjmtj.migration.domain.xml.ElementContextComparator;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import static java.lang.System.getenv;

@Slf4j
@ApplicationScoped
@ApplicationPath("/")
public class MigrationApplication extends Application {

    public static final String FTP_URL_HOST = getenv()
            .getOrDefault("PRODUCT_URL_HOST", "<host>");

    public static final String FTP_URL_USERNAME = getenv()
            .getOrDefault("PRODUCT_URL_USERNAME", "<user>");

    public static final String FTP_URL_PASSWORD = getenv()
            .getOrDefault("PRODUCT_URL_PASSWORD", "<psw>");

}

/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.organizations;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.domain.model.Indexed;
import se.inera.hjmtj.migration.domain.model.OrganizationBase;
import se.inera.hjmtj.migration.domain.model.organization.entities.Customer;

import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@Slf4j
@Stateless
@Interceptors(PerformanceLogInterceptor.class)
public class CustomerRepository {

    private static final String BUSINESS_LEVEL = "Verksamhetsområde";

    private final Map<String, String> customerNameMap = new HashMap<>();

    @PersistenceContext(unitName = "local")
    private EntityManager em;

    public CustomerRepository() {
        customerNameMap.put("VGR Internt", "Hjälpmedelscentralen Västra Götaland");
        customerNameMap.put("Syncentralen Göteborg, Habilitering och Hälsa", "Syncentralen, Göteborg, Habilitering och Hälsa");
    }

    public List<Indexed<Customer>> businessAreas(Customer userOrganization) {
        Indexed.Builder<Customer> builder = Indexed.builder();

        return em.createNamedQuery(Customer.FIND_BY_TYPE_AND_GLN, Customer.class)
                 .setParameter("type", BUSINESS_LEVEL)
                 .setParameter("gln", userOrganization.getGln())
                 .getResultList()
                 .stream()
                 .map(builder::build)
                 .collect(toList());
    }

    public List<Indexed<Customer>> rootOrganizations() {
        Indexed.Builder<Customer> builder = Indexed.builder();

        return em.createQuery("SELECT o FROM Customer o " +
                "LEFT JOIN FETCH o.secret " +
                "WHERE o.type IS NULL", Customer.class)
                 .getResultList()
                 .stream()
                 .map(builder::build)
                 .collect(toList());
    }

    public List<Customer> values() {
        return em.createQuery("SELECT distinct o FROM Customer o ", Customer.class)
                 .getResultList();
    }

    public List<Customer> customersAndExportSettings() {
        return em.createQuery("SELECT distinct c FROM Customer c " +
                "LEFT JOIN FETCH c.exportSettings e " +
                "LEFT JOIN FETCH e.generalPricelist gp " +
                "LEFT JOIN FETCH gp.seller s " +
                "LEFT JOIN FETCH c.agreementOrganization o ", Customer.class)
                 .getResultList();
    }

    public Long size() {
        return em.createQuery("SELECT COUNT(c) FROM Customer c", Long.class)
                 .getSingleResult();
    }

    public Customer getCustomer(OrganizationBase organization) {
        return findByName(organization.getName());
    }

    private Customer findByName(String name) {
        String organizationName = customerNameMap.getOrDefault(name, name);

        List<Customer> customers = em.createNamedQuery(Customer.FIND_BY_NAME, Customer.class)
                                     .setParameter("name", organizationName)
                                     .setMaxResults(1)
                                     .getResultList();

        return customers.size() > 0 ? customers.get(0) : null;
    }
}

/*
 *
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 *  Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.errors;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

@Slf4j
public enum MessageTemplate {

    VALID_FROM_IS_MISSING("VALID_FROM_IS_MISSING"),

    ROLE_COULD_NOT_BE_ASSIGNED("ROLE_COULD_NOT_BE_ASSIGNED"),

    ORGANIZATION_IS_NOT_A_SUBSCRIBER("ORGANIZATION_IS_NOT_A_SUBSCRIBER"),

    ROW_WILL_NOT_BE_MIGRATED_BECAUSE_ARTICLE_HAS_NOT_BEEN_MIGRATED("ROW_WILL_NOT_BE_MIGRATED_BECAUSE_ARTICLE_HAS_NOT_BEEN_MIGRATED"),

    CVCESTANDARD_VALUE_NOT_FOUND("CVCESTANDARD_VALUE_NOT_FOUND"),

    CVDIRECTIVE_VALUE_NOT_FOUND("CVDIRECTIVE_VALUE_NOT_FOUND"),

    INVALID_GTIN13("INVALID_GTIN13"),

    AGREEMENT_HAS_NO_ROWS("AGREEMENT_HAS_NO_ROWS"),

    AGREEMENT_HAS_NO_USERS_TO_APPROVE_PRICELISTS("AGREEMENT_HAS_NO_USERS_TO_APPROVE_PRICELISTS"),

    AGREEMENT_HAS_NO_PRICELIST_APPROVERS_FALLBACK("AGREEMENT_HAS_NO_PRICELIST_APPROVERS_FALLBACK"),

    CVGUARANTEE_VALUE_NOT_FOUND("CVGUARANTEE_VALUE_NOT_FOUND"),

    COUNTY_TO_MUNICIPALITY_OVERFLOW("COUNTY_TO_MUNICIPALITY_OVERFLOW"),

    MUNICIPALITY_NOT_SET("MUNICIPALITY_NOT_SET"),

    COUNTY_NOT_FOUND("COUNTY_NOT_FOUND"),

    NOT_ISO9999("NOT_ISO9999"),

    ARTICLE_WITHOUT_TYPE("ARTICLE_WITHOUT_TYPE"),

    ASSISTIVE_PRODUCT_WITH_ID_DOES_NOT_EXIST("ASSISTIVE_PRODUCT_WITH_ID_DOES_NOT_EXIST"),

    ASSISTIVE_PRODUCT_CANNOT_FIT_ASSISTIVE_PRODUCT_ON_DIFFERENT_SUPPLIER("ASSISTIVE_PRODUCT_CANNOT_FIT_ASSISTIVE_PRODUCT_ON_DIFFERENT_SUPPLIER"),

    CATEGORY_SPECIFIC_PROPERTY_LISTVALUE("CATEGORY_SPECIFIC_PROPERTY_LISTVALUE"),

    CATALOGUE_UNIQUE_NUMBER_NAN("CATALOGUE_UNIQUE_NUMBER_NAN"),

    ARTICLE_CANNOT_REPLACE_PRODUCT("ARTICLE_CANNOT_REPLACE_PRODUCT"),

    PRODUCT_CANNOT_REPLACE_ARTICLE("PRODUCT_CANNOT_REPLACE_ARTICLE"),

    ARTICLE_CANNOT_BE_BASED_ON_ARTICLE("ARTICLE_CANNOT_BE_BASED_ON_ARTICLE"),

    ARTICLE_H_MUST_BE_BASED_ON_A_PRODUCT("ARTICLE_H_MUST_BE_BASED_ON_A_PRODUCT"),

    NULL_PRICELIST_NUMBER("NULL_PRICELIST_NUMBER"),

    ROW_WILL_NOT_BE_MIGRATED_BECAUSE_ARTICLE_WAS_NOT_RECEIVED("ROW_WILL_NOT_BE_MIGRATED_BECAUSE_ARTICLE_WAS_NOT_RECEIVED"),

    PRODUCT_NUMBER_WAS_CHANGED("PRODUCT_NUMBER_WAS_CHANGED"),

    ARTICLE_NUMBER_WAS_CHANGED("ARTICLE_NUMBER_WAS_CHANGED"),

    WARRANTY_QUANTITY_CHANGED("WARRANTY_QUANTITY_CHANGED"),

    DELIVERY_TIME_CHANGED("DELIVERY_TIME_CHANGED"),

    LEAST_ORDER_QUANTITY_CHANGED("LEAST_ORDER_QUANTITY_CHANGED"),

    ARTICLE_CUSTOMER_UNIQUE_CHANGED("ARTICLE_CUSTOMER_UNIQUE_CHANGED"),

    EMPTY_COUNTY_CHANGED("EMPTY_COUNTY_CHANGED"),

    EMPTY_MUNICIPALITY_CHANGED("EMPTY_MUNICIPALITY_CHANGED"),

    SUPPLIER_HAS_MULTIPLE_GP("SUPPLIER_HAS_MULTIPLE_GP"),

    T1177("T1177"),

    EXPIRED_ASSISTIVE_PRODUCT_IS_DEPRECATED("EXPIRED_ASSISTIVE_PRODUCT_IS_DEPRECATED"),

    PRICELIST_EMPTY_VALIDFROM("PRICELIST_EMPTY_VALIDFROM"),

    INACTIVE_PRICELIST_IS_DEPRECATED("INACTIVE_PRICELIST_IS_DEPRECATED"),

    INACTIVE_GP("INACTIVE_GP"),

    ASSORTMENT_HAS_NO_OWNERS("ASSORTMENT_HAS_NO_OWNERS"),

    USER_EMAIL_CHANGED("USER_EMAIL_CHANGED"),

    USER_ORGANIZATION_CHANGED("USER_ORGANIZATION_CHANGED"),

    USER_REMOVED("USER_REMOVED"),

    USER_COPIED("USER_COPIED"),

    ORDER_UNIT_NULL("ORDER_UNIT_NULL"),

    INACTIVE_PRICELIST_ROW("INACTIVE_PRICELIST_ROW"),

    MISSING_VERSION_LIST_ENTRY("MISSING_VERSION_LIST_ENTRY"),

    ARTICLE_HAS_MULTIPLE_TYPES("ARTICLE_HAS_MULTIPLE_TYPES");

    private static Properties PROPERTIES;

    private final String code;

    MessageTemplate(String code) {
        this.code = code.trim()
                        .toUpperCase();
    }

    public static String get(String key) {
        return properties().getProperty(key.toUpperCase());
    }

    private static synchronized Properties properties() {
        if (PROPERTIES == null) {
            try {
                PROPERTIES = new Properties();
                InputStream input = Classifier.class.getResourceAsStream("/messagetemplate.properties");
                PROPERTIES.load(new InputStreamReader(input, StandardCharsets.UTF_8));
            } catch (IOException e) {
                log.error("Failed to load resource bundle", e);
            }
        }

        return PROPERTIES;
    }

    @Override
    public String toString() {
        return properties().getProperty(code);
    }

    public String getCode() {
        return code;
    }


}

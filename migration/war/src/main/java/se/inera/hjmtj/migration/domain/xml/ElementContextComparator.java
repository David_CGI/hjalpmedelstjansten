/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.xml;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ElementContextComparator implements Comparator<XMLFileDeserialization.ElementContext> {

    private final Map<Class<?>, Integer> persistOrder = new HashMap<>();

    public ElementContextComparator(Class<?>... clazz) {
        for (int i = 0; i < clazz.length; i++) {
            persistOrder.put(clazz[i], i);
        }
    }

    @Override
    public int compare(XMLFileDeserialization.ElementContext o1, XMLFileDeserialization.ElementContext o2) {
        return Integer.compare(order(o1), order(o2));
    }

    private int order(XMLFileDeserialization.ElementContext instance) {
        XMLFileDeserialization.ElementFilter elementFilter = instance.mapper();
        Class<?> type = elementFilter.resultClass();

        return order(type);
    }

    private int order(Class<?> type) {
        if (!persistOrder.containsKey(type)) {
            throw new RuntimeException(type + " not registered");
        }

        return persistOrder.get(type);
    }

    public Set<Class<?>> classes() {
        return persistOrder.keySet();
    }
}

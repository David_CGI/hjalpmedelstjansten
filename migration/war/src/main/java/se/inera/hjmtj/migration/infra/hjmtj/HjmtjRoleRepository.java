package se.inera.hjmtj.migration.infra.hjmtj;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjalpmedelstjansten.business.user.controller.RoleMapper;
import se.inera.hjalpmedelstjansten.model.api.RoleAPI;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Stateless
public class HjmtjRoleRepository {
    @PersistenceContext(unitName = "HjmtjPU")
    private EntityManager em;


    public List<RoleAPI> all() {
        return em.createQuery("SELECT r FROM UserRole r", UserRole.class)
                 .getResultList()
                 .stream()
                 .map(RoleMapper::map)
                 .collect(Collectors.toList());
    }
}

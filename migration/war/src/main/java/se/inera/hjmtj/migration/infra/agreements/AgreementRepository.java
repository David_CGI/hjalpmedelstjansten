/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.infra.agreements;

import lombok.extern.slf4j.Slf4j;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjmtj.migration.domain.HeapUsageInterceptor;
import se.inera.hjmtj.migration.domain.PerformanceLogInterceptor;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.Agreement;
import se.inera.hjmtj.migration.domain.model.agreements.purchase.AgreementRow;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.infra.io.BatchPersistence;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.concurrent.TimeUnit.HOURS;

@Slf4j
@Stateless
@Interceptors({PerformanceLogInterceptor.class, HeapUsageInterceptor.class})
@TransactionTimeout(unit = HOURS, value = 5)
public class AgreementRepository {

    @PersistenceContext(unitName = "local")
    private EntityManager em;

    @Inject
    private BatchPersistence batchPersistence;

    public boolean isEmpty() {
        return size() == 0;
    }

    private long size() {
        return em.createNamedQuery(Agreement.COUNT_AGREEMENT, Long.class)
                 .setMaxResults(1)
                 .getSingleResult();
    }


    public void save(List<Object> objects) {
        batchPersistence.save(objects, em);
    }

    public List<Agreement> values() {
        return em.createNamedQuery(Agreement.FIND_ALL, Agreement.class)
                 .getResultList();
    }

    public List<Agreement> findBy(Supplier supplier) {
        return em.createNamedQuery(Agreement.FIND_BY_SUPPLIER, Agreement.class)
                 .setParameter("supplier", supplier)
                 .getResultList()
                 .stream()
                 .map(this::setRows)
                 .collect(Collectors.toList());
    }

    private Agreement setRows(Agreement agreement) {
        LinkedList<AgreementRow> rows = new LinkedList<>(agreement.getRows());
        agreement.setRows(new HashSet<>(rows));
        return agreement;
    }

    public Agreement findByIdentifier(String identifier) {
        List<Agreement> agreementList = em.createNamedQuery(Agreement.FIND_BY_IDENTIFIER, Agreement.class)
                                          .setParameter("identifier", identifier)
                                          .getResultList();
        return agreementList.size() > 0 ? agreementList.get(0) : null;
    }
}

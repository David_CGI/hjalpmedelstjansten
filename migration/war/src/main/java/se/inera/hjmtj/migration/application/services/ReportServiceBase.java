/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.application.services;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public interface ReportServiceBase {

    String YES = "Ja";

    String NO = "Nej";

    Cache<String, Object> CACHE = Caffeine.newBuilder()
                                          .expireAfterWrite(30, TimeUnit.MINUTES)
                                          .build();


    default String booleanString(boolean value) {
        return value ? YES : NO;
    }

    default <T> T generateReport(String key, Supplier<T> loader) {
        long start = System.currentTimeMillis();
        Logger log = LoggerFactory.getLogger(getClass());
        log.info("REPORT {}", key);

        try {
            return (T) CACHE.get(key, nil -> loader.get());
        } finally {
            log.info("Report finished in {} s", (System.currentTimeMillis() - start) / 1000);
        }
    }
}

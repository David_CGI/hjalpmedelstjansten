/*
 *
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 *  Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package se.inera.hjmtj.migration.tools;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;
import java.util.stream.Collectors;

public class Diff {

    public static void main(String[] args) throws IOException {
        if (args.length != 2) {
            throw new IllegalArgumentException("Syntax:\n" +
                    "Diff <newfile> <oldfile>");
        }

        // remove the u parameter that seems to be tied to the user used to export the dataset
        // and is not relevant to comparison (in fact differs across migrations)
        Path path1 = Paths.get(args[0]);
        Set<String> newLines = Files.readAllLines(path1)
                                    .stream()
                                    .map(Diff::removeUParameter)
                                    .collect(Collectors.toSet());

        Path path2 = Paths.get(args[1]);
        Set<String> oldLines = Files.readAllLines(path2)
                                    .stream()
                                    .map(Diff::removeUParameter)
                                    .collect(Collectors.toSet());

        for (String line : newLines) {
            if (!oldLines.contains(line)) {
                System.out.println(line);
            }
        }
    }

    public static String removeUParameter(String line) {
        return line.replaceAll("(?<=[&])u=.*?($|[;])", ";");
    }
}

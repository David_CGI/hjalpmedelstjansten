/*
 *
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 *  Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.tools;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DiffTest {

    @Test
    public void removeUParameter() {
        String result = Diff.removeUParameter("https://webcache.visuera.com/cache_vgregion?ref=c%2c3f662b31-e5a3-4cd8-a06b-a50800abeb2b%2cBlobData&u=8D2B44A2260A580;b897a20d-d259-352d-bea3-d93cd979eef1/a1562988-af33-3fa2-9aa2-91c22184e478/ad_amaretto_ohne_trikot.jpg;ad_amaretto_ohne_trikot.jpg");
        assertEquals("https://webcache.visuera.com/cache_vgregion?ref=c%2c3f662b31-e5a3-4cd8-a06b-a50800abeb2b%2cBlobData&;b897a20d-d259-352d-bea3-d93cd979eef1/a1562988-af33-3fa2-9aa2-91c22184e478/ad_amaretto_ohne_trikot.jpg;ad_amaretto_ohne_trikot.jpg", result);
    }
}
/*
 *
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 *  Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.shellrunner;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Objects;

@Slf4j
public class Main {

    private static String FTP_HOST = System.getenv("FTP_HOST");

    private static String FTP_USERNAME = System.getenv("FTP_USERNAME");

    private static String FTP_PASSWORD = System.getenv("FTP_PASSWORD");

    private static String FTP_FILE = System.getenv("FTP_FILE");

    private static String FTP_ZIP_ENTRY = System.getenv("FTP_ZIP_ENTRY");

    private static String APP_CMD = System.getenv("APP_CMD");

    private static String APP_WORK_DIR = System.getenv("APP_WORK_DIR");

    private static String APP_SCRIPT_DIR = System.getenv("APP_SCRIPT_DIR");

    public static void main(String[] args) {
        try {
            Objects.requireNonNull(FTP_HOST, "FTP_HOST must be set");
            Objects.requireNonNull(FTP_USERNAME, "FTP_USERNAME must be set");
            Objects.requireNonNull(FTP_PASSWORD, "FTP_PASSWORD must be set");
            Objects.requireNonNull(FTP_FILE, "FTP_FILE must be set");
            Objects.requireNonNull(FTP_ZIP_ENTRY, "FTP_HOST must be set");
            Objects.requireNonNull(APP_CMD, "APP_CMD must be set");
            Objects.requireNonNull(APP_WORK_DIR, "APP_WORK_DIR must be set");
            Objects.requireNonNull(APP_SCRIPT_DIR, "APP_SCRIPT_DIR must be set");

            copyScriptDirToWorkDir();

            download(FTP_ZIP_ENTRY);
            download(FTP_ZIP_ENTRY + ".md5");

            runScript();

            System.exit(0);
        } catch(Throwable e){
            System.exit(1);
        }
    }

    private static void runScript() {
        try {
            log.info("WORK DIR = {}", APP_WORK_DIR);
            log.info("{}", APP_CMD.trim());
            new ProcessBuilder().command(Arrays.asList("bash", "-c", APP_CMD))
                                .inheritIO()
                                .start()
                                .waitFor();
        } catch (IOException | InterruptedException e) {
            log.error("{}", e);
        }
    }

    private static void copyScriptDirToWorkDir() {
        log.info("Copying {} to {}", APP_SCRIPT_DIR, APP_WORK_DIR);
        Path source = Paths.get(APP_SCRIPT_DIR);
        Path destination = Paths.get(APP_WORK_DIR, source.toFile()
                                                         .getName());
        copyFolder(source, destination);

        File outputFile = destination.toFile();
        File[] files = outputFile.listFiles();

        if (files == null) {
            log.warn("No files present in output directory {}", outputFile.toPath()
                                                                          .toAbsolutePath());
        } else {
            for (File file : files) {
                if (!file.canExecute() && file.getName()
                                              .endsWith(".sh")) {
                    file.setExecutable(true);
                    log.info("Set executable bit on shell script {}", file.getName());
                }
            }
        }
    }

    public static void copyFolder(Path source, Path destination) {
        try {
            Files.walk(source)
                 .forEach(s -> {
                     try {
                         Path d = destination.resolve(source.relativize(s));
                         if (Files.isDirectory(s)) {
                             if (!Files.exists(d)) {
                                 Files.createDirectory(d);
                             }
                             return;
                         }

                         if (!Files.exists(d)) {
                             Files.copy(s, d);
                         }
                     } catch (Exception e) {
                         e.printStackTrace();
                     }
                 });
        } catch (Exception e) {
            log.error("{}", e);
        }
    }

    private static void download(String ftpZipEntry) {
        try (InputStream is = FtpConnection.of(new FtpConnection.Parameters(FTP_HOST, FTP_FILE, ftpZipEntry, FTP_USERNAME, FTP_PASSWORD))) {
            try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(ftpZipEntry))) {
                int b;
                while ((b = is.read()) != -1) {
                    bos.write(b);
                }
            }
        } catch (IOException e) {
            log.error("File {} was not downloaded from FTP, reason: {}", ftpZipEntry, e);
        }
    }

}

#!/usr/bin/env bash
mvn clean install -DskipTests=true

mkdir -p ./modules/hjmtj/main
cp ./../web/customization/module.xml ./modules/hjmtj/main/
cp ./../web/customization/elasticsearch_index_source.json ./modules/hjmtj/main/
cp ./../web/customization/texts.properties ./modules/hjmtj/main/
cp ./../web/customization/ValidationMessages.properties ./modules/hjmtj/main/
cp ./../web/customization/shiro.ini ./modules/hjmtj/main/

docker cp ./modules/ hjmtj-backend:/wildfly/
docker cp ./war/target/hjmtj.war hjmtj-backend:/wildfly/standalone/deployments

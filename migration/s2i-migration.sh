#!/bin/bash

echo 'Building migration ear using s2i'

cd ..

s2i build --rm \
-e MYSQL_DATASOURCE=MySQLDS \
-e MYSQL_SERVICE_HOST=hjalpmedelstjansten_db-hjmtj_1 \
-e MYSQL_SERVICE_PORT=3306 \
-e MYSQL_DATABASE=hjmtj \
-e MYSQL_USER=test \
-e MYSQL_PASSWORD=test \
\
-e DB_CONNECTION_URL=jdbc:mysql://hjalpmedelstjansten_db-hjmtj_1:3306/hjmtj \
-e DB_USERNAME=test \
-e DB_PASSWORD=test \
-e HJMTJ_ENVIRONMENT=local \
-e MYSQL_SCHEMA_GENERATION_ACTION=drop-and-create \
-e REDIS_HOST=hjmtj-redis \
-e HJMTJ_SERVICE_BASE_URL=http://localhost/ \
-e ELASTICSEARCH_URL=http://hjmtj-elasticsearch:9200 \
-e MYSQL_SCHEMA_GENERATION_ACTION=none \
\
-e PRODUCT_URL_HOST=ftp \
-e PRODUCT_URL_FILE=ISO9999Products_1_20180531_2031.zip \
-e PRODUCT_URL_USERNAME=user \
-e PRODUCT_URL_PASSWORD=password \
-e PRODUCT_URL_ENTRY=data.xml \
-e AGREEMENT_URL_FILE=HjalpmedelAgreements_5_20180920_0652.zip \
-e AGREEMENT_URL_ENTRY=data.xml \
-e SUPPLIERS_PW_URL_FILE=suppliers.pw \
-e CUSTOMERS_PW_URL_FILE=customers.pw \
-e MIGRATION_MYSQL_SCHEMA_GENERATION_ACTION=drop-and-create \
-e H2_SCHEMA_GENERATION_ACTION=drop-and-create \
-e OPENSHIFT_GEAR_MEMORY_MB=24576 \
-e JAVA_OPTS_EXT=-Djboss.as.management.blocking.timeout=600 \
-e ARTIFACT_DIR=migration/target \
. openshift/wildfly-140-centos7 hjmtj-backend

if [ $? -eq 0 ]; then
    echo 'Done! To start the built container run the following command:'
    echo "docker rm hjmtj-migration && docker run --publish-all --name hjmtj-backend --network hjalpmedelstjansten_default hjmtj-backend"
else
    echo FAIL
fi
/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.repository.persistence;

import static java.util.stream.Collectors.groupingBy;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import lombok.extern.slf4j.Slf4j;

/**
 * Bean for saving huge datasets.
 */
@Slf4j
public class BatchPersistence {

    private static final int BATCH_SIZE = 10000;

    public <T> void save(List<T> data, EntityManager em) {
        logInfo(data);

        for (int j = 0; j < data.size(); j++) {
            if (j > 0 && j % BATCH_SIZE == 0) {
                log.info("{}", j);
            }

            Object entity = data.get(j);
            em.persist(entity);
        }
        em.flush();
    }


    private <T> void logInfo(List<T> rootEntities) {
        Map<? extends Class<?>, List<Object>> byClazz = rootEntities.stream()
                                                                    .collect(groupingBy(Object::getClass));
        for (Map.Entry<? extends Class<?>, List<Object>> entry : byClazz.entrySet()) {
            log.info("Got {} {}", entry.getValue()
                                       .size(), entry.getKey()
                                                     .getSimpleName());
        }
    }


}
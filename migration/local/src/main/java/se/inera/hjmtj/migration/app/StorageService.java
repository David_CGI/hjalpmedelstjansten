package se.inera.hjmtj.migration.app;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Set;

import org.apache.commons.io.IOUtils;

public class StorageService {
	private final String fileStoragePath;

	public StorageService(String filePath) throws IOException {
		super();
		this.fileStoragePath = filePath;
		Path mypath = Paths.get(fileStoragePath);
		if (!Files.exists(mypath)) {
			Set<PosixFilePermission> permissions = PosixFilePermissions.fromString("rwxr--r--");
			FileAttribute<Set<PosixFilePermission>> fileAttributes = PosixFilePermissions.asFileAttribute(permissions);
			Files.createDirectory(mypath, fileAttributes);
			System.out.println("Directory created");
		} else {
			System.out.println("Directory already exists");
		}
	}

	public boolean checkIfImageExist(String filePath) {
		StringBuilder pathTpathToFile = new StringBuilder(fileStoragePath).append(java.io.File.separator).append(filePath);
		if (Files.exists(Paths.get(pathTpathToFile.toString()))) {
			return true;
		}
		return false;
	}

	public void updateStorageWithContent(String filePath, String contentType, InputStream targetStream) throws IOException {
		byte[] aByte = IOUtils.toByteArray(targetStream);
		FileOutputStream fos = new FileOutputStream(filePath);
		IOUtils.write(aByte, fos);
	}
}

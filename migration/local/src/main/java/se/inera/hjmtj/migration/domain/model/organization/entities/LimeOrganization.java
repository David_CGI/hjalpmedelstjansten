/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.organization.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PersistenceUnit;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * Created by jahwag on 2018-06-04.
 */

@PersistenceUnit(unitName = "local")
@Data
@Entity
public class LimeOrganization implements Serializable {

    private String serviceName;

    private LocalDate validFrom;

    private String connectionType;

    private String connection;

    private String status;

    @Column(columnDefinition = "TEXT")
    private String name;

    private String nameInvoice;

    private String category;

    private String organizationNumber;

    @Id
    private String gln;

    private String address;

    private String postCode;

    private String city;

    private String country;

    private String telephone;

}

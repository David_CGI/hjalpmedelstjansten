/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.repository.products;

import java.util.Collections;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.domain.model.product.entities.Product;
import se.inera.hjmtj.migration.domain.model.product.entities.ProductOrganization;

@Slf4j
public class ProductOrganizationRepository {

    @PersistenceContext(unitName = "local")
    private EntityManager em;


    public ProductOrganizationRepository(EntityManager em) {
    	this.em=em;
    }


	public Set<Product> findBySupplierId(long supplierId) {
        try {
            return em.createQuery("SELECT distinct po FROM ProductOrganization po " +
                    "LEFT JOIN FETCH po.products p " +
                    "LEFT JOIN FETCH p.supplier " +
                    "WHERE po.supplier IS NOT NULL AND " +
                    "po.supplier.id = :supplierId", ProductOrganization.class)
                     .setParameter("supplierId", supplierId)
                     .getSingleResult()	
                     .getProducts();
        } catch (NoResultException e) {
            return Collections.emptySet();
        }
    }

}

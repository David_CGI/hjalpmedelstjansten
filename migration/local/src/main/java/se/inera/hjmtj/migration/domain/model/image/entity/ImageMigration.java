package se.inera.hjmtj.migration.domain.model.image.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PersistenceUnit;

import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;

@PersistenceUnit(unitName = "local")
@Entity
public class ImageMigration {
    @Column()
	private String description;
    @Column(length = 500)
	private String filePath;
    @Column(length = 300)
	private String fileName;
    @Column()
	private Long catalogueUniqueNumber;
    @Column()
	private String gln;
    @Column()
    private boolean mainImage=false;
    @Column()
    private boolean documentImage=false;
    @Column()
    private String originalContentUrl;
    @Column()
    private String url;
    @Column(length = 100)
    private String contentType;
    @Column()
    private String documentType;
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long imageId;
    @OneToOne
	private Supplier supplier;
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Long getCatalogueUniqueNumber() {
		return catalogueUniqueNumber;
	}
	public void setCatalogueUniqueNumber(Long catalogueUniqueNumber) {
		this.catalogueUniqueNumber = catalogueUniqueNumber;
	}
	public String getGln() {
		return gln;
	}
	public void setGln(String gln) {
		this.gln = gln;
	}
	public Long getImageId() {
		return imageId;
	}
	public void setImageId(Long imageId) {
		this.imageId = imageId;
	}
	public boolean isMainImage() {
		return mainImage;
	}
	public void setMainImage(boolean mainImage) {
		this.mainImage = mainImage;
	}
	public String getOriginalContentUrl() {
		return originalContentUrl;
	}
	public void setOriginalContentUrl(String originalContentUrl) {
		this.originalContentUrl = originalContentUrl;
	}
	public boolean isDocumentImage() {
		return documentImage;
	}
	public void setDocumentImage(boolean documentImage) {
		this.documentImage = documentImage;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Supplier getSupplier() {
		return supplier;
	}
	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
}

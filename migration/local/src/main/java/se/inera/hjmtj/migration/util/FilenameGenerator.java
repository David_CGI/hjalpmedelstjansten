package se.inera.hjmtj.migration.util;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.io.FilenameUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FilenameGenerator {
	public String extractFileName(AtomicInteger count, String _srcContentUrl, String extension, String orgFileName) {
		String fileName = "";
		try {
			if (Optional.ofNullable(orgFileName).isPresent() && !orgFileName.trim().isEmpty()) {
				fileName = FilenameUtils.getName(orgFileName);
				if (fileName.isEmpty()) {
					fileName = "image_" + count.get() + extension;
				}
			} else {
				fileName = getFileNameFromContentUrl(_srcContentUrl);
				if (!Optional.ofNullable(fileName).isPresent()) {
					fileName = "image_" + count.get() + extension;
				}
			}
		} catch (Exception e) {
			log.error("Exception no fileName extracted for orgfilename {}" + orgFileName);
			log.error(e.getMessage());
		}
		return fileName;
	}

	private String getFileNameFromContentUrl(String _orgContentUrl) {
		String[] filePath = _orgContentUrl.split("/");
		if (filePath.length > 0) {
			String fileName = filePath[filePath.length - 1];
			return fileName;
		}
		return null;
	}
}

/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.repository.errors;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;

@Slf4j
public class MigrationErrorRepository {

    private final Set<MigrationError> migrationErrors = ConcurrentHashMap.newKeySet();

    private final Map<Long, Set<MigrationError>> catalogueUniqueNumberToErrors = new ConcurrentHashMap<>();

    public Set<MigrationError> values() {
        return migrationErrors;
    }



    public List<MigrationError> get(MigrationError.Severity severity, long catalogueUniqueNumber) {
        return catalogueUniqueNumberToErrors.getOrDefault(catalogueUniqueNumber, Collections.emptySet())
                                            .stream()
                                            .filter(t -> t.getSeverity()
                                                          .equals(severity))
                                            .collect(Collectors.toList());
    }

    public Set<MigrationError> get(long catalogueUniqueNumber) {
        return catalogueUniqueNumberToErrors.getOrDefault(catalogueUniqueNumber, Collections.emptySet());
    }

    public void add(MigrationError migrationError) {
        migrationErrors.add(migrationError);

        if (migrationError.getCatalogueUniqueNumber() != null) {
            catalogueUniqueNumberToErrors.computeIfAbsent(migrationError.getCatalogueUniqueNumber(), nil -> new LinkedHashSet<>())
                                         .add(migrationError);
        }
    }

}

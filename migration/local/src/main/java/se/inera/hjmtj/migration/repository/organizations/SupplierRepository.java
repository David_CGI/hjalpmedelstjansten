/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package se.inera.hjmtj.migration.repository.organizations;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.domain.model.Indexed;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;

@Slf4j
public class SupplierRepository {
	@PersistenceContext(unitName = "local")
	private EntityManager em;

	public SupplierRepository(EntityManager em) {
		this.em=em;
	}

	public List<Indexed<Supplier>> suppliers() {
		Indexed.Builder<Supplier> builder = Indexed.builder();
		return em.createNamedQuery(Supplier.FIND_ORDERED, Supplier.class).getResultList().stream().map(builder::build)
				.collect(Collectors.toList());
	}

	public List<Supplier> values(Predicate<Supplier> predicate) {
		return values().stream().filter(predicate).collect(Collectors.toList());
	}

	public List<Supplier> values() {
		return em.createNamedQuery(Supplier.FIND_ALL, Supplier.class).getResultList();
	}

	public boolean isEmpty() {
		return size() == 0;
	}

	private long size() {
		return em.createNamedQuery(Supplier.COUNT_ALL, Long.class).getSingleResult();
	}
}

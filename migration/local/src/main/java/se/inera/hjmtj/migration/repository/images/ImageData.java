package se.inera.hjmtj.migration.repository.images;

import lombok.Data;

@Data
public class ImageData {
	private byte[] data;
	private String contentType;
}

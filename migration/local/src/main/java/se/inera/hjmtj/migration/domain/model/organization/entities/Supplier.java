/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package se.inera.hjmtj.migration.domain.model.organization.entities;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PersistenceUnit;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by jahwag on 2018-06-04.
 */
@PersistenceUnit(unitName = "local")
@Slf4j
@Data
@Entity
@NamedQueries({
		@NamedQuery(name = Supplier.FIND_ORDERED, query = "SELECT S FROM Supplier s "
				+ "LEFT JOIN ProductOrganization po ON s.gln = po.gln " + "ORDER BY size(po.products) ASC"),
		@NamedQuery(name = Supplier.FIND_ALL, query = "SELECT o FROM Supplier o "),
		@NamedQuery(name = Supplier.COUNT_ALL, query = "SELECT COUNT(o) FROM Supplier o ") })
public class Supplier implements Serializable {
	public static final long VALID_FROM_DEFAULT_VALUE = LocalDate.parse("2100-01-01").toEpochDay();
	public static final String FIND_ORDERED = "Supplier.FIND_ALL_BIGGEST_FIRST";
	public static final String FIND_ALL = "Supplier.FIND_ALL";
	public static final String COUNT_ALL = "Supplier.COUNT_ALL";
	public static final String ORGANIZATION_TYPE_SUPPLIER = "SUPPLIER";
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String gln;
	private String name;
	private String type;
	private String address;
	private String postCode;
	private String city;
	private String country;
	private String telephone;
	private String fax;
	private String email;
	private String organizationNumber;

	public Supplier(VisueraOrganization visuera, LimeOrganization lime) {
		this(lime);
		this.fax = visuera.getFax();
		this.email = visuera.getEmail();
		if (city == null) {
			this.city = visuera.getCity();
			log.info(name + " city = " + visuera.getCity());
		}
		if (address == null) {
			this.address = visuera.getAddress();
			log.info(name + " address = " + visuera.getAddress());
		}
		if (postCode == null) {
			this.postCode = visuera.getPostCode();
			log.info(name + " postCode = " + visuera.getPostCode());
		}
		if (country == null) {
			this.country = visuera.getCountry();
			log.info(name + " country = " + visuera.getCountry());
		}
		if (organizationNumber == null) {
			this.organizationNumber = visuera.getOrganizationNumber();
			log.info(name + " organizationNumber = " + visuera.getOrganizationNumber());
		}
	}

	public Supplier(LimeOrganization csvLime) {
		String citySuffix = ", " + csvLime.getCity();
		this.gln = csvLime.getGln();
		this.name = csvLime.getName().replace(citySuffix, "");
		this.address = csvLime.getAddress();
		this.postCode = csvLime.getPostCode();
		this.city = csvLime.getCity();
		this.country = csvLime.getCountry();
		this.telephone = csvLime.getTelephone();
		this.type = "Leverantörer";
		this.organizationNumber = csvLime.getOrganizationNumber().replace("-", "");
	}

	public Supplier() {
	}

	private boolean isPostBox(String streetAddress) {
		if (streetAddress == null) {
			return false;
		}
		String lowerCase = streetAddress.toLowerCase();
		return lowerCase.contains("postfach") || lowerCase.contains("postbox") || lowerCase.contains("box") || lowerCase.contains("p.o");
	}
}
package se.inera.hjmtj.migration.app;

import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import se.inera.hjmtj.migration.domain.model.Indexed;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.domain.model.product.entities.Product;
import se.inera.hjmtj.migration.repository.organizations.SupplierRepository;
import se.inera.hjmtj.migration.repository.products.ProductOrganizationRepository;

public class LocalMigration {
	
	public static void main(String[] args) {
		 EntityManagerFactory  emf=Persistence.createEntityManagerFactory("local-images");
		 EntityManager em = emf.createEntityManager();
		 SupplierRepository supplierRepository = new SupplierRepository(em);
		 List<Indexed<Supplier>>  suppliers=supplierRepository.suppliers();
		 System.out.println("Number of suppliers is "+suppliers.size());
		 ProductOrganizationRepository productrepos = new ProductOrganizationRepository(em);
		 suppliers.iterator().forEachRemaining( image -> {
			 Set<Product> products =productrepos.findBySupplierId( image.value().getId() );
			 System.out.println("Number of proucts is "+products.size());

		 });

	}
}

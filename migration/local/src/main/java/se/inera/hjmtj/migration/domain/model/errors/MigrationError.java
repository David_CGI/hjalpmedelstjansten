/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.errors;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

@Data
public class MigrationError implements Serializable {

    private static final AtomicLong nextId = new AtomicLong();

    private final long id = nextId.getAndIncrement();

    private Severity severity = Severity.ERROR;

    private Classifier classifier;

    private String organizationName;

    private Long catalogueUniqueNumber;

    private String message;

    private LocalDateTime timestamp;

    @Override
    public int hashCode() {
        return Objects.hash(severity, classifier, organizationName, catalogueUniqueNumber, message);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MigrationError that = (MigrationError) o;
        return severity == that.severity &&
                Objects.equals(classifier, that.classifier) &&
                Objects.equals(organizationName, that.organizationName) &&
                Objects.equals(catalogueUniqueNumber, that.catalogueUniqueNumber) &&
                Objects.equals(message, that.message);
    }

    public enum Severity {
        WARNING("Varning"),
        ERROR("Allvarlig");

        private final String name;

        Severity(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}

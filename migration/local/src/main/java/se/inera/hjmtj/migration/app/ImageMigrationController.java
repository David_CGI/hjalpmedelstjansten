package se.inera.hjmtj.migration.app;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.domain.model.Indexed;
import se.inera.hjmtj.migration.domain.model.errors.Classifier;
import se.inera.hjmtj.migration.domain.model.errors.MigrationError;
import se.inera.hjmtj.migration.domain.model.organization.entities.Supplier;
import se.inera.hjmtj.migration.domain.model.product.entities.Product;
import se.inera.hjmtj.migration.httpclient.ImageHttpClient;
import se.inera.hjmtj.migration.repository.errors.MigrationErrorRepository;
import se.inera.hjmtj.migration.repository.images.ImageData;
import se.inera.hjmtj.migration.repository.organizations.SupplierRepository;
import se.inera.hjmtj.migration.repository.products.ProductOrganizationRepository;
import se.inera.hjmtj.migration.util.FilenameGenerator;

@Slf4j
public class ImageMigrationController {
	private ImageHttpClient httpClient;
	private MigrationErrorRepository migrationErrorRepository;
	@PersistenceContext(unitName = "local")
	private EntityManager em;
	private AtomicInteger count = new AtomicInteger(0);
	private SupplierRepository supplierRepository;
	private ProductOrganizationRepository productOrgRepos;
	private FilenameGenerator filenameGenerator;
	private StorageService fileService;

	public static Predicate<Product> mainImage() {
		return product -> product.getMainImage() != null;
	}

	public ImageMigrationController() {
	}

	public int startImageMigrationPerSupplierForS3() {
		count.set(0);
		for (Indexed<Supplier> supplier : supplierRepository.suppliers()) {
			try {
				saveImagePerSupplier(supplier);
				log.info("number of images migrated is " + count.get());
			} catch (Exception e) {
				MigrationError migrationError = new MigrationError();
				migrationError.setClassifier(Classifier.HJALPMEDEL_IMAGE);
				migrationError.setMessage(e.getMessage());
				migrationError.setOrganizationName(supplier.value().getName());
				migrationErrorRepository.add(migrationError);
			}
		}
		log.info("total number of images migrated is " + count.get());
		return count.get();
	}

	private boolean createImageContent(Supplier supplier, Product product, String url, String description, String _srcContentUrl,
			String orgFileName, boolean isDoc, boolean isMainImage, String contentType, String docType) {
		List<String> externalKey = buildExternalKey(product);
		try {
			String extension = getFileExtension(contentType);
			String fileName = filenameGenerator.extractFileName(count, _srcContentUrl, extension, orgFileName);
			String filePath = generateFilePath(externalKey.get(0), externalKey.get(1), fileName);
			if (!fileService.checkIfImageExist(filePath)) {
				Optional<ImageData> imageData = httpClient.getImage(url);
				if (imageData.isPresent()) {
					// Load data
					ImageData imageDataLoaded = imageData.get();
					byte[] data = imageDataLoaded.getData();
					InputStream targetStream = new ByteArrayInputStream(data);
					log.info("creating content for file visuera url {} with filepath {} ", url, filePath);
					fileService.updateStorageWithContent(filePath, contentType, targetStream);
					count.getAndIncrement();
					return true;
				} else {
					log.info("couldnt load image for url {} and catalog productnumber {}", product.getMainImage().getOriginalContentUrl(),
							product.getVgrProdNo());
				}
			} else {
				log.info("content exist for filepath {} with catalog productnumber {} and URL {}", filePath, product.getVgrProdNo(), url);
			}
		} catch (Exception e) {
			log.error("exception thrown " + e.getMessage());
			createMigrationError(Long.valueOf(product.getVgrProdNo()));
		}
		return false;
	}

	public void saveImagePerSupplier(Indexed<Supplier> indexedSupplier) {
		Supplier value = indexedSupplier.value();
		AtomicInteger countOfImage = new AtomicInteger(0);
		log.info("migerating supperlier with gln {} ", value.getGln());
		Set<Product> products = productOrgRepos.findBySupplierId(value.getId());
		if (Optional.ofNullable(products).isPresent()) {
			log.info("for supplier total number of products is {}", products.size());
			products.stream().filter(img -> img.getMainImage() != null).filter(prod -> prod.getXmlCategories().isISO9999())
					.forEach(product -> {
						try {
							boolean imageCreated = createImageContent(value, product, product.getMainImage().getOriginalContentUrl(),
									product.getMainImage().getFileDescription(), product.getMainImage().getSourceURL(),
									product.getMainImage().getOriginalFileName(), false, true, product.getMainImage().getContentType(), "");
							createAdditionalImageContent(value, product);
							createAdditionalDocContent(value, product);
						} catch (Exception e) {
							if (Optional.ofNullable(product.getMainImage()).isPresent()) {
								log.error("exception in image save per product with productnumber {} and main image url {}",
										product.getVgrProdNo(), product.getMainImage().getOriginalContentUrl());
								log.error(e.getMessage());
							} else {
								log.error("exception in image save per product with productnumber {}", product.getVgrProdNo());
							}
						}
					});
		}
	}

	private boolean createAdditionalImageContent(Supplier value, Product product) {
		if (Optional.ofNullable(product.getImages()).isPresent()) {
			product.getImages().stream().forEach(prodImage -> {
				try {
					if (Optional.ofNullable(prodImage.getFile()).isPresent()) {
						String url = prodImage.getFile().getOriginalContentUrl();
						boolean imageCreated = createImageContent(value, product, url, prodImage.getFile().getFileDescription(),
								prodImage.getFile().getSourceURL(), prodImage.getFile().getOriginalFileName(), false, false,
								prodImage.getFile().getContentType(), "");
					}
				} catch (Exception e) {
					log.error("Exception thrown in create additional image content {} ", e.getMessage());
				}
			});
		}
		return true;
	}

	private boolean createAdditionalDocContent(Supplier value, Product product) {
		if (Optional.ofNullable(product.getDocuments()).isPresent()) {
			product.getDocuments().stream().forEach(prodDoc -> {
				try {
					if (Optional.ofNullable(prodDoc.getFile()).isPresent()) {
						String url = prodDoc.getFile().getOriginalContentUrl();
						String docType = "";
						if (Optional.ofNullable(prodDoc.getDocumentType()).isPresent()) {
							docType = prodDoc.getDocumentType().getCode();
						}
						boolean imageCreated = createImageContent(value, product, url, prodDoc.getFile().getFileDescription(),
								prodDoc.getFile().getSourceURL(), prodDoc.getFile().getOriginalFileName(), true, false,
								prodDoc.getFile().getContentType(), docType);
					}
				} catch (Exception e) {
					log.error("Exception thrown in create additional docment content {} ", e.getMessage());
				}
			});
		}
		return true;
	}

	private String getFileNameFromContentUrl(String _orgContentUrl) {
		String[] filePath = _orgContentUrl.split("/");
		if (filePath.length > 0) {
			String fileName = filePath[filePath.length - 1];
			return fileName;
		}
		return null;
	}

	private String getFileExtension(String contentType) {
		if (contentType.equals("image/jpeg")) {
			return ".jpg";
		} else if (contentType.equals("application/pdf")) {
			return ".pdf";
		} else if (contentType.equals("image/gif")) {
			return ".gif";
		} else if (contentType.equals("image/png")) {
			return ".png";
		} else if (contentType.equals("application/vnd.ms-excel")) {
			return ".xls";
		} else if (contentType.equals("application/msword")) {
			return ".doc";
		} else if (contentType.equals("image/pjpeg")) {
			return ".jpg";
		}
		return null;
	}

	private List<String> buildExternalKey(Product product) {
		String gln = product.getSupplier().getGln();
		String productNumberSupplier = product.getSupplierProductNumber();
		StringBuilder folderOrg;
		if (Optional.ofNullable(product.getSupplier().getEconomicInformation()).isPresent()) {
			String orgNr = product.getSupplier().getEconomicInformation().getOrganizationNo();
			folderOrg = new StringBuilder(gln).append(orgNr);
		} else {
			String id = product.getSupplier().getIdentifier();
			folderOrg = new StringBuilder(gln).append(id);
		}
		String uuidFolder = UUID.nameUUIDFromBytes(folderOrg.toString().getBytes()).toString();
		StringBuilder folderProd = new StringBuilder(folderOrg).append(productNumberSupplier);
		String artFolder = UUID.nameUUIDFromBytes(folderProd.toString().getBytes()).toString();
		List<String> valueList = new ArrayList<>();
		valueList.add(uuidFolder);
		valueList.add(artFolder);
		return valueList;
	}

	private void createMigrationError(Long articleUniqueId) {
		MigrationError migrationError = new MigrationError();
		migrationError.setClassifier(Classifier.HJALPMEDEL_IMAGE);
		migrationError.setCatalogueUniqueNumber(articleUniqueId);
		migrationErrorRepository.add(migrationError);
	}

	private String generateFilePath(String organization, String product, String filename) {
		return organization + "/" + product + "/" + filename;
	}

	public int getNumberOfImagesMigrated() {
		return count.get();
	}
}

package se.inera.hjmtj.migration.repository.images;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import lombok.extern.slf4j.Slf4j;
import se.inera.hjmtj.migration.domain.model.image.entity.ImageMigration;

@Slf4j
public class ImageMigrationRepository {
	@PersistenceContext(unitName = "local")
	private EntityManager em;

	public Optional<List<ImageMigration>> findAllImageMigrationBasedOnSupplier(Long supplierId) {
		try {
			return Optional.of(em.createQuery("SELECT b FROM ImageMigration b where b.supplier.id = :supplierId", ImageMigration.class)
					 .setParameter("supplierId", supplierId)
					 .getResultList()
	                 .stream()
	                 .collect(Collectors.toList()));
		} catch (NoResultException e) {
			return Optional.empty();
		}
	}

	public Optional<List<ImageMigration>> findAllImageMigration() {
		try {
			return Optional.of(em.createQuery("SELECT b FROM ImageMigration b ", ImageMigration.class)
					 .getResultList()
	                 .stream()
	                 .collect(Collectors.toList()));
		} catch (NoResultException e) {
			return Optional.empty();
		}
	}

	
}
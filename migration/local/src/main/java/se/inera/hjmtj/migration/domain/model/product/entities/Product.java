/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package se.inera.hjmtj.migration.domain.model.product.entities;

import static javax.persistence.CascadeType.ALL;

import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PersistenceUnit;
import javax.persistence.Table;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@PersistenceUnit(unitName = "local")
@Slf4j
@Data
@Entity
@Table(indexes = { @Index(columnList = "id"), @Index(columnList = "isTemplateProduct"), @Index(columnList = "basedOnTemplateProduct"),
		@Index(columnList = "id,basedOnTemplateProduct"), @Index(columnList = "id,basedOnTemplateProduct,vgrProdNo"), })
public class Product {
	private Long id;
	@Id
	private String vgrProdNo;
	@Embedded
	private ProductFile mainImage;
	@ManyToOne
	private ProductOrganization supplier;
	@Column(columnDefinition = "VARCHAR", length = 1024)
	private String supplierProductName;
	private String supplierProductNumber;
	@OneToMany(fetch = FetchType.EAGER, cascade = ALL)
	private Set<ProductImage> images;
	@OneToMany(fetch = FetchType.EAGER, cascade = ALL)
	private Set<ProductDocument> documents;
	private String manufacturerName;
	private String manufacturerProductNumber;
	@Embedded
	private ProductCategoryPath xmlCategories;

	public Product() {
	}

	public void setSupplier(ProductOrganization supplier) {
		this.supplier = supplier;
	}

	@Override
	public int hashCode() {
		return Objects.hash(vgrProdNo);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Product product = (Product) o;
		return Objects.equals(vgrProdNo, product.vgrProdNo);
	}
}

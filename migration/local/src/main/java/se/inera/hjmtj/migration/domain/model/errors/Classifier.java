/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjmtj.migration.domain.model.errors;

import lombok.Value;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

@Slf4j
@Value
@Accessors(fluent = true)
public class Classifier {

    public static final Classifier CUSTOMER = new Classifier(null, "CUSTOMER");

    public static final Classifier SUPPLIER = new Classifier(null, "SUPPLIER");

    public static final Classifier SUPPLIER_IMAGES = new Classifier(SUPPLIER, "SUPPLIER_IMAGES");

    // Agreement
    public static final Classifier AGREEMENT = new Classifier(null, "AGREEMENT");

    public static final Classifier AGREEMENT_DELIVERY_TIME = new Classifier(AGREEMENT, "AGREEMENT_DELIVERY_TIME");

    public static final Classifier AGREEMENT_WARRANTY_UNIT = new Classifier(AGREEMENT, "AGREEMENT_WARRANTY_UNIT");

    public static final Classifier AGREEMENT_WARRANTY_QUANTITY = new Classifier(AGREEMENT, "AGREEMENT_WARRANTY_QUANTITY");

    // Pricelist
    public static final Classifier PRICELIST = new Classifier(AGREEMENT, "PRICELIST");

    public static final Classifier PRICELIST_ROW = new Classifier(null, "PRICELIST_ROW");

    public static final Classifier PRICELIST_ROW_WARRANTY_QUANTITY = new Classifier(PRICELIST_ROW, "PRICELIST_ROW_WARRANTY_QUANTITY");

    public static final Classifier PRICELIST_ROW_LEAST_ORDER_QUANTITY = new Classifier(PRICELIST_ROW, "PRICELIST_ROW_LEAST_ORDER_QUANTITY");

    public static final Classifier PRICELIST_ROW_ARTICLE = new Classifier(PRICELIST_ROW, "PRICELIST_ROW_ARTICLE");

    // GP
    public static final Classifier GP = new Classifier(null, "GP");

    public static final Classifier GP_DELIVERY_TIME = new Classifier(GP, "GP_DELIVERY_TIME");

    public static final Classifier GP_WARRANTY_UNIT = new Classifier(GP, "GP_WARRANTY_UNIT");

    public static final Classifier GP_WARRANTY_QUANTITY = new Classifier(GP, "GP_WARRANTY_QUANTITY");

    // GP Pricelist
    public static final Classifier GP_PRICELIST = new Classifier(GP, "GP_PRICELIST");

    public static final Classifier GP_PRICELIST_ROW = new Classifier(null, "GP_PRICELIST_ROW");

    public static final Classifier GP_PRICELIST_ROW_WARRANTY_QUANTITY = new Classifier(GP_PRICELIST_ROW, "GP_PRICELIST_ROW_WARRANTY_QUANTITY");

    public static final Classifier GP_PRICELIST_ROW_LEAST_ORDER_QUANTITY = new Classifier(GP_PRICELIST_ROW, "GP_PRICELIST_ROW_LEAST_ORDER_QUANTITY");

    public static final Classifier GP_PRICELIST_ROW_ARTICLE = new Classifier(GP_PRICELIST_ROW, "GP_PRICELIST_ROW_ARTICLE");

    // Hjalpmedel
    public static final Classifier HJALPMEDEL = new Classifier(null, "HJALPMEDEL");

    public static final Classifier HJALPMEDEL_CATALOGUE_UNIQUE_NUMBER = new Classifier(HJALPMEDEL, "HJALPMEDEL_CATALOGUE_UNIQUE_NUMBER");

    public static final Classifier HJALPMEDEL_FIT = new Classifier(HJALPMEDEL, "HJALPMEDEL_FIT");

    public static final Classifier HJALPMEDEL_REPLACES = new Classifier(HJALPMEDEL, "HJALPMEDEL_REPLACES");

    public static final Classifier HJALPMEDEL_REPLACED_BY = new Classifier(HJALPMEDEL, "HJALPMEDEL_REPLACED_BY");

    public static final Classifier HJALPMEDEL_GTIN13 = new Classifier(HJALPMEDEL, "HJALPMEDEL_GTIN13");

    public static final Classifier HJALPMEDEL_CATEGORY = new Classifier(HJALPMEDEL, "HJALPMEDEL_CATEGORY");

    public static final Classifier HJALPMEDEL_ARTICLE_NUMBER = new Classifier(HJALPMEDEL, "HJALPMEDEL_ARTICLE_NUMBER");

    public static final Classifier HJALPMEDEL_PRODUCT_NUMBER = new Classifier(HJALPMEDEL, "HJALPMEDEL_PRODUCT_NUMBER");

    public static final Classifier HJALPMEDEL_IMAGE = new Classifier(HJALPMEDEL, "HJALPMEDEL_IMAGE");

    // Category-specific property
    public static final Classifier CATEGORY_SPECIFIC_PROPERTY = new Classifier(null, "CATEGORY_SPECIFIC_PROPERTY");

    public static final Classifier CATEGORY_SPECIFIC_PROPERTY_LISTVALUE = new Classifier(CATEGORY_SPECIFIC_PROPERTY, "CATEGORY_SPECIFIC_PROPERTY_LISTVALUE");

    // Kodverk
    public static final Classifier CV = new Classifier(null, "CV");

    public static final Classifier CV_CE_DIRECTIVE = new Classifier(CV, "CV_CE_DIRECTIVE");

    public static final Classifier CV_CE_STANDARD = new Classifier(CV, "CV_CE_STANDARD");

    private static final Properties PROPERTIES = new Properties();

    static {
        try {
            InputStream input = Classifier.class.getResourceAsStream("/errors.properties");
            PROPERTIES.load(new InputStreamReader(input, StandardCharsets.UTF_8));
        } catch (IOException e) {
            log.error("Failed to load resource bundle", e);
        }
    }

    private final Classifier parent;

    private final String propertyKey;

    public static Classifier field(Classifier parent, String field) {
        return new Classifier(parent, field == null || field.isEmpty() ? "?" : field);
    }

    @Override
    public String toString() {
        return PROPERTIES.getProperty(propertyKey.toUpperCase(), propertyKey);
    }

}

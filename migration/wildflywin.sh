#!/usr/bin/env bash

docker kill hjmtj-backend
docker rm hjmtj-backend

s2i build https://github.com/openshift/openshift-jee-sample.git \
-e TZ=Europe/Stockholm \
-e MYSQL_DATASOURCE=MySQLDS \
-e MYSQL_SERVICE_HOST=hjalpmedelstjansten_db-hjmtj_1 \
-e MYSQL_SERVICE_PORT=3306 \
-e MYSQL_DATABASE=hjmtj \
-e MYSQL_USER=test \
-e MYSQL_PASSWORD=test \
\
-e DB_CONNECTION_URL=jdbc:mysql://hjalpmedelstjansten_db-hjmtj_1:3306/hjmtj?rewriteBatchedStatements=true \
-e DB_USERNAME=test \
-e DB_PASSWORD=test \
-e HJMTJ_ENVIRONMENT=local \
-e REDIS_HOST=hjmtj-redis \
-e HJMTJ_SERVICE_BASE_URL=http://localhost/ \
-e ELASTICSEARCH_URL=http://hjmtj-elasticsearch:9200 \
-e ELASTICSEARCH_INDEX_NAME=product_test \
-e ELASTICSEARCH_MIN_SCORE=0.5 \
-e MYSQL_SCHEMA_GENERATION_ACTION=none \
-e PRODUCT_URL_HOST=ftp \
-e PRODUCT_URL_FILE=ISO9999Products_12_20181105_1710.zip \
-e PRODUCT_URL_USERNAME=user \
-e PRODUCT_URL_PASSWORD=password \
-e GP_URL_FILE=GPAgreements_10_20181105_1517.zip \
-e ASSORTMENT_URL_FILE=ASSORTMENT_NotVGRInternt_9_20181105_1505.zip \
-e GP_URL_ENTRY=data.xml \
-e AGREEMENT_URL_FILE=HjalpmedelAgreements_11_20181105_1532.zip \
-e AGREEMENT_URL_ENTRY=data.xml \
-e SUPPLIERS_PW_URL_FILE=suppliers.pw \
-e CUSTOMERS_PW_URL_FILE=customers.pw \
-e MIGRATION_LOCAL_DS=java:jboss/datasources/ExampleDS2 \
-e MIGRATION_MYSQL_SCHEMA_GENERATION_ACTION=drop-and-create \
-e H2_SCHEMA_GENERATION_ACTION=update \
-e OPENSHIFT_GEAR_MEMORY_MB=24576 \
-e JAVA_OPTS_EXT=-Djboss.as.management.blocking.timeout=2147483 \
-e JAVA_OPTS_APPEND=-XX:+UseParallelGC \
-e JAVA_OPTS_APPEND=-XX:MinHeapFreeRatio=5 \
-e JAVA_OPTS_APPEND=-XX:MaxHeapFreeRatio=10 \
-e JAVA_OPTS_APPEND=-XX:GCTimeRatio=4 \
-e JAVA_OPTS_APPEND=-XX:AdaptiveSizePolicyWeight=90 \
-e ARTIFACT_DIR=migration/target \
openshift/wildfly-140-centos7 wildflytest

docker run -p 8080:8080 -p 9990:9990 -v c:/Users/henaxe/h2:/h2 --name hjmtj-backend --network hjalpmedelstjansten_default wildflytest
#!/usr/bin/env bash

if [ -z $1 ]; then
    echo 'path required'
    exit 1
fi

docker run -d -v $1:/home/vsftpd \
                -p 20:20 -p 21:21 -p 47400-47470:47400-47470 \
                -e FTP_USER=user \
                -e FTP_PASS=password \
                -e PASV_ADDRESS=127.0.0.1 \
                --name ftp \
                --network hjalpmedelstjansten_default \
                --restart=always bogem/ftp
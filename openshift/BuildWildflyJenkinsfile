#!groovy

def appReleaseName = "${APP_NAME}-${RELEASE_VERSION}"
def dbName = "${appReleaseName}-${new Random().nextInt() % 1000}".replace('-', '')
def tmpDir = "${appReleaseName}-tmp"
// default values
def gitUrl = "${GIT_URL}"
def gitDevopsDir = "${GIT_DEVOPS_DIR}"
def buildVersion = "" // From build-info.json
def commonVersion = "NA"
def infraVersion = "NA"

// Parameters for backing service. Scope global so we can clean up
def backingService = ""
def backingServiceName = ""
def backingServiceAppName = ""
def backingServiceAppVersion = ""
def backingServiceDbName = ""

def propagateTo = []
def artifactFolder = "target"
def applicationZip = "${artifactFolder}/${appReleaseName}-${BUILD_NUMBER}.tar.gz"
def versionFlags = ""
def testList = []
def buildArgs = "clean build"
def latestTag = "${RELEASE_VERSION}.latest"

// run shell command, return status code
def shell(cmd) {
  sh(
          script: cmd,
          returnStatus: true
  )
}

// run shell command and signal an error on failure
def eshell(cmd) {
  def rc = shell(cmd)
  if (rc != 0) {
    error("Error: shell command exited with error code ${rc}")
  }
}

//
def log(msg) {
  echo "${new Date()} -- ${msg}"
}

def promoteImage(appName, appTag) {
  def imageStream = openshift.selector("is", "${appName}-verified").object().status.dockerImageRepository
  log "Use ImageStream: ${imageStream}"

  def model = openshift.process("-f", "openshift/buildtemplate-nexus.yaml",
          "-p", "APP_NAME=${appName}",
          "-p", "NEXUS_NAME=${appName}",
          "-p", "IMAGE='${imageStream}'",
          "-p", "STAGE=test",
          "-p", "TAG='${appTag}'")
  openshift.apply(model)
  log "Promote image to nexus..."
  def build = openshift.selector("bc", "${appName}-nexus").startBuild()
  log "${build.name()} started"
  def status = ""
  timeout (20) {
    build.untilEach(1) {
      status = it.object().status.phase
      echo "Build status: ${status}"
      return !["Running", "Pending", "New"].contains(status)
    }

    echo "Logs for ${appName}-nexus build"
    build.logs()
    if (status != "Complete") {
      error("Build terminated with status: ${status}")
    }

    log "${appName}:${appTag} promoted to Nexus"
  }
}



pipeline {
  agent { label 'maven' }

  environment {
    JAVA_TOOL_OPTIONS = '-Dfile.encoding=UTF-8'
  }

  stages {

    stage('Checkout Source') {
      steps {
        script {
          // Checkout application source
          git url: "${gitUrl}", branch: GIT_BRANCH
          def info = readJSON file: 'build-info.json'
          buildVersion = "${info.appVersion}.${BUILD_NUMBER}"

          versionFlags = "-DbuildVersion=${buildVersion} -DcommonVersion=${commonVersion} -DinfraVersion=${infraVersion}"
        }
      }
    }

    stage('Prepare') {
      steps {
        script {
          currentBuild.displayName = "${appReleaseName} ${buildVersion}"

          echo "Prepare configuration and check pre-conditions"
          openshift.withCluster() {
            openshift.withProject() {
              log "Pipeline started with:"
              echo "gitUrl: ${gitUrl}, gitBranch: ${GIT_BRANCH}"
              echo "buildVersion: ${buildVersion}"
              echo "backingService: ${backingService}"
              echo "gitDevopsDir: ${gitDevopsDir}"

              echo "GIT_DEVOPS_DIR: ${GIT_DEVOPS_DIR}"
              echo "APP_NAME: ${APP_NAME}"
              echo "RELEASE_VERSION: ${RELEASE_VERSION}"
              echo "STAGE: ${STAGE}"
              echo "BUILD_TEMPLATE: ${BUILD_TEMPLATE}"
              echo "DEPLOY_TEMPLATE: ${DEPLOY_TEMPLATE}"
              echo "ARTIFACT_IMAGE_SUFFIX: ${ARTIFACT_IMAGE_SUFFIX}"
              echo "GIT_URL: ${GIT_URL}"
              echo "GIT_CI_BRANCH: ${GIT_CI_BRANCH}"
              echo "TEST_PORT: ${TEST_PORT}"


              echo "DEPLOY_TEMPLATE: ${GIT_BRANCH}"
              echo "ARTIFACT_IMAGE_SUFFIX: ${buildVersion}"
              // check envrionment
              //[GIT_DEVOPS_DIR, APP_NAME, RELEASE_VERSION, STAGE, BUILD_TEMPLATE, DEPLOY_TEMPLATE, TESTRUNNER_TEMPLATE, ARTIFACT_IMAGE_SUFFIX, GIT_URL, GIT_CI_BRANCH, TEST_PORT, CONTEXT_PATH, HEALTH_URI, GIT_BRANCH, buildVersion].each {
              [GIT_DEVOPS_DIR, APP_NAME, PING_PATH, RELEASE_VERSION, STAGE, BUILD_TEMPLATE, DEPLOY_TEMPLATE, ARTIFACT_IMAGE_SUFFIX, GIT_URL, GIT_CI_BRANCH, TEST_PORT, GIT_BRANCH].each {
                if (!it) {
                  error("No such required context variable in environment.")
                }
              }

              echo "All environment variables in place!"
              sh "ls -la ${pwd()}"
              //sh "ls -la ${gitDevopsDir}/openshift/"

              // Make sure required template files exists in devops repo
              //["${BUILD_TEMPLATE}", "${DEPLOY_TEMPLATE}", "${TESTRUNNER_TEMPLATE}"].each {
              ["${BUILD_TEMPLATE}", "${DEPLOY_TEMPLATE}"].each {
                if (!fileExists("${pwd()}/openshift/${it}")) {
                  error("Unable to find template file ${pwd()}/openshift/${it}")
                }
              }

            }
          }
        }
      }
    }
    stage('Build Wildfly Binaries')
    {

      steps {
        script {
          def applicationDir = "."
          if (!fileExists("${artifactFolder}")) {
            eshell("mkdir -p ${artifactFolder}")
          }
          try {
            log("Starting maven build")
            sh "ls -la ${pwd()}"

            sh "mvn clean install -DskipTests"
            //"${BUILD_TOOL}" "${buildArgs} ${versionFlags}"
          }

          finally {
            publishHTML([
                    allowMissing         : true,
                    alwaysLinkToLastBuild: true,
                    keepAll              : true,
                    reportDir            : 'build/reports/allTests',
                    reportFiles          : 'index.html',
                    reportName           : 'JUnitTests',
            ])
          }
          //eshell("tar --exclude ${artifactFolder} --exclude ${tmpDir} --exclude ${gitDevopsDir} --exclude .git -chzf ${applicationZip} ${applicationDir}")
          archiveArtifacts artifacts: "${ARTIFACT_DIR}/*.war", excludes: null, onlyIfSuccessful: true //todo artifakt stuff

        }
      }

      //mvn clean install -DskipTests
    }

    stage('Setup Image Builds') {
      steps {
        script {
          openshift.withCluster() {
            openshift.withProject() {
              def models = openshift.process("-f", "${pwd()}/openshift/${BUILD_TEMPLATE}",
                      "-p", "IS_APP_NAME=${APP_NAME}",
                      "-p", "APP_NAME=${appReleaseName}",
                      "-p", "STAGE=${STAGE}",
                      "-p", "ARTIFACT_DIR=${ARTIFACT_DIR}",
                      "-p", "GIT_URL=${GIT_URL}",
                      "-p", "GIT_BRANCH=${GIT_BRANCH}",
                      "-p", "BUILD_VERSION=${buildVersion}",
                      "-p", "ARTIFACT_IMAGE_SUFFIX=${ARTIFACT_IMAGE_SUFFIX}")
              openshift.apply(models)
            }
          }
        }
      }
    }

    stage('Create S2I Runtime Image') {
      steps {
        script {
          openshift.withCluster() {
            openshift.withProject() {
              log "Build artifact image ${APP_NAME}-${ARTIFACT_IMAGE_SUFFIX}"
              def build = openshift.selector("bc", "${appReleaseName}-${ARTIFACT_IMAGE_SUFFIX}").startBuild()
              sleep(time: 1, unit: 'SECONDS')
              echo "${build.name()} started"
              timeout(30) {
                def status = ""
                build.untilEach(1) {
                  status = it.object().status.phase
                  echo "Build status: ${status}"
                  return !["Running", "Pending", "New"].contains(status)
                }
                echo "Logs for artifact build"
                build.logs()
                if (status != "Complete") {
                  error("Build terminated with status: ${status}")
                }
                echo "Build Completed"
              }
            }
          }
        }
      }
    }

    stage('Setup Deploy Config') {
      steps {
        script {
          openshift.withCluster() {
            openshift.withProject() {
              //hjmtj-backend-1.8.0
              //
              shell("oc delete configmap ${APP_NAME}-properties")
              shell("oc delete configmap ${APP_NAME}-env")

              def wd = "cd openshift; "
              eshell(wd + "oc create configmap ${APP_NAME}-env --from-env-file=${STAGE}/${APP_NAME}/configmap-env.properties")
              eshell(wd + "oc create configmap ${APP_NAME}-properties --from-file=${STAGE}/${APP_NAME}/config/")

            }
          }
        }
      }
    }
    stage('Deploy Wildfly app') {
      steps {
        script {
          openshift.withCluster() {
            openshift.withProject() {
              def imageStream = openshift.selector("is", "${APP_NAME}-${ARTIFACT_IMAGE_SUFFIX}").object().status.dockerImageRepository
              echo "Use ImageStream: ${imageStream}"
              def deployModel = openshift.process("-f", "${pwd()}/openshift/${DEPLOY_TEMPLATE}",
                      "-p", "APP_NAME=${APP_NAME}",
                      "-p", "IMAGE='$imageStream:${buildVersion}'",
                      "-p", "STAGE=${STAGE}",
                      "-p", "PING_PATH=${PING_PATH}",
                      "-p", "MOUNT_POINT=${MOUNT_POINT}")
              log "Wait for rollout (limited to 5 minutes)"
              timeout(50) {
                def rm = openshift.apply(deployModel).narrow("dc").rollout()
                log "Waiting for deployment to complete"
                rm.status()
                log "Deployment done."
              }
            }
          }
        }
      }

    }
    stage('Store Verified Image') {
      steps {
        script {
          openshift.withCluster() {
            openshift.withProject() {
              def source2 = "${APP_NAME}-artifact"
              def target = "${APP_NAME}-verified"
              echo "Promote image ${source2} to imagestream ${target}"
              openshift.tag("${source2}:${buildVersion}", "${target}:${buildVersion}", "${target}:${latestTag}")
            }
          }
        }
      }
    }
    stage('Promote to Nexus') {
      steps {
        script {
          openshift.withCluster() {
            openshift.withProject() {
              promoteImage("${APP_NAME}", "${buildVersion}")
            }
          }
        }
      }
    }
  }
}

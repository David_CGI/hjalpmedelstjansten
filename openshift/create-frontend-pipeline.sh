#!/bin/sh

#oc login https://ind-ocpt1a-api.ocp.sth.basefarm.net/console
# - enter credentials
# - after that change project to dhjm with the following command. And change version number to the appropriate version
#oc project dhjm

VERSION="1.9.0"
DEPLOYENV="dev"
READYNESS_PATH="/hjmtj-export/resources/v1/ping"
BRANCH="develop"

oc process -f pipelinetemplate-build-nginx.yaml -p APP_NAME=hjmtj-web -p RELEASE_VERSION=$VERSION -p STAGE=$DEPLOYENV -p BUILD_TEMPLATE=buildtemplate-nginx-s2i.yaml -p DEPLOY_TEMPLATE=deploytemplate-nginx-s2i.yaml -p ARTIFACT_IMAGE_SUFFIX=artifact -p GIT_URL=https://bitbucket.org/ineraservices/hjalpmedelstjansten.git -p GIT_CI_BRANCH=$BRANCH -p TEST_PORT=8081 -p CONTEXT_PATH=/ -p HEALTH_URI=/  | oc apply -f -
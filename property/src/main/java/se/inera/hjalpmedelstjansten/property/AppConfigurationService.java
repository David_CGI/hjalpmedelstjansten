/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.property;

import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;

import javax.ejb.Singleton;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Properties;

/**
 * Reads configuration files and exposes them with @Produces allowing classes to
 * get properties by writing<br /><br />
 * 
 * @Inject
 * String theNameOfTheStringPropertyAsItSaysInPropertyFile;
 * 
 * <br /><br />or<br /><br />
 * 
 * @Inject
 * boolean theNameOfTheBooleanPropertyAsItSaysInPropertyFile
 * 
 * <br /><br />or<br /><br />
 * 
 * @Inject
 * boolean theNameOfTheIntPropertyAsItSaysInPropertyFile
 *
 * @Inject
 * Date theNameOfTheDatePropertyAsItSaysInPropertyFile
 * 
 * @author Tommy Berglund
 */
@Singleton
public class AppConfigurationService {

    @Inject
    HjmtLogger LOG;

    private Properties properties = new Properties();
 
    @javax.enterprise.inject.Produces
    public String getString(InjectionPoint point) {
        String fieldName = point.getMember().getName();
        return getString(fieldName);
    }
    
    @javax.enterprise.inject.Produces
    public boolean getBoolean(InjectionPoint point) {
        String stringValue = getString(point);
        return Boolean.parseBoolean(stringValue);
    }
    
    @javax.enterprise.inject.Produces
    public int getInt(InjectionPoint point) {
        String stringValue = getString(point);
        return Integer.parseInt(stringValue);
    }
    
    public String getString(String fieldName) {
        return properties.getProperty(fieldName);
    }
    
    public boolean getBoolean(String fieldName) {
        String stringValue = getString(fieldName);
        return Boolean.parseBoolean(stringValue);
    }
    
    public int getInt(String fieldName) {
        String stringValue = getString(fieldName);
        return Integer.parseInt(stringValue);
    }
    
    public void loadProperties( String propertyFileName ) {
        try ( InputStream appPropertiesInputStream = this.getClass().getClassLoader().getResourceAsStream(propertyFileName) ) {
            Reader appPropertiesReader = new InputStreamReader(appPropertiesInputStream, "UTF-8");
            properties.load(appPropertiesReader);
        } catch (IOException ex) {
            System.err.println( "Failed to load " + propertyFileName );
            ex.printStackTrace(System.err);
        }
    }
    
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.property;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Properties;
import javax.ejb.Singleton;

/**
 * Reads texts from property file to Properties object
 * 
 * @author Tommy Berglund
 */
@Singleton
public class TextController {
    
    private Properties texts = new Properties();
 
    public Properties getTexts() {
        return texts;
    }
    
    public void loadTexts(String propertyFileName) {
        try ( InputStream textsPropertiesInputStream = this.getClass().getClassLoader().getResourceAsStream(propertyFileName) ) {
            Reader textsReader = new InputStreamReader(textsPropertiesInputStream, "UTF-8");
            texts.load(textsReader);
        } catch (IOException ex) {
            System.err.println( "Failed to load " + propertyFileName );
            ex.printStackTrace(System.err);
        }
    }
    
}

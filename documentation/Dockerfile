FROM openjdk:8-jre-alpine

ENV LC_ALL C
EXPOSE 8080

COPY entrypoint.sh /
COPY index.html /
COPY target/swagger.json /

RUN adduser java -h / -D && \
    apk update && \
    apk add --no-cache wget ca-certificates graphviz ttf-ubuntu-font-family java-postgresql-jdbc && \
    mkdir drivers && \
    wget -nv -O schemaspy.jar https://github.com/schemaspy/schemaspy/releases/download/v6.0.0-rc2/schemaspy-6.0.0-rc2.jar && \
    wget -nv -O drivers/mysql-connector-java.jar http://central.maven.org/maven2/mysql/mysql-connector-java/5.1.42/mysql-connector-java-5.1.42.jar && \
    apk del wget ca-certificates && \
    apk --no-cache add python3 && \
    mkdir -p /output/db && \
    mkdir -p /output/openapi && \
    mv /index.html /output && \
    mv /swagger.json /output/openapi && \
    chown -R java /output && \
    chmod -R 777 /output

# Generate documentation with SchemaSpy and serve html report
USER java
ENTRYPOINT /entrypoint.sh -t $DB_TYPE -db $DB_NAME -s $DB_NAME -host $DB_HOST -port $DB_PORT -u $DB_USERNAME -p $DB_PASSWORD \
           && cd /output \
           && python3 -m http.server 8080

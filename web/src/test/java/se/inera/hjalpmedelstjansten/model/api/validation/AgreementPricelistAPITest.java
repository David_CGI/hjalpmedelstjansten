/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.model.api.validation;

import java.util.Calendar;
import java.util.Iterator;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementPricelistControllerTest;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistAPI;

/**
 *
 * @author Tommy Berglund
 */
public class AgreementPricelistAPITest {
    
    private Validator beanValidator;
    
    static final long pricelistId = 1;
    static final String pricelistNumber = "001";
    
    @Before
    public void init() {
        beanValidator = Validation.buildDefaultValidatorFactory().getValidator();
    }
    
    @Test
    public void testAllEmpty() {
        AgreementPricelistAPI pricelistAPI = new AgreementPricelistAPI();
        Set<ConstraintViolation<AgreementPricelistAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(2, constraintViolations.size());
        boolean numberFound = false;
        boolean validFromFound = false;
        Iterator<ConstraintViolation<AgreementPricelistAPI>> it = constraintViolations.iterator();
        while( it.hasNext() ) {
            ConstraintViolation<AgreementPricelistAPI> constraintViolation = it.next();
            if( "{pricelist.number.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                numberFound = true;
            } else if( "{pricelist.validFrom.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                validFromFound = true;
            }
        }
        Assert.assertTrue(numberFound);
        Assert.assertTrue(validFromFound);
    }
    
    @Test
    public void testAllRight() {
        AgreementPricelistAPI pricelistAPI = AgreementPricelistControllerTest.createValidPricelistAPI(pricelistId, pricelistNumber, getValidFrom());
        Set<ConstraintViolation<AgreementPricelistAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }
        
    @Test
    public void beanTestNoAgreementNumber() {
        AgreementPricelistAPI pricelistAPI = AgreementPricelistControllerTest.createValidPricelistAPI(pricelistId, pricelistNumber, getValidFrom());
        pricelistAPI.setNumber(null);
        Set<ConstraintViolation<AgreementPricelistAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{pricelist.number.notNull}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void beanTestLongAgreementNumber() {
        AgreementPricelistAPI pricelistAPI = AgreementPricelistControllerTest.createValidPricelistAPI(pricelistId, pricelistNumber, getValidFrom());
        pricelistAPI.setNumber("abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdef");
        Set<ConstraintViolation<AgreementPricelistAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{pricelist.number.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void beanTestNoValidFrom() {
        AgreementPricelistAPI pricelistAPI = AgreementPricelistControllerTest.createValidPricelistAPI(pricelistId, pricelistNumber, getValidFrom());
        pricelistAPI.setValidFrom(null);
        Set<ConstraintViolation<AgreementPricelistAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{pricelist.validFrom.notNull}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void beanTestInvalidValidFrom() {
        AgreementPricelistAPI pricelistAPI = AgreementPricelistControllerTest.createValidPricelistAPI(pricelistId, pricelistNumber, getValidFrom());
        pricelistAPI.setValidFrom(-10l);
        Set<ConstraintViolation<AgreementPricelistAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{pricelist.validFrom.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void beanTestInvalidValidFromNotFuture() {
        AgreementPricelistAPI pricelistAPI = AgreementPricelistControllerTest.createValidPricelistAPI(pricelistId, pricelistNumber, getValidFrom());
        Calendar validFrom = Calendar.getInstance();
        validFrom.add(Calendar.DAY_OF_YEAR, -1);
        pricelistAPI.setValidFrom(validFrom.getTimeInMillis());
        Set<ConstraintViolation<AgreementPricelistAPI>> constraintViolations = beanValidator.validate(pricelistAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{pricelist.validFrom.notFuture}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    private static long getValidFrom() {
        Calendar validFrom = Calendar.getInstance();
        return validFrom.getTimeInMillis();
    }
        
}

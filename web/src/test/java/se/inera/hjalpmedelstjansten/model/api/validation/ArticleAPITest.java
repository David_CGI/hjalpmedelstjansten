/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.model.api.validation;

import java.util.Iterator;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleControllerTest;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;

/**
 *
 * @author Tommy Berglund
 */
public class ArticleAPITest {
    
    private Validator beanValidator;
    
    @Before
    public void init() {
        beanValidator = Validation.buildDefaultValidatorFactory().getValidator();
    }
    
    @Test
    public void testAllEmpty() {
        ArticleAPI articleAPI = new ArticleAPI();
        Set<ConstraintViolation<ArticleAPI>> constraintViolations = beanValidator.validate(articleAPI, Default.class);
        Assert.assertEquals(6, constraintViolations.size());
        boolean nameFound = false;
        boolean numberFound = false;
        boolean statusFound = false;
        boolean categoryFound = false;
        boolean productFound = false;
        boolean orderUnitFound = false;
        Iterator<ConstraintViolation<ArticleAPI>> it = constraintViolations.iterator();
        while( it.hasNext() ) {
            ConstraintViolation<ArticleAPI> constraintViolation = it.next();
            if( "{article.name.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                nameFound = true;
            } else if( "{article.number.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                numberFound = true;
            } else if( "{article.status.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                statusFound = true;
            } else if( "{article.category.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                categoryFound = true;
            } else if( "{article.productRelation.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                productFound = true;
            } else if( "{article.orderUnit.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                orderUnitFound = true;
            }
        }
        Assert.assertTrue(nameFound);
        Assert.assertTrue(numberFound);
        Assert.assertTrue(statusFound);
        Assert.assertTrue(categoryFound);
        Assert.assertTrue(productFound);
        Assert.assertTrue(orderUnitFound);
    }
    
    @Test
    public void testAllRight() {
        ArticleAPI articleAPI = ArticleControllerTest.createValidArticleAPI();
        Set<ConstraintViolation<ArticleAPI>> constraintViolations = beanValidator.validate(articleAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }
    
    @Test
    public void testTooShortGLN() {
        ArticleAPI articleAPI = ArticleControllerTest.createValidArticleAPI();
        articleAPI.setGtin("100000000000");
        Set<ConstraintViolation<ArticleAPI>> constraintViolations = beanValidator.validate(articleAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{article.gtin.notValid.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void testInvalidGLN() {
        ArticleAPI articleAPI = ArticleControllerTest.createValidArticleAPI();
        articleAPI.setGtin("1000000000000");
        Set<ConstraintViolation<ArticleAPI>> constraintViolations = beanValidator.validate(articleAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{article.gtin.notValid.checkSum}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void beanTestTooShortProductName() {
        ArticleAPI articleAPI = ArticleControllerTest.createValidArticleAPI();
        articleAPI.setArticleName(null);
        Set<ConstraintViolation<ArticleAPI>> constraintViolations = beanValidator.validate(articleAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{article.name.notNull}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void beanTestLongShortProductName() {
        ArticleAPI articleAPI = ArticleControllerTest.createValidArticleAPI();
        articleAPI.setArticleName("abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdef");
        Set<ConstraintViolation<ArticleAPI>> constraintViolations = beanValidator.validate(articleAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{article.name.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void beanTestTooShortProductNumber() {
        ArticleAPI articleAPI = ArticleControllerTest.createValidArticleAPI();
        articleAPI.setArticleNumber(null);
        Set<ConstraintViolation<ArticleAPI>> constraintViolations = beanValidator.validate(articleAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{article.number.notNull}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void beanTestTooLongProductNumber() {
        ArticleAPI articleAPI = ArticleControllerTest.createValidArticleAPI();
        articleAPI.setArticleNumber("abcdefghijabcdefghijabcdefghijabcdef");
        Set<ConstraintViolation<ArticleAPI>> constraintViolations = beanValidator.validate(articleAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{article.number.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void beanTestInvalidStatusEmpty() {
        ArticleAPI articleAPI = ArticleControllerTest.createValidArticleAPI();
        articleAPI.setStatus("");
        Set<ConstraintViolation<ArticleAPI>> constraintViolations = beanValidator.validate(articleAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{article.status.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }
     
    @Test
    public void beanTestInvalidStatusWrong() {
        ArticleAPI articleAPI = ArticleControllerTest.createValidArticleAPI();
        articleAPI.setStatus("test");
        Set<ConstraintViolation<ArticleAPI>> constraintViolations = beanValidator.validate(articleAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{article.status.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void beanTestNoProduct() {
        ArticleAPI articleAPI = ArticleControllerTest.createValidArticleAPI();
        articleAPI.setBasedOnProduct(null);
        articleAPI.setFitsToProducts(null);
        Set<ConstraintViolation<ArticleAPI>> constraintViolations = beanValidator.validate(articleAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{article.productRelation.notNull}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void beanTestTooLongSupplementedInformation() {
        ArticleAPI articleAPI = ArticleControllerTest.createValidArticleAPI();
        articleAPI.setSupplementedInformation("abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefabc");
        Set<ConstraintViolation<ArticleAPI>> constraintViolations = beanValidator.validate(articleAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{article.supplementedInformation.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
}
/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.model.api.validation;

import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import se.inera.hjalpmedelstjansten.model.api.PostAddressAPI;

/**
 *
 * @author Tommy Berglund
 */
public class PostAddressAPITest {
    
    private Validator validator;
    
    @Before
    public void init() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }
    
    @Test
    public void testAllEmpty() {
        PostAddressAPI postAddressAPI = new PostAddressAPI();
        Set<ConstraintViolation<PostAddressAPI>> constraintViolations = validator.validate(postAddressAPI, Default.class);
        Assert.assertTrue(constraintViolations.isEmpty());
    }
    
    // TEST POSTCODE

     
    @Test
    public void testValidPostCode() {
        PostAddressAPI postAddressAPI = new PostAddressAPI();
        postAddressAPI.setPostCode("11111");
        Set<ConstraintViolation<PostAddressAPI>> constraintViolations = validator.validate(postAddressAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }

}

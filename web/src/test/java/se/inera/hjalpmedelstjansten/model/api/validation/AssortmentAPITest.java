/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.model.api.validation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import se.inera.hjalpmedelstjansten.business.assortment.controller.AssortmentControllerTest;
import se.inera.hjalpmedelstjansten.model.api.AssortmentAPI;

/**
 *
 * @author Tommy Berglund
 */
public class AssortmentAPITest {
    
    private Validator beanValidator;
    static final long assortmentId = 1;
    static final String assortmentName = "assortmentName";
    
    @Before
    public void init() {
        beanValidator = Validation.buildDefaultValidatorFactory().getValidator();
    }
    
    @Test
    public void testAllEmpty() {
        AssortmentAPI assortmentAPI = new AssortmentAPI();
        Set<ConstraintViolation<AssortmentAPI>> constraintViolations = beanValidator.validate(assortmentAPI, Default.class);
        Assert.assertEquals(2, constraintViolations.size());
        boolean nameFound = false;
        boolean validFromFound = false;
        Iterator<ConstraintViolation<AssortmentAPI>> it = constraintViolations.iterator();
        while( it.hasNext() ) {
            ConstraintViolation<AssortmentAPI> constraintViolation = it.next();
            if( "{assortment.name.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                nameFound = true;
            } else if( "{assortment.validFrom.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                validFromFound = true;
            }
        }
        Assert.assertTrue(nameFound);
        Assert.assertTrue(validFromFound);
    }
    
    @Test
    public void testAllRight() {
        AssortmentAPI assortmentAPI = AssortmentControllerTest.createValidAssortmentAPI(assortmentId, assortmentName);
        Set<ConstraintViolation<AssortmentAPI>> constraintViolations = beanValidator.validate(assortmentAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }
    
    @Test
    public void beanTestLongName() {
        AssortmentAPI assortmentAPI = AssortmentControllerTest.createValidAssortmentAPI(assortmentId, assortmentName);
        assortmentAPI.setName("abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdef");
        Set<ConstraintViolation<AssortmentAPI>> constraintViolations = beanValidator.validate(assortmentAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{assortment.name.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void beanTestNoValidFrom() {
        AssortmentAPI assortmentAPI = AssortmentControllerTest.createValidAssortmentAPI(assortmentId, assortmentName);
        assortmentAPI.setValidFrom(null);
        Set<ConstraintViolation<AssortmentAPI>> constraintViolations = beanValidator.validate(assortmentAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{assortment.validFrom.notNull}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void beanTestInvalidValidFrom() {
        AssortmentAPI assortmentAPI = AssortmentControllerTest.createValidAssortmentAPI(assortmentId, assortmentName);
        assortmentAPI.setValidFrom(-10l);
        Set<ConstraintViolation<AssortmentAPI>> constraintViolations = beanValidator.validate(assortmentAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{assortment.validFrom.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void beanTestInvalidValidTo() {
        AssortmentAPI assortmentAPI = AssortmentControllerTest.createValidAssortmentAPI(assortmentId, assortmentName);
        assortmentAPI.setValidTo(-10l);
        Set<ConstraintViolation<AssortmentAPI>> constraintViolations = beanValidator.validate(assortmentAPI, Default.class);
        Assert.assertEquals(2, constraintViolations.size());        
        Iterator<ConstraintViolation<AssortmentAPI>> constraintViolationIterator = constraintViolations.iterator();
        List<String> violationMessages = new ArrayList();
        while( constraintViolationIterator.hasNext() ) {
            ConstraintViolation<AssortmentAPI> constraintViolation = constraintViolationIterator.next();
            violationMessages.add(constraintViolation.getMessageTemplate());
        }
        Assert.assertTrue(violationMessages.contains("{assortment.validToBeforeValidFrom}"));
        Assert.assertTrue(violationMessages.contains("{assortment.validTo.invalid}"));
    }
    
}

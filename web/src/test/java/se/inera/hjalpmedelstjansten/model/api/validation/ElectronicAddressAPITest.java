/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.model.api.validation;

import java.util.Iterator;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import se.inera.hjalpmedelstjansten.model.api.ElectronicAddressAPI;

/**
 *
 * @author Tommy Berglund
 */
public class ElectronicAddressAPITest {
    
    private Validator validator;
    
    @Before
    public void init() {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }
    
    @Test
    public void testAllEmpty() {
        ElectronicAddressAPI electronicAddressAPI = new ElectronicAddressAPI();
        Set<ConstraintViolation<ElectronicAddressAPI>> constraintViolations = validator.validate(electronicAddressAPI, Default.class);
        Assert.assertTrue(constraintViolations.isEmpty());
    }
    
    // TEST EMAIL
    
    @Test
    public void testInvalidEmail1() {
        ElectronicAddressAPI electronicAddressAPI = new ElectronicAddressAPI();
        electronicAddressAPI.setEmail("a");
        Set<ConstraintViolation<ElectronicAddressAPI>> constraintViolations = validator.validate(electronicAddressAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{electronicAddress.email.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void testInvalidEmail2() {
        ElectronicAddressAPI electronicAddressAPI = new ElectronicAddressAPI();
        electronicAddressAPI.setEmail("a@");
        Set<ConstraintViolation<ElectronicAddressAPI>> constraintViolations = validator.validate(electronicAddressAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{electronicAddress.email.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }
     
    @Test
    public void testValidEmail() {
        ElectronicAddressAPI electronicAddressAPI = new ElectronicAddressAPI();
        electronicAddressAPI.setEmail("a@b.c");
        Set<ConstraintViolation<ElectronicAddressAPI>> constraintViolations = validator.validate(electronicAddressAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }

    @Test
    public void testOddButValidEmail() {
        ElectronicAddressAPI electronicAddressAPI = new ElectronicAddressAPI();
        electronicAddressAPI.setEmail("a@b");
        Set<ConstraintViolation<ElectronicAddressAPI>> constraintViolations = validator.validate(electronicAddressAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }    
    
    // TEST WEB
    @Test
    public void testInvalidWeb1() {
        ElectronicAddressAPI electronicAddressAPI = new ElectronicAddressAPI();
        electronicAddressAPI.setWeb("a");
        Set<ConstraintViolation<ElectronicAddressAPI>> constraintViolations = validator.validate(electronicAddressAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{electronicAddress.web.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void testInvalidWeb2() {
        ElectronicAddressAPI electronicAddressAPI = new ElectronicAddressAPI();
        electronicAddressAPI.setWeb("http:/test");
        Set<ConstraintViolation<ElectronicAddressAPI>> constraintViolations = validator.validate(electronicAddressAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{electronicAddress.web.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void testInvalidWeb3() {
        ElectronicAddressAPI electronicAddressAPI = new ElectronicAddressAPI();
        electronicAddressAPI.setWeb("test://test");
        Set<ConstraintViolation<ElectronicAddressAPI>> constraintViolations = validator.validate(electronicAddressAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{electronicAddress.web.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void testValidWeb1() {
        ElectronicAddressAPI electronicAddressAPI = new ElectronicAddressAPI();
        electronicAddressAPI.setWeb("http://test");
        Set<ConstraintViolation<ElectronicAddressAPI>> constraintViolations = validator.validate(electronicAddressAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }
    
    @Test
    public void testValidWeb2() {
        ElectronicAddressAPI electronicAddressAPI = new ElectronicAddressAPI();
        electronicAddressAPI.setWeb("https://test");
        Set<ConstraintViolation<ElectronicAddressAPI>> constraintViolations = validator.validate(electronicAddressAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }
    
    // EMAIL AND WEB
    @Test
    public void testInvalidEmailAndInvalidWeb() {
        ElectronicAddressAPI electronicAddressAPI = new ElectronicAddressAPI();
        electronicAddressAPI.setEmail("a");
        electronicAddressAPI.setWeb("test://test");
        Set<ConstraintViolation<ElectronicAddressAPI>> constraintViolations = validator.validate(electronicAddressAPI, Default.class);
        Assert.assertEquals(2, constraintViolations.size());
        boolean emailValidationFound = false;
        boolean webValidationFound = false;
        Iterator<ConstraintViolation<ElectronicAddressAPI>> it = constraintViolations.iterator();
        while( it.hasNext() ) {
            ConstraintViolation<ElectronicAddressAPI> constraintViolation = it.next();
            if( "{electronicAddress.email.invalid}".equals(constraintViolation.getMessageTemplate()) ) {
                emailValidationFound = true;
            } else if( "{electronicAddress.web.invalid}".equals(constraintViolation.getMessageTemplate()) ) {
                webValidationFound = true;
            }
        }
        Assert.assertTrue(emailValidationFound);
        Assert.assertTrue(webValidationFound);
    }
    
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.model.api.validation;

import java.util.Iterator;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleControllerTest;
import se.inera.hjalpmedelstjansten.business.product.controller.PreventiveMaintenanceUnitControllerTest;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCEDirectiveAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCEStandardAPI;
import se.inera.hjalpmedelstjansten.model.api.CategoryAPI;
import se.inera.hjalpmedelstjansten.model.api.ElectronicAddressAPI;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVOrderUnitAPI;
import se.inera.hjalpmedelstjansten.model.entity.Product;

/**
 *
 * @author Tommy Berglund
 */
public class ProductAPITest {
    
    private Validator beanValidator;
    static long articleOrderUnitId = 107;
    static String preventiveMaintenanceUnitCode = "code";
    static String preventiveMaintenanceUnitName = "name";
    
    @Before
    public void init() {
        beanValidator = Validation.buildDefaultValidatorFactory().getValidator();
    }
    
    @Test
    public void beanTestAllEmpty() {
        ProductAPI productAPI = new ProductAPI();
        Set<ConstraintViolation<ProductAPI>> constraintViolations = beanValidator.validate(productAPI, Default.class);
        Assert.assertEquals(5, constraintViolations.size());
        boolean productNameFound = false;
        boolean productNumberFound = false;
        boolean productStatusFound = false;
        boolean productCategoryFound = false;
        boolean orderUnitFound = false;
        Iterator<ConstraintViolation<ProductAPI>> it = constraintViolations.iterator();
        while( it.hasNext() ) {
            ConstraintViolation<ProductAPI> constraintViolation = it.next();
            if( "{product.name.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                productNameFound = true;
            } else if( "{product.number.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                productNumberFound = true;
            } else if( "{product.status.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                productStatusFound = true;
            } else if( "{product.category.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                productCategoryFound = true;
            } else if( "{product.orderUnit.notNull}".equals(constraintViolation.getMessageTemplate()) ) {
                orderUnitFound = true;
            }
        }
        Assert.assertTrue(productNameFound);
        Assert.assertTrue(productNumberFound);
        Assert.assertTrue(productStatusFound);
        Assert.assertTrue(productCategoryFound);
        Assert.assertTrue(orderUnitFound);        
    }
    
    @Test
    public void beanTestAllRight() {
        ProductAPI productAPI = createValidProductAPI();
        Set<ConstraintViolation<ProductAPI>> constraintViolations = beanValidator.validate(productAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }
    
    @Test
    public void beanTestTooShortProductName() {
        ProductAPI productAPI = createValidProductAPI();
        productAPI.setProductName(null);
        Set<ConstraintViolation<ProductAPI>> constraintViolations = beanValidator.validate(productAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{product.name.notNull}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void beanTestLongShortProductName() {
        ProductAPI productAPI = createValidProductAPI();
        productAPI.setProductName("abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdef");
        Set<ConstraintViolation<ProductAPI>> constraintViolations = beanValidator.validate(productAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{product.name.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void beanTestTooShortProductNumber() {
        ProductAPI productAPI = createValidProductAPI();
        productAPI.setProductNumber(null);
        Set<ConstraintViolation<ProductAPI>> constraintViolations = beanValidator.validate(productAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{product.number.notNull}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void beanTestTooLongProductNumber() {
        ProductAPI productAPI = createValidProductAPI();
        productAPI.setProductNumber("abcdefghijabcdefghijabcdefghijabcdef");
        Set<ConstraintViolation<ProductAPI>> constraintViolations = beanValidator.validate(productAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{product.number.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void beanTestInvalidStatusEmpty() {
        ProductAPI productAPI = createValidProductAPI();
        productAPI.setStatus("");
        Set<ConstraintViolation<ProductAPI>> constraintViolations = beanValidator.validate(productAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{product.status.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }
     
    @Test
    public void beanTestInvalidStatusWrong() {
        ProductAPI productAPI = createValidProductAPI();
        productAPI.setStatus("test");
        Set<ConstraintViolation<ProductAPI>> constraintViolations = beanValidator.validate(productAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{product.status.invalid}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void beanTestTooLongManufacturer() {
        ProductAPI productAPI = createValidProductAPI();
        productAPI.setManufacturer("abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdef");
        Set<ConstraintViolation<ProductAPI>> constraintViolations = beanValidator.validate(productAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{product.manufacturer.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void beanTestTooLongManufacturerProductNumber() {
        ProductAPI productAPI = createValidProductAPI();
        productAPI.setManufacturerProductNumber("abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdef");
        Set<ConstraintViolation<ProductAPI>> constraintViolations = beanValidator.validate(productAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{product.manufacturerProductNumber.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void beanTestTooLongTrademark() {
        ProductAPI productAPI = createValidProductAPI();
        productAPI.setTrademark("abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdef");
        Set<ConstraintViolation<ProductAPI>> constraintViolations = beanValidator.validate(productAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{product.trademark.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void beanTestTooLongSupplementedInformation() {
        ProductAPI productAPI = createValidProductAPI();
        productAPI.setSupplementedInformation("abcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefghijabcdefabc");
        Set<ConstraintViolation<ProductAPI>> constraintViolations = beanValidator.validate(productAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{product.supplementedInformation.wrongLength}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void beanNotCEMarkedButDirective() {
        ProductAPI productAPI = createValidProductAPI();
        productAPI.setCeMarked(false);
        CVCEDirectiveAPI cEDirectiveAPI = new CVCEDirectiveAPI();        
        productAPI.setCeDirective(cEDirectiveAPI);
        Set<ConstraintViolation<ProductAPI>> constraintViolations = beanValidator.validate(productAPI, Default.class);
        Assert.assertEquals(1, constraintViolations.size());
        Assert.assertEquals("{product.noCeButDirective}", constraintViolations.iterator().next().getMessageTemplate());
    }
    
    @Test
    public void beanCEMarkedAndDirectiveAndStandard() {
        ProductAPI productAPI = createValidProductAPI();
        productAPI.setCeMarked(true);
        CVCEStandardAPI cEStandardAPI = new CVCEStandardAPI();        
        productAPI.setCeStandard(cEStandardAPI);
        CVCEDirectiveAPI cEDirectiveAPI = new CVCEDirectiveAPI();        
        productAPI.setCeDirective(cEDirectiveAPI);
        Set<ConstraintViolation<ProductAPI>> constraintViolations = beanValidator.validate(productAPI, Default.class);
        Assert.assertEquals(0, constraintViolations.size());
    }
    
    public static ProductAPI createValidProductAPI() {
        ProductAPI productAPI = new ProductAPI();
        productAPI.setProductName("test");
        productAPI.setProductNumber("test");
        productAPI.setStatus(Product.Status.PUBLISHED.toString());
        productAPI.setCategory(createValidCategoryAPI());
        productAPI.setManufacturer("test");
        productAPI.setTrademark("test");
        productAPI.setPreventiveMaintenanceDescription("test");
        productAPI.setPreventiveMaintenanceNumberOfDays(365);
        productAPI.setPreventiveMaintenanceValidFrom(PreventiveMaintenanceUnitControllerTest.createValidPreventiveMaintenanceAPI(preventiveMaintenanceUnitCode, preventiveMaintenanceUnitName));
        ElectronicAddressAPI manufacturerElectronicAddressAPI = new ElectronicAddressAPI();
        manufacturerElectronicAddressAPI.setWeb("http://test");
        productAPI.setManufacturerElectronicAddress(manufacturerElectronicAddressAPI);
        CVOrderUnitAPI orderUnit = ArticleControllerTest.createValidUnitAPI(articleOrderUnitId);
        productAPI.setOrderUnit(orderUnit);
        return productAPI;
    }
    
    public static CategoryAPI createValidCategoryAPI() {
        CategoryAPI categoryAPI = new CategoryAPI();
        categoryAPI.setName("test");
        categoryAPI.setCode(null);
        return categoryAPI;
    }
    
}

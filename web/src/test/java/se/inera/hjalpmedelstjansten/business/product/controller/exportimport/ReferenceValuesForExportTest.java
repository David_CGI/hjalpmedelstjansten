package se.inera.hjalpmedelstjansten.business.product.controller.exportimport;

public class ReferenceValuesForExportTest {
    public ReferenceValuesForExportTest() {
    }

    String[] setupDataFromFirstDataRowBasedOnArticle() {
        return new String[]{ // Column: HeaderName: Expected Value
                "1:UniktId :100.0",
                "2:Produkt/Artikel :Artikel",
                "3:Produktnummer/Artikelnummer :Article number",
                "4:Produktbenämning/Artikelbenämning :test",
                "5:Produktbenämning/Artikelbenämning :",
                "6:Baseras På Produkt :test",
                "7:Baseras På Produkt :",
                "8:Kategori:",
                "9:Visas även i följande kategorier :",
                "10:Visas även i följande kategorier :",
                "11:GTIN-13 :",
                "12:GRIN-13 :",
                "13:Kundunik :Nej"
        };
    }

    String[] setupDataFromFirstDataRowBasedOnProduct() {
        return new String[]{ // Column: HeaderName: Expected Value
                "1:UniktId :200",
                "2:Produkt/Artikel :Produkt",
                "3:Produktnummer/Artikelnummer :test",
                "4:Produktbenämning/Artikelbenämning :test",
                "5:Produktbenämning/Artikelbenämning :",
                "6:Baseras På Produkt :",
                "7:Baseras På Produkt :",
                "8:Kategori:",
                "9:Visas även i följande kategorier :",
                "10:Visas även i följande kategorier :",
                "11:GTIN-13 :",
                "12:GRIN-13 :",
                "13:Kundunik :Nej"
        };
    }

    String[] setupDataValidationReferences() {
        return new String[]{
                "BP3:BP50001 => whole ,greaterThanOrEqual, -1 ",
                "B3:B50001 => list VLProductArticle ",
                "AJ3:AJ50001 => list VLStandards ",
                "AH3:AH50001 => list VLDirectives ",
                "N3:N50001 => list VLYesNo ",
                "AF3:AF50001 => list VLYesNo ",
                "BJ3:BJ50001 => list VLYesNo ",
                "T3:T50001 => list VLOrderUnits ",
                "X3:X50001 => list VLPackageUnits ",
                "P3:P50001 => list VLOrderUnitsNoDelete ",
                "AL3:AL50001 => list VLPreventiveMaintenanceValidFrom ",
                "R3:R50001 => whole ,greaterThanOrEqual, -1 ",
                "V3:V50001 => whole ,greaterThanOrEqual, -1 ",
                "AD3:AD50001 => whole ,greaterThanOrEqual, -1 ",
                "AB3:AB50001 => whole ,greaterThanOrEqual, -1 ",
                "Z3:Z50001 => whole ,greaterThanOrEqual, -1 ",
                "AN3:AN50001 => whole ,greaterThanOrEqual, -1 ",
                "BH3:BH50001 => date ,greaterThan, 1970-01-01 ",
        };
    }

    String[] setupHeaderRowReference() {
        return new String[]{ //Column: HeaderCellName
                "1:Unikt Id",
                "2:Produkt/Artikel (*)",
                "3:Produktnummer/Artikelnummer (*)",
                "4:Produktbenämning/Artikelbenämning",
                "5:Produktbenämning/Artikelbenämning",
                "6:Baseras på produkt",
                "7:Baseras på produkt",
                "8:Kategori",
                "9:Visas även i följande kategorier",
                "10:Visas även i följande kategorier",
                "11:GTIN-13",
                "12:GTIN-13",
                "13:Kundunik",
                "14:Kundunik",
                "15:Beställningsenhet",
                "16:Beställningsenhet (*)",
                "17:Antal förpackningar",
                "18:Antal förpackningar",
                "19:Enhet för antal förpackningar",
                "20:Enhet för antal förpackningar",
                "21:Varje förpackning innehåller",
                "22:Varje förpackning innehåller",
                "23:Enhet för varje förpackning innehåller",
                "24:Enhet för varje förpackning innehåller",
                "25:Antal artiklar bas",
                "26:Antal artiklar bas",
                "27:Antal artiklar mellan",
                "28:Antal artiklar mellan",
                "29:Antal artiklar pall",
                "30:Antal artiklar pall",
                "31:Artikeln har CE-märkning",
                "32:Artikeln har CE-märkning",
                "33:CE-märkning/Direktiv",
                "34:CE-märkning/Direktiv",
                "35:CE-märkning/Standard",
                "36:CE-märkning/Standard",
                "37:Förebyggande underhåll/Gäller från",
                "38:Förebyggande underhåll/Gäller från",
                "39:Förebyggande underhåll/Intervall",
                "40:Förebyggande underhåll/Intervall",
                "41:Tillverkarens namn",
                "42:Tillverkarens namn",
                "43:Tillverkarens produktnummer/artikelnummer",
                "44:Tillverkarens produktnummer/artikelnummer",
                "45:Tillverkarens webbsida",
                "46:Tillverkarens webbsida",
                "47:Varumärke",
                "48:Varumärke",
                "49:Färg",
                "50:Färg",
                "51:Kompletterande information",
                "52:Kompletterande information",
                "53:Huvudbild",
                "54:Huvudbild",
                "55:Huvudbild beskrivning",
                "56:Huvudbild beskrivning",
                "57:Huvudbild alt-text",
                "58:Huvudbild alt-text",
                "59:Utgår datum",
                "60:Utgår datum",
                "61:Inaktivera rader",
                "62:Inaktivera rader",
                "63:Ersätts av",
                "64:Ersätts av",
                "65:Skapad",
                "66:Senast uppdaterad",
                "67:Decimal-field",
                "68:Decimal-field",
                "69:Text-field",
                "70:Text-field"

        };
    }

    short[] setupColorValidationReferences() {

        final short YELLOW = 13;
        final short ORANGE = 53;
        final short WHITE = 64;
        final short GOLD = 51;

        //Sequence of colors expected.
        return new short[]{
                WHITE, ORANGE, WHITE, WHITE, YELLOW, WHITE, YELLOW, WHITE, WHITE, YELLOW, WHITE, YELLOW
        };
    }

    String[] setupNonHArticleSheetNameReference() {
        return new String[] {
                "I",
                "R",
                "T",
                "Tj"
        };
    }
}
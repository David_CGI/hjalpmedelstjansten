/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.organization.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.Before;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.entity.BusinessLevel;
import se.inera.hjalpmedelstjansten.model.entity.Organization;

/**
 *
 * @author tomber
 */
public class OrganizationControllerTest {
    
    @Before
    public void init() {
        
    }
    
    public static Organization createValidOrganization(long organizationId,             
            Organization.OrganizationType organizationType, 
            Date validFrom,
            Long businessLevelId, 
            String organizationName) {
        Organization organization = new Organization();
        organization.setUniqueId(organizationId);
        organization.setOrganizationName(organizationName);
        organization.setOrganizationType(organizationType);
        organization.setValidFrom(validFrom);
        if( businessLevelId != null ) {
            List<BusinessLevel> businessLevels = new ArrayList<>();
            BusinessLevel businessLevel = new BusinessLevel();
            businessLevel.setUniqueId(businessLevelId);
            businessLevels.add(businessLevel);
            organization.setBusinessLevels(businessLevels);
        }
        return organization;
    }
    
    public static OrganizationAPI createValidOrganizationAPI(long organizationId, Organization.OrganizationType organizationType, long validFrom) {
        OrganizationAPI organizationAPI = new OrganizationAPI();
        organizationAPI.setId(organizationId);
        organizationAPI.setOrganisationType(organizationType.toString());
        organizationAPI.setValidFrom(validFrom);
        return organizationAPI;
    }
    
}

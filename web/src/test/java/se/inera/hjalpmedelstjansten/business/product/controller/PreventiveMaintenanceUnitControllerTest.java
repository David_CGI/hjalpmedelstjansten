/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.product.controller;


import org.junit.Before;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPreventiveMaintenanceAPI;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;

/**
 *
 * @author tomber
 */
public class PreventiveMaintenanceUnitControllerTest {
    
    PreventiveMaintenanceUnitController preventiveMaintenanceUnitController;


    @Before
    public void init() {
        preventiveMaintenanceUnitController = new PreventiveMaintenanceUnitController();
    }
    
    public static CVPreventiveMaintenanceAPI createValidPreventiveMaintenanceAPI(String code, String name) {
        CVPreventiveMaintenanceAPI preventiveMaintenanceAPI = new CVPreventiveMaintenanceAPI();
        preventiveMaintenanceAPI.setCode(code);
        preventiveMaintenanceAPI.setName(name);
        return preventiveMaintenanceAPI;
    }
    
    public static CVPreventiveMaintenance createValidPreventiveMaintenance(String code, String name) {
        CVPreventiveMaintenance preventiveMaintenance = new CVPreventiveMaintenance();
        preventiveMaintenance.setCode(code);
        preventiveMaintenance.setName(name);
        return preventiveMaintenance;
    }
    
}

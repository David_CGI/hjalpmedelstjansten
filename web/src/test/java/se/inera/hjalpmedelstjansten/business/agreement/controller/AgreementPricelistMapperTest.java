/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.agreement.controller;

import java.util.Calendar;
import org.junit.Assert;
import org.junit.Test;
import se.inera.hjalpmedelstjansten.model.entity.Agreement;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelist;

/**
 *
 * @author Tommy Berglund
 */
public class AgreementPricelistMapperTest {
    
    private static final long id = 1l;
    
    @Test
    public void testGetPricelistStatusFuture() {
        Calendar validFrom = Calendar.getInstance();
        validFrom.add(Calendar.DAY_OF_YEAR, 10);
        AgreementPricelist toTest = new AgreementPricelist();
        toTest.setValidFrom(validFrom.getTime());

        Agreement agTest = new Agreement();
        agTest.setStatus(Agreement.Status.CURRENT);
        toTest.setAgreement(agTest);

        AgreementPricelist currentPricelist = new AgreementPricelist();
        AgreementPricelist.Status status = AgreementPricelistMapper.getPricelistStatus(toTest, currentPricelist);
        Assert.assertEquals(AgreementPricelist.Status.FUTURE, status);
    }
    
    @Test
    public void testGetPricelistStatusCurrentPricelistSameAsNew() {
        Calendar validFrom = Calendar.getInstance();
        validFrom.add(Calendar.HOUR_OF_DAY, 0);
        validFrom.add(Calendar.MINUTE, 0);
        validFrom.add(Calendar.SECOND, 0);
        AgreementPricelist toTest = new AgreementPricelist();
        toTest.setUniqueId(id);
        toTest.setValidFrom(validFrom.getTime());

        Agreement agTest = new Agreement();
        agTest.setStatus(Agreement.Status.CURRENT);
        toTest.setAgreement(agTest);

        AgreementPricelist currentPricelist = new AgreementPricelist();
        currentPricelist.setUniqueId(id);
        AgreementPricelist.Status status = AgreementPricelistMapper.getPricelistStatus(toTest, currentPricelist);
        Assert.assertEquals(AgreementPricelist.Status.CURRENT, status);
    }
     
    @Test
    public void testGetPricelistStatusNoCurrentPricelist() {
        Calendar validFrom = Calendar.getInstance();
        validFrom.add(Calendar.HOUR_OF_DAY, 0);
        validFrom.add(Calendar.MINUTE, 0);
        validFrom.add(Calendar.SECOND, 0);
        AgreementPricelist toTest = new AgreementPricelist();
        toTest.setUniqueId(id);
        toTest.setValidFrom(validFrom.getTime());

        Agreement agTest = new Agreement();
        agTest.setStatus(Agreement.Status.CURRENT);
        toTest.setAgreement(agTest);

        AgreementPricelist.Status status = AgreementPricelistMapper.getPricelistStatus(toTest, null);
        Assert.assertEquals(AgreementPricelist.Status.CURRENT, status);
    }
    
}

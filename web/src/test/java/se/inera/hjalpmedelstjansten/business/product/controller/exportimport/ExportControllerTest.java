/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.product.controller.exportimport;


import java.util.ArrayList;
import java.util.List;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import se.inera.hjalpmedelstjansten.business.logging.view.NoLogger;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleControllerTest;
import se.inera.hjalpmedelstjansten.business.product.controller.ProductControllerTest;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;

import javax.inject.Inject;

/**
 *
 * @author tomber
 */
public class ExportControllerTest {
    
    ExportProductsArticlesController exportController;

    static final String article1ArticleNumber = "AN1"; 
    static final String article2ArticleNumber = "AN2"; 
    static final String product1ProductNumber = "PN1"; 
    static final String product2ProductNumber = "PN2"; 

    @Before
    public void init() {

        exportController = new ExportProductsArticlesController();
    }
    
    @Test
    public void testGetConnectionsAsStringOnlyArticles() {
        List<ArticleAPI> fitsToArticleAPIs = new ArrayList<>();
        
        ArticleAPI fitsToArticle1 = ArticleControllerTest.createValidArticleAPI();
        fitsToArticle1.setArticleNumber(article1ArticleNumber);
        fitsToArticleAPIs.add(fitsToArticle1);
        
        ArticleAPI fitsToArticle2 = ArticleControllerTest.createValidArticleAPI();
        fitsToArticle2.setArticleNumber(article2ArticleNumber);
        fitsToArticleAPIs.add(fitsToArticle2);
        
        ArticleAPI articleAPI = new ArticleAPI();
        articleAPI.setFitsToArticles(fitsToArticleAPIs);
        
        String connectionString = exportController.getConnectionsAsStringArticle(articleAPI);
        String expected = article1ArticleNumber + ";" + article2ArticleNumber;
        Assert.assertEquals(expected, connectionString);
    }
    
    @Test
    public void testGetConnectionsAsStringOnlyProducts() {
        List<ProductAPI> fitsToProductAPIs = new ArrayList<>();
        
        ProductAPI fitsToProduct1 = ProductControllerTest.createValidProductAPI();
        fitsToProduct1.setProductNumber(product1ProductNumber);
        fitsToProductAPIs.add(fitsToProduct1);
        
        ProductAPI fitsToProduct2 = ProductControllerTest.createValidProductAPI();
        fitsToProduct2.setProductNumber(product2ProductNumber);
        fitsToProductAPIs.add(fitsToProduct2);
        
        ArticleAPI articleAPI = new ArticleAPI();
        articleAPI.setFitsToProducts(fitsToProductAPIs);
        
        String connectionString = exportController.getConnectionsAsStringArticle(articleAPI);
        String expected = product1ProductNumber + ";" + product2ProductNumber;
        Assert.assertEquals(expected, connectionString);
    }
    
    @Test
    public void testGetConnectionsAsStringBothArticlesAndProducts() {
        List<ArticleAPI> fitsToArticleAPIs = new ArrayList<>();
        
        ArticleAPI fitsToArticle1 = ArticleControllerTest.createValidArticleAPI();
        fitsToArticle1.setArticleNumber(article1ArticleNumber);
        fitsToArticleAPIs.add(fitsToArticle1);
        
        ArticleAPI fitsToArticle2 = ArticleControllerTest.createValidArticleAPI();
        fitsToArticle2.setArticleNumber(article2ArticleNumber);
        fitsToArticleAPIs.add(fitsToArticle2);
        
        ArticleAPI articleAPI = new ArticleAPI();
        articleAPI.setFitsToArticles(fitsToArticleAPIs);
        
        List<ProductAPI> fitsToProductAPIs = new ArrayList<>();
        
        ProductAPI fitsToProduct1 = ProductControllerTest.createValidProductAPI();
        fitsToProduct1.setProductNumber(product1ProductNumber);
        fitsToProductAPIs.add(fitsToProduct1);
        
        ProductAPI fitsToProduct2 = ProductControllerTest.createValidProductAPI();
        fitsToProduct2.setProductNumber(product2ProductNumber);
        fitsToProductAPIs.add(fitsToProduct2);
        
        articleAPI.setFitsToProducts(fitsToProductAPIs);
        
        String connectionString = exportController.getConnectionsAsStringArticle(articleAPI);
        String expected = product1ProductNumber + ";" + product2ProductNumber + ";" + article1ArticleNumber + ";" + article2ArticleNumber;
        Assert.assertEquals(expected, connectionString);
    }

   /*  @Test
   public void testAllConnectionExport() {

        List<Long> productIds = new ArrayList<>();
        long number = 717;
        productIds.add(number);
        exportController.tryToFindAdditionalConnections(396, productIds);

    } */
    
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.product.controller;

import org.junit.Before;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Category;

/**
 *
 * @author tomber
 */
public class CategoryControllerTest {
    
    static final Article.Type PRODUCT_CATEGORY_ARTICLE_TYPE = Article.Type.T;
    static final String PRODUCT_CATEGORY_NAME = "product category name";
    static final String PRODUCT_CATEGORY_CODE = "category code";

    @Before
    public void init() {
        
    }
    
    public static Category createValidCategory(Long categoryId) {
        Category category = new Category();
        category.setUniqueId(categoryId);
        category.setArticleType(PRODUCT_CATEGORY_ARTICLE_TYPE);
        category.setName(PRODUCT_CATEGORY_NAME);
        category.setCode(PRODUCT_CATEGORY_CODE);
        return category;
    }
    
    public static Category createValidCategory(Long categoryId, Article.Type type, String code) {
        Category category = new Category();
        category.setUniqueId(categoryId);
        category.setArticleType(type);
        category.setName(PRODUCT_CATEGORY_NAME);
        category.setCode(code);
        return category;
    }
    
    public static Category createValidExtendedCategory() {
        Category category = new Category();
        category.setArticleType(PRODUCT_CATEGORY_ARTICLE_TYPE);
        category.setName(PRODUCT_CATEGORY_NAME);
        category.setCode(PRODUCT_CATEGORY_CODE);
        return category;
    }
    
}

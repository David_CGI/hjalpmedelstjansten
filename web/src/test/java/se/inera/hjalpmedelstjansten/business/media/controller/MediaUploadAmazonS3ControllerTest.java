package se.inera.hjalpmedelstjansten.business.media.controller;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.http.HttpResponse;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.junit.Before;
import org.junit.Test;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistController;
import se.inera.hjalpmedelstjansten.business.logging.view.NoLogger;

import java.io.*;
import java.net.*;

import static org.junit.Assert.*;

public class MediaUploadAmazonS3ControllerTest {

    private MediaUploadAmazonS3Controller mediaUploadAmazonS3Controller = new MediaUploadAmazonS3Controller();

    @Before
    public void init() {
        mediaUploadAmazonS3Controller.LOG = new NoLogger();
    }

    //@Test
    // Check and correct content type. If the test is failing, make sure that the images it links to are still valid.
    public void checkAndCorrectContentTypeForURLUpload() {

        try {

            URL url = new URL("https://www.irishjalpmedel.se/Webservices/images/image.ashx?WebshopImageUID=7fd63e6d-31f8-4dbb-b519-cc2df3aa9375&MaxWidth=900&MaxHeight=900");

            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

            if (httpURLConnection != null) {

                assertEquals("asdasdasdasdasd.jpeg", mediaUploadAmazonS3Controller.checkAndCorrectFileEndingBasedOnContentType(httpURLConnection, "asdasdasdasdasd.jpg"));

                assertNotEquals("asdasdasdasdasd.png", mediaUploadAmazonS3Controller.checkAndCorrectFileEndingBasedOnContentType(httpURLConnection, "asdasdasdasdasd.png"));

                assertEquals("asdasdasdasdasd.jpeg", mediaUploadAmazonS3Controller.checkAndCorrectFileEndingBasedOnContentType(httpURLConnection, "asdasdasdasdasd.png"));

                assertEquals("asdasdasdasdasd.jpeg", mediaUploadAmazonS3Controller.checkAndCorrectFileEndingBasedOnContentType(httpURLConnection, "asdasdasdasdasd"));

                assertNotEquals("asdasdasdasdasd", mediaUploadAmazonS3Controller.checkAndCorrectFileEndingBasedOnContentType(httpURLConnection, "asdasdasdasdasd"));

                assertEquals("jpg/jpg/jpg/jpg/jpg/jpg.jpeg", mediaUploadAmazonS3Controller.checkAndCorrectFileEndingBasedOnContentType(httpURLConnection, "jpg/jpg/jpg/jpg/jpg/jpg.jpg"));

                url = new URL("https://www.debiflue-keeevsch.com/405-large_default/jpeg-travel-pack.jpg");

                httpURLConnection = (HttpURLConnection) url.openConnection();

                assertEquals("07fe3d5c-1b47-481c-bb06-2df302400895/0b029017-a1e6-4318-b620-350cefa7db7d/jpeg-travel-pack.jpeg", mediaUploadAmazonS3Controller.checkAndCorrectFileEndingBasedOnContentType(httpURLConnection, "07fe3d5c-1b47-481c-bb06-2df302400895/0b029017-a1e6-4318-b620-350cefa7db7d/jpeg-travel-pack.jpg"));

                assertEquals("07fe3d5c-1b47-481c-bb06-2df302400895/0b029017-a1e6-4318-b620-350cefa7db7d/jpeg-travel-pack.jpeg", mediaUploadAmazonS3Controller.checkAndCorrectFileEndingBasedOnContentType(httpURLConnection, "07fe3d5c-1b47-481c-bb06-2df302400895/0b029017-a1e6-4318-b620-350cefa7db7d/jpeg-travel-pack.gif"));

            } else {
                httpURLConnection.disconnect();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (HjalpmedelstjanstenValidationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testPDFUpload() {

        try {

            URL url = new URL("http://www.orimi.com/pdf-test.pdf");
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

            if (httpURLConnection != null) {

                assertEquals("asdasdasdasdasd.pdf", mediaUploadAmazonS3Controller.checkAndCorrectFileEndingBasedOnContentType(httpURLConnection, "asdasdasdasdasd.jpg"));
                httpURLConnection.disconnect();
            } else httpURLConnection.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (HjalpmedelstjanstenValidationException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testXLSXUpload() {

        try {
            URL url = new URL("http://mtskheta.gov.ge/public/img/1530793528.xlsx");
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

            if (httpURLConnection != null) {

                assertEquals("asdasdasdasdasd.xlsx", mediaUploadAmazonS3Controller.checkAndCorrectFileEndingBasedOnContentType(httpURLConnection, "asdasdasdasdasd.jpg"));
                httpURLConnection.disconnect();

            } else httpURLConnection.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (HjalpmedelstjanstenValidationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void determineAndSetContentType() {

        try {

            URL url = new URL("https://www.debiflue-keeevsch.com/405-large_default/jpeg-travel-pack.jpg");
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

            if (httpURLConnection != null) {

                String contentType = mediaUploadAmazonS3Controller.determineAndSetContentType("07fe3d5c-1b47-481c-bb06-2df302400895/0b029017-a1e6-4318-b620-350cefa7db7d/jpeg-travel-pack.jpeg");

                assertEquals("image/jpeg", contentType);

                //assertEquals(mediaUploadAmazonS3Controller.determineAndSetContentType("07fe3d5c-1b47-481c-bb06-2df302400895/0b029017-a1e6-4318-b620-350cefa7db7d/jpeg-travel-pack.jpeg"), httpURLConnection.getContentType());

                contentType = mediaUploadAmazonS3Controller.determineAndSetContentType("asdasdasdasdasd.doc");

                assertEquals("application/msword", contentType);

                contentType = mediaUploadAmazonS3Controller.determineAndSetContentType("asdasdasdasdasd.docx");

                assertEquals("application/vnd.openxmlformats-officedocument.wordprocessingml.document", contentType);

                contentType = mediaUploadAmazonS3Controller.determineAndSetContentType("asdasdasdasdasd.xls");

                assertEquals("application/vnd.ms-excel", contentType);

                contentType = mediaUploadAmazonS3Controller.determineAndSetContentType("asdasdasdasd.xlsx");

                assertEquals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", contentType);
                httpURLConnection.disconnect();


            } else httpURLConnection.disconnect();


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (HjalpmedelstjanstenValidationException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void makeSureOctetStreamTriesToGetContentType() {

        try {

            URL url = new URL("https://dev-cdn.hjm.aws.ineratest.org/73bf6257-4198-4c82-9a4c-9904e70bba1c/61a26d85-0fb1-4a4d-87c5-e97591689778/pattymelt.jpg");

            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            assertEquals("image/jpeg", mediaUploadAmazonS3Controller.determineAndSetContentType("73bf6257-4198-4c82-9a4c-9904e70bba1c/61a26d85-0fb1-4a4d-87c5-e97591689778/pattymelt.jpg"));
            httpURLConnection.disconnect();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (HjalpmedelstjanstenValidationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //@Test
    public void makeSureOctetStreamDoesNotSetFileEndingBasedOnContentType() {

        try {

            URL url = new URL("https://dev-cdn.hjm.aws.ineratest.org/73bf6257-4198-4c82-9a4c-9904e70bba1c/61a26d85-0fb1-4a4d-87c5-e97591689778/pattymelt.jpg");

            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            assertEquals("73bf6257-4198-4c82-9a4c-9904e70bba1c/61a26d85-0fb1-4a4d-87c5-e97591689778/pattymelt.jpeg", mediaUploadAmazonS3Controller.checkAndCorrectFileEndingBasedOnContentType(httpURLConnection, "73bf6257-4198-4c82-9a4c-9904e70bba1c/61a26d85-0fb1-4a4d-87c5-e97591689778/pattymelt.jpg"));
            httpURLConnection.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();

        } catch (HjalpmedelstjanstenValidationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    @Test
    public void handlesURLThatIsARedirect() {
        HttpURLConnection httpURLConnection = null;
        try {

            URL url = new URL("http://salubrious.se/images/PDF/REHAB/rehab_stl_tillbeh/stl_tillbeh_stealth_glacial.pdf");
            System.out.println("Original url: " + url.openConnection().getURL());
            httpURLConnection = mediaUploadAmazonS3Controller.manageRedirects(url);
            System.out.println(httpURLConnection.getResponseCode());
            System.out.println("Redirected url: " + httpURLConnection.getURL());

            assertEquals(200, httpURLConnection.getResponseCode());
            httpURLConnection.disconnect();


        } catch (MalformedURLException e) {
            e.printStackTrace();
            httpURLConnection.disconnect();

        } catch (IOException e) {
            e.printStackTrace();
            httpURLConnection.disconnect();

        }

    }

    @Test
    public void handlesURLThatIsNotARedirect() {
        HttpURLConnection httpURLConnection = null;
        try {

            URL url = new URL("https://dev-cdn.hjm.aws.ineratest.org/73bf6257-4198-4c82-9a4c-9904e70bba1c/61a26d85-0fb1-4a4d-87c5-e97591689778/pattymelt.jpg");
            httpURLConnection = mediaUploadAmazonS3Controller.manageRedirects(url);
            assertEquals(200, httpURLConnection.getResponseCode());
            httpURLConnection.disconnect();


        } catch (MalformedURLException e) {
            e.printStackTrace();
            httpURLConnection.disconnect();

        } catch (IOException e) {
            e.printStackTrace();
            httpURLConnection.disconnect();

        }
    }

    @Test
    public void whatIfUserEntersNull() {
        HttpURLConnection httpURLConnection = null;
        try {
            URL url = new URL("");
            httpURLConnection = mediaUploadAmazonS3Controller.manageRedirects(url);
            assertEquals(200, httpURLConnection.getResponseCode());
            httpURLConnection.disconnect();

        } catch (MalformedURLException e) {
            return;

        } catch (IOException e) {
            return;
        }
    }

    //Risk of flaky test, hence there will be no assert on this one. It does however confirm that the implementation would support pure redirects (No file ending at all)
    //however it's not supported in the rest of the system. To do so we would need to remove any and all checks for file-endings and instead do it directly in the MediaUploadController.
    @Test
    public void whatIfThereIsNoFileEnding() {
        HttpURLConnection httpURLConnection;
        try {
            URL url = new URL("http://harnosandesport.se/gotokey?key=a83891cf-2c8b-4228-bfaf-e1058aa163dc");
            System.out.println("Original url: " + url.openConnection().getURL());
            httpURLConnection = mediaUploadAmazonS3Controller.manageRedirects(url);
            System.out.println("Redirected url: " + httpURLConnection.getURL());
            httpURLConnection.disconnect();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

// These are performance tests for the S3 functions, no need to run these on build, only for performance testing.
  /*  @Test
    public void manageRedirects() throws IOException, HjalpmedelstjanstenValidationException {

        String bucketName = "test";
        String mediaBaseUrl = "http://localhost:9000/test/";

        AWSCredentials credentials = new BasicAWSCredentials("test", "testtest");

        ClientConfiguration clientConfiguration = new ClientConfiguration();
        clientConfiguration.setConnectionTimeout(120 * 1000);
        clientConfiguration.withMaxErrorRetry(15);
        clientConfiguration.setSignerOverride("AWSS3V4SignerType");
        clientConfiguration.withMaxConnections(100);

        AmazonS3 se = AmazonS3ClientBuilder
                .standard()
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration("http://localhost:9000", Regions.US_EAST_1.name()))
                .withPathStyleAccessEnabled(true)
                .withClientConfiguration(clientConfiguration)
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .build();

        long timerStart = System.currentTimeMillis();
        for(int i = 0; i < 50; i++) {
            URL url = new URL("https://stage-cdn.hjm.aws.ineratest.org/2a7c4d1c-51a0-4340-a913-002890526ca2/4190fb2d-c219-49c0-b804-f6f99dbcd602/circaid_tillbehoer.pdf");
            URLConnection response = url.openConnection();

            String externalKey = url.getPath().substring(url.getPath().lastIndexOf('/') + 1);
            externalKey += i;
            InputStream input = response.getInputStream();

            ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentLength(Long.valueOf(response.getHeaderField("Content-length")));

            //byte[] theFile = this.getBytes(input);

            PutObjectRequest request = new PutObjectRequest(bucketName, externalKey, input, objectMetadata);
            request.getRequestClientOptions().setReadLimit(Integer.valueOf(response.getHeaderField("Content-length")));           //  String bucketName, String key, InputStream input, ObjectMetadata metadata
            se.putObject(request);
            }
        long timerEnd = System.currentTimeMillis();
        System.out.print("With content-length and setReadLimit set on request: " + (timerEnd - timerStart) + "ms");

    }
   @Test
    public void manageRedirectsNoContentType() throws IOException, HjalpmedelstjanstenValidationException {

        String bucketName = "test";
        String mediaBaseUrl = "http://localhost:9000/test/";

        AWSCredentials credentials = new BasicAWSCredentials("test", "testtest");

        ClientConfiguration clientConfiguration = new ClientConfiguration();
        clientConfiguration.setConnectionTimeout(120 * 1000);
        clientConfiguration.withMaxErrorRetry(15);
        clientConfiguration.setSignerOverride("AWSS3V4SignerType");
        clientConfiguration.withMaxConnections(100);

        AmazonS3 se = AmazonS3ClientBuilder
                .standard()
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration("http://localhost:9000", Regions.US_EAST_1.name()))
                .withPathStyleAccessEnabled(true)
                .withClientConfiguration(clientConfiguration)
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .build();

        long timerStart = System.currentTimeMillis();
        for(int i = 0; i < 100; i++) {
            URL url = new URL("https://stage-cdn.hjm.aws.ineratest.org/2a7c4d1c-51a0-4340-a913-002890526ca2/4190fb2d-c219-49c0-b804-f6f99dbcd602/circaid_tillbehoer.pdf");
            URLConnection response = url.openConnection();

            String externalKey = url.getPath().substring(url.getPath().lastIndexOf('/') + 1) ;
            externalKey += i;
            InputStream input = response.getInputStream();

            ObjectMetadata objectMetadata = new ObjectMetadata();
            //objectMetadata.setContentLength(Long.valueOf(response.getHeaderField("Content-length")));

            //byte[] theFile = this.getBytes(input);

            PutObjectRequest request = new PutObjectRequest(bucketName, externalKey, input, objectMetadata);
            request.getRequestClientOptions().setReadLimit(Integer.MAX_VALUE);           //  String bucketName, String key, InputStream input, ObjectMetadata metadata
            se.putObject(request);

        }

        long timerEnd = System.currentTimeMillis();
        System.out.println("Without headers set: " + (timerEnd - timerStart) + "ms");

    } */
}
/*
* Copyright (C) 2019 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.product.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.NoLogger;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.validation.ProductAPITest;
import se.inera.hjalpmedelstjansten.model.api.validation.UserAPITest;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;

/**
 *
 * @author tomber
 */
public class ProductValidationTest {
    
    static long productId = 1;
    static long organizationId = 1;
    static final long pricelistApproverUserId = 15;
    private ProductValidation businessValidator;
    ProductController productController;
    ArticleController articleController;
    UserAPI userAPI;
    
    @Before
    public void init() {
        productController = Mockito.mock(ProductController.class);
        articleController = Mockito.mock(ArticleController.class);
        businessValidator = new ProductValidation();
        businessValidator.LOG = new NoLogger();
        businessValidator.validationMessageService = Mockito.mock(ValidationMessageService.class);
        Mockito.when(productController.findByProductNumberAndOrganization(Mockito.anyString(), Mockito.anyLong())).thenReturn(null);
        businessValidator.productController = productController;
        businessValidator.articleController = articleController;
        userAPI = UserAPITest.createValidUserAPI(pricelistApproverUserId, organizationId, null, UserRole.RoleName.CustomerAgreementManager, null);
    }

    // business validator tests
    // business tests assumes bean validation has already been done
    
 /*   @Test
    public void businessTestInvalidCreateStatusDiscontinued() {
        ProductAPI productAPI = ProductAPITest.createValidProductAPI();
        productAPI.setStatus(Product.Status.DISCONTINUED.toString());
        try {
            businessValidator.validateForCreate(productAPI, organizationId);
            Assert.fail("Validation passed but shouldn't");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // ok
        }
    }
  */
    
    @Test
    public void businessTestRightStatusPublishedToDiscontinued() {
        ProductAPI productAPI = ProductAPITest.createValidProductAPI();
        productAPI.setStatus(Product.Status.DISCONTINUED.toString());
        productAPI.setReplacementDate(1l);
        productAPI.setInactivateRowsOnReplacement(Boolean.TRUE);
        Product product = ProductControllerTest.createValidProduct(productId, organizationId);
        product.setStatus(Product.Status.PUBLISHED);
        try {
            businessValidator.validateForUpdate(productAPI, product, organizationId,userAPI);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Validation didn't pass but should");
        }
    }
    
    @Test
    public void businessTestRightStatusPublishedToDiscontinuedNoInactivateRows() {
        ProductAPI productAPI = ProductAPITest.createValidProductAPI();
        productAPI.setStatus(Product.Status.DISCONTINUED.toString());
        productAPI.setReplacementDate(1l);
        Product product = ProductControllerTest.createValidProduct(productId, organizationId);
        product.setStatus(Product.Status.PUBLISHED);
        try {
            businessValidator.validateForUpdate(productAPI, product, organizationId,userAPI);
            Assert.fail("Validation passed but shouldn't");
        } catch (HjalpmedelstjanstenValidationException ex) {
        }
    }
    
    @Test
    public void businessTestRightStatusSame() {
        ProductAPI productAPI = ProductAPITest.createValidProductAPI();
        productAPI.setStatus(Product.Status.PUBLISHED.toString());
        Product product = ProductControllerTest.createValidProduct(productId, organizationId);
        product.setStatus(Product.Status.PUBLISHED);
        try {
            businessValidator.validateForUpdate(productAPI, product, organizationId,userAPI);
        } catch (HjalpmedelstjanstenValidationException ex) {
            Assert.fail("Validation didn't pass but should");     
        }
    }
    
    @Test
    public void businessTestWrongStatusDiscontinuedToPublished() {
        ProductAPI productAPI = ProductAPITest.createValidProductAPI();
        productAPI.setStatus(Product.Status.PUBLISHED.toString());
        Product product = ProductControllerTest.createValidProduct(productId, organizationId);
        product.setStatus(Product.Status.DISCONTINUED);
        try {
            businessValidator.validateForUpdate(productAPI, product, organizationId,userAPI);
        } catch (HjalpmedelstjanstenValidationException ex) {
//            Assert.fail("Validation didn't pass but should");
        }
    }
    
    @Test
    public void businessTestWrongSetManufacturerEmail() {
        ProductAPI productAPI = ProductAPITest.createValidProductAPI();
        productAPI.getManufacturerElectronicAddress().setEmail("test@test.se");
        Product product = ProductControllerTest.createValidProduct(productId, organizationId);
        try {
            businessValidator.validateForUpdate(productAPI, product, organizationId,userAPI);
            Assert.fail("Validation passed but shouldn't");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // success
        }
    }
    
    @Test
    public void businessTestWrongSetManufacturerMobile() {
        ProductAPI productAPI = ProductAPITest.createValidProductAPI();
        productAPI.getManufacturerElectronicAddress().setMobile("1234567");
        Product product = ProductControllerTest.createValidProduct(productId, organizationId);
        try {
            businessValidator.validateForUpdate(productAPI, product, organizationId,userAPI);
            Assert.fail("Validation passed but shouldn't");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // success
        }
    }
    
    @Test
    public void businessTestWrongSetManufacturerTelephone() {
        ProductAPI productAPI = ProductAPITest.createValidProductAPI();
        productAPI.getManufacturerElectronicAddress().setTelephone("1234567");
        Product product = ProductControllerTest.createValidProduct(productId, organizationId);
        try {
            businessValidator.validateForUpdate(productAPI, product, organizationId,userAPI);
            Assert.fail("Validation passed but shouldn't");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // success
        }
    }
    
    @Test
    public void businessTestWrongSetManufacturerFax() {
        ProductAPI productAPI = ProductAPITest.createValidProductAPI();
        productAPI.getManufacturerElectronicAddress().setFax("1234567");
        Product product = ProductControllerTest.createValidProduct(productId, organizationId);
        try {
            businessValidator.validateForUpdate(productAPI, product, organizationId,userAPI);
            Assert.fail("Validation passed but shouldn't");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // success
        }
    }
        
    @Test
    public void testUpdateExistingProductNumber() {
        Product existingProduct = ProductControllerTest.createValidProduct(productId, organizationId);
        existingProduct.setUniqueId(existingProduct.getUniqueId()+1);
        Mockito.when(productController.findByProductNumberAndOrganization(Mockito.anyString(), Mockito.anyLong())).thenReturn(existingProduct);
        
        ProductAPI productAPI = ProductControllerTest.createValidProductAPI();
        Product product = ProductControllerTest.createValidProduct(productId, organizationId);
        try {
            businessValidator.validateForUpdate(productAPI, product, organizationId,userAPI);
            Assert.fail("Validation passed but shouldn't");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("productNumber", errorMessageAPI.getField());
        }
    }
    
    @Test
    public void testUpdateExistingArticleNumber() {
        Article existingArticle = ArticleControllerTest.createValidArticle(productId, organizationId);
        Mockito.when(articleController.findByArticleNumberAndOrganization(Mockito.anyString(), Mockito.anyLong())).thenReturn(existingArticle);
        
        ProductAPI productAPI = ProductControllerTest.createValidProductAPI();
        Product product = ProductControllerTest.createValidProduct(productId, organizationId);
        try {
            businessValidator.validateForUpdate(productAPI, product, organizationId,userAPI);
            Assert.fail("Validation passed but shouldn't");
        } catch (HjalpmedelstjanstenValidationException ex) {
            // good
            Assert.assertEquals(1, ex.getValidationMessages().size());
            ErrorMessageAPI errorMessageAPI = ex.getValidationMessages().iterator().next();
            Assert.assertEquals("productNumber", errorMessageAPI.getField());
        }
    }
    
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.agreement.controller;

import se.inera.hjalpmedelstjansten.business.BaseController;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.organization.controller.BusinessLevelController;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.entity.*;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.persistence.Query;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleMapper;
import se.inera.hjalpmedelstjansten.business.indexing.controller.ElasticSearchController;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;

/**
 * Class for handling business logic of Pricelists on Agreements. This includes talking to the
 * database.
 * 
 * @author Tommy Berglund
 */
@Stateless
public class AgreementPricelistController extends BaseController {
    
    @Inject
    HjmtLogger LOG;
    
    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;
    
    @Inject
    AgreementPricelistValidation pricelistValidation;
    
    @Inject
    ValidationMessageService validationMessageService;
    
    @Inject
    OrganizationController organizationController;
        
    @Inject
    BusinessLevelController businessLevelController;
    
    @Inject
    UserController userController;
    
    @Inject
    AgreementController agreementController;
    
    @Inject
    ElasticSearchController elasticSearchController;
    
    @Inject
    Event<InternalAuditEvent> internalAuditEvent;
        
    @Inject
    String defaultPricelistNumber;
    
    /**
     * Get the List of <code>PricelistAPI</code> for the agreement
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement
     * @param userAPI logged in user information
     * @return the corresponding List of <code>AgreementPricelistAPI</code>
     */
    public List<AgreementPricelistAPI> getPricelistAPIsOnAgreement(long organizationUniqueId, long agreementUniqueId, UserAPI userAPI) {
        LOG.log(Level.FINEST, "getPricelistAPIsOnAgreement( organizationUniqueId: {0}, agreementUniqueId: {1} )", new Object[] {organizationUniqueId, agreementUniqueId});
        List<AgreementPricelist> pricelists = getPricelistsOnAgreement(organizationUniqueId, agreementUniqueId, userAPI);
        if( pricelists != null ) {
            AgreementPricelist currentPricelist = getCurrentPricelist( agreementUniqueId );
            List<AgreementPricelistAPI> agreementPricelistAPIs = AgreementPricelistMapper.map( pricelists, false, currentPricelist);
            if( agreementPricelistAPIs != null ) {
                for( AgreementPricelistAPI agreementPricelistAPI : agreementPricelistAPIs ) {
                    long numberOfRowsOnPricelist = getNumberOfRowsOnPricelist(agreementPricelistAPI.getId());
                    agreementPricelistAPI.setHasPricelistRows(numberOfRowsOnPricelist > 0);
                }
            }
            return agreementPricelistAPIs;
        }
        return null;
    }
    
    /**
     * Get all pricelists on the given agreement.
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement
     * @param userAPI logged in user information
     * @return the corresponding List of <code>AgreementPricelist</code>
     */
    private List<AgreementPricelist> getPricelistsOnAgreement(long organizationUniqueId, long agreementUniqueId, UserAPI userAPI) {
        LOG.log(Level.FINEST, "getPricelistAPIsOnAgreement( organizationUniqueId: {0}, agreementUniqueId: {1} )", new Object[] {organizationUniqueId, agreementUniqueId});
        // make sure user can fetch agreement
        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);
        if( agreement != null ) {
            List<AgreementPricelist> pricelists = em.createNamedQuery(AgreementPricelist.GET_ALL_BY_AGREEMENT).
                    setParameter("agreementUniqueId", agreementUniqueId).
                    getResultList();
            if( pricelists != null ) {
                return pricelists;
            }
        } else {
            LOG.log( Level.WARNING, "Attempt by user: {0} to fetch pricelists on agreement: {1} but the agreement is not visible to the user", new Object[] {userAPI.getId(), agreementUniqueId});
        }
        return null;
    }
    
    /**
     * Get the <code>PricelistAPI</code> for the given unique id on the 
     * specified organization
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist to get
     * @param userAPI user information
     * @return the corresponding <code>AgreementAPI</code>
     */
    public AgreementPricelistAPI getPricelistAPI(long organizationUniqueId, 
            long agreementUniqueId, 
            long pricelistUniqueId, 
            UserAPI userAPI, 
            String sessionId, 
            String requestIp) {
        LOG.log(Level.FINEST, "getPricelistAPI( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        AgreementPricelist pricelist = getPricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI);
        if( pricelist != null ) {
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST, InternalAudit.ActionType.VIEW, userAPI.getId(), sessionId, pricelistUniqueId, requestIp));
            AgreementPricelist currentPricelist = getCurrentPricelist( agreementUniqueId );
            return AgreementPricelistMapper.mapWithPricelist(pricelist, true, currentPricelist);
        }
        return null;
    }
    
    /**
     * Search available articles for pricelist, meaning all actice articles on organisation
     * that is not already in the pricelist
     * 
     * @param organizationUniqueId
     * @param agreementUniqueId
     * @param pricelistUniqueId
     * @param query
     * @param statuses
     * @param articleTypes
     * @param offset
     * @param limit
     * @param userAPI
     * @return 
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException 
     */
    public SearchDTO searchArticlesForAgreementPricelist(long organizationUniqueId, 
            long agreementUniqueId, 
            long pricelistUniqueId, 
            String query, 
            List<Product.Status> statuses, 
            List<Article.Type> articleTypes, 
            int offset, 
            int limit, 
            UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "searchArticlesForAgreementPricelist( organizationUniqueId: {0}, statuses: {1}, articleTypes: {2}, offset: {3}, limit: {4} )", new Object[] {organizationUniqueId, statuses, articleTypes, offset, limit});
        AgreementPricelist pricelist = getPricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI);
        if( pricelist != null ) {
            List<Long> articlesAlreadyInPricelist = em.createQuery("SELECT agp.article.uniqueId FROM AgreementPricelistRow agp WHERE agp.pricelist.uniqueId = :agreementPricelistId").
                    setParameter("agreementPricelistId", pricelistUniqueId)
                    .getResultList();

            Product.Status status = null;
            if (statuses != null && statuses.size() == 1) {
                status = statuses.get(0);
            }

            return elasticSearchController.searchOrganization(query, 
                    organizationUniqueId, 
                    false, 
                    true,
                    "",
                    "",
                    articleTypes,
                    articlesAlreadyInPricelist,
                    status,
                    25, 
                    offset,
                    false);
        }
        return null;
    }
    
    /**
     * Get the <code>Pricelist</code> with the given id on the specified organization.
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param userAPI user information
     * @return the corresponding <code>Pricelist</code>
     */
    public AgreementPricelist getPricelist(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, UserAPI userAPI) {
        LOG.log(Level.FINEST, "getPricelist( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        AgreementPricelist pricelist = em.find(AgreementPricelist.class, pricelistUniqueId);
        if( pricelist == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist: {1} that does not exist. Returning 404.", new Object[] {userAPI.getId(), pricelistUniqueId});
            return null;
        }
        // make sure user can fetch agreement connected to pricelist
        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);
        if( agreement == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist: {1}, but agreement: {2} is not available. Returning 404.", new Object[] {userAPI.getId(), pricelistUniqueId, agreementUniqueId});
            return null;
        }
        if( !pricelist.getAgreement().getUniqueId().equals(agreement.getUniqueId()) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist: {1} not on given agreement: {2}. Returning 404.", new Object[] {userAPI.getId(), pricelistUniqueId, agreement.getUniqueId()});
            return null;
        }
        return pricelist;
    }
    
    /**
     * When an agreement is created, one pricelist should be created by default.
     * 
     * @param organizationUniqueId
     * @param agreement the created agreement
     * @param userAPI
     * @param sessionId
     * @return the created <code>PricelistAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     */
    public AgreementPricelistAPI createFirstPricelistForAgreement( long organizationUniqueId, Agreement agreement, UserAPI userAPI, String sessionId, String requestIp ) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createFirstPricelistForAgreement( agreement: {0} )", new Object[] {agreement.getUniqueId()});
        AgreementPricelistAPI pricelistAPI = new AgreementPricelistAPI();
        pricelistAPI.setNumber(defaultPricelistNumber);
        pricelistAPI.setValidFrom(agreement.getValidFrom().getTime());
        return createPricelist(organizationUniqueId, agreement, pricelistAPI, userAPI, sessionId, requestIp);
    }
    
    /**
     * Create a pricelist based on the supplied data
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement
     * @param pricelistAPI user supplied data
     * @param userAPI logged in user's session information
     * @param sessionId
     * @return the created <code>PricelistAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     */
    public AgreementPricelistAPI createPricelist( long organizationUniqueId, long agreementUniqueId, AgreementPricelistAPI pricelistAPI, UserAPI userAPI, String sessionId, String requestIp ) throws HjalpmedelstjanstenValidationException {
        //TODO mail here aswell HJAL-95?
        LOG.log(Level.FINEST, "createPricelist( organizationUniqueId: {0}, agreementUniqueId: {1} )", new Object[]{organizationUniqueId, agreementUniqueId});
        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);

        List<UserAPI> supplierAgreementManager = null;
        Organization customerOrganization = null;
        if (userController != null){
            List<UserRole.RoleName> roleNames = new ArrayList<>();
            roleNames.add(UserRole.RoleName.SupplierAgreementManager);
            supplierAgreementManager = userController.getUsersInRolesOnOrganization(agreement.getSupplier().getUniqueId(), roleNames);
            customerOrganization = organizationController.getOrganization(organizationUniqueId);
        }
        AgreementPricelistAPI agreementPricelistAPI = createPricelist(organizationUniqueId, agreement, pricelistAPI, userAPI, sessionId, requestIp);
        if(customerOrganization != null && supplierAgreementManager != null) {
            agreementController.sendNotificationPricelistAddedMail(supplierAgreementManager, customerOrganization.getOrganizationName(), agreement.getAgreementNumber(), agreementPricelistAPI.getNumber());
        }
        return agreementPricelistAPI;
    }
    
    /**
     * Create a pricelist based on the supplied data
     * 
     * @param agreement the agreement of the pricelist
     * @param pricelistAPI user supplied data
     * @return the created <code>PricelistAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     */
    private AgreementPricelistAPI createPricelist( long organizationUniqueId, 
            Agreement agreement, 
            AgreementPricelistAPI pricelistAPI, 
            UserAPI userAPI, 
            String sessionId,
            String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createPricelist( agreement: {0} )", new Object[] {agreement.getUniqueId()});
        
        // organizations/business levels (customers) that the agreement is shared 
        // with can read the agreement, but only the customer can edit it
        if( !agreement.getCustomer().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to create agreement pricelist: {1}, but user organization: {2} is not customer", new Object[] {userAPI.getId(), agreement.getUniqueId(), organizationUniqueId});
            return null;
        }
        
        pricelistValidation.validateForCreate(pricelistAPI, agreement);
        AgreementPricelist pricelist = AgreementPricelistMapper.map(pricelistAPI);
        // pricelists valid from must always be at least one day in the future
        //pricelist.setStatus(Pricelist.Status.FUTURE);
        pricelist.setAgreement(agreement);
        em.persist(pricelist);
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST, InternalAudit.ActionType.CREATE, userAPI.getId(), sessionId, pricelist.getUniqueId(), requestIp));
        AgreementPricelist currentPricelist = getCurrentPricelist( agreement.getUniqueId() );
        return AgreementPricelistMapper.mapWithPricelist(pricelist, true, currentPricelist);
    }

    /**
     * Create a pricelist based on the supplied data
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement
     * @param pricelistUniqueId unique id of the pricelist
     * @param agreementPricelistAPI user supplied data
     * @param userAPI logged in user's session information
     * @param sessionId
     * @return the created <code>PricelistAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     */
    public AgreementPricelistAPI updatePricelist(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, AgreementPricelistAPI agreementPricelistAPI, UserAPI userAPI, String sessionId) throws HjalpmedelstjanstenValidationException {
        AgreementPricelist agreementPricelist = getPricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI);
        if( agreementPricelist == null ) {
            return null;
        }
        
        // organizations/business levels (customers) that the agreement is shared 
        // with can read the agreement, but only the customer can edit it
        if( !agreementPricelist.getAgreement().getCustomer().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to edit agreement pricelist: {1}, but user organization: {2} is not customer", new Object[] {userAPI.getId(), agreementPricelist.getAgreement().getUniqueId(), organizationUniqueId});
            return null;
        }
        
        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);
        AgreementPricelist currentPricelist = getCurrentPricelist( agreement.getUniqueId() );
        AgreementPricelist.Status status = AgreementPricelistMapper.getPricelistStatus(agreementPricelist, currentPricelist);
        
        // future pricelists can always be modified
        if( status != AgreementPricelist.Status.FUTURE ) {
            // no rows may exist on pricelist for update to be valid
            long numberOfRowsOnAgreement = getNumberOfRowsOnPricelist(pricelistUniqueId);
            if( numberOfRowsOnAgreement > 0 ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to update pricelist: {1} but rows already exist in pricelist", new Object[] {userAPI.getId(), pricelistUniqueId});
                throw validationMessageService.generateValidationException("rows", "pricelist.update.rowsExist");                
            }
        }
        
        pricelistValidation.validateForUpdate(agreementPricelistAPI, agreementPricelist, agreement);
        agreementPricelist.setNumber(agreementPricelistAPI.getNumber());
        agreementPricelist.setValidFrom(new Date(agreementPricelistAPI.getValidFrom()));
        return AgreementPricelistMapper.mapWithPricelist(agreementPricelist, true, currentPricelist);
    }
    

    /**
     * Get the total number of rows in pricelist
     * 
     * @param pricelistUniqueId the unique id of the pricelist
     * @return the number of rows in the pricelist
     */
    private long getNumberOfRowsOnPricelist(long pricelistUniqueId) {
        Long count = (Long) em.createNamedQuery(AgreementPricelistRow.COUNT_BY_PRICELIST).
                setParameter("pricelistUniqueId", pricelistUniqueId).
                getSingleResult();
        return count == null ? 0: count;
    }

    
    /**
     * Find pricelists on a specific agreement with given validFrom
     * 
     * @param validFrom
     * @param agreementUniqueId unique id of the agreement
     * @return 
     */
    public List<AgreementPricelist> findByValidFromAndAgreement(long validFrom, long agreementUniqueId) {
        LOG.log(Level.FINEST, "findByValidFromAndAgreement( agreementUniqueId: {0} )", new Object[] {agreementUniqueId} );
        Date date = new Date(validFrom);
        return em.createNamedQuery(AgreementPricelist.FIND_BY_VALID_FROM_AND_AGREEMENT).
                setParameter("agreementUniqueId", agreementUniqueId).
                setParameter("validFrom", date).
                getResultList();
    }
    
    /**
     * Find pricelists on a specific agreement with given number
     * 
     * @param number
     * @param agreementUniqueId unique id of the agreement
     * @return 
     */
    public List<AgreementPricelist> findByNumberAndAgreement(String number, long agreementUniqueId) {
        LOG.log(Level.FINEST, "findByNumberAndAgreement( agreementUniqueId: {0} )", new Object[] {agreementUniqueId} );
        return em.createNamedQuery(AgreementPricelist.FIND_BY_NUMBER_AND_AGREEMENT).
                setParameter("agreementUniqueId", agreementUniqueId).
                setParameter("number", number).
                getResultList();
    }
    
    /**
     * Return the current pricelist on an agreement (if any). There may be just
     * FUTURE ones
     * 
     * @param agreementUniqueId unique id of the agreement
     * @return 
     */
    public AgreementPricelist getCurrentPricelist(long agreementUniqueId) {
        LOG.log(Level.FINEST, "getCurrentPricelist( agreementUniqueId: {0} )", new Object[] {agreementUniqueId} );
        List<AgreementPricelist> pricelists = em.createNamedQuery(AgreementPricelist.FIND_BY_PASSED_VALID_FROM_AND_AGREEMENT).
                setParameter("agreementUniqueId", agreementUniqueId).
                setParameter("validFrom", new Date()).
                getResultList();
        if( pricelists != null && !pricelists.isEmpty() ) {
            // query is ORDER BY validFrom DESC so first is most recent
            return pricelists.get(0);
        }
        return null;
    }

}
/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.organization.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.model.api.PostAddressAPI;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.PostAddress;

/**
 *
 * @author Tommy Berglund
 */
public class OrganizationMapper {
    
    public static final List<OrganizationAPI> map(List<Organization> organizations) {
        if( organizations == null ) {
            return null;
        }
        List<OrganizationAPI> organizationAPIs = new ArrayList<>();
        for( Organization organization : organizations ) {
            organizationAPIs.add(map(organization, false));
        }
        return organizationAPIs;
    }
    
    public static final OrganizationAPI map(Organization organization, boolean includeEverything ) {
        if( organization == null ) {
            return null;
        }
        OrganizationAPI organizationAPI = new OrganizationAPI();
        organizationAPI.setId(organization.getUniqueId());
        organizationAPI.setOrganizationName(organization.getOrganizationName());
        organizationAPI.setOrganisationType(organization.getOrganizationType().toString());
        organizationAPI.setOrganizationNumber(organization.getOrganizationNumber());
        organizationAPI.setGln(organization.getGln());
        organizationAPI.setCountry(CountryMapper.map(organization.getCountry()));
        organizationAPI.setValidFrom(organization.getValidFrom().getTime());
        if( organization.getValidTo() != null ) {
            organizationAPI.setValidTo(organization.getValidTo().getTime());
        }
        organizationAPI.setActive( calculateActive(organization.getValidFrom(), organization.getValidTo()) );
        if( includeEverything ) {
            if( organization.getPostAddresses() != null ) {
                organizationAPI.setPostAddresses(mapPostAddresses(organization.getPostAddresses()));
            }
            if( organization.getElectronicAddress() != null ) {
                organizationAPI.setElectronicAddress(ElectronicAddressMapper.map(organization.getElectronicAddress()));
            }
        }
        return organizationAPI;
    }
    
    public static final Organization map(OrganizationAPI organizationAPI) {
        if( organizationAPI == null ) {
            return null;
        }
        Organization organization = new Organization();
        organization.setOrganizationName(organizationAPI.getOrganizationName());
        organization.setOrganizationNumber(organizationAPI.getOrganizationNumber());
        organization.setOrganizationType(Organization.OrganizationType.valueOf(organizationAPI.getOrganizationType()));
        organization.setValidFrom(new Date(organizationAPI.getValidFrom()));
        if( organizationAPI.getValidTo() != null ) {
            organization.setValidTo(new Date(organizationAPI.getValidTo()));
        }
        organization.setGln(organizationAPI.getGln());
        if( organizationAPI.getElectronicAddress() != null ) {            
            organization.setElectronicAddress(ElectronicAddressMapper.map(organizationAPI.getElectronicAddress()));
        }
        if( organizationAPI.getPostAddresses() != null ) {   
            List<PostAddress> postAddresses = mapPostAddressAPIs(organizationAPI.getPostAddresses(), organization);
            organization.setPostAddresses(postAddresses);
        }
        return organization;
    }
    
    private static List<PostAddressAPI> mapPostAddresses(List<PostAddress> postAddresses ) {
        List<PostAddressAPI> postAddressAPIs = new ArrayList<>();
        for( PostAddress postAddress : postAddresses ) {
            postAddressAPIs.add(mapPostAddress(postAddress));
        }
        return postAddressAPIs;
    }
    
    private static PostAddressAPI mapPostAddress(PostAddress postAddress ) {
        PostAddressAPI postAddressAPI = new PostAddressAPI();
        postAddressAPI.setId(postAddress.getUniqueId());
        postAddressAPI.setAddressType(postAddress.getAddressType());
        postAddressAPI.setStreetAddress(postAddress.getStreetAddress());
        postAddressAPI.setPostCode(postAddress.getPostCode());
        postAddressAPI.setCity(postAddress.getCity());
        return postAddressAPI;
    }

    private static List<PostAddress> mapPostAddressAPIs(List<PostAddressAPI> postAddresseAPIs, Organization organization) {
        List<PostAddress> postAddresses = new ArrayList<>();
        for( PostAddressAPI postAddressAPI : postAddresseAPIs ) {
            PostAddress postAddress = mapPostAddressAPI(postAddressAPI);
            postAddress.setOrganization(organization);
            postAddresses.add(postAddress);
        }
        return postAddresses;
    }
    
    private static PostAddress mapPostAddressAPI(PostAddressAPI postAddressAPI ) {
        PostAddress postAddress = new PostAddress();
        postAddress.setAddressType(postAddressAPI.getAddressType());
        postAddress.setStreetAddress(postAddressAPI.getStreetAddress());
        postAddress.setPostCode(postAddressAPI.getPostCode());
        postAddress.setCity(postAddressAPI.getCity());
        return postAddress;
    }

    public static boolean calculateActive(Date validFrom, Date validTo) {
        Date now = new Date();
        if( now.before(validFrom) ) {
            return false;
        } else if( validTo == null ) {
            return true;
        } else {
            return now.before(validTo);
        }
    }
    
}

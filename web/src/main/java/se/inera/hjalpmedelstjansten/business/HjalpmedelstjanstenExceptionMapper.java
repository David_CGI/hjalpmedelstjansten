/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import se.inera.hjalpmedelstjansten.model.api.ErrorAPI;

/**
 * This exception causes a HTTP response code of 500 and lists error in the 
 * response body.
 * 
 * @see se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenException
 * @author Tommy Berglund
 */
@Provider
public class HjalpmedelstjanstenExceptionMapper implements ExceptionMapper<HjalpmedelstjanstenException> {

    @Override
    public Response toResponse(HjalpmedelstjanstenException exception) {
        if( exception.getType() != null && exception.getType() == HjalpmedelstjanstenException.Type.FORBIDDEN ) {
            return Response.status(Response.Status.FORBIDDEN).
                build();
        }
        ErrorAPI errorAPI = new ErrorAPI();
        errorAPI.setErrors(exception.getValidationMessages());
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
            .entity(errorAPI)
            .build();
    }
    
}

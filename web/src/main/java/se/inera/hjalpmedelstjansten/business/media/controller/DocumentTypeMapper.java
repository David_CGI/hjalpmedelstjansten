/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.media.controller;

import java.util.ArrayList;
import java.util.List;
import se.inera.hjalpmedelstjansten.model.api.cv.CVDocumentTypeAPI;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVDocumentType;

/**
 *
 * @author Tommy Berglund
 */
public class DocumentTypeMapper {
    
    public static final List<CVDocumentTypeAPI> map(List<CVDocumentType> documentTypes) {
        if( documentTypes == null ) {
            return null;
        }
        List<CVDocumentTypeAPI> documentTypeAPIs = new ArrayList<>();
        for( CVDocumentType documentType : documentTypes ) {
            documentTypeAPIs.add(map(documentType));
        }
        return documentTypeAPIs;
    }
    
    public static final CVDocumentTypeAPI map(CVDocumentType documentType) {
        if( documentType == null ) {
            return null;
        }
        CVDocumentTypeAPI documentTypeAPI = new CVDocumentTypeAPI();
        documentTypeAPI.setId(documentType.getUniqueId());
        documentTypeAPI.setCode(documentType.getCode());
        documentTypeAPI.setValue(documentType.getValue());
        return documentTypeAPI;
    }
    
}

/*
* Copyright (C) 2019 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.dynamictext.view;

import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.model.api.DynamicTextAPI;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;

import se.inera.hjalpmedelstjansten.model.entity.DynamicText;
import se.inera.hjalpmedelstjansten.business.dynamictext.controller.DynamicTextController;


import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.logging.Level;

/**
 * REST API for dynamic text.
 * 
 * @author Per Abrahamsson
 */
@Stateless
@Path("dynamicText")
@Interceptors({ PerformanceLogInterceptor.class })
public class DynamicTextService extends BaseService {
    
    @Inject
    private HjmtLogger LOG;
    
    @Inject
    DynamicTextController dynamicTextController;

    
    @EJB
    AuthHandler authHandler;
    
    /**
     * Search dynamicText by uniqueId
     * 
     * @param uniqueId
     * @return a string of <code>DynamicTextAPI</code>
     */

    @GET
    @Path("{uniqueId}")
    //@SecuredService(permissions = {"assortment:view"})
    public Response getDynamicText(@Context HttpServletRequest httpServletRequest,
            @PathParam("uniqueId") long uniqueId ) {
        LOG.log(Level.FINEST, "getDynamicText( uniqueId: {0} )", new Object[] {uniqueId});

        DynamicText dynamicText = dynamicTextController.getDynamicText(uniqueId);
        LOG.log(Level.FINEST, "dynamicText: {0}", new Object[] {dynamicText});

        if( dynamicText == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(dynamicText).build();
        }

    }

    
    /**
     * Update dynamicText
     *
     * @param uniqueId the id of the dynamictext to update
     * @param dynamicTextAPI
     */


    @PUT
    @Path("{uniqueId}")
    //@SecuredService(permissions = {"dynamicText:update"})
    public Response updateDynamicText(@Context HttpServletRequest httpServletRequest,
                                      @PathParam("uniqueId") long uniqueId, DynamicTextAPI dynamicTextAPI) throws HjalpmedelstjanstenValidationException
    {
        LOG.log(Level.FINEST, "updateDynamicText( uniqueId: {0}, dynamicText: {1} )", new Object[] {uniqueId, dynamicTextAPI});

        boolean isSuperAdmin = authHandler.hasRole(UserRole.RoleName.Superadmin.toString());
        if( !isSuperAdmin) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }


        dynamicTextAPI = dynamicTextController.updateDynamicTextX(uniqueId, dynamicTextAPI);


        if( dynamicTextAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(dynamicTextAPI).build();
        }

    }

}

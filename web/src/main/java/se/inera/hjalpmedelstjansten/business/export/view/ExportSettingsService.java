/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.export.view;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.ResultExporterController;
import se.inera.hjalpmedelstjansten.business.export.controller.ExportSettingsController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.*;

/**
 *
 * @author Tommy Berglund
 */
@Stateless
@Path("organizations/{organizationUniqueId}/exportsettings")
@Interceptors({ PerformanceLogInterceptor.class })
public class ExportSettingsService extends BaseService {
 
    @Inject
    private HjmtLogger LOG;
    
    @Inject
    private ExportSettingsController exportSettingsController;

    @Inject
    ResultExporterController resultExporterController;
    
    @EJB
    AuthHandler authHandler;
        
    @GET
    @SecuredService(permissions = {"exportsettings:view_own"})
    public Response searchExportSettings(@PathParam("organizationUniqueId") long organizationUniqueId) {
        LOG.log(Level.FINEST, "searchExportSettings( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        UserEngagementAPI userEngagementAPI = UserController.getUserEngagementAPIByOrganizationId( organizationUniqueId, userAPI.getUserEngagements());
        List<ExportSettingsAPI> exportSettingsAPIs = exportSettingsController.findByOrganizationAndUser(organizationUniqueId, userEngagementAPI);
        if( exportSettingsAPIs == null ) {
            return Response.noContent().build();
        } else {
            return Response.ok(exportSettingsAPIs).build();
        }
    }
    
    @GET
    @Path("{exportSettingsUniqueId}")
    @SecuredService(permissions = {"exportsettings:view_own"})
    public Response getExportSettings(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("exportSettingsUniqueId") long exportSettingsUniqueId) {
        LOG.log(Level.FINEST, "getExportSettings( organizationUniqueId: {0}, exportSettingsUniqueId: {1} )", new Object[] {organizationUniqueId, exportSettingsUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        ExportSettingsAPI exportSettingsAPI = exportSettingsController.findAPI(organizationUniqueId, exportSettingsUniqueId);
        if( exportSettingsAPI == null ) {
            return Response.noContent().build();
        } else {
            return Response.ok(exportSettingsAPI).build();
        }
    }

    /**
     * Export GPs
     *
     * @param httpServletRequest
     * @param query
     * @return an excel containing the list of search results
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @Path("{exportSettingsUniqueId}/export")
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=UTF-8")
    @SecuredService(permissions = {"exportsettings:view_own"})
    @TransactionTimeout(value=20, unit = TimeUnit.MINUTES)
    public Response exportGPs(
            @Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("exportSettingsUniqueId") long exportSettingsUniqueId,
            @DefaultValue(value = "") @QueryParam("query") String query) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "exportGPs...");

        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        UserEngagementAPI userEngagementAPI = UserController.getUserEngagementAPIByOrganizationId( organizationUniqueId, userAPI.getUserEngagements());

        List<ExportSettingsAPI> exportSettingsAPIs = exportSettingsController.findByOrganizationAndUser(organizationUniqueId, userEngagementAPI);

        if( exportSettingsAPIs == null ) {
            return Response.noContent().build();
        } else {
            List<GeneralPricelistAPI> gpAPIs = exportSettingsController.findAPI(organizationUniqueId, exportSettingsUniqueId).getGeneralPricelists();
            try {
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'_'HHmm");
                String filename = "Export_GPs_" + dateTimeFormatter.format(ZonedDateTime.now()) + ".xlsx";

                byte[] exportBytes = resultExporterController.generateGPsList(gpAPIs);

                return Response.ok(
                        exportBytes,
                        javax.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM)
                        .header("content-disposition", "attachment; filename = " + filename)
                        .build();
            } catch (IOException ex) {
                LOG.log(Level.WARNING, "Failed to export GPs for exportfile to file", ex);
                return Response.serverError().build();
            }
        }
    }


    /**
     * Add a list of general pricelists to the export settings
     * 
     * @param organizationUniqueId
     * @param exportSettingsUniqueId
     * @param generalPricelistAPIs user supplied list of general pricelists
     * @return the updated <code>ExportSettingsAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @Path("{exportSettingsUniqueId}/generalpricelists")
    @SecuredService(permissions = {"exportsettings:update_own"})
    public Response addGeneralPricelistsToExportSetting(
            @PathParam("organizationUniqueId") long organizationUniqueId, 
            @PathParam("exportSettingsUniqueId") long exportSettingsUniqueId, 
            List<GeneralPricelistAPI> generalPricelistAPIs) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "addGeneralPricelistsToExportSetting( organizationUniqueId: {0}, exportSettingsUniqueId: {1} )", new Object[] {organizationUniqueId, exportSettingsUniqueId});
        ExportSettingsAPI exportSettingsAPI = exportSettingsController.addGeneralPricelistsToExportSetting(organizationUniqueId, exportSettingsUniqueId, generalPricelistAPIs);        
        if( exportSettingsAPI == null ) {
            return Response.noContent().build();
        } else {
            return Response.ok(exportSettingsAPI).build();
        }
    }
    
    /**
     * Delete a list of general pricelists from the export settings
     * 
     * @param organizationUniqueId
     * @param exportSettingsUniqueId
     * @param generalPricelistAPIs user supplied list of general pricelists
     * @return the updated <code>ExportSettingsAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @DELETE
    @Path("{exportSettingsUniqueId}/generalpricelists")
    @SecuredService(permissions = {"exportsettings:update_own"})
    public Response deleteGeneralPricelistsToExportSetting(
            @PathParam("organizationUniqueId") long organizationUniqueId, 
            @PathParam("exportSettingsUniqueId") long exportSettingsUniqueId, 
            List<GeneralPricelistAPI> generalPricelistAPIs) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "deleteGeneralPricelistsToExportSetting( organizationUniqueId: {0}, exportSettingsUniqueId: {1} )", new Object[] {organizationUniqueId, exportSettingsUniqueId});
        ExportSettingsAPI exportSettingsAPI = exportSettingsController.deleteGeneralPricelistsToExportSetting(organizationUniqueId, exportSettingsUniqueId, generalPricelistAPIs);        
        if( exportSettingsAPI == null ) {
            return Response.noContent().build();
        } else {
            return Response.ok(exportSettingsAPI).build();
        }
    }
    
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.product.controller;

import java.util.ArrayList;
import java.util.List;
import se.inera.hjalpmedelstjansten.model.api.cv.CVGuaranteeUnitAPI;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;

/**
 *
 * @author Tommy Berglund
 */
public class GuaranteeUnitMapper {
    
    public static final List<CVGuaranteeUnitAPI> map(List<CVGuaranteeUnit> guaranteeUnits) {
        if( guaranteeUnits == null ) {
            return null;
        }
        List<CVGuaranteeUnitAPI> guaranteeUnitAPIs = new ArrayList<>();
        for( CVGuaranteeUnit guaranteeUnit : guaranteeUnits ) {
            guaranteeUnitAPIs.add(map(guaranteeUnit));
        }
        return guaranteeUnitAPIs;
    }
    
    public static final CVGuaranteeUnitAPI map(CVGuaranteeUnit guaranteeUnit) {
        if( guaranteeUnit == null ) {
            return null;
        }
        CVGuaranteeUnitAPI guaranteeUnitAPI = new CVGuaranteeUnitAPI();
        guaranteeUnitAPI.setId(guaranteeUnit.getUniqueId());
        guaranteeUnitAPI.setCode(guaranteeUnit.getCode());
        guaranteeUnitAPI.setName(guaranteeUnit.getName());
        return guaranteeUnitAPI;
    }
    
}

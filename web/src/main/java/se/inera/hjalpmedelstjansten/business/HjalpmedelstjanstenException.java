/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business;

import java.util.LinkedHashSet;
import java.util.Set;
import javax.ejb.ApplicationException;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;

/**
 * Custom service exception which causes a rollback. 
 * 
 * @see se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenExceptionMapper
 * @author Tommy Berglund
 */
@ApplicationException(rollback = true)
public class HjalpmedelstjanstenException extends Exception {
    
    public enum Type {
        FORBIDDEN
    }
    
    private final Set<ErrorMessageAPI> validationMessages = new LinkedHashSet<>();
    private Type type = null;
    
    public HjalpmedelstjanstenException(String message) {
        super(message);
    }
    
    public HjalpmedelstjanstenException(String message, Throwable cause) {
        super(message, cause);
    }
    
    public HjalpmedelstjanstenException(String message, Type type) {
        super(message);
        this.type = type;
    }
    
    public Set<ErrorMessageAPI> getValidationMessages() {
        return validationMessages;
    }
    
    public Type getType() {
        return type;
    }
    
    public void addValidationMessage(String field, String message) {
        ErrorMessageAPI errorMessageAPI = new ErrorMessageAPI();
        errorMessageAPI.setField(field);
        errorMessageAPI.setMessage(message);
        validationMessages.add(errorMessageAPI);
    }
    
    public void addValidationMessage(String message) {
        addValidationMessage(null, message);
    }
    
}

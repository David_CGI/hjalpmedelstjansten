/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.user.controller;

import java.util.Set;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.entity.UserEngagement;

/**
 *
 * @author Tommy Berglund
 */
@Stateless
public class UserValidation {
    
    @Inject
    private HjmtLogger LOG;
    
    @Inject
    private ValidationMessageService validationMessageService;
    
    @Inject
    private UserController userController;
    
    public void validate( UserAPI userAPI, boolean update, UserEngagement currentUser ) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validate(...)" );
        HjalpmedelstjanstenValidationException exception = null;
        // email is mandatory when it comes to users, but not organization, that's why the 
        // validation is here and not in the API-class
        if( userAPI.getElectronicAddress().getEmail() == null || userAPI.getElectronicAddress().getEmail().isEmpty() ) {
            exception = new HjalpmedelstjanstenValidationException("Validation failed");
            exception.addValidationMessage("electronicAddress.email", validationMessageService.getMessage("user.email.notNull"));
        }
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<UserAPI>> constraintViolations = validator.validate(userAPI, Default.class);
        if( constraintViolations != null && !constraintViolations.isEmpty() ) {
            if( exception == null ) {
                exception = new HjalpmedelstjanstenValidationException("Validation failed");
            }
            for( ConstraintViolation constraintViolation : constraintViolations ) {
                exception.addValidationMessage(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage());
            }
        }
        if( exception != null ) {
            throw exception;
        }
        if( update ) {
            validateForUpdate(userAPI, currentUser);
        } else {
            validateForCreate(userAPI);
        }
    }
        
    
    private void validateForCreate(UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForCreate(...)" );
        HjalpmedelstjanstenValidationException exception = null;
        // make sure username and email is unique
        UserEngagement userEngagement = userController.findByEmail(userAPI.getElectronicAddress().getEmail());
        if( userEngagement != null ) {
            exception = new HjalpmedelstjanstenValidationException("Validation failed");
            exception.addValidationMessage("electronicAddress.email", validationMessageService.getMessage("user.email.notUnique"));
        }
        userEngagement = userController.findByUsername(userAPI.getUsername());
        if( userEngagement != null) {
            if( exception == null ) {
                exception = new HjalpmedelstjanstenValidationException("Validation failed");
            }
            exception.addValidationMessage("username", validationMessageService.getMessage("user.username.notUnique"));
        }
        
        if( exception != null ) {
            throw exception;
        }
    }
    
    private void validateForUpdate(UserAPI userAPI, UserEngagement currentUser) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForUpdate(...)" );
        UserEngagement userEngagement = userController.findByEmail(userAPI.getElectronicAddress().getEmail());
        if( userEngagement != null ) {
            if( !userEngagement.getUserAccount().getUniqueId().equals(currentUser.getUserAccount().getUniqueId()) ) {
                // gln changed to another organizations
                HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
                exception.addValidationMessage("electronicAddress.email", validationMessageService.getMessage("user.email.notUnique"));
                throw exception;
            }
        }
        userEngagement = userController.findByUsername(userAPI.getElectronicAddress().getEmail());
        if( userEngagement != null ) {
            if( !userEngagement.getUserAccount().getUniqueId().equals(currentUser.getUserAccount().getUniqueId()) ) {
                // gln changed to another organizations
                HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
                exception.addValidationMessage("username", validationMessageService.getMessage("user.username.notUnique"));
                throw exception;
            }
        }
    }
    
}

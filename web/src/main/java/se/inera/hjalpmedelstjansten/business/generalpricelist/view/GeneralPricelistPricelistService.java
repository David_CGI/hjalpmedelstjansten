/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.generalpricelist.view;

import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistPricelistController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Product;

/**
 * REST API for pricelists on a general pricelist.
 * 
 * @author Tommy Berglund
 */
@Stateless
@Path("organizations/{organizationUniqueId}/generalpricelists/pricelists")
@Interceptors({ PerformanceLogInterceptor.class })
public class GeneralPricelistPricelistService extends BaseService {
    
    @Inject
    private HjmtLogger LOG;
    
    @Inject
    GeneralPricelistPricelistController pricelistController;
    
    @Inject
    AuthHandler authHandler;
    
    /**
     * Get all pricelists on the general pricelist. There can be only one general 
     * pricelist per supplier organization
     * 
     * @param organizationUniqueId the unique id of the organization
     * @return the corresponding List of <code>GeneralPricelistPricelistAPI</code>
     */
    @GET
    @SecuredService(permissions = {"generalpricelist_pricelist:view_own","generalpricelist_pricelist:view_all"})
    public Response getPricelistsOnGeneralPricelist(
            @PathParam("organizationUniqueId") long organizationUniqueId) {
        LOG.log(Level.FINEST, "getPricelistsOnGeneralPricelist( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authHandler.hasPermission("generalpricelist_pricelist:view_all") && !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        List<GeneralPricelistPricelistAPI> pricelistAPIs = pricelistController.getPricelistAPIsOnGeneralPricelist(organizationUniqueId, userAPI );
        if( pricelistAPIs == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(pricelistAPIs).build();
        }
    }
    
    /**
     * Get information about a specific pricelist 
     * 
     * @param httpServletRequest
     * @param organizationUniqueId the unique id of the organization
     * @param pricelistUniqueId the id of the pricelist
     * @return the corresponding <code>GeneralPricelistPricelistAPI</code>
     */
    @GET
    @Path("{pricelistUniqueId}")
    @SecuredService(permissions = {"generalpricelist_pricelist:view_own","generalpricelist_pricelist:view_all"})
    public Response getPricelist(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId ) {
        LOG.log(Level.FINEST, "getPricelist( organizationUniqueId: {0}, pricelistUniqueId: {1} )", new Object[] {organizationUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authHandler.hasPermission("generalpricelist_pricelist:view_all") && !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        GeneralPricelistPricelistAPI pricelistAPI = pricelistController.getPricelistAPI(organizationUniqueId, pricelistUniqueId, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest) );
        if( pricelistAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(pricelistAPI).build();
        }
    }
    
    /**
     * Create new pricelist 
     * 
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization
     * @param pricelistAPI user supplied values
     * @return the created <code>GeneralPricelistPricelistAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @SecuredService(permissions = {"generalpricelist_pricelist:create_own"})
    public Response createPricelist(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            GeneralPricelistPricelistAPI pricelistAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createPricelist( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        pricelistAPI = pricelistController.createPricelist(organizationUniqueId, pricelistAPI, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        return Response.ok(pricelistAPI).build();
    }
 
    /**
     * Update pricelist. Updatable fields are name and validFrom. 
     * 
     * @param organizationUniqueId unique id of the organization to update pricelist on
     * @param pricelistUniqueId unique id of the pricelist to update
     * @param pricelistAPI user supplied values
     * @return the updated <code>GeneralPricelistPricelistAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     */
    @PUT
    @Path("{pricelistUniqueId}")
    @SecuredService(permissions = {"generalpricelist_pricelist:update_own"})
    public Response updatePricelist(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId,
            GeneralPricelistPricelistAPI pricelistAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updatePricelist( organizationUniqueId: {0}, pricelistUniqueId: {1} )", new Object[] {organizationUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        pricelistAPI = pricelistController.updatePricelist(organizationUniqueId, pricelistUniqueId, pricelistAPI, userAPI, authHandler.getSessionId());
        if( pricelistAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(pricelistAPI).build();
    }
    
    /**
     * Search articles available to add on a specific pricelist 
     * 
     * @param organizationUniqueId the unique id of the organization to get agreement from
     * @param pricelistUniqueId the id of the pricelist
     * @param query
     * @param offset
     * @param statuses
     * @param articleTypes
     * @return the corresponding <code>PricelistAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException
     */
    @GET
    @Path("{pricelistUniqueId}/articles")
    @SecuredService(permissions = {"generalpricelist_pricelist:view_own"})
    public Response searchArticlesForPricelist(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId,
            @DefaultValue(value = "") @QueryParam("query") String query,
            @DefaultValue(value = "0") @QueryParam("offset") int offset,
            @QueryParam("status") List<Product.Status> statuses, 
            @QueryParam("type") List<Article.Type> articleTypes) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "searchArticlesForPricelist( organizationUniqueId: {0}, pricelistUniqueId: {1} )", new Object[] {organizationUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        SearchDTO searchDTO = pricelistController.searchArticlesForGeneralPricelistPricelist(organizationUniqueId, pricelistUniqueId, query, statuses, articleTypes, offset, 25, userAPI);
        if( searchDTO == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(searchDTO.getItems()).
                header("X-Total-Count", searchDTO.getCount()).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                build();
        }
    }
    
}

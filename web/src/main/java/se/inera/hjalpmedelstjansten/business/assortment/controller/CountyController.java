/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.assortment.controller;

import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCountyAPI;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCounty;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVMunicipality;

/**
 *
 * @author Tommy Berglund
 */
@Stateless
public class CountyController {
    
    @Inject
    private HjmtLogger LOG;
    
    @PersistenceContext( unitName = "HjmtjPU")
    private EntityManager em;
    
    /**
     * Get all CE directives and standards
     * 
     * @return a <code>CeAPI</code> containing all standards and directives
     */
    public List<CVCountyAPI> getAllCounties() {
        LOG.log(Level.FINEST, "getAllCounties()");
        return AssortmentMapper.mapCounties(findAllCounties(), true);
    }
    
    public List<CVCounty> findAllCounties() {
        LOG.log(Level.FINEST, "findAllCounties()");
        return em.createNamedQuery(CVCounty.FIND_ALL).
                getResultList();
    }
 
    public CVCounty findCountyById(long countyId) {
        return em.find(CVCounty.class, countyId);
    }
    
    public CVMunicipality findMunicipalityById(long municipalityId) {
        return em.find(CVMunicipality.class, municipalityId);
    }
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.product.view;

import java.util.List;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.product.controller.CategoryController;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.CategoryAPI;
import se.inera.hjalpmedelstjansten.model.api.CategorySpecificPropertyAPI;
import se.inera.hjalpmedelstjansten.model.entity.Article;

/**
 * REST API for product categories
 * 
 * @author Tommy Berglund
 */
@Stateless
@Path("categories")
@Interceptors({ PerformanceLogInterceptor.class })
public class CategoryService extends BaseService {
    
    @Inject
    private HjmtLogger LOG;
    
    @EJB
    private CategoryController categoryController;
        
    /**
     * Search categories by user query
     * 
     * @param query find all categories with name like this
     * @param code search categories children to this category with this code
     * @param articleType find category of this articletype without code, should only be one
     * @param excludeCodelessCategories whether to exclude categories without code
     * from search results
     * @param excludeCodeCategories
     * @return a list of <code>CategoryAPI</code> matching the query and a 
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation of parameters fail
     */
    @GET
    @SecuredService(permissions = {"category:view"})
    public Response searchCategories(@DefaultValue(value = "") @QueryParam("query") String query, 
            @DefaultValue(value = "") @QueryParam("code") String code, 
            @QueryParam("type") Article.Type articleType,
            @DefaultValue(value = "false") @QueryParam("excludeCodelessCategories") boolean excludeCodelessCategories,
            @DefaultValue(value = "false") @QueryParam("excludeCodeCategories") boolean excludeCodeCategories) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "searchCategories( code: {0}, articleType: {1} )", new Object[] {code, articleType});
        List<CategoryAPI> categoryAPIs =  categoryController.searchCategories(query, code, articleType, excludeCodelessCategories, excludeCodeCategories);
        return Response.ok(categoryAPIs).build();
    }
    
    /**
     * Get properties for a specific query
     * 
     * @param categoryUniqueId
     * @return a list of <code>CategorySpecificPropertyAPI</code> matching the category
     */
    @GET
    @Path("{categoryUniqueId}/properties")
    @SecuredService(permissions = {"category:view"})
    public Response getCategorySpecificProperties(@PathParam("categoryUniqueId") long categoryUniqueId) {
        LOG.log(Level.FINEST, "getCategorySpecificProperties( categoryUniqueId: {0} )", new Object[] {categoryUniqueId});
        List<CategorySpecificPropertyAPI> categoryPropertyAPIs =  categoryController.getCategoryPropertyAPIs(categoryUniqueId);
        return Response.ok(categoryPropertyAPIs).build();
    }
    
}

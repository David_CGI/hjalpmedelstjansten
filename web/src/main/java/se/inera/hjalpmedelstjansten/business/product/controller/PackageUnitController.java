/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.product.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.model.api.cv.CVOrderUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPackageUnitAPI;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVOrderUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPackageUnit;

/**
 *
 * @author Tommy Berglund
 */
@Stateless
public class PackageUnitController {
    
    @Inject
    private HjmtLogger LOG;
    
    @PersistenceContext( unitName = "HjmtjPU")
    private EntityManager em;
    
    public List<CVPackageUnitAPI> getAllUnits() {
        LOG.log(Level.FINEST, "getAllUnits()");
        return PackageUnitMapper.map(findAll());
    }
        
    public List<CVPackageUnit> findAll() {
        LOG.log(Level.FINEST, "findAll()");
        return em.createNamedQuery(CVPackageUnit.FIND_ALL).getResultList();
    }
    
    public CVPackageUnit getUnit(long uniqueId) {
        LOG.log(Level.FINEST, "getUnit( uniqueId: {0} )", new Object[] {uniqueId});
        return em.find(CVPackageUnit.class, uniqueId);
    }

    public CVPackageUnit findByName(String packageUnitName) {
        LOG.log(Level.FINEST, "findByName( packageUnitName: {0} )", new Object[] {packageUnitName});
        List<CVPackageUnit> packageUnits = em.createNamedQuery(CVPackageUnit.FIND_BY_NAME).
                setParameter("name", packageUnitName).
                getResultList();
        // package unit names are unique so either we get one or nothing
        if( packageUnits != null && !packageUnits.isEmpty() ) {
            return packageUnits.get(0);
        }
        return null;
    }

    public Map<Long, CVPackageUnit> findAllAsIdMap() {
        Map<Long, CVPackageUnit> packageUnitsIdMap = new HashMap<>();
        List<CVPackageUnit> allUnits = findAll();
        if( allUnits != null && !allUnits.isEmpty() ) {
            allUnits.forEach((orderUnit) -> {
                packageUnitsIdMap.put(orderUnit.getUniqueId(), orderUnit);
            });
        }
        return packageUnitsIdMap;
    }
    
    public Map<Long, CVPackageUnit> getAllAsIdMap(List<CVPackageUnit> allUnits) {
        Map<Long, CVPackageUnit> packageUnitsIdMap = new HashMap<>();
        if( allUnits != null && !allUnits.isEmpty() ) {
            allUnits.forEach((packageUnit) -> {
                packageUnitsIdMap.put(packageUnit.getUniqueId(), packageUnit);
            });
        }
        return packageUnitsIdMap;
    }
    
}

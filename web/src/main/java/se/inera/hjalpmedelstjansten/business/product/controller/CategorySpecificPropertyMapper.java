/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.product.controller;

import java.util.ArrayList;
import java.util.List;
import se.inera.hjalpmedelstjansten.model.api.CategorySpecificPropertyAPI;
import se.inera.hjalpmedelstjansten.model.api.CategorySpecificPropertyListValueAPI;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificProperty;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificPropertyListValue;

/**
 * Class for mapping between API and Entity classes
 * 
 * @author Tommy Berglund
 */
public class CategorySpecificPropertyMapper {
    
    public static final List<CategorySpecificPropertyAPI> map(List<CategorySpecificProperty> categorySpecificPropertys) {
        if( categorySpecificPropertys == null || categorySpecificPropertys.isEmpty() ) {
            return null;
        }
        List<CategorySpecificPropertyAPI> categorySpecificPropertyAPIs = new ArrayList<>();
        for( CategorySpecificProperty categorySpecificProperty : categorySpecificPropertys ) {
            categorySpecificPropertyAPIs.add(map(categorySpecificProperty));
        }
        return categorySpecificPropertyAPIs;
    }
    
    public static final CategorySpecificPropertyAPI map(CategorySpecificProperty categorySpecificProperty) {
        if( categorySpecificProperty == null ) {
            return null;
        }
        CategorySpecificPropertyAPI categorySpecificPropertyAPI = new CategorySpecificPropertyAPI();        
        categorySpecificPropertyAPI.setId(categorySpecificProperty.getUniqueId());
        categorySpecificPropertyAPI.setName(categorySpecificProperty.getName());
        categorySpecificPropertyAPI.setDescription(categorySpecificProperty.getDescription());
        categorySpecificPropertyAPI.setType(categorySpecificProperty.getType().toString());
        if( categorySpecificProperty.getType() == CategorySpecificProperty.Type.VALUELIST_MULTIPLE ||
                categorySpecificProperty.getType() == CategorySpecificProperty.Type.VALUELIST_SINGLE ) {
            List<CategorySpecificPropertyListValueAPI> categorySpecificPropertyListValueAPIs = new ArrayList<>();
            for( CategorySpecificPropertyListValue categorySpecificPropertyListValue : categorySpecificProperty.getCategorySpecificPropertyListValues() ) {
                CategorySpecificPropertyListValueAPI categorySpecificPropertyListValueAPI = new CategorySpecificPropertyListValueAPI();
                categorySpecificPropertyListValueAPI.setId(categorySpecificPropertyListValue.getUniqueId());
                categorySpecificPropertyListValueAPI.setValue(categorySpecificPropertyListValue.getValue());
                categorySpecificPropertyListValueAPIs.add(categorySpecificPropertyListValueAPI);
            }
            if( categorySpecificProperty.getType() == CategorySpecificProperty.Type.VALUELIST_SINGLE ) {
                // must add an "empty" value so a selected value can be removed
                CategorySpecificPropertyListValueAPI categorySpecificPropertyListValueAPI = new CategorySpecificPropertyListValueAPI();
                categorySpecificPropertyListValueAPI.setValue("");
                categorySpecificPropertyListValueAPIs.add(categorySpecificPropertyListValueAPI);
            }
            categorySpecificPropertyAPI.setValues(categorySpecificPropertyListValueAPIs);
        }
        return categorySpecificPropertyAPI;
    }
    
}

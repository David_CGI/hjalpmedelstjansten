/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.media.controller;

import java.util.ArrayList;
import java.util.List;
import se.inera.hjalpmedelstjansten.model.api.media.MediaDocumentAPI;
import se.inera.hjalpmedelstjansten.model.api.media.MediaImageAPI;
import se.inera.hjalpmedelstjansten.model.api.media.MediaListAPI;
import se.inera.hjalpmedelstjansten.model.api.media.MediaVideoAPI;
import se.inera.hjalpmedelstjansten.model.entity.media.ArticleMediaDocument;
import se.inera.hjalpmedelstjansten.model.entity.media.ArticleMediaImage;
import se.inera.hjalpmedelstjansten.model.entity.media.ArticleMediaVideo;
import se.inera.hjalpmedelstjansten.model.entity.media.MediaDocument;
import se.inera.hjalpmedelstjansten.model.entity.media.MediaImage;
import se.inera.hjalpmedelstjansten.model.entity.media.MediaVideo;

/**
 * Class for mapping between API and Entity classes
 * 
 * @author Tommy Berglund
 */
public class MediaMapper {
    
    public static final List<MediaDocumentAPI> mapDocuments(List<MediaDocument> mediaDocuments) {
        if( mediaDocuments == null ) {
            return null;
        }
        List<MediaDocumentAPI> mediaDocumentAPIs = new ArrayList<>();
        mediaDocuments.forEach((mediaDocument) -> {
            mediaDocumentAPIs.add(mapDocument(mediaDocument));
        });
        return mediaDocumentAPIs;
    }
    
    public static final List<MediaDocumentAPI> mapArticleDocuments(List<ArticleMediaDocument> articleMediaDocuments) {
        if( articleMediaDocuments == null ) {
            return null;
        }
        List<MediaDocumentAPI> mediaDocumentAPIs = new ArrayList<>();
        articleMediaDocuments.forEach((articleMediaDocument) -> {            
            mediaDocumentAPIs.add(mapArticleDocument(articleMediaDocument));
        });
        return mediaDocumentAPIs;
    }
    
    public static final MediaDocumentAPI mapArticleDocument(ArticleMediaDocument articleMediaDocument) {
        if( articleMediaDocument == null ) {
            return null;
        }
        MediaDocumentAPI mediaDocumentAPI = mapDocument(articleMediaDocument.getMediaDocument());
        // we want to use the id of the entity pointing to the media
        mediaDocumentAPI.setId(articleMediaDocument.getUniqueId());
        return mediaDocumentAPI;
    }
    
    public static final List<MediaVideoAPI> mapVideos(List<MediaVideo> mediaVideos) {
        if( mediaVideos == null ) {
            return null;
        }
        List<MediaVideoAPI> mediaVideoAPIs = new ArrayList<>();
        mediaVideos.forEach((mediaVideo) -> {
            mediaVideoAPIs.add(mapVideo(mediaVideo));
        });
        return mediaVideoAPIs;
    }
    
    public static final List<MediaVideoAPI> mapArticleVideos(List<ArticleMediaVideo> articleMediaVideos) {
        if( articleMediaVideos == null ) {
            return null;
        }
        List<MediaVideoAPI> mediaVideoAPIs = new ArrayList<>();
        articleMediaVideos.forEach((articleMediaVideo) -> {            
            mediaVideoAPIs.add(mapArticleVideo(articleMediaVideo));
        });
        return mediaVideoAPIs;
    }
    
    public static final MediaVideoAPI mapArticleVideo(ArticleMediaVideo articleMediaVideo) {
        if( articleMediaVideo == null ) {
            return null;
        }
        MediaVideoAPI mediaVideoAPI = mapVideo(articleMediaVideo.getMediaVideo());
        // we want to use the id of the entity pointing to the media
        mediaVideoAPI.setId(articleMediaVideo.getUniqueId());
        return mediaVideoAPI;
    }
    
    public static final List<MediaImageAPI> mapImages(List<MediaImage> mediaImages) {
        if( mediaImages == null ) {
            return null;
        }
        List<MediaImageAPI> mediaImageAPIs = new ArrayList<>();
        mediaImages.forEach((mediaImage) -> {
            mediaImageAPIs.add(mapImage(mediaImage));
        });
        return mediaImageAPIs;
    }
    
    public static final List<MediaImageAPI> mapArticleImages(List<ArticleMediaImage> articleMediaImages) {
        if( articleMediaImages == null ) {
            return null;
        }
        List<MediaImageAPI> mediaImageAPIs = new ArrayList<>();
        articleMediaImages.forEach((articleMediaImage) -> {
            mediaImageAPIs.add(mapArticleImage(articleMediaImage));
        });
        return mediaImageAPIs;
    }
    
    public static final MediaImageAPI mapArticleImage(ArticleMediaImage articleMediaImage) {
        if( articleMediaImage == null ) {
            return null;
        }
        MediaImageAPI mediaImageAPI = mapImage(articleMediaImage.getMediaImage());
        // we want to use the id of the entity pointing to the media
        mediaImageAPI.setId(articleMediaImage.getUniqueId());
        return mediaImageAPI;
    }
    
    public static final MediaDocumentAPI mapDocument(MediaDocument mediaDocument) {
        if( mediaDocument == null ) {
            return null;
        }
        MediaDocumentAPI mediaDocumentAPI = new MediaDocumentAPI();
        mediaDocumentAPI.setId(mediaDocument.getUniqueId());
        mediaDocumentAPI.setDescription(mediaDocument.getDescription());
        if( mediaDocument.getDocumentType() != null ) {
            mediaDocumentAPI.setDocumentType(DocumentTypeMapper.map(mediaDocument.getDocumentType()));
        }
        mediaDocumentAPI.setUrl(mediaDocument.getUrl());
        mediaDocumentAPI.setFileType(mediaDocument.getFileType().name());
        mediaDocumentAPI.setFileName(mediaDocument.getFileName());
        return mediaDocumentAPI;
    }

    public static final MediaVideoAPI mapVideo(MediaVideo mediaVideo) {
        if( mediaVideo == null ) {
            return null;
        }
        MediaVideoAPI mediaVideoAPI = new MediaVideoAPI();
        mediaVideoAPI.setId(mediaVideo.getUniqueId());
        mediaVideoAPI.setUrl(mediaVideo.getUrl());
        mediaVideoAPI.setDescription(mediaVideo.getDescription());
        mediaVideoAPI.setAlternativeText(mediaVideo.getAlternativeText());
        return mediaVideoAPI;
    }
    
    public static final MediaImageAPI mapImage(MediaImage mediaImage) {
        if( mediaImage == null ) {
            return null;
        }
        MediaImageAPI mediaImageAPI = new MediaImageAPI();
        mediaImageAPI.setId(mediaImage.getUniqueId());
        mediaImageAPI.setUrl(mediaImage.getUrl());
        mediaImageAPI.setDescription(mediaImage.getDescription());
        mediaImageAPI.setAlternativeText(mediaImage.getAlternativeText());
        mediaImageAPI.setMainImage(mediaImage.isMainImage());
        mediaImageAPI.setFileName(mediaImage.getFileName());
        return mediaImageAPI;
    }
    
    public static MediaListAPI mapProductMedias(List<MediaDocument> mediaDocuments, List<MediaVideo> mediaVideos, List<MediaImage> mediaImages) {
        MediaListAPI mediaListAPI = new MediaListAPI();
        mediaListAPI.setDocuments(mapDocuments(mediaDocuments));
        mediaListAPI.setVideos(mapVideos(mediaVideos));
        mediaListAPI.setImages(mapImages(mediaImages));
        return mediaListAPI;
    }
    
    public static MediaListAPI mapArticleMedias(List<ArticleMediaDocument> articleMediaDocuments, List<ArticleMediaVideo> articleMediaVideos, List<ArticleMediaImage> articleMediaImages) {
        MediaListAPI mediaListAPI = new MediaListAPI();
        mediaListAPI.setDocuments(mapArticleDocuments(articleMediaDocuments));
        mediaListAPI.setVideos(mapArticleVideos(articleMediaVideos));
        mediaListAPI.setImages(mapArticleImages(articleMediaImages));
        return mediaListAPI;
    }
    
}
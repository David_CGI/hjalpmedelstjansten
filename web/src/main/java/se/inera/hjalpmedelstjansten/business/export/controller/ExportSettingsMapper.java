/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.export.controller;

import java.util.ArrayList;
import java.util.List;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistMapper;
import se.inera.hjalpmedelstjansten.business.organization.controller.BusinessLevelMapper;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationMapper;
import se.inera.hjalpmedelstjansten.model.api.ExportSettingsAPI;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistAPI;
import se.inera.hjalpmedelstjansten.model.entity.ExportSettings;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelist;

/**
 * Class for mapping between API and Entity classes
 * 
 * @author Tommy Berglund
 */
public class ExportSettingsMapper {
    
    public static final List<ExportSettingsAPI> map(List<ExportSettings> exportSettingses) {
        if( exportSettingses == null ) {
            return null;
        }
        List<ExportSettingsAPI> exportSettingsAPIs = new ArrayList<>();
        for( ExportSettings exportSettings : exportSettingses ) {
            exportSettingsAPIs.add(map(exportSettings, false));
        }
        return exportSettingsAPIs;
    }
    
    public static final ExportSettingsAPI map(ExportSettings exportSettings, boolean includeEverything) {
        if( exportSettings == null ) {
            return null;
        }
        ExportSettingsAPI exportSettingsAPI = new ExportSettingsAPI();
        exportSettingsAPI.setId(exportSettings.getUniqueId());
        exportSettingsAPI.setFilename(exportSettings.getFilename());
        exportSettingsAPI.setNumberOfExports(exportSettings.getNumberOfExports());
        exportSettingsAPI.setLastExported(exportSettings.getLastExported() == null ? null: exportSettings.getLastUpdated().getTime());
        exportSettingsAPI.setOrganization(OrganizationMapper.map(exportSettings.getOrganization(), false));
        exportSettingsAPI.setBusinessLevel(BusinessLevelMapper.map(exportSettings.getBusinessLevel(), false));
        exportSettingsAPI.setEnabled(exportSettings.isEnabled());
        if( includeEverything ) {
            if( exportSettings.getGeneralPricelists() != null && !exportSettings.getGeneralPricelists().isEmpty() ) {
                List<GeneralPricelistAPI> generalPricelistAPIs = new ArrayList<>();
                for( GeneralPricelist generalPricelist : exportSettings.getGeneralPricelists() ) {
                    generalPricelistAPIs.add(GeneralPricelistMapper.map(generalPricelist, false));
                }
                exportSettingsAPI.setGeneralPricelists(generalPricelistAPIs);
            }
        }
        return exportSettingsAPI;
    }

    public static final ExportSettings map(ExportSettingsAPI exportSettingsAPI) {
        if( exportSettingsAPI == null ) {
            return null;
        }
        ExportSettings exportSettings = new ExportSettings();
        exportSettings.setFilename(exportSettingsAPI.getFilename());
        exportSettings.setNumberOfExports(0);
        return exportSettings;
    }
        
}

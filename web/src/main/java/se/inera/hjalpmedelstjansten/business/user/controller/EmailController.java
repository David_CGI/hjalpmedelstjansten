/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.user.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;

/**
 *
 * @author Tommy Berglund
 */
@Stateless
public class EmailController {


    private boolean mailSendingEnabled;

    private String smptHost;

    private String smptPort;

    @Inject
    private String mailAccountCreationSubject;
    
    @Inject
    private String mailResetPasswordSubject;
    

    private String mailFromAddress;
    
    @Inject
    private String mailAccountCreationBody;
    
    @Inject
    private String mailResetPasswordBody;
    
    @Inject
    private String mailEncoding;
    
    @Inject
    private String mailMimeSubtype;
    
    @Inject
    private String mailBodyPostFix;
    
    @Inject
    private String mailSubjectPreFix;
    
    @Inject
    private HjmtLogger LOG;
    
    private String allEmailsPostFix;
    
    @Inject
    private String createPasswordPath;
    
    public void sendAccountCreationEmail(String toAddress, String emailLinkToken, String baseUrl, String username) throws UnsupportedEncodingException, MessagingException {
        LOG.log( Level.FINEST, "sendAccountCreationEmail(...)" );
        String emailVerificationLink = baseUrl + createPasswordPath + "?email=" + URLEncoder.encode(toAddress, "UTF-8") + "&token=" + URLEncoder.encode(emailLinkToken, "UTF-8" );
        String message = String.format(mailAccountCreationBody, username, emailVerificationLink);
        send(toAddress, mailAccountCreationSubject, message);
    }
 
    public void sendResetPasswordEmail(String toAddress, String emailLinkToken, String baseUrl, String username) throws UnsupportedEncodingException, MessagingException {
        LOG.log( Level.FINEST, "sendResetPasswordEmail(...)" );
        String emailVerificationLink = baseUrl + createPasswordPath + "?email=" + URLEncoder.encode(toAddress, "UTF-8") + "&token=" + URLEncoder.encode(emailLinkToken, "UTF-8" );
        String message = String.format(mailResetPasswordBody, username, emailVerificationLink);
        send(toAddress, mailResetPasswordSubject, message);
    }
    
    public void send(String toAddress, String subject, String message) throws MessagingException {
        LOG.log( Level.FINEST, "send( ... )" );
        if( toAddress == null || toAddress.isEmpty() ) {
            LOG.log( Level.INFO, "No adress to send mail to" );
            return;
        }
        if( !mailSendingEnabled ) {
            LOG.log( Level.INFO, "Email sending is disabled" );
            return;
        }
        message += allEmailsPostFix;
        subject = mailSubjectPreFix + " " + subject;
        if( smptHost != null && !smptHost.isEmpty() && 
            smptPort != null && !smptPort.isEmpty() &&
            mailFromAddress != null && !mailFromAddress.isEmpty() &&
            mailEncoding != null && !mailEncoding.isEmpty() &&
            mailMimeSubtype != null && !mailMimeSubtype.isEmpty() ) {
            Properties properties = new Properties();
            properties.put("mail.smtp.host", smptHost);
            properties.put("mail.smtp.port", smptPort);
            Session session = Session.getDefaultInstance(properties);

            MimeMessage mimeMessage = new MimeMessage(session);
            mimeMessage.setFrom(new InternetAddress(mailFromAddress));
            mimeMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(toAddress));
            mimeMessage.setSubject(subject, mailEncoding);
            mimeMessage.setText(message, mailEncoding, mailMimeSubtype);
            mimeMessage.setSentDate( new Date() );

            Transport.send(mimeMessage);
        } else {
            LOG.log( Level.WARNING, "Missing configuration for email notifications." );
        }

    }
    
    @PostConstruct
    private void initialize() {
        smptHost = System.getenv("SMTP_HOST");
        smptPort = System.getenv("SMTP_PORT");
        mailFromAddress = System.getenv("SMTP_MAIL_FROM_ADDRESS");
        mailSendingEnabled = System.getenv("MAIL_SENDING").equalsIgnoreCase("enabled");
        String serviceBaseUrl = System.getenv("HJMTJ_SERVICE_BASE_URL");
        allEmailsPostFix = String.format(mailBodyPostFix, serviceBaseUrl, serviceBaseUrl);
    }
    
}

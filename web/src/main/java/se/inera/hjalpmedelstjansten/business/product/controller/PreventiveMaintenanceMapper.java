/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.product.controller;

import java.util.ArrayList;
import java.util.List;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPreventiveMaintenanceAPI;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;

/**
 *
 * @author Tommy Berglund
 */
public class PreventiveMaintenanceMapper {
    
    public static final List<CVPreventiveMaintenanceAPI> map(List<CVPreventiveMaintenance> preventiveMaintenances) {
        if( preventiveMaintenances == null ) {
            return null;
        }
        List<CVPreventiveMaintenanceAPI> preventiveMaintenanceAPIs = new ArrayList<>();
        for( CVPreventiveMaintenance preventiveMaintenance : preventiveMaintenances ) {
            preventiveMaintenanceAPIs.add(map(preventiveMaintenance));
        }
        return preventiveMaintenanceAPIs;
    }
    
    public static final CVPreventiveMaintenanceAPI map(CVPreventiveMaintenance preventiveMaintenance) {
        if( preventiveMaintenance == null ) {
            return null;
        }
        CVPreventiveMaintenanceAPI preventiveMaintenanceAPI = new CVPreventiveMaintenanceAPI();
        preventiveMaintenanceAPI.setId(preventiveMaintenance.getUniqueId());
        preventiveMaintenanceAPI.setCode(preventiveMaintenance.getCode());
        preventiveMaintenanceAPI.setName(preventiveMaintenance.getName());
        return preventiveMaintenanceAPI;
    }
    
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.product.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.user.controller.EmailController;
import se.inera.hjalpmedelstjansten.model.api.*;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Product;

/**
 * Class for validation functionality for products
 * 
 * @author Tommy Berglund
 */
@Stateless
public class ProductValidation {
    
    @Inject
    HjmtLogger LOG;
    
    @Inject
    ValidationMessageService validationMessageService;
  
    @Inject
    ProductController productController;
    
    @Inject
    ArticleController articleController;

    @Inject
    EmailController emailController;
    
    public void validateForCreate(ProductAPI productAPI, long organizationUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForCreate(...)" );
        Set<ErrorMessageAPI> errorMessageAPIs = tryForCreate(organizationUniqueId, productAPI);
        
        if( !errorMessageAPIs.isEmpty() ) {
            HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
            exception.addValidationMessages(errorMessageAPIs);
            throw exception;
        }
    }
    
    public Set<ErrorMessageAPI> tryForCreate(long organizationUniqueId, ProductAPI productAPI) {
        Set<ErrorMessageAPI> errorMessageAPIs = new HashSet<>();
        validateProductAPI(productAPI, errorMessageAPIs);
        if( errorMessageAPIs.size() > 0 ) {
            // break early if indata is in itself incorrect
            return errorMessageAPIs;
        }
        
        // product number must be unique within organization
        validateProductNumber(productAPI, organizationUniqueId, null, errorMessageAPIs);
        
        // only web can be set in the electronic address of the manufacturer
        validateOnlyManufacturerWebSet(productAPI, errorMessageAPIs);
        
        // first status cannot be DISCONTINUED
        /*Product.Status newStatus = Product.Status.valueOf(productAPI.getStatus());
        if( newStatus == Product.Status.DISCONTINUED ) {
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("status", validationMessageService.getMessage("product.statusChange.invalid")));
        }*/
        
        return errorMessageAPIs;
    }
    
    public void validateForUpdate(ProductAPI productAPI, Product product, long organizationUniqueId,UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForUpdate(...)" );
        Set<ErrorMessageAPI> errorMessageAPIs = tryForUpdate(organizationUniqueId, productAPI, product,userAPI);
        
        if( !errorMessageAPIs.isEmpty() ) {
            HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
            exception.addValidationMessages(errorMessageAPIs);
            throw exception;
        }
    }

    public Set<ErrorMessageAPI> tryForUpdate(long organizationUniqueId, ProductAPI productAPI, Product product,UserAPI userAPI) {
        Set<ErrorMessageAPI> errorMessageAPIs = new HashSet<>();
        validateProductAPI(productAPI, errorMessageAPIs);
        validateProductStatusForUpdate(product,productAPI,errorMessageAPIs,userAPI);
        if( errorMessageAPIs.size() > 0 ) {
            // break early if indata is in itself incorrect
            return errorMessageAPIs;
        }
        
        // product number must be unique within organization
        validateProductNumber(productAPI, organizationUniqueId, product, errorMessageAPIs);
        
        // only web can be set in the electronic address of the manufacturer
        validateOnlyManufacturerWebSet(productAPI, errorMessageAPIs);
        
        return errorMessageAPIs;
    }

    private void validateProductStatusForUpdate(Product product, ProductAPI productAPI, Set<ErrorMessageAPI> errorMessageAPIs, UserAPI userAPI) {
        if(product.getStatus().toString().equalsIgnoreCase(Product.Status.DISCONTINUED.toString()) && productAPI.getStatus().equalsIgnoreCase(Product.Status.PUBLISHED.toString())){
            //ska inte gå att redigera en utgången produkt - HJAL2035
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("replacementDate", validationMessageService.getMessage("import.productarticle.product.expiredDate")));
        }
        if(product.getStatus().toString().equalsIgnoreCase(Product.Status.DISCONTINUED.toString()) && productAPI.getStatus().equalsIgnoreCase(Product.Status.DISCONTINUED.toString())){
            if(product.getReplacementDate() != null){
                //Återaktiverad produkt, skicka mail - HJAL1419
                try{
                    String mailSubject = String.format("Ändrad status för produkt %s",productAPI.getProductName()) ;
                    String mailBody = String.format("Produkt %s är inte längre inaktiv.", productAPI.getProductName());
                    List<String> customerMailList = productController.getCustomerEmailByProduct(productAPI,userAPI);
                    for(String email : customerMailList){
                        emailController.send(email, mailSubject,mailBody);
                    }
                }
                catch(Exception ex){
                    LOG.log(Level.SEVERE, "Failed to send email on update product", ex);
                }
            }
        }
        else if(productAPI.getReplacementDate() != null || product.getReplacementDate() != null){
            try{
                List<String> customerMailList = productController.getCustomerEmailByProduct(productAPI,userAPI);
                Date date = new Date(productAPI.getReplacementDate());
                if(product.getReplacementDate() == null){
                    //Nytt utgångsdatum
                    String mailSubject = String.format("Nytt utgångsdatum för produkt %s",productAPI.getProductName()) ;
                    String mailBody = String.format("Produkt %s har fått ett nytt utgångsdatum: %s", productAPI.getProductName(),new SimpleDateFormat("yyyy-MM-dd").format(date));
                    for(String email : customerMailList){
                        emailController.send(email, mailSubject,mailBody);
                    }
                }
                else if(productAPI.getReplacementDate() != product.getReplacementDate().getTime()) {
                    //Uppdaterat utgångsdatum
                    String mailSubject = String.format("Uppdaterat utgångsdatum för produkt %s",productAPI.getProductName()) ;
                    String mailBody = String.format("Produkt %s har fått ett uppdaterat utgångsdatum: %s", productAPI.getProductName(),new SimpleDateFormat("yyyy-MM-dd").format(date));
                    for(String email : customerMailList){
                        emailController.send(email, mailSubject,mailBody);
                    }
                }
            }
            catch(Exception ex){
                LOG.log(Level.SEVERE, "Failed to send email on update product", ex);
            }
        }
    }

    private void validateOnlyManufacturerWebSet(ProductAPI productAPI,  Set<ErrorMessageAPI> errorMessageAPIs) {
        // only we can be set in the electronic address of the manufacturer
        if( productAPI.getManufacturerElectronicAddress() != null ) {
            ElectronicAddressAPI electronicAddressAPI = productAPI.getManufacturerElectronicAddress();
            if( !isEmpty(electronicAddressAPI.getEmail()) ||
                    !isEmpty(electronicAddressAPI.getFax()) ||
                    !isEmpty(electronicAddressAPI.getMobile()) ||
                    !isEmpty(electronicAddressAPI.getTelephone()) ) {
                errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("web", validationMessageService.getMessage("product.manufacturer.onlyWeb")));
            }
        }
    }
    
    void validateProductNumber(ProductAPI productAPI, 
            long organizationUniqueId, 
            Product product,
            Set<ErrorMessageAPI> errorMessageAPIs) {
        Product productWithNumber = productController.findByProductNumberAndOrganization(productAPI.getProductNumber(), organizationUniqueId);
        if( productWithNumber != null ) {
            if( product == null || !productWithNumber.getUniqueId().equals(product.getUniqueId()) ) {
                errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("productNumber", validationMessageService.getMessage("product.number.notOrganizationUnique")));
            }
        }
        Article articleWithNumber = articleController.findByArticleNumberAndOrganization(productAPI.getProductNumber(), organizationUniqueId);
        if( articleWithNumber != null ) {
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("productNumber", validationMessageService.getMessage("product.article.number.notOrganizationUnique")));
        }
    }
    
    private boolean isEmpty(String value) {
        LOG.log( Level.FINEST, "value: {0}", new Object[] {value});
        if( value == null || value.isEmpty() ) {
            return true;
        }
        return false;
    }
    
    private void validateProductAPI(ProductAPI productAPI, Set<ErrorMessageAPI> errorMessageAPIs) {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<ProductAPI>> constraintViolations = validator.validate(productAPI, Default.class);
        if( constraintViolations != null && !constraintViolations.isEmpty() ) {
            for( ConstraintViolation constraintViolation : constraintViolations ) {
                errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage()));
            }
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.inera.hjalpmedelstjansten.business.ping.view;

import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * This ping service is used to check if the service is up. In the first version
 * it will only return response code 200 if possible. 
 * 
 * @author Tommy Berglund
 */
@Stateless
@Path("ping")
public class PingService {
    
    @GET
    public Response ping() {
        return Response.ok().build();
    }
    
}

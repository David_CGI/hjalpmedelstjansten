/*
* Copyright (C) 2019 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.user.view;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenException;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.ResultExporterController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;

/**
 * REST API for User functionality.
 * 
 * @author Tommy Berglund
 */
@Stateless
@Path("organizations/{organizationUniqueId}/users")
@Interceptors({ PerformanceLogInterceptor.class })
public class UserService extends BaseService {
    
    @Inject
    private HjmtLogger LOG;
    
    @Inject
    private UserController userController;

    @Inject
    ResultExporterController resultExporterController;
    
    @Inject
    private AuthHandler authHandler;

    /**
     * Search users on a specific organization by query 
     * 
     * @param organizationUniqueId the unique id of the organization to search users on
     * @param query the user query
     * @param offset offset for pagination purpose
     * @param roleNames limit search to only users with the given role names
     * @return a list of <code>UserAPI</code> matching the query and a header 
     * X-Total-Count giving the total number of users for pagination
     */
    @GET
    @SecuredService(permissions = {"user:view"})
    public Response searchUsers(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @DefaultValue(value = "") @QueryParam("query") String query,
            @DefaultValue(value = "0") @QueryParam("offset") int offset,
            @QueryParam("roleName") List<UserRole.RoleName> roleNames) {
        LOG.log(Level.FINEST, "searchUsers( organizationUniqueId: {0}, offset: {1} )", new Object[] {organizationUniqueId, offset});
        long countSearch = userController.countSearchUsers(query, organizationUniqueId, roleNames);
        List<UserAPI> userAPIs = userController.searchUsers(false, query, organizationUniqueId, offset, 25, roleNames);
        return Response.ok(userAPIs).
                header("X-Total-Count", countSearch).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                build();
    }

    /**
     * Export users
     *
     * @param httpServletRequest
     * @param query
     * @return an excel containing the list of search results
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @Path("/{onlyActives}/export")
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=UTF-8")
    @SecuredService(permissions = {"user:view"})
    @TransactionTimeout(value=20, unit = TimeUnit.MINUTES)
    public Response exportUsers(
            @Context HttpServletRequest httpServletRequest,
            @PathParam("onlyActives") boolean onlyActives,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @DefaultValue(value = "") @QueryParam("query") String query) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "exportUsers...");
        boolean isSuperAdmin = authHandler.hasRole(UserRole.RoleName.Superadmin.toString());
        if (!isSuperAdmin ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        List<UserRole.RoleName> roleNames = new ArrayList<>(Arrays.asList(UserRole.RoleName.values()));
        int offset = 0;
        int maximumNumberOfResults = 55000;

        /*
        SearchDTO searchDTO = userController.searchUsers(query,
                organizationUniqueId,
                offset,
                maximumNumberOfResults,
                roleNames);

         */

        List<UserAPI> userAPIs = userController.searchUsers(onlyActives, query, organizationUniqueId, offset, maximumNumberOfResults, roleNames);


        if (userAPIs == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            try {
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'_'HHmm");
                String filename = "Export_Användare_" + dateTimeFormatter.format(ZonedDateTime.now()) + ".xlsx";
                //List<UserAPI> searchUsersAPIs = (List<UserAPI>) searchDTO.getItems();
                //byte[] exportBytes = resultExporterController.generateUsersList(searchUsersAPIs);
                byte[] exportBytes = resultExporterController.generateUsersResultList(userAPIs);

                return Response.ok(
                        exportBytes,
                        javax.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM)
                        .header("content-disposition", "attachment; filename = " + filename)
                        .build();
            } catch (IOException ex) {
                LOG.log(Level.WARNING, "Failed to export users to file", ex);
                return Response.serverError().build();
            }
        }
    }

    /**
     * Get all users on a specific organization in specic roles. This API is not
     * paginated like the search API.
     * 
     * @param organizationUniqueId the unique id of the organization to search users on
     * @param roleNames
     * @return a list of <code>UserAPI</code> matching the query
     */
    @GET
    @Path("/roles")
    @SecuredService(permissions = {"user:view"})
    public Response getUsersInRoles(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @QueryParam("roleName") List<UserRole.RoleName> roleNames) {
        LOG.log(Level.FINEST, "getUsersInRoles( organizationUniqueId: {0}, roleNames: {1} )", new Object[] {organizationUniqueId, roleNames});
        List<UserAPI> userAPIs = null;
        if( roleNames == null || roleNames.isEmpty() ) {
            LOG.log( Level.WARNING, "No roleNames supplied in query parameter which is invalid use of API." );
        } else {
            userAPIs = userController.getUsersInRolesOnOrganization(organizationUniqueId, roleNames);
        }
        return Response.ok(userAPIs).build();
    }
    
    /**
     * Get users on a specific organization in specific business levels. This API is not
     * paginated like the search API.
     * 
     * @param organizationUniqueId the unique id of the organization to search users on
     * @param query
     * @param businessLevelIds ids of the businesslevels to look for
     * @param offset
     * @return a list of <code>UserAPI</code> matching the query
     */
    @GET
    @Path("/businesslevels")
    @SecuredService(permissions = {"user:view"})
    public Response getUsersInBusinessLevels(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @DefaultValue(value = "") @QueryParam("query") String query,
            @QueryParam("businessLevel") List<Long> businessLevelIds,
            @DefaultValue(value = "0") @QueryParam("offset") int offset) {
        LOG.log(Level.FINEST, "getUsersInBusinessLevels( organizationUniqueId: {0}, businessLevelIds: {1} )", new Object[] {organizationUniqueId, businessLevelIds});
        long countSearch = userController.countSearchUsersInBusinessLevels(query, organizationUniqueId, businessLevelIds);
        List<UserAPI> userAPIs = userController.searchUsersInBusinessLevels(query, organizationUniqueId, businessLevelIds, offset, 25);
        return Response.ok(userAPIs).
                header("X-Total-Count", countSearch).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                build();
    }
    
    /**
     * Get information about a specific user on an organization. regex och path is
     * to distinguish it from method above (getUserInRole) which has the same path, 
     * but does not accept digits, otherwise server complains 
     * 
     * @param organizationUniqueId the unique id of the organization of the user
     * @param uniqueId the unique id of the user
     * @return the corresponding <code>UserAPI</code>
     */
    @GET
    @Path("{uniqueId : \\d+}")
    @SecuredService(permissions = {"user:view"})
    public Response getUser(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("uniqueId") long uniqueId ) {
        LOG.log(Level.FINEST, "getUser( organizationUniqueId: {0}, uniqueId: {1} )", new Object[] {organizationUniqueId, uniqueId});
        UserAPI sessionUserAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        UserAPI userAPI = userController.getUserAPI(organizationUniqueId, uniqueId, sessionUserAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( userAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(userAPI).build();
        }
    }
    
    /**
     * Create a user on an organization.
     * 
     * @param httpServletRequest
     * @param organizationUniqueId the unique id of the organization of the user
     * @param userAPI the user supplied values
     * @return the created <code>UserAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenException
     */
    @POST
    @SecuredService(permissions = {"user:create", "user:create_own"})
    public Response createUser(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            UserAPI userAPI) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "createUser( ... )" );
        UserAPI sessionUserAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        boolean isUserCreate = authHandler.hasPermission("user:create");
        boolean isUserCreateOwn = authHandler.hasPermission("user:create_own");
        boolean isSuperAdmin = authHandler.hasRole(UserRole.RoleName.Superadmin.toString());
        if( !authorizeHandleUsers(organizationUniqueId, null, isUserCreate, isUserCreateOwn, sessionUserAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        userAPI = userController.createUser(organizationUniqueId, userAPI, sessionUserAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest), isSuperAdmin);
        return Response.ok(userAPI).build();
    }
 
    /**
     * Update a user on an organization.
     * 
     * @param organizationUniqueId the unique id of the organization of the user
     * @param uniqueId the unique id of the user
     * @param userAPI the user supplied values
     * @return the updated <code>UserAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException in case validation fails
     */
    @PUT
    @Path("{uniqueId}")
    @SecuredService(permissions = {"user:update", "user:update_own", "user:update_contact"})
    public Response updateUser(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("uniqueId") long uniqueId, 
            UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateOrganization( uniqueId: {0} )", new Object[] {uniqueId});
        UserAPI sessionUserAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        boolean isUpdateUser = authHandler.hasPermission("user:update");
        boolean isUpdateUserOwn = authHandler.hasPermission("user:update_own");
        boolean isSuperAdmin = authHandler.hasRole(UserRole.RoleName.Superadmin.toString());
        if( !authorizeHandleUsers(organizationUniqueId, uniqueId, isUpdateUser, isUpdateUserOwn, sessionUserAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        userAPI = userController.updateUser(organizationUniqueId, uniqueId, userAPI, isUpdateUser || isUpdateUserOwn, sessionUserAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest), isSuperAdmin);
        if( uniqueId == sessionUserAPI.getId() ) {
            // must update session profile
            authHandler.addToSession(AuthHandler.USER_API_SESSION_KEY, userAPI);
        }
        if( userAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(userAPI).build();
        }
    }
    
    /**
     * REMOVED UNTIL WE KNOW EXACTLY HOW TO HANDLE USER REMOVE
     * Delete a user on an organization.
     * 
     * @param organizationUniqueId the unique id of the organization of the user
     * @param uniqueId the unique id of the user
     * @return HTTP 200 or HTTP 404 if the user is not found fails

    @DELETE
    @Path("{uniqueId}")
    @SecuredService(permissions = {"user:delete","user:delete_own"})
    public Response deleteUser(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("uniqueId") long uniqueId ) {
        LOG.log(Level.FINEST, "deleteUser( organizationUniqueId: {0}, uniqueId: {1} )", new Object[] {organizationUniqueId, uniqueId});
        boolean isDelete = authHandler.hasPermission("user:delete");
        boolean isDeleteOwn = authHandler.hasPermission("user:update_own");
        UserAPI sessionUserAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleUsers(organizationUniqueId, uniqueId, isDelete, isDeleteOwn, sessionUserAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        UserEngagement userEngagement = userController.deleteUser( organizationUniqueId, uniqueId, sessionUserAPI, authHandler.getSessionId());
        if( userEngagement == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok().build();
        }
    }
         */
    /**
     * Authorize whether user can handle user
     * 
     * @param organizationUniqueId unique id of organization
     * @param userUniqueId unique id of the user
     * @param isHandleUser whether the user has the permission to update all user info on all users
     * @param isHandlerUserOwn whether the user has the permission to update all user info on users in own organization
     * @return true if update is ok, otherwise false
     */
    private boolean authorizeHandleUsers(long organizationUniqueId, Long userUniqueId, boolean isHandleUser, boolean isHandlerUserOwn, UserAPI userAPI) {
        if( isHandleUser ) {
            return true;
        } else {
            if( !authorizeHandleOrganization(organizationUniqueId, userAPI)) {
                return false;
            }
            if( !isHandlerUserOwn ) {
                // users can only update themselves
                if( !userUniqueId.equals(userAPI.getId()) ) {
                    LOG.log( Level.WARNING, "Attempt to handle user not self" );
                    return false;
                }
            }
        }
        return true;
    }
    
}

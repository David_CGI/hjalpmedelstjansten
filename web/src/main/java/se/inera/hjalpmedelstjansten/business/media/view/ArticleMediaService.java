/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.media.view;

import java.util.logging.Level;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.media.controller.MediaController;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.media.MediaAPI;
import se.inera.hjalpmedelstjansten.model.api.media.MediaListAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;

/**
 * REST API for articles.
 * 
 * @author Tommy Berglund
 */
@Stateless
@Path("organizations/{organizationUniqueId}/articles/{articleUniqueId}/media")
@Interceptors({ PerformanceLogInterceptor.class })
public class ArticleMediaService extends BaseService {
    
    @Inject
    private HjmtLogger LOG;
    
    @EJB
    private MediaController mediaController;
    
    @EJB
    private AuthHandler authHandler;
       
    /**
     * Get media on product
     * 
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization to create article on
     * @param articleUniqueId
     * @return the created <code>ArticleAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @GET
    @SecuredService(permissions = {"media:view"})
    public Response getArticleMedia(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("articleUniqueId") long articleUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "getArticleMedia( organizationUniqueId: {0}, articleUniqueId: {1} )", new Object[] {organizationUniqueId, articleUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        MediaListAPI mediaListAPI = mediaController.getArticleMedia(organizationUniqueId, articleUniqueId, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( mediaListAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(mediaListAPI).build();
        }
    }
    
    @DELETE
    @Path("{mediaUniqueId}")
    @SecuredService(permissions = {"media:delete"})
    public Response deleteArticleMedia(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("articleUniqueId") long articleUniqueId,
            @PathParam("mediaUniqueId") long mediaUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "deleteArticleMedia( organizationUniqueId: {0}, articleUniqueId: {1}, mediaUniqueId: {2} )", new Object[] {organizationUniqueId, articleUniqueId, mediaUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization( organizationUniqueId, userAPI ) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        mediaController.deleteArticleMedia(organizationUniqueId, articleUniqueId, mediaUniqueId, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        return Response.ok().build();
    }
    
    /**
     * Create new document on product 
     * 
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization to create media on
     * @param articleUniqueId unique id of the article to create media on
     * @param multipartFormDataInput user data, including file (if any)
     * @return the created <code>MediaAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @Path("document")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @SecuredService(permissions = {"media:create"})
    public Response createArticleDocument(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("articleUniqueId") long articleUniqueId,
            MultipartFormDataInput multipartFormDataInput) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createArticleDocument( organizationUniqueId: {0}, articleUniqueId: {1} )", new Object[] {organizationUniqueId, articleUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization( organizationUniqueId, userAPI ) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        MediaAPI mediaAPI = mediaController.handleDocumentUpload(organizationUniqueId, null, articleUniqueId, multipartFormDataInput, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        return Response.ok(mediaAPI).build();
    }
 
    /**
     * Create new image on article 
     * 
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization to create media on
     * @param articleUniqueId unique id of the article to create media on
     * @param multipartFormDataInput user data, including file (if any)
     * @return the created <code>MediaAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @Path("image")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @SecuredService(permissions = {"media:create"})
    public Response createArticleImage(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("articleUniqueId") long articleUniqueId,
            MultipartFormDataInput multipartFormDataInput) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createProductImage( organizationUniqueId: {0}, articleUniqueId: {1} )", new Object[] {organizationUniqueId, articleUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization( organizationUniqueId, userAPI ) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        MediaAPI mediaAPI = mediaController.handleImageUpload(organizationUniqueId, null, articleUniqueId, multipartFormDataInput, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        return Response.ok(mediaAPI).build();
    }
    
    /**
     * Create new video on article 
     * 
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization to create media on
     * @param articleUniqueId unique id of the article to create media on
     * @param multipartFormDataInput user data, no file
     * @return the created <code>MediaAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @Path("video")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @SecuredService(permissions = {"media:create"})
    public Response createArticleVideo(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("articleUniqueId") long articleUniqueId,
            MultipartFormDataInput multipartFormDataInput) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createArticleVideo( organizationUniqueId: {0}, articleUniqueId: {1} )", new Object[] {organizationUniqueId, articleUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization( organizationUniqueId, userAPI ) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        MediaAPI mediaAPI = mediaController.handleVideoUpload(organizationUniqueId, 
                null, 
                articleUniqueId, 
                multipartFormDataInput, 
                userAPI, 
                authHandler.getSessionId(), 
                getRequestIp(httpServletRequest));
        return Response.ok(mediaAPI).build();
    }
    
}

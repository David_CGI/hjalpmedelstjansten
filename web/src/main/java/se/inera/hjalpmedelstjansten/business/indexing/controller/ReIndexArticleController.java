/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.indexing.controller;

import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleMapper;
import se.inera.hjalpmedelstjansten.business.product.controller.CategoryController;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificProperty;

/**
 * Reindex a specific article.
 * 
 * @author Tommy Berglund
 */
@Stateless
public class ReIndexArticleController {
    
    @Inject
    HjmtLogger LOG;
    
    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;
    
    @Inject
    CategoryController categoryController;
    
    @Inject
    ElasticSearchController elasticSearchController;
    
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void reIndexArticle( long organizationUniqueId, long articleId, Map<Long, List<CategorySpecificProperty>> categorySpecificPropertyMap ) {        
        Session session = em.unwrap(Session.class);
        session.setDefaultReadOnly(true);
        session.setFlushMode(FlushMode.MANUAL);   
        Article article = em.find(Article.class, articleId);
        long categoryUniqueId = article.getBasedOnProduct() == null ? article.getCategory().getUniqueId(): article.getBasedOnProduct().getCategory().getUniqueId();
        List<CategorySpecificProperty> categorySpecificPropertys = categorySpecificPropertyMap.get(categoryUniqueId);
        if( categorySpecificPropertys == null ) {
            categorySpecificPropertys = em.createNamedQuery(CategorySpecificProperty.FIND_BY_CATEGORY).
                        setParameter("categoryUniqueId", categoryUniqueId).
                        getResultList();
            categorySpecificPropertyMap.put(categoryUniqueId, categorySpecificPropertys);
        }
        ArticleAPI articleAPI = ArticleMapper.map(article, true, categorySpecificPropertys);
        elasticSearchController.bulkIndexArticle(articleAPI);
    }
    
    
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.generalpricelist.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistAPI;
import se.inera.hjalpmedelstjansten.model.entity.GeneralPricelistPricelist;

/**
 * Class for mapping between API and Entity classes
 * 
 * @author Tommy Berglund
 */
public class GeneralPricelistPricelistMapper {
    
    public static final List<GeneralPricelistPricelistAPI> map(List<GeneralPricelistPricelist> pricelists, boolean includeEverything, GeneralPricelistPricelist currentPricelist) {
        if( pricelists == null ) {
            return null;
        }
        List<GeneralPricelistPricelistAPI> pricelistAPIs = new ArrayList<>();
        for( GeneralPricelistPricelist pricelist : pricelists ) {
            pricelistAPIs.add(mapWithPricelist(pricelist, includeEverything, currentPricelist, null));
        }
        return pricelistAPIs;
    }
        
    public static final GeneralPricelistPricelistAPI mapWithPricelist(GeneralPricelistPricelist pricelist, boolean includeEverything, GeneralPricelistPricelist currentPricelist, Long numberOfPricelistRows) {
        if( pricelist == null ) {
            return null;
        }
        return mapWithStatus(pricelist, includeEverything, getPricelistStatus(pricelist, currentPricelist), null);
    }

    public static final GeneralPricelistPricelistAPI mapWithStatus(GeneralPricelistPricelist pricelist, boolean includeEverything, GeneralPricelistPricelist.Status status, Long numberOfPricelistRows) {
        if( pricelist == null ) {
            return null;
        }
        GeneralPricelistPricelistAPI pricelistAPI = new GeneralPricelistPricelistAPI();
        pricelistAPI.setId(pricelist.getUniqueId());
        pricelistAPI.setNumber(pricelist.getNumber());
        pricelistAPI.setValidFrom(pricelist.getValidFrom().getTime());
        pricelistAPI.setStatus(status == null ? null: status.toString());
        pricelistAPI.setHasPricelistRows(numberOfPricelistRows == null ? false: numberOfPricelistRows > 0);
        if( includeEverything ) {
            pricelistAPI.setGeneralPricelist(GeneralPricelistMapper.map(pricelist.getGeneralPricelist(), false));            
        }
        return pricelistAPI;
    }
    
    public static final GeneralPricelistPricelist map(GeneralPricelistPricelistAPI pricelistAPI) {
        if( pricelistAPI == null ) {
            return null;
        }
        GeneralPricelistPricelist pricelist = new GeneralPricelistPricelist();
        pricelist.setNumber(pricelistAPI.getNumber());
        pricelist.setValidFrom(new Date(pricelistAPI.getValidFrom()));
        return pricelist;
    }
    
    public static GeneralPricelistPricelist.Status getPricelistStatus(GeneralPricelistPricelist pricelist, GeneralPricelistPricelist currentPricelist) {
        Date now = new Date();
        GeneralPricelistPricelist.Status status;
        if( now.before(pricelist.getValidFrom()) ) {
            status = GeneralPricelistPricelist.Status.FUTURE;
        } else {
            if( currentPricelist == null || pricelist.getUniqueId().equals(currentPricelist.getUniqueId()) ) {
                status = GeneralPricelistPricelist.Status.CURRENT;
            } else {
                status = GeneralPricelistPricelist.Status.PAST;
            }
        }
        return status;
    }
    
}

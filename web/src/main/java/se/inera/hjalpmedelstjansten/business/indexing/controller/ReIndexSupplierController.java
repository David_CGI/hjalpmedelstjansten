/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.indexing.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificProperty;

/**
 * Reindex products and articles for a specific supplier.
 * 
 * @author Tommy Berglund
 */
@Stateless
public class ReIndexSupplierController {
    
    @Inject
    HjmtLogger LOG;
    
    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;

    @Inject
    ReIndexProductController reIndexProductController;
    
    @Inject
    ReIndexArticleController reIndexArticleController;
    
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void reIndexSupplier( long organizationUniqueId, String organizationName ) {
        LOG.log( Level.FINEST, "reIndexSupplier( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        Session session = em.unwrap(Session.class);
        session.setDefaultReadOnly(true);
        session.setFlushMode(FlushMode.MANUAL);
        long start = System.currentTimeMillis();
        // we want to reuse category specific propertys for each category as much
        // as possible
        Map<Long, List<CategorySpecificProperty>> categorySpecificPropertyMap = new HashMap<>();

        // handle products for organization
        List<Long> productIds =  em.createQuery("SELECT p.uniqueId FROM Product p WHERE p.organization.uniqueId = :organizationUniqueId").
                setParameter("organizationUniqueId", organizationUniqueId)
                .getResultList();
        if( productIds != null ) {
            LOG.log( Level.INFO, "reindex {0} products for supplier {1} )", new Object[] {productIds.size(), organizationUniqueId});
            int i = 1;
            int numberOfProducts = productIds.size();
            for( Long productId : productIds ) {
                LOG.log( Level.INFO, "Reindex product {0} of {1} for supplier {2}", new Object[] {i, numberOfProducts, organizationName});
                reIndexProductController.reIndexProduct(organizationUniqueId, productId, categorySpecificPropertyMap);
                i++;
            }
        }
                        
        // handle articles for organization
        List<Long> articleIds =  em.createQuery("SELECT a.uniqueId FROM Article a WHERE a.organization.uniqueId = :organizationUniqueId").
                setParameter("organizationUniqueId", organizationUniqueId)
                .getResultList();
        if( articleIds != null ) {
            LOG.log( Level.INFO, "reindex {0} articles for supplier {1} )", new Object[] {articleIds.size(), organizationUniqueId});            
            int i = 1;
            int numberOfArticles = articleIds.size();
            for( Long articleId : articleIds ) {
                LOG.log( Level.INFO, "Reindex article {0} of {1} for supplier {2}", new Object[] {i, numberOfArticles, organizationName});
                reIndexArticleController.reIndexArticle(organizationUniqueId, articleId, categorySpecificPropertyMap);
                i++;
            }
        }
        long total = System.currentTimeMillis() - start;
        LOG.logPerformance(this.getClass().getName(), "reIndexSupplier()", total, null);
    }
    
    
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.user.view;

import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.business.user.controller.RoleController;
import se.inera.hjalpmedelstjansten.model.api.RoleAPI;

/**
 * REST API for User functionality.
 * 
 * @author Tommy Berglund
 */
@Stateless
@Path("organizations/{organizationUniqueId}/roles")
@Interceptors({ PerformanceLogInterceptor.class })
public class RoleService extends BaseService {
    
    @Inject
    private HjmtLogger LOG;
    
    @Inject
    private RoleController roleController;

    /**
     * Get available roles for an organization
     * 
     * @param organizationUniqueId the unique id of the organization
     * @return a list of <code>RoleAPI</code> suitable for organization
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException
     */
    @GET
    @SecuredService(permissions = {"roles:view"})
    public Response getRolesForOrganization(
            @PathParam("organizationUniqueId") long organizationUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "getRolesForOrganization( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        List<RoleAPI> roleAPIs = roleController.getRolesForOrganization(organizationUniqueId);
        return Response.ok(roleAPIs).build();
    }

}

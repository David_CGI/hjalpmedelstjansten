/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.security.controller;

import java.util.logging.Level;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.realm.jdbc.JdbcRealm;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.LoggerService;

/**
 *
 * @author tomber
 */
public class TokenUserJDBCRealm extends JdbcRealm {

    private static final HjmtLogger LOG = LoggerService.getLogger(TokenUserJDBCRealm.class.getName());
    
    @Override
    public boolean supports(AuthenticationToken token) {
        if( token instanceof TokenUserToken ) {
            return true;
        }
        LOG.log( Level.FINEST, "token is NOT of type TokenUserToken" );
        return false;
    }    
    
}

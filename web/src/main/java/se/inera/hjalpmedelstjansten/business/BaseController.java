/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business;

import java.util.logging.Level;
import javax.inject.Inject;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;

/**
 *
 * @author tomber
 */
public class BaseController {
    
    @Inject
    private HjmtLogger LOG;
    
    /**
     * Generate a custom exception based on the supplied message and exception.
     * 
     * @param field
     * @param message the message
     * @param cause the cause of the exception
     * @param validationMessageService
     * @return a custom exception containing the message and cause.
     */
    protected HjalpmedelstjanstenException generateException( String field, String message, Throwable cause, ValidationMessageService validationMessageService ) {
        LOG.log(Level.FINEST, "generateException( ... )");
        HjalpmedelstjanstenException exception = null;
        if( cause != null ) {
            exception = new HjalpmedelstjanstenException("Request failed", cause);
        } else {
            exception = new HjalpmedelstjanstenException("Request failed");
        }
        exception.addValidationMessage(field, validationMessageService.getMessage(message));
        return exception;
    }
    
    /**
     * Generate an exception because of a forbidden action, will cause service to return 
     * a response with HTTP status code FORBIDDEN
     * 
     * @return a forbidden exception 
     */
    protected HjalpmedelstjanstenException generateForbiddenException() {
        LOG.log(Level.FINEST, "generateException( ... )");
        HjalpmedelstjanstenException exception = new HjalpmedelstjanstenException("Request failed", HjalpmedelstjanstenException.Type.FORBIDDEN);
        return exception;
    }
    
}

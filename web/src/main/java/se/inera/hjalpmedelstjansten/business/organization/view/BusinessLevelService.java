/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.organization.view;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.organization.controller.BusinessLevelController;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.BusinessLevelAPI;
import se.inera.hjalpmedelstjansten.model.entity.BusinessLevel;

/**
 * REST API for BusinessLevel functionality.
 * 
 * @author Tommy Berglund
 */
@Stateless
@Path("organizations/{organizationUniqueId}/businesslevels")
@Interceptors({ PerformanceLogInterceptor.class })
public class BusinessLevelService extends BaseService {
    
    @Inject
    private HjmtLogger LOG;
    
    @EJB
    private BusinessLevelController businessLevelController;

    /**
     * Return all business levels in the database for the given organization.
     * 
     * @param organizationUniqueId
     * @param statuses
     * @return a list of <code>BusinessLevelAPI</code>
     */
    @GET
    @SecuredService(permissions = {"businesslevel:view"})
    public Response searchBusinessLevelsByOrganization(
            @PathParam("organizationUniqueId") long organizationUniqueId, 
            @QueryParam("status") List<BusinessLevel.Status> statuses) {
        LOG.log(Level.FINEST, "searchBusinessLevelsByOrganization( organizationUniqueId: {0}, statuses: {1} )", new Object[] {organizationUniqueId, statuses});
        if( statuses == null || statuses.isEmpty() ) {
            statuses = new ArrayList<>();
            statuses.addAll(Arrays.asList(BusinessLevel.Status.values()));
        }
        List<BusinessLevelAPI> businessLevelAPIs = businessLevelController.getBusinessLevelsByOrganizationAndStatus(organizationUniqueId, statuses);
        return Response.
                ok(businessLevelAPIs).
                build();
    }
    
    /**
     * Add business level to the given organization.
     * 
     * @param organizationUniqueId unique id of the organization
     * @param businessLevelAPI user supplied values
     * @return a list of <code>BusinessLevelAPI</code>
     */
    @POST
    @SecuredService(permissions = {"businesslevel:create"})
    public Response addBusinessLevelToOrganization(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            BusinessLevelAPI businessLevelAPI) {
        LOG.log(Level.FINEST, "addBusinessLevelToOrganization( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        businessLevelAPI = businessLevelController.addBusinessLevelToOrganization(organizationUniqueId, businessLevelAPI);
        if( businessLevelAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.
                ok(businessLevelAPI).
                build();
    }
    
    /**
     * Add business level to the given organization.
     * 
     * @param organizationUniqueId unique id of the organization
     * @param businessLevelId unique id of the business level
     * @return the inactivated <code>BusinessLevelAPI</code>
     */
    @POST
    @Path("{businessLevelId}/inactivate")
    @SecuredService(permissions = {"businesslevel:inactivate"})
    public Response inactivateBusinessLevelOnOrganization(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("businessLevelId") long businessLevelId) {
        LOG.log(Level.FINEST, "addBusinessLevelToOrganization( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        BusinessLevelAPI businessLevelAPI = businessLevelController.inactivateBusinessLevelOnOrganization(organizationUniqueId, businessLevelId);
        if( businessLevelAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.
                ok(businessLevelAPI).
                build();
    }

    /**
     * Edit business level for the given organization.
     *
     * @param organizationUniqueId unique id of the organization
     * @param businessLevelId unique id of the business level
     * @return the inactivated <code>BusinessLevelAPI</code>
     */
    @POST
    @Path("{businessLevelId}/update")
    @SecuredService(permissions = {"businesslevel:inactivate"})   //?
    public Response editBusinessLevelOnOrganization(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("businessLevelId") long businessLevelId,
            BusinessLevelAPI businessLevelAPI) {
        LOG.log(Level.FINEST, "editBusinessLevelOnOrganization( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        businessLevelAPI = businessLevelController.editBusinessLevelOnOrganization(organizationUniqueId, businessLevelId, businessLevelAPI);
        if( businessLevelAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.
                ok(businessLevelAPI).
                build();
    }
    
    /**
     * Remove business level from the given organization.
     * 
     * @param organizationUniqueId unique id of the organization
     * @param businessLevelId unique id of the business level
     * @return 
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     * e.g. there are <code>UserEngagement</code>s connnected to the business level.
     */
    @DELETE
    @Path("{businessLevelId}")
    @SecuredService(permissions = {"businesslevel:delete"})
    public Response deleteBusinessLevelFromOrganization(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("businessLevelId") long businessLevelId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "deleteBusinessLevelFromOrganization( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        BusinessLevelAPI businessLevelAPI = businessLevelController.deleteBusinessLevelFromOrganization(organizationUniqueId, businessLevelId);
        return Response.
                ok(businessLevelAPI).
                build();
    }
    
}
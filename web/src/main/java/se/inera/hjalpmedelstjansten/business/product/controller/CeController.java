/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.product.controller;

import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCEAPI;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEDirective;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEStandard;

/**
 *
 * @author Tommy Berglund
 */
@Stateless
public class CeController {
    
    @Inject
    private HjmtLogger LOG;
    
    @PersistenceContext( unitName = "HjmtjPU")
    private EntityManager em;
    
    /**
     * Get all CE directives and standards
     * 
     * @return a <code>CeAPI</code> containing all standards and directives
     */
    public CVCEAPI getAllCe() {
        LOG.log(Level.FINEST, "getAllCe()");
        CVCEAPI ceAPI = new CVCEAPI();
        ceAPI.setStandards(ProductMapper.mapCEStandards(findAllCEStandards()));
        ceAPI.setDirectives(ProductMapper.mapCEDirectives(findAllCEDirectives()));
        return ceAPI;
    }
    
    public List<CVCEStandard> findAllCEStandards() {
        return em.createNamedQuery(CVCEStandard.FIND_ALL).getResultList();
    }
    
    public List<CVCEDirective> findAllCEDirectives() {
        return em.createNamedQuery(CVCEDirective.FIND_ALL).getResultList();
    }
    
    public CVCEStandard findCEStandardById(long uniqueId) {
        LOG.log(Level.FINEST, "findCEStandardById(uniqueId: {0})", new Object[] {uniqueId});
        return em.find(CVCEStandard.class, uniqueId);
    }

    public CVCEDirective findCEDirectiveById(long uniqueId) {
        LOG.log(Level.FINEST, "findCEDirectiveById(uniqueId: {0})", new Object[] {uniqueId});
        return em.find(CVCEDirective.class, uniqueId);
    }
    
    public CVCEStandard findCEStandardByName(String name) {
        LOG.log(Level.FINEST, "findCEStandardByName(name: {0})", new Object[] {name});
        List<CVCEStandard> standards = em.createNamedQuery(CVCEStandard.FIND_BY_NAME).
                setParameter("name", name).
                getResultList();
        // there can be maximum one standard by code
        if( standards != null && !standards.isEmpty() ) {
            return standards.get(0);
        }
        return null;
    }

    public CVCEDirective findCEDirectiveByName(String name) {
        LOG.log(Level.FINEST, "findCEDirectiveByName(name: {0})", new Object[] {name});
        List<CVCEDirective> directives = em.createNamedQuery(CVCEDirective.FIND_BY_NAME).
                setParameter("name", name).
                getResultList();
        // there can be maximum one directive by name
        if( directives != null && !directives.isEmpty() ) {
            return directives.get(0);
        }
        return null;
    }
    
}

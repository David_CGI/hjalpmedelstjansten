/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.export.controller;

import java.util.Set;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.api.ExportSettingsAPI;

/**
 * Validation methods for export settings
 * 
 * @author Tommy Berglund
 */
@Stateless
public class ExportSettingsValidation {
    
    @Inject
    HjmtLogger LOG;
    
    @Inject
    ValidationMessageService validationMessageService;
    
    public void validateForCreate(ExportSettingsAPI exportSettingsAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForCreate(...)" );
        validateExportSettingsAPI(exportSettingsAPI);
    }
    
    public void validateForUpdate(ExportSettingsAPI exportSettingsAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForUpdate(...)" );
        validateExportSettingsAPI(exportSettingsAPI);
    }
    
    private void validateExportSettingsAPI(ExportSettingsAPI exportSettingsAPI) throws HjalpmedelstjanstenValidationException {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<ExportSettingsAPI>> constraintViolations = validator.validate(exportSettingsAPI, Default.class);
        if( constraintViolations != null && !constraintViolations.isEmpty() ) {
            HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
            for( ConstraintViolation constraintViolation : constraintViolations ) {
                exception.addValidationMessage(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage());
            }
            throw exception;
        }
    }
    
}

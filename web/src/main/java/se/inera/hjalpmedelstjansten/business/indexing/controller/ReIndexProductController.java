/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.indexing.controller;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.product.controller.CategoryController;
import se.inera.hjalpmedelstjansten.business.product.controller.ProductMapper;
import se.inera.hjalpmedelstjansten.model.api.ProductAPI;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificProperty;
import se.inera.hjalpmedelstjansten.model.entity.Product;

/**
 * Reindex a specific product. 
 * 
 * @author Tommy Berglund
 */
@Stateless
public class ReIndexProductController {
    
    @Inject
    HjmtLogger LOG;
    
    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;

    @Inject
    ReIndexArticleController reIndexArticleController;
    
    @Inject
    CategoryController categoryController;
    
    @Inject
    ElasticSearchController elasticSearchController;
    
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void reIndexProduct( long organizationUniqueId, long productId, Map<Long, List<CategorySpecificProperty>> categorySpecificPropertyMap ) {
        LOG.log( Level.FINEST, "Fetch product: {0}", new Object[] {productId});
        Session session = em.unwrap(Session.class);
        session.setDefaultReadOnly(true);
        session.setFlushMode(FlushMode.MANUAL);
        Product product = em.find(Product.class, productId);
        List<CategorySpecificProperty> categorySpecificPropertys = categorySpecificPropertyMap.get(product.getCategory().getUniqueId());
        if( categorySpecificPropertys == null ) {
            categorySpecificPropertys = em.createNamedQuery(CategorySpecificProperty.FIND_BY_CATEGORY).
                        setParameter("categoryUniqueId", product.getCategory().getUniqueId()).
                        getResultList();
            categorySpecificPropertyMap.put(product.getCategory().getUniqueId(), categorySpecificPropertys);
        }
        ProductAPI productAPI = ProductMapper.map(product, true, categorySpecificPropertys);
        elasticSearchController.bulkIndexProduct(productAPI);
    }
    
    
}

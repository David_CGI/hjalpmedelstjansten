/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.assortment.view;

import java.util.List;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.assortment.controller.CountyController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCountyAPI;

/**
 * REST API for counties and municipalities Unit functionality.
 * 
 * @author Tommy Berglund
 */
@Stateless
@Path("counties")
@Interceptors({ PerformanceLogInterceptor.class })
public class CountyService extends BaseService {
    
    @Inject
    private HjmtLogger LOG;
    
    @EJB
    private CountyController countyMunicipalityController;
    
    /**
     * Return all counties in the database. 
     * 
     * @return a list of <code>CVCountyMunicipalityAPI</code>
     */
    @GET
    @SecuredService(permissions = {"counties:view"})
    public Response getAllCounties() {
        LOG.log(Level.FINEST, "getAllCounties()");
        List<CVCountyAPI> countyAPIs = countyMunicipalityController.getAllCounties();
        return Response.ok(countyAPIs).build();
    }
    
}

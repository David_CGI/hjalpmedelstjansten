/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.security.view;

import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.subject.Subject;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.security.controller.StandardUserToken;
import se.inera.hjalpmedelstjansten.business.security.controller.TokenUserToken;

/**
 * Interface used to be able to substitute authentication framework without
 * having to rewrite all code. 
 * 
 * @author Tommy Berglund
 */
@Stateless
public class ShiroAuthHandler implements AuthHandler {

    @Inject
    private HjmtLogger LOG;
    
    @Override
    public void standardLogin(String username, String password) {
        try {
            Subject currentUser = SecurityUtils.getSubject();
            StandardUserToken standardUserToken = new StandardUserToken(username, password);
            currentUser.login(standardUserToken);
        } catch( UnknownAccountException e ) {
            LOG.log(Level.INFO, "Attempt to login with unknown account");
            throw e;
        } catch( Exception e ) {
            LOG.log(Level.WARNING, "Attempt to login of unhandled reason", e);
            throw e;
        }
    }
        
    @Override
    public void tokenLogin(String organizationNumber, String token) {
        try {
            Subject currentUser = SecurityUtils.getSubject();
            TokenUserToken tokenUserToken = new TokenUserToken(organizationNumber, token);
            currentUser.login(tokenUserToken);
        } catch( UnknownAccountException e ) {
            LOG.log(Level.INFO, "Attempt to login with unknown account");
            throw e;
        } catch( Exception e ) {
            LOG.log(Level.WARNING, "Attempt to login of unhandled reason", e);
            throw e;
        }
    }
        
    @Override
    public void addToSession( Object key, Object value ) {
        Subject currentUser = SecurityUtils.getSubject();
        currentUser.getSession().setAttribute(key, value);
    }
    
    @Override
    public Object getFromSession( Object key ) {
        Subject currentUser = SecurityUtils.getSubject();
        return currentUser.getSession().getAttribute(key);
    }

    @Override
    public boolean hasRole(String role) {
        Subject currentUser = SecurityUtils.getSubject();
        return currentUser.hasRole(role);
    }

    @Override
    public boolean hasPermission(String permission) {
        Subject currentUser = SecurityUtils.getSubject();
        return currentUser.isPermitted(permission);
    }

    @Override
    public String getSessionId() {
        return SecurityUtils.getSubject().getSession().getId().toString();
    }

}

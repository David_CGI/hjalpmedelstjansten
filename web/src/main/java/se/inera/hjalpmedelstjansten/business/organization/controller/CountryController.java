/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.organization.controller;

import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCountryAPI;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCountry;

/**
 *
 * @author Tommy Berglund
 */
@Stateless
public class CountryController {
    
    @Inject
    private HjmtLogger LOG;
    
    @PersistenceContext( unitName = "HjmtjPU")
    private EntityManager em;
    
    public List<CVCountryAPI> getAllCountries() {
        LOG.log(Level.FINEST, "getAllCountries()");
        return CountryMapper.map(em.createNamedQuery(CVCountry.FIND_ALL)
                .getResultList());
    }
    
    public CVCountry getCountry(long uniqueId) {
        LOG.log(Level.FINEST, "getCountry( uniqueId: {0} )");
        return em.find(CVCountry.class, uniqueId);
    }
    
}

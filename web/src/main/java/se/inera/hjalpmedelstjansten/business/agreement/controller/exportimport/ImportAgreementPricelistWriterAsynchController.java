/*
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.agreement.controller.exportimport;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.*;
import java.util.logging.Level;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.transaction.Transactional;

import org.apache.poi.POIXMLProperties;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jboss.ejb3.annotation.TransactionTimeout;
import org.openxmlformats.schemas.officeDocument.x2006.customProperties.CTProperty;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementPricelistController;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementPricelistRowController;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementPricelistRowMapper;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementPricelistRowValidation;
import se.inera.hjalpmedelstjansten.business.helpers.ImportExcelHelper;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.product.controller.exportimport.ExportImportProductsArticlesController;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.user.controller.EmailController;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVGuaranteeUnitAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPreventiveMaintenanceAPI;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelist;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelistRow;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;

/**
 * Asynchronous import of read agreement pricelist rows
 *
 * @author Tommy Berglund
 */
@Stateless
public class ImportAgreementPricelistWriterAsynchController extends ExportImportPricelistController {

    @Inject
    HjmtLogger LOG;

    @Inject
    ImportExcelHelper importExcelHelper;

    @Inject
    AgreementPricelistRowController agreementPricelistRowController;

    @Inject
    ImportAgreementPricelistController importPricelistController;

    @Inject
    EmailController emailController;

    @Inject
    ValidationMessageService validationMessageService;

    @Inject
    String importPricelistRowsMailSubjectSuccessful;

    @Inject
    String importPricelistRowsMailBodySuccessful;

    @Inject
    OrganizationController organizationController;

    @Inject
    String importPricelistRowsMailSubjectFailed;

    @Inject
    AgreementPricelistController agreementPricelistController;

    @Inject
    AgreementPricelistRowValidation agreementPricelistRowValidation;

    @Inject
    String importPricelistRowsMailBodyFailed;


    @Asynchronous
    @Transactional
    @TransactionTimeout(7200)
    public void readAndSaveItems(AgreementPricelist agreementPricelist,
                                 long organizationUniqueId,
                                 long agreementUniqueId,
                                 long pricelistUniqueId,
                                 XSSFWorkbook workbook,
                                 UserAPI userAPI,
                                 String sessionId,
                                 String requestIp,
                                 String filename) {
        LOG.log(Level.FINEST, "importWorkbook( organizationUniqueId: {0} )", new Object[]{organizationUniqueId});
        HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validering misslyckades");
        Organization organization = organizationController.getOrganization(organizationUniqueId);
        List<AgreementPricelistRowAPI> rowsThatWillBeSentForInactivityRequest = new ArrayList<>();
        AgreementPricelist currentPricelist = agreementPricelistController.getCurrentPricelist(agreementUniqueId);
        List<String> pricelistArticleUniqueNumbers = agreementPricelistRowController.getAgreementPricelistArticleUniqueNumbers(pricelistUniqueId);
        List<ImportItem> importItems = null;
        //#HJAL- 1791 added general Exception catch
        try {
            // first read items from excel to list of rows and validate them
            importItems = readItems(organizationUniqueId,
                    agreementUniqueId,
                    pricelistUniqueId,
                    agreementPricelist,
                    currentPricelist,
                    pricelistArticleUniqueNumbers,
                    workbook,
                    organization,
                    exception,
                    userAPI,
                    rowsThatWillBeSentForInactivityRequest);
            // we stop if there are validation errors already
        } catch (Exception ex) {
            try {
                StringWriter sw = new StringWriter();
                ex.printStackTrace(new PrintWriter(sw));
                String exceptionAsString = sw.toString();

                String mailText = "Okänt fel vid validering. Är kolumnerna rätt formaterade? Detaljer: <br/><br/>" + ex.getMessage() + "<br/> <br/><b>Stacktrace att skicka till support</b><br/>" + exceptionAsString;
                String mailBody = String.format(importPricelistRowsMailBodyFailed, mailText);
                emailController.send(userAPI.getElectronicAddress().getEmail(), filename + ":" + importPricelistRowsMailSubjectFailed, mailBody);
            } catch (MessagingException ex2) {
                LOG.log(Level.SEVERE, "Failed to send message on import pricelist", ex2);
            }
            LOG.log(Level.SEVERE, "Failed to import pricelist", ex);
            return;
        }
        LOG.log(Level.FINEST, "Finished reading items. Found: {0} validation errors", new Object[]{exception.getValidationMessages().size()});


        // now it's time to save rows
        List<Long> pricelistArticleUniqueIds = agreementPricelistRowController.getAgreementPricelistArticleUniqueIds(pricelistUniqueId);
        LOG.log(Level.FINEST, "Number of article unique ids: {0}", new Object[]{pricelistArticleUniqueIds.size()});
        if (importItems != null) {
            saveItems(organizationUniqueId,
                    agreementUniqueId,
                    pricelistUniqueId,
                    importItems,
                    agreementPricelist,
                    currentPricelist,
                    pricelistArticleUniqueIds,
                    exception,
                    userAPI,
                    sessionId,
                    requestIp,
                    filename,
                    rowsThatWillBeSentForInactivityRequest);
        }
    }

    /**
     * @param organizationUniqueId
     * @param agreementUniqueId
     * @param pricelistUniqueId
     * @param importItems
     * @param agreementPricelist
     * @param currentPricelist
     * @param pricelistArticleUniqueIds
     * @param userAPI
     * @param sessionId
     * @param requestIp
     */

    public void saveItems(long organizationUniqueId,
                          long agreementUniqueId,
                          long pricelistUniqueId,
                          List<ImportItem> importItems,
                          AgreementPricelist agreementPricelist,
                          AgreementPricelist currentPricelist,
                          List<Long> pricelistArticleUniqueIds,
                          HjalpmedelstjanstenValidationException exception,
                          UserAPI userAPI,
                          String sessionId,
                          String requestIp,
                          String filename,
                          List<AgreementPricelistRowAPI> rowsThatWillBeSentForInactivityRequest) {
        LOG.log(Level.FINEST, "saveItems( organizationUniqueId: {0}, pricelistUniqueId: {1} )", new Object[]{organizationUniqueId, pricelistUniqueId});
        if (exception.getValidationMessages().isEmpty()) {
            for (ImportItem importItem : importItems) {
                AgreementPricelistRowAPI agreementPricelistRowAPI = importItem.getAgreementPricelistRowAPI();
                try {
                    // id
                    if (agreementPricelistRowAPI.getId() == null) {
                        agreementPricelistRowController.createPricelistRow(organizationUniqueId,
                                agreementPricelistRowAPI,
                                agreementPricelist,
                                currentPricelist,
                                pricelistArticleUniqueIds,
                                userAPI,
                                sessionId,
                                requestIp);
                        pricelistArticleUniqueIds.add(agreementPricelistRowAPI.getArticle().getId());
                    } else {
                        agreementPricelistRowController.updatePricelistRow(organizationUniqueId,
                                agreementUniqueId,
                                pricelistUniqueId,
                                agreementPricelistRowAPI.getId(),
                                agreementPricelistRowAPI,
                                userAPI,
                                sessionId,
                                requestIp);
                    }
                } catch (HjalpmedelstjanstenValidationException ex) {
                    for (ErrorMessageAPI errorMessageAPI : ex.getValidationMessages()) {
                        importPricelistController.addRowErrorMessage(exception, importItem.getRowNumber(), errorMessageAPI.getField(), errorMessageAPI.getMessage());
                    }
                    // we break immediately since the transaction is done, errors should
                    // already have been handled before we get to this stage. if validation
                    // errors still exist, they should be handled in the read-phase
                    // in ImportPricelistController
                    break;
                } catch (Exception ex) {
                    LOG.log(Level.WARNING, "Failed to handle pricelist row import", ex);
                    importPricelistController.addRowErrorMessage(exception, importItem.getRowNumber(), "uknown", validationMessageService.getMessage("import.pricelist.unhandledException"));
                    // we break immediately since this is an "unhandled" error
                    break;
                }
            }
                //long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, List<AgreementPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp
            try {
                agreementPricelistRowController.inactivateRows(organizationUniqueId, agreementUniqueId, pricelistUniqueId,rowsThatWillBeSentForInactivityRequest, userAPI, sessionId, requestIp);
            } catch (HjalpmedelstjanstenValidationException e) {
                exception.addValidationMessage(FIELD_REQUEST_INACTIVE, validationMessageService.getMessage("pricelist.update.cantAskForInactivationWhenChangesHaveBeenMade"));
            }
        }
        try {
            if (!exception.getValidationMessages().isEmpty()) {
                LOG.log(Level.FINEST, "Import failed, send fail message");
                StringBuilder allErrors = new StringBuilder();
                for (ErrorMessageAPI errorMessageAPI : exception.getValidationMessages()) {
                    allErrors.append(errorMessageAPI.getMessage()).append("<br />");
                }
                String mailBody = String.format(importPricelistRowsMailBodyFailed, allErrors.toString());
                emailController.send(userAPI.getElectronicAddress().getEmail(), filename + ":" + importPricelistRowsMailSubjectFailed, mailBody);
            } else {
                LOG.log(Level.FINEST, "Import successful of {0} items, send success message", new Object[]{importItems.size()});
                String mailBody = String.format(importPricelistRowsMailBodySuccessful, importItems.size());
                emailController.send(userAPI.getElectronicAddress().getEmail(), filename + ":" + importPricelistRowsMailSubjectSuccessful, mailBody);
            }
        } catch (MessagingException ex) {
            LOG.log(Level.SEVERE, "Failed to send message on import pricelist", ex);
        }
    }


    private List<ImportItem> readItems(long organizationUniqueId,
                                       long agreementUniqueId,
                                       long pricelistUniqueId,
                                       AgreementPricelist agreementPricelist,
                                       AgreementPricelist currentPricelist,
                                       List<String> pricelistArticleUniqueNumbers,
                                       XSSFWorkbook workbook,
                                       Organization organization,
                                       HjalpmedelstjanstenValidationException exception,
                                       UserAPI userAPI,
                                       List<AgreementPricelistRowAPI> rowsThatWillBeSentForInactivityRequest) {
        LOG.log(Level.FINEST, "readItems( organizationUniqueId: {0}, pricelistUniqueId: {1} )", new Object[]{organizationUniqueId, pricelistUniqueId});
        List<ImportItem> importItems = new ArrayList<>();
        Map<String, CVGuaranteeUnit> guaranteeUnitNamesMap = guaranteeUnitController.getGuaranteeUnitNamesMap();
        XSSFSheet sheet = workbook.getSheet(PRICELISTROW_SHEET_NAME);
        XSSFRow fieldNamesRow = sheet.getRow(0);
        int i = 2;
        XSSFRow row;
        Set<String> articleNumbersAlreadyHandled = new HashSet<>();
        Long artUniqueId;
        while (!isEmptyRow(row = sheet.getRow(i), fieldNamesRow)) {
            i++;
            // id
            XSSFCell uniqueIdCell = row.getCell(0);
            Long uniqueId = uniqueIdCell == null ? null : (long) uniqueIdCell.getNumericCellValue();
            LOG.log(Level.FINEST, "Found pricelist row unique id: {0}", new Object[]{uniqueId});

            //if uniqueId == null, try to get it from existing
            String articleNumber = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_ARTICLE_NUMBER);
            if (uniqueId == null) {
                artUniqueId = articleController.findByArticleNumberAndOrganization(articleNumber, organization.getUniqueId()) == null ? null : articleController.findByArticleNumberAndOrganization(articleNumber, organization.getUniqueId()).getUniqueId();
                //uniqueId = artUniqueId == null ? null : agreementPricelistRowController.getAgreementPricelistRowUniqueId(artUniqueId, pricelistUniqueId).get(0);
                uniqueId = artUniqueId == null ? null : agreementPricelistRowController.getAgreementPricelistRowUniqueId(artUniqueId, pricelistUniqueId).size() == 0 ? null : agreementPricelistRowController.getAgreementPricelistRowUniqueId(artUniqueId, pricelistUniqueId).get(0);
            }

            AgreementPricelistRow agreementPricelistRow = uniqueId == null ? null : agreementPricelistRowController.getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, uniqueId, userAPI);

            AgreementPricelistRowAPI agreementPricelistRowAPI = agreementPricelistRow == null ? new AgreementPricelistRowAPI() : AgreementPricelistRowMapper.map(agreementPricelistRow, true, false);
            //String articleNumber = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_ARTICLE_NUMBER);
            boolean getIdFromExisting = false;
            if (agreementPricelistRowAPI.getId() == null) {
                // this is an add, check article number already exists
                // HJAL-2022
                // this is an update if article number exists
                // get the id from the existing
                if (pricelistArticleUniqueNumbers.contains(articleNumber)) {
                    getIdFromExisting = true;
                //    addRowErrorMessage(exception, row.getRowNum(), "articleNumber", validationMessageService.getMessage("pricelistrow.article.alreadyExistsInPricelist", articleNumber));
                //    // no need to continue
                //    continue;
                }
            }
            //if (articleNumbersAlreadyHandled.contains(articleNumber)) {
                //addRowErrorMessage(exception, row.getRowNum(), "articleNumber", validationMessageService.getMessage("pricelistrow.article.alreadyExistsInInput", articleNumber));
                // no need to continue
                //continue;
            //}
            // HJAL-2022
            if (!articleNumbersAlreadyHandled.contains(articleNumber)) {
                articleNumbersAlreadyHandled.add(articleNumber);
            }

            boolean isNewOrChanged = fillAgreementPricelistRowAPI(getIdFromExisting, organization, guaranteeUnitNamesMap, row, fieldNamesRow, agreementPricelistRowAPI, exception, articleNumbersAlreadyHandled, rowsThatWillBeSentForInactivityRequest);

            // if not success, it means something caused the fill not to complete
            // in that case we don't have to validate
            if (isNewOrChanged) {
                Set<ErrorMessageAPI> errorMessageAPIs;
                if (agreementPricelistRowAPI.getId() == null) {
                    errorMessageAPIs = agreementPricelistRowValidation.tryForCreate(agreementPricelistRowAPI, agreementPricelist, currentPricelist);
                } else {
                    errorMessageAPIs = agreementPricelistRowValidation.tryForUpdate(agreementPricelistRowAPI, agreementPricelistRow, currentPricelist);
                }
                for (ErrorMessageAPI errorMessageAPI : errorMessageAPIs) {
                    addRowErrorMessage(exception, row.getRowNum(), errorMessageAPI.getField(), errorMessageAPI.getMessage());
                }
                importItems.add(new ImportItem(agreementPricelistRowAPI, row.getRowNum()));
            }
        }
        return importItems;
    }

    private boolean isEmptyRow(XSSFRow row, XSSFRow fieldNamesRow) {
        if (row == null) {
            return true;
        }
        String articleNumber = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_ARTICLE_NUMBER);
        if (articleNumber == null || articleNumber.isEmpty()) {
            return true;
        }
        return false;
    }

    private boolean fillAgreementPricelistRowAPI(
            boolean getIdFromExisting,
            Organization organization,
            Map<String, CVGuaranteeUnit> guaranteeUnitNamesMap,
            XSSFRow row,
            XSSFRow fieldNamesRow,
            AgreementPricelistRowAPI agreementPricelistRowAPI,
            HjalpmedelstjanstenValidationException exception,
            Set<String> articleNumbersAlreadyHandled,
            List<AgreementPricelistRowAPI> rowsToBeInactivated) {
        LOG.log(Level.FINEST, "fillAgreementPricelistRowAPI( organization->uniqueId: {0}, agreementPricelistRowAPI->id: {1} )", new Object[]{organization.getUniqueId(), agreementPricelistRowAPI.getId()});
        boolean isNewOrChanged = false;
        Long aprId;
        if (agreementPricelistRowAPI.getId() == null) {
            if (!getIdFromExisting) {
                agreementPricelistRowAPI.setStatus(AgreementPricelistRow.Status.CREATED.toString());
                isNewOrChanged = true;
                String articleNumber = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_ARTICLE_NUMBER);
                ArticleAPI articleAPI = articleController.getArticleAPIByArticleNumber(organization.getUniqueId(), articleNumber);
                agreementPricelistRowAPI.setArticle(articleAPI);
            }
            else {
                agreementPricelistRowAPI.setStatus(AgreementPricelistRow.Status.CREATED.toString());
                isNewOrChanged = true;
                String articleNumber = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_ARTICLE_NUMBER);
                ArticleAPI articleAPI = articleController.getArticleAPIByArticleNumber(organization.getUniqueId(), articleNumber);
                aprId = articleController.findByArticleNumberAndOrganization(articleNumber, organization.getUniqueId()).getUniqueId();
                agreementPricelistRowAPI.setId(aprId);
                agreementPricelistRowAPI.setArticle(articleAPI);
            }
        }

        // price
        Double price = this.importExcelHelper.getCellNumericValueByName(fieldNamesRow, row, FIELD_PRICE_NAME);
        if (price != null) {
            agreementPricelistRowAPI.setPrice(BigDecimal.valueOf(price));
            isNewOrChanged = true;
        }

        // valid from
        Date validFromDate = this.importExcelHelper.getCellDateValueByName(fieldNamesRow, row, FIELD_VALID_FROM_NAME);
        if (validFromDate != null) {
            agreementPricelistRowAPI.setValidFrom(validFromDate.getTime());
            isNewOrChanged = true;
        }

        // least order quantity
        Double leastOrderQuantity = this.importExcelHelper.getCellNumericValueByName(fieldNamesRow, row, FIELD_LEAST_ORDER_QUANTITY_NAME);
        if (leastOrderQuantity != null) {
            agreementPricelistRowAPI.setLeastOrderQuantity(leastOrderQuantity.intValue());
            isNewOrChanged = true;
        }

        // delivery time
        Double deliveryTime = this.importExcelHelper.getCellNumericValueByName(fieldNamesRow, row, FIELD_DELIVERY_TIME_NAME);
        if (deliveryTime != null) {
            agreementPricelistRowAPI.setDeliveryTime(deliveryTime.intValue());
            isNewOrChanged = true;
        }

        // warranty quantity
        Double warrantyQuantity = this.importExcelHelper.getCellNumericValueByName(fieldNamesRow, row, FIELD_WARRANTY_QUANTITY_NAME);
        if (warrantyQuantity != null) {
            if (CLEAR_VALUE_DOUBLE.equals(warrantyQuantity)) {
                agreementPricelistRowAPI.setWarrantyQuantity(null);
            } else {
                agreementPricelistRowAPI.setWarrantyQuantity(warrantyQuantity.intValue());
            }
            isNewOrChanged = true;
        }

        // warranty quantity unit
        String warrantyQuantityUnitName = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_WARRANTY_QUANTITY_UNIT_NAME);
        if (warrantyQuantityUnitName != null && !warrantyQuantityUnitName.isEmpty()) {
            CVGuaranteeUnitAPI warrantyQuantityUnitAPI = getGuaranteeUnitAPI(warrantyQuantityUnitName, row.getRowNum(), exception, guaranteeUnitNamesMap);
            agreementPricelistRowAPI.setWarrantyQuantityUnit(warrantyQuantityUnitAPI);
            isNewOrChanged = true;
        }

        // warranty valid from
        String warrantyValidFromName = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_WARRANTY_VALID_FROM_NAME);
        if (warrantyValidFromName != null && !warrantyValidFromName.isEmpty()) {
            agreementPricelistRowAPI.setWarrantyValidFrom(getPreventiveMaintenanceAPI(warrantyValidFromName));
            isNewOrChanged = true;
        }

        // warranty terms
        String warrantyTerms = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_WARRANTY_TERMS_NAME);
        if (warrantyTerms != null && !warrantyTerms.isEmpty()) {
            if (CLEAR_VALUE_STRING.equals(warrantyTerms)) {
                agreementPricelistRowAPI.setWarrantyTerms(null);
            } else {
                agreementPricelistRowAPI.setWarrantyTerms(warrantyTerms);
            }
            isNewOrChanged = true;
        }

        String requestInactivate = this.importExcelHelper.getCellStringValueByName(fieldNamesRow, row, FIELD_REQUEST_INACTIVE);
        if(requestInactivate != null && !requestInactivate.isEmpty() && requestInactivate.equalsIgnoreCase("*inaktivera*")) {
            rowsToBeInactivated.add(agreementPricelistRowAPI);
            // Don't set isNewOrCahnged since it overrites the current "status" on all rows if you do, not letting it send for inactivation status.
        }

        return isNewOrChanged;
    }

    public void addRowErrorMessage(HjalpmedelstjanstenValidationException exception, int rowNumber, String field, String message) {
        String errorOnRowMessage = validationMessageService.getMessage("import.pricelist.errorOnRow", rowNumber + 1);
        exception.addValidationMessage(field, errorOnRowMessage + " " + message);
    }

    private CVGuaranteeUnitAPI getGuaranteeUnitAPI(String guaranteeUnitName, int rowNumber, HjalpmedelstjanstenValidationException exception, Map<String, CVGuaranteeUnit> guaranteeUnitNamesMap) {
        if (guaranteeUnitName != null && !guaranteeUnitName.trim().isEmpty()) {
            CVGuaranteeUnit guaranteeUnit = guaranteeUnitNamesMap.get(guaranteeUnitName);
            if (guaranteeUnit == null) {
                addRowErrorMessage(exception, rowNumber, "orderUnit", validationMessageService.getMessage("import.pricelist.guaranteeunit.notFound", guaranteeUnitName));
            } else {
                CVGuaranteeUnitAPI guaranteeUnitAPI = new CVGuaranteeUnitAPI();
                guaranteeUnitAPI.setId(guaranteeUnit.getUniqueId());
                return guaranteeUnitAPI;
            }
        }
        return null;
    }

    private CVPreventiveMaintenanceAPI getPreventiveMaintenanceAPI(String preventiveMaintenanceName) {
        if (preventiveMaintenanceName != null && !preventiveMaintenanceName.trim().isEmpty()) {
            CVPreventiveMaintenance preventiveMaintenance = preventiveMaintenanceUnitController.findByName(preventiveMaintenanceName);
            CVPreventiveMaintenanceAPI preventiveMaintenanceAPI = new CVPreventiveMaintenanceAPI();
            preventiveMaintenanceAPI.setId(preventiveMaintenance.getUniqueId());
            preventiveMaintenanceAPI.setCode(preventiveMaintenance.getCode());
            return preventiveMaintenanceAPI;
        }
        return null;
    }
}

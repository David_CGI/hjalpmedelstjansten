/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.agreement.view;

import java.util.List;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementPricelistController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Product;

/**
 * REST API for agreement pricelists.
 * 
 * @author Tommy Berglund
 */
@Stateless
@Path("organizations/{organizationUniqueId}/agreements/{agreementUniqueId}/pricelists")
@Interceptors({ PerformanceLogInterceptor.class })
public class AgreementPricelistService extends BaseService {
    
    @Inject
    private HjmtLogger LOG;
    
    @Inject
    AgreementPricelistController agreementPricelistController;
    
    @EJB
    AuthHandler authHandler;
    
    /**
     * Get all pricelists on a specific agreement
     * 
     * @param organizationUniqueId the unique id of the organization to get agreement from
     * @param agreementUniqueId the id of the agreement
     * @return the corresponding List of <code>PricelistAPI</code>
     */
    @GET
    @SecuredService(permissions = {"pricelist:view_own"})
    public Response getPricelistsOnAgreement(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("agreementUniqueId") long agreementUniqueId ) {
        LOG.log(Level.FINEST, "getPricelistsOnAgreement( organizationUniqueId: {0}, agreementUniqueId: {1} )", new Object[] {organizationUniqueId, agreementUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        List<AgreementPricelistAPI> pricelistAPIs = agreementPricelistController.getPricelistAPIsOnAgreement( organizationUniqueId, agreementUniqueId, userAPI );
        if( pricelistAPIs == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(pricelistAPIs).build();
        }
    }
    
    /**
     * Get information about a specific pricelist 
     * 
     * @param organizationUniqueId the unique id of the organization to get agreement from
     * @param agreementUniqueId the id of the agreement
     * @param pricelistUniqueId the id of the pricelist
     * @return the corresponding <code>PricelistAPI</code>
     */
    @GET
    @Path("{pricelistUniqueId}")
    @SecuredService(permissions = {"pricelist:view_own"})
    public Response getPricelist(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("agreementUniqueId") long agreementUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId ) {
        LOG.log(Level.FINEST, "getPricelist( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        AgreementPricelistAPI pricelistAPI = agreementPricelistController.getPricelistAPI( organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest) );
        if( pricelistAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(pricelistAPI).build();
        }
    }
    
    /**
     * Create new pricelist 
     * 
     * @param organizationUniqueId unique id of the organization to create pricelist on
     * @param agreementUniqueId unique id of the agreement to create the pricelist on
     * @param pricelistAPI user supplied values
     * @return the created <code>PricelistAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @SecuredService(permissions = {"pricelist:create_own"})
    public Response createPricelist(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("agreementUniqueId") long agreementUniqueId,
            AgreementPricelistAPI pricelistAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createPricelist( organizationUniqueId: {0}, agreementUniqueId: {1} )", new Object[] {organizationUniqueId, agreementUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        pricelistAPI = agreementPricelistController.createPricelist(organizationUniqueId, agreementUniqueId, pricelistAPI, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( pricelistAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(pricelistAPI).build();
    }
    
    /**
     * Update pricelist. Updatable fields are name and validFrom. 
     * 
     * @param organizationUniqueId unique id of the organization to update pricelist on
     * @param agreementUniqueId unique id of the agreement to update the pricelist on
     * @param pricelistUniqueId unique id of the pricelist to update
     * @param pricelistAPI user supplied values
     * @return the updated <code>PricelistAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     */
    @PUT
    @Path("{pricelistUniqueId}")
    @SecuredService(permissions = {"pricelist:update_own"})
    public Response updatePricelist(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("agreementUniqueId") long agreementUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId,
            AgreementPricelistAPI pricelistAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updatePricelist( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        pricelistAPI = agreementPricelistController.updatePricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistAPI, userAPI, authHandler.getSessionId());
        if( pricelistAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        return Response.ok(pricelistAPI).build();
    }
    
    /**
     * Search articles available to add on a specific pricelist 
     * 
     * @param organizationUniqueId the unique id of the organization to get agreement from
     * @param agreementUniqueId the id of the agreement
     * @param pricelistUniqueId the id of the pricelist
     * @param query
     * @param offset
     * @param statuses
     * @param articleTypes
     * @return the corresponding <code>PricelistAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException
     */
    @GET
    @Path("{pricelistUniqueId}/articles")
    @SecuredService(permissions = {"pricelist:view_own"})
    public Response searchArticlesForPricelist(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("agreementUniqueId") long agreementUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId,
            @DefaultValue(value = "") @QueryParam("query") String query,
            @DefaultValue(value = "0") @QueryParam("offset") int offset,
            @QueryParam("status") List<Product.Status> statuses, 
            @QueryParam("type") List<Article.Type> articleTypes) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "searchArticlesForPricelist( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        SearchDTO searchDTO = agreementPricelistController.searchArticlesForAgreementPricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, query, statuses, articleTypes, offset, 25, userAPI);
        if( searchDTO == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(searchDTO.getItems()).
                header("X-Total-Count", searchDTO.getCount()).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                build();
        }
    }
    
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.user.controller;

import java.util.ArrayList;
import java.util.List;
import se.inera.hjalpmedelstjansten.model.api.RoleAPI;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;

/**
 *
 * @author Tommy Berglund
 */
public class RoleMapper {

    public static List<RoleAPI> map(List<UserRole> userRoles) {
        if( userRoles == null ) {
            return null;
        }
        List<RoleAPI> roleAPIs = new ArrayList<>();
        for( UserRole userRole : userRoles ) {
            roleAPIs.add(map(userRole));
        }
        return roleAPIs;
    }

    public static RoleAPI map(UserRole userRole) {
        if( userRole == null ) {
            return null;
        }
        RoleAPI roleAPI = new RoleAPI();
        roleAPI.setId(userRole.getUniqueId());
        roleAPI.setName(userRole.getName().toString());
        roleAPI.setDescription(userRole.getDescription());
        return roleAPI;
    }
    
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.agreement.controller;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.entity.Agreement;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelist;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelistRow;

/**
 * Validation methods for agreement pricelist row
 * 
 * @author Tommy Berglund
 */
@Stateless
public class AgreementPricelistRowValidation {
    
    @Inject
    HjmtLogger LOG;
    
    @Inject
    ValidationMessageService validationMessageService;
    
    public Set<ErrorMessageAPI> tryForCreate(AgreementPricelistRowAPI pricelistRowAPI, AgreementPricelist pricelist, AgreementPricelist currentPricelist) {
        LOG.log( Level.FINEST, "tryForCreate(...)" );
        Set<ErrorMessageAPI> errorMessageAPIs = new HashSet<>();
        
        if( pricelist.getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("status", validationMessageService.getMessage("agreement.status.discontinued")));
        }
        
        AgreementPricelist.Status pricelistStatus = AgreementPricelistMapper.getPricelistStatus(pricelist, currentPricelist);
        if( pricelistStatus == AgreementPricelist.Status.PAST ) {
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("status", validationMessageService.getMessage("pricelist.status.past")));
        }
        
        validatePricelistRowAPI(pricelistRowAPI, errorMessageAPIs);
        
        // validation makes sure status is set and valid
        AgreementPricelistRow.Status status = AgreementPricelistRow.Status.valueOf(pricelistRowAPI.getStatus());
        if( status != AgreementPricelistRow.Status.CREATED ) {
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("status", validationMessageService.getMessage("pricelistrow.status.invalidCreate")));
        }
        return errorMessageAPIs;
    }
    
    public void validateForCreate(AgreementPricelistRowAPI pricelistRowAPI, AgreementPricelist pricelist, AgreementPricelist currentPricelist) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForCreate(...)" );
        Set<ErrorMessageAPI> errorMessageAPIs = tryForCreate(pricelistRowAPI, pricelist, currentPricelist);
        
        if( !errorMessageAPIs.isEmpty() ) {
            HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
            exception.addValidationMessages(errorMessageAPIs);
            throw exception;
        }
    }

    public Set<ErrorMessageAPI> tryForUpdate(AgreementPricelistRowAPI pricelistRowAPI, AgreementPricelistRow pricelistRow, AgreementPricelist currentPricelist) {
        LOG.log( Level.FINEST, "tryForUpdate(...)" );
        
        Set<ErrorMessageAPI> errorMessageAPIs = new HashSet<>();
        
        // only possible to update rows with status active, created, changed and declined
        if( pricelistRow.getStatus() != AgreementPricelistRow.Status.CREATED &&
                pricelistRow.getStatus() != AgreementPricelistRow.Status.DECLINED &&
                pricelistRow.getStatus() != AgreementPricelistRow.Status.ACTIVE &&
                pricelistRow.getStatus() != AgreementPricelistRow.Status.CHANGED) {
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("status", validationMessageService.getMessage("pricelistrow.update.wrongStatus")));
        }
        
        if( pricelistRow.getPricelist().getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("status", validationMessageService.getMessage("agreement.status.discontinued")));
        } 

        AgreementPricelist.Status pricelistStatus = AgreementPricelistMapper.getPricelistStatus(pricelistRow.getPricelist(), currentPricelist);
        if( pricelistStatus == AgreementPricelist.Status.PAST ) {
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("status", validationMessageService.getMessage("pricelist.status.past")));
        }
        
        validatePricelistRowAPI(pricelistRowAPI, errorMessageAPIs);
        
        // on update, deliverytime may not be null
        if( pricelistRowAPI.getDeliveryTime() == null ) {
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("status", validationMessageService.getMessage("pricelistrow.deliveryTime.notNull")));
        }
        
        return errorMessageAPIs;
    }
    
    public void validateForUpdate(AgreementPricelistRowAPI pricelistRowAPI, AgreementPricelistRow pricelistRow, AgreementPricelist currentPricelist) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForUpdate(...)" );
        Set<ErrorMessageAPI> errorMessageAPIs = tryForUpdate(pricelistRowAPI, pricelistRow, currentPricelist);
        
        if( !errorMessageAPIs.isEmpty() ) {
            HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
            exception.addValidationMessages(errorMessageAPIs);
            throw exception;
        }
    }
    
    private void validatePricelistRowAPI(AgreementPricelistRowAPI pricelistRowAPI, Set<ErrorMessageAPI> errorMessageAPIs) {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<AgreementPricelistRowAPI>> constraintViolations = validator.validate(pricelistRowAPI, Default.class);
        if( constraintViolations != null && !constraintViolations.isEmpty() ) {
            HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
            for( ConstraintViolation constraintViolation : constraintViolations ) {
                errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage()));
            }
        }
    }
    
}

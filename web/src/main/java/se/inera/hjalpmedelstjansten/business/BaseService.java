/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business;

import java.util.logging.Level;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;

/**
 * Base class for JAX-RS services 
 * 
 * @author tomber
 */
public class BaseService {
        
    @Inject
    private HjmtLogger LOG;
    
    /**
     * Authorize whether user can handle this organization
     * 
     * @param organizationUniqueId unique id of organization
     * @param userAPI
     * @return true if handle is ok, otherwise false
     */
    protected boolean authorizeHandleOrganization(long organizationUniqueId, UserAPI userAPI) {
        LOG.log( Level.FINEST, "authorizeHandleOrganization( organizationUniqueId: {0} )", new Object[] {organizationUniqueId} );        
        if( UserController.getUserEngagementAPIByOrganizationId( organizationUniqueId, userAPI.getUserEngagements()) == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to handle organization not connected to", new Object[] {userAPI.getId()} );
            return false;
        }
        return true;
    }
    
    protected String getRequestIp(HttpServletRequest httpServletRequest) {
        // return httpServletRequest.getRemoteAddr();
        return httpServletRequest.getHeader("X-FORWARDED-FOR");
        // return null;
    }
    
}

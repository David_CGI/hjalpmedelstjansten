/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.agreement.controller;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;
import se.inera.hjalpmedelstjansten.business.DateUtils;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.api.AgreementAPI;
import se.inera.hjalpmedelstjansten.model.api.ErrorMessageAPI;
import se.inera.hjalpmedelstjansten.model.entity.Agreement;

/**
 * Validation methods for agreements
 * 
 * @author Tommy Berglund
 */
@Stateless
public class AgreementValidation {
    
    @Inject
    HjmtLogger LOG;
    
    @Inject
    ValidationMessageService validationMessageService;
    
    public void validateForCreate(AgreementAPI agreementAPI, long organizationUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForCreate(...)" );
        Set<ErrorMessageAPI> errorMessageAPIs = tryForCreate(organizationUniqueId, agreementAPI);
        if( !errorMessageAPIs.isEmpty() ) {
            HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
            exception.addValidationMessages(errorMessageAPIs);
            throw exception;
        }
    }
    
    public void validateForUpdate(AgreementAPI agreementAPI, Agreement agreement, long organizationUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForUpdate(...)" );
        Set<ErrorMessageAPI> errorMessageAPIs = tryForUpdate(organizationUniqueId, agreementAPI, agreement);
        if( !errorMessageAPIs.isEmpty() ) {
            HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
            exception.addValidationMessages(errorMessageAPIs);
            throw exception;
        }
    }
    
    /**
     * Generates a Set of <code>ErrorMessageAPI</code> in case the given agreementAPI
     * does not validate entirely for create
     * 
     * @param organizationUniqueId
     * @param agreementAPI
     * @return 
     */
    public Set<ErrorMessageAPI> tryForCreate(long organizationUniqueId, AgreementAPI agreementAPI) {
        Set<ErrorMessageAPI> errorMessageAPIs = new HashSet<>();
        validateAgreementAPI(agreementAPI, errorMessageAPIs);
        if( !errorMessageAPIs.isEmpty() ) {
            return errorMessageAPIs;
        }
        if( !validFromDate(agreementAPI.getValidFrom()) ) {
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("validFrom", validationMessageService.getMessage("agreement.validFrom.notFuture")));
        }
        return errorMessageAPIs;
    }
    
    /**
     * Generates a Set of <code>ErrorMessageAPI</code> in case the given agreementAPI
     * does not validate entirely for update
     * 
     * @param organizationUniqueId
     * @param agreementAPI
     * @return 
     */
    public Set<ErrorMessageAPI> tryForUpdate(long organizationUniqueId, AgreementAPI agreementAPI, Agreement agreement) {
        Set<ErrorMessageAPI> errorMessageAPIs = new HashSet<>();
        validateAgreementAPI(agreementAPI, errorMessageAPIs);
        if( !errorMessageAPIs.isEmpty() ) {
            return errorMessageAPIs;
        }
        //if( agreement.getStatus() == Agreement.Status.DISCONTINUED ) {
        //    errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("status", validationMessageService.getMessage("agreement.status.discontinued")));
        //}
        if( !DateUtils.isSameDay(agreementAPI.getValidFrom(), agreement.getValidFrom().getTime()) && 
                !validFromDate(agreementAPI.getValidFrom()) ) {
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("validFrom", validationMessageService.getMessage("agreement.validFrom.notFuture")));
        }
        return errorMessageAPIs;
    }
    
    private void validateAgreementAPI(AgreementAPI agreementAPI, Set<ErrorMessageAPI> errorMessageAPIs) {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<AgreementAPI>> constraintViolations = validator.validate(agreementAPI, Default.class);
        if( constraintViolations != null && !constraintViolations.isEmpty() ) {
            for( ConstraintViolation constraintViolation : constraintViolations ) {
                errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage()));
            }
        }
    }
    
    private boolean validFromDate(Long dateAsMillis) {
        // valid from must be today or a future date when creating or updating validFrom
        Instant nowInstant = Instant.now();
        ZoneId zoneId = ZoneId.systemDefault();
        ZonedDateTime todayStartOfDay = ZonedDateTime.ofInstant(nowInstant, zoneId).toLocalDate().atStartOfDay(zoneId);
        Instant validFromInstant = Instant.ofEpochMilli(dateAsMillis);
        ZonedDateTime validFrom = ZonedDateTime.ofInstant(validFromInstant, zoneId);
        if( validFrom.isBefore(todayStartOfDay) ) {
            return false;
        }
        return true;
    }

}

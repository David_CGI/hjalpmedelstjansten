/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.organization.controller;

import java.util.ArrayList;
import java.util.List;
import se.inera.hjalpmedelstjansten.model.api.BusinessLevelAPI;
import se.inera.hjalpmedelstjansten.model.entity.BusinessLevel;
import se.inera.hjalpmedelstjansten.model.entity.Organization;

/**
 *
 * @author Tommy Berglund
 */
public class BusinessLevelMapper {
    
    public static final List<BusinessLevelAPI> map(List<BusinessLevel> businessLevels, boolean includeAll) {
        if( businessLevels == null ) {
            return null;
        }
        List<BusinessLevelAPI> businessLevelAPIs = new ArrayList<>();
        for( BusinessLevel businessLevel : businessLevels ) {
            businessLevelAPIs.add(map(businessLevel, includeAll));
        }
        return businessLevelAPIs;
    }
    
    public static final BusinessLevelAPI map(BusinessLevel businessLevel, boolean includeAll) {
        if( businessLevel == null ) {
            return null;
        }
        BusinessLevelAPI businessLevelAPI = new BusinessLevelAPI();
        businessLevelAPI.setId(businessLevel.getUniqueId());
        businessLevelAPI.setName(businessLevel.getName());
        businessLevelAPI.setStatus(businessLevel.getStatus().toString());
        if( includeAll ) {
            businessLevelAPI.setOrganization(OrganizationMapper.map(businessLevel.getOrganization(), false));
        }
        return businessLevelAPI;
    }

    public static final BusinessLevel map(BusinessLevelAPI businessLevelAPI, Organization organization, BusinessLevel.Status status) {
        if( businessLevelAPI == null ) {
            return null;
        }
        BusinessLevel businessLevel = new BusinessLevel();
        businessLevel.setName(businessLevelAPI.getName());
        businessLevel.setOrganization(organization);
        businessLevel.setStatus(status);
        return businessLevel;
    }
    
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.product.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.LoggerService;
import se.inera.hjalpmedelstjansten.model.api.cv.CVPackageUnitAPI;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPackageUnit;

/**
 *
 * @author Tommy Berglund
 */
public class PackageUnitMapper {
    
    private static final HjmtLogger LOG = LoggerService.getLogger(PackageUnitMapper.class.getName());
    
    public static final List<CVPackageUnitAPI> map(List<CVPackageUnit> packageUnits) {
        if( packageUnits == null ) {
            return null;
        }
        List<CVPackageUnitAPI> packageUnitAPIs = new ArrayList<>();
        for( CVPackageUnit packageUnit : packageUnits ) {
            packageUnitAPIs.add(map(packageUnit));
        }
        return packageUnitAPIs;
    }
    
    public static final CVPackageUnitAPI map(CVPackageUnit packageUnit) {
        LOG.log( Level.FINEST, "map(...)" );
        if( packageUnit == null ) {
            return null;
        }
        CVPackageUnitAPI packageUnitAPI = new CVPackageUnitAPI();
        packageUnitAPI.setId(packageUnit.getUniqueId());
        packageUnitAPI.setCode(packageUnit.getCode());
        packageUnitAPI.setName(packageUnit.getName());
        return packageUnitAPI;
    }
    
}

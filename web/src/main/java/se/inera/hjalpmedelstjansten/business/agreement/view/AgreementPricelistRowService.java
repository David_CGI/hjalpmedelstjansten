/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.agreement.view;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenException;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementPricelistRowController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.business.ResultExporterController;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelistRow;
import se.inera.hjalpmedelstjansten.model.entity.Article;

/**
 * REST API for agreement pricelist rows.
 * 
 * @author Tommy Berglund
 */
@Stateless
@Path("organizations/{organizationUniqueId}/agreements/{agreementUniqueId}/pricelists/{pricelistUniqueId}/pricelistrows")
@Interceptors({ PerformanceLogInterceptor.class })
public class AgreementPricelistRowService extends BaseService {
    
    @Inject
    private HjmtLogger LOG;
    
    @Inject
    AgreementPricelistRowController pricelistRowController;
    
    @Inject
    ResultExporterController resultExporterController;
    
    @Inject
    AuthHandler authHandler;
        
    /**
     * Search pricelist rows by user query
     * 
     * @param query the user query
     * @param organizationUniqueId the unique id of the organization
     * @param agreementUniqueId the unique id of the agreement
     * @param pricelistUniqueId the uniqueid of the pricelist
     * @param offset for pagination purpose
     * @param statuses list of statuses to include in search, if null or empty, 
     * all statuses are included
     * @param articleTypes list of article types to include in search, if null or empty, 
     * all article types are included
     * @param showRowsWithPrices include rows where price is not null, if neither this
     * not showRowsWithoutPrices is set, then all rows are included
     * @param showRowsWithoutPrices include rows where price is null, if neither this
     * not showRowsWithPrices is set, then all rows are included
     * @param categoryId
     * @return a list of <code>AgreementPricelistRowAPI</code> matching the query and a 
     * header X-Total-Count giving the total number of agreements for pagination
     */
    @GET
    @SecuredService(permissions = {"pricelistrow:view_own"})
    public Response searchPricelistRows(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("agreementUniqueId") long agreementUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId,
            @DefaultValue(value = "") @QueryParam("query") String query, 
            @DefaultValue(value = "0") @QueryParam("offset") int offset,
            @QueryParam("status") List<AgreementPricelistRow.Status> statuses,
            @QueryParam("type") List<Article.Type> articleTypes,
            @QueryParam("showRowsWithPrices") Boolean showRowsWithPrices,
            @QueryParam("showRowsWithoutPrices") Boolean showRowsWithoutPrices,
            @QueryParam("category") Long categoryId) {
        LOG.log(Level.FINEST, "searchPricelistRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2}, showRowsWithPrices: {3}, showRowsWithoutPrices: {4} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId, showRowsWithPrices, showRowsWithoutPrices});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        SearchDTO searchDTO = pricelistRowController.searchPricelistRows(query, organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI, offset, 25, statuses, articleTypes, showRowsWithPrices, showRowsWithoutPrices, categoryId);
        if( searchDTO == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(searchDTO.getItems()).
                    header("X-Total-Count", searchDTO.getCount()).
                    header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                    build();
        }
    }
    
    /**
     * Export search result list to file. Same permission required as
     * searching. 
     * 
     * @param organizationUniqueId
     * @param agreementUniqueId
     * @param pricelistUniqueId
     * @param query
     * @param statuses
     * @param articleTypes
     * @param showRowsWithPrices
     * @param showRowsWithoutPrices
     * @param categoryId
     * @return
     */
    @GET
    @Path("export")
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=UTF-8")
    @SecuredService(permissions = {"pricelistrow:view_own"})
    @TransactionTimeout(value=20, unit = TimeUnit.MINUTES)
    public Response exportPricelistRows(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("agreementUniqueId") long agreementUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId,
            @DefaultValue(value = "") @QueryParam("query") String query,             
            @QueryParam("status") List<AgreementPricelistRow.Status> statuses,
            @QueryParam("type") List<Article.Type> articleTypes,
            @QueryParam("showRowsWithPrices") Boolean showRowsWithPrices,
            @QueryParam("showRowsWithoutPrices") Boolean showRowsWithoutPrices, 
            @QueryParam("category") Long categoryId) {
        LOG.log(Level.FINEST, "exportPricelistRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2}, showRowsWithPrices: {3}, showRowsWithoutPrices: {4} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId, showRowsWithPrices, showRowsWithoutPrices});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        SearchDTO searchDTO = pricelistRowController.searchPricelistRows(query, organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI, 0, Integer.MAX_VALUE, statuses, articleTypes, showRowsWithPrices, showRowsWithoutPrices, categoryId);
        if( searchDTO == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            try {
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'_'HHmm");
                String filename = "Export_Prislista_" + dateTimeFormatter.format(ZonedDateTime.now()) + ".xlsx";
                byte[] exportBytes = resultExporterController.generateAgreementPricelistRowResultList(searchDTO, organizationUniqueId, agreementUniqueId, userAPI);
                return Response.ok(
                        exportBytes,
                        javax.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM)
                        .header("content-disposition", "attachment; filename = " + filename)
                        .build();
            } catch (IOException ex) {
                LOG.log(Level.WARNING, "Failed to export pricelist rows search to file", ex);
                return Response.serverError().build();
            }
        }
    }
    
    /**
     * Get information about a specific pricelist row
     * 
     * @param httpServletRequest
     * @param organizationUniqueId the unique id of the organization to get agreement from
     * @param agreementUniqueId the id of the agreement
     * @param pricelistUniqueId the id of the pricelist
     * @param pricelistRowUniqueId unique id of the row
     * @return the corresponding <code>PricelistAPI</code>
     */
    @GET
    @Path("{pricelistRowUniqueId : \\d+}")
    @SecuredService(permissions = {"pricelistrow:view_own"})
    public Response getPricelistRow(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("agreementUniqueId") long agreementUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId,
            @PathParam("pricelistRowUniqueId") long pricelistRowUniqueId) {
        LOG.log(Level.FINEST, "getPricelist( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2}, pricelistRowUniqueId: {3} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        AgreementPricelistRowAPI pricelistRowAPI = pricelistRowController.getPricelistRowAPI( organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowUniqueId, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest) );
        if( pricelistRowAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(pricelistRowAPI).build();
        }
    }
    
    /**
     * Create new pricelist row
     * 
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement 
     * @param pricelistUniqueId unique id of the pricelist  
     * @param pricelistRowAPIs user supplied values
     * @return the created <code>PricelistRowAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validations fails
     */
    @POST
    @SecuredService(permissions = {"pricelistrow:create_own"})
    public Response createPricelistRow(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("agreementUniqueId") long agreementUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId,
            List<AgreementPricelistRowAPI> pricelistRowAPIs) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createPricelistRow( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        pricelistRowAPIs = pricelistRowController.createPricelistRows(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPIs, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        return Response.ok(pricelistRowAPIs).build();
    }

    /**
     * Delete supplied row. Supplier deletes a row in the agreement price list
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement
     * @param pricelistUniqueId user supplied unique id
     * @param pricelistRowUniqueId user supplied unique id
     * @return new list
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validations fails
     */
    @DELETE
    @Path("{pricelistRowUniqueId}/delete")
    @SecuredService(permissions = {"pricelistrow:create_own"})
    public Response deleteRow(@Context HttpServletRequest httpServletRequest,
                              @PathParam("organizationUniqueId") long organizationUniqueId,
                              @PathParam("agreementUniqueId") long agreementUniqueId,
                              @PathParam("pricelistUniqueId") long pricelistUniqueId,
                              @PathParam("pricelistRowUniqueId") long pricelistRowUniqueId
    ) throws HjalpmedelstjanstenValidationException {

        LOG.log(Level.FINEST, "Deleterow( organizationUniqueId: {0}, pricelistRowUniqueId: {1}, agreementUniqueId: {2} )", new Object[] {organizationUniqueId, pricelistRowUniqueId, agreementUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        AgreementPricelistRowAPI pricelistRowAPI = pricelistRowController.deleteRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowUniqueId, userAPI, authHandler.getSessionId(),getRequestIp(httpServletRequest));

        if( pricelistRowAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(pricelistRowAPI).build();
        }
    }


    /**
     * Update a pricelist row
     * 
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement 
     * @param pricelistUniqueId unique id of the pricelist  
     * @param pricelistRowUniqueId unique id of the row
     * @param pricelistRowAPI user supplied values
     * @return the created <code>PricelistRowAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validations fails
     */
    @PUT
    @Path("{pricelistRowUniqueId}")
    @SecuredService(permissions = {"pricelistrow:update_own"})
    public Response updatePricelistRow(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("agreementUniqueId") long agreementUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId,
            @PathParam("pricelistRowUniqueId") long pricelistRowUniqueId,
            AgreementPricelistRowAPI pricelistRowAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updatePricelistRow( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        pricelistRowAPI = pricelistRowController.updatePricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowUniqueId, pricelistRowAPI, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( pricelistRowAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(pricelistRowAPI).build();
        }
    }
    
    /**
     * Send supplied rows for customer approval. Supplier sends rows for approval
     * to the customer.
     * 
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement 
     * @param pricelistUniqueId unique id of the pricelist  
     * @param pricelistRowAPIs user supplied rows
     * @return the created <code>PricelistRowAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validations fails
     */
    @POST
    @Path("sendForApproval")
    @SecuredService(permissions = {"pricelistrow:sendForCustomerApproval"})
    public Response sendRowsForCustomerApproval(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("agreementUniqueId") long agreementUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId,
            List<AgreementPricelistRowAPI> pricelistRowAPIs) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "sendRowsForCustomerApproval( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        pricelistRowAPIs = pricelistRowController.sendRowsForCustomerApproval(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPIs, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( pricelistRowAPIs == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(pricelistRowAPIs).build();
        }
    }

    /**
     * Send supplied rows for customer approval. Supplier sends rows for approval
     * to the customer. Including comment.
     *
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowAPIs user supplied rows
     * @return the created <code>PricelistRowAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validations fails
     */
    @POST
    @Path("sendForApproval/{comment}")
    @SecuredService(permissions = {"pricelistrow:sendForCustomerApproval"})
    public Response sendRowsAndCommentForCustomerApproval(@Context HttpServletRequest httpServletRequest,
                                                @PathParam("organizationUniqueId") long organizationUniqueId,
                                                @PathParam("agreementUniqueId") long agreementUniqueId,
                                                @PathParam("pricelistUniqueId") long pricelistUniqueId,
                                                @PathParam("comment") String comment,
                                                List<AgreementPricelistRowAPI> pricelistRowAPIs) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "sendRowsAndCommentForCustomerApproval( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2}, comment: {3} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId, comment});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        pricelistRowAPIs = pricelistRowController.sendRowsAndCommentForCustomerApproval(comment, organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPIs, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( pricelistRowAPIs == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(pricelistRowAPIs).build();
        }
    }

    /**
     * Send all rows for customer approval. Supplier sends rows for approval
     * to the customer.
     * 
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement 
     * @param pricelistUniqueId unique id of the pricelist  
     * @return the created <code>PricelistRowAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validations fails
     */
    @POST
    @Path("sendAllForApproval")
    @SecuredService(permissions = {"pricelistrow:sendForCustomerApproval"})
    public Response sendAllRowsForCustomerApproval(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("agreementUniqueId") long agreementUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "sendAllRowsForCustomerApproval( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        List<AgreementPricelistRowAPI> pricelistRowAPIs = pricelistRowController.sendAllRowsForCustomerApproval(organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( pricelistRowAPIs == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok().build();
        }
    }

    /**
     * Send all rows for customer approval. Supplier sends rows for approval
     * to the customer. Including comment.
     *
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement
     * @param pricelistUniqueId unique id of the pricelist
     * @return the created <code>PricelistRowAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validations fails
     */
    @POST
    @Path("sendAllForApproval/{comment}")
    @SecuredService(permissions = {"pricelistrow:sendForCustomerApproval"})
    public Response sendAllRowsAndCommentForCustomerApproval(@Context HttpServletRequest httpServletRequest,
                                                   @PathParam("organizationUniqueId") long organizationUniqueId,
                                                   @PathParam("agreementUniqueId") long agreementUniqueId,
                                                   @PathParam("pricelistUniqueId") long pricelistUniqueId,
                                                   @PathParam("comment") String comment) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "sendAllRowsAndCommentForCustomerApproval( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        List<AgreementPricelistRowAPI> pricelistRowAPIs = pricelistRowController.sendAllRowsAndCommentForCustomerApproval(comment, organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( pricelistRowAPIs == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok().build();
        }
    }


    /**
     * Decline supplied rows. Customer declines rows which are returned to the 
     * supplier for edit. 
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement 
     * @param pricelistUniqueId unique id of the pricelist  
     * @param pricelistRowAPIs user supplied rows
     * @return the created <code>PricelistRowAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validations fails
     */
    @POST
    @Path("decline")
    @SecuredService(permissions = {"pricelistrow:decline"})
    public Response desclineRows(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("agreementUniqueId") long agreementUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId,
            List<AgreementPricelistRowAPI> pricelistRowAPIs) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "desclineRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        pricelistRowAPIs = pricelistRowController.declineRows(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPIs, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( pricelistRowAPIs == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(pricelistRowAPIs).build();
        }
    }

    /**
     * Decline supplied rows. Customer declines rows which are returned to the
     * supplier for edit. Including comment.
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowAPIs user supplied rows
     * @return the created <code>PricelistRowAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validations fails
     */
    @POST
    @Path("decline/{comment}")
    @SecuredService(permissions = {"pricelistrow:decline"})
    public Response declineRowsWithComment(@Context HttpServletRequest httpServletRequest,
                                 @PathParam("organizationUniqueId") long organizationUniqueId,
                                 @PathParam("agreementUniqueId") long agreementUniqueId,
                                 @PathParam("pricelistUniqueId") long pricelistUniqueId,
                                 @PathParam("comment") String comment,
                                 List<AgreementPricelistRowAPI> pricelistRowAPIs) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "declineRowsWithComment( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2}, comment: {3} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId, comment});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        pricelistRowAPIs = pricelistRowController.declineRowsWithComment(comment, organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPIs, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( pricelistRowAPIs == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(pricelistRowAPIs).build();
        }
    }

    /**
     * Activate supplied rows. The customer activates rows. Activating rows means
     * that the price is displayed when searching for articles (for those with the
     * right roles) and that the rows are included in the export.
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowAPIs user supplied rows
     * @return the created <code>PricelistRowAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validations fails
     */
    @POST
    @Path("activate/{comment}")
    @SecuredService(permissions = {"pricelistrow:activate"})
    public Response activateRowsWithComment(@Context HttpServletRequest httpServletRequest,
                                 @PathParam("organizationUniqueId") long organizationUniqueId,
                                 @PathParam("agreementUniqueId") long agreementUniqueId,
                                 @PathParam("pricelistUniqueId") long pricelistUniqueId,
                                 @PathParam("comment") String comment,
                                 List<AgreementPricelistRowAPI> pricelistRowAPIs) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "activateRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        pricelistRowAPIs = pricelistRowController.activateRowsWithComment(comment, organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPIs, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( pricelistRowAPIs == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(pricelistRowAPIs).build();
        }
    }

    /**
     * Activate supplied rows. The customer activates rows. Activating rows means
     * that the price is displayed when searching for articles (for those with the
     * right roles) and that the rows are included in the export. 
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement 
     * @param pricelistUniqueId unique id of the pricelist  
     * @param pricelistRowAPIs user supplied rows
     * @return the created <code>PricelistRowAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validations fails
     */
    @POST
    @Path("activate")
    @SecuredService(permissions = {"pricelistrow:activate"})
    public Response activateRows(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("agreementUniqueId") long agreementUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId,
            List<AgreementPricelistRowAPI> pricelistRowAPIs) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "activateRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        pricelistRowAPIs = pricelistRowController.activateRows(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPIs, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( pricelistRowAPIs == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(pricelistRowAPIs).build();
        }
    }

    /**
     * Activate all rows. The customer activates rows. Activating rows means
     * that the price is displayed when searching for articles (for those with the
     * right roles) and that the rows are included in the export.
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement
     * @param pricelistUniqueId unique id of the pricelist
     * @return the created <code>PricelistRowAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validations fails
     */
    @POST
    @Path("activateall/{comment}")
    @SecuredService(permissions = {"pricelistrow:activate"})
    public Response activateAllRowsWithComment(@Context HttpServletRequest httpServletRequest,
                                    @PathParam("organizationUniqueId") long organizationUniqueId,
                                    @PathParam("agreementUniqueId") long agreementUniqueId,
                                    @PathParam("pricelistUniqueId") long pricelistUniqueId,
                                    @PathParam("comment") String comment) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "activateAllRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        List<AgreementPricelistRowAPI> pricelistRowAPIs = pricelistRowController.activateAllRowsWithComment(comment, organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( pricelistRowAPIs == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok().build();
        }
    }

    /**
     * Activate all rows. The customer activates rows. Activating rows means
     * that the price is displayed when searching for articles (for those with the
     * right roles) and that the rows are included in the export. 
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement 
     * @param pricelistUniqueId unique id of the pricelist  
     * @return the created <code>PricelistRowAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validations fails
     */
    @POST
    @Path("activateall")
    @SecuredService(permissions = {"pricelistrow:activate"})
    public Response activateAllRows(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("agreementUniqueId") long agreementUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "activateAllRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        List<AgreementPricelistRowAPI> pricelistRowAPIs = pricelistRowController.activateAllRows(organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( pricelistRowAPIs == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok().build();
        }
    }
    
    /**
     * Inactivate supplied rows. Supplier inactivates a row. This must be
     * approved by the customer
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement 
     * @param pricelistUniqueId unique id of the pricelist  
     * @param pricelistRowAPIs user supplied rows
     * @return the created <code>PricelistRowAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validations fails
     */
    @POST
    @Path("inactivate")
    @SecuredService(permissions = {"pricelistrow:inactivate"})
    public Response inactivateRows(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("agreementUniqueId") long agreementUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId,
            List<AgreementPricelistRowAPI> pricelistRowAPIs) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "inactivateRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        pricelistRowAPIs = pricelistRowController.inactivateRows(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPIs, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( pricelistRowAPIs == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(pricelistRowAPIs).build();
        }
    }    
    
    /**
     * Approve inactivated rows. Customer approves supplier inactivation of rows.
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement 
     * @param pricelistUniqueId unique id of the pricelist  
     * @param pricelistRowAPIs user supplied rows
     * @return the created <code>PricelistRowAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validations fails
     */
    @POST
    @Path("approveinactivate")
    @SecuredService(permissions = {"pricelistrow:approve_inactivate"})
    public Response approveInactivateRows(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("agreementUniqueId") long agreementUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId,
            List<AgreementPricelistRowAPI> pricelistRowAPIs) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "approveInactivateRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        pricelistRowAPIs = pricelistRowController.approveInactivateRows(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPIs, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( pricelistRowAPIs == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(pricelistRowAPIs).build();
        }
    }

    @POST
    @Path("approveinactivate/{comment}")
    @SecuredService(permissions = {"pricelistrow:approve_inactivate"})
    public Response approveInactivateRowsComment(@Context HttpServletRequest httpServletRequest,
                                          @PathParam("organizationUniqueId") long organizationUniqueId,
                                          @PathParam("agreementUniqueId") long agreementUniqueId,
                                          @PathParam("pricelistUniqueId") long pricelistUniqueId,
                                          List<AgreementPricelistRowAPI> pricelistRowAPIs,
                                          @PathParam("comment") String comment) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "approveInactivateRowsComment( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        pricelistRowAPIs = pricelistRowController.approveInactivateRowsComment(comment, organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPIs, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( pricelistRowAPIs == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(pricelistRowAPIs).build();
        }
    }

    /**
     * Decline request to inactivate rows. Customer declines supplier inactivation of rows.
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement 
     * @param pricelistUniqueId unique id of the pricelist  
     * @param pricelistRowAPIs user supplied rows
     * @return the created <code>PricelistRowAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validations fails
     */
    @POST
    @Path("declineinactivate")
    @SecuredService(permissions = {"pricelistrow:decline_inactivate"})
    public Response declineInactivateRows(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("agreementUniqueId") long agreementUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId,
            List<AgreementPricelistRowAPI> pricelistRowAPIs) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "declineInactivateRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        pricelistRowAPIs = pricelistRowController.declineInactivateRows(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPIs, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( pricelistRowAPIs == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(pricelistRowAPIs).build();
        }
    }

    @POST
    @Path("declineinactivate/{comment}")
    @SecuredService(permissions = {"pricelistrow:decline_inactivate"})
    public Response declineInactivateRowsComment(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("agreementUniqueId") long agreementUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId,
            List<AgreementPricelistRowAPI> pricelistRowAPIs,
            @PathParam("comment") String comment) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "declineInactivateRowsComment( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        pricelistRowAPIs = pricelistRowController.declineInactivateRowsComment(comment, organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPIs, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( pricelistRowAPIs == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(pricelistRowAPIs).build();
        }
    }


    /**
     * Reopen inactivated rows.
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement 
     * @param pricelistUniqueId unique id of the pricelist  
     * @param pricelistRowAPIs user supplied rows
     * @return the created <code>PricelistRowAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validations fails
     */
    @POST
    @Path("reopen")
    @SecuredService(permissions = {"pricelistrow:reopen"})
    public Response reopenInactivatedRows(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("agreementUniqueId") long agreementUniqueId,
            @PathParam("pricelistUniqueId") long pricelistUniqueId,
            List<AgreementPricelistRowAPI> pricelistRowAPIs) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "reopenInactivatedRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        pricelistRowAPIs = pricelistRowController.reopenInactivatedRows(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPIs, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( pricelistRowAPIs == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(pricelistRowAPIs).build();
        }
    }
    
}

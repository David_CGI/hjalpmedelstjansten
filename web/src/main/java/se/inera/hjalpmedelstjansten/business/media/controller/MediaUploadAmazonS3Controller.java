/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.inera.hjalpmedelstjansten.business.media.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.ws.rs.ProcessingException;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;

import com.amazonaws.util.IOUtils;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;

/**
 * This class uses Amazon S3 to store media files (documents and images).
 *
 * @author Tommy Berglund
 */
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@TransactionAttribute(value = TransactionAttributeType.NEVER)
public class MediaUploadAmazonS3Controller {

    @Inject
    HjmtLogger LOG;

    @Inject
    ValidationMessageService validationMessageService;

    private String mediaFetchProxyServer;
    private int mediaFetchProxyPort;

    private boolean enabled = false;
    private boolean local = false;

    private String mediaBaseUrl;

    private String bucketName;

    public String uploadFileFromUrl(String externalKey, String urlString) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "uploadFileFromUrl, ExternalKey: " + externalKey + ". UrlString: " + urlString);
        if (enabled) {
            InputStream inputStream = null;
            HttpURLConnection httpURLConnection = null;
            try {
                URL url = new URL(urlString);
                if (mediaFetchProxyServer != null && !mediaFetchProxyServer.isEmpty()) {
                    Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(mediaFetchProxyServer, mediaFetchProxyPort));
                    httpURLConnection = this.manageRedirects(url, proxy);

                } else {
                    httpURLConnection = this.manageRedirects(url);
                }
                int responseCode = httpURLConnection.getResponseCode();
                LOG.log(Level.FINEST, "Server responded with responseCode: {0}", new Object[]{responseCode});
                LOG.log(Level.FINEST, "Response code from HttpURLConnection: " + httpURLConnection.getResponseCode());

                if (responseCode != HttpURLConnection.HTTP_OK) {
                    throw validationMessageService.generateValidationException("file", "media.url.fetchfail");
                }

                inputStream = httpURLConnection.getInputStream();
                externalKey = checkAndCorrectFileEndingBasedOnContentType(httpURLConnection, externalKey);

                if (externalKey != null) {
                    LOG.log(Level.FINEST, "uploadFileFromUrl: UploadingFile");
                    return uploadFile(externalKey, inputStream);
                }
            } catch (ProcessingException | IOException  ex) {
                LOG.log(Level.WARNING, "Failed to handle media upload using url", ex);
                throw validationMessageService.generateValidationException("file", "media.url.fetchfail");
            }
             finally {
                try {
                    if (inputStream != null) {
                        LOG.log(Level.FINEST, "uploadFileFromUrl: Closing Stream");
                        inputStream.close();
                    }
                    if (httpURLConnection != null) {
                        LOG.log(Level.FINEST, "uploadFileFromUrl: Force closing Stream");
                        httpURLConnection.disconnect();
                    }
                } catch (IOException ex) {
                    LOG.log(Level.WARNING, "Failed to close inputstream on upload using url", ex);
                }
            }
        } else {
            LOG.log(Level.WARNING, "External media handling is not configured");
        }
        return null;
    }


    public HttpURLConnection manageRedirects(URL url) {

        HttpURLConnection httpURLConnection = null;

        try {
            while (true) {

                httpURLConnection = (HttpURLConnection) url.openConnection();
                if (httpURLConnection.getResponseCode() == 200) {
                    LOG.log(Level.FINE, "No redirect");
                    return httpURLConnection;
                }

                //We only allow for 10 redirects, still very high but better that than users having issues.
                if (httpURLConnection.getResponseCode() != 200 /*&& i <= 10 */) {
                    url = new URL(httpURLConnection.getHeaderField("Location"));
                    httpURLConnection.disconnect();
                } else {
                    LOG.log(Level.FINE, "Managed to redirect");
                    return httpURLConnection;
                }
            }
        } catch (IOException e) {
            LOG.log(Level.WARNING, "Didn't manage to redirect to user URL: " + url + ". Probably doesn't point to anything.");
        }
        return httpURLConnection;
    }
    private static long timer = 0;
    public HttpURLConnection manageRedirects(URL url, Proxy proxy) {

        HttpURLConnection httpURLConnection = null;

        try {
            while (true) {

                httpURLConnection = (HttpURLConnection) url.openConnection(proxy);
                if (httpURLConnection.getResponseCode() == 200 /*&& i <= 10 */) {
                    LOG.log(Level.FINE, "No redirect");
                    return httpURLConnection;
                }

                //We only allow for 10 redirects, still very high but better that than users having issues.
                if (httpURLConnection.getResponseCode() != 200) {
                    url = new URL(httpURLConnection.getHeaderField("Location"));
                    httpURLConnection.disconnect();
                } else {
                    LOG.log(Level.FINE, "Managed to redirect");
                    return httpURLConnection;
                }
            }

        } catch (IOException e) {
            LOG.log(Level.WARNING, "Didn't manage to redirect to user URL: " + url + ". Probably a Proxy issue.");
        }
        return httpURLConnection;
    }

    private String correctFileEnding(String subtype) {

        switch (subtype) {

            case "vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                return "xlsx";
            case "msword":
                return "docx";
            case "vnd.openxmlformats-officedocument.wordprocessingml.document":
            case "vnd.ms-excel":
                return "xls";
            default:
                return subtype;
        }
    }

    public String checkAndCorrectFileEndingBasedOnContentType(HttpURLConnection httpURLConnection, String externalKey) throws HjalpmedelstjanstenValidationException {

        String fileEnding;
        String contentType = httpURLConnection.getContentType();
        String mainType = contentType.substring(0, contentType.lastIndexOf('/')).toLowerCase();
        String subType = contentType.substring(contentType.lastIndexOf('/') + 1).toLowerCase();

        if (externalKey != null) {
            switch (mainType) {

                case "image":
                case "application":

                    if (subType.equals("octet-stream")) {
                        return externalKey;
                    }
                    if (!externalKey.contains(".")) {

                        externalKey = externalKey + "." + this.correctFileEnding(subType);
                        return externalKey;
                    } else {

                        fileEnding = externalKey.substring(externalKey.lastIndexOf('.'));
                        externalKey = externalKey.replace(fileEnding, '.' + correctFileEnding(subType));
                        return externalKey;
                    }

                case "binary":

                    if (subType.equals("octet-stream")) {
                        return externalKey;
                    } else {
                        LOG.log(Level.FINEST, "Only accepts octet-stream as binary type: " + subType + " is not supported");
                        break;
                    }
                default:
                    LOG.log(Level.FINEST, "Only accepts image, application and one binary as a main content-type, type: " + httpURLConnection.getContentType() + " is not supported");
                    throw new HjalpmedelstjanstenValidationException("Non supported content-type");
            }
        }
        return null;
    }


    /**
     * @param externalKey
     * @return returns the correct contentType based on the file ending of the file being uploaded.
     * @throws HjalpmedelstjanstenValidationException
     */
    public String determineAndSetContentType(String externalKey) throws HjalpmedelstjanstenValidationException {

        String contentType;

        String image = "image/";
        String application = "application/";
        String fileEnding = externalKey.substring(externalKey.lastIndexOf('.') + 1).toLowerCase();

        switch (fileEnding) {

            case "jpeg":
            case "jpg":
                contentType = image + "jpeg";
                return contentType;
            case "png":
                contentType = image + "png";
                return contentType;
            case "gif":
                contentType = image + "gif";
                return contentType;
            case "pdf":
                contentType = application + "pdf";
                return contentType;
            case "doc":
                contentType = application + "msword";
                return contentType;
            case "docx":
                contentType = application + "vnd.openxmlformats-officedocument.wordprocessingml.document";
                return contentType;
            case "xls":
                contentType = application + "vnd.ms-excel";
                return contentType;
            case "xlsx":
                contentType = application + "vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                return contentType;

            default:
                LOG.log(Level.FINEST, externalKey.substring(externalKey.lastIndexOf("." + 1)) + "Non supported content-type ");
                throw new HjalpmedelstjanstenValidationException("Non supported content-type");
            }
    }

    /**
     * Saves the file to a bucket with the given externalkey as object key
     *
     * @param externalKey object key for the file
     * @param inputStream the object data
     * @return the url of the uploaded file
     * @throws HjalpmedelstjanstenValidationException if an error occurs during upload
     */
    public String uploadFile(String externalKey, InputStream inputStream) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "uploadFile( externalKey: {0} )", new Object[]{externalKey});
        if (enabled) {
            try {
                AmazonS3 s3 = createS3Connection();

                PutObjectRequest request = generateRequest(externalKey, inputStream);

                s3.putObject(request);

            } catch (AmazonServiceException ex) {
                // this error is considered less "severe" by amazon
                LOG.log(Level.WARNING, "Failed to upload file to bucket.", ex);
                throw validationMessageService.generateValidationException("file", "media.file.uploadfail");
            } catch (AmazonClientException ex) {
                // this error is considered more "severe" by amazon
                LOG.log(Level.SEVERE, "Failed to upload file to bucket.", ex);
                throw validationMessageService.generateValidationException("file", "media.file.uploadfail");
            } catch (IOException e) {
                LOG.log(Level.FINEST, "IOException");
            }
            return mediaBaseUrl + (mediaBaseUrl.endsWith("/") ? "" : "/") + externalKey;
        } else {
            LOG.log(Level.WARNING, "External media handling is not configured");
        }
        return null;
    }

    private PutObjectRequest generateRequest(String externalKey, InputStream inputStream) throws HjalpmedelstjanstenValidationException, IOException {

        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentType(determineAndSetContentType(externalKey));

        PutObjectRequest request;

        byte[] bytes = IOUtils.toByteArray(inputStream);

        objectMetadata.setContentLength(bytes.length);

        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);

        request = new PutObjectRequest(bucketName, externalKey, byteArrayInputStream, objectMetadata);
        request.getRequestClientOptions().setReadLimit(bytes.length);
        return request;
    }

    /**
     * Delete the file with the given externalKey as object key from bucket.
     *
     * @param externalKey object key of the file
     * @throws HjalpmedelstjanstenValidationException if an error occurs during delete
     */
    public void deleteFile(String externalKey) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "deleteFile( externalKey: {0} )", new Object[]{externalKey});
        if (enabled) {
            try {
                AmazonS3 s3 = createS3Connection();
                if (!s3.doesObjectExist(bucketName, externalKey)) {
                    // if the object doesn't exist, this shouldn't stop the deletion of
                    // the media locally, so we log warning and continue
                    LOG.log(Level.WARNING, "No object with key exists in bucket when trying to delete");
                    return;
                }
                s3.deleteObject(bucketName, externalKey);
            } catch (AmazonServiceException ex) {
                // this error is considered less "severe" by amazon
                LOG.log(Level.WARNING, "Failed to delete file from bucket.", ex);
                throw validationMessageService.generateValidationException("file", "media.file.deletefail");
            } catch (AmazonClientException ex) {
                // this error is considered more "severe" by amazon
                LOG.log(Level.SEVERE, "Failed to delete file from bucket.", ex);
                throw validationMessageService.generateValidationException("file", "media.file.deletefail");
            }
        } else {
            LOG.log(Level.WARNING, "External media handling is not configured");
        }
    }

    public boolean checkIfExist(String externalKey) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "deleteFile( externalKey: {0} )", new Object[]{externalKey});
        if (enabled) {
            try {
                AmazonS3 s3 = createS3Connection();
                return s3.doesObjectExist(bucketName, externalKey);
            } catch (AmazonServiceException ex) {
                // this error is considered less "severe" by amazon
                LOG.log(Level.WARNING, "Failed to get file from bucket.", ex);
                throw validationMessageService.generateValidationException("file", "media.file.deletefail");
            } catch (AmazonClientException ex) {
                // this error is considered more "severe" by amazon
                LOG.log(Level.SEVERE, "Failed to get file from bucket.", ex);
                throw validationMessageService.generateValidationException("file", "media.file.deletefail");
            }
        }
        return false;
    }

    protected AmazonS3 createS3Connection() {

        //This is for the local S3 - hard configured since the local bool is only triggered if a system prop is missing for the S3 configuration. This way we don't have to keep
        // these details in the docker-compose.yml.
        if(local) {

        this.bucketName = "test";
        this.mediaBaseUrl = "http://localhost:9000/test/";

        AWSCredentials credentials = new BasicAWSCredentials("test", "testtest");

        ClientConfiguration clientConfiguration = new ClientConfiguration();
        clientConfiguration.setConnectionTimeout(120 * 1000);
        clientConfiguration.withMaxErrorRetry(15);
        clientConfiguration.setSignerOverride("AWSS3V4SignerType");
        clientConfiguration.withMaxConnections(100);

        return AmazonS3ClientBuilder
                .standard()
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration("http://minio:9000", Regions.US_EAST_1.name()))
                .withPathStyleAccessEnabled(true)
                .withClientConfiguration(clientConfiguration)
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .build();
        }

        else {
            return AmazonS3ClientBuilder.standard().withClientConfiguration(new ClientConfiguration().withMaxConnections(100)
                    .withConnectionTimeout(120 * 1000)
                    .withMaxErrorRetry(15)).build();
        }
    }


    @PostConstruct
    private void initialize() {

        LOG.log(Level.FINEST, "AWS BUCKET = " + System.getenv("AWS_BUCKET"));
        LOG.log(Level.FINEST, "MEDIA_BASE_URL = " + System.getenv("MEDIA_BASE_URL"));

        if (System.getenv("AWS_REGION") != null &&
                !System.getenv("AWS_REGION").isEmpty() &&
                System.getenv("AWS_ACCESS_KEY_ID") != null &&
                !System.getenv("AWS_ACCESS_KEY_ID").isEmpty() &&
                System.getenv("AWS_SECRET_ACCESS_KEY") != null &&
                !System.getenv("AWS_SECRET_ACCESS_KEY").isEmpty() &&
                System.getenv("AWS_BUCKET") != null &&
                !System.getenv("AWS_BUCKET").isEmpty() &&
                System.getenv("MEDIA_BASE_URL") != null &&
                !System.getenv("MEDIA_BASE_URL").isEmpty()) {
            LOG.log(Level.FINEST, "AWS BUCKET = " + System.getenv("AWS_BUCKET"));
            LOG.log(Level.FINEST, "MEDIA_BASE_URL = " + System.getenv("MEDIA_BASE_URL"));
            bucketName = System.getenv("AWS_BUCKET");
            mediaBaseUrl = System.getenv("MEDIA_BASE_URL");
            enabled = true;
            local = false;
        } else {
            LOG.log(Level.FINEST, "Local S3 is being configured");
            local = true;
            enabled = true;
        }
        if (System.getenv("MEDIA_FETCH_PROXY_SERVER") != null &&
                !System.getenv("MEDIA_FETCH_PROXY_SERVER").isEmpty() &&
                System.getenv("MEDIA_FETCH_PROXY_PORT") != null &&
                !System.getenv("MEDIA_FETCH_PROXY_PORT").isEmpty()) {
            mediaFetchProxyServer = System.getenv("MEDIA_FETCH_PROXY_SERVER");
            mediaFetchProxyPort = Integer.parseInt(System.getenv("MEDIA_FETCH_PROXY_PORT"));
        }
        LOG.log(Level.INFO, "External file upload enabled: {0}", new Object[]{enabled});
    }

}

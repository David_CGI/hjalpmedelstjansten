/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.product.controller.exportimport;

import se.inera.hjalpmedelstjansten.model.api.media.MediaAPI;

/**
 * Class for moving media data between import product/article steps read and save
 * 
 * @author Tommy Berglund
 */
public class ImportMediaItem {

    private MediaAPI mediaAPI;
    private Long productId;
    private Long articleId;
    private String sheetName;
    private int rowNumber;

    public ImportMediaItem(MediaAPI mediaAPI, 
            Long productId,
            Long articleId, 
            String sheetName, 
            int rowNumber) {
        this.mediaAPI = mediaAPI;
        this.productId = productId;
        this.articleId = articleId;
        this.sheetName = sheetName;
        this.rowNumber = rowNumber;
    }

    public MediaAPI getMediaAPI() {
        return mediaAPI;
    }

    public void setMediaAPI(MediaAPI mediaAPI) {
        this.mediaAPI = mediaAPI;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getArticleId() {
        return articleId;
    }

    public void setArticleId(Long articleId) {
        this.articleId = articleId;
    }
    
    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }

    public int getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(int rowNumber) {
        this.rowNumber = rowNumber;
    }

    
}

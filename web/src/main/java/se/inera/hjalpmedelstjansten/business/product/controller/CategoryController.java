/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.product.controller;

import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.api.CategoryAPI;
import se.inera.hjalpmedelstjansten.model.api.CategorySpecificPropertyAPI;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Category;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificProperty;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificPropertyListValue;

/**
 *
 * @author Tommy Berglund
 */
@Stateless
public class CategoryController {
    
    @Inject
    HjmtLogger LOG;
    
    @Inject
    ValidationMessageService validationMessageService;
    
    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;
    
    /**
     * Search for categories based on user input. If no query and no code is requested
     * returns all first level codes (without parents)
     * 
     * @param queryString user query
     * @param code code to search for
     * @param articleType if this is not null, only one category is returned, which is
     * the one without a code for this article type
     * @param excludeCodeLessCategories
     * @param excludeCodeCategories
     * @return a list of <code>CategoryAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if articleType is H
     */
    public List<CategoryAPI> searchCategories(String queryString, 
            String code, 
            Article.Type articleType, 
            boolean excludeCodeLessCategories,
            boolean excludeCodeCategories) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "searchCategories(...)");
        if( articleType != null ) {
            if( articleType == Article.Type.H ) {
                LOG.log( Level.WARNING, "Attempt to filter categories on articletype H which is not allowed");
                throw validationMessageService.generateValidationException("type", "category.selectHNotAllowed");
            }
            List<Category> categorys = em.createNamedQuery(Category.FIND_BY_ARTICLE_TYPE_EMPTY_CODE).
                        setParameter("articleType", articleType).
                        getResultList();
            if( categorys == null || categorys.isEmpty() ) {
                LOG.log( Level.WARNING, "No category without code found for articleType: {0}", new Object[] {articleType});
                return null;
            } else if( categorys.size() > 1 ) {
                LOG.log( Level.WARNING, "Multiple categorys without code found for articleType: {0}", new Object[] {articleType});
                return null;
            } else {
                return CategoryMapper.map(categorys);
            }            
        } else {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT c FROM Category c ");
            boolean orderByUniqueId = false;
            if( queryString != null && !queryString.isEmpty() ) {
                sb.append("WHERE (c.name LIKE :query OR c.code LIKE :query) AND c.articleType IS NOT NULL ");
            } else if( code != null && !code.isEmpty() ) {
                sb.append("WHERE c.parent.code = :code ");
            } else {
                sb.append("WHERE c.parent IS NULL ");
                orderByUniqueId = true;
            }
            if( excludeCodeLessCategories ) {
                sb.append("AND c.code IS NOT NULL ");
            }
            if( excludeCodeCategories ) {
                sb.append("AND c.code IS NULL ");
            }
            // when looking for first level codes, we want to order by id because
            // there may be categories that do not have codes to sort on
            if( orderByUniqueId ) {
                sb.append( "ORDER BY c.uniqueId ASC" );
            } else {
                sb.append( "ORDER BY c.code ASC" );
            }
            String querySql = sb.toString();
            LOG.log(Level.FINEST, "querySql: {0}", new Object[] {querySql});
            Query query = em.createQuery(sb.toString());
            if( queryString != null && !queryString.isEmpty() ) {
                query.setParameter("query", "%" + queryString + "%");
            }
            if( code != null && !code.isEmpty() ) {
                query.setParameter("code", code);
            }
            return CategoryMapper.map(query.getResultList());
        }
    }
    
    /**
     * Get the <code>Category</code> with the given id. We are only 
     * interested in categories without children, i.e. leaf nodes cause
     * these are the only ones that can be added.
     * 
     * @param categoryUniqueId unique id of the category
     * @return the corresponding list of <code>Category</code>, this should
     * never include more than 1 result
     */
    public List<Category> getChildlessById(long categoryUniqueId) {
        LOG.log(Level.FINEST, "getChildlessById(categoryUniqueId: {0})", new Object[] {categoryUniqueId});
        return em.createNamedQuery(Category.FIND_BY_ID_AND_LEAFNODE). 
                setParameter("uniqueId", categoryUniqueId).
                getResultList();                
    }

    /**
     * Get the <code>Category</code> with the given code. We are only 
     * interested in categories without children, i.e. leaf nodes cause
     * these are the only ones that can be used.
     * 
     * @param code unique code of the category
     * @return the corresponding <code>Category</code>
     */
    public Category getChildlessByCode(String code) {
        LOG.log(Level.FINEST, "getChildlessByCode(code: {0})", new Object[] {code});
        List<Category> categorys = em.createNamedQuery(Category.FIND_BY_CODE_AND_LEAFNODE). 
                setParameter("code", code).
                getResultList();
        // category codes are unique so maximum one can be returned
        if( categorys != null && !categorys.isEmpty() ) {
            return categorys.get(0);
        }
        return null;
    }
    
    /**
     * Get the <code>Category</code> with the given id. We are only 
     * interested in categories without children, i.e. leaf nodes cause
     * these are the only ones that can be added.
     * 
     * @param categoryUniqueId unique id of the category
     * @return the corresponding list of <code>Category</code>, this should
     * never include more than 1 result
     */
    public Category getById(long categoryUniqueId) {
        LOG.log(Level.FINEST, "getById(categoryUniqueId: {0})", new Object[] {categoryUniqueId});
        return em.find(Category.class, categoryUniqueId);                
    }
    
    /**
     * Get all category specific propertiy apis for the given category
     * 
     * @param categoryUniqueId unique id of the category
     * @return a list of specific properties mapped to API, may be null or empty
     */
    public List<CategorySpecificPropertyAPI> getCategoryPropertyAPIs(long categoryUniqueId) {
        LOG.log(Level.FINEST, "getCategoryPropertyAPIs(categoryUniqueId: {0})", new Object[] {categoryUniqueId});
        List<CategorySpecificProperty> categorySpecificPropertys = em.createNamedQuery(CategorySpecificProperty.FIND_BY_CATEGORY).
                setParameter("categoryUniqueId", categoryUniqueId).
                getResultList();
        return CategorySpecificPropertyMapper.map(categorySpecificPropertys);
    }
     
    /**
     * Get all category specific properties for the given category
     * 
     * @param categoryUniqueId unique id of the category
     * @return a list of specific properties, may be null or empty
     */
    public List<CategorySpecificProperty> getCategoryPropertys(Long categoryUniqueId) {
        // TODO: changed from long to Long to make test cases pass, testcases should be fixed instead
        LOG.log(Level.FINEST, "getCategoryPropertys(categoryUniqueId: {0})", new Object[] {categoryUniqueId});
        return em.createNamedQuery(CategorySpecificProperty.FIND_BY_CATEGORY).
                setParameter("categoryUniqueId", categoryUniqueId).
                getResultList();
    }

    public List<Category> getCategoryByArticleType(Article.Type articleType)
    {
        LOG.log(Level.FINEST, "getCategoryByArticleType(articleType: {0})", new Object[] {articleType});
        return em.createNamedQuery(Category.FIND_BY_ARTICLE_TYPE_EMPTY_CODE).
                setParameter("articleType", articleType).
                getResultList();
    }

    public CategorySpecificProperty getCategorySpecificProperty( long categorySpecificPropertyUniqueId ) {
        LOG.log(Level.FINEST, "getCategorySpecificProperty(categorySpecificPropertyUniqueId: {0})", new Object[] {categorySpecificPropertyUniqueId});
        return em.find(CategorySpecificProperty.class, categorySpecificPropertyUniqueId);
    }
 
    public CategorySpecificPropertyListValue getCategorySpecificPropertyListValue( long categorySpecificPropertyListValueId ) {
        LOG.log(Level.FINEST, "getCategorySpecificPropertyListValue(categorySpecificPropertyListValueId: {0})", new Object[] {categorySpecificPropertyListValueId});
        return em.find(CategorySpecificPropertyListValue.class, categorySpecificPropertyListValueId);
    }
    
}

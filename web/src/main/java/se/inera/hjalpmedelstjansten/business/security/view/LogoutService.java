/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.security.view;

import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.controller.LogoutController;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;

/**
 *
 * @author Tommy Berglund
 */
@Stateless
@Path("logout")
@Interceptors({ PerformanceLogInterceptor.class })
public class LogoutService extends BaseService {
    
    @Inject
    private HjmtLogger LOG;
    
    @Inject
    private LogoutController logoutController;

    @Inject
    private AuthHandler authHandler;
    
    @POST
    public Response logoutUser(@Context HttpServletRequest httpServletRequest) {
        LOG.log(Level.FINEST, "logoutUser(...)");
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        logoutController.logoutUser(userAPI, getRequestIp(httpServletRequest));
        return Response.ok().build();
    }

}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.media.controller;

import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.model.api.cv.CVDocumentTypeAPI;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVDocumentType;

/**
 *
 * @author Tommy Berglund
 */
@Stateless
public class DocumentTypeController {
    
    @Inject
    private HjmtLogger LOG;
    
    @PersistenceContext( unitName = "HjmtjPU")
    private EntityManager em;
    
    public List<CVDocumentTypeAPI> getAllDocumentTypes() {
        LOG.log(Level.FINEST, "getAllDocumentTypes()");
        return DocumentTypeMapper.map(findAll());
    }
    
    public List<CVDocumentType> findAll() {
        LOG.log(Level.FINEST, "findAll()");
        return em.createNamedQuery(CVDocumentType.FIND_ALL)
                .getResultList();
    }
    
    public CVDocumentType getDocumentType(long uniqueId) {
        LOG.log(Level.FINEST, "getDocumentType( uniqueId: {0} )");
        return em.find(CVDocumentType.class, uniqueId);
    }
    
    public CVDocumentType findByValue(String value) {
        LOG.log(Level.FINEST, "findByValue( value: {0} )", new Object[] {value});
        List<CVDocumentType> documentTypes = em.
                createNamedQuery(CVDocumentType.FIND_BY_VALUE).
                setParameter("value", value).
                getResultList();
        if( documentTypes == null || documentTypes.isEmpty() ) {
            return null;
        } else if( documentTypes.size() > 1 ) {
            LOG.log( Level.WARNING, "Multiple document types found for value: {1}", new Object[] {value});
            return null;
        } else {
            return documentTypes.get(0);
        }
    }
    
}

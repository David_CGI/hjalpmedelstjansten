/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.product.controller;

import java.util.ArrayList;
import java.util.List;
import se.inera.hjalpmedelstjansten.model.api.ResourceSpecificPropertyAPI;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificPropertyListValue;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValue;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueDecimal;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueInterval;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueTextField;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueValueListMultiple;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueValueListSingle;

/**
 * Class for mapping between API and Entity classes
 * 
 * @author Tommy Berglund
 */
public class ResourceSpecificPropertyMapper {
    
    public static final List<ResourceSpecificPropertyAPI> map(List<ResourceSpecificPropertyValue> resourceSpecificPropertyValues) {
        if( resourceSpecificPropertyValues == null || resourceSpecificPropertyValues.isEmpty() ) {
            return null;
        }
        List<ResourceSpecificPropertyAPI> resourceSpecificPropertyAPIs = new ArrayList<>();
        for( ResourceSpecificPropertyValue resourceSpecificPropertyValue : resourceSpecificPropertyValues ) {
            resourceSpecificPropertyAPIs.add(map(resourceSpecificPropertyValue));
        }
        return resourceSpecificPropertyAPIs;
    }
    
    public static final ResourceSpecificPropertyAPI map(ResourceSpecificPropertyValue resourceSpecificPropertyValue) {
        if( resourceSpecificPropertyValue == null ) {
            return null;
        }
        ResourceSpecificPropertyAPI resourceSpecificPropertyAPI = new ResourceSpecificPropertyAPI();
        resourceSpecificPropertyAPI.setId(resourceSpecificPropertyValue.getUniqueId());
        resourceSpecificPropertyAPI.setProperty(CategorySpecificPropertyMapper.map(resourceSpecificPropertyValue.getCategorySpecificProperty()));
        if( resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueTextField ) {
            ResourceSpecificPropertyValueTextField resourceSpecificPropertyValueTextfield = ( ResourceSpecificPropertyValueTextField ) resourceSpecificPropertyValue;
            resourceSpecificPropertyAPI.setTextValue(resourceSpecificPropertyValueTextfield.getValue());
        } else if( resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueDecimal ) {
            ResourceSpecificPropertyValueDecimal resourceSpecificPropertyValueDecimal = ( ResourceSpecificPropertyValueDecimal ) resourceSpecificPropertyValue;
            resourceSpecificPropertyAPI.setDecimalValue(resourceSpecificPropertyValueDecimal.getValue());
        } else if( resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueInterval ) {
            ResourceSpecificPropertyValueInterval resourceSpecificPropertyValueInterval = ( ResourceSpecificPropertyValueInterval ) resourceSpecificPropertyValue;
            resourceSpecificPropertyAPI.setIntervalFromValue(resourceSpecificPropertyValueInterval.getFromValue());
            resourceSpecificPropertyAPI.setIntervalToValue(resourceSpecificPropertyValueInterval.getToValue());
        } else if( resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueValueListSingle ) {
            ResourceSpecificPropertyValueValueListSingle resourceSpecificPropertyValueValueListSingle = ( ResourceSpecificPropertyValueValueListSingle ) resourceSpecificPropertyValue;
            resourceSpecificPropertyAPI.setSingleListValue(resourceSpecificPropertyValueValueListSingle.getValue() == null ? null: resourceSpecificPropertyValueValueListSingle.getValue().getUniqueId());            
        } else if( resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueValueListMultiple ) {
            ResourceSpecificPropertyValueValueListMultiple resourceSpecificPropertyValueValueListMultiple = ( ResourceSpecificPropertyValueValueListMultiple ) resourceSpecificPropertyValue;
            if( resourceSpecificPropertyValueValueListMultiple.getValues() != null && !resourceSpecificPropertyValueValueListMultiple.getValues().isEmpty() ) {
                List<Long> multiListValues = new ArrayList<>();
                for( CategorySpecificPropertyListValue categorySpecificPropertyListValue : resourceSpecificPropertyValueValueListMultiple.getValues() ) {
                    multiListValues.add(categorySpecificPropertyListValue.getUniqueId());
                }
                resourceSpecificPropertyAPI.setMultipleListValue(multiListValues);                
            }
        }
        return resourceSpecificPropertyAPI;
    }
    
}

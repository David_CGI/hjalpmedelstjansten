/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.property.view;

import java.util.ResourceBundle;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;

/**
 *
 * @author Tommy Berglund
 */
@Singleton
@Startup
public class ValidationMessageService {
    
    @Inject
    private HjmtLogger LOG;
    
    private ResourceBundle swedishResources;
 
    public String getMessage(String key) {
        if( swedishResources.containsKey(key) ) {
            return swedishResources.getString(key);
        } else {
            LOG.log(Level.WARNING, "Missing validation message: {0}", new Object[] {key});
            return key;
        }
    }
    
    public String getMessage(String key, Object... args) {
        if( swedishResources.containsKey(key) ) {
            return String.format(swedishResources.getString(key), args);
        } else {
            LOG.log(Level.WARNING, "Missing validation message: {0}", new Object[] {key});
            return key;
        }
    }
    
    @PostConstruct
    private void init() {
        swedishResources = ResourceBundle.getBundle("ValidationMessages");
    }
            
    /**
     * Generate a validation exception based on the supplied message.
     * 
     * @param field the specific field that failed
     * @param message the message
     * @return a custom validation exception with the supplied message.
     */
    public HjalpmedelstjanstenValidationException generateValidationException( String field, String message ) {
        LOG.log( Level.FINEST, "generateValidationException( message: {0} )", new Object[] {message});
        HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
        exception.addValidationMessage(field, getMessage(message));
        return exception;
    }
    
    /**
     * Generate a validation exception based on the supplied message.
     * 
     * @param field the specific field that failed
     * @param message the message
     * @param args arguments for formatted string 
     * @return a custom validation exception with the supplied message.
     */
    public HjalpmedelstjanstenValidationException generateValidationException( String field, String message, Object ... args ) {
        LOG.log( Level.FINEST, "generateValidationException( message: {0} )", new Object[] {message});
        HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
        exception.addValidationMessage(field, getMessage(message, args));
        return exception;
    }
    
}

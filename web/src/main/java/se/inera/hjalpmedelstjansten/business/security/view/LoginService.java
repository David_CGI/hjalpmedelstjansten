/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.security.view;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.security.controller.LoginController;
import se.inera.hjalpmedelstjansten.model.api.LoginAPI;

/**
 *
 * @author Tommy Berglund
 */
@Stateless
@Path("login")
@Interceptors({ PerformanceLogInterceptor.class })
public class LoginService extends BaseService {
    
    @Inject
    private HjmtLogger LOG;
    
    @Inject
    private LoginController loginController;

    @Inject
    private ValidationMessageService validationMessageService;
    
    private String serviceBaseUrl;
    
    @POST
    public Response standardLoginUser(@Context HttpServletRequest httpServletRequest, LoginAPI loginAPI) {
        LOG.log(Level.FINEST, "standardLoginUser(...)");
        boolean success = loginController.standardLoginUser(loginAPI, getRequestIp(httpServletRequest));
        if( success ) {
            return Response.ok().build();
        } else {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
    }

    @GET
    public Response tokenLoginUser(@Context HttpServletRequest httpServletRequest, @QueryParam("username") String username, @QueryParam("token") String token) throws URISyntaxException, UnsupportedEncodingException {
        LOG.log(Level.FINEST, "tokenLoginUser(...)");
        boolean success = loginController.tokenLoginUser(username, token, getRequestIp(httpServletRequest));
        if( success ) {
            return Response.temporaryRedirect(new URI(serviceBaseUrl + "start")).build();
        } else {
            return Response.status(Response.Status.UNAUTHORIZED).entity(validationMessageService.getMessage("login.token.fail")).build();
        }
    }
    
    @PostConstruct
    private void initialize() {
        serviceBaseUrl = System.getenv("HJMTJ_SERVICE_BASE_URL");
    }
    
}

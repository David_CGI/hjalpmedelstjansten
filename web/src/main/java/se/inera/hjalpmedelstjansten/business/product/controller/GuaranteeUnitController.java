/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.product.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.model.api.cv.CVGuaranteeUnitAPI;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;

/**
 *
 * @author Tommy Berglund
 */
@Stateless
public class GuaranteeUnitController {
    
    @Inject
    private HjmtLogger LOG;
    
    @PersistenceContext( unitName = "HjmtjPU")
    private EntityManager em;
    
    public List<CVGuaranteeUnitAPI> getAllGuaranteeUnitAPIs() {
        LOG.log(Level.FINEST, "getAllGuaranteeUnitAPIs()");
        return GuaranteeUnitMapper.map(getAllGuaranteeUnits());
    }
    
    public CVGuaranteeUnit getGuaranteeUnit(long uniqueId) {
        LOG.log(Level.FINEST, "getGuaranteeUnit( uniqueId: {0} )");
        return em.find(CVGuaranteeUnit.class, uniqueId);
    }

    public List<CVGuaranteeUnit> getAllGuaranteeUnits() {
        LOG.log(Level.FINEST, "getAllGuaranteeUnits()");
        return em.createNamedQuery(CVGuaranteeUnit.FIND_ALL)
                .getResultList();
    }

    public CVGuaranteeUnit findByName(String name) {
        LOG.log(Level.FINEST, "findByName( name: {0} )", new Object[] {name});
        List<CVGuaranteeUnit> guaranteeUnits = em.createNamedQuery(CVGuaranteeUnit.FIND_BY_NAME).
                setParameter("name", name).
                getResultList();
        // there is a maximum of one
        if( guaranteeUnits != null && !guaranteeUnits.isEmpty() ) {
            return guaranteeUnits.get(0);
        }
        return null;
    }

    public Map<String, CVGuaranteeUnit> getGuaranteeUnitNamesMap() {
        Map<String, CVGuaranteeUnit> guaranteeUnitNamesMap = new HashMap<>();
        List<CVGuaranteeUnit> allGuaranteeUnits = getAllGuaranteeUnits();
        allGuaranteeUnits.forEach((guaranteeUnit) -> {
            guaranteeUnitNamesMap.put(guaranteeUnit.getName(), guaranteeUnit);
        });
        return guaranteeUnitNamesMap;
    }
    
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.property.view;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.PropertyAPI;
import se.inera.hjalpmedelstjansten.property.TextController;

/**
 * REST API for text to frontend
 * 
 * @author Tommy Berglund
 */
@Stateless
@Path("texts")
@Interceptors({ PerformanceLogInterceptor.class })
public class TextsService extends BaseService {
    
    @Inject
    private HjmtLogger LOG;
    
    @Inject
    private TextController textController;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @SecuredService(permissions = {"texts:view"})
    public Response getTexts() {
        LOG.log(Level.FINEST, "getTexts()");
        PropertyAPI propertyAPI = new PropertyAPI();
        Map<Object, Object> values = new HashMap<>();
        Properties properties = textController.getTexts();
        if( properties != null ) {
            for( Object key : properties.keySet() ) {
                Object value = properties.get(key);
                values.put(key, value);
            }
        }
        propertyAPI.setValues(values);
        return Response.ok(propertyAPI).build();
    }
       
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.product.controller;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementPricelistRowController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.user.controller.EmailController;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.model.api.*;
import se.inera.hjalpmedelstjansten.model.entity.*;

/**
 * Class for validation functionality for articles
 * 
 * @author Tommy Berglund
 */
@Stateless
public class ArticleValidation {
    
    @Inject
    HjmtLogger LOG;
    
    @Inject
    ValidationMessageService validationMessageService;
  
    @Inject
    ArticleController articleController;
    
    @Inject
    ProductController productController;

    @Inject
    EmailController emailController;

    @Inject
    UserController userController;

    @Inject
    AgreementPricelistRowController agreementPricelistRowController;

    @Inject
    OrganizationController organizationController;

    public void validateForCreate(ArticleAPI articleAPI, long organizationUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForCreate(...)" );
        Set<ErrorMessageAPI> errorMessageAPIs = tryForCreate(organizationUniqueId, articleAPI);
        
        if( !errorMessageAPIs.isEmpty() ) {
            HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
            exception.addValidationMessages(errorMessageAPIs);
            throw exception;
        }
    }
    
    public Set<ErrorMessageAPI> tryForCreate(long organizationUniqueId, ArticleAPI articleAPI) {
        Set<ErrorMessageAPI> errorMessageAPIs = new HashSet<>();
        validateArticleAPI(articleAPI, errorMessageAPIs);
        if( errorMessageAPIs.size() > 0 ) {
            // break early if indata is in itself incorrect
            return errorMessageAPIs;
        }
        // only web can be set in the electronic address of the manufacturer
        validateOnlyManufacturerWebSet(articleAPI, errorMessageAPIs);
        
        // first status cannot be DISCONTINUED
        /* Product.Status newStatus = Product.Status.valueOf(articleAPI.getStatus());

        if( newStatus == Product.Status.DISCONTINUED ) {
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("status", validationMessageService.getMessage("article.statusChange.invalid")));
        } */

        validateArticleNumber(articleAPI, organizationUniqueId, null, errorMessageAPIs);
        return errorMessageAPIs;
    }
    
    public void validateForUpdate(ArticleAPI articleAPI, Article article, long organizationUniqueId, UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log( Level.FINEST, "validateForUpdate(...)" );
        Set<ErrorMessageAPI> errorMessageAPIs = tryForUpdate(organizationUniqueId, articleAPI, article,userAPI);
        
        if( !errorMessageAPIs.isEmpty() ) {
            HjalpmedelstjanstenValidationException exception = new HjalpmedelstjanstenValidationException("Validation failed");
            exception.addValidationMessages(errorMessageAPIs);
            throw exception;
        }
    }
    
    public Set<ErrorMessageAPI> tryForUpdate(long organizationUniqueId, ArticleAPI articleAPI, Article article, UserAPI userAPI) {
        Set<ErrorMessageAPI> errorMessageAPIs = new HashSet<>();
        validateArticleAPI(articleAPI, errorMessageAPIs);
        validateArticleStatusForUpdate(article,articleAPI,errorMessageAPIs,userAPI);
        if( errorMessageAPIs.size() > 0 ) {
            // break early if indata is in itself incorrect
            return errorMessageAPIs;
        }
        // only web can be set in the electronic address of the manufacturer
        validateOnlyManufacturerWebSet(articleAPI, errorMessageAPIs);
        validateArticleNumber(articleAPI, organizationUniqueId, article, errorMessageAPIs);
        return errorMessageAPIs;
    }

    private void validateArticleStatusForUpdate(Article article, ArticleAPI articleAPI, Set<ErrorMessageAPI> errorMessageAPIs, UserAPI userAPI) {
        if(article.getStatus().toString().equalsIgnoreCase(Product.Status.DISCONTINUED.toString()) && articleAPI.getStatus().equalsIgnoreCase(Product.Status.PUBLISHED.toString())){
            //ska inte gå att redigera en utgången artikel - HJAL2035
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("replacementDate", validationMessageService.getMessage("import.productarticle.article.expiredDate")));
        }
        if(article.getStatus().toString().equalsIgnoreCase(Product.Status.DISCONTINUED.toString()) && articleAPI.getStatus().equalsIgnoreCase(Product.Status.DISCONTINUED.toString())){
            if(article.getReplacementDate() != null){
                //Återaktiverad artikel, skicka mail - HJAL1419
                try{
                    String mailSubject = String.format("Ändrad status för artikel %s",articleAPI.getArticleName()) ;
                    String mailBody = String.format("Artikel %s är inte längre inaktiv.", articleAPI.getArticleName());
                    List<String> customerMailList = articleController.getCustomerEmailByArticle(articleAPI,userAPI);
                    for(String email : customerMailList){
                        emailController.send(email, mailSubject,mailBody);
                    }
                }
                catch(Exception ex){
                    LOG.log(Level.SEVERE, "Failed to send email on update article", ex);
                }
            }
        }
        else if(articleAPI.getReplacementDate() != null || article.getReplacementDate() != null){
            try{
                List<String> customerMailList = articleController.getCustomerEmailByArticle(articleAPI,userAPI);
                Date date = new Date(articleAPI.getReplacementDate());
                if(article.getReplacementDate() == null){
                    //Nytt utgångsdatum
                    String mailSubject = String.format("Nytt utgångsdatum för artkel %s",articleAPI.getArticleName()) ;
                    String mailBody = String.format("Artikel %s har fått ett nytt utgångsdatum: %s", articleAPI.getArticleName(),new SimpleDateFormat("yyyy-MM-dd").format(date));
                    for(String email : customerMailList){
                        emailController.send(email, mailSubject,mailBody);
                    }
                }
                else if(articleAPI.getReplacementDate() != article.getReplacementDate().getTime()) {
                    //Uppdaterat utgångsdatum
                    String mailSubject = String.format("Uppdaterat utgångsdatum för artkel %s",articleAPI.getArticleName()) ;
                    String mailBody = String.format("Artikel %s har fått ett uppdaterat utgångsdatum: %s", articleAPI.getArticleName(),new SimpleDateFormat("yyyy-MM-dd").format(date));
                    for(String email : customerMailList){
                        emailController.send(email, mailSubject,mailBody);
                    }
                }
            }
            catch(Exception ex){
                LOG.log(Level.SEVERE, "Failed to send email on update article", ex);
            }
        }
    }

    private void validateArticleAPI(ArticleAPI articleAPI, Set<ErrorMessageAPI> errorMessageAPIs) {
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        Set<ConstraintViolation<ArticleAPI>> constraintViolations = validator.validate(articleAPI, Default.class);
        if( constraintViolations != null && !constraintViolations.isEmpty() ) {
            for( ConstraintViolation constraintViolation : constraintViolations ) {
                errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage(constraintViolation.getPropertyPath().toString(), constraintViolation.getMessage()));
            }
        }
    }
    
    private void validateOnlyManufacturerWebSet(ArticleAPI articleAPI, Set<ErrorMessageAPI> errorMessageAPIs) {
        // only web can be set in the electronic address of the manufacturer
        if( articleAPI.getManufacturerElectronicAddress() != null ) {
            ElectronicAddressAPI electronicAddressAPI = articleAPI.getManufacturerElectronicAddress();
            if( !isEmpty(electronicAddressAPI.getEmail()) ||
                    !isEmpty(electronicAddressAPI.getFax()) ||
                    !isEmpty(electronicAddressAPI.getMobile()) ||
                    !isEmpty(electronicAddressAPI.getTelephone()) ) {
                errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("web", validationMessageService.getMessage("article.manufacturer.onlyWeb")));
            }
        }
    }
    
    void validateArticleNumber(ArticleAPI articleAPI, 
            long organizationUniqueId, 
            Article article,
            Set<ErrorMessageAPI> errorMessageAPIs) {
        Article articleWithNumber = articleController.findByArticleNumberAndOrganization(articleAPI.getArticleNumber(), organizationUniqueId);
        if( articleWithNumber != null ) {
            if( article == null || !articleWithNumber.getUniqueId().equals(article.getUniqueId()) ) {
                errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("articleNumber", validationMessageService.getMessage("article.number.notOrganizationUnique")));
            }
        }
        Product productWithNumber = productController.findByProductNumberAndOrganization(articleAPI.getArticleNumber(), organizationUniqueId);
        if( productWithNumber != null ) {
            errorMessageAPIs.add(HjalpmedelstjanstenValidationException.generateValidationMessage("articleNumber", validationMessageService.getMessage("article.product.number.notOrganizationUnique")));
        }
    }
    
    private boolean isEmpty(String value) {
        LOG.log( Level.FINEST, "value: {0}", new Object[] {value});
        if( value == null || value.isEmpty() ) {
            return true;
        }
        return false;
    }

}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.product.view;

import java.util.List;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.product.controller.GuaranteeUnitController;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.cv.CVGuaranteeUnitAPI;

/**
 * REST API for Guarantee Unit functionality.
 * 
 * @author Tommy Berglund
 */
@Stateless
@Path("guaranteeunits")
@Interceptors({ PerformanceLogInterceptor.class })
public class GuaranteeUnitService extends BaseService {
    
    @Inject
    private HjmtLogger LOG;
    
    @EJB
    private GuaranteeUnitController guaranteeUnitController;
    
    /**
     * Return all guarantee units in the database. 
     * 
     * @return a list of <code>GuaranteeUnitAPI</code>
     */
    @GET
    @SecuredService(permissions = {"guaranteeunit:view"})
    public List<CVGuaranteeUnitAPI> getAllGuaranteeUnits() {
        LOG.log(Level.FINEST, "getAllGuaranteeUnits()");
        return guaranteeUnitController.getAllGuaranteeUnitAPIs();
    }
    
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.generalpricelist.view;

import java.util.logging.Level;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;

/**
 * REST API for general pricelists from a customer point of view. Customer can
 * search all active general pricelists from all suppliers.
 * 
 * @author Tommy Berglund
 */
@Stateless
@Path("generalpricelists")
@Interceptors({ PerformanceLogInterceptor.class })
public class GeneralPricelistCustomerService extends BaseService {
    
    @Inject
    private HjmtLogger LOG;
    
    @EJB
    GeneralPricelistController generalPricelistController;
        
    /**
     * Search all general pricelists. For customers who wants to include general pricelists
     * in the export settings 
     * 
     * @param userSearchQuery user can search for gp on certain values
     * @param offset
     * @see se.inera.hjalpmedelstjansten.model.entity.AgreementExportSettings
     * @return a list of <code>GeneralPricelistAPI</code>
     */
    @GET
    @SecuredService(permissions = {"generalpricelist:view_all"})
    public Response searchAllGeneralPricelist(
            @DefaultValue(value = "") @QueryParam("query") String userSearchQuery,
            @DefaultValue(value = "0") @QueryParam("offset") int offset) {
        LOG.log(Level.FINEST, "searchAllGeneralPricelist()");
        SearchDTO searchDTO = generalPricelistController.searchAllGeneralPricelistAPIs(userSearchQuery, 25, offset);
        return Response.ok(searchDTO.getItems()).
                header("X-Total-Count", searchDTO.getCount()).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                build();
    }
    /**
     * Search all general pricelists. For customers who wants to include general pricelists
     * in the export settings
     *
     * @param organizationUniqueId user can search for gp on certain values
     * @return a list of <code>GeneralPricelistAPI</code>
     */
    @GET
    @Path("organization/{organizationUniqueId}/exportsettings/{exportSettingsUniqueId}")
    @SecuredService(permissions = {"generalpricelist:view_all","exportsettings:view_own"})
    public Response searchAllUnselectedGeneralPricelist(
            @PathParam("exportSettingsUniqueId") long exportSettingsUniqueId,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @DefaultValue(value = "") @QueryParam("query") String userSearchQuery,
            @DefaultValue(value = "0") @QueryParam("offset") int offset)
        {

        LOG.log(Level.FINEST, "searchAllUnselectedGeneralPricelist()");

        SearchDTO sdto = generalPricelistController.searchAllUnselectedPricelistAPIs(exportSettingsUniqueId, userSearchQuery, 25, offset);
        return Response.ok(sdto.getItems()).
                header("X-Total-Count", sdto.getCount()).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                build();
    }

    
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.product.controller;

import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;

/**
 *
 * @author Tommy Berglund
 */
@Stateless
public class CVPreventiveMaintenanceController {
    
    @Inject
    private HjmtLogger LOG;
    
    @PersistenceContext( unitName = "HjmtjPU")
    private EntityManager em;
    
    public List<CVPreventiveMaintenance> getAllCVPreventiveMaintenances() {
        LOG.log(Level.FINEST, "getAllCVPreventiveMaintenances()");
        return em.createNamedQuery(CVPreventiveMaintenance.FIND_ALL).getResultList();
    }
    
    public CVPreventiveMaintenance findByCode(String code) {
        LOG.log(Level.FINEST, "findByCode( code: {0} )", new Object[] {code});
        List<CVPreventiveMaintenance> preventiveMaintenances = em.
                createNamedQuery(CVPreventiveMaintenance.FIND_BY_CODE).
                setParameter("code", code).
                getResultList();
        if( preventiveMaintenances == null || preventiveMaintenances.isEmpty() ) {
            return null;
        } else if( preventiveMaintenances.size() > 1 ) {
            LOG.log( Level.WARNING, "Multiple preventive maintenances found for code: {1}", new Object[] {code});
            return null;
        } else {
            return preventiveMaintenances.get(0);
        }
    }
    
}

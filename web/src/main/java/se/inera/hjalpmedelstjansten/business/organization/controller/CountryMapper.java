/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.organization.controller;

import java.util.ArrayList;
import java.util.List;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCountryAPI;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCountry;

/**
 *
 * @author Tommy Berglund
 */
public class CountryMapper {
    
    public static final List<CVCountryAPI> map(List<CVCountry> countries) {
        if( countries == null ) {
            return null;
        }
        List<CVCountryAPI> countryAPIs = new ArrayList<>();
        for( CVCountry country : countries ) {
            countryAPIs.add(map(country));
        }
        return countryAPIs;
    }
    
    public static final CVCountryAPI map(CVCountry country) {
        if( country == null ) {
            return null;
        }
        CVCountryAPI countryAPI = new CVCountryAPI();
        countryAPI.setId(country.getUniqueId());
        countryAPI.setCode(country.getCode());
        countryAPI.setName(country.getName());
        return countryAPI;
    }
    
}

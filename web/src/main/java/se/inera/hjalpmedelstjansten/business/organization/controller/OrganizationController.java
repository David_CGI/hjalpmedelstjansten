/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.organization.controller;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import se.inera.hjalpmedelstjansten.business.DateUtils;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.model.api.OrganizationAPI;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.model.api.PostAddressAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.UserEngagementAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCountry;
import se.inera.hjalpmedelstjansten.model.entity.InternalAudit;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.PostAddress;

/**
 * Business logic for organizations
 * 
 * @author Tommy Berglund
 */
@Stateless
public class OrganizationController {
    
    @Inject
    HjmtLogger LOG;
    
    @Inject
    Event<InternalAuditEvent> internalAuditEvent;
    
    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;
    
    @Inject
    CountryController countryController;
    
    @Inject
    BusinessLevelController businessLevelController;

    @Inject
    OrganizationValidation organizationValidation;
    
    /**
     * Get the <code>OrganizationAPI</code> for the <code>Organization</code>
     * with the given id
     * 
     * @param uniqueId unique id of the organization to get
     * @param userAPI
     * @param sessionId
     * @param requestIp
     * @return the corresponding <code>OrganizationAPI</code>
     */
    public OrganizationAPI getOrganizationAPI(long uniqueId, UserAPI userAPI, String sessionId, String requestIp) {
        LOG.log(Level.FINEST, "getOrganizationAPI( uniqueId: {0} )", new Object[] {uniqueId});
        Organization organization = getOrganization(uniqueId);
        if( organization != null ) {
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.ORGANIZATION, InternalAudit.ActionType.VIEW, userAPI.getId(), sessionId, uniqueId, requestIp));
            return OrganizationMapper.map( organization, true );
        }
        return null;
    }
    
    /**
     * Get the <code>Organization</code> with the given id
     * 
     * @param uniqueId unique id of the organization to get
     * @return the corresponding <code>Organization</code>
     */
    public Organization getOrganization(long uniqueId) {
        LOG.log(Level.FINEST, "getOrganization( uniqueId: {0} )", new Object[] {uniqueId});
        return em.find(Organization.class, uniqueId);
    }
    
    /**
     * Create a new <code>Organization</code> with the supplied values
     * 
     * @param organizationAPI the user supplied values
     * @param userAPI current user's session information
     * @param sessionId
     * @return an <code>OrganizationAPI</code> for the created organization
     * @throws HjalpmedelstjanstenValidationException in case validation fails
     */
    public OrganizationAPI createOrganization(OrganizationAPI organizationAPI, 
            UserAPI userAPI, 
            String sessionId,
            String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createOrganization(...)");
        organizationValidation.validateForCreate(organizationAPI);
        Organization organization = OrganizationMapper.map(organizationAPI);
        // add country
        CVCountry country = countryController.getCountry(organizationAPI.getCountry().getId());
        organization.setCountry(country);
        organization.setMediaFolderName(UUID.randomUUID().toString());
        em.persist(organization);
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.ORGANIZATION, InternalAudit.ActionType.CREATE, userAPI.getId(), sessionId, organization.getUniqueId(), requestIp));
        return OrganizationMapper.map( organization, true );
    }
    
    /**
     * Update the <code>Organization</code> with the given id with the supplied values.
     * Users in role Superadmin can modify most values of an <code>Organization</code> while
     * "local" admins only can update address information.
     * 
     * @param uniqueId the unique id of the <code>Organization</code>
     * @param organizationAPI the user supplied values
     * @param updateOrganization whether user has permission to update organization basic info
     * @param userAPI current user's session information
     * @param sessionId
     * @param requestIp
     * @return the updated <code>OrganizationAPI</code>
     * @throws HjalpmedelstjanstenValidationException in case validation fails
     */
    public OrganizationAPI updateOrganization(long uniqueId, 
            OrganizationAPI organizationAPI, 
            boolean updateOrganization, 
            UserAPI userAPI, 
            String sessionId,
            String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateOrganization( uniqueId: {0} )", new Object[] {uniqueId});
        Organization dbOrganization = getOrganization(uniqueId);
        if( dbOrganization == null ) {
            return null;
        }
        organizationValidation.validateForUpdate(organizationAPI, dbOrganization);

        if( updateOrganization ) {
            updateOrganizationBasics(dbOrganization, organizationAPI);
        }
        updateOrganizationAddresses(dbOrganization, organizationAPI);
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.ORGANIZATION, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, uniqueId, requestIp));
        return OrganizationMapper.map( dbOrganization, true );
    }

    /**
     * Update basic information about an organization.
     * 
     * @param organization the organization to update
     * @param organizationAPI the user supplied values
     */
    private void updateOrganizationBasics( Organization organization, OrganizationAPI organizationAPI ) {
        LOG.log(Level.FINEST, "updateOrganizationBasics( ... )");
        organization.setOrganizationName(organizationAPI.getOrganizationName());
        organization.setGln(organizationAPI.getGln());
        organization.setValidFrom(new Date(organizationAPI.getValidFrom()));
        organization.setValidTo(organizationAPI.getValidTo() == null ? null: DateUtils.endOfDay(new Date(organizationAPI.getValidTo())));
    }
    
    /**
     * Update address information about an organization.
     * 
     * @param organization the organization to update
     * @param organizationAPI the user supplied values
     */
    private void updateOrganizationAddresses( Organization organization, OrganizationAPI organizationAPI ) {
        LOG.log(Level.FINEST, "updateOrganizationAddresses( ... )");
        if( organizationAPI.getPostAddresses() != null && !organizationAPI.getPostAddresses().isEmpty() ) {
            for( PostAddressAPI postAddressAPI : organizationAPI.getPostAddresses() ) {
                PostAddress postAddress = getByAddressType(organization, postAddressAPI.getAddressType() );
                if( postAddress != null ) {
                    postAddress.setStreetAddress(postAddressAPI.getStreetAddress());
                    postAddress.setPostCode(postAddressAPI.getPostCode());
                    postAddress.setCity(postAddressAPI.getCity());
                }
            }
        }
        if( organizationAPI.getElectronicAddress() != null ) {
            if( organization.getElectronicAddress() != null ) {
                organization.getElectronicAddress().setEmail(organizationAPI.getElectronicAddress().getEmail());
                organization.getElectronicAddress().setFax(organizationAPI.getElectronicAddress().getFax());
                organization.getElectronicAddress().setMobile(organizationAPI.getElectronicAddress().getMobile());
                organization.getElectronicAddress().setTelephone(organizationAPI.getElectronicAddress().getTelephone());
                organization.getElectronicAddress().setWeb(organizationAPI.getElectronicAddress().getWeb());
            }
        }
    }
    
    /**
     * Get the <code>PostAddress</code> of the specified type for the given organization.
     * 
     * @param organization the organization
     * @param addressType the addressType to find
     * @return the PostAddress for the given addressType
     */
    private PostAddress getByAddressType( Organization organization, PostAddress.AddressType addressType ) {
        LOG.log(Level.FINEST, "getByAddressType( addressType: {0} )", new Object[] {addressType});
        if( organization.getPostAddresses() != null && !organization.getPostAddresses().isEmpty() ) {
            for( PostAddress postAddress : organization.getPostAddresses() ) {
                if( postAddress.getAddressType().equals(addressType) ) {
                    return postAddress;
                }
            }
        }
        return null;
    }
    
    /**
     * Search organizations for the user supplied query. Search is paginated so only
     * results valid for given offset and limit is returned.
     * 
     * @param queryString user supplied query
     * @param organizationTypes types of organizations to return
     * @param offset start returning from this search result
     * @param limit maximum number of search results to return
     * @return a list of <code>OrganizationAPI</code> matching the query parameters
     */
    public SearchDTO searchOrganizations(String queryString, List<Organization.OrganizationType> organizationTypes, int offset, int limit) {
        LOG.log(Level.FINEST, "searchOrganizations( organizationTypes: {0}, offset: {1}, limit: {2} )", new Object[] {organizationTypes, offset, limit});
        long countOrganizations = countSearchOrganizations(queryString, organizationTypes);
        Query query = em.createNamedQuery(Organization.SEARCH).
                setParameter("query", "%" + queryString + "%").
                setParameter("organizationTypes", organizationTypes);
        if( limit > 0 ) {
            query.setMaxResults(limit);
        }
        List<Organization> organizations = query.setFirstResult(offset).
                getResultList();
        return new SearchDTO(countOrganizations, OrganizationMapper.map(organizations));
    }

    /**
     * Counts the total number of organizations that matches a user supplied search query
     * 
     * @param query user supplied query
     * @param organizationTypes types of organizations to count
     * @return total number of organizations matching the query parameters
     */
    private long countSearchOrganizations(String query, List<Organization.OrganizationType> organizationTypes) {
        LOG.log(Level.FINEST, "countSearchOrganizations( organizationTypes: {0} )", new Object[] {organizationTypes});
        return  (Long) em.createNamedQuery(Organization.COUNT_SEARCH).
                setParameter("query", "%" + query + "%").
                setParameter("organizationTypes", organizationTypes).
                getSingleResult();
    }
    
    /**
     * Delete a specific organization. Only organizations with additional data, 
     * like users can be deleted.
     * 
     * @param uniqueId the unique id of the organization
     * @param userAPI
     * @param sessionId
     * @throws HjalpmedelstjanstenValidationException in case validation fails, e.g. organization has users
     */
    public void deleteOrganization(long uniqueId, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "deleteOrganization( uniqueId: {0} )", new Object[] {uniqueId});
        Organization organization = getOrganization(uniqueId);
        organizationValidation.validateForDelete(organization);
        em.remove(organization);
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.ORGANIZATION, InternalAudit.ActionType.DELETE, userAPI.getId(), sessionId, uniqueId, requestIp));
    }

    /**
     * Find organizations for a specific organization number and type.
     * 
     * @param organizationNumber the organization number
     * @param organizationType type of the organization
     * @return a list of organizations matching the gln and type. this should only be one
     */
    public List<Organization> findByOrganizationNumberAndType(String organizationNumber, Organization.OrganizationType organizationType) {
        LOG.log(Level.FINEST, "findByOrganizationNumberAndType( organizationNumber: {0}, organizationType: {1} )", new Object[] {organizationNumber, organizationType});
        return em.createNamedQuery(Organization.FIND_BY_ORGANIZATION_NUMBER_AND_TYPE).
                setParameter("organizationNumber", organizationNumber).
                setParameter("type", organizationType).
                getResultList();
    }
    
    /**
     * Find organizations for a specific organization gln and type. 
     * 
     * @param gln the organization gln
     * @param organizationType type of the organization
     * @return a list of organizations matching the gln and type. this should only be one
     */
    public List<Organization> findByGlnAndType(String gln, Organization.OrganizationType organizationType) {
        LOG.log(Level.FINEST, "findByGlnAndType( gln: {0}, type: {1} )", new Object[] {organizationType, organizationType});
        return em.createNamedQuery(Organization.FIND_BY_GLN_AND_TYPE).
                setParameter("gln", gln).
                setParameter("type", organizationType).
                getResultList();
    }
    
    /**
     * 
     * @param userAPI
     * @return 
     */
    public Organization getOrganizationFromLoggedInUser(UserAPI userAPI) {
        UserEngagementAPI userEngagementAPI = userAPI.getUserEngagements().get(0);
        Long loggedInUserOrganizationId = userEngagementAPI.getOrganizationId();
        Organization organization = getOrganization(loggedInUserOrganizationId);
        return organization;
    }
    


}

package se.inera.hjalpmedelstjansten.business.helpers;

import javax.ejb.Stateless;

@Stateless
public class ExportHelper {

    private boolean doesTheNumberContainDecimals(Number number) {

        if(number.doubleValue() % 1 == 0) {
            return false;
        } else return true;

    }

    public String parseNumberValueToStringWithCorrectDecimalFormatting(Number number) {

        //If number is not an actual double return long representation of the number
        if(!doesTheNumberContainDecimals(number)) {
            String notADecimalValue = String.valueOf(number.longValue());
            return notADecimalValue;

            // Else return String representation of the double value of number and add visual formatting rules.
        } else {
            return String.valueOf(number.doubleValue()).replaceAll("[.]", ",");
        }
    }
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.organization.controller;

import java.util.logging.Level;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.LoggerService;
import se.inera.hjalpmedelstjansten.model.api.ElectronicAddressAPI;
import se.inera.hjalpmedelstjansten.model.entity.ElectronicAddress;

/**
 *
 * @author Tommy Berglund
 */
public class ElectronicAddressMapper {
    
    private static final HjmtLogger LOG = LoggerService.getLogger(ElectronicAddressMapper.class.getName());
    
    public static ElectronicAddressAPI map(ElectronicAddress electronicAddress ) {
        LOG.log( Level.FINEST, "map(...)" );
        if( electronicAddress == null ) {
            return null;
        }
        ElectronicAddressAPI electronicAddressAPI = new ElectronicAddressAPI();
        electronicAddressAPI.setId(electronicAddress.getUniqueId());
        electronicAddressAPI.setEmail(electronicAddress.getEmail());
        electronicAddressAPI.setFax(electronicAddress.getFax());
        electronicAddressAPI.setMobile(electronicAddress.getMobile());
        electronicAddressAPI.setTelephone(electronicAddress.getTelephone());
        electronicAddressAPI.setWeb(electronicAddress.getWeb());
        return electronicAddressAPI;
    }
    
    public static ElectronicAddress map(ElectronicAddressAPI electronicAddressAPI ) {
        if( electronicAddressAPI == null ) {
            return null;
        }
        ElectronicAddress electronicAddress = new ElectronicAddress();
        electronicAddress.setEmail(electronicAddressAPI.getEmail());
        electronicAddress.setFax(electronicAddressAPI.getFax());
        electronicAddress.setMobile(electronicAddressAPI.getMobile());
        electronicAddress.setTelephone(electronicAddressAPI.getTelephone());
        electronicAddress.setWeb(electronicAddressAPI.getWeb());
        return electronicAddress;
    }
    
}

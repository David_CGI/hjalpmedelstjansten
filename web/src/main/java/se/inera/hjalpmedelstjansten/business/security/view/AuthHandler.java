/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.security.view;

/**
 *
 * @author Tommy Berglund
 */
public interface AuthHandler {
    
    public static final String USER_API_SESSION_KEY = "HJMTJ_USER_API_SESSION_KEY";
    
    public void standardLogin( String username, String password );
    
    public void tokenLogin( String username, String password );
    
    public void addToSession( Object key, Object value );
    
    public Object getFromSession( Object key );
    
    public boolean hasRole( String role );
    
    public boolean hasPermission( String permission );
    
    public String getSessionId();
    
}

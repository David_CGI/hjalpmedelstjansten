/*
* Copyright (C) 2019 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.assortment.view;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.ResultExporterController;
import se.inera.hjalpmedelstjansten.business.assortment.controller.AssortmentController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.AssortmentAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.Assortment;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;

/**
 * REST API for assortments on organization.
 * 
 * @author Tommy Berglund
 */
@Stateless
@Path("organizations/{organizationUniqueId}/assortments")
@Interceptors({ PerformanceLogInterceptor.class })
public class AssortmentOrganizationService extends BaseService {
    
    @Inject
    private HjmtLogger LOG;
    
    @EJB
    AssortmentController assortmentController;
    
    @Inject
    ResultExporterController resultExporterController;
    
    @EJB
    AuthHandler authHandler;
    
    /**
     * Search assortments by user query
     * 
     * @param query the user query
     * @param statuses
     * @param include
     * @param organizationUniqueId
     * @param offset for pagination purpose
     * @return a list of <code>AgreementAPI</code> matching the query and a 
     * header X-Total-Count giving the total number of agreements for pagination
     */
    @GET
    @SecuredService(permissions = {"assortment:view"})
    public Response searchAssortments(
            @DefaultValue(value = "") @QueryParam("query") String query,
            @QueryParam("status") List<Assortment.Status> statuses,
            @QueryParam("include") AssortmentAPI.Include include,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @DefaultValue(value = "0") @QueryParam("offset") int offset) {
        LOG.log(Level.FINEST, "searchAssortments( organizationUniqueId: {0}, offset: {1} )", new Object[] {organizationUniqueId, offset});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        if( query != null && query.isEmpty() ) {
            query = null;
        }
        boolean onlyWhereManager = false;
        if( include != null && include == AssortmentAPI.Include.MINE ) {
            onlyWhereManager = true;
        }
        if( statuses != null && statuses.size() == 3 ) {
            // means we should include all statuses, meaning we don't have to filter on it
            statuses = null;
        }
        SearchDTO searchDTO = assortmentController.searchAssortments(
                organizationUniqueId, 
                query, 
                statuses,
                onlyWhereManager,
                userAPI, 
                offset, 
                25);        
        return Response.ok(searchDTO.getItems()).
                header("X-Total-Count", searchDTO.getCount()).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                build();
    }

    /**
     * Export assortments
     *
     * @param query the user query
     * @param statuses
     * @param include
     * @param organizationUniqueId
     * @return an excel containing the list of search results
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @Path("/export")
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=UTF-8")
    @SecuredService(permissions = {"assortment:view"})
    @TransactionTimeout(value=20, unit = TimeUnit.MINUTES)
    public Response exportAssortments(
            @DefaultValue(value = "") @QueryParam("query") String query,
            @QueryParam("status") List<Assortment.Status> statuses,
            @QueryParam("include") AssortmentAPI.Include include,
            @PathParam("organizationUniqueId") long organizationUniqueId) {
        LOG.log(Level.FINEST, "exportAssortments...");
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        if( query != null && query.isEmpty() ) {
            query = null;
        }

        boolean onlyWhereManager = false;
        if( include != null && include == AssortmentAPI.Include.MINE ) {
            onlyWhereManager = true;
        }

        if( statuses != null && statuses.size() == 3 ) {
            // means we should include all statuses, meaning we don't have to filter on it
            statuses = null;
        }

        int offset = 0;
        int maximumNumberOfResults = 55000;

        SearchDTO searchDTO = assortmentController.searchAssortments(organizationUniqueId, query, statuses, onlyWhereManager, userAPI, offset, maximumNumberOfResults);

        if (searchDTO == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            try {
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'_'HHmm");
                String filename = "Export_Utbud_" + dateTimeFormatter.format(ZonedDateTime.now()) + ".xlsx";

                List<AssortmentAPI> searchAssortmentsAPIs = (List<AssortmentAPI>) searchDTO.getItems();
                byte[] exportBytes = resultExporterController.generateAssortmentsResultList(searchAssortmentsAPIs);

                return Response.ok(
                        exportBytes,
                        javax.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM)
                        .header("content-disposition", "attachment; filename = " + filename)
                        .build();
            } catch (IOException ex) {
                LOG.log(Level.WARNING, "Failed to export users to file", ex);
                return Response.serverError().build();
            }
        }
    }

    /**
     * Get information about a specific assortment 
     * 
     * @param httpServletRequest
     * @param organizationUniqueId the unique id of the organization to get assortment from
     * @param assortmentUniqueId the id of the assortment to return
     * @return the corresponding <code>AssortmentAPI</code>
     */
    @GET
    @Path("{assortmentUniqueId}")
    @SecuredService(permissions = {"assortment:view"})
    public Response getAssortment(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("assortmentUniqueId") long assortmentUniqueId ) {
        LOG.log(Level.FINEST, "getAssortment( organizationUniqueId: {0}, assortmentUniqueId: {1} )", new Object[] {organizationUniqueId, assortmentUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        boolean isSuperAdmin = authHandler.hasRole(UserRole.RoleName.Superadmin.toString());
        if( !isSuperAdmin && !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        AssortmentAPI assortmentAPI = assortmentController.getAssortmentAPI(
                organizationUniqueId, 
                assortmentUniqueId, 
                userAPI, 
                authHandler.getSessionId(), 
                getRequestIp(httpServletRequest));
        if( assortmentAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(assortmentAPI).build();
        }
    }
    
    /**
     * Create new assortment 
     * 
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization to create assortment on
     * @param assortmentAPI user supplied values
     * @return the created <code>AssortmentAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @SecuredService(permissions = {"assortment:create"})
    public Response createAssortment(@Context HttpServletRequest httpServletRequest, 
            @PathParam("organizationUniqueId") long organizationUniqueId, 
            AssortmentAPI assortmentAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createAssortment( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        assortmentAPI = assortmentController.createAssortment(
                organizationUniqueId, 
                assortmentAPI, 
                userAPI, 
                authHandler.getSessionId(), 
                getRequestIp(httpServletRequest));
        return Response.
                ok(assortmentAPI).
                build();
    }
    
    /**
     * Update assortment 
     * 
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization to update assortment on
     * @param assortmentUniqueId
     * @param assortmentAPI user supplied values
     * @return the created <code>AssortmentAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @PUT
    @Path("{assortmentUniqueId}")
    @SecuredService(permissions = {"assortment:update"})
    public Response updateAssortment(@Context HttpServletRequest httpServletRequest, 
            @PathParam("organizationUniqueId") long organizationUniqueId, 
            @PathParam("assortmentUniqueId") long assortmentUniqueId, 
            AssortmentAPI assortmentAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateAssortment( organizationUniqueId: {0}, assortmentUniqueId: {1} )", new Object[] {organizationUniqueId, assortmentUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        boolean isSuperAdmin = authHandler.hasRole(UserRole.RoleName.Superadmin.toString());
        if( !isSuperAdmin && !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        assortmentAPI = assortmentController.updateAssortment(
                organizationUniqueId, 
                assortmentUniqueId, 
                assortmentAPI, 
                userAPI, 
                authHandler.getSessionId(), 
                getRequestIp(httpServletRequest),
                isSuperAdmin);
        return Response.
                ok(assortmentAPI).
                build();
    }
    
    /**
     * Read articles on assortment 
     * 
     * @param httpServletRequest
     * @param query
     * @param offset
     * @param organizationUniqueId unique id of the organization to update assortment on
     * @param assortmentUniqueId
     * @return a list of <code>ArticleAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @GET
    @Path("{assortmentUniqueId}/articles")
    @SecuredService(permissions = {"assortmentarticle:view"})
    public Response searchArticlesOnAssortment(@Context HttpServletRequest httpServletRequest, 
            @DefaultValue(value = "") @QueryParam("query") String query, 
            @DefaultValue(value = "0") @QueryParam("offset") int offset,
            @PathParam("organizationUniqueId") long organizationUniqueId, 
            @PathParam("assortmentUniqueId") long assortmentUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "addArticlesToAssortment( organizationUniqueId: {0}, assortmentUniqueId: {1} )", new Object[] {organizationUniqueId, assortmentUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        boolean isSuperAdmin = authHandler.hasRole(UserRole.RoleName.Superadmin.toString());
        if( !isSuperAdmin && !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        SearchDTO searchDTO = assortmentController.searchArticlesOnAssortment(
                query,
                offset,
                25,
                organizationUniqueId, 
                assortmentUniqueId, 
                userAPI, 
                authHandler.getSessionId(), 
                getRequestIp(httpServletRequest));
        if( searchDTO == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(searchDTO.getItems()).
                    header("X-Total-Count", searchDTO.getCount()).
                    header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                    build();
        }
    }



    /**
     * Export articles for a specific assortment
     *
     * @param httpServletRequest
     * @param query
     * @param organizationUniqueId unique id of the organization
     * @param assortmentUniqueId
     * @return an excel containing the list of search results
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @Path("{assortmentUniqueId}/export")
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=UTF-8")
    @SecuredService(permissions = {"assortmentarticle:view"})
    @TransactionTimeout(value=20, unit = TimeUnit.MINUTES)
    public Response exportArticlesForAssortment(
            @Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("assortmentUniqueId") long assortmentUniqueId,
            @DefaultValue(value = "") @QueryParam("query") String query) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "exportArticlesOnAssortment( organizationUniqueId: {0}, assortmentUniqueId: {1} )", new Object[]{organizationUniqueId, assortmentUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        boolean isSuperAdmin = authHandler.hasRole(UserRole.RoleName.Superadmin.toString());
        if (!isSuperAdmin && !authorizeHandleOrganization(organizationUniqueId, userAPI)) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        int maximumNumberOfResults = 55000;

        SearchDTO searchDTO = assortmentController.searchArticlesOnAssortment(query,
                0,
                maximumNumberOfResults,
                organizationUniqueId,
                assortmentUniqueId,
                userAPI,
                authHandler.getSessionId(),
                getRequestIp(httpServletRequest));

        if (searchDTO == null) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            try {
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'_'HHmm");
                String filename = "Export_ArtiklarUtbud_" + dateTimeFormatter.format(ZonedDateTime.now()) + ".xlsx";

                List<ArticleAPI> searchArticlesAPIs = (List<ArticleAPI>) searchDTO.getItems();

                byte[] exportBytes = resultExporterController.generateArticlesList(searchArticlesAPIs);

                return Response.ok(
                        exportBytes,
                        javax.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM)
                        .header("content-disposition", "attachment; filename = " + filename)
                        .build();
                } catch (IOException ex) {
                    LOG.log(Level.WARNING, "Failed to export articles for assortment to file", ex);
                    return Response.serverError().build();
                }
            }
        }


    /**
     * Add article to assortment 
     * 
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization to update assortment on
     * @param assortmentUniqueId
     * @param articleAPIs articles to add
     * @return 200 ok or 400 if validation fails
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @Path("{assortmentUniqueId}/articles")
    @SecuredService(permissions = {"assortmentarticle:create_all", "assortmentarticle:create_own"})
    public Response addArticlesToAssortment(@Context HttpServletRequest httpServletRequest, 
            @PathParam("organizationUniqueId") long organizationUniqueId, 
            @PathParam("assortmentUniqueId") long assortmentUniqueId, 
            List<ArticleAPI> articleAPIs) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "addArticlesToAssortment( organizationUniqueId: {0}, assortmentUniqueId: {1} )", new Object[] {organizationUniqueId, assortmentUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        boolean hasAllPermissions = authHandler.hasPermission("assortmentarticle:create_all");
        AssortmentAPI assortmentAPI = assortmentController.addArticlesToAssortment(
                organizationUniqueId, 
                assortmentUniqueId, 
                articleAPIs, 
                userAPI, 
                authHandler.getSessionId(), 
                getRequestIp(httpServletRequest), 
                hasAllPermissions);
        if( assortmentAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.
                    ok().
                    build();
        }
    }
    
    /**
     * Delete articles from assortment 
     * 
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization to update assortment on
     * @param assortmentUniqueId
     * @param articleUniqueId unique id of the article to remove from assortment
     * @return 200 ok or 400 if validation fails
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @DELETE
    @Path("{assortmentUniqueId}/articles/{articleUniqueId}")
    @SecuredService(permissions = {"assortmentarticle:delete_all", "assortmentarticle:delete_own"})
    public Response deleteArticleFromAssortment(@Context HttpServletRequest httpServletRequest, 
            @PathParam("organizationUniqueId") long organizationUniqueId, 
            @PathParam("assortmentUniqueId") long assortmentUniqueId, 
            @PathParam("articleUniqueId") long articleUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "deleteArticleFromAssortment( organizationUniqueId: {0}, assortmentUniqueId: {1}, articleUniqueId: {2} )", new Object[] {organizationUniqueId, assortmentUniqueId, articleUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        boolean hasAllPermissions = authHandler.hasPermission("assortmentarticle:delete_all");
        AssortmentAPI assortmentAPI = assortmentController.deleteArticleFromAssortment(
                organizationUniqueId, 
                assortmentUniqueId, 
                articleUniqueId, 
                userAPI, 
                authHandler.getSessionId(), 
                getRequestIp(httpServletRequest), 
                hasAllPermissions);
        if( assortmentAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.
                    ok().
                    build();
        }
    }
}

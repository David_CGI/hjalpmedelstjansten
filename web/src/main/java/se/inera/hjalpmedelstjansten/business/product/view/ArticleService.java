/*
* Copyright (C) 2019 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.product.view;

import java.util.List;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistPricelistRowController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleController;
import se.inera.hjalpmedelstjansten.business.media.controller.MediaController;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.BooleanAPI;
import se.inera.hjalpmedelstjansten.model.api.GeneralPricelistPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.entity.media.*;

/**
 * REST API for articles.
 * 
 * @author Tommy Berglund
 */
@Stateless
@Path("organizations/{organizationUniqueId}/articles")
@Interceptors({ PerformanceLogInterceptor.class })
public class ArticleService extends BaseService {
    
    @Inject
    private HjmtLogger LOG;

    @EJB
    GeneralPricelistPricelistRowController generalPricelistPricelistRowController;

    @EJB
    private ArticleController articleController;

    @EJB
    private MediaController mediaController;
    
    @EJB
    private AuthHandler authHandler;
    
    /**
     * Get information about a specific article 
     * 
     * @param httpServletRequest
     * @param organizationUniqueId the unique id of the organization to get article from
     * @param uniqueId the id of the article to return
     * @return the corresponding <code>ArticleAPI</code>
     */
    @GET
    @Path("{uniqueId}")
    @SecuredService(permissions = {"article:view"})
    public Response getArticle(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("uniqueId") long uniqueId ) {
        LOG.log(Level.FINEST, "getArticle( organizationUniqueId: {0}, uniqueId: {1} )", new Object[] {organizationUniqueId, uniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        ArticleAPI articleAPI = articleController.getArticleAPI( organizationUniqueId, uniqueId, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest) );
        if( articleAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(articleAPI).build();
        }
    }
    
    /**
     * Get information about which pricelist rows a specific article appears on
     * 
     * @param organizationUniqueId the unique id of the organization to get article from
     * @param uniqueId the id of the article
     * @return a list of <code>AgreementPricelistRowAPI</code>
     */
    @GET
    @Path("{uniqueId}/pricelistrows")
    @SecuredService(permissions = {"pricelistrow:view_own"})
    public Response getArticlePricelistRows(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("uniqueId") long uniqueId ) {
        LOG.log(Level.FINEST, "getArticle( organizationUniqueId: {0}, uniqueId: {1} )", new Object[] {organizationUniqueId, uniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        List<AgreementPricelistRowAPI> agreementPricelistRowAPIs = articleController.getArticlePricelistRows( uniqueId, userAPI );
        return Response.ok(agreementPricelistRowAPIs).build();
    }
     
    /**
     * Get information about which general pricelist rows a specific article appears on
     * 
     * @param organizationUniqueId the unique id of the organization to get article from
     * @param uniqueId the id of the article
     * @return a list of <code>GeneralPricelistPricelistRowAPI</code>
     */
    @GET
    @Path("{uniqueId}/generalpricelistrows")
    @SecuredService(permissions = {"generalpricelist_pricelistrow:view_own","generalpricelist_pricelistrow:view_all"})
    public Response getArticleGeneralPricelistRows(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("uniqueId") long uniqueId ) {
        LOG.log(Level.FINEST, "getArticleGeneralPricelistRows( organizationUniqueId: {0}, uniqueId: {1} )", new Object[] {organizationUniqueId, uniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        List<GeneralPricelistPricelistRowAPI> generalPricelistPricelistRowAPIs = articleController.getArticleGeneralPricelistRows( uniqueId, userAPI, false );
        return Response.ok(generalPricelistPricelistRowAPIs).build();
    }
    
    /**
     * Get information about if article appears in pricelist. Currently only 
     * customers may call this, but the permission is set for all organization types
     * , the controller method checks organization type. If the controller returns
     * null, this is interpreted as permission is missing
     * 
     * @param organizationUniqueId the unique id of the organization 
     * @param uniqueId the id of the article
     * @return a <code>BooleanAPI</code> whether the article exists in an agreement or not
     */
    @GET
    @Path("{uniqueId}/pricelistrowsexist")
    @SecuredService(permissions = {"pricelistrow:view_exists"})
    public Response getArticleExistsPricelistRows(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("uniqueId") long uniqueId ) {
        LOG.log(Level.FINEST, "getArticleExistsPricelistRows( organizationUniqueId: {0}, uniqueId: {1} )", new Object[] {organizationUniqueId, uniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        BooleanAPI booleanAPI = articleController.getArticleExistsPricelistRows(uniqueId, userAPI);
        if( booleanAPI == null ) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        } else {
            return Response.ok(booleanAPI).build();
        }
    }
    
    /**
     * Create new article 
     * 
     * @param httpServletRequest
     * @param organizationUniqueId unique id of the organization to create article on
     * @param articleAPI user supplied values
     * @return the created <code>ArticleAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    @POST
    @SecuredService(permissions = {"article:create_own"})
    public Response createArticle(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId, 
            ArticleAPI articleAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createArticle( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization( organizationUniqueId, userAPI ) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        articleAPI = articleController.createArticle(organizationUniqueId, 
                articleAPI, 
                userAPI, 
                authHandler.getSessionId(), 
                getRequestIp(httpServletRequest), 
                true);
        return Response.ok(articleAPI).build();
    }
    
    /**
     * Update an existing <code>Article</code>
     * 
     * @param organizationUniqueId unique id of organization
     * @param uniqueId the id of the article to update
     * @param articleAPI user supplied values
     * @return the updated <code>ArticleAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException in case validation fails
     */
    @PUT
    @Path("{uniqueId}")
    @SecuredService(permissions = {"article:update_own"})
    public Response updateArticle(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId, 
            @PathParam("uniqueId") long uniqueId, 
            ArticleAPI articleAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateArticle( uniqueId: {0} )", new Object[] {uniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization( organizationUniqueId, userAPI ) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        articleAPI = articleController.updateArticle(organizationUniqueId, 
                uniqueId, 
                articleAPI, 
                userAPI, 
                authHandler.getSessionId(), 
                getRequestIp(httpServletRequest), 
                true);
        if( articleAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(articleAPI).build();
        }
    }

    /**
     * Delete article
     *
     * @param organizationUniqueId unique id of organization
     * @param uniqueId the id of the article to delete
     * @return response code 200 if delete is successful, otherwise 404 if the user
     * cannot read the article or 400 if there is a validation error
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException in case validation fails
     */
    @DELETE
    @Path("{uniqueId}")
    @SecuredService(permissions = {"article:update_own"})
    public Response deleteArticle(@Context HttpServletRequest httpServletRequest,
                                        @PathParam("organizationUniqueId") long organizationUniqueId,
                                        @PathParam("uniqueId") long uniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "deleteArticle( uniqueId: {0} )", new Object[]{uniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if (!authorizeHandleOrganization(organizationUniqueId, userAPI)) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        List<ArticleMediaVideo> articleMediaVideos = mediaController.getArticleMediaVideos(uniqueId);
        List<ArticleMediaImage> articleMediaImages = mediaController.getArticleMediaImages(uniqueId);
        List<ArticleMediaDocument> articleMediaDocuments = mediaController.getArticleMediaDocuments(uniqueId);

        if (articleMediaImages != null) {
            for (ArticleMediaImage media : articleMediaImages) {
                mediaController.deleteArticleMedia(articleController.getArticle(organizationUniqueId, uniqueId, userAPI), media, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
            }
        }
        if (articleMediaDocuments != null) {
            for (ArticleMediaDocument media : articleMediaDocuments) {
                mediaController.deleteArticleMedia(articleController.getArticle(organizationUniqueId, uniqueId, userAPI), media, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
            }
        }
        if (articleMediaVideos != null) {
            for (ArticleMediaVideo media : articleMediaVideos) {
                mediaController.deleteArticleMedia(articleController.getArticle(organizationUniqueId, uniqueId, userAPI), media, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
            }
        }

        List<GeneralPricelistPricelistRowAPI> generalPricelistPricelistRowAPIs = articleController.getArticleGeneralPricelistRows( uniqueId, userAPI, true );
        if(generalPricelistPricelistRowAPIs != null && generalPricelistPricelistRowAPIs.size() > 0)
        {
            for(GeneralPricelistPricelistRowAPI row : generalPricelistPricelistRowAPIs)
            {
                generalPricelistPricelistRowController.deleteRow(organizationUniqueId, row.getPricelist().getId(), row.getId(), userAPI,authHandler.getSessionId(),getRequestIp(httpServletRequest));
            }
        }

        articleController.delete(organizationUniqueId, uniqueId, userAPI);
        return Response.ok().build();
    }

    /**
     * Delete connection to/from article
     * 
     * @param organizationUniqueId unique id of organization
     * @param uniqueId the id of the article to update
     * @param fitsToUniqueId unique id of the article that fits to this article or
     * that this article fits to
     * @return response code 200 if delete is successful, otherwise 404 if the user
     * cannot read the article or the article that fits to or 400 if there is a validation
     * error
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException in case validation fails
     */
    @DELETE
    @Path("{uniqueId}/fitsto/{fitsToUniqueId}")
    @SecuredService(permissions = {"article:update_own"})
    public Response deleteFitsToArticle(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId, 
            @PathParam("uniqueId") long uniqueId,
            @PathParam("fitsToUniqueId") long fitsToUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateArticle( uniqueId: {0} )", new Object[] {uniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization( organizationUniqueId, userAPI ) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        ArticleAPI articleAPI = articleController.deleteFitsTo(organizationUniqueId, uniqueId, fitsToUniqueId, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( articleAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(articleAPI).build();
        }
    }

    /**
     * Update an existing <code>Article</code>s based on product
     * 
     * @param httpServletRequest
     * @param organizationUniqueId unique id of organization
     * @param articleUniqueId the id of the article to update
     * @param productUniqueId the id of the new product to be based on
     * @return the updated <code>ArticleAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException in case validation fails
     */
    @POST
    @Path("{articleUniqueId}/switchbasedon/{productUniqueId}")
    @SecuredService(permissions = {"article:update_own"})
    public Response switchArticlesBasedOn(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId, 
            @PathParam("articleUniqueId") long articleUniqueId,
            @PathParam("productUniqueId") long productUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateArticle( articleUniqueId: {0} )", new Object[] {articleUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization( organizationUniqueId, userAPI ) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        ArticleAPI articleAPI = articleController.switchArticleBasedOn(organizationUniqueId, 
                articleUniqueId, 
                productUniqueId, 
                userAPI, 
                authHandler.getSessionId(), 
                getRequestIp(httpServletRequest));
        if( articleAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(articleAPI).build();
        }
    }
    
}

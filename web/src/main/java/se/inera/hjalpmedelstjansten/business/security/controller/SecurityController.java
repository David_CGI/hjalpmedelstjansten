/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.security.controller;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import java.util.Random;
import java.util.logging.Level;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.ejb.Stateless;
import javax.inject.Inject;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;

/**
 * Contains methods for generating passwords, salts etc. Based on OWASP Java 
 * password hashing. Describe here: https://www.owasp.org/index.php/Hashing_Java
 * 
 * @author Tommy Berglund
 */
@Stateless
public class SecurityController {
   
    @Inject
    private HjmtLogger LOG;
    
    @Inject
    private int hashingMinIterations;
    
    @Inject
    private int hashingMaxIterations;
    
    /**
     * Should log time taken to generate to make sure generations takes at least 
     * 500 ms in production
     * 
     * @param passwordToHash
     * @param salt
     * @param iterations
     * @return 
     */
    public String hashPassword(String passwordToHash, String salt, int iterations) {
        try {
            long start = System.currentTimeMillis();
            SecretKeyFactory skf = SecretKeyFactory.getInstance( "PBKDF2WithHmacSHA512" );
            PBEKeySpec spec = new PBEKeySpec( passwordToHash.toCharArray(), salt.getBytes(), iterations, 256 );
            SecretKey key = skf.generateSecret( spec );
            byte[] res = key.getEncoded( );
            long total = System.currentTimeMillis() - start;
            LOG.log( Level.FINEST, "hashing password took: {0} ms", new Object[] {total} );
            return Base64.getEncoder().encodeToString(res); 
       } catch( NoSuchAlgorithmException | InvalidKeySpecException e ) {
           throw new RuntimeException( e );
       }
    }
    
    /**
     * Valid passwords are:
     * - minimum 8 characters
     * - not the same as username
     * - not the same as email
     * @param passwordToValidate
     * @param username
     * @param email
     * @return true if validation passes
     */
    public boolean validatePassword( String passwordToValidate, String username, String email ) {
        if( passwordToValidate == null ) {
            LOG.log( Level.FINEST, "Password validation failed, password is null" );
            return false;
        }
        if( passwordToValidate.length() < 8 ) {
            LOG.log( Level.FINEST, "Password validation failed, too short" );
            return false;
        }
        if( passwordToValidate.equals(username) || passwordToValidate.equals(email) ) {
            LOG.log( Level.FINEST, "Password validation failed, matches username or email" );
            return false;
        }
        return true;
    }
    
    /**
     * Generate a random salt for password storage
     * 
     * @return 
     */
    public String generateSalt() {
        final Random random = new SecureRandom();
        byte[] salt = new byte[32];
        random.nextBytes(salt);
        return Base64.getEncoder().encodeToString(salt);
    }

    /**
     * Generate a random iteration count. Password should take around 500ms to
     * hash. The more generations, the longer the password takes to hash.
     * 
     * @return 
     */
    public int generateIterations() {
        final Random random = new SecureRandom();
        return random.nextInt(hashingMaxIterations-hashingMinIterations) + hashingMinIterations;
    }
    
    /**
     * Generate a token to include in link to user email when an account is
     * created.
     * 
     * @return 
     */
    public String generateEmailLinkToken() {
        final Random random = new SecureRandom();
        byte[] salt = new byte[32];
        random.nextBytes(salt);
        return Base64.getEncoder().encodeToString(salt);
    }
    
}

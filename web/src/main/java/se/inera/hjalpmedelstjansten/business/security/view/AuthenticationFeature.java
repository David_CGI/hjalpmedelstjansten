/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.security.view;

import java.lang.reflect.Method;
import java.util.logging.Level;
import javax.inject.Inject;
import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.ext.Provider;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;

/**
 * This feature checks the APIs for the <code>SecuredService</code> annotation
 * and adds the <code>AuthenticationFilter</code> to it. 
 * 
 * @see se.inera.hjalpmedelstjansten.business.security.view.AuthenticationFilter
 * @see se.inera.hjalpmedelstjansten.business.security.view.SecuredService
 * @author Tommy Berglund
 */
@Provider
public class AuthenticationFeature implements DynamicFeature {

    @Inject
    private HjmtLogger LOG;
    
    @Override
    public void configure(ResourceInfo resourceInfo, FeatureContext context) {
        Method method = resourceInfo.getResourceMethod();
        LOG.log(Level.FINEST, "Checking for security annotation class: {0}, method: {1}", new Object[] {method.getDeclaringClass().getName(), method.getName()});
        if( method.isAnnotationPresent(SecuredService.class) ) {
            LOG.log(Level.FINEST, "Securing method: {0}", new Object[] {method.getName()} );
            SecuredService securedService = method.getAnnotation(SecuredService.class);
            AuthenticationFilter authenticationFilter = new AuthenticationFilter(securedService.permissions());
            context.register(authenticationFilter);
        } else {
            LOG.log(Level.INFO, "Method: {0} in class: {1} is NOT secured", new Object[] {method.getName(), method.getDeclaringClass().getName()} );
        }
    }
    
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.product.controller;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import se.inera.hjalpmedelstjansten.model.api.SearchProductsAndArticlesAPI;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Product;

/**
 *
 * @author Tommy Berglund
 */
public class SearchMapper {
    
    static List<SearchProductsAndArticlesAPI> map(List<Object[]> objectArrayList) {
        if( objectArrayList == null || objectArrayList.isEmpty() ) {
            return null;
        }
        List<SearchProductsAndArticlesAPI> searchProductsAndArticlesAPIs = new ArrayList<>();
        for( Object[] objectArray : objectArrayList ) {
            SearchProductsAndArticlesAPI searchProductsAndArticlesAPI = new SearchProductsAndArticlesAPI();
            searchProductsAndArticlesAPI.setType(SearchProductsAndArticlesAPI.Type.valueOf((String) objectArray[0]));
            searchProductsAndArticlesAPI.setId(((BigInteger) objectArray[1]).longValue());
            searchProductsAndArticlesAPI.setName((String) objectArray[2]);
            searchProductsAndArticlesAPI.setNumber((String) objectArray[3]);
            searchProductsAndArticlesAPI.setStatus(Product.Status.valueOf((String) objectArray[4]));
            String code = (String) objectArray[5];
            String code2 = (String) objectArray[6];
            if( searchProductsAndArticlesAPI.getType() == SearchProductsAndArticlesAPI.Type.ARTICLE ) {
                if( code == null || code.isEmpty() ) {
                    code = code2;
                }
            }
            searchProductsAndArticlesAPI.setCode(code);
            String articleType = ( String ) objectArray[7];
            String articleType2 = ( String ) objectArray[8];
            if( searchProductsAndArticlesAPI.getType() == SearchProductsAndArticlesAPI.Type.ARTICLE ) {
                if( articleType == null || articleType.isEmpty() ) {
                    articleType = articleType2;
                }
            }
            searchProductsAndArticlesAPI.setArticleType(Article.Type.valueOf(articleType));
            if( objectArray.length > 9 ) {
                // in some cases, result include information about deleteable
                searchProductsAndArticlesAPI.setDeleteable(Boolean.valueOf((String) objectArray[9]));
            }
            searchProductsAndArticlesAPIs.add(searchProductsAndArticlesAPI);
        }
        return searchProductsAndArticlesAPIs;
    }
    
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.logging.controller;

import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.DependsOn;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.inject.Inject;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.clustering.ClusterController;

/**
 * Timer for deleting internal audits older than a certain number of months
 * 
 * @author Tommy Berglund
 */
@Singleton
@Startup
@DependsOn("PropertyLoader")
public class InternalAuditCleanerTimer {

    @Inject
    HjmtLogger LOG;
    
    @Inject
    private boolean internalAuditCleanerTimerEnabled;
    
    @Inject
    private String internalAuditCleanerTimerHour;
    
    @Inject
    private String internalAuditCleanerTimerMinute;
    
    @Inject
    private String internalAuditCleanerTimerSecond;
    
    @Inject
    private InternalAuditController internalAuditController;
    
    @Inject
    private int internalAuditCleanerTimerMaxAgeMonths;
    
    @Inject
    private ClusterController clusterController;
    
    @Resource
    private TimerService timerService;
    
    @Timeout
    private void schedule() {
        LOG.log( Level.FINEST, "schedule" );
        boolean lockReceived = clusterController.getLock(this.getClass().getName(), 30);
        if( lockReceived ) {
            LOG.log( Level.FINEST, "Received lock!" );
            long start = System.currentTimeMillis();
            internalAuditController.removeInternalAuditsOlderThan(internalAuditCleanerTimerMaxAgeMonths);
            long total = System.currentTimeMillis() - start;
            if( total > 60000 ) {
                LOG.log( Level.WARNING, "Scheduled job took: {0} ms which is more than 1 minute which is a long time, a developer should take a look at this.", new Object[] {total});
            } else {
                LOG.log( Level.INFO, "Scheduled job took: {0} ms.", new Object[] {total});
            }
        } else {
            LOG.log( Level.INFO, "Did not received lock!" );
        }
    }
    
    @PostConstruct
    private void initialize() {
        LOG.log( Level.FINEST, "initialize()" );
        if( internalAuditCleanerTimerEnabled ) {
            LOG.log( Level.FINEST, "Timer is ENABLED" );
            if (timerService.getTimers().isEmpty()) {
                String name = this.getClass().getName();
                TimerConfig configuration = new TimerConfig();
                configuration.setPersistent(false);
                configuration.setInfo(name);
                ScheduleExpression scheduleExpression = new ScheduleExpression();
                scheduleExpression.hour(internalAuditCleanerTimerHour).minute(internalAuditCleanerTimerMinute).second(internalAuditCleanerTimerSecond);
                timerService.createCalendarTimer(scheduleExpression, configuration);
            }            
        } else {
            LOG.log( Level.INFO, "Timer is NOT ENABLED" );
        }
    }
    
}

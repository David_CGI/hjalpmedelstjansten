/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.agreement.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import se.inera.hjalpmedelstjansten.business.user.controller.UserMapper;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistAPI;
import se.inera.hjalpmedelstjansten.model.entity.Agreement;
import se.inera.hjalpmedelstjansten.model.entity.AgreementPricelist;

/**
 * Class for mapping between API and Entity classes
 * 
 * @author Tommy Berglund
 */
public class AgreementPricelistMapper {
    
    public static final List<AgreementPricelistAPI> map(List<AgreementPricelist> pricelists, boolean includeEverything, AgreementPricelist currentPricelist) {
        if( pricelists == null ) {
            return null;
        }
        List<AgreementPricelistAPI> pricelistAPIs = new ArrayList<>();
        for( AgreementPricelist pricelist : pricelists ) {
            pricelistAPIs.add(mapWithPricelist(pricelist, includeEverything, currentPricelist));
        }
        return pricelistAPIs;
    }
        
    public static final AgreementPricelistAPI mapWithPricelist(AgreementPricelist pricelist, boolean includeEverything, AgreementPricelist currentPricelist) {
        if( pricelist == null ) {
            return null;
        }
        return mapWithStatus(pricelist, includeEverything, getPricelistStatus(pricelist, currentPricelist), null);
    }

    public static final AgreementPricelistAPI mapWithStatus(AgreementPricelist pricelist, boolean includeEverything, AgreementPricelist.Status status, Long numberOfPricelistRows) {
        if( pricelist == null ) {
            return null;
        }
        AgreementPricelistAPI pricelistAPI = new AgreementPricelistAPI();
        pricelistAPI.setId(pricelist.getUniqueId());
        pricelistAPI.setNumber(pricelist.getNumber());
        pricelistAPI.setValidFrom(pricelist.getValidFrom().getTime());
        pricelistAPI.setStatus(status == null ? null: status.toString());
        pricelistAPI.setHasPricelistRows(numberOfPricelistRows == null ? false: numberOfPricelistRows > 0);
        if( includeEverything ) {
            Agreement agreement = pricelist.getAgreement();
            pricelistAPI.setAgreement(AgreementMapper.map(agreement, false));
            // in this specific case, we also need the pricelist approvers
            if( agreement.getCustomerPricelistApprovers() != null && !agreement.getCustomerPricelistApprovers().isEmpty() ) {
                pricelistAPI.getAgreement().setCustomerPricelistApprovers(UserMapper.map(agreement.getCustomerPricelistApprovers(), false));
            }
        }
        return pricelistAPI;
    }
    
    public static final AgreementPricelist map(AgreementPricelistAPI pricelistAPI) {
        if( pricelistAPI == null ) {
            return null;
        }
        AgreementPricelist pricelist = new AgreementPricelist();
        pricelist.setNumber(pricelistAPI.getNumber());
        pricelist.setValidFrom(new Date(pricelistAPI.getValidFrom()));
        return pricelist;
    }
    
    public static AgreementPricelist.Status getPricelistStatus(AgreementPricelist pricelist, AgreementPricelist currentPricelist) {
        Date now = new Date();
        AgreementPricelist.Status status;

        Agreement agreement = pricelist.getAgreement();
        if (agreement != null && agreement.getStatus() != Agreement.Status.DISCONTINUED) {
            if (now.before(pricelist.getValidFrom())) {
                status = AgreementPricelist.Status.FUTURE;
            } else {
                // now is same or after pricelist valid from
                if (currentPricelist == null) {
                    status = AgreementPricelist.Status.CURRENT;
                } else {
                    if (pricelist.getUniqueId().equals(currentPricelist.getUniqueId())) {
                        status = AgreementPricelist.Status.CURRENT;
                    } else {
                        status = AgreementPricelist.Status.PAST;
                    }
                }
            }
        } else {
            status = AgreementPricelist.Status.PAST;
        }
        return status;
    }
    
}

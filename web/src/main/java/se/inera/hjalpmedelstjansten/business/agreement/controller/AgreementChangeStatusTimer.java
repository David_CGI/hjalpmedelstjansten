/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.agreement.controller;

import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.DependsOn;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.inject.Inject;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.clustering.ClusterController;

/**
 * Timer for discontinuing agreements where the validTo is passed or where endDate
 * has been entered and is passed.
 * 
 * @author Tommy Berglund
 */
@Singleton
@Startup
@DependsOn("PropertyLoader")
public class AgreementChangeStatusTimer {

    @Inject
    private HjmtLogger LOG;
    
    @Inject
    private boolean agreementChangeStatusTimerEnabled;
    
    @Inject
    private String agreementChangeStatusTimerHour;
    
    @Inject
    private String agreementChangeStatusTimerMinute;
    
    @Inject
    private String agreementChangeStatusTimerSecond;
    
    @Inject
    private AgreementController agreementController;
    
    @Inject
    private ClusterController clusterController;
    
    @Resource
    private TimerService timerService;
    
    @Timeout
    private void schedule() {
        LOG.log( Level.FINEST, "schedule" );
        // attempt to get lock
        boolean lockReceived = clusterController.getLock(this.getClass().getName(), 30);
        if( lockReceived ) {
            LOG.log( Level.FINEST, "Received lock!" );
            long start = System.currentTimeMillis();
            agreementController.changeAgreementStatusByDate();
            long total = System.currentTimeMillis() - start;
            if( total > 60000 ) {
                LOG.log( Level.WARNING, "Scheduled job took: {0} ms which is more than 1 minute which is a long time, a developer should take a look at this.", new Object[] {total});
            } else {
                LOG.log( Level.INFO, "Scheduled job took: {0} ms.", new Object[] {total});
            }
        } else {
            LOG.log( Level.INFO, "Did not receive lock!" );
        }
    }
    
    @PostConstruct
    private void initialize() {
        LOG.log( Level.FINEST, "initialize()" );
        if( agreementChangeStatusTimerEnabled ) {
            LOG.log( Level.FINEST, "Timer is ENABLED" );
            if (timerService.getTimers().isEmpty()) {
                String name = this.getClass().getName();
                TimerConfig configuration = new TimerConfig();
                configuration.setPersistent(false);
                configuration.setInfo(name);
                ScheduleExpression scheduleExpression = new ScheduleExpression();
                scheduleExpression.
                        hour(agreementChangeStatusTimerHour).
                        minute(agreementChangeStatusTimerMinute).
                        second(agreementChangeStatusTimerSecond);
                timerService.createCalendarTimer(scheduleExpression, configuration);
            }
        } else {
            LOG.log( Level.INFO, "Timer is NOT ENABLED" );
        }
    }
    
}

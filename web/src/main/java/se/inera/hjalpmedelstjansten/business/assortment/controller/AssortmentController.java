/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.assortment.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import se.inera.hjalpmedelstjansten.business.BaseController;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleController;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleMapper;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.ArticleAPI;
import se.inera.hjalpmedelstjansten.model.api.AssortmentAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVMunicipalityAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Assortment;
import se.inera.hjalpmedelstjansten.model.entity.InternalAudit;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.UserEngagement;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCounty;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVMunicipality;

/**
 * Class for handling business logic of Assortments. This includes talking to the
 * database. Only customers can handle assortments.
 * 
 * @author Tommy Berglund
 */
@Stateless
public class AssortmentController extends BaseController {
    
    @Inject
    HjmtLogger LOG;
    
    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;
    
    @Inject
    AssortmentValidation assortmentValidation;
    
    @Inject
    ValidationMessageService validationMessageService;

    @Inject
    UserController userController;
    
    @Inject
    OrganizationController organizationController;
    
    @Inject
    CountyController countyController;
    
    @Inject
    ArticleController articleController;

    @Inject
    Event<InternalAuditEvent> internalAuditEvent;
    
    /**
     * Create an assortment on the <code>Organization</code> with the given id.
     * 
     * @param organizationUniqueId unique id of the Organization
     * @param assortmentAPI user supplied values
     * @param userAPI user information, needed for business levels validation
     * @param sessionId
     * @param requestIp
     * @return the mapped <code>ArticleAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    public AssortmentAPI createAssortment(long organizationUniqueId, AssortmentAPI assortmentAPI, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createAssortment( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        assortmentValidation.validateForCreate(assortmentAPI);
        Assortment assortment = AssortmentMapper.map(assortmentAPI);

        // add customer organization
        setCustomer(assortment, organizationUniqueId);

        // add managers
        setAssortmentManagers(organizationUniqueId, assortmentAPI, assortment, userAPI);
        
        // save it
        em.persist(assortment);
                
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.ASSORTMENT, InternalAudit.ActionType.CREATE, userAPI.getId(), sessionId, assortment.getUniqueId(), requestIp));
        return AssortmentMapper.map(assortment);
    }
    
    /**
     * Update an assortment with the given id. 
     * 
     * @param organizationUniqueId unique id of the Organization
     * @param assortmentUniqueId
     * @param assortmentAPI user supplied values
     * @param userAPI user information, needed for business levels validation
     * @param sessionId
     * @param requestIp
     * @param isSuperAdmin
     * @return the mapped <code>ArticleAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    public AssortmentAPI updateAssortment(long organizationUniqueId, 
            long assortmentUniqueId, 
            AssortmentAPI assortmentAPI, 
            UserAPI userAPI, 
            String sessionId, 
            String requestIp, 
            boolean isSuperAdmin) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createAssortment( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        Assortment assortment = getAssortment(organizationUniqueId, assortmentUniqueId, userAPI);
        if( assortment == null ) {
            // assortment either does not exist or the user is not allowed to view it
            return null;
        }
        assortmentValidation.validateForUpdate(assortmentAPI, assortment, isSuperAdmin);
        
        if( isSuperAdmin ) {
            // if this is superadmin, then we may set county and municipality
            setCountyAndMunicipalities(assortmentAPI, assortment);
        } else {
            // update fields
            assortment.setName(assortmentAPI.getName());
            assortment.setValidTo(assortmentAPI.getValidTo() == null ? null: new Date(assortmentAPI.getValidTo()));
            // add managers
            setAssortmentManagers(organizationUniqueId, assortmentAPI, assortment, userAPI);
        }
                
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.ASSORTMENT, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, assortment.getUniqueId(), requestIp));
        return AssortmentMapper.map(assortment);
    }
    
    /**
     * Get the <code>AssortmentAPI</code> for the given unique id on the 
     * specified organization
     * 
     * @param organizationUniqueId unique id of the organization
     * @param assortmentUniqueId unique id of the assortment to get
     * @param userAPI user information, needed for business levels validation
     * @param sessionId
     * @return the corresponding <code>AgreementAPI</code>
     */
    public AssortmentAPI getAssortmentAPI(long organizationUniqueId, long assortmentUniqueId, UserAPI userAPI, String sessionId, String requestIp) {
        LOG.log(Level.FINEST, "getAssortmentAPI( organizationUniqueId: {0}, assortmentUniqueId: {1} )", new Object[] {organizationUniqueId, assortmentUniqueId});
        Assortment assortment = getAssortment(organizationUniqueId, assortmentUniqueId, userAPI);
        if( assortment != null ) {
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.ASSORTMENT, InternalAudit.ActionType.VIEW, userAPI.getId(), sessionId, assortmentUniqueId, requestIp));
            return AssortmentMapper.map( assortment );
        }
        return null;
    }
    
    /**
     * Get the <code>Assortment</code> with the given id on the specified organization. 
     * The agreement can be read by the defined customer 
     * 
     * @param organizationUniqueId unique id of the organization
     * @param assortmentUniqueId unique id of the Assortment to get
     * @param userAPI user information, needed for business levels validation
     * @return the corresponding <code>Assortment</code>
     */
    public Assortment getAssortment(long organizationUniqueId, long assortmentUniqueId, UserAPI userAPI) {
        LOG.log(Level.FINEST, "getAssortment( organizationUniqueId: {0}, assortmentUniqueId: {1} )", new Object[] {organizationUniqueId, assortmentUniqueId});
        Assortment assortment = em.find(Assortment.class, assortmentUniqueId);
        if( assortment == null ) {
            return null;
        }
        if( !assortment.getCustomer().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user to access assortment: {0} not on given organization: {1} or in user business levels. Returning null.", new Object[] {assortmentUniqueId, organizationUniqueId});
            return null;
        }
        return assortment;
    }
        
    public SearchDTO searchArticlesOnAssortment(String query, 
            int offset, 
            int limit, 
            long organizationUniqueId, 
            long assortmentUniqueId, 
            UserAPI userAPI, 
            String sessionId, 
            String requestIp) {
        LOG.log(Level.FINEST, "searchArticlesOnAssortment( organizationUniqueId: {0}, assortmentUniqueId: {1} )", new Object[] {organizationUniqueId, assortmentUniqueId});
        Assortment assortment = getAssortment(organizationUniqueId, assortmentUniqueId, userAPI);
        if( assortment == null ) {
            // assortment either does not exist or the user is not allowed to view it
            return null;
        }
        
        // count
        StringBuilder countSqlBuilder = new StringBuilder();
        countSqlBuilder.append("SELECT COUNT(ar.uniqueId) FROM Assortment a JOIN a.articles ar ");
        countSqlBuilder.append("WHERE a.uniqueId = :assortmentUniqueId ");
        if( query != null && !query.isEmpty() ) {
            countSqlBuilder.append("AND (ar.articleName LIKE :query OR ar.articleNumber LIKE :query) " );
        }
        String countQuerySql = countSqlBuilder.toString();
        LOG.log(Level.FINEST, "countQuerySql: {0}", new Object[] {countQuerySql});
        Query countQuery = em.createQuery(countQuerySql).
                setParameter("assortmentUniqueId", assortmentUniqueId);
        if( query != null && !query.isEmpty() ) {
            countQuery.setParameter("query", "%" + query + "%");
        }
        Long count = (Long) countQuery.getSingleResult();
              
        // search
        StringBuilder searchSqlBuilder = new StringBuilder();
        searchSqlBuilder.append("SELECT ar FROM Assortment a JOIN a.articles ar ");
        searchSqlBuilder.append("WHERE a.uniqueId = :assortmentUniqueId ");
        if( query != null && !query.isEmpty() ) {
            searchSqlBuilder.append("AND (ar.articleName LIKE :query OR ar.articleNumber LIKE :query) " );
        }
        searchSqlBuilder.append("ORDER BY ar.articleName ASC");
        String searchQuerySql = searchSqlBuilder.toString();
        LOG.log(Level.FINEST, "searchQuerySql: {0}", new Object[] {searchQuerySql});
        Query searchQuery = em.createQuery(searchQuerySql).
                setParameter("assortmentUniqueId", assortmentUniqueId);
        if( query != null && !query.isEmpty() ) {
            searchQuery.setParameter("query", "%" + query + "%");
        }
        List<ArticleAPI> articleAPIs = ArticleMapper.map(searchQuery.
                setMaxResults(limit).
                setFirstResult(offset).
                getResultList(), false);
        return new SearchDTO(count, articleAPIs);
    }
    
    /**
     * Add articles to given assortment. A user that has all permissions 
     * 
     * assortmentarticle:create_all
     * 
     * can add articles to any assortment in the given organization. A user that only
     * has 
     * 
     * assortmentarticle:create_own
     * 
     * can only add articles to assortments on own organization where the user is
     * a designated "manager".
     * 
     * @param organizationUniqueId
     * @param assortmentUniqueId
     * @param articleAPIs
     * @param userAPI
     * @param sessionId
     * @param requestIp
     * @param hasAllPermissions true if user can add articles to any assortment in organization
     * @return
     * @throws HjalpmedelstjanstenValidationException
     */
    public AssortmentAPI addArticlesToAssortment(long organizationUniqueId, long assortmentUniqueId, List<ArticleAPI> articleAPIs, UserAPI userAPI, String sessionId, String requestIp, boolean hasAllPermissions) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "addArticlesToAssortment( organizationUniqueId: {0}, assortmentUniqueId: {1}, hasAllPermissions: {2} )", new Object[] {organizationUniqueId, assortmentUniqueId, hasAllPermissions});
        Assortment assortment = getAssortment(organizationUniqueId, assortmentUniqueId, userAPI);
        if( assortment == null ) {
            // assortment either does not exist or the user is not allowed to view it
            return null;
        }
        if( !hasAllPermissions ) {
            // user can only add articles to assortment where manager
            if( !userIsAssortmentManager(assortment, userAPI) ) {
                LOG.log( Level.WARNING, "User {0} does not have the right to add articles to assortment: {1}", new Object[]{userAPI.getId(), assortment.getUniqueId()});
                return null;
            }
        }
        if( assortment.getArticles() == null ) {
            assortment.setArticles(new ArrayList<>());
        }
        List<Long> currentArticleUniqueIdsInAssortment = getAssortmentArticleUniqueIds(assortment.getUniqueId());
        if( articleAPIs != null && !articleAPIs.isEmpty() ) {
            for( ArticleAPI articleAPI : articleAPIs ) {
                // article
                Article article = articleController.getArticle(articleAPI.getId(), userAPI);
                if( article == null ) {
                    LOG.log( Level.WARNING, "Attempt by user: {0} to add article: {1} which doesn't exist to assortment: {2}", new Object[] {userAPI.getId(), articleAPI.getId(), assortment.getUniqueId()});
                    throw validationMessageService.generateValidationException("article", "assortment.article.notExist");                
                }
                if( currentArticleUniqueIdsInAssortment.contains(article.getUniqueId())) {
                    LOG.log( Level.FINEST, "Article {0} already exists in assortment: {1}. Skip.", new Object[] {article.getUniqueId(), assortment.getUniqueId()});
                    continue;
                }
                assortment.getArticles().add(article);
            }
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.ASSORTMENT, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, assortment.getUniqueId(), requestIp));
        } 
        return AssortmentMapper.map(assortment);
    }
    
    /**
     * Delete articles from given assortment. A user that has all permissions 
     * 
     * assortmentarticle:delete_all
     * 
     * can delete articles from any assortment in the given organization. A user that only
     * has 
     * 
     * assortmentarticle:delete_own
     * 
     * can only delete articles from assortments on own organization where the user is
     * a designated "manager".
     * 
     * @param organizationUniqueId
     * @param assortmentUniqueId
     * @param articleUniqueId unique id of the article to remove from the assortment
     * @param userAPI
     * @param sessionId
     * @param requestIp
     * @param hasAllPermissions true if user can delete articles from any assortment in organization
     * @return
     * @throws HjalpmedelstjanstenValidationException
     */
    public AssortmentAPI deleteArticleFromAssortment(long organizationUniqueId, long assortmentUniqueId, long articleUniqueId, UserAPI userAPI, String sessionId, String requestIp, boolean hasAllPermissions) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "deleteArticlesFromAssortment( organizationUniqueId: {0}, assortmentUniqueId: {1}, hasAllPermissions: {2} )", new Object[] {organizationUniqueId, assortmentUniqueId, hasAllPermissions});
        Assortment assortment = getAssortment(organizationUniqueId, assortmentUniqueId, userAPI);
        if( assortment == null ) {
            // assortment either does not exist or the user is not allowed to view it
            return null;
        }
        if( !hasAllPermissions ) {
            // user can only add articles to assortment where manager
            if( !userIsAssortmentManager(assortment, userAPI) ) {
                LOG.log( Level.WARNING, "User {0} does not have the right to delete articles from assortment: {1}", new Object[]{userAPI.getId(), assortment.getUniqueId()});
                return null;
            }
        }
        if( assortment.getArticles() == null || assortment.getArticles().isEmpty() ) {
            LOG.log( Level.INFO, "Attempt to remove articles from assortment: {0} that has no articles.", new Object[]{assortment.getUniqueId()});            
        } else {
            // article
            Article article = articleController.getArticle(articleUniqueId, userAPI);
            if( article == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to delete article: {1} which doesn't exist from assortment: {2}", new Object[] {userAPI.getId(), articleUniqueId, assortment.getUniqueId()});
                throw validationMessageService.generateValidationException("article", "assortment.article.delete.notExist");                
            }
            assortment.getArticles().remove(article);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.ASSORTMENT, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, assortment.getUniqueId(), requestIp));
        }
        return AssortmentMapper.map(assortment);
    }
    
    /**
     * When an article is changed in its "essence" (like switches based on product)
     * all references to that article must be removed.
     * 
     * @param article the article to find assortments for
     */
    public void deleteRowsByArticle( Article article ) {
        LOG.log(Level.FINEST, "deleteRowsByArticle( article->uniqueId: {0} )", new Object[] {article.getUniqueId()});
        List<Assortment> assortments = 
                em.createNamedQuery(Assortment.FIND_BY_ARTICLE).
                setParameter("articleUniqueId", article.getUniqueId()).
                getResultList();
        if( assortments != null && !assortments.isEmpty() ) {
            for( Assortment assortment : assortments ) {
                assortment.getArticles().remove(article);
            }
        }
    }
    
    /**
     * Set customer organization on assortment
     * 
     * @param assortment the assortment to set organization on
     * @param organizationUniqueId unique id of the customer organization
     * @throws HjalpmedelstjanstenValidationException if the organizatino doesn't exist
     * or is not of type CUSTOMER
     */
    void setCustomer(Assortment assortment, long organizationUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "setCustomer( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        Organization customerOrganization = organizationController.getOrganization(organizationUniqueId);
        if( customerOrganization == null ) {
            throw validationMessageService.generateValidationException("customer", "assortment.customerOrganization.notExists");
        }
        if( customerOrganization.getOrganizationType() != Organization.OrganizationType.CUSTOMER ) {
            throw validationMessageService.generateValidationException("customer", "assortment.customerOrganization.invalidType");
        }
        assortment.setCustomer(customerOrganization);
    }
    
    /**
     * Set the manager for this assortment. Only the customer can do this. 
     * Each person added must have the role CustomerAssignedAssortmentManager. 
     * 
     * @param organizationUniqueId unique id of the organization
     * @param assortmentAPI user supplied values
     * @param assortment assortment to add managers to
     * @param userAPI the currently logged in person
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g. 
     * user does not have valid role or doesn't exist
     */
    void setAssortmentManagers(long organizationUniqueId, AssortmentAPI assortmentAPI, Assortment assortment, UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "setAssortmentManagers( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        if( assortmentAPI.getManagers() != null && !assortmentAPI.getManagers().isEmpty() ) {
            List<UserEngagement> assortmentManagers = new ArrayList<>();
            for( UserAPI assortmentManagerAPI : assortmentAPI.getManagers() ) {
                UserEngagement assortmentManagerUserEngagement = userController.getUserEngagement(organizationUniqueId, assortmentManagerAPI.getId());
                if( assortmentManagerUserEngagement == null ) {
                    LOG.log( Level.WARNING, "Attempt by user: {0} to add assortment manager: {1} which does not exist or does not belong to organization: {2}", new Object[] {userAPI.getId(), assortmentManagerAPI.getId(), organizationUniqueId});
                    throw validationMessageService.generateValidationException("customerPricelistApprovers", "agreement.customerPricelistApprovers.notExist");
                }
                checkAssortmentManagerRoles(assortmentManagerUserEngagement, userAPI);
                assortmentManagers.add(assortmentManagerUserEngagement);
            }
            assortment.setManagers(assortmentManagers);
        } else {
            assortment.setManagers(null);
        }
    }
    
    private void checkAssortmentManagerRoles(UserEngagement assortmentManagerUserEngagement, UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        boolean roleFound = false;
        if( assortmentManagerUserEngagement.getRoles() != null && !assortmentManagerUserEngagement.getRoles().isEmpty() ) {
            for( UserRole userRole : assortmentManagerUserEngagement.getRoles() ) {
                if( userRole.getName() == UserRole.RoleName.CustomerAssignedAssortmentManager ) {
                    roleFound = true;
                    break;
                }
            }
        }
        if( !roleFound ) {
            LOG.log(Level.WARNING, "attempt by user: {0} to set assortment manager which does not have valid role, this is an odd situation and should be checked", new Object[] {userAPI.getId()});
            throw validationMessageService.generateValidationException("managers", "assortment.managers.wrongRole", assortmentManagerUserEngagement.getUserAccount().getFullName());
        }
    }
    
    /**
     * Set county and municipalities on assortment
     * 
     * @param assortment the assortment to set organization on
     * @throws HjalpmedelstjanstenValidationException if the organizatino doesn't exist
     * or is not of type CUSTOMER
     */
    void setCountyAndMunicipalities(AssortmentAPI assortmentAPI, Assortment assortment) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "setCountyAndMunicipalities( assortment->uniqueId: {0} )", new Object[] {assortment.getUniqueId()});

        // county 
        CVCounty county = countyController.findCountyById(assortmentAPI.getCounty().getId());
        if( county == null ) {
            throw validationMessageService.generateValidationException("county", "assortment.county.notExists");
        }
        assortment.setCounty(county);

        // municipalities
        assortment.setMunicipalities(new ArrayList<>());
        if( county.getShowMunicipalities() ) {
            if( assortmentAPI.getMunicipalities() == null || assortmentAPI.getMunicipalities().isEmpty() ) {
                throw validationMessageService.generateValidationException("municipalities", "assortment.municipalities.notNull");
            }
            for( CVMunicipalityAPI municipalityAPI : assortmentAPI.getMunicipalities() ) {
                CVMunicipality municipality = countyController.findMunicipalityById(municipalityAPI.getId());
                // municipality must be in the municipalities of the county we already fetched
                if( !county.getMunicipalities().contains(municipality) ) {
                    throw validationMessageService.generateValidationException("municipalities", "assortment.municipalities.notInCounty");
                }
                assortment.getMunicipalities().add(municipality);
            } 
        }
    }
    
    /**
     * Search agreements for the user supplied query. Search is paginated so only
     * results valid for given offset and limit is returned.
     * 
     * @param organizationUniqueId
     * @param queryString user supplied query
     * @param statuses
     * @param onlyWhereManager
     * @param userAPI user information, needed for business levels validation
     * @param offset start returning from this search result
     * @param limit maximum number of search results to return
     * @return a SearchDTO object of number of hits and list of <code>AssortmentAPI</code> matching the query parameters
     */
    public SearchDTO searchAssortments(Long organizationUniqueId, 
            String queryString, 
            List<Assortment.Status> statuses,
            boolean onlyWhereManager,
            UserAPI userAPI, 
            int offset, 
            int limit) {
        LOG.log(Level.FINEST, "searchAssortments( offset: {0}, limit: {1}, organizationUniqueId: {2} )", new Object[] {offset, limit, organizationUniqueId});
        
        // create SQL
        boolean whereSet = false;
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT a FROM Assortment a ");
        if( onlyWhereManager ) {
            queryBuilder.append("JOIN a.managers am ");
        }
        if( organizationUniqueId != null ) {
            queryBuilder.append( "WHERE a.customer.uniqueId = :customerOrganizationId " );
            whereSet = true;
        }
        if( queryString != null && !queryString.isEmpty() ) {
            queryBuilder.append(whereSet ? "AND ": "WHERE " );
            queryBuilder.append("a.name LIKE :query ");
            whereSet = true;
        }
        if( onlyWhereManager ) {
            queryBuilder.append(whereSet ? "AND ": "WHERE " );
            queryBuilder.append("(am.uniqueId = :userEngagementId) ");  
            whereSet = true;
        }
        if( statuses != null && !statuses.isEmpty() ) {
            queryBuilder.append(whereSet ? "AND (": "WHERE (" );
            for( int i = 0; i<statuses.size(); i++ ) {
                Assortment.Status status = statuses.get(i);
                if( status == Assortment.Status.CURRENT ) {
                    queryBuilder.append("(a.validFrom < :now AND (a.validTo IS NULL OR a.validTo > :now)) ");
                } else if( status == Assortment.Status.FUTURE ) {
                    queryBuilder.append("(a.validFrom > :now) ");
                } else {
                    // DISCONTINUED
                    queryBuilder.append("(a.validTo < :now) ");
                }
                if( i < (statuses.size()-1) ) {
                    queryBuilder.append("OR ");
                }
            }
            queryBuilder.append(") ");
            whereSet = true;
        }
        queryBuilder.append("ORDER BY a.name");
        String querySql = queryBuilder.toString();
        LOG.log( Level.FINEST, "querySql: {0}", new Object[] {querySql});

        // create query and populate parameters
        Query query = em.createQuery(querySql);
        if( queryString != null && !queryString.isEmpty() ) {
            query.setParameter("query", "%" + queryString + "%");
        }
        if( organizationUniqueId != null ) {
            query.setParameter("customerOrganizationId", organizationUniqueId);
        }
        if( onlyWhereManager ) {
            query.setParameter("userEngagementId", userAPI.getId());
        }
        if( statuses != null && !statuses.isEmpty() ) {
            query.setParameter("now", new Date());
        }

        // execute query
        List<Assortment> assortments = query.setMaxResults(limit).
                    setFirstResult(offset).
                    getResultList();
        
        long count = countSearchAssortments(organizationUniqueId, 
                queryString, 
                statuses, 
                onlyWhereManager, 
                userAPI);
        return new SearchDTO(count, AssortmentMapper.map(assortments));
    }
    
    /**
     * Counts the total number of agreements that matches a user supplied search query
     * 
     * @param queryString user supplied query
     * @return total number of agreements matching the query parameters
     */
    private long countSearchAssortments(Long organizationUniqueId,
            String queryString, 
            List<Assortment.Status> statuses,
            boolean onlyWhereManager,
            UserAPI userAPI) {
        LOG.log(Level.FINEST, "countSearchAssortments( ... )");
        
        // create SQL
        boolean whereSet = false;
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT COUNT(a.uniqueId) FROM Assortment a ");
        if( onlyWhereManager ) {
            queryBuilder.append("JOIN a.managers am ");
        }
        if( organizationUniqueId != null ) {
            queryBuilder.append( "WHERE a.customer.uniqueId = :customerOrganizationId " );
            whereSet = true;
        }
        if( queryString != null && !queryString.isEmpty() ) {
            queryBuilder.append(whereSet ? "AND ": "WHERE " );
            queryBuilder.append("a.name LIKE :query ");
            whereSet = true;
        }
        if( onlyWhereManager ) {
            queryBuilder.append(whereSet ? "AND ": "WHERE " );
            queryBuilder.append("(am.uniqueId = :userEngagementId) ");  
            whereSet = true;
        }
        if( statuses != null && !statuses.isEmpty() ) {
            queryBuilder.append(whereSet ? "AND (": "WHERE (" );
            for( int i = 0; i<statuses.size(); i++ ) {
                Assortment.Status status = statuses.get(i);
                if( status == Assortment.Status.CURRENT ) {
                    queryBuilder.append("(a.validFrom < :now AND (a.validTo IS NULL OR a.validTo > :now)) ");
                } else if( status == Assortment.Status.FUTURE ) {
                    queryBuilder.append("(a.validFrom > :now) ");
                } else {
                    // DISCONTINUED
                    queryBuilder.append("(a.validTo < :now) ");
                }
                if( i < (statuses.size()-1) ) {
                    queryBuilder.append("OR ");
                }
            }
            queryBuilder.append(") ");
            whereSet = true;
        }
        String querySql = queryBuilder.toString();
        LOG.log( Level.FINEST, "querySql: {0}", new Object[] {querySql});

        // create query and populate parameters
        Query query = em.createQuery(querySql);
        if( queryString != null && !queryString.isEmpty() ) {
            query.setParameter("query", "%" + queryString + "%");
        }
        if( organizationUniqueId != null ) {
            query.setParameter("customerOrganizationId", organizationUniqueId);
        }
        if( onlyWhereManager ) {
            query.setParameter("userEngagementId", userAPI.getId());
        }
        if( statuses != null && !statuses.isEmpty() ) {
            query.setParameter("now", new Date());
        }
        return (Long) query.getSingleResult();
    }

    private List<Long> getAssortmentArticleUniqueIds(Long assortmentUniqueId) {
        LOG.log(Level.FINEST, "getAssortmentArticleUniqueIds( assortmentUniqueId: {0})", new Object[] {assortmentUniqueId});
        List<Long> ids = em.createNamedQuery(Assortment.GET_ARTICLE_UNIQUE_IDS).
                setParameter("assortmentUniqueId", assortmentUniqueId).
                getResultList();
        return ids;
    }

    private boolean userIsAssortmentManager(Assortment assortment, UserAPI userAPI) {
        LOG.log(Level.FINEST, "userIsAssortmentManager( assortment->uniqueId: {0})", new Object[] {assortment.getUniqueId()});
        boolean isManager = false;
        if( assortment.getManagers() != null && !assortment.getManagers().isEmpty() ) {
            for( UserEngagement userEngagement : assortment.getManagers() ) {
                if( userEngagement.getUniqueId().equals(userAPI.getId()) ) {
                    isManager = true;
                    break;
                }
            }
        }
        return isManager;
    }
    
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.product.controller;

import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementPricelistRowMapper;
import se.inera.hjalpmedelstjansten.business.indexing.controller.ElasticSearchController;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import se.inera.hjalpmedelstjansten.business.DateUtils;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementPricelistRowController;
import se.inera.hjalpmedelstjansten.business.generalpricelist.controller.GeneralPricelistPricelistRowController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.*;
import se.inera.hjalpmedelstjansten.model.entity.*;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEDirective;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCEStandard;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVOrderUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPackageUnit;

/**
 * Class for handling business logic of Articles. This includes talking to the
 * database.
 *
 * @author Tommy Berglund
 */
@Stateless
public class ProductController {

    @Inject
    HjmtLogger LOG;

    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;

    @Inject
    ProductValidation productValidation;

    @Inject
    OrganizationController organizationController;

    @Inject
    CategoryController categoryController;

    @Inject
    CeController ceController;

    @Inject
    OrderUnitController orderUnitController;

    @Inject
    PackageUnitController packageUnitController;
    
    @Inject
    UserController userController;

    @Inject
    ArticleController articleController;

    @Inject
    CVPreventiveMaintenanceController cVPreventiveMaintenanceController;

    @Inject
    ValidationMessageService validationMessageService;

    @Inject
    ElasticSearchController elasticSearchController;

    @Inject
    AgreementPricelistRowController agreementPricelistRowController;
    
    @Inject
    GeneralPricelistPricelistRowController generalPricelistPricelistRowController;
    
    @Inject
    Event<InternalAuditEvent> internalAuditEvent;

    /**
     * Create a product on the <code>Organization</code> with the given id.
     *
     * @param organizationUniqueId unique id of the Organization
     * @param productAPI user supplied values
     * @param userAPI logged in users sessions information
     * @param sessionId
     * @param requestIp
     * @param doIndex
     * @return the mapped <code>ProductAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    public ProductAPI createProduct(long organizationUniqueId,
            ProductAPI productAPI,
            UserAPI userAPI,
            String sessionId,
            String requestIp,
            boolean doIndex) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createProduct( organizationUniqueId: {0} )", new Object[] {organizationUniqueId} );
        ProductMapper.fix(productAPI);
        productValidation.validateForCreate(productAPI, organizationUniqueId);
        List<Category> categories = categoryController.getChildlessById(productAPI.getCategory().getId());
        if( categories == null || categories.isEmpty() ) {
            LOG.log(Level.FINEST, "category {0} does not exist or is not a leaf node", new Object[] {productAPI.getCategory().getId()});
            throw validationMessageService.generateValidationException("category", "product.category.notExists");
        }
        Category category = categories.get(0);
        List<CategorySpecificProperty> categorySpecificPropertys = categoryController.getCategoryPropertys(category.getUniqueId());
        Map<Long, CVOrderUnit> allOrderUnits = orderUnitController.findAllAsIdMap();
        Map<Long, CVPackageUnit> allPackageUnits = packageUnitController.findAllAsIdMap();
        Product product = doCreateProduct(organizationUniqueId, productAPI, category, allOrderUnits, allPackageUnits, categorySpecificPropertys, userAPI, sessionId, requestIp);
        productAPI = ProductMapper.map(product, true, categorySpecificPropertys);
        productAPI.setNumberEditable(isProductNumberEditable(product));
        if( doIndex ) {
            elasticSearchController.indexProduct(productAPI);
        }
        return productAPI;
    }
    
    public Product createProduct(long organizationUniqueId,
            ProductAPI productAPI,
            Category category,
            Map<Long, CVOrderUnit> allOrderUnits, 
            Map<Long, CVPackageUnit> allPackageUnits,
            List<CategorySpecificProperty> categorySpecificPropertys,
            UserAPI userAPI,
            String sessionId,
            String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createProduct( organizationUniqueId: {0}, category: {1} )", new Object[] {organizationUniqueId, category.getUniqueId()} );
        ProductMapper.fix(productAPI);
        productValidation.validateForCreate(productAPI, organizationUniqueId);
        return doCreateProduct(organizationUniqueId, productAPI, category, allOrderUnits, allPackageUnits, categorySpecificPropertys, userAPI, sessionId, requestIp);
    }
    
    private Product doCreateProduct(long organizationUniqueId,
            ProductAPI productAPI,
            Category category,
            Map<Long, CVOrderUnit> allOrderUnits,
            Map<Long, CVPackageUnit> allPackageUnits,
            List<CategorySpecificProperty> categorySpecificPropertys,
            UserAPI userAPI,
            String sessionId,
            String requestIp) throws HjalpmedelstjanstenValidationException {
        Product product = ProductMapper.map(productAPI);

        // add organization
        Organization organization = organizationController.getOrganization(organizationUniqueId);
        if (organization == null) {
            throw validationMessageService.generateValidationException("organizationId", "product.organization.notExists");
        }
        product.setOrganization(organization);

        // add primary category

        // only possible to create products with article type H and T with iso codes
        if (category.getArticleType() != Article.Type.T && category.getArticleType() != Article.Type.H) {
            LOG.log(Level.FINEST, "category {0} is not of article type T or H, but instead: {1} which is not allowed for products", new Object[]{category.getUniqueId(), category.getArticleType()});
            throw validationMessageService.generateValidationException("category", "product.category.wrongArticleType");
        } else {
            if (category.getCode() == null) {
                LOG.log(Level.FINEST, "category {0} is of right article type T or H, but has not iso code which is not allowed for products", new Object[]{category.getUniqueId()});
                throw validationMessageService.generateValidationException("category", "product.category.wrongArticleType");
            }
        }
        product.setCategory(category);

        // add extended categories
        setExtendedCategories(productAPI, product);

        // add directives and standards (if any)
        setCe(productAPI, product);

        // preventive maintenance valid from
        setPreventiveMaintenanceValidFrom(productAPI, product, userAPI);

        // order information
        setOrderInformation(productAPI, product, allOrderUnits, allPackageUnits);

        if (productAPI.getReplacementDate() != null){
            Date replacementDate = new Date(productAPI.getReplacementDate());
            product.setReplacementDate(replacementDate);
        }

        setResourceSpecificProperties(productAPI, product, categorySpecificPropertys, null);

        product.setMediaFolderName(UUID.randomUUID().toString());

        // save it
        em.persist(product);
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.PRODUCT, InternalAudit.ActionType.CREATE, userAPI.getId(), sessionId, product.getUniqueId(), requestIp));
        return product;
    }

    /**
     * Search for products
     *
     * @param organizationUniqueId unique id of the organization
     * @param statuses user supplied list of product statuses
     * @param articleTypes
     * @return a list of <code>ProductAPI</code>
     */
    public List<ProductAPI> getProductsByOrganizations(long organizationUniqueId, List<Product.Status> statuses, List<Article.Type> articleTypes) {
        LOG.log(Level.FINEST, "getProductsByOrganizations( organizationUniqueId: {0}, statuses: {1}, articleTypes: {2} )", new Object[] {organizationUniqueId, statuses, articleTypes});
        return ProductMapper.map(em.createNamedQuery(Product.GET_BY_ORGANIZATION).
                setParameter("organizationUniqueId", organizationUniqueId).
                setParameter("statuses", statuses).
                setParameter("articleTypes", articleTypes).
                getResultList());
    }

    /**
     * Search for articles based on the given product
     *
     * @param organizationUniqueId unique id of the organization
     * @param productUniqueId unique id of the product
     * @param offset start search from this point
     * @param limit limit search to this number of results
     * @return a list of <code>ArticleAPI</code>
     */
    public List<ArticleAPI> searchArticlesBasedOnProduct(long organizationUniqueId, long productUniqueId, int offset, int limit) {
        LOG.log(Level.FINEST, "searchArticlesBasedOnProduct( organizationUniqueId: {0}, productUniqueId: {1}, offset: {2}, limit: {3} )", new Object[] {organizationUniqueId, productUniqueId, offset, limit});
        return ArticleMapper.map(em.createNamedQuery(Article.SEARCH_BASED_ON_PRODUCT).
                setParameter("organizationUniqueId", organizationUniqueId).
                setParameter("productUniqueId", productUniqueId).
                setMaxResults(limit).
                setFirstResult(offset).
                getResultList());
    }

    /**
     * Get all articles based on a specific product
     *
     * @param organizationUniqueId unique id of the organization
     * @param productUniqueId unique id of the product
     * @return a list of <code>Article</code>
     */
    public List<Article> getArticlesBasedOnProduct(long organizationUniqueId, long productUniqueId) {
        LOG.log(Level.FINEST, "getArticlesBasedOnProduct( organizationUniqueId: {0}, productUniqueId: {1} )", new Object[] {organizationUniqueId, productUniqueId});
        return em.createNamedQuery(Article.SEARCH_BASED_ON_PRODUCT).
                setParameter("organizationUniqueId", organizationUniqueId).
                setParameter("productUniqueId", productUniqueId).
                getResultList();
    }

    /**
     * Get all articles based on a specific product as APIs
     *
     * @param organizationUniqueId unique id of the organization
     * @param productUniqueId unique id of the product
     * @return a list of <code>ArticleAPI</code>
     */
    public List<ArticleAPI> getArticleAPIsBasedOnProduct(long organizationUniqueId, long productUniqueId, UserAPI userAPI) {
        LOG.log(Level.FINEST, "getArticlesBasedOnProduct( organizationUniqueId: {0}, productUniqueId: {1}, categoryUniqueId: {2} )", new Object[] {organizationUniqueId, productUniqueId});
        Product product = getProduct(organizationUniqueId, productUniqueId, userAPI);
        List<CategorySpecificProperty> categorySpecificPropertys = categoryController.getCategoryPropertys(product.getCategory().getUniqueId());
        return ArticleMapper.map(getArticlesBasedOnProduct(organizationUniqueId, productUniqueId), true, categorySpecificPropertys, userAPI);
    }

    /**
     * Counts the total number of articles that are based on the given product
     *
     * @param organizationUniqueId unique id of the organization
     * @param uniqueId unique id of the product
     * @return total number of articles matching the query parameters
     */
    public long countSearchBasedOnProduct(long organizationUniqueId, long uniqueId) {
        LOG.log(Level.FINEST, "countSearchBasedOnProduct( organizationUniqueId: {0}, uniqueId: {1} )", new Object[] {organizationUniqueId, uniqueId});
        return  (Long) em.createNamedQuery(Article.COUNT_SEARCH_BASED_ON_PRODUCT).
                setParameter("organizationUniqueId", organizationUniqueId).
                setParameter("uniqueId", uniqueId).
                getSingleResult();
    }

    /**
     * Search for articles that fits to the given product
     *
     * @param organizationUniqueId unique id of the organization
     * @param uniqueId unique id of the product
     * @param offset start search from this point
     * @param limit limit search to this number of results
     * @return a list of <code>ArticleAPI</code>
     */
    public List<ArticleAPI> searchArticlesFitsToProduct(long organizationUniqueId, long uniqueId, int offset, int limit) {
        LOG.log(Level.FINEST, "searchArticlesFitsToProduct( organizationUniqueId: {0}, uniqueId: {1}, offset: {2}, limit: {3} )", new Object[] {organizationUniqueId, uniqueId, offset, limit});
        return ArticleMapper.map(em.createNamedQuery(Article.SEARCH_FITS_TO_PRODUCT).
                setParameter("organizationUniqueId", organizationUniqueId).
                setParameter("uniqueId", uniqueId).
                setMaxResults(limit).
                setFirstResult(offset).
                getResultList());
    }

    /**
     * Counts the total number of articles that fits to the given product
     *
     * @param organizationUniqueId unique id of the organization
     * @param uniqueId unique id of the product
     * @return total number of articles matching the query parameters
     */
    public long countSearchFitsToProduct(long organizationUniqueId, long uniqueId) {
        LOG.log(Level.FINEST, "countSearchFitsToProduct( organizationUniqueId: {0}, uniqueId: {1} )", new Object[] {organizationUniqueId, uniqueId});
        return  (Long) em.createNamedQuery(Article.COUNT_SEARCH_FITS_TO_PRODUCT).
                setParameter("organizationUniqueId", organizationUniqueId).
                setParameter("uniqueId", uniqueId).
                getSingleResult();
    }

    /**
     * Get the <code>ProductAPI</code> for the given unique id on the
     * specified organization
     *
     * @param organizationUniqueId unique id of the organization
     * @param uniqueId unique id of the product to get
     * @param userAPI
     * @param sessionId
     * @param requestIp
     * @return the corresponding <code>ProductAPI</code>
     */
    public ProductAPI getProductAPI(long organizationUniqueId, long uniqueId, UserAPI userAPI, String sessionId, String requestIp) {
        LOG.log(Level.FINEST, "getProductAPI( organizationUniqueId: {0}, uniqueId: {1} )", new Object[] {organizationUniqueId, uniqueId});
        Product product = getProduct(organizationUniqueId, uniqueId, userAPI);        
        if( product != null ) {            
            List<CategorySpecificProperty> categorySpecificPropertys = categoryController.getCategoryPropertys(product.getCategory().getUniqueId());
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.PRODUCT, InternalAudit.ActionType.VIEW, userAPI.getId(), sessionId, uniqueId, requestIp));
            ProductAPI productAPI = ProductMapper.map( product, true, categorySpecificPropertys, userAPI );
            productAPI.setNumberEditable(isProductNumberEditable(product));
            return productAPI;
        }
        return null;
    }

    /**
     * Get the <code>ProductAPI</code> for the product with the given product number
     * on the specified organization
     *
     * @param organizationUniqueId unique id of the organization
     * @param productNumber the product number
     * @return the corresponding <code>ProductAPI</code>
     */
    public ProductAPI getProductAPIByProductNumber(long organizationUniqueId, String productNumber) {
        LOG.log(Level.FINEST, "getProductAPIByProductNumber( organizationUniqueId: {0}, productNumber: {1} )", new Object[] {organizationUniqueId, productNumber});
        Product product = findByProductNumberAndOrganization(productNumber, organizationUniqueId);
        return ProductMapper.map( product, false, null );
    }

    /**
     * Get the <code>Product</code> with the given id on the specified organization
     *
     * @param organizationUniqueId unique id of the organization
     * @param uniqueId unique id of the product to get
     * @param userAPI
     * @return the corresponding <code>Product</code>
     */
    public Product getProduct(long organizationUniqueId, long uniqueId, UserAPI userAPI) {
        LOG.log(Level.FINEST, "getProduct( organizationUniqueId: {0}, uniqueId: {1} )", new Object[] {organizationUniqueId, uniqueId});
        Product product = em.find(Product.class, uniqueId);
        if( product == null ) {
            return null;
        }
        if( !product.getOrganization().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user to access product: {0} not on given organization: {1}. Returning null.", new Object[] {uniqueId, organizationUniqueId});
            return null;
        }
        if( product.isCustomerUnique() ) {
            // this means only the organization that created the product can read it
            // this is the only check we need to to for products, as opposed to articles
            // where an organization also may have the article on an agreement

            Organization organization = organizationController.getOrganizationFromLoggedInUser(userAPI);
            if( organization.getOrganizationType() != Organization.OrganizationType.SERVICE_OWNER ) {


                UserEngagementAPI userEngagementAPI = UserController.getUserEngagementAPIByOrganizationId(organizationUniqueId, userAPI.getUserEngagements());
                if (userEngagementAPI == null) {
                    LOG.log(Level.WARNING, "Attempt by user to access product: {0} not on given organization: {1}. Returning null.", new Object[]{uniqueId, organizationUniqueId});
                    return null;
                }
            }
        }
        return product;
    }
    
    /**
     * Update the <code>Product</code> with the given id with the supplied values.
     *
     * @param organizationUniqueId the unique id of the <code>Organization</code>
     * @param uniqueId the unique id of the <code>Product</code>
     * @param productAPI the user supplied values
     * @param userAPI logged in users sessions information
     * @param sessionId
     * @param requestIp
     * @param doIndex
     * @return the updated <code>OrganizationAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException in case of validation failure
     */
    public ProductAPI updateProduct(long organizationUniqueId,
            long uniqueId,
            ProductAPI productAPI,
            UserAPI userAPI,
            String sessionId,
            String requestIp,
            boolean doIndex) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateProduct( organizationUniqueId: {0}, uniqueId: {1} )", new Object[] {organizationUniqueId, uniqueId});
        Map<Long, CVOrderUnit> allOrderUnits = orderUnitController.findAllAsIdMap();
        Map<Long, CVPackageUnit> allPackageUnits = packageUnitController.findAllAsIdMap();
        Product product = getProduct(organizationUniqueId, uniqueId, userAPI);
        if( product == null ) {
            return null;
        }
        List<CategorySpecificProperty> categorySpecificPropertys;
        if( !productAPI.getCategory().getId().equals(product.getCategory().getUniqueId()) ) {
            categorySpecificPropertys = categoryController.getCategoryPropertys(productAPI.getCategory().getId());
        } else {
            categorySpecificPropertys = categoryController.getCategoryPropertys(product.getCategory().getUniqueId());            
        }
        product = doUpdateProduct(product, productAPI, allOrderUnits, allPackageUnits, categorySpecificPropertys, userAPI, sessionId, requestIp);
        productAPI = ProductMapper.map( product, true, categorySpecificPropertys, userAPI );
        productAPI.setNumberEditable(isProductNumberEditable(product));
        if( doIndex ) {
            elasticSearchController.updateIndexProduct(productAPI);
            List<ArticleAPI> articleAPIs = getArticleAPIsBasedOnProduct(organizationUniqueId, uniqueId, userAPI);
            if( articleAPIs != null ) {
                for( ArticleAPI articleAPI : articleAPIs ) {
                    elasticSearchController.updateIndexArticle(articleAPI);
                }
            }
        }
        return productAPI;
    }
    
    /**
     * Update the <code>Product</code> with the given id with the supplied values. 
     * Note, to change category on product this way, the list of categorySpecificPropertys
     * must be the list of the new category specific properties.
     *
     * @param organizationUniqueId the unique id of the <code>Organization</code>
     * @param uniqueId the unique id of the <code>Product</code>
     * @param productAPI the user supplied values
     * @param allOrderUnits
     * @param allPackageUnits
     * @param categorySpecificPropertys
     * @param userAPI logged in users sessions information
     * @param sessionId
     * @param requestIp
     * @return the updated <code>OrganizationAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException in case of validation failure
     */
    public Product updateProduct(long organizationUniqueId,
            long uniqueId,
            ProductAPI productAPI,
            Map<Long, CVOrderUnit> allOrderUnits, 
            Map<Long, CVPackageUnit> allPackageUnits,
            List<CategorySpecificProperty> categorySpecificPropertys, 
            UserAPI userAPI,
            String sessionId,
            String requestIp) throws HjalpmedelstjanstenValidationException {
        Product product = getProduct(organizationUniqueId, uniqueId, userAPI);
        if( product == null ) {
            return null;
        }
        return doUpdateProduct(product, productAPI, allOrderUnits, allPackageUnits, categorySpecificPropertys, userAPI, sessionId, requestIp);
    }

    public void delete(long organizationUniqueId,
                       long productUniqueId,
                       UserAPI userAPI) {
        Product product = getProduct(organizationUniqueId, productUniqueId, userAPI);

        ProductAPI productAPI = getProductAPIByProductNumber( organizationUniqueId, product.getProductNumber());
        em.remove(product);
        elasticSearchController.removeIndexProduct(productAPI);
    }


    /**
     * Update the <code>Product</code> with the given id with the supplied values.
     *
     * @param product
     * @param productAPI the user supplied values
     * @param allOrderUnits
     * @param categorySpecificPropertys
     * @param userAPI logged in users sessions information
     * @param sessionId
     * @param requestIp
     * @return the updated <code>OrganizationAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException in case of validation failure
     */
    private Product doUpdateProduct(Product product,
            ProductAPI productAPI,
            Map<Long, CVOrderUnit> allOrderUnits, 
            Map<Long, CVPackageUnit> allPackageUnits,
            List<CategorySpecificProperty> categorySpecificPropertys, 
            UserAPI userAPI,
            String sessionId,
            String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "doUpdateProduct( organizationUniqueId: {0}, uniqueId: {1} )", new Object[] {product.getOrganization().getUniqueId(), product.getUniqueId()});
        ProductMapper.fix(productAPI);
        productValidation.validateForUpdate(productAPI, product, product.getOrganization().getUniqueId(),userAPI);
        Product.Status oldProductStatus = product.getStatus();
        
        handleSetStatus(product, productAPI);
        handleReplaceProduct(product, productAPI, product.getOrganization().getUniqueId(), product.getUniqueId(), userAPI);
        
        // if the article was previously discontinued, the only possible action
        // is to republish it, or change replaced by articles
        if( oldProductStatus != Product.Status.DISCONTINUED ) {
            if( isProductNumberEditable(product) ) {
                product.setProductNumber(productAPI.getProductNumber());
            }
            product.setProductName(productAPI.getProductName());
            product.setSupplementedInformation(productAPI.getSupplementedInformation());
            product.setCustomerUnique(productAPI.isCustomerUnique());
            boolean categoryWasChanged = updateCategory(productAPI, product);

            updateManufacturer(productAPI, product);
            updatePreventiveMaintenance(productAPI, product, userAPI);
            setExtendedCategories(productAPI, product);
            product.setCeMarked(productAPI.isCeMarked());
            setCe(productAPI, product);
            setOrderInformation(productAPI, product, allOrderUnits, allPackageUnits);

            product.setColor(productAPI.getColor());

            setResourceSpecificProperties(productAPI, product, categorySpecificPropertys, categoryWasChanged);
            if( categoryWasChanged ) {
                List<Article> articlesBasedOnProduct = getArticlesBasedOnProduct(product.getOrganization().getUniqueId(), product.getUniqueId());
                if( articlesBasedOnProduct != null && !articlesBasedOnProduct.isEmpty() ) {
                    for( Article article : articlesBasedOnProduct ) {
                        if( article.getResourceSpecificPropertyValues() != null ) {
                            article.getResourceSpecificPropertyValues().clear();
                        }
                    }
                }
            }
        }
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.PRODUCT, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, product.getUniqueId(), requestIp));
        return product;
    }

    /**
     * Find products with a specific productNumber. Must check per organization
     * since uniqueness is only guaranteed within an organization.
     *
     * @param productNumber the product number
     * @param organizationUniqueId unique id of the organization
     * @return the product or null
     */
    public Product findByProductNumberAndOrganization( String productNumber, long organizationUniqueId ) {
        LOG.log(Level.FINEST, "findByProductNumberAndOrganization( productNumber: {0}, organizationUniqueId: {1} )", new Object[] {productNumber, organizationUniqueId});
        try {
            return (Product)  em.createNamedQuery(Product.FIND_BY_PRODUCT_NUMBER_AND_ORGANIZATION).
                setParameter("productNumber", productNumber).
                setParameter("organizationUniqueId", organizationUniqueId).
                getSingleResult();
        } catch( NoResultException ex ) {
            return null;
        }
    }

    /**
     * Set product status.
     *
     * @param product
     * @param productAPI
     */
    private void handleSetStatus(Product product, ProductAPI productAPI) {
        if( productAPI.getReplacementDate() == null && product.getStatus() != Product.Status.DISCONTINUED) {
            product.setStatus(Product.Status.valueOf(productAPI.getStatus()));
        }
    }

    /**
     * Handle product replacement functionality and checks. Only interesting if
     * user supplied data has replacement date set.
     *
     * @param product product to handle replacement of
     * @param productAPI user supplied values
     * @param organizationUniqueId unique id of the organization
     * @param uniqueId unique id of the product
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * if the product is discontineued
     */
    public void handleReplaceProduct(Product product, ProductAPI productAPI, long organizationUniqueId, long uniqueId, UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        if( product.getStatus() == Product.Status.DISCONTINUED ) {
            // if product already is discontinued, we can only republish or change replacement products
            if( productAPI.getReplacementDate() != null ) {
                Date replacementDate = new Date(productAPI.getReplacementDate());                
                if( !DateUtils.isSameDay(product.getReplacementDate().getTime(), replacementDate.getTime()) ) {
                    LOG.log(Level.FINEST, "attempt to update replacement date on discontinued product {0} which is not allowed", new Object[] {product.getUniqueId()});
                    throw validationMessageService.generateValidationException("replacementDate", "product.replacementDate.change.discontinued");
                }
                updateReplacedByProduct(organizationUniqueId, productAPI.getReplacedByProducts(), product, userAPI);
            } else {
                // this is a request to republish product
                product.setStatus(Product.Status.PUBLISHED);
                product.setReplacementDate(null);
                product.setReplacedByProducts(null);
                product.setInactivateRowsOnReplacement(null);
            }
        } else {
            if( productAPI.getReplacementDate() != null ) {
                Date replacementDate = new Date(productAPI.getReplacementDate());
                Date now = new Date();
                doReplacementProduct(product, productAPI, replacementDate, now, organizationUniqueId, uniqueId, userAPI);
            }
        }        
    }

    /**
     * Handle product replacement. If replacementdate is in the past, the product
     * is discontinued
     *
     * @param product product to handle replacement of
     * @param productAPI user supplied values
     * @param replacementDate date of replacement
     * @param now current date
     * @param organizationUniqueId unique id of the organization
     * @param uniqueId unique id of the product
     * @throws HjalpmedelstjanstenValidationException
     */
    private void doReplacementProduct( Product product, ProductAPI productAPI, Date replacementDate, Date now, long organizationUniqueId, long uniqueId, UserAPI userAPI ) throws HjalpmedelstjanstenValidationException {
        // check if date is passed, in that case we need to set the product
        // to DISCONTINUED immediately
        product.setReplacementDate(replacementDate);
        product.setInactivateRowsOnReplacement(productAPI.getInactivateRowsOnReplacement());
        if( replacementDate.before(now) ) {
            discontinueProduct(product, productAPI.getInactivateRowsOnReplacement(), organizationUniqueId, uniqueId, userAPI);
        }
        updateReplacedByProduct(organizationUniqueId, productAPI.getReplacedByProducts(), product, userAPI);
    }

    /**
     * Discontinue the given product and articles based on it
     *
     * @param product the product to discontinue
     * @param inactivateRows whether rows (gp and agreement) for articles based on product should be inactivated or not
     * @param organizationUniqueId unique id of the organization
     * @param uniqueId unique id of the product
     * @param userAPI
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException
     */
    public void discontinueProduct( Product product, Boolean inactivateRows, long organizationUniqueId, long uniqueId, UserAPI userAPI ) throws HjalpmedelstjanstenValidationException {
        product.setStatus(Product.Status.DISCONTINUED);
        List<CategorySpecificProperty> categorySpecificPropertys = categoryController.getCategoryPropertys(product.getCategory().getUniqueId());
        ProductAPI productAPI = ProductMapper.map( product, true, categorySpecificPropertys );
        elasticSearchController.bulkIndexProduct(productAPI);

        // set all PUBLISHED articles based on this product to DISCONTINUED
        // remove all UNPUBLISHED articles based on this product
        List<Article> articles = getArticlesBasedOnProduct(organizationUniqueId, uniqueId);
        if( articles != null && !articles.isEmpty() ) {
            for( Article article : articles ) {
                if( article.getStatus() == Product.Status.PUBLISHED ) {
                    articleController.doReplacementArticle(article, null, inactivateRows, product.getReplacementDate(), new Date(), organizationUniqueId, userAPI);
                    //articleController.doReplacementArticle(article, null, product.getReplacementDate(), new Date(), organizationUniqueId, userAPI);
                    ArticleAPI articleAPI = ArticleMapper.map(article, true, categorySpecificPropertys);
                    elasticSearchController.bulkIndexArticle(articleAPI);
                } else {
                    // ignore already DISCONTINUED articles
                }
            }
        }
    }

    /**
     * Update category on the given product. new Category must be of the same
     * article type as the old category.
     *
     * @param productAPI user supplied values
     * @param product the product to update category on.
     * @throws HjalpmedelstjanstenValidationException if validation fails
     */
    private boolean updateCategory(ProductAPI productAPI, Product product) throws HjalpmedelstjanstenValidationException {
        boolean categoryWasChanged = false;
        // has the category changed?
        if( !productAPI.getCategory().getId().equals(product.getCategory().getUniqueId()) ) {
            // category can only be changed between article type H and T (with a code)
            List<Category> categories = categoryController.getChildlessById(productAPI.getCategory().getId());
            if( categories == null || categories.isEmpty() ) {
                LOG.log(Level.FINEST, "category {0} does not exist or is not a leaf node", new Object[] {productAPI.getCategory().getId()});
                throw validationMessageService.generateValidationException("category", "product.category.notExists");
            }
            Category category = categories.get(0);
            if( !isValidProductChangeToCategory(category) ) {
                LOG.log(Level.FINEST, "product {0} cannot update category to {1} since it does not have a valid article type: {2}, or code: {3}", new Object[] {product.getUniqueId(), productAPI.getCategory().getId(), category.getArticleType(), category.getCode()});
                throw validationMessageService.generateValidationException("category", "product.update.wrongCategory");
            }
            product.setCategory(categories.get(0));
            categoryWasChanged = true;
        }
        return categoryWasChanged;
    }

    private boolean isValidProductChangeToCategory( Category category ) {
        // can always change to H
        if( category.getArticleType() == Article.Type.H ) {
            return true;
        }
        // can also change to T if the category has a code
        if( category.getArticleType() == Article.Type.T && 
                (category.getCode() != null && !category.getCode().isEmpty()) ) {
            return true;
        }
        return false;
    }
    
    /**
     * Set extended categories on the given product
     *
     * @param productAPI user supplied values
     * @param product the product to update
     * @throws HjalpmedelstjanstenValidationException
     */
    public void setExtendedCategories(ProductAPI productAPI, Product product) throws HjalpmedelstjanstenValidationException {
        if( productAPI.getExtendedCategories() != null && !productAPI.getExtendedCategories().isEmpty() ) {
            if( product.getCategory().getCode() == null || product.getCategory().getCode().isEmpty() ) {
                LOG.log(Level.WARNING, "product {0} main category {1} has no code and extended categories can therefore not be set", new Object[] {product.getUniqueId(), product.getCategory().getUniqueId()});
                throw validationMessageService.generateValidationException("extendedCategories", "product.extendedCategory.categoryNoCode");
            }
            List<Category> extendedCategories = new ArrayList<>();
            for( CategoryAPI categoryAPI : productAPI.getExtendedCategories()) {
                List<Category> extendedCategoriesSearchResult = categoryController.getChildlessById(categoryAPI.getId());
                if( extendedCategoriesSearchResult == null || extendedCategoriesSearchResult.isEmpty() ) {
                    LOG.log(Level.FINEST, "category {0} does not exist or is not a leaf node", new Object[] {categoryAPI.getId()});
                    throw validationMessageService.generateValidationException("extendedCategories", "product.category.notExists");
                }
                extendedCategories.add(extendedCategoriesSearchResult.get(0));
            }
            product.setExtendedCategories(extendedCategories);
        }
    }

    /**
     * Set directive and standard on product
     *
     * @param productAPI user supplied values
     * @param product the product to set info on
     * @throws HjalpmedelstjanstenValidationException
     */
    public void setCe(ProductAPI productAPI, Product product) throws HjalpmedelstjanstenValidationException {
        if( !productAPI.isCeMarked() ) {
            product.setCeDirective(null);
        } else {
            if( productAPI.getCeDirective() != null ) {
                CVCEDirective directive = ceController.findCEDirectiveById(productAPI.getCeDirective().getId());
                if( directive == null ) {
                    LOG.log(Level.FINEST, "directive {0} does not exist", new Object[] {productAPI.getCeDirective().getId()});
                    throw validationMessageService.generateValidationException("ceDirective", "product.directive.notExists");
                }
                product.setCeDirective(directive);
            } else {
                product.setCeDirective(null);
            }
        }
        if( productAPI.getCeStandard() != null ) {
            CVCEStandard standard = ceController.findCEStandardById(productAPI.getCeStandard().getId());
            if( standard == null ) {
                LOG.log(Level.FINEST, "standard {0} does not exist", new Object[] {productAPI.getCeStandard().getId()});
                throw validationMessageService.generateValidationException("ceStandard", "product.standard.notExists");
            }
            product.setCeStandard(standard);
        } else {
            product.setCeStandard(null);
        }
    }

    /**
     * Update manufacturer information on product (multiple fields)
     *
     * @param productAPI user supplied values
     * @param product the product to set info on
     */
    private void updateManufacturer(ProductAPI productAPI, Product product) {
        product.setManufacturer(productAPI.getManufacturer());
        product.setManufacturerProductNumber(productAPI.getManufacturerProductNumber());
        product.setTrademark(productAPI.getTrademark());
        if( productAPI.getManufacturerElectronicAddress() != null ) {
            if( product.getManufacturerElectronicAddress() != null ) {
                product.getManufacturerElectronicAddress().setWeb(productAPI.getManufacturerElectronicAddress().getWeb());
            } else {
                ElectronicAddress electronicAddress = new ElectronicAddress();
                electronicAddress.setWeb(productAPI.getManufacturerElectronicAddress().getWeb());
                product.setManufacturerElectronicAddress(electronicAddress);
            }
        } else {
            product.setManufacturerElectronicAddress(null);
        }
    }

    /**
     * Update preventive maintenance on product (multiple fields)
     *
     * @param productAPI user supplied values
     * @param product the product to set info on
     */
    private void updatePreventiveMaintenance(ProductAPI productAPI, Product product, UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updatePreventiveMaintenance(...)");
        product.setPreventiveMaintenanceDescription(productAPI.getPreventiveMaintenanceDescription());
        product.setPreventiveMaintenanceNumberOfDays(productAPI.getPreventiveMaintenanceNumberOfDays());
        setPreventiveMaintenanceValidFrom(productAPI, product, userAPI);
    }

    /**
     * Update replaced by product
     *
     * @param organizationUniqueId unique id of the organization
     * @param replacedByProductAPIs products to be replaced by, may be null
     * @param product product to replace
     * @throws HjalpmedelstjanstenValidationException if validation fails e.g.
     * replacement product does not exist
     */
    private void updateReplacedByProduct(long organizationUniqueId, List<ProductAPI> replacedByProductAPIs, Product product, UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateReplacedByProduct(organizationUniqueId: {0})", new Object[] {organizationUniqueId});
        if( replacedByProductAPIs != null ) {
            product.setReplacedByProducts(new ArrayList<>());
            for( ProductAPI productAPI : replacedByProductAPIs ) {
                Product replacementProduct = getProduct(organizationUniqueId, productAPI.getId(), userAPI);
                if( replacementProduct == null ) {
                    LOG.log(Level.FINEST, "cannot set replacement product for product {0} since it does not exist", new Object[] {product.getUniqueId()});
                    throw validationMessageService.generateValidationException("replacedByProduct", "product.replacedBy.notExist");
                }
                if( replacementProduct.getUniqueId().equals(product.getUniqueId()) ) {
                    LOG.log(Level.FINEST, "cannot set replacement product for product {0} since it is the same", new Object[] {product.getUniqueId()});
                    throw validationMessageService.generateValidationException("replacedByProduct", "product.replacedBy.same");
                }
                product.getReplacedByProducts().add(replacementProduct);
            }
        } else {
            if( product.getReplacedByProducts() != null ) {
                product.setReplacedByProducts(null);
            }
        }
    }

    /**
     * Set preventive maintenance valid from on the <code>Product</code> (if
     * given)
     *
     * @param productAPI user supplied values
     * @param product the Product to add data to
     * @throws HjalpmedelstjanstenValidationException if validations fails, e.g.
     * if a <code>CVPreventiveMaintenance</code> does not exist.
     */
    public void setPreventiveMaintenanceValidFrom(ProductAPI productAPI, Product product, UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "setOrderInformation(...)");
        if( productAPI.getPreventiveMaintenanceValidFrom() != null ) {
            CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(productAPI.getPreventiveMaintenanceValidFrom().getCode());
            if( preventiveMaintenance == null ) {
                LOG.log(Level.WARNING, "attempt by user: {0} to set preventive maintenance with code: {1} which does not exist", new Object[] {userAPI.getId(), productAPI.getPreventiveMaintenanceValidFrom().getCode()});
                throw validationMessageService.generateValidationException("preventiveMaintenanceValidFrom", "product.preventiveMaintenanceValidFrom.notExist");
            }
            product.setPreventiveMaintenanceValidFrom(preventiveMaintenance);
        } else {
            product.setPreventiveMaintenanceValidFrom(null);
        }
    }


    /**
     * Set order information on the <code>Product</code>
     *
     * @param productAPI user supplied values
     * @param product the Product to add data to
     * @param allOrderUnits
     * @throws HjalpmedelstjanstenValidationException if validations fails, e.g.
     * if a <code>Unit</code> does not exist.
     */
    public void setOrderInformation(ProductAPI productAPI, Product product, Map<Long, CVOrderUnit> allOrderUnits, Map<Long, CVPackageUnit> allPackageUnits) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "setOrderInformation(...)");
        // order unit
        if( productAPI.getOrderUnit() != null ) {
            CVOrderUnit orderUnit = allOrderUnits.get(productAPI.getOrderUnit().getId());
            if( orderUnit == null ) {
                LOG.log(Level.FINEST, "cannot set order unit for product {0} since it does not exist", new Object[] {product.getUniqueId()});
                throw validationMessageService.generateValidationException("orderUnit", "product.orderUnit.notExist");
            }
            product.setOrderUnit(orderUnit);
        } else {
            product.setOrderUnit(null);
        }

        // article quantity in outer package
        product.setArticleQuantityInOuterPackage(productAPI.getArticleQuantityInOuterPackage());
        if( productAPI.getArticleQuantityInOuterPackageUnit() != null ) {
            CVOrderUnit articleQuantityInOuterPackageUnit = allOrderUnits.get(productAPI.getArticleQuantityInOuterPackageUnit().getId());
            if( articleQuantityInOuterPackageUnit == null ) {
                LOG.log(Level.FINEST, "cannot set article quantity in outer package unit for product {0} since it does not exist", new Object[] {product.getUniqueId()});
                throw validationMessageService.generateValidationException("articleQuantityInOuterPackageUnit", "product.articleQuantityInOuterPackageUnit.notExist");
            }
            product.setArticleQuantityInOuterPackageUnit(articleQuantityInOuterPackageUnit);
        } else {
            product.setArticleQuantityInOuterPackageUnit(null);
        }

        // package content
        product.setPackageContent(productAPI.getPackageContent());
        if( productAPI.getPackageContentUnit() != null ) {
            CVPackageUnit packageContentUnit = allPackageUnits.get(productAPI.getPackageContentUnit().getId());
            if( packageContentUnit == null ) {
                LOG.log(Level.FINEST, "cannot set article quantity in outer package unit for product {0} since it does not exist", new Object[] {product.getUniqueId()});
                throw validationMessageService.generateValidationException("packageContentUnit", "product.packageContentUnit.notExist");
            }
            product.setPackageContentUnit(packageContentUnit);
        } else {
            product.setPackageContentUnit(null);
        }

        // package level base
        product.setPackageLevelBase(productAPI.getPackageLevelBase());

        // package level middle
        product.setPackageLevelMiddle(productAPI.getPackageLevelMiddle());

        // package level top
        product.setPackageLevelTop(productAPI.getPackageLevelTop());
    }

    public void setResourceSpecificProperties(ProductAPI productAPI, Product product, List<CategorySpecificProperty> categorySpecificPropertys, Boolean categoryWasChanged) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "setResourceSpecificProperties( product->uniqueId: {1} )", new Object[] {product.getUniqueId()});

        if( categoryWasChanged != null && categoryWasChanged ) {
            product.getResourceSpecificPropertyValues().clear();
        }
        
        // add/update properties
        if( productAPI.getCategoryPropertys() != null && !productAPI.getCategoryPropertys().isEmpty() ) {
            if( product.getResourceSpecificPropertyValues() == null ) {
                product.setResourceSpecificPropertyValues(new ArrayList<>());
            }
            for( ResourceSpecificPropertyAPI resourceSpecificPropertyAPI : productAPI.getCategoryPropertys() ) {
                // first make sure the property exists for the product category
                CategorySpecificProperty categorySpecificProperty = categorySpecificPropertysContains(resourceSpecificPropertyAPI.getProperty().getId(), categorySpecificPropertys);
                if( categorySpecificProperty == null ) {
                    LOG.log(Level.WARNING, "attempt to set category specific property for product {0} but category: {1} of product does not have it", new Object[] {product.getUniqueId(), product.getCategory().getUniqueId()});
                    throw validationMessageService.generateValidationException("categoryProperties", "product.categoryProperty.notExist");
                }
                LOG.log( Level.FINEST, "Check if category specific property: {0} is already answered", new Object[] {categorySpecificProperty.getUniqueId()});
                if( resourceSpecificPropertyAPI.getId() != null ) {
                    // previously answered question, value may be updated
                    ResourceSpecificPropertyValue resourceSpecificPropertyValue = getResourceSpecificPropertyValue(product.getResourceSpecificPropertyValues(), resourceSpecificPropertyAPI.getId());
                    if( resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueTextField ) {
                        ResourceSpecificPropertyValueTextField resourceSpecificPropertyValueTextfield = (ResourceSpecificPropertyValueTextField) resourceSpecificPropertyValue;
                        resourceSpecificPropertyValueTextfield.setValue(resourceSpecificPropertyAPI.getTextValue());
                    } else if( resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueDecimal ) {
                        ResourceSpecificPropertyValueDecimal resourceSpecificPropertyValueDecimal = (ResourceSpecificPropertyValueDecimal) resourceSpecificPropertyValue;
                        resourceSpecificPropertyValueDecimal.setValue(resourceSpecificPropertyAPI.getDecimalValue());
                    } else if( resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueInterval ) {
                        ResourceSpecificPropertyValueInterval resourceSpecificPropertyValueInterval = (ResourceSpecificPropertyValueInterval) resourceSpecificPropertyValue;
                        resourceSpecificPropertyValueInterval.setFromValue(resourceSpecificPropertyAPI.getIntervalFromValue());
                        resourceSpecificPropertyValueInterval.setToValue(resourceSpecificPropertyAPI.getIntervalToValue());
                    } else if( resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueValueListSingle ) {
                        CategorySpecificPropertyListValue categorySpecificPropertyListValue = resourceSpecificPropertyAPI.getSingleListValue() == null ? null: categoryController.getCategorySpecificPropertyListValue(resourceSpecificPropertyAPI.getSingleListValue());
                        ResourceSpecificPropertyValueValueListSingle resourceSpecificPropertyValueValueListSingle = (ResourceSpecificPropertyValueValueListSingle) resourceSpecificPropertyValue;
                        resourceSpecificPropertyValueValueListSingle.setValue(categorySpecificPropertyListValue);
                    } else if( resourceSpecificPropertyValue instanceof ResourceSpecificPropertyValueValueListMultiple ) {
                        List<CategorySpecificPropertyListValue> categorySpecificPropertyListValues = new ArrayList<>();
                        ResourceSpecificPropertyValueValueListMultiple resourceSpecificPropertyValueValueListMultiple = (ResourceSpecificPropertyValueValueListMultiple) resourceSpecificPropertyValue;
                        if( resourceSpecificPropertyAPI.getMultipleListValue() != null && !resourceSpecificPropertyAPI.getMultipleListValue().isEmpty() ) {
                            for( Long listValue : resourceSpecificPropertyAPI.getMultipleListValue() ) {
                                CategorySpecificPropertyListValue categorySpecificPropertyListValue = categoryController.getCategorySpecificPropertyListValue(listValue);
                                categorySpecificPropertyListValues.add(categorySpecificPropertyListValue);
                            }
                        }
                        resourceSpecificPropertyValueValueListMultiple.setValues(categorySpecificPropertyListValues);
                    }
                } else {
                    // make sure property hasn't already been answered
                    ResourceSpecificPropertyValue alreadyAnsweredResourceSpecificPropertyValue = getResourceSpecificPropertyValueByCategorySpecificProperty(product.getResourceSpecificPropertyValues(), resourceSpecificPropertyAPI.getProperty().getId());
                    if( alreadyAnsweredResourceSpecificPropertyValue != null ) {
                        LOG.log(Level.WARNING, "attempt to set category specific property: {0} for product {1} but property has already been set in request ", new Object[] {categorySpecificProperty.getUniqueId(), product.getUniqueId()});
                        throw validationMessageService.generateValidationException("categoryProperties", "product.categoryProperty.alreadySet");
                    }
                    // only save new property if a value is set
                    if( resourceSpecificPropertyAPI.getTextValue() != null && !resourceSpecificPropertyAPI.getTextValue().isEmpty() ) {
                        ResourceSpecificPropertyValueTextField resourceSpecificPropertyValueTextfield = new ResourceSpecificPropertyValueTextField();
                        resourceSpecificPropertyValueTextfield.setProduct(product);
                        resourceSpecificPropertyValueTextfield.setCategorySpecificProperty(categorySpecificProperty);
                        resourceSpecificPropertyValueTextfield.setValue(resourceSpecificPropertyAPI.getTextValue());
                        em.persist(resourceSpecificPropertyValueTextfield);
                        product.getResourceSpecificPropertyValues().add(resourceSpecificPropertyValueTextfield);
                    } else if( resourceSpecificPropertyAPI.getDecimalValue() != null ) {
                        ResourceSpecificPropertyValueDecimal resourceSpecificPropertyValueDecimal = new ResourceSpecificPropertyValueDecimal();
                        resourceSpecificPropertyValueDecimal.setCategorySpecificProperty(categorySpecificProperty);
                        resourceSpecificPropertyValueDecimal.setProduct(product);
                        resourceSpecificPropertyValueDecimal.setValue(resourceSpecificPropertyAPI.getDecimalValue());
                        em.persist(resourceSpecificPropertyValueDecimal);
                        product.getResourceSpecificPropertyValues().add(resourceSpecificPropertyValueDecimal);
                    } else if( resourceSpecificPropertyAPI.getIntervalFromValue() != null || resourceSpecificPropertyAPI.getIntervalToValue() != null ) {
                        ResourceSpecificPropertyValueInterval resourceSpecificPropertyValueInterval = new ResourceSpecificPropertyValueInterval();
                        resourceSpecificPropertyValueInterval.setCategorySpecificProperty(categorySpecificProperty);
                        resourceSpecificPropertyValueInterval.setProduct(product);
                        resourceSpecificPropertyValueInterval.setFromValue(resourceSpecificPropertyAPI.getIntervalFromValue());
                        resourceSpecificPropertyValueInterval.setToValue(resourceSpecificPropertyAPI.getIntervalToValue());
                        em.persist(resourceSpecificPropertyValueInterval);
                        product.getResourceSpecificPropertyValues().add(resourceSpecificPropertyValueInterval);
                    } else if( resourceSpecificPropertyAPI.getSingleListValue() != null ) {
                        CategorySpecificPropertyListValue categorySpecificPropertyListValue = categoryController.getCategorySpecificPropertyListValue(resourceSpecificPropertyAPI.getSingleListValue());
                        ResourceSpecificPropertyValueValueListSingle resourceSpecificPropertyValueValueListSingle = new ResourceSpecificPropertyValueValueListSingle();
                        resourceSpecificPropertyValueValueListSingle.setCategorySpecificProperty(categorySpecificProperty);
                        resourceSpecificPropertyValueValueListSingle.setProduct(product);
                        resourceSpecificPropertyValueValueListSingle.setValue(categorySpecificPropertyListValue);
                        em.persist(resourceSpecificPropertyValueValueListSingle);
                        product.getResourceSpecificPropertyValues().add(resourceSpecificPropertyValueValueListSingle);
                    } else if( resourceSpecificPropertyAPI.getMultipleListValue() != null ) {
                        List<CategorySpecificPropertyListValue> categorySpecificPropertyListValues = new ArrayList<>();
                        ResourceSpecificPropertyValueValueListMultiple resourceSpecificPropertyValueValueListMultiple = new ResourceSpecificPropertyValueValueListMultiple();
                        resourceSpecificPropertyValueValueListMultiple.setCategorySpecificProperty(categorySpecificProperty);
                        resourceSpecificPropertyValueValueListMultiple.setProduct(product);
                        for( Long listValue : resourceSpecificPropertyAPI.getMultipleListValue() ) {
                            CategorySpecificPropertyListValue categorySpecificPropertyListValue = categoryController.getCategorySpecificPropertyListValue(listValue);
                            categorySpecificPropertyListValues.add(categorySpecificPropertyListValue);
                        }
                        resourceSpecificPropertyValueValueListMultiple.setValues(categorySpecificPropertyListValues);
                        em.persist(resourceSpecificPropertyValueValueListMultiple);
                        product.getResourceSpecificPropertyValues().add(resourceSpecificPropertyValueValueListMultiple);

                    }
                }
            }
        }
    }

    public ResourceSpecificPropertyValue getResourceSpecificPropertyValue(List<ResourceSpecificPropertyValue> resourceSpecificPropertyValues, long resourceSpecificPropertyId) {
        if( resourceSpecificPropertyValues != null && !resourceSpecificPropertyValues.isEmpty() ) {
            for( ResourceSpecificPropertyValue resourceSpecificPropertyValue : resourceSpecificPropertyValues ) {
                if( resourceSpecificPropertyValue.getUniqueId().equals(resourceSpecificPropertyId) ) {
                    return resourceSpecificPropertyValue;
                }
            }
        }
        return null;
    }

    public ResourceSpecificPropertyValue getResourceSpecificPropertyValueByCategorySpecificProperty(List<ResourceSpecificPropertyValue> resourceSpecificPropertyValues, long categorySpecificPropertyId) {
        if( resourceSpecificPropertyValues != null && !resourceSpecificPropertyValues.isEmpty() ) {
            for( ResourceSpecificPropertyValue resourceSpecificPropertyValue : resourceSpecificPropertyValues ) {
                if( resourceSpecificPropertyValue.getCategorySpecificProperty().getUniqueId().equals(categorySpecificPropertyId) ) {
                    return resourceSpecificPropertyValue;
                }
            }
        }
        return null;
    }

    public CategorySpecificProperty categorySpecificPropertysContains(long categorySpecificPropertyId, List<CategorySpecificProperty> categorySpecificPropertys) {
        if( categorySpecificPropertys != null && !categorySpecificPropertys.isEmpty() ) {
            for( CategorySpecificProperty categorySpecificProperty : categorySpecificPropertys ) {
                if( categorySpecificProperty.getUniqueId().equals(categorySpecificPropertyId) ) {
                    return categorySpecificProperty;
                }
            }
        }
        return null;
    }

    public List<ProductAPI> findByCategory(long organizationUniqueId, long categoryUniqueId, UserAPI userAPI) {
        List<CategorySpecificProperty> categorySpecificPropertys = categoryController.getCategoryPropertys(categoryUniqueId);
        return ProductMapper.map(em.createNamedQuery(Product.FIND_BY_ORGANIZATION_AND_CATEGORY).
                setParameter("organizationUniqueId", organizationUniqueId).
                setParameter("categoryUniqueId", categoryUniqueId).
                getResultList(), true, categorySpecificPropertys, userAPI);
    }

    public List<AgreementPricelistRowAPI> getProductPricelistRows(long productUniqueId, UserAPI userAPI) {
        LOG.log(Level.FINEST, "getArticlePricelistRows(articleUniqueId: {0})", new Object[] {productUniqueId});
        Organization organization = organizationController.getOrganizationFromLoggedInUser(userAPI);

        List<AgreementPricelistRow> agreementPricelistRows = agreementPricelistRowController.getOrganizationPricelistRowsByArticle(organization, productUniqueId, userAPI);

        List<AgreementPricelistRowAPI> agreementPricelistRowAPIS = AgreementPricelistRowMapper.map(agreementPricelistRows, true, true);

        articleController.restrictCustomersFromSeeingPriceOfDiscontinuedArticles(organization, new ArrayList<>(agreementPricelistRowAPIS));

        return agreementPricelistRowAPIS;
    }

    public List<String> getCustomerEmailByProduct(ProductAPI productAPI, UserAPI userAPI){
        //Hämta email för kunder som ska godkänna pristlistor för en artikel
        List<AgreementPricelistRowAPI> agreementPricelistRowAPIS = getProductPricelistRows(productAPI.getId(),userAPI);
        List<String> customerMailList = new ArrayList<>();
        int i = 0;
        for(AgreementPricelistRowAPI agreementPriceListRowAPI : agreementPricelistRowAPIS) {
            AgreementAPI agreement = agreementPricelistRowAPIS.get(i).getPricelist().getAgreement();
            for (UserAPI userEngagement : agreement.getCustomerPricelistApprovers()) {
                if(!customerMailList.contains(userEngagement.getElectronicAddress().getEmail())){
                    customerMailList.add(userEngagement.getElectronicAddress().getEmail());
                }
            }
            i++;
        }
        return customerMailList;
    }

    /**
     * The product number is editable if:
     *   - no articles based on products is in a pricelist
     *   - no articles that fits to this product is in a pricelist
     * @param product
     * @return 
     */
    private boolean isProductNumberEditable(Product product) {
        List<Long> articlesBasedOnAndFitsTo = em.createNamedQuery(Article.GET_IDS_BASED_ON_PRODUCT).
                setParameter("productUniqueId", product.getUniqueId()).
                getResultList();
        articlesBasedOnAndFitsTo.addAll(em.createNamedQuery(Article.GET_IDS_FITS_TO_PRODUCT).
                setParameter("productUniqueId", product.getUniqueId()).
                getResultList());
        if( !articlesBasedOnAndFitsTo.isEmpty() ) {
            if( agreementPricelistRowController.existRowsByArticle(articlesBasedOnAndFitsTo) ||
                    generalPricelistPricelistRowController.existRowsByArticle(articlesBasedOnAndFitsTo)) {
                LOG.log( Level.FINEST, "Articles based on product: {0} or one that fits to it exists in agreement or gp pricelist so number is not editable", new Object[] {product.getUniqueId()});
                return false;
            }
        }
        return true;
    }
    
}

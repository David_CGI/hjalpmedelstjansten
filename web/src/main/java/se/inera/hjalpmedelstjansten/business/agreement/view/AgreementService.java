/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.agreement.view;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import se.inera.hjalpmedelstjansten.business.BaseService;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenException;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.ResultExporterController;
import se.inera.hjalpmedelstjansten.business.agreement.controller.AgreementController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.business.security.view.AuthHandler;
import se.inera.hjalpmedelstjansten.business.security.view.SecuredService;
import se.inera.hjalpmedelstjansten.model.api.AgreementAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.Agreement;

/**
 * REST API for agreements.
 * 
 * @author Tommy Berglund
 */
@Stateless
@Path("organizations/{organizationUniqueId}/agreements")
@Interceptors({ PerformanceLogInterceptor.class })
public class AgreementService extends BaseService {
    
    @Inject
    private HjmtLogger LOG;
    
    @EJB
    AgreementController agreementController;
    
    @Inject
    ResultExporterController resultExporterController;
    
    @EJB
    AuthHandler authHandler;
    
    /**
     * Search agreements by user query
     * 
     * @param query the user query
     * @param organizationUniqueId
     * @param statuses
     * @param include
     * @param offset for pagination purpose
     * @param limit for assortment we need to be able to return all agreements, 0 
     * means return all agreements
     * @return a list of <code>AgreementAPI</code> matching the query and a 
     * header X-Total-Count giving the total number of agreements for pagination
     */
    @GET
    @SecuredService(permissions = {"agreement:view_own"})
    public Response searchAgreeements(
            @DefaultValue(value = "") @QueryParam("query") String query, 
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @QueryParam("status") List<Agreement.Status> statuses,
            @QueryParam("include") AgreementAPI.Include include,
            @DefaultValue(value = "0") @QueryParam("offset") int offset,
            @DefaultValue(value = "25") @QueryParam("limit") int limit) {
        LOG.log(Level.FINEST, "searchAgreeements( organizationUniqueId: {0}, offset: {1} )", new Object[] {organizationUniqueId, offset});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        if( query != null && query.isEmpty() ) {
            query = null;
        }
        boolean onlyWhereContactPerson = false;
        if( include != null && include == AgreementAPI.Include.MINE ) {
            onlyWhereContactPerson = true;
        }
        long countSearch = agreementController.countSearchAgreements(query, statuses, organizationUniqueId, userAPI, onlyWhereContactPerson);
        List<AgreementAPI> agreementAPIs = agreementController.searchAgreements(query,statuses, organizationUniqueId, userAPI, onlyWhereContactPerson, offset, limit);
        return Response.ok(agreementAPIs).
                header("X-Total-Count", countSearch).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                build();
    }
    
    /**
     * Export search result list to file. Same permission required as
     * searching. 
     * 
     * @param organizationUniqueId
     * @param query the user query
     * @param statuses
     * @param include
     * @return
     */
    @GET
    @Path("export")
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=UTF-8")
    @SecuredService(permissions = {"agreement:view_own"})    
    public Response exportSearchAgreeements(
            @DefaultValue(value = "") @QueryParam("query") String query, 
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @QueryParam("status") List<Agreement.Status> statuses,
            @QueryParam("include") AgreementAPI.Include include) {
        LOG.log(Level.FINEST, "searchAgreeements( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        if( query != null && query.isEmpty() ) {
            query = null;
        }
        boolean onlyWhereContactPerson = false;
        if( include != null && include == AgreementAPI.Include.MINE ) {
            onlyWhereContactPerson = true;
        }
        List<AgreementAPI> agreementAPIs = agreementController.searchAgreements(query,statuses, organizationUniqueId, userAPI, onlyWhereContactPerson, 0, Integer.MAX_VALUE);
        if( agreementAPIs == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            try {
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'_'HHmm");
                String filename = "Export_Avtal_" + dateTimeFormatter.format(ZonedDateTime.now()) + ".xlsx";
                byte[] exportBytes = resultExporterController.generateAgreementResultList(agreementAPIs);
                return Response.ok(
                        exportBytes,
                        javax.ws.rs.core.MediaType.APPLICATION_OCTET_STREAM)
                        .header("content-disposition", "attachment; filename = " + filename)
                        .build();
            } catch (IOException ex) {
                LOG.log(Level.WARNING, "Failed to export agreements search to file", ex);
                return Response.serverError().build();
            }
        }
    }
    
    /**
     * Get information about a specific agreement 
     * 
     * @param httpServletRequest
     * @param organizationUniqueId the unique id of the organization to get agreement from
     * @param uniqueId the id of the agreement to return
     * @return the corresponding <code>AgreementAPI</code>
     */
    @GET
    @Path("{uniqueId : \\d+}") // the backslash backslash d+ part is to distinguish this path from another by saying it's only digits
    @SecuredService(permissions = {"agreement:view_own"})
    public Response getAgreement(@Context HttpServletRequest httpServletRequest,
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @PathParam("uniqueId") long uniqueId ) {
        LOG.log(Level.FINEST, "getAgreement( organizationUniqueId: {0}, uniqueId: {1} )", new Object[] {organizationUniqueId, uniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        AgreementAPI agreementAPI = agreementController.getAgreementAPI( organizationUniqueId, uniqueId, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest) );
        if( agreementAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(agreementAPI).build();
        }
    }
    
    /**
     * Create new agreement 
     * 
     * @param organizationUniqueId unique id of the organization to create agreement on
     * @param agreementAPI user supplied values
     * @return the created <code>AgreementAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenException
     */
    @POST
    @SecuredService(permissions = {"agreement:create_own"})
    public Response createAgreement(@Context HttpServletRequest httpServletRequest, 
            @PathParam("organizationUniqueId") long organizationUniqueId, 
            AgreementAPI agreementAPI) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "createAgreement( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }

        agreementAPI = agreementController.createAgreement(organizationUniqueId, agreementAPI, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        return Response.ok(agreementAPI).build();
    }
        
    /**
     * Update an existing <code>Agreement</code>
     * 
     * @param organizationUniqueId unique id of organization
     * @param uniqueId the id of the agreement to update
     * @param agreementAPI user supplied values
     * @return the updated <code>AgreementAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException in case validation fails
     */
    @PUT
    @Path("{uniqueId}")
    @SecuredService(permissions = {"agreement:update_own"})
    public Response updateAgreement(@Context HttpServletRequest httpServletRequest, 
            @PathParam("organizationUniqueId") long organizationUniqueId, 
            @PathParam("uniqueId") long uniqueId, 
            AgreementAPI agreementAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateAgreement( uniqueId: {0} )", new Object[] {uniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        agreementAPI = agreementController.updateAgreement(organizationUniqueId, uniqueId, agreementAPI, userAPI, authHandler.getSessionId(), getRequestIp(httpServletRequest));
        if( agreementAPI == null ) {
            return Response.status(Response.Status.NOT_FOUND).build();
        } else {
            return Response.ok(agreementAPI).build();
        }
    }
    
    @GET
    @Path("pricelistapprovers")
    @SecuredService(permissions = {"agreement:view_own"}) 
    public Response searchPricelistApprovers(
            @PathParam("organizationUniqueId") long organizationUniqueId,
            @QueryParam("query") String query,
            @DefaultValue(value = "0") @QueryParam("offset") int offset,
            @QueryParam("exclude") List<Long> excludeUserEngagementIds,
            @QueryParam("businessLevel") List<Long> businessLevelIds) {
        LOG.log(Level.FINEST, "searchPricelistApprovers( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        UserAPI userAPI = (UserAPI) authHandler.getFromSession(AuthHandler.USER_API_SESSION_KEY);
        if( !authorizeHandleOrganization(organizationUniqueId, userAPI) ) {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
        
        SearchDTO searchDTO = agreementController.searchPricelistApprovers(organizationUniqueId, query, offset, 25, excludeUserEngagementIds, businessLevelIds); 
        return Response.ok(searchDTO.getItems()).
                header("X-Total-Count", searchDTO.getCount()).
                header("Access-Control-Expose-Headers", "X-Total-Count"). // header to allow frontend to read X-Total-Count header
                build();
    }
        
}

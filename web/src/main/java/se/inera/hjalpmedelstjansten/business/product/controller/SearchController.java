/*
 * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *
 * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *
 * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.product.controller;

import se.inera.hjalpmedelstjansten.business.indexing.controller.ElasticSearchController;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.core.MultivaluedMap;

import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.model.api.SearchAPI;
import se.inera.hjalpmedelstjansten.model.api.SearchProductsAndArticlesAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Category;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificProperty;
import se.inera.hjalpmedelstjansten.model.entity.Product;

/**
 * Class for handling business logic of search Articles and Products. This
 * includes talking to the database.
 *
 * @author Tommy Berglund
 */
@Stateless
public class SearchController {

    @Inject
    HjmtLogger LOG;

    @PersistenceContext(unitName = "HjmtjPU")
    EntityManager em;

    @Inject
    Event<InternalAuditEvent> internalAuditEvent;

    @Inject
    ArticleController articleController;

    @Inject
    CategoryController categoryController;

    @Inject
    ElasticSearchController elasticSearchController;

    @Inject
    ValidationMessageService validationMessageService;

    /**
     * Count all articles connected to product
     *
     * @param organizationUniqueId
     * @param productUniqueId       unique id of the product
     * @param query
     * @param statuses
     * @param articleTypes
     * @param excludeCustomerUnique if true, we exclude all articles that is customer unique,
     *                              either in themselves or thru inheritence
     * @return
     */
    public long countSearchArticlesByProduct(long organizationUniqueId,
                                             Long productUniqueId,
                                             String query,
                                             List<Product.Status> statuses,
                                             List<Article.Type> articleTypes,
                                             boolean excludeCustomerUnique) {
        LOG.log(Level.FINEST, "countSearchProductsAndArticlesByProduct( organizationUniqueId: {0}, productUniqueId: {1}, statuses: {2}, articleTypes: {3} )", new Object[]{organizationUniqueId, productUniqueId, statuses, articleTypes});
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT 'ARTICLE', COUNT(*) FROM Article a "
                + "LEFT JOIN Category ac ON a.categoryId=ac.uniqueId "
                + "LEFT JOIN Product ap ON a.basedOnProductId=ap.uniqueId "
                + "LEFT JOIN Category apc ON ap.categoryId=apc.uniqueId ");
        queryBuilder.append("LEFT JOIN ArticleFitsToProduct pfta ON pfta.articleId=a.uniqueId ");
        queryBuilder.append("WHERE (a.organizationId = :organizationUniqueId OR ap.organizationId = :organizationUniqueId) ");
        queryBuilder.append("AND (pfta.productId = :productUniqueId OR ap.uniqueId = :productUniqueId) ");
        if (query != null && !query.isEmpty()) {
            queryBuilder.append("AND (a.articleName LIKE :query OR apc.code LIKE :query OR apc.name LIKE :query OR ac.code LIKE :query OR ac.name LIKE :query) ");
        }
        /*
        if (statuses != null && !statuses.isEmpty()) {
            queryBuilder.append("AND (");
            for (int i = 0; i < statuses.size(); i++) {
                if (i != 0) {
                    queryBuilder.append("OR ");
                }
                queryBuilder.append("a.status = :status").append(i).append(" ");
            }
            queryBuilder.append(") ");
        }
        */
        if (statuses != null && !statuses.isEmpty() && statuses.size() == 1) {
            queryBuilder.append("AND (a.status = :status").append(0).append(" )");
        }
        if (articleTypes != null && !articleTypes.isEmpty()) {
            queryBuilder.append("AND (");
            for (int i = 0; i < articleTypes.size(); i++) {
                if (i != 0) {
                    queryBuilder.append("OR ");
                }
                queryBuilder.append("(ac.articleType = :type").append(i).append(" OR apc.articleType = :type").append(i).append(") ");
            }
            queryBuilder.append(") ");
        }
        if (excludeCustomerUnique) {
            queryBuilder.append("AND (a.customerUnique = 0 OR (a.customerUniqueOverridden = 0 AND ap.customerUnique = 0)) ");
        }
        LOG.log(Level.FINEST, "queryBuilder: {0}", new Object[]{queryBuilder.toString()});
        Query searchQuery = em.createNativeQuery(queryBuilder.toString());
        searchQuery.setParameter("organizationUniqueId", organizationUniqueId);
        if (productUniqueId != null) {
            searchQuery.setParameter("productUniqueId", productUniqueId);
        }
        if (query != null && !query.isEmpty()) {
            searchQuery.setParameter("query", "%" + query + "%");
        }

        /*
        if (statuses != null && !statuses.isEmpty()) {
            for (int i = 0; i < statuses.size(); i++) {
                searchQuery.setParameter("status" + i, statuses.get(0).toString());
            }
        }
        */

        if (statuses != null && !statuses.isEmpty() && statuses.size() == 1) {
            searchQuery.setParameter("status" + 0, statuses.get(0).toString());
        }


        if (articleTypes != null && !articleTypes.isEmpty()) {
            for (int i = 0; i < articleTypes.size(); i++) {
                searchQuery.setParameter("type" + i, articleTypes.get(i).toString());
            }
        }
        List<Object[]> objectArrayList = searchQuery.getResultList();
        long total = 0;
        for (Object[] objectArray : objectArrayList) {
            long count = ((BigInteger) objectArray[1]).longValue();
            total += count;
        }
        return total;
    }

    /**
     * Search all articles connected to product
     *
     * @param organizationUniqueId
     * @param productUniqueId       unique id of the product
     * @param query
     * @param statuses
     * @param articleTypes
     * @param offset
     * @param limit                 variable scope of how many articles should be presented per page in the UI.
     * @param excludeCustomerUnique if true, we exclude all articles that is customer unique,
     *                              either in themselves or thru inheritence
     * @return
     */
    public List<SearchProductsAndArticlesAPI> searchArticlesByProduct(long organizationUniqueId,
                                                                      long productUniqueId,
                                                                      String query,
                                                                      List<Product.Status> statuses,
                                                                      List<Article.Type> articleTypes,
                                                                      int offset,
                                                                      int limit,
                                                                      boolean excludeCustomerUnique) {

        LOG.log(Level.FINEST, "searchProductsAndArticlesByProduct( organizationUniqueId: {0}, productUniqueId: {1}, statuses: {2}, articleTypes: {3}, offset: {4}, limit: {5} )", new Object[]{organizationUniqueId, productUniqueId, statuses, articleTypes, offset, limit});
        limit = adjustLimitToAllowedScope(limit);
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT 'ARTICLE', "
                + "a.uniqueId, "
                + "a.articleName AS 'name', "
                + "a.articleNumber, "
                + "a.status, "
                + "ac.code AS code1, "
                + "apc.code AS code2, "
                + "ac.articleType AS articleType1, "
                + "apc.articleType AS articleType2 from Article a "
                + "LEFT JOIN Category ac ON a.categoryId=ac.uniqueId "
                + "LEFT JOIN Product ap ON a.basedOnProductId=ap.uniqueId "
                + "LEFT JOIN Category apc ON ap.categoryId=apc.uniqueId ");
        queryBuilder.append("LEFT JOIN ArticleFitsToProduct pfta ON pfta.articleId=a.uniqueId ");
        queryBuilder.append("WHERE (a.organizationId = :organizationUniqueId OR ap.organizationId = :organizationUniqueId) ");
        queryBuilder.append("AND (pfta.productId = :productUniqueId OR ap.uniqueId = :productUniqueId) ");
        if (query != null && !query.isEmpty()) {
            queryBuilder.append("AND (a.articleName LIKE :query OR apc.code LIKE :query OR apc.name LIKE :query OR ac.code LIKE :query OR ac.name LIKE :query) ");
        }
        /*
        if (statuses != null && !statuses.isEmpty()) {
            queryBuilder.append("AND (");
            for (int i = 0; i < statuses.size(); i++) {
                if (i != 0) {
                    queryBuilder.append("OR ");
                }
                queryBuilder.append("a.status = :status").append(i).append(" ");
            }
            queryBuilder.append(") ");
        }
        */
        if (statuses != null && !statuses.isEmpty() && statuses.size() == 1) {
            queryBuilder.append("AND (a.status = :status").append(0).append(" )");
        }
        if (articleTypes != null && !articleTypes.isEmpty()) {
            queryBuilder.append("AND (");
            for (int i = 0; i < articleTypes.size(); i++) {
                if (i != 0) {
                    queryBuilder.append("OR ");
                }
                queryBuilder.append("(ac.articleType = :type").append(i).append(" OR apc.articleType = :type").append(i).append(") ");
            }
            queryBuilder.append(") ");
        }
        if (excludeCustomerUnique) {
            queryBuilder.append("AND (a.customerUnique = 0 OR (a.customerUniqueOverridden = 0 AND ap.customerUnique = 0)) ");
        }
        queryBuilder.append("ORDER BY name ASC");
        LOG.log(Level.FINEST, "queryBuilder: {0}", new Object[]{queryBuilder.toString()});
        Query searchQuery = em.createNativeQuery(queryBuilder.toString()).
                setParameter("organizationUniqueId", organizationUniqueId).
                setParameter("productUniqueId", productUniqueId);
        if (query != null && !query.isEmpty()) {
            searchQuery.setParameter("query", "%" + query + "%");
        }


        if (statuses != null && !statuses.isEmpty() && statuses.size() == 1) {
                searchQuery.setParameter("status" + 0, statuses.get(0).toString());
        }


        if (articleTypes != null && !articleTypes.isEmpty()) {
            for (int i = 0; i < articleTypes.size(); i++) {
                searchQuery.setParameter("type" + i, articleTypes.get(i).toString());
            }
        }
        searchQuery.setMaxResults(limit);
        searchQuery.setFirstResult(offset);
        List<Object[]> objectArrayList = searchQuery.getResultList();
        return SearchMapper.map(objectArrayList);
    }

    /**
     * Count products and articles connected to article. An article connected to a product
     * which in turn has articles based on it is also indirectly connected to those articles
     *
     * @param organizationUniqueId
     * @param article               unique id of the article
     * @param query
     * @param statuses
     * @param articleTypes
     * @param excludeCustomerUnique if true, we exclude all articles that is customer unique,
     *                              either in themselves or thru inheritence
     * @return
     */
    public long countSearchProductsAndArticlesForArticle(long organizationUniqueId,
                                                         Article article,
                                                         String query,
                                                         List<Product.Status> statuses,
                                                         List<Article.Type> articleTypes,
                                                         boolean excludeCustomerUnique) {
        LOG.log(Level.FINEST, "countSearchProductsAndArticlesForArticle( organizationUniqueId: {0}, articleUniqueId: {1}, statuses: {2}, articleTypes: {3} )", new Object[]{organizationUniqueId, article.getUniqueId(), statuses, articleTypes});
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT 'PRODUCT', COUNT(*) FROM Product p "
                + "LEFT JOIN Category pc ON p.categoryId=pc.uniqueId ");
        queryBuilder.append("WHERE (p.organizationId = :organizationUniqueId) ");
        queryBuilder.append("AND p.uniqueId IN (SELECT productId FROM ArticleFitsToProduct WHERE articleId=:articleUniqueId) ");
        if (query != null && !query.isEmpty()) {
            queryBuilder.append("AND (p.productName LIKE :query OR pc.code LIKE :query OR pc.name LIKE :query) ");
        }
        /*
        if (statuses != null && !statuses.isEmpty()) {
            queryBuilder.append("AND (");
            for (int i = 0; i < statuses.size(); i++) {
                if (i != 0) {
                    queryBuilder.append("OR ");
                }
                queryBuilder.append("p.status = :status").append(i).append(" ");
            }
            queryBuilder.append(") ");
        }
        */
        if (statuses != null && !statuses.isEmpty() && statuses.size() == 1) {
            queryBuilder.append("AND (p.status = :status").append(0).append(" )");
        }
        if (excludeCustomerUnique) {
            queryBuilder.append("AND p.customerUnique = 0 ");
        }

        queryBuilder.append("UNION ");
        queryBuilder.append("SELECT 'ARTICLE', COUNT(*) FROM Article a "
                + "LEFT JOIN Category ac ON a.categoryId=ac.uniqueId "
                + "LEFT JOIN Product ap ON a.basedOnProductId=ap.uniqueId "
                + "LEFT JOIN Category apc ON ap.categoryId=apc.uniqueId ");
        queryBuilder.append("WHERE (a.organizationId = :organizationUniqueId OR ap.organizationId = :organizationUniqueId) ");
        queryBuilder.append("AND (a.uniqueId IN (SELECT fittedByArticleId FROM ArticleFitsToArticle WHERE fitsToArticleId=:articleUniqueId)) OR (a.uniqueId IN (SELECT fitsToArticleId FROM ArticleFitsToArticle WHERE fittedByArticleId=:articleUniqueId)) ");
        if (query != null && !query.isEmpty()) {
            queryBuilder.append("AND (a.articleName LIKE :query OR apc.code LIKE :query OR apc.name LIKE :query OR ac.code LIKE :query OR ac.name LIKE :query) ");
        }
        /*
        if (statuses != null && !statuses.isEmpty()) {
            queryBuilder.append("AND (");
            for (int i = 0; i < statuses.size(); i++) {
                if (i != 0) {
                    queryBuilder.append("OR ");
                }
                queryBuilder.append("a.status = :status").append(i).append(" ");
            }
            queryBuilder.append(") ");
        }
        */
        if (statuses != null && !statuses.isEmpty() && statuses.size() == 1) {
            queryBuilder.append("AND (a.status = :status").append(0).append(" )");
        }
        if (articleTypes != null && !articleTypes.isEmpty()) {
            queryBuilder.append("AND (");
            for (int i = 0; i < articleTypes.size(); i++) {
                if (i != 0) {
                    queryBuilder.append("OR ");
                }
                queryBuilder.append("(ac.articleType = :type").append(i).append(" OR apc.articleType = :type").append(i).append(") ");
            }
            queryBuilder.append(") ");
        }
        if (excludeCustomerUnique) {
            queryBuilder.append("AND (a.customerUnique = 0 OR (a.customerUniqueOverridden = 0 AND ap.customerUnique = 0)) ");
        }

        // if article is based on product we want all articles connected to that product as well
        if (article.getBasedOnProduct() != null) {
            queryBuilder.append("UNION ");
            queryBuilder.append("SELECT 'ARTICLE', COUNT(*) FROM Article a "
                    + "LEFT JOIN Category ac ON a.categoryId=ac.uniqueId "
                    + "LEFT JOIN Product ap ON a.basedOnProductId=ap.uniqueId "
                    + "LEFT JOIN Category apc ON ap.categoryId=apc.uniqueId ");
            queryBuilder.append("WHERE (a.organizationId = :organizationUniqueId OR ap.organizationId = :organizationUniqueId) ");
            queryBuilder.append("AND a.uniqueId IN (SELECT articleId FROM ArticleFitsToProduct WHERE productId=:basedOnProductId) ");
            if (query != null && !query.isEmpty()) {
                queryBuilder.append("AND (a.articleName LIKE :query OR ac.code LIKE :query OR ac.name LIKE :query) ");
            }
            /*
            if (statuses != null && !statuses.isEmpty()) {
                queryBuilder.append("AND (");
                for (int i = 0; i < statuses.size(); i++) {
                    if (i != 0) {
                        queryBuilder.append("OR ");
                    }
                    queryBuilder.append("a.status = :status").append(i).append(" ");
                }
                queryBuilder.append(") ");
            }
            */
            if (statuses != null && !statuses.isEmpty() && statuses.size() == 1) {
                queryBuilder.append("AND (a.status = :status").append(0).append(" )");
            }
            if (articleTypes != null && !articleTypes.isEmpty()) {
                queryBuilder.append("AND (");
                for (int i = 0; i < articleTypes.size(); i++) {
                    if (i != 0) {
                        queryBuilder.append("OR ");
                    }
                    queryBuilder.append("(ac.articleType = :type").append(i).append(" OR apc.articleType = :type").append(i).append(") ");
                }
                queryBuilder.append(") ");
            }
        }
        if (excludeCustomerUnique) {
            queryBuilder.append("AND (a.customerUnique = 0 OR (a.customerUniqueOverridden = 0 AND ap.customerUnique = 0)) ");
        }
        LOG.log(Level.FINEST, "queryBuilder: {0}", new Object[]{queryBuilder.toString()});
        Query searchQuery = em.createNativeQuery(queryBuilder.toString());
        searchQuery.setParameter("organizationUniqueId", organizationUniqueId);
        searchQuery.setParameter("articleUniqueId", article.getUniqueId());

        if (query != null && !query.isEmpty()) {
            searchQuery.setParameter("query", "%" + query + "%");
        }
        if (statuses != null && !statuses.isEmpty() && statuses.size() == 1) {
            //for (int i = 0; i < statuses.size(); i++) {
                searchQuery.setParameter("status" + 0, statuses.get(0).toString());
            //}
        }
        if (articleTypes != null && !articleTypes.isEmpty()) {
            for (int i = 0; i < articleTypes.size(); i++) {
                searchQuery.setParameter("type" + i, articleTypes.get(i).toString());
            }
        }
        if (article.getBasedOnProduct() != null) {
            searchQuery.setParameter("basedOnProductId", article.getBasedOnProduct().getUniqueId());
        }
        List<Object[]> objectArrayList = searchQuery.getResultList();
        long total = 0;
        for (Object[] objectArray : objectArrayList) {
            long count = ((BigInteger) objectArray[1]).longValue();
            LOG.log(Level.FINEST, "testy0: {0} : {1}", new Object[]{objectArray[0].toString(), objectArray[1].toString()});
            total += count;
        }


        return total;
    }

    /**
     * Search products and articles connected to article. An article connected to a product
     * which in turn has articles based on it is also indirectly connected to those articles
     *
     * @param organizationUniqueId
     * @param article               unique id of the article
     * @param query
     * @param statuses
     * @param articleTypes
     * @param limit                 variable scope of how many articles should be presented per page in the UI.
     * @param excludeCustomerUnique if true, we exclude all articles that is customer unique,
     *                              either in themselves or thru inheritence
     * @return
     */
    public List<SearchProductsAndArticlesAPI> searchProductsAndArticlesForArticle(long organizationUniqueId,
                                                                                  Article article,
                                                                                  String query,
                                                                                  List<Product.Status> statuses,
                                                                                  List<Article.Type> articleTypes,
                                                                                  int offset,
                                                                                  int limit,
                                                                                  boolean excludeCustomerUnique) {
        LOG.log(Level.FINEST, "searchProductsAndArticlesForArticle( organizationUniqueId: {0}, articleUniqueId: {1}, statuses: {2}, articleTypes: {3}, offset: {4}, limit: {5} )", new Object[]{organizationUniqueId, article.getUniqueId(), statuses, articleTypes, offset, limit});
        limit = adjustLimitToAllowedScope(limit);
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT 'PRODUCT', "
                + "p.uniqueId, "
                + "p.productName AS 'name', "
                + "p.productNumber, "
                + "p.status, "
                + "pc.code AS code1, "
                + "'' AS code2, "
                + "pc.articleType AS articleType1, "
                + "'' AS articleType2, "
                + "'true' AS deleteable "
                + "FROM Product p "
                + "LEFT JOIN Category pc ON p.categoryId=pc.uniqueId ");
        queryBuilder.append("WHERE (p.organizationId = :organizationUniqueId) ");
        queryBuilder.append("AND p.uniqueId IN (SELECT productId FROM ArticleFitsToProduct WHERE articleId=:articleUniqueId) ");
        if (query != null && !query.isEmpty()) {
            queryBuilder.append("AND (p.productName LIKE :query OR pc.code LIKE :query OR pc.name LIKE :query) ");
        }
        /*
        if (statuses != null && !statuses.isEmpty()) {
            queryBuilder.append("AND (");
            for (int i = 0; i < statuses.size(); i++) {
                if (i != 0) {
                    queryBuilder.append("OR ");
                }
                queryBuilder.append("p.status = :status").append(i).append(" ");
            }
            queryBuilder.append(") ");
        }
        */
        if (statuses != null && !statuses.isEmpty() && statuses.size() == 1) {
            queryBuilder.append("AND (p.status = :status").append(0).append(" )");
        }

        if (excludeCustomerUnique) {
            queryBuilder.append("AND p.customerUnique = 0 ");
        }
        queryBuilder.append("UNION ");
        queryBuilder.append("SELECT 'ARTICLE', "
                + "a.uniqueId, "
                + "a.articleName AS 'name', "
                + "a.articleNumber, "
                + "a.status, "
                + "ac.code AS code1, "
                + "apc.code AS code2, "
                + "ac.articleType AS articleType1, "
                + "apc.articleType AS articleType2, "
                + "'true' AS deleteable "
                + "FROM Article a "
                + "LEFT JOIN Category ac ON a.categoryId=ac.uniqueId "
                + "LEFT JOIN Product ap ON a.basedOnProductId=ap.uniqueId "
                + "LEFT JOIN Category apc ON ap.categoryId=apc.uniqueId ");
        queryBuilder.append("WHERE (a.organizationId = :organizationUniqueId OR ap.organizationId = :organizationUniqueId) ");
        queryBuilder.append("AND (a.uniqueId IN (SELECT fittedByArticleId FROM ArticleFitsToArticle WHERE fitsToArticleId=:articleUniqueId)) OR (a.uniqueId IN (SELECT fitsToArticleId FROM ArticleFitsToArticle WHERE fittedByArticleId=:articleUniqueId)) ");
        if (query != null && !query.isEmpty()) {
            queryBuilder.append("AND (a.articleName LIKE :query OR apc.code LIKE :query OR apc.name LIKE :query OR ac.code LIKE :query OR ac.name LIKE :query) ");
        }
        /*
        if (statuses != null && !statuses.isEmpty()) {
            queryBuilder.append("AND (");
            for (int i = 0; i < statuses.size(); i++) {
                if (i != 0) {
                    queryBuilder.append("OR ");
                }
                queryBuilder.append("a.status = :status").append(i).append(" ");
            }
            queryBuilder.append(") ");
        }
        */
        if (statuses != null && !statuses.isEmpty() && statuses.size() == 1) {
            queryBuilder.append("AND (a.status = :status").append(0).append(" )");
        }
        if (articleTypes != null && !articleTypes.isEmpty()) {
            queryBuilder.append("AND (");
            for (int i = 0; i < articleTypes.size(); i++) {
                if (i != 0) {
                    queryBuilder.append("OR ");
                }
                queryBuilder.append("(ac.articleType = :type").append(i).append(" OR apc.articleType = :type").append(i).append(") ");
            }
            queryBuilder.append(") ");
        }
        if (excludeCustomerUnique) {
            queryBuilder.append("AND (a.customerUnique = 0 OR (a.customerUniqueOverridden = 0 AND ap.customerUnique = 0)) ");
        }

        // if article is based on product we want all articles connected to that product as well
        if (article.getBasedOnProduct() != null) {
            queryBuilder.append("UNION ");
            queryBuilder.append("SELECT 'ARTICLE', "
                    + "a.uniqueId, "
                    + "a.articleName AS 'name', "
                    + "a.articleNumber, "
                    + "a.status, "
                    + "ac.code AS code1, "
                    + "apc.code AS code2, "
                    + "ac.articleType AS articleType1, "
                    + "apc.articleType AS articleType2, "
                    + "'false' AS deleteable "
                    + "FROM Article a "
                    + "LEFT JOIN Category ac ON a.categoryId=ac.uniqueId "
                    + "LEFT JOIN Product ap ON a.basedOnProductId=ap.uniqueId "
                    + "LEFT JOIN Category apc ON ap.categoryId=apc.uniqueId ");
            queryBuilder.append("WHERE (a.organizationId = :organizationUniqueId OR ap.organizationId = :organizationUniqueId) ");
            queryBuilder.append("AND a.uniqueId IN (SELECT articleId FROM ArticleFitsToProduct WHERE productId=:basedOnProductId) ");
            if (query != null && !query.isEmpty()) {
                queryBuilder.append("AND (a.articleName LIKE :query OR ac.code LIKE :query OR ac.name LIKE :query) ");
            }
            /*
            if (statuses != null && !statuses.isEmpty()) {
                queryBuilder.append("AND (");
                for (int i = 0; i < statuses.size(); i++) {
                    if (i != 0) {
                        queryBuilder.append("OR ");
                    }
                    queryBuilder.append("a.status = :status").append(i).append(" ");
                }
                queryBuilder.append(") ");
            }
            */
            if (statuses != null && !statuses.isEmpty() && statuses.size() == 1) {
                queryBuilder.append("AND (a.status = :status").append(0).append(" )");
            }
            if (articleTypes != null && !articleTypes.isEmpty()) {
                queryBuilder.append("AND (");
                for (int i = 0; i < articleTypes.size(); i++) {
                    if (i != 0) {
                        queryBuilder.append("OR ");
                    }
                    queryBuilder.append("(ac.articleType = :type").append(i).append(" OR apc.articleType = :type").append(i).append(") ");
                }
                queryBuilder.append(") ");
            }
        }
        if (excludeCustomerUnique) {
            queryBuilder.append("AND (a.customerUnique = 0 OR (a.customerUniqueOverridden = 0 AND ap.customerUnique = 0)) ");
        }
        queryBuilder.append("ORDER BY name ASC");
        LOG.log(Level.FINEST, "queryBuilder: {0}", new Object[]{queryBuilder.toString()});
        Query searchQuery = em.createNativeQuery(queryBuilder.toString());
        searchQuery.setParameter("organizationUniqueId", organizationUniqueId);
        searchQuery.setParameter("articleUniqueId", article.getUniqueId());
        if (query != null && !query.isEmpty()) {
            searchQuery.setParameter("query", "%" + query + "%");
        }
        if (statuses != null && !statuses.isEmpty() && statuses.size() == 1) {
            //for (int i = 0; i < statuses.size(); i++) {
                searchQuery.setParameter("status" + 0, statuses.get(0).toString());
            //}
        }
        if (articleTypes != null && !articleTypes.isEmpty()) {
            for (int i = 0; i < articleTypes.size(); i++) {
                searchQuery.setParameter("type" + i, articleTypes.get(i).toString());
            }
        }
        if (article.getBasedOnProduct() != null) {
            searchQuery.setParameter("basedOnProductId", article.getBasedOnProduct().getUniqueId());
        }
        searchQuery.setMaxResults(limit);
        searchQuery.setFirstResult(offset);
        List<Object[]> objectArrayList = searchQuery.getResultList();
        return SearchMapper.map(objectArrayList);
    }

    public long countArticlesByOrganization(long organizationUniqueId, String query, List<Product.Status> statuses, List<Article.Type> articleTypes) {
        LOG.log(Level.FINEST, "countArticlesByOrganization( organizationUniqueId: {0}, statuses: {1}, articleTypes: {2} )", new Object[]{organizationUniqueId, statuses, articleTypes});
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT 'ARTICLE', COUNT(a.uniqueId) FROM Article a "
                + "LEFT JOIN Product ap ON a.basedOnProductId=ap.uniqueId "
                + "LEFT JOIN Category ac ON a.categoryId=ac.uniqueId "
                + "LEFT JOIN Category apc ON ap.categoryId=apc.uniqueId ");
        queryBuilder.append("WHERE (a.organizationId = :organizationUniqueId OR ap.organizationId = :organizationUniqueId) ");
        if (query != null && !query.isEmpty()) {
            queryBuilder.append("AND (a.articleName LIKE :query OR a.articleNumber LIKE :query OR apc.code LIKE :query OR apc.name LIKE :query OR ac.code LIKE :query OR ac.name LIKE :query) ");
        }
        /*
        if (statuses != null && !statuses.isEmpty()) {
            queryBuilder.append("AND (");
            for (int i = 0; i < statuses.size(); i++) {
                if (i != 0) {
                    queryBuilder.append("OR ");
                }
                queryBuilder.append("a.status = :status").append(i).append(" ");
            }
            queryBuilder.append(") ");
        }
        */
        if (statuses != null && !statuses.isEmpty() && statuses.size() == 1) {
            queryBuilder.append("AND (a.status = :status").append(0).append(" )");
        }
        if (articleTypes != null && !articleTypes.isEmpty()) {
            queryBuilder.append("AND (");
            for (int i = 0; i < articleTypes.size(); i++) {
                if (i != 0) {
                    queryBuilder.append("OR ");
                }
                queryBuilder.append("(ac.articleType = :type").append(i).append(" OR apc.articleType = :type").append(i).append(" ) ");
            }
            queryBuilder.append(") ");
        }
        LOG.log(Level.FINEST, "queryBuilder: {0}", new Object[]{queryBuilder.toString()});
        Query searchQuery = em.createNativeQuery(queryBuilder.toString());
        searchQuery.setParameter("organizationUniqueId", organizationUniqueId);
        if (query != null && !query.isEmpty()) {
            searchQuery.setParameter("query", "%" + query + "%");
        }
        if (statuses != null && !statuses.isEmpty() && statuses.size() == 1) {
                searchQuery.setParameter("status" + 0, statuses.get(0).toString());
        }
        if (articleTypes != null && !articleTypes.isEmpty()) {
            for (int i = 0; i < articleTypes.size(); i++) {
                searchQuery.setParameter("type" + i, articleTypes.get(i).toString());
            }
        }
        List<Object[]> objectArrayList = searchQuery.getResultList();
        long total = 0;
        for (Object[] objectArray : objectArrayList) {
            long count = ((BigInteger) objectArray[1]).longValue();
            total += count;
        }
        return total;
    }

    public List<SearchProductsAndArticlesAPI> searchArticlesByOrganization(long organizationUniqueId, String query, List<Product.Status> statuses, List<Article.Type> articleTypes, int offset, int limit) {

        LOG.log(Level.FINEST, "searchArticlesByOrganization( organizationUniqueId: {0}, statuses: {1}, articleTypes: {2}, offset: {3}, limit: {4} )", new Object[]{organizationUniqueId, statuses, articleTypes, offset, limit});
        limit = adjustLimitToAllowedScope(limit);
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT 'ARTICLE', "
                + "a.uniqueId, "
                + "a.articleName AS 'name', "
                + "a.articleNumber, "
                + "a.status, "
                + "ac.code AS code1, "
                + "apc.code AS code2, "
                + "ac.articleType AS articleType1, "
                + "apc.articleType AS articleType2 from Article a "
                + "LEFT JOIN Category ac ON a.categoryId=ac.uniqueId "
                + "LEFT JOIN Product ap ON a.basedOnProductId=ap.uniqueId "
                + "LEFT JOIN Category apc ON ap.categoryId=apc.uniqueId ");
        queryBuilder.append("WHERE (a.organizationId = :organizationUniqueId OR ap.organizationId = :organizationUniqueId) ");
        if( query != null && !query.isEmpty() ) {            
             queryBuilder.append("AND (a.articleName LIKE :query OR a.articleNumber LIKE :query OR apc.code LIKE :query OR apc.name LIKE :query OR ac.code LIKE :query OR ac.name LIKE :query) ");
        }
        /*
        if (statuses != null && !statuses.isEmpty()) {
            queryBuilder.append("AND (");
            for (int i = 0; i < statuses.size(); i++) {
                if (i != 0) {
                    queryBuilder.append("OR ");
                }
                queryBuilder.append("a.status = :status").append(i).append(" ");
            }
            queryBuilder.append(") ");
        }
        */
        if (statuses != null && !statuses.isEmpty() && statuses.size() == 1) {
            queryBuilder.append("AND (a.status = :status").append(0).append(" )");
        }
        if (articleTypes != null && !articleTypes.isEmpty()) {
            queryBuilder.append("AND (");
            for (int i = 0; i < articleTypes.size(); i++) {
                if (i != 0) {
                    queryBuilder.append("OR ");
                }
                queryBuilder.append("(ac.articleType = :type").append(i).append(" OR apc.articleType = :type").append(i).append(") ");
            }
            queryBuilder.append(") ");
        }
        queryBuilder.append("ORDER BY name ASC");
        LOG.log(Level.FINEST, "queryBuilder: {0}", new Object[]{queryBuilder.toString()});
        Query searchQuery = em.createNativeQuery(queryBuilder.toString());
        searchQuery.setParameter("organizationUniqueId", organizationUniqueId);
        if (query != null && !query.isEmpty()) {
            searchQuery.setParameter("query", "%" + query + "%");
        }
        if (statuses != null && !statuses.isEmpty() && statuses.size() == 1) {
            //for (int i = 0; i < statuses.size(); i++) {
                searchQuery.setParameter("status" + 0, statuses.get(0).toString());
            //}
        }
        if (articleTypes != null && !articleTypes.isEmpty()) {
            for (int i = 0; i < articleTypes.size(); i++) {
                searchQuery.setParameter("type" + i, articleTypes.get(i).toString());
            }
        }
        searchQuery.setMaxResults(limit);
        searchQuery.setFirstResult(offset);
        List<Object[]> objectArrayList = searchQuery.getResultList();
        return SearchMapper.map(objectArrayList);
    }

    /**
     * Search organization products and articles. This includes customer unique
     * items.
     *
     * @param organizationUniqueId
     * @param query
     * @param statuses
     * @param articleTypes
     * @param offset
     * @param limit                variable scope of how many articles should be presented per page in the UI.
     * @param includeProducts
     * @param includeArticles
     * @param searchAPI
     * @return
     * @throws HjalpmedelstjanstenValidationException
     */
    public SearchDTO searchProductsAndArticlesByOrganization(long organizationUniqueId,
                                                             String query,
                                                             List<Product.Status> statuses,
                                                             List<Article.Type> articleTypes,
                                                             String sortType,
                                                             String sortOrder,
                                                             int offset,
                                                             int limit,
                                                             boolean includeProducts,
                                                             boolean includeArticles,
                                                             boolean includeOnlyTiso,
                                                             SearchAPI searchAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "searchProductsAndArticlesByOrganization( organizationUniqueId: {0}, statuses: {1}, articleTypes: {2}, offset: {3}, limit: {4}, includeProducts: {5}, includeArticles: {6} )", new Object[]{organizationUniqueId, statuses, articleTypes, offset, limit, includeProducts, includeArticles});
        Product.Status status = null;

        limit = adjustLimitToAllowedScope(limit);

        if (statuses != null && statuses.size() == 1) {
            status = statuses.get(0);
        }

        SearchDTO result =  elasticSearchController.searchOrganization(query,
                organizationUniqueId,
                includeProducts,
                includeArticles,
                sortType,
                sortOrder,
                articleTypes,
                null,
                status,
                limit,
                offset,
                includeOnlyTiso);
        return result;

    }

    /**
     * The big search, search all (non customer unique) products and articles.
     *
     * @param queryParameters
     * @param limit           variable scope of how many articles should be presented per page in the UI.
     * @return
     * @throws HjalpmedelstjanstenValidationException
     */
    public SearchDTO searchProductsAndArticles(MultivaluedMap<String, String> queryParameters, int limit, String sortType,
                                               String sortOrder) throws HjalpmedelstjanstenValidationException {
        limit = adjustLimitToAllowedScope(limit);
        int offset = queryParameters.getFirst("offset") == null ? 0 : Integer.parseInt(queryParameters.getFirst("offset"));
        String query = queryParameters.getFirst("query");
        Category category = null;
        List<CategorySpecificProperty> categorySpecificPropertys = null;
        Long categoryId = queryParameters.getFirst("category") == null ? null : Long.parseLong(queryParameters.getFirst("category"));
        if (categoryId != null) {
            category = categoryController.getById(categoryId);
            if (category == null) {
                LOG.log(Level.WARNING, "Attempt to search for products and articles on category with id: {0} which does not exist", new Object[]{categoryId});
                throw validationMessageService.generateValidationException("category", "search.productsandarticles.categoryDoesNotExist");
            }
            categorySpecificPropertys = categoryController.getCategoryPropertys(categoryId);
        }

        // include products ?
        boolean includeProducts = false;
        String includeProductsString = queryParameters.getFirst("includeProducts");
        if (includeProductsString != null && !includeProductsString.isEmpty()) {
            includeProducts = Boolean.parseBoolean(includeProductsString);
        }

        // include articles ?
        boolean includeArticles = false;
        String includeArticlesString = queryParameters.getFirst("includeArticles");
        if (includeArticlesString != null && !includeArticlesString.isEmpty()) {
            includeArticles = Boolean.parseBoolean(includeArticlesString);
        }

        // filter on supplier ?
        int supplier = 0;
        String supplierString = queryParameters.getFirst("supplier");
        if (supplierString != null && !supplierString.isEmpty()) {
            supplier = Integer.parseInt(supplierString);
        }

        // article types
        List<Article.Type> articleTypes = null;
        List<String> articleTypesStringList = queryParameters.get("type");
        if (articleTypesStringList != null && !articleTypesStringList.isEmpty()) {
            articleTypes = new ArrayList<>();
            for (String articleTypeString : articleTypesStringList) {
                articleTypes.add(Article.Type.valueOf(articleTypeString));
            }
        }

        List<Product.Status> statuses = null;
        List<String> statusesStringList = queryParameters.get("status");
        if (statusesStringList != null && !statusesStringList.isEmpty()) {
            statuses = new ArrayList<>();
            for (String statusString : statusesStringList) {
                statuses.add(Product.Status.valueOf(statusString));
            }
        }
        Product.Status status = null;
        if (statuses != null && statuses.size() == 1) {
            status = statuses.get(0);
        }

        return elasticSearchController.search(query,
                category,
                status,
                categorySpecificPropertys,
                queryParameters,
                sortType,
                sortOrder,
                includeProducts,
                includeArticles,
                articleTypes,
                supplier,
                limit,
                offset);
    }

    private int adjustLimitToAllowedScope(int limit) {

        int maxAllowed = 55000;
        int minAllowed = 25;

        if (limit > maxAllowed) {
            LOG.log(Level.FINEST, "Limit was set too high: " + limit + " adjusting to " + maxAllowed);
            return maxAllowed;

        } else if (limit < minAllowed) {
            LOG.log(Level.FINEST, "Limit was set too low: " + limit + " adjusting to " + minAllowed);
            return minAllowed;

        } else {

            LOG.log(Level.FINEST, "Limit was set to: " + limit);
            return limit;
        }
    }

}
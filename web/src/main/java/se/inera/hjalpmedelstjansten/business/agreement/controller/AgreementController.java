/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.agreement.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import se.inera.hjalpmedelstjansten.business.BaseController;
import se.inera.hjalpmedelstjansten.business.DateUtils;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenException;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.organization.controller.BusinessLevelController;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.product.controller.CVPreventiveMaintenanceController;
import se.inera.hjalpmedelstjansten.business.product.controller.GuaranteeUnitController;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.user.controller.EmailController;
import se.inera.hjalpmedelstjansten.business.user.controller.RoleController;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.business.user.controller.UserMapper;
import se.inera.hjalpmedelstjansten.model.api.*;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.Agreement;
import se.inera.hjalpmedelstjansten.model.entity.BusinessLevel;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;
import se.inera.hjalpmedelstjansten.model.entity.InternalAudit;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.UserEngagement;
import se.inera.hjalpmedelstjansten.model.entity.UserRole;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;

/**
 * Class for handling business logic of Agreements. This includes talking to the
 * database.
 * 
 * @author Tommy Berglund
 */
@Stateless
public class AgreementController extends BaseController {
    
    @Inject
    HjmtLogger LOG;
    
    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;

    @Inject
    private EmailController emailController;

    @Inject
    private String notifyNewPricelistMailSubject;

    @Inject
    private String notifyNewPricelistMailBody;

    @Inject
    AgreementValidation agreementValidation;
    
    @Inject
    AgreementPricelistRowController agreementPricelistRowController;
    
    @Inject
    ValidationMessageService validationMessageService;
    
    @Inject
    OrganizationController organizationController;
        
    @Inject
    BusinessLevelController businessLevelController;
    
    @Inject
    UserController userController;
    
    @Inject
    GuaranteeUnitController guaranteeUnitController;

    @Inject
    AgreementPricelistController pricelistController;
    
    @Inject
    CVPreventiveMaintenanceController cVPreventiveMaintenanceController;
    
    @Inject
    Event<InternalAuditEvent> internalAuditEvent;

    @Inject
    String defaultPricelistNumber;

    public SearchDTO searchPricelistApprovers(long organizationUniqueId, String userInput, int offset, int limit, List<Long> excludeUserEngagementIds, List<Long> businessLevelIds) {
        LOG.log(Level.FINEST, "searchPricelistApprovers( organizationUniqueId: {0}, offset: {1}, limit: {2} )", new Object[] {organizationUniqueId, offset, limit});
        
        // count
        StringBuilder countQueryBuilder = new StringBuilder();
        countQueryBuilder.append("SELECT COUNT(DISTINCT ue.uniqueId) FROM UserEngagement ue ");
        populateBasePartOfQuery(countQueryBuilder, userInput, excludeUserEngagementIds, businessLevelIds);
        String countQueryString = countQueryBuilder.toString();
        Query countQuery = em.createQuery(countQueryString);
        populateQuery(countQuery, organizationUniqueId, userInput, excludeUserEngagementIds, businessLevelIds);
        long total = (long) countQuery.getSingleResult();
        
        // list
        StringBuilder listQueryBuilder = new StringBuilder();
        listQueryBuilder.append("SELECT DISTINCT(ue.uniqueId), ue.validFrom, ue.validTo, ua.firstName, ua.lastName, ua.title, ua.username, ea.uniqueId, ea.email, ea.mobile, ea.telephone FROM UserEngagement ue ");
        populateBasePartOfQuery(listQueryBuilder, userInput, excludeUserEngagementIds, businessLevelIds);        
        listQueryBuilder.append("ORDER BY ue.userAccount.lastName ASC ");
        String listQueryString = listQueryBuilder.toString();
        LOG.log( Level.FINEST, "listQueryString: {0}", new Object[] {listQueryString} );
        Query listQuery = em.createQuery(listQueryString).                
                setFirstResult(offset).
                setMaxResults(limit);
        populateQuery(listQuery, organizationUniqueId, userInput, excludeUserEngagementIds, businessLevelIds);
        List<UserEngagement> userEngagements = UserMapper.mapSearchToUserEngagements(listQuery.getResultList());

        return new SearchDTO(total, UserMapper.map(userEngagements, false));
    }
    
    private void populateBasePartOfQuery(StringBuilder queryBuilder, String userInput, List<Long> excludeUserEngagementIds, List<Long> businessLevelIds) {
        queryBuilder.append("JOIN ue.userAccount ua ");
        queryBuilder.append("JOIN ua.electronicAddress ea ");
        queryBuilder.append("JOIN ue.roles uer ");
        queryBuilder.append("LEFT JOIN ue.businessLevels ubl ");
        queryBuilder.append("WHERE ue.organization.uniqueId = :organizationUniqueId ");
        if( excludeUserEngagementIds != null && !excludeUserEngagementIds.isEmpty() ) {
            queryBuilder.append("AND ue.uniqueId NOT IN :userEngagementIds ");
        }
        if( userInput != null && !userInput.isEmpty() ) {
            queryBuilder.append("AND (ue.userAccount.firstName LIKE :query OR ue.userAccount.lastName LIKE :query OR ue.userAccount.username LIKE :query OR ue.userAccount.electronicAddress.email LIKE :query) ");
        }
        if( businessLevelIds != null && !businessLevelIds.isEmpty() ) {
            queryBuilder.append("AND (ubl.uniqueId IN :businessLevelsIds OR ubl IS NULL) ");
        } else {
            queryBuilder.append("AND ubl IS NULL ");
        }
        queryBuilder.append("AND uer.name IN :roleNames ");
    }
    
    private void populateQuery(Query query, long organizationUniqueId, String userInput, List<Long> excludeUserEngagementIds, List<Long> businessLevelIds) {
        query.setParameter("organizationUniqueId", organizationUniqueId);
        if( userInput != null && !userInput.isEmpty() ) {
            query.setParameter("query", "%" + userInput + "%");
        }
        List<UserRole.RoleName> roleNames = new ArrayList<>();
        roleNames.add(UserRole.RoleName.CustomerAgreementManager);
        roleNames.add(UserRole.RoleName.CustomerAgreementViewer);
        query.setParameter("roleNames", roleNames);
        if( businessLevelIds != null && !businessLevelIds.isEmpty() ) {
            query.setParameter("businessLevelsIds", businessLevelIds);
        }
        if( excludeUserEngagementIds != null && !excludeUserEngagementIds.isEmpty() ) {
            query.setParameter("userEngagementIds", excludeUserEngagementIds);
        }
    }
    
    /**
     * Search agreements for the user supplied query. Search is paginated so only
     * results valid for given offset and limit is returned.
     * 
     * @param queryString user supplied query
     * @param statuses
     * @param organizationUniqueId unique id of the organization
     * @param userAPI user information, needed for business levels validation
     * @param onlyWhereContactPerson
     * @param offset start returning from this search result
     * @param limit maximum number of search results to return
     * @return a list of <code>AgreementAPI</code> matching the query parameters
     */
    public List<AgreementAPI> searchAgreements(String queryString, List<Agreement.Status> statuses, long organizationUniqueId, UserAPI userAPI, boolean onlyWhereContactPerson, int offset, int limit) {
        LOG.log(Level.FINEST, "searchAgreements( organizationUniqueId: {0}, offset: {1}, limit: {2}, onlyWhereContactPerson: {3} )", new Object[] {organizationUniqueId, offset, limit, onlyWhereContactPerson});
        Organization organization = organizationController.getOrganization(organizationUniqueId);
        Query query;
        if( organization.getOrganizationType() == Organization.OrganizationType.SERVICE_OWNER ) {
            // a user on service owner organization can see all agreements
            StringBuilder queryBuilder = new StringBuilder();
            queryBuilder.append("SELECT a FROM Agreement a ");
            boolean whereSet = false;
            if( queryString != null && !queryString.isEmpty() ) {
                queryBuilder.append("WHERE (a.agreementName LIKE :query OR a.agreementNumber LIKE :query OR a.customer.organizationName LIKE :query OR a.supplier.organizationName LIKE :query) ");
                whereSet = true;
            }
            if( statuses != null && !statuses.isEmpty() ) {
                queryBuilder.append(whereSet ? "AND ": "WHERE ");
                queryBuilder.append("a.status IN :statuses ");            
            }
            String querySql = queryBuilder.toString();
            LOG.log( Level.FINEST, "querySql: {0}", new Object[] {querySql});
            query = em.createQuery(querySql);
        } else {
            List<Long> businessLevelIds = getUserBusinessLevelIds(userAPI, organizationUniqueId);
            StringBuilder queryBuilder = new StringBuilder();
            queryBuilder.append("SELECT DISTINCT(a) FROM Agreement a ");
            if( onlyWhereContactPerson ) {
                queryBuilder.append("JOIN a.customerPricelistApprovers cpa ");
            } else {
                queryBuilder.append("LEFT JOIN a.sharedWithCustomerBusinessLevels swcbl ");
                if( businessLevelIds == null || businessLevelIds.isEmpty() ) {
                    queryBuilder.append("LEFT JOIN a.sharedWithCustomers swc ");
                }
            }
            if( onlyWhereContactPerson ) {
                queryBuilder.append("WHERE (:userEngagementId IS NOT NULL AND cpa.uniqueId = :userEngagementId) ");            
            } else {
                if( businessLevelIds != null && !businessLevelIds.isEmpty() ) {
                    queryBuilder.append("WHERE (a.customerBusinessLevel.uniqueId IN :businessLevelIds OR swcbl.uniqueId IN :businessLevelIds) ");
                } else {
                    queryBuilder.append("WHERE (a.customer.uniqueId = :organizationUniqueId OR a.supplier.uniqueId = :organizationUniqueId OR swc.uniqueId = :organizationUniqueId OR swcbl.organization.uniqueId = :organizationUniqueId) ");
                }
            }
            if( queryString != null && !queryString.isEmpty() ) {
                queryBuilder.append("AND (a.agreementName LIKE :query OR a.agreementNumber LIKE :query OR a.customer.organizationName LIKE :query OR a.supplier.organizationName LIKE :query) ");
            }
            if( statuses != null && !statuses.isEmpty() ) {
                queryBuilder.append("AND a.status IN :statuses ");            
            }
            queryBuilder.append("ORDER BY a.agreementNumber");
            String querySql = queryBuilder.toString();
            LOG.log( Level.FINEST, "querySql: {0}", new Object[] {querySql});
            query = em.createQuery(querySql);
            if( onlyWhereContactPerson ) {
                query.setParameter("userEngagementId", userAPI.getId());
            } else {
                if( businessLevelIds != null && !businessLevelIds.isEmpty() ) {
                    query.setParameter("businessLevelIds", businessLevelIds);
                } else {
                    query.setParameter("organizationUniqueId", organizationUniqueId);
                }
            }
        }
        if( queryString != null && !queryString.isEmpty() ) {
            query.setParameter("query", "%" + queryString + "%");
        }
        if( statuses != null && !statuses.isEmpty() ) {
            query.setParameter("statuses", statuses);          
        }
        if( limit > 0 ) {
            query.setMaxResults(limit);
        }
        List<Agreement> agreements = query.
                    setFirstResult(offset).
                    getResultList();
        return AgreementMapper.map(agreements, false);
    }

    /**
     * Counts the total number of agreements that matches a user supplied search query
     * 
     * @param queryString user supplied query
     * @param statuses
     * @param organizationUniqueId unique id of the organization
     * @param userAPI user information, needed for business levels validation
     * @param onlyWhereContactPerson
     * @return total number of agreements matching the query parameters
     */
    public long countSearchAgreements(String queryString, List<Agreement.Status> statuses, long organizationUniqueId, UserAPI userAPI, boolean onlyWhereContactPerson) {
        LOG.log(Level.FINEST, "countSearchAgreements( organizationUniqueId: {0}, onlyWhereContactPerson: {1} )", new Object[] {organizationUniqueId, onlyWhereContactPerson});
        Organization organization = organizationController.getOrganization(organizationUniqueId);
        Query query;
        if( organization.getOrganizationType() == Organization.OrganizationType.SERVICE_OWNER ) {
            // a user on service owner organization can see all agreements
            StringBuilder queryBuilder = new StringBuilder();
            queryBuilder.append("SELECT COUNT(a) FROM Agreement a ");
            boolean whereSet = false;
            if( queryString != null && !queryString.isEmpty() ) {
                queryBuilder.append("WHERE (a.agreementName LIKE :query OR a.agreementNumber LIKE :query OR a.customer.organizationName LIKE :query OR a.supplier.organizationName LIKE :query) ");
                whereSet = true;
            }
            if( statuses != null && !statuses.isEmpty() ) {
                queryBuilder.append(whereSet ? "AND ": "WHERE ");
                queryBuilder.append("a.status IN :statuses ");            
            }
            String querySql = queryBuilder.toString();
            LOG.log( Level.FINEST, "querySql: {0}", new Object[] {querySql});
            query = em.createQuery(querySql);
        } else {
            List<Long> businessLevelIds = getUserBusinessLevelIds(userAPI, organizationUniqueId);
            StringBuilder queryBuilder = new StringBuilder();
            queryBuilder.append("SELECT COUNT(DISTINCT a) FROM Agreement a ");
            if( onlyWhereContactPerson ) {
                queryBuilder.append("JOIN a.customerPricelistApprovers cpa ");
            } else {
                queryBuilder.append("LEFT JOIN a.sharedWithCustomerBusinessLevels swcbl ");
                if( businessLevelIds == null || businessLevelIds.isEmpty() ) {
                    queryBuilder.append("LEFT JOIN a.sharedWithCustomers swc ");
                }
            }
            if( onlyWhereContactPerson ) {
                queryBuilder.append("WHERE (:userEngagementId IS NOT NULL AND cpa.uniqueId = :userEngagementId) ");            
            } else {
                if( businessLevelIds != null && !businessLevelIds.isEmpty() ) {
                    queryBuilder.append("WHERE (a.customerBusinessLevel.uniqueId IN :businessLevelIds OR swcbl.uniqueId IN :businessLevelIds) ");
                } else {
                    queryBuilder.append("WHERE (a.customer.uniqueId = :organizationUniqueId OR a.supplier.uniqueId = :organizationUniqueId OR swc.uniqueId = :organizationUniqueId OR swcbl.organization.uniqueId = :organizationUniqueId) ");
                }
            }
            if( queryString != null && !queryString.isEmpty() ) {
                queryBuilder.append("AND (a.agreementName LIKE :query OR a.agreementNumber LIKE :query OR a.customer.organizationName LIKE :query OR a.supplier.organizationName LIKE :query) ");
            }
            if( statuses != null && !statuses.isEmpty() ) {
                queryBuilder.append("AND a.status IN :statuses ");            
            }
            String querySql = queryBuilder.toString();
            LOG.log( Level.FINEST, "querySql: {0}", new Object[] {querySql});
            query = em.createQuery(querySql);
            if( onlyWhereContactPerson ) {
                query.setParameter("userEngagementId", userAPI.getId());
            } else {
                if( businessLevelIds != null && !businessLevelIds.isEmpty() ) {
                    query.setParameter("businessLevelIds", businessLevelIds);
                } else {
                    query.setParameter("organizationUniqueId", organizationUniqueId);
                }
            }
        }
        if( queryString != null && !queryString.isEmpty() ) {
            query.setParameter("query", "%" + queryString + "%");
        }
        if( statuses != null && !statuses.isEmpty() ) {
            query.setParameter("statuses", statuses);          
        }
        return (Long) query.getSingleResult();
    }
    
    /**
     * Get the <code>AgreementAPI</code> for the given unique id on the 
     * specified organization
     * 
     * @param organizationUniqueId unique id of the organization
     * @param uniqueId unique id of the agreement to get
     * @param userAPI user information, needed for business levels validation
     * @param sessionId
     * @return the corresponding <code>AgreementAPI</code>
     */
    public AgreementAPI getAgreementAPI(long organizationUniqueId, long uniqueId, UserAPI userAPI, String sessionId, String requestIp) {
        LOG.log(Level.FINEST, "getAgreementAPI( organizationUniqueId: {0}, uniqueId: {1} )", new Object[] {organizationUniqueId, uniqueId});
        Agreement agreement = getAgreement(organizationUniqueId, uniqueId, userAPI);
        if( agreement != null ) {
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT, InternalAudit.ActionType.VIEW, userAPI.getId(), sessionId, uniqueId, requestIp));
            long numberOfRows = agreementPricelistRowController.getNumberOfRowsOnAgreement(agreement.getUniqueId());
            return AgreementMapper.map( agreement, numberOfRows, true );
        }
        return null;
    }
    
    /**
     * Get the <code>Agreement</code> with the given id on the specified organization. 
     * The agreement can be read by the defined customer and the supplier and by
     * any organization (customer) that the agreement is shared with.
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the Agreement to get
     * @param userAPI user information, needed for business levels validation
     * @return the corresponding <code>Agreement</code>
     */
    public Agreement getAgreement(long organizationUniqueId, long agreementUniqueId, UserAPI userAPI) {
        LOG.log(Level.FINEST, "getAgreement( organizationUniqueId: {0}, agreementUniqueId: {1} )", new Object[] {organizationUniqueId, agreementUniqueId});
        Agreement agreement = em.find(Agreement.class, agreementUniqueId);
        if( agreement == null ) {
            return null;
        }
        if( !organizationAllowedToReadAgreement(agreement, organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user {0} to access agreement: {1} not on given organization: {2} or in user business levels. Returning null.", new Object[] {userAPI.getId(), agreementUniqueId, organizationUniqueId});
            return null;
        }
        // don't need to check business levels for suppliers
        Organization organization = organizationController.getOrganization(organizationUniqueId);
        if( organization.getOrganizationType() == Organization.OrganizationType.CUSTOMER ) {
            List<Long> userBusinessLevelIds = getUserBusinessLevelIds(userAPI, organizationUniqueId);
            if( userBusinessLevelIds != null && !userBusinessLevelIds.isEmpty() ) {
                if( agreement.getCustomer().getUniqueId().equals(organizationUniqueId) ) {
                    // user is with the "main" customer
                    // if agreement has no business level, only users without business level can access
                    // if agreement has business level, users without business level or users with that specific level can access
                    if( agreement.getCustomerBusinessLevel() != null ) {
                        if( !userBusinessLevelIds.contains(agreement.getCustomerBusinessLevel().getUniqueId()) ) {
                            LOG.log( Level.WARNING, "Attempt by user to access agreement: {0} on organization {1} with business levels but not in users list of business levels. Returning null.", new Object[] {agreementUniqueId, organizationUniqueId});
                            return null;
                        }
                    } else {
                        LOG.log( Level.WARNING, "Attempt by user to access agreement: {0} without business level on organization {1}, but user has business levels. Returning null.", new Object[] {agreementUniqueId, organizationUniqueId});
                        return null;
                    }
                } else {
                    // user is with a shared with organization, user has business level
                    // the agreement shared with business levels must match a user
                    // business level
                    if( !userHasBusinessLevel(userAPI, agreement.getSharedWithCustomerBusinessLevels()) ) {
                        LOG.log( Level.WARNING, "Attempt by user to access agreement: {0} shared with organization {1} with business levels but not in users list of business levels. Returning null.", new Object[] {agreementUniqueId, organizationUniqueId});
                        return null;
                    }
                }
            }
        }
        return agreement;
    }
        
    boolean userHasBusinessLevel( UserAPI userAPI, List<BusinessLevel> businessLevels ) {
        if( userAPI.getUserEngagements() != null ) {
            for( UserEngagementAPI userEngagementAPI : userAPI.getUserEngagements() ) {
                if( userEngagementAPI.getBusinessLevels() != null ) {
                    for( BusinessLevelAPI businessLevelAPI : userEngagementAPI.getBusinessLevels() ) {
                        for( BusinessLevel businessLevel : businessLevels ) {
                            if( businessLevelAPI.getId().equals(businessLevel.getUniqueId()) ) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
    
    /**
     * Create an agreement on the <code>Organization</code> with the given id. Only
     * Customers can create an agreement.
     * 
     * @param organizationUniqueId unique id of the Organization
     * @param agreementAPI user supplied values
     * @param userAPI user information, needed for business levels validation
     * @param sessionId
     * @param requestIp
     * @return the mapped <code>ArticleAPI</code>
     * @throws HjalpmedelstjanstenValidationException if validation fails
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenException
     */
    public AgreementAPI createAgreement(long organizationUniqueId, AgreementAPI agreementAPI, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "createAgreement( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        agreementValidation.validateForCreate(agreementAPI, organizationUniqueId);
        Agreement agreement = AgreementMapper.map(agreementAPI);

        // add customer organization
        setCustomer(agreement, organizationUniqueId);

        // add supplier organization
        setSupplier(agreement, agreementAPI);
        
        // set business level on customer
        setCustomerBusinessLevel(agreementAPI, agreement, userAPI, agreement.getCustomer());
        
        // set pricelist approvers
        setAgreementPricelistApprovers(organizationUniqueId, agreementAPI, agreement, userAPI);
        
        // set shared with customers
        setSharedWithCustomers(agreement, agreementAPI, userAPI);
        
        // set warranty quantity units
        setWarrantyQuantityUnits(agreement, agreementAPI);
        
        // set warranty valid from
        setWarrantyValidFroms(agreementAPI, agreement, userAPI);
        
        // save it
        em.persist(agreement);
        
        // a new agreement should have one first pricelist
        pricelistController.createFirstPricelistForAgreement(organizationUniqueId, agreement, userAPI, sessionId, requestIp);
        

        // HJAL-95 Mattias Silva
        List<UserRole.RoleName> roleNames = new ArrayList<>();
        roleNames.add(UserRole.RoleName.SupplierAgreementManager);
        List<UserAPI> supplierAgreementManager = userController.getUsersInRolesOnOrganization(agreementAPI.getSupplierOrganization().getId(), roleNames);
        Organization customerOrganization = organizationController.getOrganization(organizationUniqueId);

        // First
        sendNotificationPricelistAddedMail(supplierAgreementManager,customerOrganization.getOrganizationName(), agreementAPI.getAgreementNumber(), defaultPricelistNumber);

        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT, InternalAudit.ActionType.CREATE, userAPI.getId(), sessionId, agreement.getUniqueId(), requestIp));
        return AgreementMapper.map(agreement, 0l, true);
    }
    
    /**
     * Update the <code>Agreement</code> with the given id with the supplied values.
     * Suppliers can only update contact persons on an agreement, Customers can 
     * update everything else.
     * 
     * @param organizationUniqueId unique id of the organization
     * @param uniqueId the unique id of the <code>Organization</code>
     * @param agreementAPI the user supplied values
     * @param userAPI user information, needed for business levels validation
     * @param sessionId
     * @param requestIp
     * @return the updated <code>AgreementAPI</code>
     * @throws HjalpmedelstjanstenValidationException in case validation fails
     */
    public AgreementAPI updateAgreement(long organizationUniqueId, 
            long uniqueId, 
            AgreementAPI agreementAPI, 
            UserAPI userAPI, 
            String sessionId, 
            String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateAgreement( organizationUniqueId: {0}, uniqueId: {1} )", new Object[] {organizationUniqueId, uniqueId});
        Agreement agreement = getAgreement(organizationUniqueId, uniqueId, userAPI);
        if( agreement == null ) {
            return null;
        }

        // organizations/business levels (customers) that the agreement is shared 
        // with can read the agreement, but only the customer can edit it
        if( !agreement.getCustomer().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to edit agreement: {1}, but user organization: {2} is not customer", new Object[] {userAPI.getId(), agreement.getUniqueId(), organizationUniqueId});
            return null;
        }
        
        // validate
        agreementValidation.validateForUpdate(agreementAPI, agreement, organizationUniqueId);

        // customer can update everything except contact persons
        updateAgreementBasics(agreement, agreementAPI);

        // set pricelist approvers
        setAgreementPricelistApprovers(organizationUniqueId, agreementAPI, agreement, userAPI);

        // set shared with customers
        setSharedWithCustomers(agreement, agreementAPI, userAPI);

        AgreementMapper.mapDeliveryAndWarrantyFields(agreementAPI, agreement);
        setWarrantyValidFroms(agreementAPI, agreement, userAPI);

        // set warranty quantity units
        setWarrantyQuantityUnits(agreement, agreementAPI);

        // log
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, uniqueId, requestIp));

        long numberOfRows = agreementPricelistRowController.getNumberOfRowsOnAgreement(agreement.getUniqueId());
        return AgreementMapper.map( agreement, numberOfRows, true );
    }
    
    /**
     * Find business levels from user session information and specific organization
     * 
     * @param userAPI user session information
     * @param organizationUniqueId unique id of the organization
     * @return a list of business levels for the given organization for the user
     * @throws HjalpmedelstjanstenException 
     */
    private List<BusinessLevelAPI> findBusinessLevelsByUserAndOrganization(UserAPI userAPI, long organizationUniqueId) throws HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "findBusinessLevelsByUserAndOrganization( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        for( UserEngagementAPI userEngagementAPI : userAPI.getUserEngagements() ) {
            if( userEngagementAPI.getOrganizationId().equals(organizationUniqueId) ) {
                return userEngagementAPI.getBusinessLevels();
            }
        }
        // this should never happen since user should not be able to be logged in
        // on an organization where no engagement exists. if the user for some 
        // reason tries another organization in the REST-API this should be prevented
        // earlier :)
        LOG.log( Level.WARNING, "Attempt by user to access organization: {0} where no user engagement exists.", new Object[] {organizationUniqueId});
        throw generateException("businessLevel", "agreement.businessLevel.notExist", null, validationMessageService);
    }

    /**
     * Sets the "basic" fields of the agreement from the user supplied values.
     * Some fields can only be updated if there are no rows (articles) on any
     * pricelist in the agreement
     * 
     * @param agreement the agreement to update
     * @param agreementAPI the user supplied values
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     */
    public void updateAgreementBasics(Agreement agreement, AgreementAPI agreementAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateAgreementBasics( agreementUniqueId: {0} )", new Object[] {agreement.getUniqueId()});
        long numberOfRows = agreementPricelistRowController.getNumberOfRowsOnAgreement(agreement.getUniqueId());
        if( numberOfRows == 0 ) {
            agreement.setValidFrom(DateUtils.beginningOfDay(agreementAPI.getValidFrom()));            
            agreement.setAgreementNumber(agreementAPI.getAgreementNumber());
            setSupplier(agreement, agreementAPI);
        }
        agreement.setAgreementName(agreementAPI.getAgreementName());
        agreement.setValidTo(DateUtils.endOfDay(agreementAPI.getValidTo()));
        if( agreementAPI.getEndDate() != null ) {
            agreement.setEndDate(DateUtils.beginningOfDay(agreementAPI.getEndDate()));
        }
        if( agreementAPI.getExtensionOptionTo() != null ) {
            agreement.setExtensionOptionTo(DateUtils.beginningOfDay(agreementAPI.getExtensionOptionTo()));
        }
        if( agreementAPI.getSendReminderOn() != null ) {
            agreement.setSendReminderOn(DateUtils.beginningOfDay(agreementAPI.getSendReminderOn()));
        }

        // set status
        agreement.setStatus(AgreementMapper.getAgreementStatus(agreement));                    
        
    }
    
    /**
     * Set the pricelist approvers for this agreement. Only the customer can do this. 
     * Each person added must have the role CustomerAgreementManager. 
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementAPI user supplied values
     * @param agreement agreement to add pricelist approvers to
     * @param userAPI the currently logged in person
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g. 
     * user does not exist or doesn't exist
     */
    void setAgreementPricelistApprovers(long organizationUniqueId, AgreementAPI agreementAPI, Agreement agreement, UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "setAgreementPricelistApprovers( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        if( agreementAPI.getCustomerPricelistApprovers() != null && !agreementAPI.getCustomerPricelistApprovers().isEmpty() ) {
            List<UserEngagement> customerPricelistApprovers = new ArrayList<>();
            for( UserAPI pricelistApproverUserAPI : agreementAPI.getCustomerPricelistApprovers() ) {
                UserEngagement customerPricelistApproverUserEngagement = userController.getUserEngagement(organizationUniqueId, pricelistApproverUserAPI.getId());
                if( customerPricelistApproverUserEngagement == null ) {
                    LOG.log( Level.WARNING, "Attempt by user: {0} to add agreement pricelist approver: {1} which does not exist or does not belong to organization: {2}", new Object[] {userAPI.getId(), pricelistApproverUserAPI.getId(), organizationUniqueId});
                    throw validationMessageService.generateValidationException("customerPricelistApprovers", "agreement.customerPricelistApprovers.notExist");
                }
                checkAgreementPricelistApproverBusinessLevel(organizationUniqueId, agreement, customerPricelistApproverUserEngagement, userAPI);
                checkAgreementPricelistApproverRoles(customerPricelistApproverUserEngagement, userAPI);
                customerPricelistApprovers.add(customerPricelistApproverUserEngagement);
            }
            agreement.setCustomerPricelistApprovers(customerPricelistApprovers);
        } else {
            agreement.setCustomerPricelistApprovers(null);
        }
    }
    
    /**
     * Checks whether the specified business level exists in the list of business levels
     * 
     * @param userBusinessLevelAPIs list of business levels to check
     * @param businessLevelId id of the business level to check for
     * @return true if the business level with the given id exists in the list
     */
    boolean userBusinessLevelsContains(List<BusinessLevelAPI> userBusinessLevelAPIs, Long businessLevelId) {
        LOG.log(Level.FINEST, "userBusinessLevelsContains( businessLevelId: {0} )", new Object[] {businessLevelId});
        if( userBusinessLevelAPIs != null && !userBusinessLevelAPIs.isEmpty() ) {
            for( BusinessLevelAPI businessLevelAPI : userBusinessLevelAPIs ) {
                if( businessLevelAPI.getId().equals(businessLevelId) ) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Set customer business level on agreement 
     * 
     * @param agreementAPI user supplied values
     * @param agreement the agreement to set business levels on
     * @param userAPI logged in user's session information
     * @param customerOrganization the customer organization
     * @throws HjalpmedelstjanstenValidationException
     * @throws HjalpmedelstjanstenException 
     */
    void setCustomerBusinessLevel(AgreementAPI agreementAPI, Agreement agreement, UserAPI userAPI, Organization customerOrganization) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "setCustomerBusinessLevel( organizationUniqueId: {0} )", new Object[] {customerOrganization.getUniqueId()});
        if( agreementAPI.getCustomerBusinessLevel() != null && agreementAPI.getCustomerBusinessLevel().getId() != null ) {            
            BusinessLevel businessLevel = null;
            if( customerOrganization.getBusinessLevels() != null ) {
                for( BusinessLevel customerBusinessLevel : customerOrganization.getBusinessLevels() ) {
                    if( customerBusinessLevel.getUniqueId().equals(agreementAPI.getCustomerBusinessLevel().getId()) ) {
                        businessLevel = customerBusinessLevel;
                        break;
                    }
                }
            }
            if( businessLevel == null ) {
                throw validationMessageService.generateValidationException("customerBusinessLevel", "agreement.customerBusinessLevel.notExist");
            } else {
                // ok, business level does indeed exist within the customer organization
                // make sure the user also has it
                List<BusinessLevelAPI> userBusinessLevelAPIs = findBusinessLevelsByUserAndOrganization(userAPI, customerOrganization.getUniqueId());
                if( userBusinessLevelAPIs == null || userBusinessLevelsContains(userBusinessLevelAPIs, agreementAPI.getCustomerBusinessLevel().getId()) ) {
                    agreement.setCustomerBusinessLevel(businessLevel);
                } else {
                    LOG.log( Level.WARNING, "Attempt by user: {0} to add agreement to business level: {1} not owned", new Object[] {userAPI.getId(), agreementAPI.getCustomerBusinessLevel().getId()});
                    throw validationMessageService.generateValidationException("customerBusinessLevel", "agreement.customerBusinessLevel.notExist");
                }
            }
        } else {
            List<BusinessLevelAPI> userBusinessLevelAPIs = findBusinessLevelsByUserAndOrganization(userAPI, customerOrganization.getUniqueId());
            if( userBusinessLevelAPIs != null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to add agreement to organization, but user is restricted by businesss levels", new Object[] {userAPI.getId()});
                throw validationMessageService.generateValidationException("customerBusinessLevel", "agreement.customerBusinessLevel.notExist");
            }
        }
    }

    /**
     * Set customer organizatino on agreement
     * 
     * @param agreement the agreement to set organization on
     * @param organizationUniqueId unique id of the customer organization
     * @throws HjalpmedelstjanstenValidationException if the organizatino doesn't exist
     * or is not of type CUSTOMER
     */
    void setCustomer(Agreement agreement, long organizationUniqueId) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "setCustomer( organizationUniqueId: {0} )", new Object[] {organizationUniqueId});
        Organization customerOrganization = organizationController.getOrganization(organizationUniqueId);
        if( customerOrganization == null ) {
            throw validationMessageService.generateValidationException("customerOrganization", "agreement.customerOrganization.notExists");
        }
        if( customerOrganization.getOrganizationType() != Organization.OrganizationType.CUSTOMER ) {
            throw validationMessageService.generateValidationException("customerOrganization", "agreement.customerOrganization.invalidType");
        }
        agreement.setCustomer(customerOrganization);
    }

    /**
     * Set supplier organizatino on agreement
     * 
     * @param agreement the agreement to set organization on
     * @param agreementAPI unique id of the supplier organization
     * @throws HjalpmedelstjanstenValidationException if the organization doesn't exist
     * or is not of type SUPPLIER
     */
    void setSupplier(Agreement agreement, AgreementAPI agreementAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "setSupplier( ... )");
        Organization supplierOrganization = organizationController.getOrganization(agreementAPI.getSupplierOrganization().getId());
        if( supplierOrganization == null ) {
            throw validationMessageService.generateValidationException("supplierOrganization", "agreement.supplierOrganization.notExists");
        }
        if( supplierOrganization.getOrganizationType() != Organization.OrganizationType.SUPPLIER ) {
            throw validationMessageService.generateValidationException("supplierOrganization", "agreement.supplierOrganization.invalidType");
        }
        agreement.setSupplier(supplierOrganization);
        
    }
        
    /**
     * Set organizations that the agreement is shared with. Can only be of type 
     * CUSTOMER. 
     * 
     * @param agreement the agreemenbt to share
     * @param agreementAPI user supplied values 
     * @param userAPI logged in user's session information
     * @throws HjalpmedelstjanstenValidationException if an organization doesn't exist
     * or is not of type CUSTOMER
     */
    void setSharedWithCustomers(Agreement agreement, AgreementAPI agreementAPI, UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        List<OrganizationAPI> sharedWithCustomersAPIs = agreementAPI.getSharedWithCustomers();
        List<Organization> sharedWithCustomers = null;
        if( sharedWithCustomersAPIs != null && !sharedWithCustomersAPIs.isEmpty() ) {
            sharedWithCustomers = new ArrayList<>();
            for( OrganizationAPI organizationAPI: sharedWithCustomersAPIs ) {
                Organization organization = organizationController.getOrganization(organizationAPI.getId());
                if( organization == null ) {
                    LOG.log( Level.WARNING, "Attempt by user: {0} to share agreement with organization: {1} which does not exist", new Object[] {userAPI.getId(), organizationAPI.getId()});
                    throw validationMessageService.generateValidationException("sharedWithCustomers", "agreement.sharedWithCustomers.notExist");
                }
                // must be customer
                if( organization.getOrganizationType() != Organization.OrganizationType.CUSTOMER ) {
                    LOG.log( Level.WARNING, "Attempt by user: {0} to share agreement with organization: {1} which is not a customer", new Object[] {userAPI.getId(), organization.getUniqueId()});
                    throw validationMessageService.generateValidationException("sharedWithCustomers", "agreement.sharedWithCustomers.notCustomer");
                }
                sharedWithCustomers.add(organization);
            }
        }
        List<BusinessLevelAPI> sharedWithBusinessLevelAPIs = agreementAPI.getSharedWithCustomersBusinessLevels();
        List<BusinessLevel> sharedWithBusinessLevels = null;
        if( sharedWithBusinessLevelAPIs != null && !sharedWithBusinessLevelAPIs.isEmpty() ) {
            sharedWithBusinessLevels = new ArrayList<>();
            for( BusinessLevelAPI businessLevelAPI: sharedWithBusinessLevelAPIs ) {
                BusinessLevel businessLevel = businessLevelController.getBusinessLevel(businessLevelAPI.getId());
                if( businessLevel == null ) {
                    LOG.log( Level.WARNING, "Attempt by user: {0} to share agreement with business level: {1} which does not exist", new Object[] {userAPI.getId(), businessLevelAPI.getId()});
                    throw validationMessageService.generateValidationException("sharedWithCustomerBusinessLevels", "agreement.sharedWithCustomersBusinessLevel.notExist");
                }
                // must be customer
                if( businessLevel.getOrganization().getOrganizationType() != Organization.OrganizationType.CUSTOMER ) {
                    LOG.log( Level.WARNING, "Attempt by user: {0} to share agreement with business level: {1} on organization which is not a customer", new Object[] {userAPI.getId(), businessLevel.getUniqueId()});
                    throw validationMessageService.generateValidationException("sharedWithCustomerBusinessLevels", "agreement.sharedWithCustomersBusinessLevel.notCustomer");
                }
                if( sharedWithCustomers != null && sharedWithCustomers.contains(businessLevel.getOrganization()) ) {
                    LOG.log( Level.WARNING, "Attempt by user: {0} to share agreement with both business levels and organization which is not allowed", new Object[] {userAPI.getId()});
                    throw validationMessageService.generateValidationException("sharedWithCustomerBusinessLevels", "agreement.sharedWithCustomersBusinessLevel.notBothOrganizationAndBusinessLevel", businessLevel.getName(), businessLevel.getOrganization().getOrganizationName());
                }
                sharedWithBusinessLevels.add(businessLevel);
            }
        }
        agreement.setSharedWithCustomerBusinessLevels(sharedWithBusinessLevels);
        agreement.setSharedWithCustomers(sharedWithCustomers);
    }
            
    /**
     * Agreement pricelist approvers must match the business level of the agreement. 
     * Users with no business level can be added to any agreement as pricelist 
     * approver.
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreement the agreement containing the configured business level
     * @param customerPricelistApproverUserEngagement the user engagement of the user
     * to be added as pricelist approver
     * @param userAPI the currently logged in users session info. needed for logging
     * @throws HjalpmedelstjanstenValidationException if agreement business level and
     * pricelist approver does not "match"
     */
    void checkAgreementPricelistApproverBusinessLevel(long organizationUniqueId, Agreement agreement, UserEngagement customerPricelistApproverUserEngagement, UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        List<BusinessLevel> businessLevels = customerPricelistApproverUserEngagement.getBusinessLevels();
        // if pricelist approver has no business levels s/he can handle any agreement
        if( businessLevels != null && !businessLevels.isEmpty() ) {            
            if( agreement.getCustomerBusinessLevel() == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to add agreement pricelist approver: {1} which does exist and belong to organization: {2}, but cannot handle agreement on top business level", new Object[] {userAPI.getId(), customerPricelistApproverUserEngagement.getUniqueId(), organizationUniqueId});
                throw validationMessageService.generateValidationException("customerPricelistApprovers", "agreement.customerPricelistApprovers.wrongBusinessLevel", customerPricelistApproverUserEngagement.getUserAccount().getFullName() );
            } else {
                boolean found = false;
                for( BusinessLevel businessLevel : businessLevels ) {
                    if( businessLevel.getUniqueId().equals(agreement.getCustomerBusinessLevel().getUniqueId()) ) {
                        found = true;
                        break;
                    }
                }
                if( !found ) {
                    LOG.log( Level.WARNING, "Attempt by user: {0} to add agreement pricelist approver: {1} which does exist and belong to organization: {2}, but cannot handle agreement business level: {3}", new Object[] {userAPI.getId(), customerPricelistApproverUserEngagement.getUniqueId(), organizationUniqueId, agreement.getCustomerBusinessLevel().getUniqueId()});
                    throw validationMessageService.generateValidationException("customerPricelistApprovers", "agreement.customerPricelistApprovers.wrongBusinessLevel", customerPricelistApproverUserEngagement.getUserAccount().getFullName());
                }
            }
        }
    }

    /**
     * Checks if the organization with the given id can read the specified agreement. 
     * This means the organization must be the customer, the supplier, one of
     * the organizations (customers) or one of the business levels that the agreement 
     * is shared with. This only checks if the organization can read, not write.
     * 
     * @param agreement the agreement to check
     * @param organizationUniqueId unique id of the organization
     * @return true if the organization with organizationUniqueId is allowed to 
     * read agreement
     */
    boolean organizationAllowedToReadAgreement(Agreement agreement, long organizationUniqueId) {
        Organization organization = organizationController.getOrganization(organizationUniqueId);
        if( organization.getOrganizationType() == Organization.OrganizationType.SERVICE_OWNER ) {
            // users on service owner can always read agreements
            return true;
        }
        if( agreement.getCustomer().getUniqueId().equals(organizationUniqueId) ||
                agreement.getSupplier().getUniqueId().equals(organizationUniqueId)) {
            return true;
        } else {
            if( agreement.getSharedWithCustomers() != null && !agreement.getSharedWithCustomers().isEmpty() ) {
                for( Organization sharedWithCustomerOrganization : agreement.getSharedWithCustomers() ) {
                    if( sharedWithCustomerOrganization.getUniqueId().equals(organizationUniqueId) ) {
                        return true;
                    }
                }
            }
            if( agreement.getSharedWithCustomerBusinessLevels() != null && !agreement.getSharedWithCustomerBusinessLevels().isEmpty() ) {
                for( BusinessLevel businessLevel : agreement.getSharedWithCustomerBusinessLevels() ) {
                    if( businessLevel.getOrganization().getUniqueId().equals(organizationUniqueId) ) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
        
    /**
     * Set units for warrant quantity
     * 
     * @param agreement the agreement to set units on
     * @param agreementAPI the user supplied values
     * @throws HjalpmedelstjanstenValidationException if a unit does not exist
     */
    void setWarrantyQuantityUnits(Agreement agreement, AgreementAPI agreementAPI) throws HjalpmedelstjanstenValidationException {
        // H
        if( agreementAPI.getWarrantyQuantityHUnit() != null ) {
            CVGuaranteeUnit unit = guaranteeUnitController.getGuaranteeUnit(agreementAPI.getWarrantyQuantityHUnit().getId());
            if( unit == null ) {
                LOG.log(Level.FINEST, "cannot set warranty quantity unit: {0} for article type H for agreement since it does not exist", new Object[] {agreementAPI.getWarrantyQuantityHUnit().getId()});
                throw validationMessageService.generateValidationException("warrantyQuantityHUnit", "agreement.warrantyQuantityHUnit.notExist");
            }
            agreement.setWarrantyQuantityHUnit(unit);
        } else {
            agreement.setWarrantyQuantityHUnit(null);
        }
        
        // T
        if( agreementAPI.getWarrantyQuantityTUnit() != null ) {
            CVGuaranteeUnit unit = guaranteeUnitController.getGuaranteeUnit(agreementAPI.getWarrantyQuantityTUnit().getId());
            if( unit == null ) {
                LOG.log(Level.FINEST, "cannot set warranty quantity unit: {0} for article type T for agreement since it does not exist", new Object[] {agreementAPI.getWarrantyQuantityTUnit().getId()});
                throw validationMessageService.generateValidationException("warrantyQuantityTUnit", "agreement.warrantyQuantityTUnit.notExist");
            }
            agreement.setWarrantyQuantityTUnit(unit);
        } else {
            agreement.setWarrantyQuantityTUnit(null);
        }
        
        // I
        if( agreementAPI.getWarrantyQuantityIUnit() != null ) {
            CVGuaranteeUnit unit = guaranteeUnitController.getGuaranteeUnit(agreementAPI.getWarrantyQuantityIUnit().getId());
            if( unit == null ) {
                LOG.log(Level.FINEST, "cannot set warranty quantity unit: {0} for article type I for agreement since it does not exist", new Object[] {agreementAPI.getWarrantyQuantityIUnit().getId()});
                throw validationMessageService.generateValidationException("warrantyQuantityIUnit", "agreement.warrantyQuantityIUnit.notExist");
            }
            agreement.setWarrantyQuantityIUnit(unit);
        } else {
            agreement.setWarrantyQuantityIUnit(null);
        }
        
        // R
        if( agreementAPI.getWarrantyQuantityRUnit() != null ) {
            CVGuaranteeUnit unit = guaranteeUnitController.getGuaranteeUnit(agreementAPI.getWarrantyQuantityRUnit().getId());
            if( unit == null ) {
                LOG.log(Level.FINEST, "cannot set warranty quantity unit: {0} for article type R for agreement since it does not exist", new Object[] {agreementAPI.getWarrantyQuantityRUnit().getId()});
                throw validationMessageService.generateValidationException("warrantyQuantityRUnit", "agreement.warrantyQuantityRUnit.notExist");
            }
            agreement.setWarrantyQuantityRUnit(unit);
        } else {
            agreement.setWarrantyQuantityRUnit(null);
        }
        
        // TJ
        if( agreementAPI.getWarrantyQuantityTJUnit() != null ) {
            CVGuaranteeUnit unit = guaranteeUnitController.getGuaranteeUnit(agreementAPI.getWarrantyQuantityTJUnit().getId());
            if( unit == null ) {
                LOG.log(Level.FINEST, "cannot set warranty quantity unit: {0} for article type TJ for agreement since it does not exist", new Object[] {agreementAPI.getWarrantyQuantityTJUnit().getId()});
                throw validationMessageService.generateValidationException("warrantyQuantityTJUnit", "agreement.warrantyQuantityTJUnit.notExist");
            }
            agreement.setWarrantyQuantityTJUnit(unit);
        } else {
            agreement.setWarrantyQuantityTJUnit(null);
        }
    }

    /**
     * Find agreements to change status on.
     *  - where validTo or endDate is passed change status to DISCONTINUED
     *  - where validFrom is in the past and status is not CURRENT change status to CURRENT
     */
    public void changeAgreementStatusByDate() {
        LOG.log(Level.FINEST, "changeAgreementStatusByDate()");

        // agreements to discontinue
        List<Agreement> agreementsToDiscontinue = em.createNamedQuery(Agreement.FIND_TO_DISCONTINUE).
                setParameter("date", new Date()).
                getResultList();
        if( agreementsToDiscontinue != null && !agreementsToDiscontinue.isEmpty() ) {
            LOG.log( Level.FINEST, "Number of agreements to discontinue: {0}", new Object[] {agreementsToDiscontinue.size()});
            for( Agreement agreementToDiscontinue : agreementsToDiscontinue ) {
                agreementToDiscontinue.setStatus(AgreementMapper.getAgreementStatus(agreementToDiscontinue));
            }
        } else {
            LOG.log( Level.FINEST, "No agreements to discontinue");
        }

        // agreements to "activate"
        List<Agreement> agreementsToActivate = em.createNamedQuery(Agreement.FIND_TO_ACTIVATE).
                setParameter("date", new Date()).
                getResultList();
        if( agreementsToActivate != null && !agreementsToActivate.isEmpty() ) {
            LOG.log( Level.FINEST, "Number of agreements to activate: {0}", new Object[] {agreementsToActivate.size()});
            for( Agreement agreementToActivate : agreementsToActivate ) {
                agreementToActivate.setStatus(AgreementMapper.getAgreementStatus(agreementToActivate));
            }
        } else {
            LOG.log( Level.FINEST, "No agreements to activate");
        }
    }

    public List<Agreement> findByBusinessLevel(long businessLevelUniqueId) {
        LOG.log(Level.FINEST, "findByBusinessLevel( businessLevelUniqueId: {0} )", new Object[] {businessLevelUniqueId});
        return em.createNamedQuery(Agreement.FIND_BY_BUSINESS_LEVEL).
                setParameter("businessLevelUniqueId", businessLevelUniqueId).
                getResultList();
    }
    
    /**
     * Get a list of ids of the business levels of the users engagement
     * 
     * @param userAPI user session information
     * @param organizationUniqueId unique id of the organization of the engagement
     * @return a list of ids or null if none exist
     */
    public List<Long> getUserBusinessLevelIds(UserAPI userAPI, long organizationUniqueId) {
        List<Long> userBusinessLevelIds = null;
        UserEngagementAPI userEngagementAPI = getUserEngagementAPI(userAPI, organizationUniqueId);
        if( userEngagementAPI.getBusinessLevels() != null && !userEngagementAPI.getBusinessLevels().isEmpty() ) {
            userBusinessLevelIds = new ArrayList<>();
            for( BusinessLevelAPI businessLevelAPI : userEngagementAPI.getBusinessLevels() ) {
                userBusinessLevelIds.add(businessLevelAPI.getId());
            }
        }
        return userBusinessLevelIds;
    }

    /**
     * Get the user's engagement for the given organization
     * 
     * @param userAPI user session information
     * @param organizationUniqueId unique id of the organization of the engagement
     * @return the user engagement or null if it doesn't exist (which should not 
     * happen when the user gets this far)
     */
    public UserEngagementAPI getUserEngagementAPI(UserAPI userAPI, long organizationUniqueId) {
        for( UserEngagementAPI userEngagementAPI : userAPI.getUserEngagements() ) {
            if( userEngagementAPI.getOrganizationId().equals(organizationUniqueId) ) {
                return userEngagementAPI;
            }
        }
        return null;
    }
        
    private void setWarrantyValidFroms(AgreementAPI agreementAPI, Agreement agreement, UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        // warranty valid from
        if( agreementAPI.getWarrantyValidFromH() != null ) {
            CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(agreementAPI.getWarrantyValidFromH().getCode());
            if( preventiveMaintenance == null ) {
                LOG.log(Level.WARNING, "attempt by user: {0} to set preventive maintenance with code: {1} which does not exist", new Object[] {userAPI.getId(), agreementAPI.getWarrantyValidFromH()});
                throw validationMessageService.generateValidationException("warrantyValidFrom", "agreementAPI.warrantyValidFromH.notExist");
            }
            agreement.setWarrantyValidFromH(preventiveMaintenance);
        } else {
            agreement.setWarrantyValidFromH(null);
        }
        if( agreementAPI.getWarrantyValidFromT() != null ) {
            CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(agreementAPI.getWarrantyValidFromT().getCode());
            if( preventiveMaintenance == null ) {
                LOG.log(Level.WARNING, "attempt by user: {0} to set preventive maintenance with code: {1} which does not exist", new Object[] {userAPI.getId(), agreementAPI.getWarrantyValidFromT()});
                throw validationMessageService.generateValidationException("warrantyValidFrom", "agreement.warrantyValidFromT.notExist");
            }
            agreement.setWarrantyValidFromT(preventiveMaintenance);
        } else {
            agreement.setWarrantyValidFromT(null);
        }
        if( agreementAPI.getWarrantyValidFromI() != null ) {
            CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(agreementAPI.getWarrantyValidFromI().getCode());
            if( preventiveMaintenance == null ) {
                LOG.log(Level.WARNING, "attempt by user: {0} to set preventive maintenance with code: {1} which does not exist", new Object[] {userAPI.getId(), agreementAPI.getWarrantyValidFromI()});
                throw validationMessageService.generateValidationException("warrantyValidFrom", "agreement.warrantyValidFromI.notExist");
            }
            agreement.setWarrantyValidFromI(preventiveMaintenance);
        }
         else {
            agreement.setWarrantyValidFromI(null);
        }
        if( agreementAPI.getWarrantyValidFromR() != null ) {
            CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(agreementAPI.getWarrantyValidFromR().getCode());
            if( preventiveMaintenance == null ) {
                LOG.log(Level.WARNING, "attempt by user: {0} to set preventive maintenance with code: {1} which does not exist", new Object[] {userAPI.getId(), agreementAPI.getWarrantyValidFromR()});
                throw validationMessageService.generateValidationException("warrantyValidFrom", "agreement.warrantyValidFromR.notExist");
            }
            agreement.setWarrantyValidFromR(preventiveMaintenance);
        } else {
            agreement.setWarrantyValidFromR(null);
        }
        if( agreementAPI.getWarrantyValidFromTJ() != null ) {
            CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(agreementAPI.getWarrantyValidFromTJ().getCode());
            if( preventiveMaintenance == null ) {
                LOG.log(Level.WARNING, "attempt by user: {0} to set preventive maintenance with code: {1} which does not exist", new Object[] {userAPI.getId(), agreementAPI.getWarrantyValidFromTJ()});
                throw validationMessageService.generateValidationException("warrantyValidFrom", "agreement.warrantyValidFromTJ.notExist");
            }
            agreement.setWarrantyValidFromTJ(preventiveMaintenance);
        } else {
            agreement.setWarrantyValidFromTJ(null);
        }
        
    }

    private void checkAgreementPricelistApproverRoles(UserEngagement customerPricelistApproverUserEngagement, UserAPI userAPI) throws HjalpmedelstjanstenValidationException {
        boolean roleFound = false;
        if( customerPricelistApproverUserEngagement.getRoles() != null && !customerPricelistApproverUserEngagement.getRoles().isEmpty() ) {
            for( UserRole userRole : customerPricelistApproverUserEngagement.getRoles() ) {
                if( userRole.getName() == UserRole.RoleName.CustomerAgreementManager || userRole.getName() == UserRole.RoleName.CustomerAgreementViewer ) {
                    roleFound = true;
                    break;
                }
            }
        }
        if( !roleFound ) {
            LOG.log(Level.WARNING, "attempt by user: {0} to set agreement pricelist approver which does not have valid role, this is an odd situation and should be checked", new Object[] {userAPI.getId()});
            throw validationMessageService.generateValidationException("customerPricelistApprovers", "agreement.customerPricelistApprovers.wrongRole", customerPricelistApproverUserEngagement.getUserAccount().getFirstName() + customerPricelistApproverUserEngagement.getUserAccount().getLastName());
        }
    }
    public void sendNotificationPricelistAddedMail(List<UserAPI> supplierContactPersons, String organizationName, String agreementNumber, String pricelistNumbers) {
        if(notifyNewPricelistMailBody == null)
            return;
        //String pricelistNumbers = "001";
        String sendBody =  String.format(notifyNewPricelistMailBody, (organizationName==null)?"":organizationName, pricelistNumbers, agreementNumber);
        LOG.log(Level.FINEST, "Mailing.. subject: " + notifyNewPricelistMailSubject + " Body: " + sendBody);
        for(UserAPI user: supplierContactPersons)
        {
            try {
                if(user.getElectronicAddress() != null && user.getElectronicAddress().getEmail() != null && !user.getElectronicAddress().getEmail().equals(""))
                    emailController.send(user.getElectronicAddress().getEmail(), notifyNewPricelistMailSubject, sendBody);
            } catch (MessagingException ex) {
                LOG.log(Level.SEVERE, "Failed to send notification message to provider about pending pricelists", ex);
            }
        }

    }

}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.product.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.LoggerService;
import se.inera.hjalpmedelstjansten.model.api.CategoryAPI;
import se.inera.hjalpmedelstjansten.model.entity.Category;

/**
 * Class for mapping between API and Entity classes
 * 
 * @author Tommy Berglund
 */
public class CategoryMapper {
    
    private static final HjmtLogger LOG = LoggerService.getLogger(CategoryMapper.class.getName());
    
    public static final List<CategoryAPI> map(List<Category> categories) {
        LOG.log( Level.FINEST, "map(...)" );
        if( categories == null || categories.isEmpty() ) {
            return null;
        }
        List<CategoryAPI> categoryAPIs = new ArrayList<>();
        for( Category category : categories ) {
            categoryAPIs.add(map(category));
        }
        return categoryAPIs;
    }
    
    public static final CategoryAPI map(Category category) {
        LOG.log( Level.FINEST, "map(...)" );
        if( category == null ) {
            return null;
        }
        CategoryAPI categoryAPI = new CategoryAPI();        
        categoryAPI.setId(category.getUniqueId());
        categoryAPI.setName(category.getName());
        categoryAPI.setCode(category.getCode());
        categoryAPI.setDescription(category.getDescription());
        categoryAPI.setArticleType(category.getArticleType());
        return categoryAPI;
    }
    
}

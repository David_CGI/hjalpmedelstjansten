/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.logging.view;

import java.util.Date;
import se.inera.hjalpmedelstjansten.model.entity.InternalAudit;

/**
 *
 * @author Tommy Berglund
 */
public class InternalAuditEvent {
    
    private Date auditTime;
    private InternalAudit.EntityType entityType;
    private Long entityId;
    private InternalAudit.ActionType actionType;
    private Long userEngagementId;
    private String sessionId;
    private String requestIp;
    
    private InternalAuditEvent() {
        
    }

    public InternalAuditEvent( Date auditTime, InternalAudit.EntityType entityType, InternalAudit.ActionType actionType, Long userEngagementId, String sessionId, Long entityId, String requestIp ) {
        this.auditTime = auditTime;
        this.entityType = entityType;
        this.actionType = actionType;
        this.userEngagementId = userEngagementId;
        this.sessionId = sessionId;
        this.entityId = entityId;
        this.requestIp = requestIp;
    }
    
    public Date getAuditTime() {
        return auditTime;
    }

    public InternalAudit.EntityType getEntityType() {
        return entityType;
    }

    public InternalAudit.ActionType getActionType() {
        return actionType;
    }

    public Long getUserEngagementId() {
        return userEngagementId;
    }

    public String getSessionId() {
        return sessionId;
    }    

    public Long getEntityId() {
        return entityId;
    }

    public String getRequestIp() {
        return requestIp;
    }
    
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.security.controller;

import java.util.Date;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.entity.InternalAudit;

/**
 * 
 * @author Tommy Berglund
 */
@Stateless
public class LogoutController {
    
    @Inject
    private HjmtLogger LOG;
    
    @Inject
    private Event<InternalAuditEvent> internalAuditEvent;
    
    public void logoutUser(UserAPI userAPI, String requestIp) {
        LOG.log(Level.FINEST, "logoutUser(...)");
        Subject currentUser = SecurityUtils.getSubject();
        String sessionId = currentUser.getSession().getId().toString();
        currentUser.logout();
        internalAuditEvent.fire(new InternalAuditEvent(
                new Date(), 
                InternalAudit.EntityType.USER, 
                InternalAudit.ActionType.LOGOUT, 
                userAPI == null ? null:userAPI.getId(), 
                sessionId,
                userAPI == null ? null:userAPI.getId(),
                requestIp)
        );
    }

}

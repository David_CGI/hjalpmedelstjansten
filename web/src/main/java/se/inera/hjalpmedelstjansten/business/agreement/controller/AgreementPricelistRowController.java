/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.agreement.controller;

import se.inera.hjalpmedelstjansten.business.BaseController;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenException;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.organization.controller.ElectronicAddressMapper;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleController;
import se.inera.hjalpmedelstjansten.business.product.controller.CVPreventiveMaintenanceController;
import se.inera.hjalpmedelstjansten.business.product.controller.GuaranteeUnitController;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.user.controller.EmailController;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.AgreementPricelistRowAPI;
import se.inera.hjalpmedelstjansten.model.api.ElectronicAddressAPI;
import se.inera.hjalpmedelstjansten.model.api.UserAPI;
import se.inera.hjalpmedelstjansten.model.api.UserEngagementAPI;
import se.inera.hjalpmedelstjansten.model.dto.SearchDTO;
import se.inera.hjalpmedelstjansten.model.entity.*;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVGuaranteeUnit;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVPreventiveMaintenance;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.mail.MessagingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Level;

/**
 * Class for handling business logic of Agreements. This includes talking to the
 * database.
 * 
 * @author Tommy Berglund
 */
@Stateless
public class AgreementPricelistRowController extends BaseController {
    
    @Inject
    HjmtLogger LOG;
    
    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;
    
    @Inject
    AgreementPricelistRowValidation pricelistRowValidation;
    
    @Inject
    ValidationMessageService validationMessageService;
    
    @Inject
    AgreementPricelistController pricelistController;
    
    @Inject
    ArticleController articleController;
    
    @Inject
    OrganizationController organizationController;
    
    @Inject
    GuaranteeUnitController guaranteeUnitController;
    
    @Inject
    CVPreventiveMaintenanceController cVPreventiveMaintenanceController;
    
    @Inject
    AgreementController agreementController;
    
    @Inject
    AgreementPricelistController agreementPricelistController;
    
    @Inject
    UserController userController;

    @Inject
    AgreementPricelistRowController agreementPricelistRowController;
    
    @Inject
    Event<InternalAuditEvent> internalAuditEvent;
        
    @Inject
    private String rowsSentForApprovalMailBodySingle;
    
    @Inject
    private String rowsSentForApprovalMailSubjectSingle;
    
    @Inject
    private String rowsSentForApprovalMailBodyMultiple;
    
    @Inject
    private String rowsSentForApprovalMailSubjectMultiple;

    @Inject
    private String rowsSentForInactivationMailBodySingle;

    @Inject
    private String rowsSentForInactivationMailSubjectSingle;

    @Inject
    private String rowsSentForInactivationMailBodyMultiple;

    @Inject
    private String rowsSentForInactivationMailSubjectMultiple;

    @Inject
    private String rowsSentApprovedMailSubject;

    @Inject
    private String rowsSentApprovedMailBody;

    @Inject
    private String rowsSentApprovedInactivateMailSubject;
    @Inject
    private String rowsSentApprovedInactivateMailSubject_single;

    @Inject
    private String rowsSentApprovedInactivateMailBody;
    @Inject
    private String rowsSentApprovedInactivateMailBody_single;

    @Inject
    private String rowsSentDeclinedInactivateMailSubject;
    @Inject
    private String rowsSentDeclinedInactivateMailSubject_single;

    @Inject
    private String rowsSentDeclinedInactivateMailBody;
    @Inject
    private String rowsSentDeclinedInactivateMailBody_single;

    @Inject
    private String rowsSentDeclinedMailSubject;

    @Inject
    private String rowsSentDeclinedMailBody;


    @Inject
    EmailController emailController;
    
    /**
     * Search pricelist rows for the user supplied query. Search is paginated so only
     * results valid for given offset and limit is returned.
     * 
     * @param queryString user supplied query
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement
     * @param pricelistUniqueId unique id of the pricelist
     * @param userAPI user information, needed for business levels validation
     * @param offset start returning from this search result
     * @param limit maximum number of search results to return
     * @param statuses list of statuses to include in search, if null or empty, 
     * all statuses are included
     * @param articleTypes list of article types to include in search, if null or empty, 
     * all article types are included
     * @param showRowsWithPrices include rows where price is not null, if neither this
     * not showRowsWithoutPrices is set, then all rows are included
     * @param showRowsWithoutPrices include rows where price is null, if neither this
     * not showRowsWithPrices is set, then all rows are included
     * @param categoryUniqueId
     * @return a list of <code>AgreementPricelistRowAPI</code> matching the query parameters
     */
    public SearchDTO searchPricelistRows(String queryString, 
            long organizationUniqueId, 
            long agreementUniqueId, 
            long pricelistUniqueId, 
            UserAPI userAPI,
            int offset, 
            int limit,
            List<AgreementPricelistRow.Status> statuses,
            List<Article.Type> articleTypes, 
            Boolean showRowsWithPrices,
            Boolean showRowsWithoutPrices,
            Long categoryUniqueId) {
        LOG.log(Level.FINEST, "searchPricelistRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2}, statuses: {3}, articleTypes: {4}, showRowsWithPrices: {5}, showRowsWithoutPrices: {6}, categoryUniqueId: {6} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId, statuses, articleTypes, showRowsWithPrices, showRowsWithoutPrices, categoryUniqueId});
        AgreementPricelist pricelist = pricelistController.getPricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI);
        if( pricelist == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to search rows on pricelist: {1} which is is not available. Returning 404.", new Object[] {userAPI.getId(), pricelistUniqueId});
            return null;
        }

        // customers and suppliers should get rows in different statuses, suppliers get all
        // statuses which means we don't have to include it in the search so list is left null
        // or empty
        Organization organization = organizationController.getOrganization(organizationUniqueId);
        if( organization.getOrganizationType() == Organization.OrganizationType.CUSTOMER ) {
            // if no statuses are included, include all statuses except CREATED and CHANGED
            if( statuses == null || statuses.isEmpty() ) {
                statuses = new ArrayList<>(Arrays.asList(AgreementPricelistRow.Status.values()));
            }
            statuses.remove(AgreementPricelistRow.Status.CREATED);
            statuses.remove(AgreementPricelistRow.Status.CHANGED);
        }
        
        // if both showRows... is null, it means, show everything, i.e. do not include in search
        boolean searchWithPriceLimitation = true;
        showRowsWithPrices = showRowsWithPrices == null ? false: showRowsWithPrices;
        showRowsWithoutPrices = showRowsWithoutPrices == null ? false: showRowsWithoutPrices;
        if( (showRowsWithPrices && showRowsWithoutPrices) || (!showRowsWithPrices && !showRowsWithoutPrices) ) {
            searchWithPriceLimitation = false;
        }
        
        // count
        StringBuilder countSqlBuilder = new StringBuilder();
        countSqlBuilder.append("SELECT COUNT(p.uniqueId) FROM AgreementPricelistRow p ");
        if( (articleTypes != null && !articleTypes.isEmpty()) || categoryUniqueId != null ) {
            countSqlBuilder.append("LEFT JOIN p.article.category ac ");
            countSqlBuilder.append("LEFT JOIN p.article.basedOnProduct.category apc ");
        }
        countSqlBuilder.append("WHERE p.pricelist.uniqueId = :pricelistUniqueId ");
        if( statuses != null && !statuses.isEmpty() ) {
            countSqlBuilder.append("AND p.status IN :statuses ");            
        }
        if( queryString != null && !queryString.isEmpty() ) {
            countSqlBuilder.append("AND (p.article.articleName LIKE :query OR p.article.articleNumber LIKE :query) ");
        }
        if( categoryUniqueId != null ) {
            countSqlBuilder.append("AND (ac.uniqueId = :categoryUniqueId OR apc.uniqueId = :categoryUniqueId) ");
        }
        if( articleTypes != null && !articleTypes.isEmpty() ) {
            countSqlBuilder.append("AND (");
            for( int i=0; i<articleTypes.size(); i++ ) {
                if( i!=0 ) {
                    countSqlBuilder.append("OR ");
                }
                countSqlBuilder.append("(ac.articleType = :type").append(i).append(" OR apc.articleType = :type").append(i).append(" ) ");
            }
            countSqlBuilder.append(") ");
        }
        if( searchWithPriceLimitation ) {
            // means one of showRows... is false and one is true
            if( showRowsWithPrices ) {
                countSqlBuilder.append("AND p.price IS NOT NULL ");
            } else if( showRowsWithoutPrices ) {
                countSqlBuilder.append("AND p.price IS NULL ");
            }
        }
        String countQuerySql = countSqlBuilder.toString();
        LOG.log(Level.FINEST, "countQuerySql: {0}", new Object[] {countQuerySql});
        Query countQuery = em.createQuery(countQuerySql).
                setParameter("pricelistUniqueId", pricelistUniqueId);
        if( categoryUniqueId != null ) {
            countQuery.setParameter("categoryUniqueId", categoryUniqueId);
        }
        if( articleTypes != null && !articleTypes.isEmpty() ) {
            for( int i=0; i<articleTypes.size(); i++ ) {
                countQuery.setParameter("type"+i, articleTypes.get(i));
            }
        }
        if( statuses != null && !statuses.isEmpty() ) {
            countQuery.setParameter("statuses", statuses);
        }
        if( queryString != null && !queryString.isEmpty() ) {
            countQuery.setParameter("query", "%" + queryString + "%");
        }
        Long count = (Long) countQuery.getSingleResult();
              
        // search
        StringBuilder searchSqlBuilder = new StringBuilder();
        searchSqlBuilder.append("SELECT p FROM AgreementPricelistRow p ");
        if( (articleTypes != null && !articleTypes.isEmpty()) || categoryUniqueId != null ) {
            searchSqlBuilder.append("LEFT JOIN p.article.category ac ");
            searchSqlBuilder.append("LEFT JOIN p.article.basedOnProduct.category apc ");
        }
        searchSqlBuilder.append("WHERE p.pricelist.uniqueId = :pricelistUniqueId ");
        if( statuses != null && !statuses.isEmpty() ) {
            searchSqlBuilder.append("AND p.status IN :statuses ");
        }
        if( queryString != null && !queryString.isEmpty() ) {
            searchSqlBuilder.append("AND (p.article.articleName LIKE :query OR p.article.articleNumber LIKE :query) ");            
        }
        if( categoryUniqueId != null ) {
            searchSqlBuilder.append("AND (ac.uniqueId = :categoryUniqueId OR apc.uniqueId = :categoryUniqueId) ");
        }
        if( articleTypes != null && !articleTypes.isEmpty() ) {
            searchSqlBuilder.append("AND (");
            for( int i=0; i<articleTypes.size(); i++ ) {
                if( i!=0 ) {
                    searchSqlBuilder.append("OR ");
                }
                searchSqlBuilder.append("(ac.articleType = :type").append(i).append(" OR apc.articleType = :type").append(i).append(" ) ");
            }
            searchSqlBuilder.append(") ");
        }
        if( searchWithPriceLimitation ) {
            // means one of showRows... is false and one is true
            if( showRowsWithPrices ) {
                searchSqlBuilder.append("AND p.price IS NOT NULL ");
            } else if( showRowsWithoutPrices ) {
                searchSqlBuilder.append("AND p.price IS NULL ");
            }
        }
        searchSqlBuilder.append("ORDER BY p.article.articleNumber ASC");
        String searchQuerySql = searchSqlBuilder.toString();
        LOG.log(Level.FINEST, "searchQuerySql: {0}", new Object[] {searchQuerySql});
        Query searchQuery = em.createQuery(searchQuerySql).
                setParameter("pricelistUniqueId", pricelistUniqueId);
        if( statuses != null && !statuses.isEmpty() ) {
            searchQuery.setParameter("statuses", statuses);
        }
        if( queryString != null && !queryString.isEmpty() ) {
            searchQuery.setParameter("query", "%" + queryString + "%");
        }
        if( categoryUniqueId != null ) {
            searchQuery.setParameter("categoryUniqueId", categoryUniqueId);
        }
        if( articleTypes != null && !articleTypes.isEmpty() ) {
            for( int i=0; i<articleTypes.size(); i++ ) {
                searchQuery.setParameter("type"+i, articleTypes.get(i));
            }
        }        
        List<AgreementPricelistRowAPI> pricelistRowAPIs = AgreementPricelistRowMapper.map(searchQuery.
                setMaxResults(limit).
                setFirstResult(offset).
                getResultList(), false, false);
        return new SearchDTO(count, pricelistRowAPIs);
    }
    
    /**
     * Get the <code>AgreementPricelistRowAPI</code> for the given unique id on the 
     * specified organization
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowUniqueId unique id of the pricelist row to get
     * @param userAPI user information
     * @param sessionId
     * @return the corresponding <code>AgreementPricelistRowAPI</code>
     */
    public AgreementPricelistRowAPI getPricelistRowAPI(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, long pricelistRowUniqueId, UserAPI userAPI, String sessionId, String requestIp) {
        LOG.log(Level.FINEST, "getPricelistRowAPI( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2}, pricelistRowUniqueId: {3} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowUniqueId});
        AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowUniqueId, userAPI);
        if( pricelistRow != null ) {
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.VIEW, userAPI.getId(), sessionId, pricelistRowUniqueId, requestIp));            
            return AgreementPricelistRowMapper.map( pricelistRow, true, false );
        }
        return null;
    }
    
    /**
     * Get the <code>AgreementPricelistRow</code> with the given id on the specified organization. 
     * The agreement can be read by the defined customer and the supplier and by
     * any organization (customer) that the agreement is shared with.
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowUniqueId
     * @param userAPI user information
     * @return the corresponding <code>AgreementPricelist</code>
     */
    public AgreementPricelistRow getPricelistRow(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, long pricelistRowUniqueId, UserAPI userAPI) {
        LOG.log(Level.FINEST, "getAgreement( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        AgreementPricelistRow pricelistRow = em.find(AgreementPricelistRow.class, pricelistRowUniqueId);
        if( pricelistRow == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning null.", new Object[] {userAPI.getId(), pricelistRowUniqueId});
            return null;
        }

        // make sure user can fetch agreement connected to pricelist
        AgreementPricelist pricelist = pricelistController.getPricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI);
        if( pricelist == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1}, but pricelist: {2} is not available. Returning null.", new Object[] {userAPI.getId(), pricelistRowUniqueId, pricelistUniqueId});
            return null;
        }

        if( !pricelistRow.getPricelist().getUniqueId().equals(pricelist.getUniqueId()) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} not on given pricelist: {2}. Returning null.", new Object[] {userAPI.getId(), pricelistRowUniqueId, pricelist.getUniqueId()});
            return null;
        }
        
        // customers cannot see rows in status CREATED, only limitation right now
        Organization organization = organizationController.getOrganization(organizationUniqueId);
        if( organization.getOrganizationType() == Organization.OrganizationType.CUSTOMER && pricelistRow.getStatus() == AgreementPricelistRow.Status.CREATED ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} on given pricelist: {2} in status: {3} which is not allowed. Returning null.", new Object[] {userAPI.getId(), pricelistRowUniqueId, pricelist.getUniqueId(), pricelistRow.getStatus()});
            return null;
        }

        return pricelistRow;
    }

    public ApprovedAgreementPricelistRow getApprovedPricelistRow(long pricelistRowUniqueId) {
        ApprovedAgreementPricelistRow approvedPricelistRow = em.find(ApprovedAgreementPricelistRow.class, pricelistRowUniqueId);
        return approvedPricelistRow;
    }

    /**
     * Create a pricelist row based on the supplied data. 
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowAPIs user supplied data
     * @param userAPI logged in user's session information
     * @return the created <code>AgreementPricelistRowAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     */
    public List<AgreementPricelistRowAPI> createPricelistRows( long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, List<AgreementPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp ) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "createPricelistRow( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});        
        
        // pricelist
        AgreementPricelist pricelist = pricelistController.getPricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI);
        if( pricelist == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to add articles to pricelist: {1} which does not exist or is not available to user", new Object[] {userAPI.getId(), pricelistUniqueId});
            throw validationMessageService.generateValidationException("pricelist", "pricelistrow.pricelist.notNull"); 
        }
        List<AgreementPricelistRowAPI> agreementPricelistRowAPIs = new ArrayList<>();
        List<Long> currentArticleUniqueIdsInPricelist = getAgreementPricelistArticleUniqueIds(pricelist.getUniqueId());
        for( AgreementPricelistRowAPI pricelistRowAPI : pricelistRowAPIs ) {
            AgreementPricelistRow pricelistRow = createPricelistRow(organizationUniqueId, agreementUniqueId, pricelistRowAPI, pricelist, currentArticleUniqueIdsInPricelist, userAPI, sessionId, requestIp);
            currentArticleUniqueIdsInPricelist.add(pricelistRow.getArticle().getUniqueId());
            agreementPricelistRowAPIs.add(AgreementPricelistRowMapper.map(pricelistRow, true, false));
        }
        return agreementPricelistRowAPIs;
    }

    public AgreementPricelistRow createPricelistRow(long organizationUniqueId, long agreementUniqueId, AgreementPricelistRowAPI pricelistRowAPI, AgreementPricelist pricelist, List<Long> currentArticleUniqueIdsInPricelist, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        AgreementPricelist currentPricelist = pricelistController.getCurrentPricelist(agreementUniqueId);
        return createPricelistRow(organizationUniqueId, pricelistRowAPI, pricelist, currentPricelist, currentArticleUniqueIdsInPricelist, userAPI, sessionId, requestIp);
    }

    public AgreementPricelistRowAPI deleteRow(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, long pricelistRowUniqueId, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "deleteRow( organizationUniqueId: {0}, agreementUniqueId: {1} pricelistUniqueId: {2} pricelistRowUniqueId: {3})", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowUniqueId});
        AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowUniqueId, userAPI);
        //ApprovedAgreementPricelistRow approvedPricelistRow = getApprovedPricelistRow(pricelistRowUniqueId);

        em.remove(pricelistRow);
        LOG.log(Level.INFO, "Row in agreement pricelist deleted...");

        //em.remove(approvedPricelistRow);
        //LOG.log(Level.INFO, "Row in approved agreement pricelist deleted...");
        return AgreementPricelistRowMapper.map(pricelistRow, true, true); //???
    }

    public AgreementPricelistRow createPricelistRow(long organizationUniqueId, AgreementPricelistRowAPI pricelistRowAPI, AgreementPricelist pricelist, AgreementPricelist currentPricelist, List<Long> currentArticleUniqueIdsInPricelist, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        pricelistRowValidation.validateForCreate(pricelistRowAPI, pricelist, currentPricelist);

        Date validFrom = new Date();
        AgreementPricelist.Status pricelistStatus = AgreementPricelistMapper.getPricelistStatus(pricelist, currentPricelist);
        if( pricelistStatus == AgreementPricelist.Status.FUTURE ) {
            validFrom = pricelist.getValidFrom();
        }
        AgreementPricelistRow pricelistRow = AgreementPricelistRowMapper.map(pricelistRowAPI, validFrom);
        pricelistRow.setPricelist(pricelist);

        // article
        Article article = articleController.getArticle(organizationUniqueId, pricelistRowAPI.getArticle().getId(), userAPI);
        if( article == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to add article: {1} which doesn't exist to pricelist: {2}", new Object[] {userAPI.getId(), pricelistRowAPI.getArticle().getId(), pricelist.getUniqueId()});
            throw validationMessageService.generateValidationException("article", "pricelistrow.article.notExist");                
        }
        /*
        HJAL-1774
        if( article.getStatus() == Product.Status.DISCONTINUED ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to add article: {1} which is discontinued to pricelist: {2}", new Object[] {userAPI.getId(), article.getUniqueId(), pricelist.getUniqueId()});
            throw validationMessageService.generateValidationException("article", "pricelistrow.article.discontinued");                
        }*/

        /*
        HJAL-2022
        if( currentArticleUniqueIdsInPricelist.contains(article.getUniqueId())) {
            throw validationMessageService.generateValidationException("article", "pricelistrow.article.alreadyExistsInPricelist", article.getArticleName());                
        }
        */
        pricelistRow.setArticle(article);

        Article.Type articleType = pricelistRow.getArticle().getBasedOnProduct() == null ? pricelistRow.getArticle().getCategory().getArticleType(): pricelistRow.getArticle().getBasedOnProduct().getCategory().getArticleType();
        AgreementPricelistRowMapper.mapUpdatableFields(pricelistRowAPI, pricelistRow, articleType, true);
        setOverrideWarrantyValidFrom(pricelistRowAPI, pricelistRow, articleType, userAPI, true);

        // set overridden warranty quantity unit (if any)
        setOverriddenWarrantyQuantityUnit(pricelistRow, pricelistRowAPI, true);
        
        em.persist(pricelistRow);
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.CREATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));            
        return pricelistRow;
    }
    
    /**
     * Update a pricelist based on the supplied data
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist 
     * @param pricelistRowUniqueId unique id of the pricelist row
     * @param pricelistRowAPI user supplied data
     * @param userAPI logged in user's session information
     * @param sessionId
     * @param requestIp
     * @return the updated <code>AgreementPricelistRowAPI</code>
     * @throws se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException if validation fails
     */
    public AgreementPricelistRowAPI updatePricelistRow(long organizationUniqueId, 
            long agreementUniqueId, 
            long pricelistUniqueId, 
            long pricelistRowUniqueId, 
            AgreementPricelistRowAPI pricelistRowAPI, 
            UserAPI userAPI, 
            String sessionId, 
            String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updatePricelistRow( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2}, pricelistRowUniqueId: {3} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowUniqueId});        
        AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowUniqueId, userAPI);
        if( pricelistRow == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning 404.", new Object[] {userAPI.getId(), pricelistRowUniqueId});
            return null;
        }
        AgreementPricelist currentPricelist = pricelistController.getCurrentPricelist(agreementUniqueId);
        return updatePricelistRow(pricelistRow, pricelistRowAPI, userAPI, sessionId, requestIp, currentPricelist);
    }
    
    public AgreementPricelistRowAPI updatePricelistRow(AgreementPricelistRow pricelistRow, 
            AgreementPricelistRowAPI pricelistRowAPI, 
            UserAPI userAPI, 
            String sessionId, 
            String requestIp, 
            AgreementPricelist currentPricelist) throws HjalpmedelstjanstenValidationException {
        pricelistRowValidation.validateForUpdate(pricelistRowAPI, pricelistRow, currentPricelist);
        pricelistRow.setPrice(pricelistRowAPI.getPrice());
        pricelistRow.setLeastOrderQuantity(pricelistRowAPI.getLeastOrderQuantity());
        Article.Type articleType = pricelistRow.getArticle().getBasedOnProduct() == null ? pricelistRow.getArticle().getCategory().getArticleType(): pricelistRow.getArticle().getBasedOnProduct().getCategory().getArticleType();
        AgreementPricelistRowMapper.mapUpdatableFields(pricelistRowAPI, pricelistRow, articleType, false);        
        setOverrideWarrantyValidFrom(pricelistRowAPI, pricelistRow, articleType, userAPI, false);

        // set overridden warranty quantity unit (if any)
        setOverriddenWarrantyQuantityUnit(pricelistRow, pricelistRowAPI, false);
        if( pricelistRow.getStatus() == AgreementPricelistRow.Status.ACTIVE || pricelistRow.getStatus() == AgreementPricelistRow.Status.DECLINED ) {
            pricelistRow.setStatus(AgreementPricelistRow.Status.CHANGED);
        }
        internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
        return AgreementPricelistRowMapper.map(pricelistRow, true, false);
    }

    /**
     * Send the specified pricelist rows for approval by customer. The supplier
     * can do this and only if each row is either in status CREATED, CHANGED or DECLINED.
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowAPIs list of rows to send to customer for approval
     * @param userAPI user session information
     * @param sessionId
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * a rows has the wrong status
     */
    public List<AgreementPricelistRowAPI> sendRowsForCustomerApproval(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, List<AgreementPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "sendRowsForCustomerApproval( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        if( pricelistRowAPIs == null || pricelistRowAPIs.isEmpty() ) {
            return null;
        }
        List<AgreementPricelistRow> pricelistRows = new ArrayList<>();
        for( AgreementPricelistRowAPI pricelistRowAPI : pricelistRowAPIs ) {
            AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPI.getId(), userAPI);
            if( pricelistRow == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning null.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                return null;
            }
            if( pricelistRow.getPricelist().getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to send pricelist row: {1} for customer approval where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelistRow.getUniqueId()});
                throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");
            }
            if( !AgreementPricelistRow.Status.CREATED.equals(pricelistRow.getStatus()) &&
                    !AgreementPricelistRow.Status.DECLINED.equals(pricelistRow.getStatus()) &&
                    !AgreementPricelistRow.Status.CHANGED.equals(pricelistRow.getStatus())) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to send pricelist row: {1} for customer approval which has wrong status: {2}", new Object[] {userAPI.getId(), pricelistRow.getUniqueId(), pricelistRow.getStatus()});
                throw validationMessageService.generateValidationException("status", "pricelistrow.sendRowsForCustomerApproval.wrongStatus");
            }
            if( pricelistRow.getPrice() == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to send pricelist row: {1} for customer approval, but row has no price.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                throw validationMessageService.generateValidationException("price", "pricelistrow.sendRowsForCustomerApproval.noPrice");
            }
            pricelistRow.setStatus(AgreementPricelistRow.Status.PENDING_APPROVAL);
            pricelistRows.add(pricelistRow);
        }
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            em.merge(pricelistRow);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
        }

        try {
            // send message to customer
            Agreement agreement = pricelistRows.get(0).getPricelist().getAgreement();
            if( agreement.getCustomerPricelistApprovers() != null && !agreement.getCustomerPricelistApprovers().isEmpty() ) {
                LOG.log( Level.FINEST, "{0} rows sent for approval, send mail", new Object[] {pricelistRows.size()});
                String selectedSubject = pricelistRows.size() == 1 ? rowsSentForApprovalMailSubjectSingle: rowsSentForApprovalMailSubjectMultiple;
                String selectedBody = pricelistRows.size() == 1 ? rowsSentForApprovalMailBodySingle: rowsSentForApprovalMailBodyMultiple;
                String priceListNumber = "-";
                if( pricelistRows.size() > 0 && pricelistRows.get(0) != null) {
                    priceListNumber = pricelistRows.get(0).getPricelist().getNumber();
                }

                String mailBody = String.format(selectedBody, pricelistRows.size(), agreement.getAgreementName(), agreement.getAgreementNumber(), priceListNumber, agreement.getSupplier().getOrganizationName());

                for( UserEngagement userEngagement : agreement.getCustomerPricelistApprovers() ) {
                    emailController.send(userEngagement.getUserAccount().getElectronicAddress().getEmail(), selectedSubject, mailBody);
                }
            }
        } catch (MessagingException ex) {
            LOG.log(Level.SEVERE, "Failed to send message on rows sent for approval", ex);
        }
        return AgreementPricelistRowMapper.map(pricelistRows, true, false);
    }


    /**
     * Send the specified pricelist rows for approval by customer. The supplier 
     * can do this and only if each row is either in status CREATED, CHANGED or DECLINED. 
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist 
     * @param pricelistRowAPIs list of rows to send to customer for approval
     * @param userAPI user session information
     * @param sessionId
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * a rows has the wrong status
     */
    public List<AgreementPricelistRowAPI> sendRowsAndCommentForCustomerApproval(String comment, long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, List<AgreementPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "sendRowsForCustomerApproval( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2}, comment: {3} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId, comment});
        if( pricelistRowAPIs == null || pricelistRowAPIs.isEmpty() ) {
            return null;
        }
        List<AgreementPricelistRow> pricelistRows = new ArrayList<>();

        ApprovedAgreementPricelistRow approvedPricelistRow = new ApprovedAgreementPricelistRow();
        String changeMessage = "";

        for( AgreementPricelistRowAPI pricelistRowAPI : pricelistRowAPIs ) {
            AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPI.getId(), userAPI);
            if( pricelistRow == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning null.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                return null;
            }
            if( pricelistRow.getPricelist().getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to send pricelist row: {1} for customer approval where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelistRow.getUniqueId()});
                throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");   
            }
            if( !AgreementPricelistRow.Status.CREATED.equals(pricelistRow.getStatus()) &&
                    !AgreementPricelistRow.Status.DECLINED.equals(pricelistRow.getStatus()) &&
                    !AgreementPricelistRow.Status.CHANGED.equals(pricelistRow.getStatus())) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to send pricelist row: {1} for customer approval which has wrong status: {2}", new Object[] {userAPI.getId(), pricelistRow.getUniqueId(), pricelistRow.getStatus()});
                throw validationMessageService.generateValidationException("status", "pricelistrow.sendRowsForCustomerApproval.wrongStatus");   
            }
            if( pricelistRow.getPrice() == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to send pricelist row: {1} for customer approval, but row has no price.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                throw validationMessageService.generateValidationException("price", "pricelistrow.sendRowsForCustomerApproval.noPrice");   
            }
            pricelistRow.setStatus(AgreementPricelistRow.Status.PENDING_APPROVAL);
            pricelistRows.add(pricelistRow);
        }
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            em.merge(pricelistRow);
            //jämför med värden i tabellen ApprovedAgreementPricelistRow
            approvedPricelistRow = getApprovedPricelistRow(pricelistRow.getUniqueId());
            if (approvedPricelistRow == null)
                changeMessage += "<br>Prislisterad med artikelnummer " + pricelistRow.getArticle().getArticleNumber() + " saknar historik.<br>";
            else {
                try {
                    changeMessage += "<br>Prislisterad med artikelnummer " + pricelistRow.getArticle().getArticleNumber() + " blev senast godkänd " + approvedPricelistRow.getApprovedDate().toString().substring(0, 10) + " av " + approvedPricelistRow.getApprovedBy() + ".<br>";
                    if (!pricelistRow.getPrice().equals(approvedPricelistRow.getPrice()))
                        changeMessage += "Priset är ändrat från " + approvedPricelistRow.getPrice() + " till " + pricelistRow.getPrice() + ".<br>";
                    if (pricelistRow.getValidFrom() != null && pricelistRow.getValidFrom() != approvedPricelistRow.getValidFrom())
                        changeMessage += "Giltig from är ändrat från " + approvedPricelistRow.getValidFrom() + " till " + pricelistRow.getValidFrom() + ".<br>";
                    if (pricelistRow.getLeastOrderQuantity() != approvedPricelistRow.getLeastOrderQuantity())
                        changeMessage += "Min best är ändrat från " + approvedPricelistRow.getLeastOrderQuantity() + " till " + pricelistRow.getLeastOrderQuantity() + ".<br>";
                    if (pricelistRow.getDeliveryTime() != null && !pricelistRow.getDeliveryTime().equals(approvedPricelistRow.getDeliveryTime()))
                        changeMessage += "Levtid är ändrat från " + approvedPricelistRow.getDeliveryTime() + " till " + pricelistRow.getDeliveryTime() + ".<br>";
                    if (pricelistRow.getWarrantyQuantity() != null && !pricelistRow.getWarrantyQuantity().equals(approvedPricelistRow.getWarrantyQuantity()))
                        changeMessage += "Garanti (antal) är ändrat från " + approvedPricelistRow.getWarrantyQuantity() + " till " + pricelistRow.getWarrantyQuantity() + ".<br>";
                    if (pricelistRow.getWarrantyQuantityUnit() != null && !pricelistRow.getWarrantyQuantityUnit().getName().equals(approvedPricelistRow.getWarrantyQuantityUnit().getName()))
                        changeMessage += "Garanti (enhet) är ändrat från " + approvedPricelistRow.getWarrantyQuantityUnit().getName() + " till " + pricelistRow.getWarrantyQuantityUnit().getName() + ".<br>";
                    if (pricelistRow.getWarrantyValidFrom() != null && !pricelistRow.getWarrantyValidFrom().getName().equals(approvedPricelistRow.getWarrantyValidFrom().getName()))
                        changeMessage += "Garanti (gäller från) är ändrat från " + approvedPricelistRow.getWarrantyValidFrom().getName() + " till " + pricelistRow.getWarrantyValidFrom().getName() + ".<br>";
                    if (pricelistRow.getWarrantyTerms() != null && !pricelistRow.getWarrantyTerms().equals(approvedPricelistRow.getWarrantyTerms()))
                        changeMessage += "Garanti (villkor) är ändrat från " + approvedPricelistRow.getWarrantyTerms() + " till " + pricelistRow.getWarrantyTerms() + ".<br>";
                }
                catch (Exception e)
                {
                    StringWriter sw = new StringWriter();
                    e.printStackTrace(new PrintWriter(sw));
                    String exceptionAsString = sw.toString();
                    LOG.log(Level.WARNING, "Failed to construct updatemessage error:" + e.getMessage() + "<br/>" + exceptionAsString.replaceAll("(\r\n|\n)", "<br />"));
                    changeMessage += " Uppdaterad prislisterad";
                }
            }

            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
        }
        
        try {
            // send message to customer
            Agreement agreement = pricelistRows.get(0).getPricelist().getAgreement();
            if( agreement.getCustomerPricelistApprovers() != null && !agreement.getCustomerPricelistApprovers().isEmpty() ) {
                LOG.log( Level.FINEST, "{0} rows sent for approval, send mail", new Object[] {pricelistRows.size()});
                String selectedSubject = pricelistRows.size() == 1 ? rowsSentForApprovalMailSubjectSingle: rowsSentForApprovalMailSubjectMultiple;
                String selectedBody = pricelistRows.size() == 1 ? rowsSentForApprovalMailBodySingle: rowsSentForApprovalMailBodyMultiple;
                String priceListNumber = "-";
                if( pricelistRows.size() > 0 && pricelistRows.get(0) != null) {
                    priceListNumber = pricelistRows.get(0).getPricelist().getNumber();
                }
                String mailBody = String.format(selectedBody, pricelistRows.size(), agreement.getAgreementName(), agreement.getAgreementNumber(), priceListNumber, agreement.getSupplier().getOrganizationName());
                if (comment.length() != 0)
                    mailBody = "Kommentar från leverantören:<br> " + comment.replace("QQQ","<br>").replace("@@@","/").replace("£££","\\")  + "<br><br>" + mailBody;
                    //mailBody = "Kommentar från leverantören:<br> " + comment.replace("QQQ","<br>")  + "<br><br>" + mailBody;

                mailBody += "<br><br>" + changeMessage;

                for( UserEngagement userEngagement : agreement.getCustomerPricelistApprovers() ) {
                    emailController.send(userEngagement.getUserAccount().getElectronicAddress().getEmail(), selectedSubject, mailBody);
                }
            }
        } catch (MessagingException ex) {
            LOG.log(Level.SEVERE, "Failed to send message on rows sent for approval", ex);
        }
        return AgreementPricelistRowMapper.map(pricelistRows, true, false);
    }

    /**
     * Send all valid rows on specified pricelist for approval by customer. Valid rows
     * are in status CREATED, CHANGED or DECLINED and has a price set.
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist 
     * @param userAPI user session information
     * @param sessionId
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * the agreement is discontinued
     */
    public List<AgreementPricelistRowAPI> sendAllRowsForCustomerApproval(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "sendRowsForCustomerApproval( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});        
        AgreementPricelist pricelist = pricelistController.getPricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI);
        if( pricelist == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to send all rows on pricelist: {1} for customer approval which is is not available. Returning null.", new Object[] {userAPI.getId(), pricelistUniqueId});
            return null;
        }
        if( pricelist.getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to send all pricelist rows for  pricelist: {1} for customer approval where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelist.getUniqueId()});
            throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");   
        }
        
        List<AgreementPricelistRow.Status> statuses = new ArrayList<>();
        statuses.add(AgreementPricelistRow.Status.CREATED);
        statuses.add(AgreementPricelistRow.Status.CHANGED);
        List<AgreementPricelistRow> pricelistRows = em.createNamedQuery(AgreementPricelistRow.FIND_BY_PRICELIST_AND_STATUSES_WITH_PRICE).
                setParameter("pricelistUniqueId", pricelistUniqueId).
                setParameter("statuses", statuses).
                getResultList();
        if( pricelistRows != null && !pricelistRows.isEmpty() ) {
            for( AgreementPricelistRow pricelistRow : pricelistRows ) {
                pricelistRow.setStatus(AgreementPricelistRow.Status.PENDING_APPROVAL);
            }
            for( AgreementPricelistRow pricelistRow : pricelistRows ) {
                em.merge(pricelistRow);
                internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));            
            }

            try {
                // send message to customer
                Agreement agreement = pricelistRows.get(0).getPricelist().getAgreement();
                if( agreement.getCustomerPricelistApprovers() != null && !agreement.getCustomerPricelistApprovers().isEmpty() ) {
                    LOG.log( Level.FINEST, "{0} rows sent for approval, send mail", new Object[] {pricelistRows.size()});
                    String selectedSubject = pricelistRows.size() == 1 ? rowsSentForApprovalMailSubjectSingle: rowsSentForApprovalMailSubjectMultiple;
                    String selectedBody = pricelistRows.size() == 1 ? rowsSentForApprovalMailBodySingle: rowsSentForApprovalMailBodyMultiple;
                    String priceListNumber = "-";
                    if( pricelistRows.size() > 0 && pricelistRows.get(0) != null) {
                        priceListNumber = pricelistRows.get(0).getPricelist().getNumber();
                    }
                    String mailBody = String.format(selectedBody, pricelistRows.size(), agreement.getAgreementName(), agreement.getAgreementNumber(),  priceListNumber, agreement.getSupplier().getOrganizationName());
                    for( UserEngagement userEngagement : agreement.getCustomerPricelistApprovers() ) {
                        emailController.send(userEngagement.getUserAccount().getElectronicAddress().getEmail(), selectedSubject, mailBody);
                    }
                }
            } catch (MessagingException ex) {
                LOG.log(Level.SEVERE, "Failed to send message on rows sent for approval", ex);
            }
        }   
        // don't need to include rows in reply, only return something not null
        return new ArrayList();
    }

    /**
     * Send all valid rows on specified pricelist for approval by customer. Valid rows
     * are in status CREATED, CHANGED or DECLINED and has a price set.
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param userAPI user session information
     * @param sessionId
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * the agreement is discontinued
     */
    public List<AgreementPricelistRowAPI> sendAllRowsAndCommentForCustomerApproval(String comment, long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "sendRowsForCustomerApproval( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        AgreementPricelist pricelist = pricelistController.getPricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI);
        if( pricelist == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to send all rows on pricelist: {1} for customer approval which is is not available. Returning null.", new Object[] {userAPI.getId(), pricelistUniqueId});
            return null;
        }
        if( pricelist.getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to send all pricelist rows for pricelist: {1} for customer approval where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelist.getUniqueId()});
            throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");
        }

        List<AgreementPricelistRow.Status> statuses = new ArrayList<>();
        statuses.add(AgreementPricelistRow.Status.CREATED);
        statuses.add(AgreementPricelistRow.Status.CHANGED);
        List<AgreementPricelistRow> pricelistRows = em.createNamedQuery(AgreementPricelistRow.FIND_BY_PRICELIST_AND_STATUSES_WITH_PRICE).
                setParameter("pricelistUniqueId", pricelistUniqueId).
                setParameter("statuses", statuses).
                getResultList();

        ApprovedAgreementPricelistRow approvedPricelistRow = new ApprovedAgreementPricelistRow();
        String changeMessage = "";

        if( pricelistRows != null && !pricelistRows.isEmpty() ) {
            for( AgreementPricelistRow pricelistRow : pricelistRows ) {
                pricelistRow.setStatus(AgreementPricelistRow.Status.PENDING_APPROVAL);
            }
            for( AgreementPricelistRow pricelistRow : pricelistRows ) {
                em.merge(pricelistRow);
                //jämför med värden i tabellen ApprovedAgreementPricelistRow
                approvedPricelistRow = getApprovedPricelistRow(pricelistRow.getUniqueId());
                if (approvedPricelistRow == null)
                    changeMessage += "<br>Prislisterad med artikelnummer " + pricelistRow.getArticle().getArticleNumber() + " saknar historik.<br>";
                else {
                    try {
                        changeMessage += "<br>Prislisterad med artikelnummer " + pricelistRow.getArticle().getArticleNumber() + " blev senast godkänd " + approvedPricelistRow.getApprovedDate().toString().substring(0, 10) + " av " + approvedPricelistRow.getApprovedBy() + ".<br>";
                        if (!pricelistRow.getPrice().equals(approvedPricelistRow.getPrice()))
                            changeMessage += "Priset är ändrat från " + approvedPricelistRow.getPrice() + " till " + pricelistRow.getPrice() + ".<br>";
                        if (pricelistRow.getValidFrom() != null && pricelistRow.getValidFrom() != approvedPricelistRow.getValidFrom())
                            changeMessage += "Giltig from är ändrat från " + approvedPricelistRow.getValidFrom() + " till " + pricelistRow.getValidFrom() + ".<br>";
                        if (pricelistRow.getLeastOrderQuantity() != approvedPricelistRow.getLeastOrderQuantity())
                            changeMessage += "Min best är ändrat från " + approvedPricelistRow.getLeastOrderQuantity() + " till " + pricelistRow.getLeastOrderQuantity() + ".<br>";
                        if (pricelistRow.getDeliveryTime() != null && !pricelistRow.getDeliveryTime().equals(approvedPricelistRow.getDeliveryTime()))
                            changeMessage += "Levtid är ändrat från " + approvedPricelistRow.getDeliveryTime() + " till " + pricelistRow.getDeliveryTime() + ".<br>";
                        if (pricelistRow.getWarrantyQuantity() != null && !pricelistRow.getWarrantyQuantity().equals(approvedPricelistRow.getWarrantyQuantity()))
                            changeMessage += "Garanti (antal) är ändrat från " + approvedPricelistRow.getWarrantyQuantity() + " till " + pricelistRow.getWarrantyQuantity() + ".<br>";
                        if (pricelistRow.getWarrantyQuantityUnit() != null && !pricelistRow.getWarrantyQuantityUnit().getName().equals(approvedPricelistRow.getWarrantyQuantityUnit() != null ? approvedPricelistRow.getWarrantyQuantityUnit().getName() : ""))
                            changeMessage += "Garanti (enhet) är ändrat från " + approvedPricelistRow.getWarrantyQuantityUnit().getName() + " till " + pricelistRow.getWarrantyQuantityUnit().getName() + ".<br>";
                        if (pricelistRow.getWarrantyValidFrom() != null && !pricelistRow.getWarrantyValidFrom().getName().equals(approvedPricelistRow.getWarrantyValidFrom().getName()))
                            changeMessage += "Garanti (gäller från) är ändrat från " + approvedPricelistRow.getWarrantyValidFrom().getName() + " till " + pricelistRow.getWarrantyValidFrom().getName() + ".<br>";
                        if (pricelistRow.getWarrantyTerms() != null && !pricelistRow.getWarrantyTerms().equals(approvedPricelistRow.getWarrantyTerms()))
                            changeMessage += "Garanti (villkor) är ändrat från " + approvedPricelistRow.getWarrantyTerms() + " till " + pricelistRow.getWarrantyTerms() + ".<br>";
                    }
                    catch (Exception e)
                    {
                            StringWriter sw = new StringWriter();
                            e.printStackTrace(new PrintWriter(sw));
                            String exceptionAsString = sw.toString();
                            LOG.log(Level.WARNING, "Failed to construct updatemessage error:" + e.getMessage() + "<br/>" + exceptionAsString.replaceAll("(\r\n|\n)", "<br />"));
                            changeMessage += " Uppdaterad prislisterad";
                    }
                }
                internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
            }

            try {
                // send message to customer
                Agreement agreement = pricelistRows.get(0).getPricelist().getAgreement();
                if( agreement.getCustomerPricelistApprovers() != null && !agreement.getCustomerPricelistApprovers().isEmpty() ) {
                    LOG.log( Level.FINEST, "{0} rows sent for approval, send mail", new Object[] {pricelistRows.size()});
                    String selectedSubject = pricelistRows.size() == 1 ? rowsSentForApprovalMailSubjectSingle: rowsSentForApprovalMailSubjectMultiple;
                    String selectedBody = pricelistRows.size() == 1 ? rowsSentForApprovalMailBodySingle: rowsSentForApprovalMailBodyMultiple;
                    String priceListNumber = "-";
                    if( pricelistRows.size() > 0 && pricelistRows.get(0) != null) {
                        priceListNumber = pricelistRows.get(0).getPricelist().getNumber();
                    }
                    String mailBody = String.format(selectedBody, pricelistRows.size(), agreement.getAgreementName(), agreement.getAgreementNumber(),  priceListNumber, agreement.getSupplier().getOrganizationName());
                    if (comment.length() != 0)
                        mailBody = "Kommentar från leverantören:<br> " + comment.replace("QQQ","<br>").replace("@@@","/").replace("£££","\\") + "<br><br>" + mailBody;
                    //mailBody = "Kommentar från leverantören:<br> " + comment.replace("QQQ","<br>") + "<br><br>" + mailBody;

                    mailBody += "<br><br>" + changeMessage;

                    for( UserEngagement userEngagement : agreement.getCustomerPricelistApprovers() ) {
                        emailController.send(userEngagement.getUserAccount().getElectronicAddress().getEmail(), selectedSubject, mailBody);
                    }
                }
            } catch (MessagingException ex) {
                LOG.log(Level.SEVERE, "Failed to send message on rows sent for approval", ex);
            }
        }
        // don't need to include rows in reply, only return something not null
        return new ArrayList();
    }

    /**
     * Decline the specified pricelist rows. The customer can do this and only 
     * if each row is either in status PENDING_APPROVAL or ACTIVE. 
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist 
     * @param pricelistRowAPIs list of rows to send to decline
     * @param userAPI user session information
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * a rows has the wrong status
     */
    public List<AgreementPricelistRowAPI> declineRows(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, List<AgreementPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "declineRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});        
        if( pricelistRowAPIs == null || pricelistRowAPIs.isEmpty() ) {
            return null;
        }
        
        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);
        
        // organizations/business levels (customers) that the agreement is shared 
        // with can read the agreement, but only the customer can edit it
        if( !agreement.getCustomer().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to decline row on agreement: {1}, but user organization: {2} is not customer", new Object[] {userAPI.getId(), agreement.getUniqueId(), organizationUniqueId});
            return null;
        }
        
        // the logged in user must be set as pricelist approver in order to decline rows
        if( !userIsPricelistApprover(organizationUniqueId, agreement, userAPI) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to decline pricelist rows on agreement: {1} but user is not pricelist approver", new Object[] {userAPI.getId(), agreement.getUniqueId()});
            throw generateForbiddenException();
        }
        
        List<AgreementPricelistRow> pricelistRows = new ArrayList<>();
        for( AgreementPricelistRowAPI pricelistRowAPI : pricelistRowAPIs ) {
            AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPI.getId(), userAPI);
            if( pricelistRow == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning null.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                return null;
            }
            if( pricelistRow.getPricelist().getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to decline pricelist row: {1} where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelistRow.getUniqueId()});
                throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");   
            }
            if( !AgreementPricelistRow.Status.PENDING_APPROVAL.equals(pricelistRow.getStatus()) &&
                    !AgreementPricelistRow.Status.ACTIVE.equals(pricelistRow.getStatus())) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to decline pricelist row: {1} which has wrong status: {2}", new Object[] {userAPI.getId(), pricelistRow.getUniqueId(), pricelistRow.getStatus()});
                throw validationMessageService.generateValidationException("status", "pricelistrow.declineRows.wrongStatus");   
            }
            pricelistRow.setStatus(AgreementPricelistRow.Status.DECLINED);
            pricelistRows.add(pricelistRow);
        }
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            em.merge(pricelistRow);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
        }
        // TODO: send message to supplier, this may include a customizable message from the customer

        // HJAL-1701 - Send message to supplier...
        try{
            List<UserRole.RoleName> roleNames = new ArrayList<>();
            roleNames.add(UserRole.RoleName.SupplierAgreementManager);
            List<UserAPI> supplierAgreementManager = null;
            if(userController != null && agreement.getSupplier() != null && agreement.getSupplier().getUniqueId() != null) {
                supplierAgreementManager = userController.getUsersInRolesOnOrganization(agreement.getSupplier().getUniqueId(), roleNames);
            }



            if(supplierAgreementManager != null && supplierAgreementManager.size() != 0) {
                String selectedSubject = rowsSentDeclinedMailSubject;
                StringBuilder rowsFormatted = new StringBuilder();
                for (AgreementPricelistRow pricelistRow : pricelistRows)
                {
                    rowsFormatted.append("Artikelnummer: " + pricelistRow.getArticle().getArticleNumber() + "<br/>");
                }
                String priceListNumber = "-";
                if( pricelistRows.size() > 0 && pricelistRows.get(0) != null) {
                    priceListNumber = pricelistRows.get(0).getPricelist().getNumber();
                }

                String mailBody  = String.format(rowsSentDeclinedMailBody,
                        pricelistRows.size(),
                        agreement.getAgreementName(),
                        agreement.getValidFrom(),
                        agreement.getAgreementNumber(),
                        priceListNumber,
                        agreement.getCustomer().getOrganizationName(),
                        rowsFormatted.toString());
                for (UserAPI agreementManager : supplierAgreementManager) {
                    if(agreementManager.getElectronicAddress() != null && agreementManager.getElectronicAddress().getEmail() != null && !agreementManager.getElectronicAddress().getEmail().equals("")) {
                        emailController.send(agreementManager.getElectronicAddress().getEmail(), selectedSubject, mailBody);
                    }
                }
            }
        } catch (MessagingException ex) {
            LOG.log(Level.SEVERE, "Failed to send message on declined rows on approval", ex);

        }

        return AgreementPricelistRowMapper.map(pricelistRows, true, false);
    }

    /**
     * Decline the specified pricelist rows. The customer can do this and only
     * if each row is either in status PENDING_APPROVAL or ACTIVE.
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowAPIs list of rows to send to decline
     * @param userAPI user session information
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * a rows has the wrong status
     */
    public List<AgreementPricelistRowAPI> declineRowsWithComment(String comment, long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, List<AgreementPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "declineRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        if( pricelistRowAPIs == null || pricelistRowAPIs.isEmpty() ) {
            return null;
        }

        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);

        // organizations/business levels (customers) that the agreement is shared
        // with can read the agreement, but only the customer can edit it
        if( !agreement.getCustomer().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to decline row on agreement: {1}, but user organization: {2} is not customer", new Object[] {userAPI.getId(), agreement.getUniqueId(), organizationUniqueId});
            return null;
        }

        // the logged in user must be set as pricelist approver in order to decline rows
        if( !userIsPricelistApprover(organizationUniqueId, agreement, userAPI) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to decline pricelist rows on agreement: {1} but user is not pricelist approver", new Object[] {userAPI.getId(), agreement.getUniqueId()});
            throw generateForbiddenException();
        }

        List<AgreementPricelistRow> pricelistRows = new ArrayList<>();
        for( AgreementPricelistRowAPI pricelistRowAPI : pricelistRowAPIs ) {
            AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPI.getId(), userAPI);
            if( pricelistRow == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning null.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                return null;
            }
            if( pricelistRow.getPricelist().getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to decline pricelist row: {1} where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelistRow.getUniqueId()});
                throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");
            }
            if( !AgreementPricelistRow.Status.PENDING_APPROVAL.equals(pricelistRow.getStatus()) &&
                    !AgreementPricelistRow.Status.ACTIVE.equals(pricelistRow.getStatus())) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to decline pricelist row: {1} which has wrong status: {2}", new Object[] {userAPI.getId(), pricelistRow.getUniqueId(), pricelistRow.getStatus()});
                throw validationMessageService.generateValidationException("status", "pricelistrow.declineRows.wrongStatus");
            }
            pricelistRow.setStatus(AgreementPricelistRow.Status.DECLINED);
            pricelistRows.add(pricelistRow);
        }
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            em.merge(pricelistRow);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
        }

        // HJAL-1701 - Send message to supplier...
        try{
            List<UserRole.RoleName> roleNames = new ArrayList<>();
            roleNames.add(UserRole.RoleName.SupplierAgreementManager);
            List<UserAPI> supplierAgreementManager = null;
            if(userController != null && agreement.getSupplier() != null && agreement.getSupplier().getUniqueId() != null) {
                supplierAgreementManager = userController.getUsersInRolesOnOrganization(agreement.getSupplier().getUniqueId(), roleNames);
            }



            if(supplierAgreementManager != null && supplierAgreementManager.size() != 0) {
                String selectedSubject = rowsSentDeclinedMailSubject;
                StringBuilder rowsFormatted = new StringBuilder();
                for (AgreementPricelistRow pricelistRow : pricelistRows)
                {
                    rowsFormatted.append("Artikelnummer: " + pricelistRow.getArticle().getArticleNumber() + "<br/>");
                }
                String priceListNumber = "-";
                if( pricelistRows.size() > 0 && pricelistRows.get(0) != null) {
                    priceListNumber = pricelistRows.get(0).getPricelist().getNumber();
                }

                String mailBody  = String.format(rowsSentDeclinedMailBody,
                        pricelistRows.size(),
                        agreement.getAgreementName(),
                        agreement.getValidFrom(),
                        agreement.getAgreementNumber(),
                        priceListNumber,
                        agreement.getCustomer().getOrganizationName(),
                        rowsFormatted.toString());
                if (comment.length() != 0)
                    mailBody = "Kommentar från kunden:<br> " + comment.replace("QQQ","<br>").replace("@@@","/").replace("£££","\\") + "<br><br>" + mailBody;
                for (UserAPI agreementManager : supplierAgreementManager) {
                    if(agreementManager.getElectronicAddress() != null && agreementManager.getElectronicAddress().getEmail() != null && !agreementManager.getElectronicAddress().getEmail().equals("")) {
                        emailController.send(agreementManager.getElectronicAddress().getEmail(), selectedSubject, mailBody);
                    }
                }
            }
        } catch (MessagingException ex) {
            LOG.log(Level.SEVERE, "Failed to send message on declined rows on approval", ex);

        }

        return AgreementPricelistRowMapper.map(pricelistRows, true, false);
    }


    /**
     * Activate the specified pricelist rows. The customer can do this and only 
     * if each row is either in status PENDING_APPROVAL. 
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist 
     * @param pricelistRowAPIs list of rows to send to activate
     * @param userAPI user session information
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * a rows has the wrong status
     */
    public List<AgreementPricelistRowAPI> activateRows(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, List<AgreementPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "activateRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});        
        if( pricelistRowAPIs == null || pricelistRowAPIs.isEmpty() ) {
            return null;
        }
        
        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);
        
        // organizations/business levels (customers) that the agreement is shared 
        // with can read the agreement, but only the customer can edit it
        if( !agreement.getCustomer().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to activate row on agreement: {1}, but user organization: {2} is not customer", new Object[] {userAPI.getId(), agreement.getUniqueId(), organizationUniqueId});
            return null;
        }
        
        // the logged in user must be set as pricelist approver in order to activate rows
        if( !userIsPricelistApprover(organizationUniqueId, agreement, userAPI) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to activate pricelist rows on agreement: {1} but user is not pricelist approver", new Object[] {userAPI.getId(), agreement.getUniqueId()});
            throw generateForbiddenException();
        }
        
        List<AgreementPricelistRow> pricelistRows = new ArrayList<>();
        for( AgreementPricelistRowAPI pricelistRowAPI : pricelistRowAPIs ) {
            AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPI.getId(), userAPI);
            if( pricelistRow == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning null.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                return null;
            }

            if( pricelistRow.getPricelist().getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to activate pricelist row: {1} where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelistRow.getUniqueId()});
                throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");   
            }
            if( !AgreementPricelistRow.Status.PENDING_APPROVAL.equals(pricelistRow.getStatus()) ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to activate pricelist row: {1} which has wrong status: {2}", new Object[] {userAPI.getId(), pricelistRow.getUniqueId(), pricelistRow.getStatus()});
                throw validationMessageService.generateValidationException("status", "pricelistrow.activateRows.wrongStatus");   
            }
            pricelistRow.setStatus(AgreementPricelistRow.Status.ACTIVE);
            pricelistRows.add(pricelistRow);
        }
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            em.merge(pricelistRow);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));            
        }

        try
        {
            // HJAL-1701 - Send message to supplier...
            List<UserRole.RoleName> roleNames = new ArrayList<>();
            roleNames.add(UserRole.RoleName.SupplierAgreementManager);
            List<UserAPI> supplierAgreementManager = null;
            if(userController != null && agreement.getSupplier() != null && agreement.getSupplier().getUniqueId() != null) {
                supplierAgreementManager = userController.getUsersInRolesOnOrganization(agreement.getSupplier().getUniqueId(), roleNames);
            }

            if(supplierAgreementManager != null && supplierAgreementManager.size() != 0)
            {
                LOG.log( Level.FINEST, "{0} rows are accepted, send mail", new Object[] {pricelistRows.size()});
                String priceListNumber = "-";
                if( pricelistRows.size() > 0 && pricelistRows.get(0) != null) {
                    priceListNumber = pricelistRows.get(0).getPricelist().getNumber();
                }
                String selectedSubject = rowsSentApprovedMailSubject;

                String mailBody  = String.format(rowsSentApprovedMailBody, agreement.getAgreementName(),  agreement.getValidFrom(), agreement.getAgreementNumber(),priceListNumber,agreement.getCustomer().getOrganizationName(),pricelistRows.size());
                for (UserAPI agreementManager : supplierAgreementManager) {
                    if(agreementManager.getElectronicAddress() != null && agreementManager.getElectronicAddress().getEmail() != null && !agreementManager.getElectronicAddress().getEmail().equals("")) {
                        emailController.send(agreementManager.getElectronicAddress().getEmail(), selectedSubject, mailBody);
                    }
                }
            }

        } catch (MessagingException ex) {
            LOG.log(Level.SEVERE, "Failed to send message on rows, activate pricelist rows", ex);
        }


        return AgreementPricelistRowMapper.map(pricelistRows, true, false);
    }

    /**
     * Activate the specified pricelist rows. The customer can do this and only
     * if each row is either in status PENDING_APPROVAL. Including comment.
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowAPIs list of rows to send to activate
     * @param userAPI user session information
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * a rows has the wrong status
     */
    public List<AgreementPricelistRowAPI> activateRowsWithComment(String comment, long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, List<AgreementPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "activateRowsWithComment( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        if( pricelistRowAPIs == null || pricelistRowAPIs.isEmpty() ) {
            return null;
        }

        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);

        // organizations/business levels (customers) that the agreement is shared
        // with can read the agreement, but only the customer can edit it
        if( !agreement.getCustomer().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to activate row on agreement: {1}, but user organization: {2} is not customer", new Object[] {userAPI.getId(), agreement.getUniqueId(), organizationUniqueId});
            return null;
        }

        // the logged in user must be set as pricelist approver in order to activate rows
        if( !userIsPricelistApprover(organizationUniqueId, agreement, userAPI) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to activate pricelist rows on agreement: {1} but user is not pricelist approver", new Object[] {userAPI.getId(), agreement.getUniqueId()});
            throw generateForbiddenException();
        }

        List<AgreementPricelistRow> pricelistRows = new ArrayList<>();
        List<ApprovedAgreementPricelistRow> approvedPricelistRows = new ArrayList<>();

        for( AgreementPricelistRowAPI pricelistRowAPI : pricelistRowAPIs ) {
            AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPI.getId(), userAPI);
            ApprovedAgreementPricelistRow approvedPricelistRow = getApprovedPricelistRow(pricelistRowAPI.getId());
            if( pricelistRow == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning null.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                return null;
            }

            if( pricelistRow.getPricelist().getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to activate pricelist row: {1} where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelistRow.getUniqueId()});
                throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");
            }
            if( !AgreementPricelistRow.Status.PENDING_APPROVAL.equals(pricelistRow.getStatus()) ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to activate pricelist row: {1} which has wrong status: {2}", new Object[] {userAPI.getId(), pricelistRow.getUniqueId(), pricelistRow.getStatus()});
                throw validationMessageService.generateValidationException("status", "pricelistrow.activateRows.wrongStatus");
            }
            pricelistRow.setStatus(AgreementPricelistRow.Status.ACTIVE);
            pricelistRows.add(pricelistRow);

            if (approvedPricelistRow == null){
                approvedPricelistRow = new ApprovedAgreementPricelistRow();
            }

            approvedPricelistRow.setUniqueId(pricelistRow.getUniqueId()); //OK
            approvedPricelistRow.setApprovedBy(userAPI.getFirstName() + " " + userAPI.getLastName() + " (" + userAPI.getUsername() + ")");
            approvedPricelistRow.setLeastOrderQuantity(pricelistRow.getLeastOrderQuantity()); //OK
            approvedPricelistRow.setPrice(pricelistRow.getPrice()); //OK

            if (pricelistRow.getValidFrom() != null)
                approvedPricelistRow.setValidFrom(pricelistRow.getValidFrom());
            else
                approvedPricelistRow.setValidFrom(agreement.getValidFrom());


            Article.Type mtype = pricelistRow.getArticle().getBasedOnProduct() == null ? pricelistRow.getArticle().getCategory().getArticleType(): pricelistRow.getArticle().getBasedOnProduct().getCategory().getArticleType();

            if (pricelistRow.getDeliveryTime() != null)
                approvedPricelistRow.setDeliveryTime(pricelistRow.getDeliveryTime());
            else {

                if (mtype == Article.Type.H)
                    approvedPricelistRow.setDeliveryTime(agreement.getDeliveryTimeH());
                if (mtype == Article.Type.I)
                    approvedPricelistRow.setDeliveryTime(agreement.getDeliveryTimeI());
                if (mtype == Article.Type.R)
                    approvedPricelistRow.setDeliveryTime(agreement.getDeliveryTimeR());
                if (mtype == Article.Type.T)
                    approvedPricelistRow.setDeliveryTime(agreement.getDeliveryTimeT());
                if (mtype == Article.Type.Tj)
                    approvedPricelistRow.setDeliveryTime(agreement.getDeliveryTimeTJ());
            }

            if (pricelistRow.getWarrantyQuantity() != null)
                approvedPricelistRow.setWarrantyQuantity(pricelistRow.getWarrantyQuantity());
            else {
                if (mtype == Article.Type.H)
                    approvedPricelistRow.setWarrantyQuantity(agreement.getWarrantyQuantityH());
                if (mtype == Article.Type.I)
                    approvedPricelistRow.setWarrantyQuantity(agreement.getWarrantyQuantityI());
                if (mtype == Article.Type.R)
                    approvedPricelistRow.setWarrantyQuantity(agreement.getWarrantyQuantityR());
                if (mtype == Article.Type.T)
                    approvedPricelistRow.setWarrantyQuantity(agreement.getWarrantyQuantityT());
                if (mtype == Article.Type.Tj)
                    approvedPricelistRow.setWarrantyQuantity(agreement.getWarrantyQuantityTJ());
            }

            if (pricelistRow.getWarrantyQuantityUnit() != null)
                approvedPricelistRow.setWarrantyQuantityUnit(pricelistRow.getWarrantyQuantityUnit());
            else{
                if (mtype == Article.Type.H)
                    approvedPricelistRow.setWarrantyQuantityUnit(agreement.getWarrantyQuantityHUnit());
                if (mtype == Article.Type.I)
                    approvedPricelistRow.setWarrantyQuantityUnit(agreement.getWarrantyQuantityIUnit());
                if (mtype == Article.Type.R)
                    approvedPricelistRow.setWarrantyQuantityUnit(agreement.getWarrantyQuantityRUnit());
                if (mtype == Article.Type.T)
                    approvedPricelistRow.setWarrantyQuantityUnit(agreement.getWarrantyQuantityTUnit());
                if (mtype == Article.Type.Tj)
                    approvedPricelistRow.setWarrantyQuantityUnit(agreement.getWarrantyQuantityTJUnit());
            }

            if (pricelistRow.getWarrantyTerms() != null)
                approvedPricelistRow.setWarrantyTerms(pricelistRow.getWarrantyTerms());
            else{
                if (mtype == Article.Type.H)
                    approvedPricelistRow.setWarrantyTerms(agreement.getWarrantyTermsH());
                if (mtype == Article.Type.I)
                    approvedPricelistRow.setWarrantyTerms(agreement.getWarrantyTermsI());
                if (mtype == Article.Type.R)
                    approvedPricelistRow.setWarrantyTerms(agreement.getWarrantyTermsR());
                if (mtype == Article.Type.T)
                    approvedPricelistRow.setWarrantyTerms(agreement.getWarrantyTermsT());
                if (mtype == Article.Type.Tj)
                    approvedPricelistRow.setWarrantyTerms(agreement.getWarrantyTermsTJ());
            }

            if (pricelistRow.getWarrantyValidFrom() != null)
                approvedPricelistRow.setWarrantyValidFrom(pricelistRow.getWarrantyValidFrom());
            else{
                if (mtype == Article.Type.H)
                    approvedPricelistRow.setWarrantyValidFrom(agreement.getWarrantyValidFromH());
                if (mtype == Article.Type.I)
                    approvedPricelistRow.setWarrantyValidFrom(agreement.getWarrantyValidFromI());
                if (mtype == Article.Type.R)
                    approvedPricelistRow.setWarrantyValidFrom(agreement.getWarrantyValidFromR());
                if (mtype == Article.Type.T)
                    approvedPricelistRow.setWarrantyValidFrom(agreement.getWarrantyValidFromT());
                if (mtype == Article.Type.Tj)
                    approvedPricelistRow.setWarrantyValidFrom(agreement.getWarrantyValidFromTJ());
            }
            approvedPricelistRows.add(approvedPricelistRow);
        }
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            em.merge(pricelistRow);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
        }

        for( ApprovedAgreementPricelistRow approvedPricelistRow : approvedPricelistRows ) {
            em.merge(approvedPricelistRow);
        }

        try
        {
            // HJAL-1701 - Send message to supplier...
            List<UserRole.RoleName> roleNames = new ArrayList<>();
            roleNames.add(UserRole.RoleName.SupplierAgreementManager);
            List<UserAPI> supplierAgreementManager = null;
            if(userController != null && agreement.getSupplier() != null && agreement.getSupplier().getUniqueId() != null) {
                supplierAgreementManager = userController.getUsersInRolesOnOrganization(agreement.getSupplier().getUniqueId(), roleNames);
            }

            if(supplierAgreementManager != null && supplierAgreementManager.size() != 0)
            {
                LOG.log( Level.FINEST, "{0} rows are accepted, send mail", new Object[] {pricelistRows.size()});
                String priceListNumber = "-";
                if( pricelistRows.size() > 0 && pricelistRows.get(0) != null) {
                    priceListNumber = pricelistRows.get(0).getPricelist().getNumber();
                }
                String selectedSubject = rowsSentApprovedMailSubject;

                String mailBody  = String.format(rowsSentApprovedMailBody, agreement.getAgreementName(),  agreement.getValidFrom(), agreement.getAgreementNumber(),priceListNumber,agreement.getCustomer().getOrganizationName(),pricelistRows.size());
                if (comment.length() != 0)
                    mailBody = "Kommentar från kunden:<br> " + comment.replace("QQQ","<br>").replace("@@@","/").replace("£££","\\") + "<br><br>" + mailBody;
                for (UserAPI agreementManager : supplierAgreementManager) {
                    if(agreementManager.getElectronicAddress() != null && agreementManager.getElectronicAddress().getEmail() != null && !agreementManager.getElectronicAddress().getEmail().equals("")) {
                        emailController.send(agreementManager.getElectronicAddress().getEmail(), selectedSubject, mailBody);
                    }
                }
            }

        } catch (MessagingException ex) {
            LOG.log(Level.SEVERE, "Failed to send message on rows, activate pricelist rows", ex);
        }


        return AgreementPricelistRowMapper.map(pricelistRows, true, false);
    }

    /**
     * Activate all pricelist rows in status PENDING_APPROVAL. The customer can
     * do this
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param userAPI user session information
     * @return null or empty list
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * the agreement is discontinued
     */
    public List<AgreementPricelistRowAPI> activateAllRowsWithComment(String comment, long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "activateAllRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        AgreementPricelist pricelist = pricelistController.getPricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI);

        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);

        if( pricelist == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to send all rows on pricelist: {1} for customer approval which is is not available. Returning null.", new Object[] {userAPI.getId(), pricelistUniqueId});
            return null;
        }
        if( pricelist.getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to activate all pricelist rows for pricelist: {1} where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelist.getUniqueId()});
            throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");
        }

        List<AgreementPricelistRow.Status> statuses = new ArrayList<>();
        statuses.add(AgreementPricelistRow.Status.PENDING_APPROVAL);
        List<AgreementPricelistRow> pricelistRows = em.createNamedQuery(AgreementPricelistRow.FIND_BY_PRICELIST_AND_STATUSES).
                setParameter("pricelistUniqueId", pricelistUniqueId).
                setParameter("statuses", statuses).
                getResultList();

        List<ApprovedAgreementPricelistRow> approvedPricelistRows = new ArrayList<>();

        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            pricelistRow.setStatus(AgreementPricelistRow.Status.ACTIVE);

            ApprovedAgreementPricelistRow approvedPricelistRow = getApprovedPricelistRow(pricelistRow.getUniqueId());

            if (approvedPricelistRow == null){
                approvedPricelistRow = new ApprovedAgreementPricelistRow();
            }

            approvedPricelistRow.setUniqueId(pricelistRow.getUniqueId());
            approvedPricelistRow.setApprovedBy(userAPI.getFirstName() + " " + userAPI.getLastName() + " (" + userAPI.getUsername() + ")");
            approvedPricelistRow.setLeastOrderQuantity(pricelistRow.getLeastOrderQuantity());
            approvedPricelistRow.setPrice(pricelistRow.getPrice());

            if (pricelistRow.getValidFrom() != null)
                approvedPricelistRow.setValidFrom(pricelistRow.getValidFrom());
            else
                approvedPricelistRow.setValidFrom(agreement.getValidFrom());

            Article.Type mtype = pricelistRow.getArticle().getBasedOnProduct() == null ? pricelistRow.getArticle().getCategory().getArticleType(): pricelistRow.getArticle().getBasedOnProduct().getCategory().getArticleType();

            if (pricelistRow.getDeliveryTime() != null)
                approvedPricelistRow.setDeliveryTime(pricelistRow.getDeliveryTime());
            else {
                if (mtype == Article.Type.H)
                    approvedPricelistRow.setDeliveryTime(agreement.getDeliveryTimeH());
                if (mtype == Article.Type.I)
                    approvedPricelistRow.setDeliveryTime(agreement.getDeliveryTimeI());
                if (mtype == Article.Type.R)
                    approvedPricelistRow.setDeliveryTime(agreement.getDeliveryTimeR());
                if (mtype == Article.Type.T)
                    approvedPricelistRow.setDeliveryTime(agreement.getDeliveryTimeT());
                if (mtype == Article.Type.Tj)
                    approvedPricelistRow.setDeliveryTime(agreement.getDeliveryTimeTJ());
            }

            if (pricelistRow.getWarrantyQuantity() != null)
                approvedPricelistRow.setWarrantyQuantity(pricelistRow.getWarrantyQuantity());
            else {
                if (mtype == Article.Type.H)
                    approvedPricelistRow.setWarrantyQuantity(agreement.getWarrantyQuantityH());
                if (mtype == Article.Type.I)
                    approvedPricelistRow.setWarrantyQuantity(agreement.getWarrantyQuantityI());
                if (mtype == Article.Type.R)
                    approvedPricelistRow.setWarrantyQuantity(agreement.getWarrantyQuantityR());
                if (mtype == Article.Type.T)
                    approvedPricelistRow.setWarrantyQuantity(agreement.getWarrantyQuantityT());
                if (mtype == Article.Type.Tj)
                    approvedPricelistRow.setWarrantyQuantity(agreement.getWarrantyQuantityTJ());
            }

            if (pricelistRow.getWarrantyQuantityUnit() != null)
                approvedPricelistRow.setWarrantyQuantityUnit(pricelistRow.getWarrantyQuantityUnit());
            else{
                if (mtype == Article.Type.H)
                    approvedPricelistRow.setWarrantyQuantityUnit(agreement.getWarrantyQuantityHUnit());
                if (mtype == Article.Type.I)
                    approvedPricelistRow.setWarrantyQuantityUnit(agreement.getWarrantyQuantityIUnit());
                if (mtype == Article.Type.R)
                    approvedPricelistRow.setWarrantyQuantityUnit(agreement.getWarrantyQuantityRUnit());
                if (mtype == Article.Type.T)
                    approvedPricelistRow.setWarrantyQuantityUnit(agreement.getWarrantyQuantityTUnit());
                if (mtype == Article.Type.Tj)
                    approvedPricelistRow.setWarrantyQuantityUnit(agreement.getWarrantyQuantityTJUnit());
            }

            if (pricelistRow.getWarrantyTerms() != null)
                approvedPricelistRow.setWarrantyTerms(pricelistRow.getWarrantyTerms());
            else{
                if (mtype == Article.Type.H)
                    approvedPricelistRow.setWarrantyTerms(agreement.getWarrantyTermsH());
                if (mtype == Article.Type.I)
                    approvedPricelistRow.setWarrantyTerms(agreement.getWarrantyTermsI());
                if (mtype == Article.Type.R)
                    approvedPricelistRow.setWarrantyTerms(agreement.getWarrantyTermsR());
                if (mtype == Article.Type.T)
                    approvedPricelistRow.setWarrantyTerms(agreement.getWarrantyTermsT());
                if (mtype == Article.Type.Tj)
                    approvedPricelistRow.setWarrantyTerms(agreement.getWarrantyTermsTJ());
            }

            if (pricelistRow.getWarrantyValidFrom() != null)
                approvedPricelistRow.setWarrantyValidFrom(pricelistRow.getWarrantyValidFrom());
            else{
                if (mtype == Article.Type.H)
                    approvedPricelistRow.setWarrantyValidFrom(agreement.getWarrantyValidFromH());
                if (mtype == Article.Type.I)
                    approvedPricelistRow.setWarrantyValidFrom(agreement.getWarrantyValidFromI());
                if (mtype == Article.Type.R)
                    approvedPricelistRow.setWarrantyValidFrom(agreement.getWarrantyValidFromR());
                if (mtype == Article.Type.T)
                    approvedPricelistRow.setWarrantyValidFrom(agreement.getWarrantyValidFromT());
                if (mtype == Article.Type.Tj)
                    approvedPricelistRow.setWarrantyValidFrom(agreement.getWarrantyValidFromTJ());
            }
            approvedPricelistRows.add(approvedPricelistRow);
        }
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            em.merge(pricelistRow);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
        }
        for( ApprovedAgreementPricelistRow approvedPricelistRow : approvedPricelistRows ) {
            em.merge(approvedPricelistRow);
        }

        try
        {
            // Send message to supplier...

            List<UserRole.RoleName> roleNames = new ArrayList<>();
            roleNames.add(UserRole.RoleName.SupplierAgreementManager);
            List<UserAPI> supplierAgreementManager = null;
            if(userController != null && agreement.getSupplier() != null && agreement.getSupplier().getUniqueId() != null) {
                supplierAgreementManager = userController.getUsersInRolesOnOrganization(agreement.getSupplier().getUniqueId(), roleNames);
            }

            if(supplierAgreementManager != null && supplierAgreementManager.size() != 0)
            {
                LOG.log( Level.FINEST, "{0} rows are accepted, send mail", new Object[] {pricelistRows.size()});
                String priceListNumber = "-";
                if( pricelistRows.size() > 0 && pricelistRows.get(0) != null) {
                    priceListNumber = pricelistRows.get(0).getPricelist().getNumber();
                }
                String selectedSubject = rowsSentApprovedMailSubject;

                String mailBody  = String.format(rowsSentApprovedMailBody, agreement.getAgreementName(),  agreement.getValidFrom(), agreement.getAgreementNumber(),priceListNumber,agreement.getCustomer().getOrganizationName(),pricelistRows.size());
                if (comment.length() != 0)
                    mailBody = "Kommentar från kunden:<br> " + comment.replace("QQQ","<br>").replace("@@@","/").replace("£££","\\") + "<br><br>" + mailBody;
                for (UserAPI agreementManager : supplierAgreementManager) {
                    if(agreementManager.getElectronicAddress() != null && agreementManager.getElectronicAddress().getEmail() != null && !agreementManager.getElectronicAddress().getEmail().equals("")) {
                        emailController.send(agreementManager.getElectronicAddress().getEmail(), selectedSubject, mailBody);
                    }
                }
            }

        } catch (MessagingException ex) {
            LOG.log(Level.SEVERE, "Failed to send message on rows, activate pricelist rows", ex);
        }

        // don't need to include rows in reply, only return something not null
        return new ArrayList();
    }


    /**
     * Activate all pricelist rows in status PENDING_APPROVAL. The customer can 
     * do this
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist 
     * @param userAPI user session information
     * @return null or empty list
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * the agreement is discontinued
     */
    public List<AgreementPricelistRowAPI> activateAllRows(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "activateAllRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});        
        AgreementPricelist pricelist = pricelistController.getPricelist(organizationUniqueId, agreementUniqueId, pricelistUniqueId, userAPI);
        if( pricelist == null ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to send all rows on pricelist: {1} for customer approval which is is not available. Returning null.", new Object[] {userAPI.getId(), pricelistUniqueId});
            return null;
        }
        if( pricelist.getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to activate all pricelist rows for pricelist: {1} where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelist.getUniqueId()});
            throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");   
        }
        
        List<AgreementPricelistRow.Status> statuses = new ArrayList<>();
        statuses.add(AgreementPricelistRow.Status.PENDING_APPROVAL);
        List<AgreementPricelistRow> pricelistRows = em.createNamedQuery(AgreementPricelistRow.FIND_BY_PRICELIST_AND_STATUSES).
                setParameter("pricelistUniqueId", pricelistUniqueId).
                setParameter("statuses", statuses).
                getResultList();
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            pricelistRow.setStatus(AgreementPricelistRow.Status.ACTIVE);
        }
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            em.merge(pricelistRow);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));            
        }
        
        // don't need to include rows in reply, only return something not null
        return new ArrayList();
    }
    
    /**
     * Inactivate the specified pricelist rows. The supplier can do this and only 
     * if each row is either in status ACTIVE. 
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist 
     * @param pricelistRowAPIs list of rows to send to customer for inactivation
     * @param userAPI user session information
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * a rows has the wrong status
     */
    public List<AgreementPricelistRowAPI> inactivateRows(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, List<AgreementPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "inactivateRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});        
        if( pricelistRowAPIs == null || pricelistRowAPIs.isEmpty() ) {
            return null;
        }
        List<AgreementPricelistRow> pricelistRows = new ArrayList<>();
        for( AgreementPricelistRowAPI pricelistRowAPI : pricelistRowAPIs ) {
            AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPI.getId(), userAPI);
            if( pricelistRow == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning null.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                return null;
            }
            if( pricelistRow.getPricelist().getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to inactivate pricelist row: {1} where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelistRow.getUniqueId()});
                throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");   
            }
            if( !AgreementPricelistRow.Status.ACTIVE.equals(pricelistRow.getStatus()) ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to inactivate pricelist row: {1} which has wrong status: {2}", new Object[] {userAPI.getId(), pricelistRow.getUniqueId(), pricelistRow.getStatus()});
                throw validationMessageService.generateValidationException("status", "pricelistrow.inactivateRows.wrongStatus");   
            }

            pricelistRow.setStatus(AgreementPricelistRow.Status.PENDING_INACTIVATION);
            pricelistRows.add(pricelistRow);
        }
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            em.merge(pricelistRow);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));            
        }
        // TODO: send message to supplier
        return AgreementPricelistRowMapper.map(pricelistRows, true, false);
    }
    
    /**
     * Approve inactivation of the specified pricelist rows. The customer can do 
     * this and only if each row is either in status PENDING_INACTIVATION. An
     * inactive pricelist row cannot change status
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist 
     * @param pricelistRowAPIs list of rows to approve inactivation
     * @param userAPI user session information
     * @param sessionId
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * a rows has the wrong status
     */
    public List<AgreementPricelistRowAPI> approveInactivateRows(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, List<AgreementPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "approveInactivateRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});        
        if( pricelistRowAPIs == null || pricelistRowAPIs.isEmpty() ) {
            return null;
        }
        
        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);
        
        // organizations/business levels (customers) that the agreement is shared 
        // with can read the agreement, but only the customer can edit it
        if( !agreement.getCustomer().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to approve inactivate row on agreement: {1}, but user organization: {2} is not customer", new Object[] {userAPI.getId(), agreement.getUniqueId(), organizationUniqueId});
            return null;
        }
        
        // the logged in user must be set as pricelist approver in order to decline rows
        if( !userIsPricelistApprover(organizationUniqueId, agreement, userAPI) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to decline pricelist rows on agreement: {1} but user is not pricelist approver", new Object[] {userAPI.getId(), agreement.getUniqueId()});
            throw generateForbiddenException();
        }
        
        List<AgreementPricelistRow> pricelistRows = new ArrayList<>();
        for( AgreementPricelistRowAPI pricelistRowAPI : pricelistRowAPIs ) {
            AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPI.getId(), userAPI);
            if( pricelistRow == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning null.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                return null;
            }
            if( pricelistRow.getPricelist().getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to approve inactivate pricelist row: {1}  where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelistRow.getUniqueId()});
                throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");   
            }
            if( !AgreementPricelistRow.Status.PENDING_INACTIVATION.equals(pricelistRow.getStatus()) ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to approve inactivate pricelist row: {1} which has wrong status: {2}", new Object[] {userAPI.getId(), pricelistRow.getUniqueId(), pricelistRow.getStatus()});
                throw validationMessageService.generateValidationException("status", "pricelistrow.approveInactivateRows.wrongStatus");   
            }
            pricelistRow.setStatus(AgreementPricelistRow.Status.INACTIVE);
            pricelistRows.add(pricelistRow);
        }
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            em.merge(pricelistRow);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
        }
        // TODO: send message to supplier
        return AgreementPricelistRowMapper.map(pricelistRows, true, false);
    }

    /**
     * Approve inactivation of the specified pricelist rows. The customer can do
     * this and only if each row is either in status PENDING_INACTIVATION. An
     * inactive pricelist row cannot change status
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowAPIs list of rows to approve inactivation
     * @param userAPI user session information
     * @param sessionId
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * a rows has the wrong status
     */
    public List<AgreementPricelistRowAPI> approveInactivateRowsComment(String comment, long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, List<AgreementPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "approveInactivateRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        if( pricelistRowAPIs == null || pricelistRowAPIs.isEmpty() ) {
            return null;
        }

        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);

        // organizations/business levels (customers) that the agreement is shared
        // with can read the agreement, but only the customer can edit it
        if( !agreement.getCustomer().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to approve inactivate row on agreement: {1}, but user organization: {2} is not customer", new Object[] {userAPI.getId(), agreement.getUniqueId(), organizationUniqueId});
            return null;
        }

        // the logged in user must be set as pricelist approver in order to decline rows
        if( !userIsPricelistApprover(organizationUniqueId, agreement, userAPI) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to decline pricelist rows on agreement: {1} but user is not pricelist approver", new Object[] {userAPI.getId(), agreement.getUniqueId()});
            throw generateForbiddenException();
        }

        List<AgreementPricelistRow> pricelistRows = new ArrayList<>();
        for( AgreementPricelistRowAPI pricelistRowAPI : pricelistRowAPIs ) {
            AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPI.getId(), userAPI);
            if( pricelistRow == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning null.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                return null;
            }
            if( pricelistRow.getPricelist().getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to approve inactivate pricelist row: {1}  where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelistRow.getUniqueId()});
                throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");
            }
            if( !AgreementPricelistRow.Status.PENDING_INACTIVATION.equals(pricelistRow.getStatus()) ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to approve inactivate pricelist row: {1} which has wrong status: {2}", new Object[] {userAPI.getId(), pricelistRow.getUniqueId(), pricelistRow.getStatus()});
                throw validationMessageService.generateValidationException("status", "pricelistrow.approveInactivateRows.wrongStatus");
            }
            pricelistRow.setStatus(AgreementPricelistRow.Status.INACTIVE);
            pricelistRows.add(pricelistRow);
        }
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            em.merge(pricelistRow);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
        }

        // send mail to supplier PELLE
        try
        {
            List<UserRole.RoleName> roleNames = new ArrayList<>();
            roleNames.add(UserRole.RoleName.SupplierAgreementManager);
            List<UserAPI> supplierAgreementManager = null;
            if(userController != null && agreement.getSupplier() != null && agreement.getSupplier().getUniqueId() != null) {
                supplierAgreementManager = userController.getUsersInRolesOnOrganization(agreement.getSupplier().getUniqueId(), roleNames);
            }

            if(supplierAgreementManager != null && supplierAgreementManager.size() != 0)
            {
                LOG.log( Level.FINEST, "{0} inactivated row(s) accepted, send mail", new Object[] {pricelistRows.size()});
                String priceListNumber = "-";
                if( pricelistRows.size() > 0 && pricelistRows.get(0) != null) {
                    priceListNumber = pricelistRows.get(0).getPricelist().getNumber();
                }

                String selectedSubject = rowsSentApprovedInactivateMailSubject;
                String selectedBody = rowsSentApprovedInactivateMailBody;
                if (pricelistRows.size() == 1) {
                    selectedSubject = rowsSentApprovedInactivateMailSubject_single;
                    selectedBody = rowsSentApprovedInactivateMailBody_single;
                }

                String mailBody  = String.format(selectedBody, agreement.getAgreementName(), agreement.getValidFrom(), agreement.getAgreementNumber(), priceListNumber,agreement.getCustomer().getOrganizationName(), pricelistRows.size());

                if (comment.length() != 0)
                    mailBody = "Kommentar från kunden:<br> " + comment.replace("QQQ","<br>").replace("@@@","/").replace("£££","\\") + "<br><br>" + mailBody;

                for (UserAPI agreementManager : supplierAgreementManager) {
                    if(agreementManager.getElectronicAddress() != null && agreementManager.getElectronicAddress().getEmail() != null && !agreementManager.getElectronicAddress().getEmail().equals("")) {
                        emailController.send(agreementManager.getElectronicAddress().getEmail(), selectedSubject, mailBody);
                    }
                }
            }

        } catch (MessagingException ex) {
            LOG.log(Level.SEVERE, "Failed to send message on rows, approve inactivated pricelist rows", ex);
        }


        return AgreementPricelistRowMapper.map(pricelistRows, true, false);
    }

    /**
     * Decline inactivation of the specified pricelist rows. The customer can do 
     * this and only if each row is in status PENDING_INACTIVATION. An
     * inactive pricelist row cannot change status
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist 
     * @param pricelistRowAPIs list of rows to send to decline inactivation
     * @param userAPI user session information
     * @param sessionId
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * a rows has the wrong status
     */
    public List<AgreementPricelistRowAPI> declineInactivateRows(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, List<AgreementPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "declineInactivateRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});        
        if( pricelistRowAPIs == null || pricelistRowAPIs.isEmpty() ) {
            return null;
        }
        
        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);
        
        // organizations/business levels (customers) that the agreement is shared 
        // with can read the agreement, but only the customer can edit it
        if( !agreement.getCustomer().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to decline inactivate row on agreement: {1}, but user organization: {2} is not customer", new Object[] {userAPI.getId(), agreement.getUniqueId(), organizationUniqueId});
            return null;
        }
        
        // the logged in user must be set as pricelist approver in order to decline rows
        if( !userIsPricelistApprover(organizationUniqueId, agreement, userAPI) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to decline pricelist rows on agreement: {1} but user is not pricelist approver", new Object[] {userAPI.getId(), agreement.getUniqueId()});
            throw generateForbiddenException();
        }
        
        List<AgreementPricelistRow> pricelistRows = new ArrayList<>();
        for( AgreementPricelistRowAPI pricelistRowAPI : pricelistRowAPIs ) {
            AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPI.getId(), userAPI);
            if( pricelistRow == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning null.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                return null;
            }
            if( pricelistRow.getPricelist().getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to decline inactivate pricelist row: {1}  where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelistRow.getUniqueId()});
                throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");   
            }
            if( !AgreementPricelistRow.Status.PENDING_INACTIVATION.equals(pricelistRow.getStatus()) ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to decline inactivate pricelist row: {1} which has wrong status: {2}", new Object[] {userAPI.getId(), pricelistRow.getUniqueId(), pricelistRow.getStatus()});
                throw validationMessageService.generateValidationException("status", "pricelistrow.declineInactivateRows.wrongStatus");   
            }
            pricelistRow.setStatus(AgreementPricelistRow.Status.ACTIVE);
            pricelistRows.add(pricelistRow);
        }
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            em.merge(pricelistRow);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));                        
        }
        // TODO: send message to supplier
        return AgreementPricelistRowMapper.map(pricelistRows, true, false);
    }

    /**
     * Decline inactivation of the specified pricelist rows. The customer can do
     * this and only if each row is in status PENDING_INACTIVATION. An
     * inactive pricelist row cannot change status
     *
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist
     * @param pricelistRowAPIs list of rows to send to decline inactivation
     * @param userAPI user session information
     * @param sessionId
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * a rows has the wrong status
     */
    public List<AgreementPricelistRowAPI> declineInactivateRowsComment(String comment, long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, List<AgreementPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException, HjalpmedelstjanstenException {
        LOG.log(Level.FINEST, "declineInactivateRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});
        if( pricelistRowAPIs == null || pricelistRowAPIs.isEmpty() ) {
            return null;
        }

        Agreement agreement = agreementController.getAgreement(organizationUniqueId, agreementUniqueId, userAPI);

        // organizations/business levels (customers) that the agreement is shared
        // with can read the agreement, but only the customer can edit it
        if( !agreement.getCustomer().getUniqueId().equals(organizationUniqueId) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to decline inactivate row on agreement: {1}, but user organization: {2} is not customer", new Object[] {userAPI.getId(), agreement.getUniqueId(), organizationUniqueId});
            return null;
        }

        // the logged in user must be set as pricelist approver in order to decline rows
        if( !userIsPricelistApprover(organizationUniqueId, agreement, userAPI) ) {
            LOG.log( Level.WARNING, "Attempt by user: {0} to decline pricelist rows on agreement: {1} but user is not pricelist approver", new Object[] {userAPI.getId(), agreement.getUniqueId()});
            throw generateForbiddenException();
        }

        List<AgreementPricelistRow> pricelistRows = new ArrayList<>();
        for( AgreementPricelistRowAPI pricelistRowAPI : pricelistRowAPIs ) {
            AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPI.getId(), userAPI);
            if( pricelistRow == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning null.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                return null;
            }
            if( pricelistRow.getPricelist().getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to decline inactivate pricelist row: {1}  where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelistRow.getUniqueId()});
                throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");
            }
            if( !AgreementPricelistRow.Status.PENDING_INACTIVATION.equals(pricelistRow.getStatus()) ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to decline inactivate pricelist row: {1} which has wrong status: {2}", new Object[] {userAPI.getId(), pricelistRow.getUniqueId(), pricelistRow.getStatus()});
                throw validationMessageService.generateValidationException("status", "pricelistrow.declineInactivateRows.wrongStatus");
            }
            pricelistRow.setStatus(AgreementPricelistRow.Status.ACTIVE);
            pricelistRows.add(pricelistRow);
        }
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            em.merge(pricelistRow);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));
        }

        // send mail to supplier PELLE
        try
        {
            List<UserRole.RoleName> roleNames = new ArrayList<>();
            roleNames.add(UserRole.RoleName.SupplierAgreementManager);
            List<UserAPI> supplierAgreementManager = null;
            if(userController != null && agreement.getSupplier() != null && agreement.getSupplier().getUniqueId() != null) {
                supplierAgreementManager = userController.getUsersInRolesOnOrganization(agreement.getSupplier().getUniqueId(), roleNames);
            }

            if(supplierAgreementManager != null && supplierAgreementManager.size() != 0)
            {
                LOG.log( Level.FINEST, "{0} inactivated row(s) declined, send mail", new Object[] {pricelistRows.size()});
                String priceListNumber = "-";
                if( pricelistRows.size() > 0 && pricelistRows.get(0) != null) {
                    priceListNumber = pricelistRows.get(0).getPricelist().getNumber();
                }
                String selectedSubject = rowsSentDeclinedInactivateMailSubject;

                String selectedBody = rowsSentDeclinedInactivateMailBody;
                if (pricelistRows.size() == 1) {
                    selectedSubject = rowsSentDeclinedInactivateMailSubject_single;
                    selectedBody = rowsSentDeclinedInactivateMailBody_single;
                }

                StringBuilder rowsFormatted = new StringBuilder();
                for (AgreementPricelistRow pricelistRow : pricelistRows)
                {
                    rowsFormatted.append("Artikelnummer: " + pricelistRow.getArticle().getArticleNumber() + "<br/>");
                }

                String mailBody   = String.format(selectedBody, pricelistRows.size(), agreement.getAgreementName(),  agreement.getValidFrom(), agreement.getAgreementNumber(), priceListNumber, agreement.getCustomer().getOrganizationName(), rowsFormatted.toString());

                if (comment.length() != 0)
                    mailBody = "Kommentar från kunden:<br> " + comment.replace("QQQ","<br>").replace("@@@","/").replace("£££","\\") + "<br><br>" + mailBody;
                for (UserAPI agreementManager : supplierAgreementManager) {
                    if(agreementManager.getElectronicAddress() != null && agreementManager.getElectronicAddress().getEmail() != null && !agreementManager.getElectronicAddress().getEmail().equals("")) {
                        emailController.send(agreementManager.getElectronicAddress().getEmail(), selectedSubject, mailBody);
                    }
                }
            }

        } catch (MessagingException ex) {
            LOG.log(Level.SEVERE, "Failed to send message on rows, decline inactivate pricelist rows", ex);
        }

        return AgreementPricelistRowMapper.map(pricelistRows, true, false);
    }

    /**
     * Reopen the specified pricelist rows. Only if each row is in status INACTIVE. 
     * An inactive pricelist row cannot change status
     * 
     * @param organizationUniqueId unique id of the organization
     * @param agreementUniqueId unique id of the agreement of the pricelist
     * @param pricelistUniqueId unique id of the pricelist 
     * @param pricelistRowAPIs list of rows to reopen
     * @param userAPI user session information
     * @param sessionId
     * @return a list of updated rows
     * @throws HjalpmedelstjanstenValidationException if validation fails, e.g.
     * a rows has the wrong status
     */
    public List<AgreementPricelistRowAPI> reopenInactivatedRows(long organizationUniqueId, long agreementUniqueId, long pricelistUniqueId, List<AgreementPricelistRowAPI> pricelistRowAPIs, UserAPI userAPI, String sessionId, String requestIp) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "reopenInactivatedRows( organizationUniqueId: {0}, agreementUniqueId: {1}, pricelistUniqueId: {2} )", new Object[] {organizationUniqueId, agreementUniqueId, pricelistUniqueId});        
        if( pricelistRowAPIs == null || pricelistRowAPIs.isEmpty() ) {
            return null;
        }
        List<AgreementPricelistRow> pricelistRows = new ArrayList<>();
        for( AgreementPricelistRowAPI pricelistRowAPI : pricelistRowAPIs ) {
            AgreementPricelistRow pricelistRow = getPricelistRow(organizationUniqueId, agreementUniqueId, pricelistUniqueId, pricelistRowAPI.getId(), userAPI);
            if( pricelistRow == null ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to access pricelist row: {1} that does not exist. Returning null.", new Object[] {userAPI.getId(), pricelistRowAPI.getId()});
                return null;
            }
            if( pricelistRow.getPricelist().getAgreement().getStatus() == Agreement.Status.DISCONTINUED ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to reopen inactivated pricelist row: {1}  where agreement is DISCONTINUED", new Object[] {userAPI.getId(), pricelistRow.getUniqueId()});
                throw validationMessageService.generateValidationException("status", "agreement.status.discontinued");   
            }
            if( !AgreementPricelistRow.Status.INACTIVE.equals(pricelistRow.getStatus()) ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to reopen inactivated pricelist row: {1} which has wrong status: {2}", new Object[] {userAPI.getId(), pricelistRow.getUniqueId(), pricelistRow.getStatus()});
                throw validationMessageService.generateValidationException("status", "pricelistrow.reopenInactivatedRows.wrongStatus");   
            }
            if( pricelistRow.getArticle().getStatus() == Product.Status.DISCONTINUED ) {
                LOG.log( Level.WARNING, "Attempt by user: {0} to reopen pricelist row: {1} with discontinued article}", new Object[] {userAPI.getId(), pricelistRow.getUniqueId()});
                throw validationMessageService.generateValidationException("status", "pricelistrow.reopenInactivatedRows.articleDiscontinued");   
            }
            pricelistRow.setStatus(AgreementPricelistRow.Status.CREATED);
            pricelistRows.add(pricelistRow);
        }
        for( AgreementPricelistRow pricelistRow : pricelistRows ) {
            em.merge(pricelistRow);
            internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.AGREEMENT_PRICELIST_ROW, InternalAudit.ActionType.UPDATE, userAPI.getId(), sessionId, pricelistRow.getUniqueId(), requestIp));                        
        }
        // TODO: send message to supplier
        return AgreementPricelistRowMapper.map(pricelistRows, true, false);
    }
    
    /**
     * When an article is discontinued, all rows on all agreements where the 
     * given article is referred must be inactivated
     * 
     * @param article the article to find rows to inactivate by
     */
    public void handleRowsWhereArticleIsDiscontinued( Article article ) {
        LOG.log(Level.FINEST, "handleRowsWhereArticleIsDiscontinued( article->uniqueId: {0} )", new Object[] {article.getUniqueId()});
        List<AgreementPricelistRow> agreementPricelistRows = 
                em.createNamedQuery(AgreementPricelistRow.FIND_BY_ARTICLE).
                setParameter("articleUniqueId", article.getUniqueId()).
                getResultList();
        if( agreementPricelistRows != null && !agreementPricelistRows.isEmpty() ) {
            agreementPricelistRows.forEach((agreementPricelistRow) -> {
                // only update row if it's pricelist is current or future. this is not trivial to find out
                AgreementPricelist currentPricelist = agreementPricelistController.getCurrentPricelist(agreementPricelistRow.getPricelist().getAgreement().getUniqueId());
                AgreementPricelist.Status pricelistStatus = AgreementPricelistMapper.getPricelistStatus(agreementPricelistRow.getPricelist(), currentPricelist);
                AgreementPricelistRow.Status status = agreementPricelistRow.getStatus();
                if( pricelistStatus != AgreementPricelist.Status.PAST && status == AgreementPricelistRow.Status.ACTIVE ) {
                    agreementPricelistRow.setStatus(AgreementPricelistRow.Status.PENDING_INACTIVATION);
                    //agreementPricelistRow.setPrice(BigDecimal.ZERO); (1711)

                    //här borde vi skicka mail till kund
                    try{
                        Agreement agreement = agreementPricelistRow.getPricelist().getAgreement();
                        if( agreement.getCustomerPricelistApprovers() != null && !agreement.getCustomerPricelistApprovers().isEmpty() ) {
                            LOG.log( Level.FINEST, "{0} rows (total) sent for inactivation, send mail", new Object[] {agreementPricelistRows.size()});
                            String selectedSubject = rowsSentForInactivationMailSubjectSingle;
                            String selectedBody = rowsSentForInactivationMailBodySingle;
                            String priceListNumber = "-";
                            if( agreementPricelistRows.size() > 0 && agreementPricelistRows.get(0) != null) {
                                //priceListNumber = agreementPricelistRows.get(0).getPricelist().getNumber();
                                priceListNumber = agreementPricelistRow.getPricelist().getNumber();
                            }
                            //String mailBody = String.format(selectedBody, agreementPricelistRows.size(), agreement.getAgreementName(), agreement.getAgreementNumber(),  priceListNumber, agreement.getSupplier().getOrganizationName());
                            String mailBody = String.format(selectedBody, 1, agreement.getAgreementName(), agreement.getAgreementNumber(),  priceListNumber, agreement.getSupplier().getOrganizationName());
                            for( UserEngagement userEngagement : agreement.getCustomerPricelistApprovers() ) {
                                emailController.send(userEngagement.getUserAccount().getElectronicAddress().getEmail(), selectedSubject, mailBody);
                            }
                        }

                    } catch (MessagingException ex) {
                        LOG.log(Level.SEVERE, "Failed to send message on rows sent for inactivation", ex);
                    }
                }
            });
        }
    }
    
    public boolean existRowsByArticle( List<Long> articleUniqueIds ) {
        Long count = (Long) em.createNamedQuery(AgreementPricelistRow.COUNT_BY_ARTICLES).
                setParameter("articleUniqueIds", articleUniqueIds).
                getSingleResult();
        if( count != null && count > 0 ) {
            return true;
        }
        return false;
    }
    
    /**
     * When an article is changed in its "essence" (like switches based on product)
     * all references to that article must be removed.
     * 
     * @param article the article to find rows for to remove
     */
    public void deleteRowsByArticle( Article article ) {
        LOG.log(Level.FINEST, "deleteRowsByArticle( article->uniqueId: {0} )", new Object[] {article.getUniqueId()});
        List<AgreementPricelistRow> agreementPricelistRows = 
                em.createNamedQuery(AgreementPricelistRow.FIND_BY_ARTICLE).
                setParameter("articleUniqueId", article.getUniqueId()).
                getResultList();
        if( agreementPricelistRows != null && !agreementPricelistRows.isEmpty() ) {
            Iterator<AgreementPricelistRow> it = agreementPricelistRows.iterator();
            while( it.hasNext() ) {
                em.remove(it.next());
            }
        }
    }
    
    /**
     * Get customer pricelist rows by article
     * 
     * @param organization
     * @param articleUniqueId
     * @return
     */
    public List<AgreementPricelistRow> getOrganizationPricelistRowsByArticle(Organization organization, long articleUniqueId, UserAPI userAPI) {
        LOG.log(Level.FINEST, "getOrganizationPricelistRowsByArticle( organization -> uniqueId: {0}, articleUniqueId: {1} )", new Object[] {organization.getUniqueId(), articleUniqueId});
        // customers and suppliers should get rows in different statuses
        List<AgreementPricelistRow> agreementPricelistRows = null;
        if( organization.getOrganizationType() == Organization.OrganizationType.CUSTOMER ) {
            // include all statuses except CREATED and CHANGED
            List<AgreementPricelistRow.Status> statuses = new ArrayList<>();
            statuses.add(AgreementPricelistRow.Status.ACTIVE);
            statuses.add(AgreementPricelistRow.Status.PENDING_APPROVAL);
            List<Long> userBusinessLevelIds = agreementController.getUserBusinessLevelIds(userAPI, organization.getUniqueId());
            if( userBusinessLevelIds == null || userBusinessLevelIds.isEmpty() ) {
                // user IS NOT limited by business levels
                agreementPricelistRows = em.createNamedQuery(AgreementPricelistRow.FIND_BY_CUSTOMER_AND_ARTICLE).
                        setParameter("organizationUniqueId", organization.getUniqueId()).
                        setParameter("articleUniqueId", articleUniqueId).
                        setParameter("statuses", statuses).
                        getResultList();
            } else {
                // user IS limited by business levels
                agreementPricelistRows = em.createNamedQuery(AgreementPricelistRow.FIND_BY_CUSTOMER_BUSINESS_LEVELS_AND_ARTICLE).
                        setParameter("businessLevelIds", userBusinessLevelIds).
                        setParameter("articleUniqueId", articleUniqueId).
                        setParameter("statuses", statuses).
                        getResultList();
            }
        } else if( organization.getOrganizationType() == Organization.OrganizationType.SUPPLIER ) {
            agreementPricelistRows = em.createNamedQuery(AgreementPricelistRow.FIND_BY_SUPPLIER_AND_ARTICLE).
                    setParameter("organizationUniqueId", organization.getUniqueId()).
                    setParameter("articleUniqueId", articleUniqueId).
                    getResultList();
        } else if( organization.getOrganizationType() == Organization.OrganizationType.SERVICE_OWNER) {
            agreementPricelistRows = em.createNamedQuery(AgreementPricelistRow.FIND_BY_ARTICLE).
                    setParameter("articleUniqueId", articleUniqueId).
                    getResultList();
        }
        if( agreementPricelistRows != null ) {
            LOG.log(Level.FINEST, "agreementPricelistRows.size() {0}", new Object[] {agreementPricelistRows.size()});
            if (organization.getOrganizationType() != Organization.OrganizationType.SERVICE_OWNER) {
                Iterator<AgreementPricelistRow> agreementPricelistRowIterator = agreementPricelistRows.iterator();
                while (agreementPricelistRowIterator.hasNext()) {
                    AgreementPricelistRow agreementPricelistRow = agreementPricelistRowIterator.next();
                    AgreementPricelist currentPricelistForAgreement = agreementPricelistController.getCurrentPricelist(agreementPricelistRow.getPricelist().getAgreement().getUniqueId());
                    if (currentPricelistForAgreement == null || !currentPricelistForAgreement.equals(agreementPricelistRow.getPricelist())) {
                        agreementPricelistRowIterator.remove();
                    }
                }
            }
        }
        return agreementPricelistRows;
    }
    
    /**
     * Sets overridden warranty quantity unit if it is set by the user. 
     * 
     * @param pricelistRow the row to update
     * @param pricelistRowAPI user supplied data
     * @throws HjalpmedelstjanstenValidationException if unit is set but does not exist
     */
    void setOverriddenWarrantyQuantityUnit(AgreementPricelistRow pricelistRow, AgreementPricelistRowAPI pricelistRowAPI, boolean isCreate) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "setOverriddenWarrantyQuantityUnit( ... )");
        if( isCreate && pricelistRowAPI.getWarrantyQuantityUnit() == null ) {
            // do nothing, default to inherit
        } else {
            if( pricelistRow.isWarrantyQuantityUnitOverridden() ) {
                if( pricelistRowAPI.getWarrantyQuantityUnit() != null ) {
                    CVGuaranteeUnit unit = guaranteeUnitController.getGuaranteeUnit(pricelistRowAPI.getWarrantyQuantityUnit().getId());
                    if( unit == null ) {
                        LOG.log(Level.FINEST, "cannot set warranty quantity unit: {0} for pricelist row since it does not exist", new Object[] {pricelistRowAPI.getWarrantyQuantityUnit().getId()});
                        throw validationMessageService.generateValidationException("warrantyQuantityUnit", "pricelistrow.warrantyQuantityUnit.notExist");
                    }
                    pricelistRow.setWarrantyQuantityUnit(unit);
                } else {
                    pricelistRow.setWarrantyQuantityUnit(null);
                }
            } else {
                Article article = pricelistRow.getArticle();
                Article.Type articleType = article.getBasedOnProduct() == null ? article.getCategory().getArticleType(): article.getBasedOnProduct().getCategory().getArticleType();

                Agreement agreement = em.find(Agreement.class, pricelistRow.getPricelist().getAgreement().getUniqueId());

                boolean overridden = false;
                if( pricelistRowAPI.getWarrantyQuantityUnit() == null || pricelistRowAPI.getWarrantyQuantityUnit().getId() == null) {
                    // no unit sent in API, check whether it is different from agreement
                    if( articleType == Article.Type.H && pricelistRow.getPricelist().getAgreement().getWarrantyQuantityHUnit() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.I && agreement.getWarrantyQuantityIUnit() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.R && agreement.getWarrantyQuantityRUnit() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.T && agreement.getWarrantyQuantityTUnit() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.Tj && agreement.getWarrantyQuantityTJUnit() != null ) {
                        overridden = true;
                    }
                    if( overridden ) {
                        pricelistRow.setWarrantyQuantityUnit(null);
                        pricelistRow.setWarrantyQuantityUnitOverridden(true);
                    }
                } else {
                    // unit sent in API
                    if( articleType == Article.Type.H ) {
                        if( agreement.getWarrantyQuantityHUnit() == null ||
                                !agreement.getWarrantyQuantityHUnit().getUniqueId().equals(pricelistRowAPI.getWarrantyQuantityUnit().getId()) ) {
                            overridden = true;
                        }
                    } else if( articleType == Article.Type.R ) {
                        if( agreement.getWarrantyQuantityRUnit() == null ||
                                !agreement.getWarrantyQuantityRUnit().getUniqueId().equals(pricelistRowAPI.getWarrantyQuantityUnit().getId()) ) {
                            overridden = true;
                        }
                    } else if( articleType == Article.Type.I ) {
                        if( agreement.getWarrantyQuantityIUnit() == null ||
                                !agreement.getWarrantyQuantityIUnit().getUniqueId().equals(pricelistRowAPI.getWarrantyQuantityUnit().getId()) ) {
                            overridden = true;
                        }
                    } else if( articleType == Article.Type.T ) {
                        if( agreement.getWarrantyQuantityTUnit() == null ||
                                !agreement.getWarrantyQuantityTUnit().getUniqueId().equals(pricelistRowAPI.getWarrantyQuantityUnit().getId()) ) {
                            overridden = true;
                        }
                    } else if( articleType == Article.Type.Tj ) {
                        if( agreement.getWarrantyQuantityTJUnit() == null ||
                                !agreement.getWarrantyQuantityTJUnit().getUniqueId().equals(pricelistRowAPI.getWarrantyQuantityUnit().getId()) ) {
                            overridden = true;
                        }
                    }
                    if( overridden ) {
                        CVGuaranteeUnit unit = guaranteeUnitController.getGuaranteeUnit(pricelistRowAPI.getWarrantyQuantityUnit().getId());
                        if( unit == null ) {
                            LOG.log(Level.FINEST, "cannot set warranty quantity unit: {0} for pricelist row since it does not exist", new Object[] {pricelistRowAPI.getWarrantyQuantityUnit().getId()});
                            throw validationMessageService.generateValidationException("warrantyQuantityUnit", "pricelistrow.warrantyQuantityUnit.notExist");
                        }
                        pricelistRow.setWarrantyQuantityUnit(unit);
                        pricelistRow.setWarrantyQuantityUnitOverridden(true);
                    }
                }
            }
        }
    }

    public List<Long> getAgreementPricelistArticleUniqueIds(Long pricelistUniqueId) {
        LOG.log(Level.FINEST, "getAgreementPricelistArticleUniqueIds( pricelistUniqueId: {0})", new Object[] {pricelistUniqueId});
        List<Long> ids = em.createNamedQuery(AgreementPricelistRow.GET_ARTICLE_UNIQUE_IDS).
                setParameter("pricelistUniqueId", pricelistUniqueId).
                getResultList();
        return ids;
    }
    
    public List<String> getAgreementPricelistArticleUniqueNumbers(Long pricelistUniqueId) {
        LOG.log(Level.FINEST, "getAgreementPricelistArticleUniqueNumbers( pricelistUniqueId: {0})", new Object[] {pricelistUniqueId});
        List<String> ids = em.createNamedQuery(AgreementPricelistRow.GET_ARTICLE_UNIQUE_NUMBERS).
                setParameter("pricelistUniqueId", pricelistUniqueId).
                getResultList();
        return ids;
    }

    public List<Long> getAgreementPricelistRowUniqueId(Long articleUniqueId, Long pricelistUniqueId) {
        LOG.log(Level.FINEST, "getAgreementPricelistRowUniqueId( articleUniqueId: {0}, pricelistUniqueId: {1})", new Object[] {articleUniqueId, pricelistUniqueId});
        List<Long> id = em.createNamedQuery(AgreementPricelistRow.GET_PRICELISTROW_UNIQUE_ID_BY_PRICELIST_AND_ARTICLE).
                setParameter("articleUniqueId", articleUniqueId).
                setParameter("pricelistUniqueId", pricelistUniqueId).
                getResultList();
        return id;
    }

    
    /**
     * Get the total number of rows on all pricelists in agreement
     * 
     * @param agreementUniqueId the unique id of the agreement
     * @return the total number of rows in the agreement
     */
    public long getNumberOfRowsOnAgreement(long agreementUniqueId) {
        Long count = (Long) em.createNamedQuery(AgreementPricelistRow.COUNT_BY_AGREEMENT).
                setParameter("agreementUniqueId", agreementUniqueId).
                getSingleResult();
        return count == null ? 0: count;
    }

    
    private void setOverrideWarrantyValidFrom(AgreementPricelistRowAPI pricelistRowAPI, AgreementPricelistRow pricelistRow, Article.Type articleType, UserAPI userAPI, boolean isCreate) throws HjalpmedelstjanstenValidationException {
        if( isCreate && pricelistRowAPI.getWarrantyValidFrom() == null ) {
            // do nothing, default to inherit
        } else {
            if( pricelistRow.isWarrantyValidFromOverridden() ) {
                if( pricelistRowAPI.getWarrantyValidFrom() != null ) {
                    CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(pricelistRowAPI.getWarrantyValidFrom().getCode());
                    if( preventiveMaintenance == null ) {
                        LOG.log(Level.WARNING, "attempt by user: {0} to set preventive maintenance with code: {1} which does not exist", new Object[] {userAPI.getId(), pricelistRowAPI.getWarrantyValidFrom()});
                        throw validationMessageService.generateValidationException("warrantyValidFrom", "pricelistrow.warrantyValidFrom.notExist");
                    }
                    pricelistRow.setWarrantyValidFrom(preventiveMaintenance);
                } else {
                    pricelistRow.setWarrantyValidFrom(null);
                }
            } else {
                boolean overridden = false;
                if( pricelistRowAPI.getWarrantyValidFrom() != null ) {
                    CVPreventiveMaintenance preventiveMaintenance = cVPreventiveMaintenanceController.findByCode(pricelistRowAPI.getWarrantyValidFrom().getCode());
                    if( preventiveMaintenance == null ) {
                        LOG.log(Level.WARNING, "attempt by user: {0} to set preventive maintenance with code: {1} which does not exist", new Object[] {userAPI.getId(), pricelistRowAPI.getWarrantyValidFrom()});
                        throw validationMessageService.generateValidationException("warrantyValidFrom", "pricelistrow.warrantyValidFrom.notExist");
                    }

                    Agreement agreement = em.find(Agreement.class, pricelistRow.getPricelist().getAgreement().getUniqueId());
                    //agreement.getWarrantyQuantityHUnit().getUniqueId();

                    if( articleType == Article.Type.H ) {

                        if( agreement.getWarrantyValidFromH() == null ) {
                            overridden = true;
                        } else {

                            if( !preventiveMaintenance.getUniqueId().equals(agreement.getWarrantyValidFromH().getUniqueId()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.I ) {
                        if( agreement.getWarrantyValidFromI() == null ) {
                            overridden = true;
                        } else {
                            if( !preventiveMaintenance.getUniqueId().equals(agreement.getWarrantyValidFromI().getUniqueId()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.R ) {
                        if( agreement.getWarrantyValidFromR() == null ) {
                            overridden = true;
                        } else {
                            if( !preventiveMaintenance.getUniqueId().equals(agreement.getWarrantyValidFromR().getUniqueId()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.T ) {
                        if( agreement.getWarrantyValidFromT() == null ) {
                            overridden = true;
                        } else {
                            if( !preventiveMaintenance.getUniqueId().equals(agreement.getWarrantyValidFromT().getUniqueId()) ) {
                                overridden = true;
                            }
                        }
                    } else if( articleType == Article.Type.Tj ) {
                        if( agreement.getWarrantyValidFromTJ() == null ) {
                            overridden = true;
                        } else {
                            if( !preventiveMaintenance.getUniqueId().equals(agreement.getWarrantyValidFromTJ().getUniqueId()) ) {
                                overridden = true;
                            }
                        }
                    }
                    if( overridden ) {
                        pricelistRow.setWarrantyValidFrom(preventiveMaintenance);
                        pricelistRow.setWarrantyValidFromOverridden(true);
                    }
                } else {
                    if( articleType == Article.Type.H && pricelistRow.getPricelist().getAgreement().getWarrantyValidFromH() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.I && pricelistRow.getPricelist().getAgreement().getWarrantyValidFromI() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.R && pricelistRow.getPricelist().getAgreement().getWarrantyValidFromR() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.T && pricelistRow.getPricelist().getAgreement().getWarrantyValidFromT() != null ) {
                        overridden = true;
                    } else if( articleType == Article.Type.Tj && pricelistRow.getPricelist().getAgreement().getWarrantyValidFromTJ() != null ) {
                        overridden = true;
                    }
                    if( overridden ) {
                        pricelistRow.setWarrantyValidFrom(null);
                        pricelistRow.setWarrantyValidFromOverridden(true);
                    }
                }
            }
        }
    }

    private boolean userIsPricelistApprover(long organizationUniqueId, Agreement agreement, UserAPI userAPI) {
        List<UserEngagement> userEngagements = agreement.getCustomerPricelistApprovers();
        UserEngagementAPI userEngagementAPI = UserController.getUserEngagementAPIByOrganizationId(organizationUniqueId, userAPI.getUserEngagements());
        for( UserEngagement userEngagement : userEngagements ) {
            if( userEngagement.getUniqueId().equals(userEngagementAPI.getId()) ) {
                return true;
            }
        }
        return false;
    }

}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.business.security.view;

import java.io.IOException;
import java.util.logging.Level;
import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.LoggerService;

/**
 * If a REST API requires some form of permission, the request will pass 
 * thru this and be validated. If the user lacks the correct permission, HTTP 
 * response code 403 is returned.
 * 
 * @see se.inera.hjalpmedelstjansten.business.security.view.AuthenticationFeature
 * @see se.inera.hjalpmedelstjansten.business.security.view.SecuredService
 * @author Tommy Berglund
 */
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {
    
    private static final HjmtLogger LOG = LoggerService.getLogger(AuthenticationFilter.class.getName());
    
    private final String[] requiresPermissions;
    
    public AuthenticationFilter(String[] requiresPermissions) {
        this.requiresPermissions = requiresPermissions;
    }
    
    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        Subject subject = SecurityUtils.getSubject();
        if( subject == null || !subject.isAuthenticated() ) {
            LOG.log( Level.FINEST, "User is not authenticated." );
            requestContext.abortWith(
                    Response.status(Response.Status.UNAUTHORIZED)
                    .build()
                );
        } else {
            boolean permissionFound = false;
            for( String requiresPermission: requiresPermissions ) {
                if( subject.isPermitted(requiresPermission) ) {
                    permissionFound = true;
                    break;
                }                
            }
            if( !permissionFound ) {
                LOG.log( Level.FINEST, "User is not permitted." );
                requestContext.abortWith(
                    Response.status(Response.Status.FORBIDDEN)
                    .build()
                );
            }
        }
    }
    
}

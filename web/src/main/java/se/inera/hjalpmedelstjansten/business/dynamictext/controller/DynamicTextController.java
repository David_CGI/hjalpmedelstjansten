/*
* Copyright (C) 2019 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.dynamictext.controller;

import se.inera.hjalpmedelstjansten.business.BaseController;
import se.inera.hjalpmedelstjansten.business.HjalpmedelstjanstenValidationException;
import se.inera.hjalpmedelstjansten.business.assortment.controller.AssortmentValidation;
import se.inera.hjalpmedelstjansten.business.assortment.controller.CountyController;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.business.organization.controller.OrganizationController;
import se.inera.hjalpmedelstjansten.business.product.controller.ArticleController;
import se.inera.hjalpmedelstjansten.business.property.view.ValidationMessageService;
import se.inera.hjalpmedelstjansten.business.user.controller.UserController;
import se.inera.hjalpmedelstjansten.model.api.DynamicTextAPI;
import se.inera.hjalpmedelstjansten.model.entity.*;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.logging.Level;

/**
 * Class for handling business logic of dynamic text. This includes talking to the
 * database. Only service owner can handle dynamic text.
 * 
 * @author Per Abrahamsson
 */
@Stateless
public class DynamicTextController extends BaseController {
    
    @Inject
    HjmtLogger LOG;
    
    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;
    
    @Inject
    AssortmentValidation assortmentValidation;
    
    @Inject
    ValidationMessageService validationMessageService;

    @Inject
    UserController userController;
    
    @Inject
    OrganizationController organizationController;
    
    @Inject
    CountyController countyController;
    
    @Inject
    ArticleController articleController;

    @Inject
    Event<InternalAuditEvent> internalAuditEvent;

    
    /**
     * Get the <code>Dynamic Text</code> with the given unique id.
     * 
     * @param uniqueId unique id of the dynamic text
     * @return the corresponding <code>Dynamic Text</code>
     */

    public DynamicText getDynamicText(long uniqueId) {
        LOG.log(Level.FINEST, "getDynamicText(...)");

        StringBuilder querySql = new StringBuilder();
        querySql.append("SELECT dt FROM DynamicText dt ");
        querySql.append("WHERE dt.uniqueId = :uniqueId");

        String queryString = querySql.toString();
        LOG.log( Level.FINEST, "queryString: {0}", new Object[] {queryString} );
        Query query = em.createQuery(queryString);
        query.setParameter("uniqueId", uniqueId);

        DynamicText dynamicText = (DynamicText) query.getSingleResult();

        return dynamicText;
    }



    public DynamicTextAPI updateDynamicTextX(long uniqueId, DynamicTextAPI dynamicTextAPI) throws HjalpmedelstjanstenValidationException {
        LOG.log(Level.FINEST, "updateDynamicText( uniqueId: {0} )", new Object[] { uniqueId});
        //UserEngagement dbUserEngagement = getUserEngagement(organizationUniqueId, uniqueId);
        DynamicText dbDynamicText = getDynamicText(uniqueId);
        if( dbDynamicText == null ) {
            return null;
        }
        /*
        userValidation.validate( userAPI, true, dbUserEngagement );
        if( isAdmin ) {
            updateUserBasics(dbUserEngagement, userAPI);
            UserEngagementAPI userEngagementAPI = userAPI.getUserEngagements().get(0);
            setUserRoles(userEngagementAPI, dbUserEngagement, isSuperAdmin);
        }
        */

        updateDynamicTextZ(dbDynamicText, dynamicTextAPI);
        //internalAuditEvent.fire(new InternalAuditEvent(new Date(), InternalAudit.EntityType.USER, InternalAudit.ActionType.UPDATE, sessionUserAPI.getId(), sessionId, uniqueId, requestIp));
        //return UserMapper.map( dbUserEngagement, true );
        return dynamicTextAPI;
    }

    /**
     * Update DynamicText.
     *
     * @param dbDynamicText the DynamicText to update
     * @param dynamicTextAPI the user supplied data
     */
    private void updateDynamicTextZ(DynamicText dbDynamicText, DynamicTextAPI dynamicTextAPI) {
        LOG.log(Level.FINEST, "updateDynamicText( ... )");
        if( dynamicTextAPI.getDynamicText() != null ) {
            dbDynamicText.setDynamicText(dynamicTextAPI.getDynamicText());
            dbDynamicText.setChangedBy(dynamicTextAPI.getChangedBy());
            dbDynamicText.setUniqueId(dynamicTextAPI.getUniqueId());
            dbDynamicText.setWhenChanged(dynamicTextAPI.getWhenChanged());


        }
    }
}

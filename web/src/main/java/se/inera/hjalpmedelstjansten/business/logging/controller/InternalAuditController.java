/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.logging.controller;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.business.logging.view.InternalAuditEvent;
import se.inera.hjalpmedelstjansten.model.entity.InternalAudit;

/**
 *
 * @author Tommy Berglund
 */
@Stateless
public class InternalAuditController {
    
    @Inject
    private HjmtLogger LOG;
    
    @PersistenceContext( unitName = "HjmtjPU")
    private EntityManager em;
    
    private void saveInternalAudit(InternalAudit internalAudit) {
        LOG.log( Level.FINEST, "saveInternalAudit( internalAudit: {0} )", new Object[] {internalAudit});
        em.persist(internalAudit);
    }
    
    public void removeInternalAuditsOlderThan(int internalAuditMaxAgeMonths) {
        LOG.log( Level.FINEST, "removeInternalAuditsOlderThan( internalAuditMaxAgeMonths: {0} )", new Object[] {internalAuditMaxAgeMonths});
        Date date = new Date();
        LocalDateTime localDateTime = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        localDateTime = localDateTime.minusMonths(internalAuditMaxAgeMonths);
        date = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        LOG.log( Level.FINEST, "Remove logs older than: {0}", new Object[] {date});
        em.createQuery("DELETE FROM InternalAudit ia WHERE ia.auditTime < :date").
                setParameter("date", date).
                executeUpdate();
    }
    
    @Asynchronous
    public void handleInternalAuditEvent(@Observes InternalAuditEvent internalAuditEvent) {
        LOG.log( Level.FINEST, "handleInternalAuditEvent( internalAuditEvent: {0} )", new Object[] {internalAuditEvent});
        // do not save views, it's only for audit logging, not for saving
        if( internalAuditEvent.getActionType() != InternalAudit.ActionType.VIEW) {
            InternalAudit internalAudit = new InternalAudit();
            internalAudit.setAuditTime(internalAuditEvent.getAuditTime());
            internalAudit.setEntityType(internalAuditEvent.getEntityType());
            internalAudit.setActionType(internalAuditEvent.getActionType());
            internalAudit.setUserEngagementId(internalAuditEvent.getUserEngagementId());
            internalAudit.setRequestIp(internalAuditEvent.getRequestIp());
            internalAudit.setSessionId(internalAuditEvent.getSessionId());
            saveInternalAudit(internalAudit);
        }
        LOG.logAudit(
                internalAuditEvent.getUserEngagementId() == null ? null: internalAuditEvent.getUserEngagementId().toString(), 
                internalAuditEvent.getActionType() == null ? null: internalAuditEvent.getActionType().toString(), 
                internalAuditEvent.getEntityType() == null ? null: internalAuditEvent.getEntityType().toString(), 
                internalAuditEvent.getEntityId() == null ? null: internalAuditEvent.getEntityId().toString(),
                internalAuditEvent.getSessionId());
    }

}

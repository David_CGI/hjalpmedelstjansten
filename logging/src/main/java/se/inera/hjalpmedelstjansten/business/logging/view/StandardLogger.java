/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.logging.view;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author Tommy Berglund
 */
public class StandardLogger implements HjmtLogger {

    private Logger systemLogger;
    private Logger performanceLogger;
    private Logger auditLogger;
    
    StandardLogger(Logger systemLogger, Logger performanceLogger, Logger auditLogger) {
        this.systemLogger = systemLogger;  
        this.performanceLogger = performanceLogger;
        this.auditLogger = auditLogger;
    }

    @Override
    public void log(Level level, String message, Object[] params) {
        this.systemLogger.log(level, message, params);
    }

    @Override
    public void log(Level level, String message) {
        this.systemLogger.log(level, message);
    }

    @Override
    public void log(Level level, String message, Throwable t) {
        this.systemLogger.log(level, message, t);
    }
    
    @Override
    public void logPerformance(String className, String methodName, long duration, String sessionId) {
        this.performanceLogger.log(Level.INFO, "Class: {0}, Method: {1}, Duration: {2}, SessionId: {3}", new Object[] {className,methodName,duration,sessionId});
    }

    @Override
    public void logAudit(String who, String did, String toType, String toId, String sessionId) {
        this.auditLogger.log(Level.INFO, "who: {0}, did: {1}, toType: {2}, toId: {3}, sessionId: {4}", new Object[] {who, did, toType, toId, sessionId});
    }
    
}

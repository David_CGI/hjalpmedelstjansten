/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.business.logging.view;

import java.util.logging.Logger;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

/**
 *
 * @author Tommy Berglund
 */
@Singleton
@Startup
public class LoggerService {
    
    @Produces
    public HjmtLogger getLogger(InjectionPoint ip) {
        Class<?> aClass = ip.getMember().getDeclaringClass();
        Logger systemLogger = Logger.getLogger(aClass.getName());
        Logger performanceLogger = Logger.getLogger("se.inera.hjalpmedelstjansten.performance");
        Logger auditLogger = Logger.getLogger("se.inera.hjalpmedelstjansten.audit");
        return new StandardLogger(systemLogger, performanceLogger, auditLogger);
    }    
    
    public static HjmtLogger getLogger( String className ) {
        Logger logger = Logger.getLogger(className);
        Logger performanceLogger = Logger.getLogger("se.inera.hjalpmedelstjansten.performance");
        Logger auditLogger = Logger.getLogger("se.inera.hjalpmedelstjansten.audit");
        return new StandardLogger(logger, performanceLogger, auditLogger);
    }
    
}

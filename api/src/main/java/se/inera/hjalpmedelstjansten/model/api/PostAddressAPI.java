/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.model.api;

import se.inera.hjalpmedelstjansten.model.entity.*;
import java.io.Serializable;
import javax.validation.constraints.Size;

/**
 *
 * @author Tommy Berglund
 */
public class PostAddressAPI implements Serializable {
    
    private Long id;
    private PostAddress.AddressType addressType;
    @Size(max = 255, message = "{postAddress.streetAddress.wrongLength}")
    private String streetAddress;
    @Size(max = 255, message = "{postAddress.postCode.wrongLength}")
    private String postCode;
    @Size(max = 255, message = "{postAddress.city.wrongLength}")
    private String city;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PostAddress.AddressType getAddressType() {
        return addressType;
    }

    public void setAddressType(PostAddress.AddressType addressType) {
        this.addressType = addressType;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

}
/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.model.api;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Tommy Berglund
 */
public class ResourceSpecificPropertyAPI implements Serializable {
    
    private Long id;
    private CategorySpecificPropertyAPI property;
    private String textValue;
    private Long singleListValue;
    private List<Long> multipleListValue;
    private Double decimalValue;
    private Double intervalFromValue;
    private Double intervalToValue;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CategorySpecificPropertyAPI getProperty() {
        return property;
    }

    public void setProperty(CategorySpecificPropertyAPI property) {
        this.property = property;
    }

    public Double getDecimalValue() {
        return decimalValue;
    }

    public void setDecimalValue(Double decimalValue) {
        this.decimalValue = decimalValue;
    }

    public Double getIntervalFromValue() {
        return intervalFromValue;
    }

    public void setIntervalFromValue(Double intervalFromValue) {
        this.intervalFromValue = intervalFromValue;
    }

    public String getTextValue() {
        return textValue;
    }

    public void setTextValue(String textValue) {
        this.textValue = textValue;
    }

    public Double getIntervalToValue() {
        return intervalToValue;
    }

    public void setIntervalToValue(Double intervalToValue) {
        this.intervalToValue = intervalToValue;
    }

    public Long getSingleListValue() {
        return singleListValue;
    }

    public void setSingleListValue(Long singleListValue) {
        this.singleListValue = singleListValue;
    }    

    public List<Long> getMultipleListValue() {
        return multipleListValue;
    }

    public void setMultipleListValue(List<Long> multipleListValue) {
        this.multipleListValue = multipleListValue;
    }
    
}
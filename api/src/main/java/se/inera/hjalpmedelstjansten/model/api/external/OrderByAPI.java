/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.model.api.external;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 * 
 * @author Tommy Berglund
 */
public class OrderByAPI {
    
    private List<String> idPath;
    private String Ascending;

    @JsonProperty("IdPath")
    public List<String> getIdPath() {
        return idPath;
    }

    @JsonProperty("IdPath")
    public void setIdPath(List<String> idPath) {
        this.idPath = idPath;
    }

    @JsonProperty("Ascending")
    public String getAscending() {
        return Ascending;
    }

    @JsonProperty("Ascending")
    public void setAscending(String Ascending) {
        this.Ascending = Ascending;
    }

}

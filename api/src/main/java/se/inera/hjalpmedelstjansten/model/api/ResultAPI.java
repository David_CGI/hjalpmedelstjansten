/*
 *
 *  * Copyright (C) 2018 Inera AB (http://www.inera.se)
 *  *
 *  * This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
 *  *
 *  * Hjalpmedelstjansten is free software: you can redistribute it and/or modify
 *  * it under the terms of the GNU General Public License as published by
 *  * the Free Software Foundation, either version 3 of the License, or
 *  * (at your option) any later version.
 *  *
 *  * Hjalpmedelstjansten is distributed in the hope that it will be useful,
 *  * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  * GNU General Public License for more details.
 *  *
 *  * You should have received a copy of the GNU General Public License
 *  * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package se.inera.hjalpmedelstjansten.model.api;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class ResultAPI<T> {

    private List<T> elements = new LinkedList<>();

    private Set<ErrorMessageAPI> validationMessages = new HashSet<>();

    public void add(T element) {
        elements.add(element);
    }

    public void add(Set<ErrorMessageAPI> validationMessages) {
        this.validationMessages.addAll(validationMessages);
    }

    public List<T> getElements() {
        return elements;
    }

    public Set<ErrorMessageAPI> getValidationMessages() {
        return validationMessages;
    }
}

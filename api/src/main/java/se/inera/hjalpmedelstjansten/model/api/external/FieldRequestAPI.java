/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.model.api.external;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 * API request object
 * 
 * @author Tommy Berglund
 */
public class FieldRequestAPI {
    
    public static final String CREDENTIALS_PROPERTY_NAME = "Credentials";
    public static final String NODE_ID_PROPERTY_NAME = "NodeId";
    public static final String ID_PATH_LIST_PROPERTY_NAME = "IdPathList";
    
    private String credentials;
    private String nodeId;
    private List<List<String>> idPathList;

    @JsonProperty(CREDENTIALS_PROPERTY_NAME)
    public String getCredentials() {
        return credentials;
    }

    @JsonProperty(CREDENTIALS_PROPERTY_NAME)
    public void setCredentials(String credentials) {
        this.credentials = credentials;
    }

    @JsonProperty(NODE_ID_PROPERTY_NAME)
    public String getNodeId() {
        return nodeId;
    }

    @JsonProperty(NODE_ID_PROPERTY_NAME)
    public void setNodeId(String nodeId) {
        this.nodeId = nodeId;
    }

    @JsonProperty(ID_PATH_LIST_PROPERTY_NAME)
    public List<List<String>> getIdPathList() {
        return idPathList;
    }

    @JsonProperty(ID_PATH_LIST_PROPERTY_NAME)
    public void setIdPathList(List<List<String>> idPathList) {
        this.idPathList = idPathList;
    }

}

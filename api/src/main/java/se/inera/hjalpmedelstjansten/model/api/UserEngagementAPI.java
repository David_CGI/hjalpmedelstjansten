/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.model.api;

import java.io.Serializable;
import java.util.List;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

/**
 * Basic bean validation is in this file. More complex validations implemented
 * in business methods.
 * 
 * @author Tommy Berglund
 */
public class UserEngagementAPI implements Serializable {
    
    private Long id;
    
    @NotNull(message = "{userEngagement.organizationId.notNull}")
    private Long organizationId;
    
    @NotNull(message = "{userEngagement.validFrom.notNull}")
    @DecimalMin(value = "0", inclusive = true, message = "{userEngagement.validFrom.invalid}")
    private Long validFrom;
    
    @DecimalMin(value = "0", inclusive = true, message = "{userEngagement.validTo.invalid}")
    private Long validTo;
        
    private List<BusinessLevelAPI> businessLevels;
    
    private List<RoleAPI> roles;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Long getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Long validFrom) {
        this.validFrom = validFrom;
    }

    public Long getValidTo() {
        return validTo;
    }

    public void setValidTo(Long validTo) {
        this.validTo = validTo;
    }

    public List<BusinessLevelAPI> getBusinessLevels() {
        return businessLevels;
    }

    public void setBusinessLevels(List<BusinessLevelAPI> businessLevels) {
        this.businessLevels = businessLevels;
    }

    public List<RoleAPI> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleAPI> roles) {
        this.roles = roles;
    }

}

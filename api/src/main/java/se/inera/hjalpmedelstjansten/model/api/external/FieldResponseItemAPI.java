/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.model.api.external;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 * 
 * @author Tommy Berglund
 */
public class FieldResponseItemAPI {
    
    public enum ValueType {
        DECIMAL("decimal"),
        REFERENCE_TO("referenceto"), 
        STRING("string"),
        BOOLEAN("boolean"),
        DATETIME("datetime");
        
        private String actualName;
        
        public String getActualName() {
            return actualName;
        }
        
        private ValueType(String actualName) {
            this.actualName = actualName;
        }
    }
    
    public static final String ID_PATH_PROPERTY_NAME = "IdPath";
    public static final String VALUE_TYPE_PROPERTY_NAME = "ValueType";
    public static final String NAME_PROPERTY_NAME = "Name";
    
    private List<String> idPath;
    private String valueType;
    private String name;

    @JsonProperty(ID_PATH_PROPERTY_NAME)
    public List<String> getIdPath() {
        return idPath;
    }

    @JsonProperty(ID_PATH_PROPERTY_NAME)
    public void setIdPath(List<String> idPath) {
        this.idPath = idPath;
    }

    @JsonProperty(VALUE_TYPE_PROPERTY_NAME)
    public String getValueType() {
        return valueType;
    }

    @JsonProperty(VALUE_TYPE_PROPERTY_NAME)
    public void setValueType(String valueType) {
        this.valueType = valueType;
    }
    
    @JsonProperty(NAME_PROPERTY_NAME)
    public String getName() {
        return name;
    }

    @JsonProperty(NAME_PROPERTY_NAME)
    public void setName(String name) {
        this.name = name;
    }

}

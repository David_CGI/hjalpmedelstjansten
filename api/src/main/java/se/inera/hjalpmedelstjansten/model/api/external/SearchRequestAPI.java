/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.model.api.external;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 * API request object
 * 
 * @author Tommy Berglund
 */
public class SearchRequestAPI {
    
    public static final String ID_LIST_PROPERTY_NAME = "IdList";
    public static final String COUNT_PROPERTY_NAME = "Count";
    public static final String OFFSET_PROPERTY_NAME = "Offset";
    public static final String CREDENTIALS_PROPERTY_NAME = "Credentials";
    public static final String CATEGORY_ID_PROPERTY_NAME = "CategoryId";
    public static final String INCLUDE_SUBCATEGORIES_PROPERTY_NAME = "IncludeSubCategories";
    public static final String EXPRESSIONS_PROPERTY_NAME = "Expressions";
    public static final String ORDER_BY_PROPERTY_NAME = "OrderBy";
    
    private String credentials;
    private String categoryId;
    private String includeSubCategories;
    private List<ExpressionAPI> expressions;
    private List<OrderByAPI> orderBys;
    private List<String> idList;
    private Integer offset;
    private Integer count;

    @JsonProperty(CREDENTIALS_PROPERTY_NAME)
    public String getCredentials() {
        return credentials;
    }

    @JsonProperty(CREDENTIALS_PROPERTY_NAME)
    public void setCredentials(String credentials) {
        this.credentials = credentials;
    }

    @JsonProperty(CATEGORY_ID_PROPERTY_NAME)
    public String getCategoryId() {
        return categoryId;
    }

    @JsonProperty(CATEGORY_ID_PROPERTY_NAME)
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    @JsonProperty(INCLUDE_SUBCATEGORIES_PROPERTY_NAME)
    public String getIncludeSubCategories() {
        return includeSubCategories;
    }

    @JsonProperty(INCLUDE_SUBCATEGORIES_PROPERTY_NAME)
    public void setIncludeSubCategories(String includeSubCategories) {
        this.includeSubCategories = includeSubCategories;
    }    
    
    @JsonProperty(EXPRESSIONS_PROPERTY_NAME)
    public List<ExpressionAPI> getExpressions() {
        return expressions;
    }

    @JsonProperty(EXPRESSIONS_PROPERTY_NAME)
    public void setExpressions(List<ExpressionAPI> expressions) {
        this.expressions = expressions;
    }

    @JsonProperty(ORDER_BY_PROPERTY_NAME)
    public List<OrderByAPI> getOrderBys() {
        return orderBys;
    }

    @JsonProperty(ORDER_BY_PROPERTY_NAME)
    public void setOrderBys(List<OrderByAPI> orderBys) {
        this.orderBys = orderBys;
    }
    
    @JsonProperty(OFFSET_PROPERTY_NAME)
    public Integer getOffset() {
        return offset;
    }

    @JsonProperty(OFFSET_PROPERTY_NAME)
    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    @JsonProperty(COUNT_PROPERTY_NAME)
    public Integer getCount() {
        return count;
    }

    @JsonProperty(COUNT_PROPERTY_NAME)
    public void setCount(Integer count) {
        this.count = count;
    }

    @JsonProperty(ID_LIST_PROPERTY_NAME)
    public List<String> getIdList() {
        return idList;
    }

    @JsonProperty(ID_LIST_PROPERTY_NAME)
    public void setIdList(List<String> idList) {
        this.idList = idList;
    }

}

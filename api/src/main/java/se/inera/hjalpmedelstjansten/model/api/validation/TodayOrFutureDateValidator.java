/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.model.api.validation;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Validates if a time (given in milliseconds) is a time during today or a future
 * time.
 * 
 * @author Tommy Berglund
 */
public class TodayOrFutureDateValidator implements ConstraintValidator<ValidTodayOrFutureDate, Long> {
    
    @Override
    public void initialize(ValidTodayOrFutureDate constraintAnnotation) {
        
    }

    @Override
    public boolean isValid(Long dateAsMillis, ConstraintValidatorContext context) {
        if( dateAsMillis == null || dateAsMillis < 0 ) {
            // these cases should be handled by other bean validations
            return true;
        }
        // valid from must be a future date
        Instant nowInstant = Instant.now();
        ZoneId zoneId = ZoneId.systemDefault();
        ZonedDateTime todayStartOfDay = ZonedDateTime.ofInstant(nowInstant, zoneId).toLocalDate().atStartOfDay(zoneId);
        Instant validFromInstant = Instant.ofEpochMilli(dateAsMillis);
        ZonedDateTime validFrom = ZonedDateTime.ofInstant(validFromInstant, zoneId);
        if( validFrom.isBefore(todayStartOfDay) ) {
            return false;
        }
        return true;
    }
    
    
    
}

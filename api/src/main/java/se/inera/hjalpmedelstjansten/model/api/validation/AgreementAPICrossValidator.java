/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.model.api.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import se.inera.hjalpmedelstjansten.model.api.AgreementAPI;

/**
 *
 * @author Tommy Berglund
 */
public class AgreementAPICrossValidator implements ConstraintValidator<ValidAgreementAPICross, AgreementAPI> {
    
    
    @Override
    public void initialize(ValidAgreementAPICross constraintAnnotation) {
        
    }

    @Override
    public boolean isValid(AgreementAPI agreementAPI, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        boolean valid = true;
        if( agreementAPI.getValidFrom() != null && agreementAPI.getValidTo() != null ) {
            if( agreementAPI.getValidFrom() >= agreementAPI.getValidTo() ) {
                context.
                        buildConstraintViolationWithTemplate( "{agreement.validToBeforeValidFrom}" ).
                        addPropertyNode("validTo").
                        addConstraintViolation();
                valid = false;
            }
        }
        return valid;
    }
    
    
    
}

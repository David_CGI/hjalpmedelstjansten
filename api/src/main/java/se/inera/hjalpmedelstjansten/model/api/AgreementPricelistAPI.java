/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.model.api;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import se.inera.hjalpmedelstjansten.model.api.validation.ValidTodayOrFutureDate;

import java.util.List;

/**
 * Basic bean validation is in this file. More complex validations implemented
 * in business methods.
 * 
 * @author Tommy Berglund
 */
public class AgreementPricelistAPI {
    
    private Long id;
    
    @NotNull(message = "{pricelist.number.notNull}")
    @Size(min = 1, max = 255, message = "{pricelist.number.wrongLength}")
    private String number;
    
    @NotNull(message = "{pricelist.validFrom.notNull}")
    @DecimalMin(value = "0", inclusive = true, message = "{pricelist.validFrom.invalid}")
    @ValidTodayOrFutureDate(message = "{pricelist.validFrom.notFuture}")
    private Long validFrom;
    
    private AgreementAPI agreement;

    private String status;

    private List<AgreementPricelistRowAPI> rows;
    
    private Boolean hasPricelistRows;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Long validFrom) {
        this.validFrom = validFrom;
    }

    public AgreementAPI getAgreement() {
        return agreement;
    }

    public void setAgreement(AgreementAPI agreement) {
        this.agreement = agreement;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<AgreementPricelistRowAPI> getRows() {
        return rows;
    }

    public void setRows(List<AgreementPricelistRowAPI> rows) {
        this.rows = rows;
    }

    public Boolean getHasPricelistRows() {
        return hasPricelistRows;
    }

    public void setHasPricelistRows(Boolean hasPricelistRows) {
        this.hasPricelistRows = hasPricelistRows;
    }
        
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.model.api;

import java.io.Serializable;
import javax.validation.constraints.Size;
import se.inera.hjalpmedelstjansten.model.api.validation.ValidEmail;
import se.inera.hjalpmedelstjansten.model.api.validation.ValidURL;

/**
 * ElectronicAddressAPI is used from both OrganizationAPI, UserAPI and ProductAPI. 
 * For a user, email is mandatory, but not for organization and for product only
 * web can be set. Those validations is not done here in order to reuse the same 
 * object. Validation is instead done in business logic.
 * 
 * @author Tommy Berglund
 */
public class ElectronicAddressAPI implements Serializable {
    
    private Long id;
    
    @ValidEmail(message = "{electronicAddress.email.invalid}")
    @Size(max = 255, message = "{electronicAddress.email.wrongLength}")
    private String email;
    
    @Size(max = 255, message = "{electronicAddress.mobile.wrongLength}")
    private String mobile;
    
    @Size(max = 255, message = "{electronicAddress.telephone.wrongLength}")
    private String telephone;
    
    @ValidURL(message = "{electronicAddress.web.invalid}")
    @Size(max = 255, message = "{electronicAddress.web.wrongLength}")
    private String web;
    
    @Size(max = 255, message = "{electronicAddress.fax.wrongLength}")
    private String fax;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }
        
}

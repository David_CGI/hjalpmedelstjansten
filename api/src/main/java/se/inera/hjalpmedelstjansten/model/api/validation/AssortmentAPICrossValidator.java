/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.model.api.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import se.inera.hjalpmedelstjansten.model.api.AgreementAPI;
import se.inera.hjalpmedelstjansten.model.api.AssortmentAPI;

/**
 *
 * @author Tommy Berglund
 */
public class AssortmentAPICrossValidator implements ConstraintValidator<ValidAssortmentAPICross, AssortmentAPI> {
    
    
    @Override
    public void initialize(ValidAssortmentAPICross constraintAnnotation) {
        
    }

    @Override
    public boolean isValid(AssortmentAPI assortmentAPI, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        boolean valid = true;
        if( assortmentAPI.getValidFrom() != null && assortmentAPI.getValidTo() != null ) {
            if( assortmentAPI.getValidFrom() >= assortmentAPI.getValidTo() ) {
                context.
                        buildConstraintViolationWithTemplate( "{assortment.validToBeforeValidFrom}" ).
                        addPropertyNode("validTo").
                        addConstraintViolation();
                valid = false;
            }
        }
        return valid;
    }
    
    
    
}

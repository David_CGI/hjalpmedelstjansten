/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.model.api;

import java.io.Serializable;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Basic bean validation is in this file. More complex validations implemented
 * in business methods.
 * 
 * @author Tommy Berglund
 */
public class UserAPI implements Serializable {
    
    private Long id;
    
    @NotNull(message = "{user.firstname.notNull}")
    private String firstName;
    
    @NotNull(message = "{user.lastname.notNull}")
    private String lastName;
    
    private String title;
    
    @NotNull(message = "{user.username.notNull}")
    private String username;
    
    private Boolean previousLoginFound;
    
    @Valid
    @NotNull(message = "{user.electronicAddress.notNull}")
    private ElectronicAddressAPI electronicAddress;

    @Valid
    @NotNull(message = "{user.engagements.notNull}")
    @Size(min = 1, max = 1, message = "{user.engagements.wrongSize}")
    private List<UserEngagementAPI> userEngagements;
    
    private String loginType;
    private String loginUrl;
    
    private boolean active;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public ElectronicAddressAPI getElectronicAddress() {
        return electronicAddress;
    }

    public void setElectronicAddress(ElectronicAddressAPI electronicAddress) {
        this.electronicAddress = electronicAddress;
    }

    public List<UserEngagementAPI> getUserEngagements() {
        return userEngagements;
    }

    public void setUserEngagementAPIs(List<UserEngagementAPI> userEngagements) {
        this.userEngagements = userEngagements;
    }    

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Boolean getPreviousLoginFound() {
        return previousLoginFound;
    }

    public void setPreviousLoginFound(Boolean previousLoginFound) {
        this.previousLoginFound = previousLoginFound;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getLoginUrl() {
        return loginUrl;
    }

    public void setLoginUrl(String loginUrl) {
        this.loginUrl = loginUrl;
    }

}

/*
* Copyright (C) 2019 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.model.api;


import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Per Abrahamsson
 */



public class DynamicTextAPI implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long uniqueId;

    @Column(nullable = true)
    private String dynamicText;

    @Temporal(TemporalType.TIMESTAMP)
    private Date whenChanged;

    @Column(nullable = false)
    private String changedBy;


    public void setUniqueId(Long uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Long getUniqueId() {
        return uniqueId;
    }


    public void setDynamicText(String dynamicText) {
        this.dynamicText = dynamicText;
    }

    public String getDynamicText() {
        return dynamicText;
    }


    public void setWhenChanged(Date whenChanged) {
        this.whenChanged = whenChanged;
    }

    public Date getWhenChanged() {
        return whenChanged;
    }


    public void setChangedBy(String changedBy) {
        this.changedBy = changedBy;
    }

    public String getChangedBy() {
        return changedBy;
    }

}

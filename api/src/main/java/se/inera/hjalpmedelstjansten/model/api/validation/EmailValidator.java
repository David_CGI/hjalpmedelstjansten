/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.model.api.validation;

import javax.mail.internet.AddressException;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.mail.internet.InternetAddress;

/**
 *
 * @author Tommy Berglund
 */
public class EmailValidator implements ConstraintValidator<ValidEmail, String> {
    
    
    @Override
    public void initialize(ValidEmail constraintAnnotation) {
        
    }

    @Override
    public boolean isValid(String email, ConstraintValidatorContext context) {
        if( email == null || email.isEmpty() ) {
            return true;
        }
        try {
            new InternetAddress(email, true);
        } catch (AddressException ex) {
            return false;
        }
        return true;
    }
    
    
    
}

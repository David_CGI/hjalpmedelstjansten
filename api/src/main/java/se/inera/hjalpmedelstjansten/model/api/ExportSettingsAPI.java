/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.model.api;

import java.util.List;
import javax.validation.constraints.NotNull;

/**
 * Basic bean validation is in this file. More complex validations implemented
 * in business methods.
 * 
 * @author Tommy Berglund
 */
public class ExportSettingsAPI {
    
    private Long id;
    
    @NotNull(message = "{exportsettings.organization.notNull}")
    private OrganizationAPI organization;    
    
    private BusinessLevelAPI businessLevel;
    
    @NotNull(message = "{exportsettings.filename.notNull}")
    private String filename;

    private int numberOfExports;
        
    private List<GeneralPricelistAPI> generalPricelists;
    
    private Long lastExported;
    
    private boolean enabled;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OrganizationAPI getOrganization() {
        return organization;
    }

    public void setOrganization(OrganizationAPI organization) {
        this.organization = organization;
    }

    public BusinessLevelAPI getBusinessLevel() {
        return businessLevel;
    }

    public void setBusinessLevel(BusinessLevelAPI businessLevel) {
        this.businessLevel = businessLevel;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public int getNumberOfExports() {
        return numberOfExports;
    }

    public void setNumberOfExports(int numberOfExports) {
        this.numberOfExports = numberOfExports;
    }

    public Long getLastExported() {
        return lastExported;
    }

    public void setLastExported(Long lastExported) {
        this.lastExported = lastExported;
    }

    public List<GeneralPricelistAPI> getGeneralPricelists() {
        return generalPricelists;
    }

    public void setGeneralPricelists(List<GeneralPricelistAPI> generalPricelists) {
        this.generalPricelists = generalPricelists;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

}

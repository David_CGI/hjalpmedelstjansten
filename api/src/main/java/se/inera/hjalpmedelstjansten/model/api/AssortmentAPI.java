/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.model.api;

import java.util.List;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import se.inera.hjalpmedelstjansten.model.api.cv.CVCountyAPI;
import se.inera.hjalpmedelstjansten.model.api.cv.CVMunicipalityAPI;
import se.inera.hjalpmedelstjansten.model.api.validation.ValidAssortmentAPICross;

/**
 * Basic bean validation is in this file. More complex validations implemented
 * in business methods.
 * 
 * @author Tommy Berglund
 */
@ValidAssortmentAPICross
public class AssortmentAPI {

    public enum Include {
        ALL, MINE
    }
    
    private Long id;
    
    @NotNull(message = "{assortment.name.notNull}")
    @Size(max = 255, message = "{assortment.name.wrongLength}")
    private String name;
    
    @NotNull(message = "{assortment.validFrom.notNull}")
    @DecimalMin(value = "0", inclusive = true, message = "{assortment.validFrom.invalid}")
    private Long validFrom;
    
    @DecimalMin(value = "0", inclusive = true, message = "{assortment.validTo.invalid}")
    private Long validTo;
    
    private CVCountyAPI county;
    
    private List<CVMunicipalityAPI> municipalities;
    
    private OrganizationAPI customer;
    
    private List<UserAPI> managers;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OrganizationAPI getCustomer() {
        return customer;
    }

    public void setCustomer(OrganizationAPI customer) {
        this.customer = customer;
    }

    public Long getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Long validFrom) {
        this.validFrom = validFrom;
    }

    public Long getValidTo() {
        return validTo;
    }

    public void setValidTo(Long validTo) {
        this.validTo = validTo;
    }

    public CVCountyAPI getCounty() {
        return county;
    }

    public void setCounty(CVCountyAPI county) {
        this.county = county;
    }

    public List<CVMunicipalityAPI> getMunicipalities() {
        return municipalities;
    }

    public void setMunicipalities(List<CVMunicipalityAPI> municipalities) {
        this.municipalities = municipalities;
    }

    public List<UserAPI> getManagers() {
        return managers;
    }

    public void setManagers(List<UserAPI> managers) {
        this.managers = managers;
    }

}

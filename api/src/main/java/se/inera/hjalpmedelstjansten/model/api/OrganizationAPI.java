/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.model.api;

import se.inera.hjalpmedelstjansten.model.api.cv.CVCountryAPI;
import java.io.Serializable;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import se.inera.hjalpmedelstjansten.model.api.validation.ValidOrganizationType;
import se.inera.hjalpmedelstjansten.model.api.validation.ValidGlnOrGtin;

/**
 * Basic bean validation is in this file. More complex validations implemented
 * in business methods.
 * 
 * @author Tommy Berglund
 */
@ValidOrganizationType
public class OrganizationAPI implements Serializable {
    
    private Long id;
        
    // null checked by @ValidOrganizationType on class
    private String organizationType;
    
    @NotNull(message = "{organization.name.notNull}")
    @Size(min = 1, max = 255, message = "{organization.name.wrongLength}")
    private String organizationName;
    
    @NotNull(message = "{organization.country.notNull}")
    @Valid
    private CVCountryAPI country;
    
    @NotNull(message = "{organization.number.notNull}")
    @Size(min = 1, max = 255, message = "{organization.number.wrongLength}")
    private String organizationNumber;
    
    @NotNull(message = "{organization.gln.notNull}")
    @ValidGlnOrGtin(message = "{organization.gln}")
    private String gln;
    
    @NotNull(message = "{organization.validFrom.notNull}")
    @DecimalMin(value = "0", inclusive = true, message = "{organization.validFrom.invalid}")
    private Long validFrom;
    
    @DecimalMin(value = "0", inclusive = true, message = "{organization.validTo.invalid}")
    private Long validTo;
    
    @Valid
    @NotNull(message = "{organization.electronicAddress.notNull}")
    private ElectronicAddressAPI electronicAddress;
    
    @Valid
    @NotNull(message = "{organization.postAddresses.wrongSize}")
    @Size(min = 2, max = 2, message = "{organization.postAddresses.wrongSize}")
    private List<PostAddressAPI> postAddresses;
    
    private boolean active;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOrganizationType() {
        return organizationType;
    }

    public void setOrganisationType(String organizationType) {
        this.organizationType = organizationType;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public CVCountryAPI getCountry() {
        return country;
    }

    public void setCountry(CVCountryAPI country) {
        this.country = country;
    }

    public String getOrganizationNumber() {
        return organizationNumber;
    }

    public void setOrganizationNumber(String organizationNumber) {
        this.organizationNumber = organizationNumber;
    }

    public String getGln() {
        return gln;
    }

    public void setGln(String gln) {
        this.gln = gln;
    }

    public Long getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Long validFrom) {
        this.validFrom = validFrom;
    }

    public Long getValidTo() {
        return validTo;
    }

    public void setValidTo(Long validTo) {
        this.validTo = validTo;
    }

    public ElectronicAddressAPI getElectronicAddress() {
        return electronicAddress;
    }

    public void setElectronicAddress(ElectronicAddressAPI electronicAddress) {
        this.electronicAddress = electronicAddress;
    }

    public List<PostAddressAPI> getPostAddresses() {
        return postAddresses;
    }

    public void setPostAddresses(List<PostAddressAPI> postAddresses) {
        this.postAddresses = postAddresses;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

}

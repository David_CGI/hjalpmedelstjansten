#!/bin/bash

JBOSS_HOME=/opt/jboss/wildfly
JBOSS_MODE=${1:-"standalone"}
JBOSS_CONFIG=${2:-"$JBOSS_MODE-hjmtj.xml"}

cp /opt/jboss/wildfly/customization/standalone-hjmtj.xml $JBOSS_HOME/$JBOSS_MODE/configuration
sed -i.bak "s|{DB_CONNECTION_URL}|$DB_CONNECTION_URL|g" $JBOSS_HOME/$JBOSS_MODE/configuration/$JBOSS_CONFIG
sed -i.bak "s|{DB_USERNAME}|$DB_USERNAME|g" $JBOSS_HOME/$JBOSS_MODE/configuration/$JBOSS_CONFIG
sed -i.bak "s|{DB_PASSWORD}|$DB_PASSWORD|g" $JBOSS_HOME/$JBOSS_MODE/configuration/$JBOSS_CONFIG
unzip /opt/jboss/wildfly/customization/mysql-connector-java-5.1.45.zip -d /tmp/
cp /tmp/mysql-connector-java-5.1.45/mysql-connector-java-5.1.45-bin.jar $JBOSS_HOME/$JBOSS_MODE/deployments
cp /opt/jboss/wildfly/customization/hjmtj-export.war $JBOSS_HOME/$JBOSS_MODE/deployments

if [ "$HJMTJ_ENVIRONMENT" = "local" ]
then
    mkdir /opt/jboss/wildfly/modules/hjmtj-export
    mkdir /opt/jboss/wildfly/modules/hjmtj-export/main
    cp /opt/jboss/wildfly/customization/dev.properties /opt/jboss/wildfly/modules/hjmtj-export/main/app.properties
    cp /opt/jboss/wildfly/customization/module.xml /opt/jboss/wildfly/modules/hjmtj-export/main
fi

echo "=> Starting WildFly server" with config $JBOSS_CONFIG
$JBOSS_HOME/bin/$JBOSS_MODE.sh -b 0.0.0.0 -c $JBOSS_CONFIG --debug -bmanagement 0.0.0.0

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.export;

import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.DependsOn;
import javax.ejb.ScheduleExpression;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timeout;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.inject.Inject;
import org.jboss.ejb3.annotation.TransactionTimeout;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.clustering.ClusterController;

/**
 * The worker wakes up at regular intervals and checks a specific cluster list to
 * see if there is anything to do. If so, the worker will work until there is nothing
 * left to do. The cluster list is populated by the ExportGenerationTimer. 
 * 
 * @author Tommy Berglund
 */
@Singleton
@Startup
@DependsOn("PropertyLoader")
public class ExportGenerationTimerWorker {
    
    @Inject
    private HjmtLogger LOG;
    
    @Inject
    private boolean exportGenerationWorkerTimerEnabled;
    
    @Inject
    private String exportGenerationWorkerTimerHour;
    
    @Inject
    private String exportGenerationWorkerTimerMinute;
    
    @Inject
    private String exportGenerationWorkerTimerSecond;
    
    @Inject
    ExportGenerationController exportGenerationController;
    
    @Inject
    private ClusterController clusterController;
    
    @Resource
    private TimerService timerService;
    
    @Timeout
    @TransactionTimeout(value = 8, unit = TimeUnit.HOURS)
    private void schedule() {
        LOG.log( Level.FINEST, "schedule" );
        long start = System.currentTimeMillis();
        Long nextId = clusterController.getNextLongFromQueue(ExportGenerationTimer.QUEUE_NAME);
        int numberOfExportsHandled = 0;
        while( nextId != null ) {
            exportGenerationController.export(nextId, false, null);
            nextId = clusterController.getNextLongFromQueue(ExportGenerationTimer.QUEUE_NAME);
            numberOfExportsHandled++;
        }
        if( numberOfExportsHandled > 0 ) {
            LOG.log( Level.FINEST, "Worker handled {0} exports", new Object[] {numberOfExportsHandled} );
            long total = System.currentTimeMillis() - start;
            LOG.logPerformance(this.getClass().getName(), "schedule()", total, null);
        } else {
            LOG.log( Level.FINEST, "Nothing to do" );
        }
    }
    
    @PostConstruct
    private void initialize() {
        LOG.log( Level.FINEST, "initialize(exportGenerationWorkerTimerHour: {0}, exportGenerationWorkerTimerMinute: {1}, exportGenerationWorkerTimerSecond: {2})", new Object[] {exportGenerationWorkerTimerHour, exportGenerationWorkerTimerMinute, exportGenerationWorkerTimerSecond} );
        if( exportGenerationWorkerTimerEnabled ) {
            LOG.log( Level.FINEST, "Timer is ENABLED" );
            if (timerService.getTimers().isEmpty()) {
                String name = this.getClass().getName();
                TimerConfig configuration = new TimerConfig();
                configuration.setPersistent(false);
                configuration.setInfo(name);
                ScheduleExpression scheduleExpression = new ScheduleExpression();
                scheduleExpression.
                        hour(exportGenerationWorkerTimerHour).
                        minute(exportGenerationWorkerTimerMinute).
                        second(exportGenerationWorkerTimerSecond);
                timerService.createCalendarTimer(scheduleExpression, configuration);
            }
        } else {
            LOG.log( Level.INFO, "Timer is NOT ENABLED" );
        }
    }
    
}

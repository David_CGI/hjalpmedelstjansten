/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.inera.hjalpmedelstjansten.export.property;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import se.inera.hjalpmedelstjansten.property.AppConfigurationService;

/**
 *
 * @author tomber
 */
@Startup
@Singleton
public class PropertyLoader {
    
    @Inject
    private AppConfigurationService appConfigurationService;
    
    @PostConstruct
    private void loadProperties() {
        appConfigurationService.loadProperties("app.properties");
    }
    
}

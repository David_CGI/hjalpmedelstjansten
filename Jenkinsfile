pipeline {

    agent any

    stages {

        stage('Build') {
            parallel {
                stage('Build Backend') {
                    agent { 
                        docker {
                            image 'maven:latest' 
                            reuseNode true
                        }
                    }
                    steps {
                        sh 'mvn clean install -DskipTests'
                    }
                }
                stage('Build Frontend') {
                    agent {
                        docker {
                            image 'node:9.11'
                            args '-e HOME=$WORKSPACE'
                            reuseNode true
                        }
                    }
                    steps {
                        sh 'npm --prefix frontend/html install'
                        sh 'npm --prefix frontend/html run-script build'
                    }
                }
            }
        }

        stage('Test') {
            parallel {
                stage('Test Backend') {
                    agent {
                        docker {
                            image 'maven:latest'
                            reuseNode true
                        }
                    }
                    steps {
                        sh 'mvn test'
                    }
                }
            }
        }

    }
    post {
        failure {
            script {
                emailext subject: "Jenkins build failure",
                    mimeType: "text/html",
                    body: "Jenkins project ${env.JOB_NAME} failed to build<br />Build Number: ${env.BUILD_ID} <br/> Jenkins URL: ${env.BUILD_URL}",
                    recipientProviders: [
                        [$class: 'CulpritsRecipientProvider'],
                        [$class: 'DevelopersRecipientProvider']
                    ]
            }   
        }
        changed {
            script {
                if (currentBuild.currentResult == 'SUCCESS') {
                    emailext subject: "Jenkins Build back to normal",
                        mimeType: "text/html",
                        body: "Jenkins project ${env.JOB_NAME} is back to normal on build<br />Build Number: ${env.BUILD_ID} <br/> Jenkins URL: ${env.BUILD_URL}",
                        recipientProviders: [
                            [$class: 'CulpritsRecipientProvider'],
                            [$class: 'DevelopersRecipientProvider']
                        ]
                }
            }
        }
    }
}
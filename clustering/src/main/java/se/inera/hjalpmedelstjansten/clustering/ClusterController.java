/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.clustering;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisSentinelPool;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;

/**
 * This class contains methods for handling cluster issues, like locks and 
 * global sessions
 * 
 * Reads two environment variables:
 * - REDIS_MASTER_NAME
 * - REDIS_HOST
 * 
 * if the environment variable REDIS_MASTER_NAME is set an attempt to start
 * a JedisSentinelPool is made. The REDIS_HOST environment variable is 
 * also assumed to be a comma separated string of host names
 * 
 * if the environment variable REDIS_MASTER_NAME is NOT set an attempt to start
 * a JedisPool is made. The REDIS_HOST environment variable can in this case NOT
 * be a comma separated string of host name, but instead a single hostname
 * 
 * @author Tommy Berglund
 */
@Singleton
@Startup
public class ClusterController {
    
    @Inject
    HjmtLogger LOG;
    
    private JedisPool jedisPool;
    private JedisSentinelPool jedisSentinelPool;
    private String password;
    
    /**
     * Attempts to get a lock in Redis using the setnx (set if not exists) method.
     * If lock is received, the lock is maintained for <code>expiresAfter</code>
     * seconds. 
     * 
     * @param key the key of the lock
     * @param expiresAfter number of seconds to maintain the lock
     * @return true if lock was received, otherwise false
     */
    public boolean getLock(String key, int expiresAfter) {
        LOG.log( Level.FINEST, "getLock( key: {0}, expiresAfter: {1} )", new Object[] {key, expiresAfter} );
        boolean lockReceived = false;
        try (Jedis jedis = getJedis() ) {
            Long result = jedis.setnx(key, "lock");
            if( result == 1l ) {
                lockReceived = true;
                jedis.expire(key, expiresAfter);
            }
        }
        return lockReceived;
    }
    
    /**
     * Get a value with the given prefix and key.
     * 
     * @param prefix
     * @param key
     * @return 
     */
    public String getValue( String prefix, String key ) {
        try (Jedis jedis = getJedis() ) {
            return jedis.get(prefix + key);
        }
    }
    
    /**
     * Set a value with the given prefix and key.
     * 
     * @param prefix
     * @param key
     * @param value 
     */
    public void setValue( String prefix, String key, String value ) {
        try (Jedis jedis = getJedis() ) {
             jedis.set(prefix + key, value);
        }
    }
    
    /**
     * Set that a value with given key (and prefix) expires after the given 
     * amount of seconds.
     * 
     * @param prefix
     * @param key
     * @param expiresSeconds the number of seconds to expire value 
     */
    public void valueExpires( String prefix, String key, int expiresSeconds ) {
        try (Jedis jedis = getJedis() ) {
            jedis.expire(prefix + key, expiresSeconds);
        }
    }
    
    /**
     * Remove a value with the given key (and prefix)
     * 
     * @param prefix
     * @param key 
     */
    public void removeValue( String prefix, String key ) {
        try (Jedis jedis = getJedis() ) {
            jedis.del(prefix + key);
        }
    }
    
    /**
     * Get all keys with the given prefix
     * 
     * @param prefix
     * @return 
     */
    public Set<String> getKeys(String prefix) {
        try (Jedis jedis = getJedis() ) {
            return jedis.keys(prefix + "*");
        }
    }
    
    /**
     * Add a long to a cluster list. Uses Redis LPUSH
     * 
     * @param queueName the name of the list (queue)
     * @param value long value, usually an id of some sort
     */
    public void addLongToQueue(String queueName, Long value) {
        try (Jedis jedis = getJedis() ) {
            jedis.lpush(queueName, value.toString());
        }
    }
    
    /**
     * Get next long from a cluster list. Uses Redis RPOP 
     * 
     * @param queueName the name of the list (queue)
     * @return next long value, null if there is none
     */
    public Long getNextLongFromQueue(String queueName) {
        try (Jedis jedis = getJedis() ) {
            String value = jedis.rpop(queueName);
            return value == null ? null: Long.parseLong(value);
        }
    }
    
    private Jedis getJedis() {
        Jedis jedis;
        if( jedisPool != null ) {
            jedis = jedisPool.getResource();
        } else {
            jedis = jedisSentinelPool.getResource();
        }
        if( password != null ) {
            jedis.auth(password);
        }
        return jedis;
    }
    
    @PostConstruct
    private void initialize() {
        LOG.log( Level.FINEST, "initialize()" );
        String redisMasterNameEnv = System.getenv("REDIS_MASTER_NAME");
        String redisHostEnv = System.getenv("REDIS_HOST");
        if( redisMasterNameEnv != null && !redisMasterNameEnv.equals("") ) {
            LOG.log( Level.FINEST, "Found REDIS_MASTER_NAME environment variable. Starting JedisSentinelPool" );
            String[] redisHosts = redisHostEnv.split(",");
            Set<String> sentinels = new HashSet<>();
            for( String redisHost : redisHosts ) {
                sentinels.add(redisHost);
            }
            jedisSentinelPool = new JedisSentinelPool(redisMasterNameEnv, sentinels);
        } else {
            LOG.log( Level.FINEST, "Did not find REDIS_MASTER_NAME environment variable. Starting JedisPool" );
            jedisPool = new JedisPool(redisHostEnv);
        }
        if( System.getenv("REDIS_PASSWORD") != null && !System.getenv("REDIS_PASSWORD").isEmpty() ) {
            password = System.getenv("REDIS_PASSWORD");
        }
    }
    
}

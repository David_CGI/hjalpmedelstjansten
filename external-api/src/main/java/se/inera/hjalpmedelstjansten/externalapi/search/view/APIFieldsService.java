/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.externalapi.search.view;

import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.externalapi.logging.view.PerformanceLogInterceptor;
import se.inera.hjalpmedelstjansten.externalapi.search.controller.APIFieldController;
import se.inera.hjalpmedelstjansten.externalapi.search.controller.LogRequest;
import se.inera.hjalpmedelstjansten.model.api.external.FieldRequestAPI;
import se.inera.hjalpmedelstjansten.model.api.external.FieldResponseItemAPI;

/**
 * The entrypoint to the api. All requests go thru here. 
 * 
 * @author Tommy Berglund
 */
@Stateless
@Path("fields")
@Interceptors({ PerformanceLogInterceptor.class })
public class APIFieldsService {
    
    @Inject
    HjmtLogger LOG;
    
    @Inject
    APIFieldController apiFieldController;
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @LogRequest
    public Response apiSearch(FieldRequestAPI fieldRequestAPI) {
        LOG.log( Level.FINEST, "apiSearch(...)" );
        List<FieldResponseItemAPI> fieldResponseAPIs = apiFieldController.search(fieldRequestAPI);        
        return Response.ok(fieldResponseAPIs).build();
    }
    
}

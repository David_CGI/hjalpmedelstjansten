/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.externalapi.search.controller;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.model.api.external.ExpressionAPI;
import se.inera.hjalpmedelstjansten.model.api.external.SearchRequestAPI;
import se.inera.hjalpmedelstjansten.model.entity.Article;
import se.inera.hjalpmedelstjansten.model.entity.Category;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificProperty;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificPropertyListValue;
import se.inera.hjalpmedelstjansten.model.entity.ExternalCategoryGrouping;
import se.inera.hjalpmedelstjansten.model.entity.Organization;
import se.inera.hjalpmedelstjansten.model.entity.PostAddress;
import se.inera.hjalpmedelstjansten.model.entity.Product;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValue;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueDecimal;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueInterval;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueValueListMultiple;
import se.inera.hjalpmedelstjansten.model.entity.ResourceSpecificPropertyValueValueListSingle;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVCounty;
import se.inera.hjalpmedelstjansten.model.entity.cv.CVMunicipality;
import se.inera.hjalpmedelstjansten.model.entity.media.ArticleMediaDocument;
import se.inera.hjalpmedelstjansten.model.entity.media.ArticleMediaImage;
import se.inera.hjalpmedelstjansten.model.entity.media.Media;
import se.inera.hjalpmedelstjansten.model.entity.media.MediaDocument;
import se.inera.hjalpmedelstjansten.model.entity.media.MediaImage;

/**
 * Using api classes does not work since some apis must return dynamic field 
 * names, builders like JsonObjectBuilder are used instead.
 * 
 * @author Tommy Berglund
 */
@Stateless
public class APISearchController {

    @Inject
    HjmtLogger LOG;
    
    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;
    
    @Inject
    private String baseNavigationLevelName;
    
    @Inject
    private String baseNavigationLevelDescription;
    
    @Inject
    private String fileEndingsToMimeTypes;
    
    private Map<String,String> fileEndingsToMimeTypesMap = new HashMap();
    
    private static final String CATEGORY_ID_MENUTREE = "menutree";
    private static final String CATEGORY_ID_COUNTY_MUNICIPALITY_CODE = "CountyMunicipaltyCode";
    private static final String CATEGORY_ID_CLASSIFICATION = "Classification";
    private static final String CATEGORY_ID_PRODUCT = "Product";
    private static final String CATEGORY_ID_ORGANIZATION = "Organization";
    
    private static final String EXPRESSION_IDPATH_CLASSIFICATION = "Classification";
    private static final String EXPRESSION_IDPATH_PROD_NO = "VGRProdNo";
    private static final String EXPRESSION_IDPATH_BASED_ON_PRODUCT = "BasedOnTemplateProduct";
    
    private static final String SORT_NAME = "name";
    private static final String SORT_ORDER = "order";
    private static final String SORT_CODE = "code";
    private static final String SORT_PRODUCT_NAME = "productName";
    
    private static final String CREDENTIALS_ALL = "all";
    
    /**
     * Search can be made for the menu tree, a county/municipality, classifications,
     * organization or products (in 3 different ways)
     * 
     * @param searchRequestAPI
     * @return 
     */
    public JsonObject search(SearchRequestAPI searchRequestAPI) {
        LOG.log( Level.FINEST, "search(...)");
        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
        JsonArray items = null;
        if( CATEGORY_ID_MENUTREE.equals(searchRequestAPI.getCategoryId()) ) {
            items = searchMenuTree(searchRequestAPI);
        } else if( CATEGORY_ID_COUNTY_MUNICIPALITY_CODE.equals(searchRequestAPI.getCategoryId()) ) {
            items = searchCountyMunicipality(searchRequestAPI);
        } else if( CATEGORY_ID_CLASSIFICATION.equals(searchRequestAPI.getCategoryId()) ) {
            items = searchClassification();
        } else if( CATEGORY_ID_PRODUCT.equals(searchRequestAPI.getCategoryId()) ) {
            items = searchProduct(searchRequestAPI);
        } else if( CATEGORY_ID_ORGANIZATION.equals(searchRequestAPI.getCategoryId()) ) {
            items = searchOrganization(searchRequestAPI);
        } else {
            LOG.log( Level.WARNING, "Failed to search. No valid CategoryId found in request" );
        }

        if( items != null ) {
            jsonObjectBuilder.add(FieldNames.ITEMS, items);
            // count
            jsonObjectBuilder.add(FieldNames.TOTAL_HITS, items.size());
        }
        
        return jsonObjectBuilder.build();
    }
        
    
    /**
     * There are different types of search product, search all products, search 
     * for a specific product and search for articles based on product. The
     * Expressions part of the requests determines which type of search to do.
     * 
     * @param searchRequestAPI
     * @param searchResponseAPI 
     */
    private JsonArray searchProduct(SearchRequestAPI searchRequestAPI) {
        LOG.log( Level.FINEST, "searchProduct(...)");
        // first decide what type of search we're doing
        for( ExpressionAPI expressionAPI : searchRequestAPI.getExpressions() ) {
            String idPath = expressionAPI.getIdPath().get(0);
            if( EXPRESSION_IDPATH_PROD_NO.equals(idPath) ) {
                return searchProductByProdNo(expressionAPI);
            } else if( EXPRESSION_IDPATH_BASED_ON_PRODUCT.equals(idPath) ) {
                return searchProductByBasedOnProduct(searchRequestAPI, expressionAPI);
            } else if( EXPRESSION_IDPATH_CLASSIFICATION.equals(idPath) ) {
                return searchProductsByCategory(searchRequestAPI, expressionAPI);
            }
            // ignore other
        }
        LOG.log( Level.WARNING, "Failed to search product. No valid expression found" );
        return null;
    }
    
    /**
     * Search for a product OR article with the given catalogue unique number. The 
     * requested product is sent using the MatchIds field and it's the catalogue 
     * unique number of the product that is used.
     * 
     * @param searchRequestAPI
     * @param searchResponseAPI 
     */
    private JsonArray searchProductByProdNo(ExpressionAPI expressionAPI) {
        LOG.log( Level.FINEST, "searchProductByProdNo(...)");
        if( expressionAPI.getMatchIds() == null || expressionAPI.getMatchIds().isEmpty() ) {
            LOG.log( Level.WARNING, "Failed to search product. No valid catalogue unique number found" );
            return null;
        }
        Long catalogueUniqueNumber;
        try {
            catalogueUniqueNumber = Long.parseLong(expressionAPI.getMatchIds().get(0));
        } catch( NumberFormatException e ) {
            LOG.log( Level.WARNING, "Failed to search product. No numeric catalogue unique number found" );
            return null;
        }
        List<Product> products = em.createQuery("SELECT p FROM Product p WHERE p.catalogUniqueNumber.uniqueId = :catalogueUniqueNumber").
                setParameter("catalogueUniqueNumber", catalogueUniqueNumber).
                getResultList();
        if( products == null || products.isEmpty() ) {
            List<Article> articles = em.createQuery("SELECT a FROM Article a WHERE a.catalogUniqueNumber.uniqueId = :catalogueUniqueNumber").
                setParameter("catalogueUniqueNumber", catalogueUniqueNumber).
                getResultList();
            if( articles == null || articles.isEmpty() ) {
                LOG.log( Level.WARNING, "Failed to search product/article. No product or article with catalogue unique number {0} found", new Object[]{catalogueUniqueNumber} );
                return null;
            } else if(articles.size() > 1 ) {
                LOG.log( Level.WARNING, "Failed to search product/article. Multiple articles with catalogue unique number {0} found which is illegal", new Object[]{catalogueUniqueNumber} );
                return null;
            } else {
                JsonObject productItem = getArticleItem(articles.get(0), true);
                return Json.createArrayBuilder().add(productItem).build();
            }
        } else if(products.size() > 1 ) {
            LOG.log( Level.WARNING, "Failed to search product. Multiple products with  catalogue unique number {0} found which is illegal", new Object[]{catalogueUniqueNumber} );
            return null;
        } else {
            JsonObject productItem = getProductItem(products.get(0), true);
            return Json.createArrayBuilder().add(productItem).build();
        }        
    }
    
    /**
     * Search all articles based on the given product. The requested product is 
     * sent using the MatchIds field and it's the uniqueId of the product that is used.
     * 
     * @param searchRequestAPI
     * @param searchResponseAPI 
     */
    private JsonArray searchProductByBasedOnProduct(SearchRequestAPI searchRequestAPI, ExpressionAPI expressionAPI) {
        LOG.log( Level.FINEST, "searchProductByBasedOnProduct(...)");
        JsonArrayBuilder productsBuilder = Json.createArrayBuilder();
        int offset = searchRequestAPI.getOffset() == null ? 0: searchRequestAPI.getOffset();
        int maxResults = searchRequestAPI.getCount() == null ? 25: searchRequestAPI.getCount();
        if( expressionAPI.getMatchIds() == null || expressionAPI.getMatchIds().isEmpty() ) {
            LOG.log( Level.WARNING, "Failed to search product. No valid product unique id found" );
            return null;
        }
        Long productUniqueNumber;
        try {
            productUniqueNumber = Long.parseLong(expressionAPI.getMatchIds().get(0));
        } catch( NumberFormatException e ) {
            LOG.log( Level.WARNING, "Failed to search product. No numeric product unique id found" );
            return null;
        }
        String sortField = getSortField(searchRequestAPI);
        boolean sortDescending = isSortDescending(searchRequestAPI);
        StringBuilder queryBuilder = new StringBuilder("SELECT a FROM Article a WHERE a.basedOnProduct.uniqueId = :productUniqueId");
        if( sortField != null ) {
            if( SORT_PRODUCT_NAME.equals(sortField) ) {
                queryBuilder.append(" ORDER BY a.articleName");
                if( sortDescending ) {
                    queryBuilder.append(" DESC");
                } else {
                    queryBuilder.append(" ASC");
                }
            }
        } 
        List<Article> articles = em.createQuery(queryBuilder.toString()).
                setParameter("productUniqueId", productUniqueNumber).
                setFirstResult(offset).
                setMaxResults(maxResults).
                getResultList();
        for( Article article : articles ) {
            productsBuilder.add(getArticleItem(article, false));
        }
        return productsBuilder.build();
    }
    
    /**
     * Search all products for the given categories. Categories are sent using
     * the MatchIds field
     * 
     * @param searchRequestAPI
     * @param searchResponseAPI
     * @param expressionAPI 
     */
    private JsonArray searchProductsByCategory(SearchRequestAPI searchRequestAPI, ExpressionAPI expressionAPI) {
        LOG.log( Level.FINEST, "searchProductsByCategory(...)");
        JsonArrayBuilder productsBuilder = Json.createArrayBuilder();
        String limitToCountyMunicipality = searchRequestAPI.getCredentials();
        if( limitToCountyMunicipality == null || limitToCountyMunicipality.isEmpty() ) {
            LOG.log( Level.WARNING, "no credentials given for search products" );
            return null;
        }
        List<Long> categoryIds = getCategoryIdsFromMatchIds(expressionAPI.getMatchIds());
        if( categoryIds == null || categoryIds.isEmpty() ) {
            LOG.log( Level.WARNING, "no categories given for search products" );
            return null;
        }
        int offset = searchRequestAPI.getOffset() == null ? 0: searchRequestAPI.getOffset();
        int maxResults = searchRequestAPI.getCount() == null ? 25: searchRequestAPI.getCount();
        String sortField = getSortField(searchRequestAPI);
        boolean sortDescending = isSortDescending(searchRequestAPI);
        StringBuilder queryBuilder = new StringBuilder("SELECT DISTINCT p FROM Assortment at JOIN at.articles ar JOIN ar.basedOnProduct p JOIN at.municipalities am WHERE p.category.uniqueId IN :categoryIds AND at.county IS NOT NULL");
        boolean credentialsUsed = false;
        Long credentialsId = null;
        if( !CREDENTIALS_ALL.equals(limitToCountyMunicipality) ) {
            // find county or municipality by credentials id, assuming municipality is more commong
            try {
                credentialsId = Long.parseLong(limitToCountyMunicipality);
                credentialsUsed = true;
                CVMunicipality municipality = em.find(CVMunicipality.class, credentialsId);
                if( municipality != null ) {
                    queryBuilder.append(" AND am.uniqueId = :regionId");
                } else {
                    CVCounty county = em.find(CVCounty.class, credentialsId);
                    if( county != null ) {
                        queryBuilder.append(" AND at.county.uniqueId = :regionId");
                    } else {
                        LOG.log( Level.WARNING, "invalid credentials given for search products, does not match county or municipality" );
                        return null;
                    }
                }
            } catch( NumberFormatException e ) {
                LOG.log( Level.WARNING, "invalid credentials given for search products, not numeric" );
                return null;
            }
        }
        if( sortField != null ) {
            if( SORT_PRODUCT_NAME.equals(sortField) ) {
                queryBuilder.append(" ORDER BY p.productName");
                if( sortDescending ) {
                    queryBuilder.append(" DESC");
                } else {
                    queryBuilder.append(" ASC");
                }
            }
        } 

        Query query = em.createQuery(queryBuilder.toString()).
                setParameter("categoryIds", categoryIds).
                setFirstResult(offset).
                setMaxResults(maxResults);
        if( credentialsUsed ) {
            query.setParameter("regionId", credentialsId);
        }
        List<Product> products = query.getResultList();
        for( Product product : products ) {
            productsBuilder.add(getProductItem(product, false));
        }
        return productsBuilder.build();
    }
                
    private JsonObject getProductItem(Product product, boolean includeCategorySpecificProperties) {
        JsonObjectBuilder productBuilder = Json.createObjectBuilder().
                add(FieldNames.ID, product.getUniqueId().toString()).
                add(FieldNames.PRODUCT_PROD_NO, product.getCatalogUniqueNumber().getUniqueId().toString()).
                add(FieldNames.CATEGORY_ID, product.getCategory().getCode()).
                add(FieldNames.CATEGORY_TEXT, product.getCategory().getName()).
                add(FieldNames.PRODUCT_NAME, product.getProductName()).
                add(FieldNames.PRODUCT_IS_TEMPLATE_PRODUCT, Boolean.TRUE);
        
        // product supplier 
        productBuilder.add(FieldNames.PRODUCT_SUPPLIER, Json.createObjectBuilder().
                add(FieldNames.ID, product.getOrganization().getUniqueId().toString()).
                add(FieldNames.LABEL, product.getOrganization().getOrganizationName()).
                build()
        );

        // images
        List<MediaImage> productImages = em.createQuery("SELECT m FROM MediaImage m WHERE m.product.uniqueId = :productUniqueId").
                setParameter("productUniqueId", product.getUniqueId()).
                getResultList();
        
        if( productImages != null ) {
            JsonArrayBuilder imagesBuilder = Json.createArrayBuilder();
            for( MediaImage mediaImage : productImages ) {
                if( mediaImage.isMainImage() ) {
                    productBuilder.add(FieldNames.PRODUCT_MAIN_IMAGE, getMediaItem(mediaImage));
                } else {
                    // under images, each image must be under a File field
                    JsonObjectBuilder fileBuilder = Json.createObjectBuilder();
                    JsonObject mediaObject = getMediaItem(mediaImage);
                    fileBuilder.add(FieldNames.PRODUCT_FILE, mediaObject);
                    imagesBuilder.add(fileBuilder.build());
                }
            }
            productBuilder.add(FieldNames.PRODUCT_IMAGES, imagesBuilder.build());
        }
        
        // documents
        List<MediaDocument> productDocuments = em.createQuery("SELECT m FROM MediaDocument m WHERE m.product.uniqueId = :productUniqueId").
                setParameter("productUniqueId", product.getUniqueId()).
                getResultList();
        
        if( productDocuments != null ) {
            JsonArrayBuilder documentsBuilder = Json.createArrayBuilder();
            for( MediaDocument mediaDocument : productDocuments ) {
                documentsBuilder.add(getMediaItem(mediaDocument));
            }
            productBuilder.add(FieldNames.PRODUCT_DOCUMENTS, documentsBuilder.build());
        }
        
        productBuilder.add(FieldNames.PRODUCT_CLASSIFICATION, Json.createObjectBuilder().
                add(FieldNames.ID, product.getCategory().getUniqueId().toString()).
                add(FieldNames.LABEL, product.getCategory().getCode()).
                build());
        
        // ce
        productBuilder.add(FieldNames.PRODUCT_CE, Json.createObjectBuilder().
                add(FieldNames.PRODUCT_CE_MARKED, product.isCeMarked()).
                build());
                
        if( includeCategorySpecificProperties ) {
            // category specific properties. only include the ones which have a value
            for( ResourceSpecificPropertyValue resourceSpecificPropertyValue : product.getResourceSpecificPropertyValues() ) {
                addResourceSpecificPropertyValue(resourceSpecificPropertyValue, productBuilder);
            }
        }
        
        return productBuilder.build();
    }
    
    
    private JsonObject getArticleItem(Article article, boolean includeCategorySpecificProperties) {
        // articles here are always based on a product so we get category from it
        JsonObjectBuilder articleBuilder = Json.createObjectBuilder().
                add(FieldNames.ID, article.getUniqueId().toString()).
                add(FieldNames.PRODUCT_PROD_NO, article.getCatalogUniqueNumber().getUniqueId().toString()).
                add(FieldNames.CATEGORY_ID, article.getBasedOnProduct().getCategory().getCode()).
                add(FieldNames.CATEGORY_TEXT, article.getBasedOnProduct().getCategory().getName()).
                add(FieldNames.PRODUCT_NAME, article.getArticleName()).
                add(FieldNames.PRODUCT_IS_TEMPLATE_PRODUCT, Boolean.FALSE);
        
        // supplier 
        articleBuilder.add(FieldNames.PRODUCT_SUPPLIER, Json.createObjectBuilder().
                add(FieldNames.ID, article.getOrganization().getUniqueId().toString()).
                add(FieldNames.LABEL, article.getOrganization().getOrganizationName()).
                build()
        );

        // images
        List<ArticleMediaImage> articleImages = em.createQuery("SELECT a FROM ArticleMediaImage a WHERE a.article.uniqueId = :articleUniqueId").
                setParameter("articleUniqueId", article.getUniqueId()).
                getResultList();
        
        if( articleImages != null ) {
            JsonArrayBuilder imagesBuilder = Json.createArrayBuilder();
            for( ArticleMediaImage articleMediaImage : articleImages ) {
                if( articleMediaImage.getMediaImage().isMainImage() ) {
                    articleBuilder.add(FieldNames.PRODUCT_MAIN_IMAGE, getMediaItem(articleMediaImage.getMediaImage()));
                } else {
                    // under images, each image must be under a File field
                    JsonObjectBuilder fileBuilder = Json.createObjectBuilder();
                    JsonObject mediaObject = getMediaItem(articleMediaImage.getMediaImage());
                    fileBuilder.add(FieldNames.PRODUCT_FILE, mediaObject);
                    imagesBuilder.add(fileBuilder.build());
                }
            }
            articleBuilder.add(FieldNames.PRODUCT_IMAGES, imagesBuilder.build());
        }
        
        // documents
        List<ArticleMediaDocument> articleDocuments = em.createQuery("SELECT a FROM ArticleMediaDocument a WHERE a.article.uniqueId = :articleUniqueId").
                setParameter("articleUniqueId", article.getUniqueId()).
                getResultList();
        
        if( articleDocuments != null ) {
            JsonArrayBuilder documentsBuilder = Json.createArrayBuilder();
            for( ArticleMediaDocument articleMediaDocument : articleDocuments ) {
                documentsBuilder.add(getMediaItem(articleMediaDocument.getMediaDocument()));
            }
            articleBuilder.add(FieldNames.PRODUCT_DOCUMENTS, documentsBuilder.build());
        }
        
        // classification
        // articles here are always based on a product so we get category from it
        articleBuilder.add(FieldNames.PRODUCT_CLASSIFICATION, Json.createObjectBuilder().
                add(FieldNames.ID, article.getBasedOnProduct().getCategory().getUniqueId().toString()).
                add(FieldNames.LABEL, article.getBasedOnProduct().getCategory().getCode()).
                build());
        
        // ce, ce marked may be inherited
        // ce
        articleBuilder.add(FieldNames.PRODUCT_CE, Json.createObjectBuilder().
                add(FieldNames.PRODUCT_CE_MARKED, article.isCeMarkedOverridden() ? article.isCeMarked(): article.getBasedOnProduct().isCeMarked()).
                build());
        
        // based on product
        articleBuilder.add(FieldNames.PRODUCT_BASED_ON_PRODUCT, Json.createObjectBuilder().
                add(FieldNames.ID, article.getBasedOnProduct().getUniqueId().toString()).
                add(FieldNames.LABEL, article.getBasedOnProduct().getProductName()).
                build());
        
        if( includeCategorySpecificProperties ) {
            // category specific properties. only include the ones which have a value
            // must also check if the product this article is based on have any not overridden
            List<Long> categorySpecificPropertiesAdded = new ArrayList<>();
            for( ResourceSpecificPropertyValue articleResourceSpecificPropertyValue : article.getResourceSpecificPropertyValues() ) {
                addResourceSpecificPropertyValue(articleResourceSpecificPropertyValue, articleBuilder);
                categorySpecificPropertiesAdded.add(articleResourceSpecificPropertyValue.getCategorySpecificProperty().getUniqueId());
            }
            for( ResourceSpecificPropertyValue productResourceSpecificPropertyValue : article.getBasedOnProduct().getResourceSpecificPropertyValues() ) {
                // only add if the category of the resource specific property value wasn't already added
                if( !categorySpecificPropertiesAdded.contains(productResourceSpecificPropertyValue.getCategorySpecificProperty().getUniqueId()) ) {
                    addResourceSpecificPropertyValue(productResourceSpecificPropertyValue, articleBuilder);
                }
            }
        }
        
        return articleBuilder.build();
    }
        
    private void addResourceSpecificPropertyValue(ResourceSpecificPropertyValue resourceSpecificPropertyValue, JsonObjectBuilder objectBuilder) {
        CategorySpecificProperty categorySpecificProperty = resourceSpecificPropertyValue.getCategorySpecificProperty();
        String fieldName = FieldNames.PRODUCT_CATEGORY_SPECIFIC_PROPERTY_PREFIX + categorySpecificProperty.getUniqueId();
        if( categorySpecificProperty.getType() == CategorySpecificProperty.Type.DECIMAL ) {
            ResourceSpecificPropertyValueDecimal resourceSpecificPropertyValueDecimal = (ResourceSpecificPropertyValueDecimal) resourceSpecificPropertyValue;
            objectBuilder.add(fieldName, resourceSpecificPropertyValueDecimal.getValue());
        } else if( categorySpecificProperty.getType() == CategorySpecificProperty.Type.VALUELIST_SINGLE ) {
            ResourceSpecificPropertyValueValueListSingle resourceSpecificPropertyValueValueListSingle = (ResourceSpecificPropertyValueValueListSingle) resourceSpecificPropertyValue;
            objectBuilder.add(fieldName, Json.createObjectBuilder().
                        add(FieldNames.LABEL, resourceSpecificPropertyValueValueListSingle.getValue().getValue())
            );
        } else if( categorySpecificProperty.getType() == CategorySpecificProperty.Type.VALUELIST_MULTIPLE ) {
            ResourceSpecificPropertyValueValueListMultiple resourceSpecificPropertyValueValueListMultiple = (ResourceSpecificPropertyValueValueListMultiple) resourceSpecificPropertyValue;
            JsonArrayBuilder valuesBuilder = Json.createArrayBuilder();
            for( CategorySpecificPropertyListValue categorySpecificPropertyListValue : resourceSpecificPropertyValueValueListMultiple.getValues() ) {
                valuesBuilder.add(Json.createObjectBuilder().
                        add(FieldNames.LABEL, categorySpecificPropertyListValue.getValue()).
                        build()                            
                );
            }
            objectBuilder.add(fieldName, valuesBuilder.build());
        } else if( categorySpecificProperty.getType() == CategorySpecificProperty.Type.INTERVAL ) {
            ResourceSpecificPropertyValueInterval resourceSpecificPropertyValueInterval = (ResourceSpecificPropertyValueInterval) resourceSpecificPropertyValue;
            objectBuilder.add(fieldName, Json.createObjectBuilder().
                    add(FieldNames.PRODUCT_CATEGORY_SPECIFIC_PROPERTY_MIN, resourceSpecificPropertyValueInterval.getFromValue()).
                    add(FieldNames.PRODUCT_CATEGORY_SPECIFIC_PROPERTY_MAX, resourceSpecificPropertyValueInterval.getToValue()).
                    build());
        }
    }    
    
    private JsonArray searchOrganization(SearchRequestAPI searchRequestAPI) {
        LOG.log( Level.FINEST, "searchOrganization(...)");
        if( searchRequestAPI.getIdList() == null || searchRequestAPI.getIdList().isEmpty() ) {
            LOG.log( Level.WARNING, "Failed to search organization. No valid organization id found" );
            return null;
        }
        Long organizationId;
        try {
            organizationId = Long.parseLong(searchRequestAPI.getIdList().get(0));
        } catch( NumberFormatException e ) {
            LOG.log( Level.WARNING, "Failed to search organization. No numeric organization unique id found" );
            return null;
        }
        Organization organization = em.find(Organization.class, organizationId);
        if( organization == null ) {
            LOG.log( Level.WARNING, "Failed to search organization. No organization for unique id found" );
            return null;
        }
        
        JsonObjectBuilder organizationBuilder = Json.createObjectBuilder().
                add(FieldNames.ID, organization.getUniqueId().toString()).
                add(FieldNames.CATEGORY_ID, "Organization").
                add(FieldNames.CATEGORY_TEXT, "Organisation").
                add(FieldNames.NAME, organization.getOrganizationName()).
                add(FieldNames.ORGANIZATION_GLN, organization.getGln());
        
        // address
        JsonObjectBuilder addressBuilder = Json.createObjectBuilder();
        if( organization.getPostAddresses() != null && !organization.getPostAddresses().isEmpty() ) {
            for( PostAddress postAddress : organization.getPostAddresses() ) {
                if( postAddress.getAddressType() == PostAddress.AddressType.VISIT ) {
                    addressBuilder.
                            add(FieldNames.ORGANIZATION_ADDRESS1, postAddress.getStreetAddress()).
                            add(FieldNames.ORGANIZATION_TOWN_CITY, postAddress.getCity()).
                            add(FieldNames.ORGANIZATION_POSTAL_CODE, postAddress.getPostCode());
                    break;
                }
            }
        }
        
        // phone number, if any
        if( organization.getElectronicAddress() != null && organization.getElectronicAddress().getTelephone() != null ) {
            addressBuilder.add(FieldNames.ORGANIZATION_PHONE_NUMBER, organization.getElectronicAddress().getTelephone());
        }

        // country
        addressBuilder.add(FieldNames.ORGANIZATION_COUNTRY, Json.createObjectBuilder().
                add(FieldNames.ID, organization.getCountry().getUniqueId().toString()).
                add(FieldNames.LABEL, organization.getCountry().getName()).
                build());
        organizationBuilder.add(FieldNames.ORGANIZATION_ADDRESS, addressBuilder.build());
        
        // economic information
        organizationBuilder.add(FieldNames.ORGANIZATION_ECONOMIC_INFORMATION, Json.createObjectBuilder().
                add(FieldNames.ORGANIZATION_NUMBER, organization.getOrganizationNumber()).
                build());
        
        return Json.createArrayBuilder().add(organizationBuilder.build()).build();
    }
        
    private JsonArray searchCountyMunicipality(SearchRequestAPI searchRequestAPI) {
        LOG.log( Level.FINEST, "searchCountyMunicipality(...)");
        JsonArrayBuilder items = Json.createArrayBuilder();
        boolean sortDescending = isSortDescending(searchRequestAPI);
        String sortField = getSortField(searchRequestAPI);
        List<CVCounty> countys = getCounties(sortDescending, sortField);
        for( CVCounty county : countys ) {
            items.add(getCountyCodeItem(county));
            if( county.getShowMunicipalities() && county.getMunicipalities() != null ) {
                for( CVMunicipality municipality : county.getMunicipalities() ) {
                    items.add(getMunicipalityCodeItem(municipality, county));
                }
            }
        }
        return items.build();
    }
    
    private JsonObject getCountyCodeItem(CVCounty county) {
        LOG.log( Level.FINEST, "getCountyMunicipaltyCodeItem(...)");
        return Json.createObjectBuilder().
                add(FieldNames.ID, county.getUniqueId().toString()).
                add(FieldNames.NAME, county.getName()).
                add(FieldNames.REGION_CODE, county.getCode()).
                build();
    }
    
    private JsonObject getMunicipalityCodeItem(CVMunicipality municipality, CVCounty county) {
        return Json.createObjectBuilder().
                add(FieldNames.ID, municipality.getUniqueId().toString()).
                add(FieldNames.NAME, municipality.getName()).
                add(FieldNames.REGION_CODE, municipality.getCode()).
                add(FieldNames.REGION_PARENT, Json.createObjectBuilder().
                        add(FieldNames.ID, county.getUniqueId().toString()).
                        build()).
                build();
    }
    
    /**
     * Search all categories in the system
     * 
     * @param searchRequestAPI
     * @return 
     */
    private JsonArray searchClassification() {
        LOG.log( Level.FINEST, "searchClassification(...)");
        JsonArrayBuilder items = Json.createArrayBuilder();     
        List<Category> categories = em.createQuery("SELECT c FROM Category c ORDER BY c.code ASC").
                getResultList();
        if( categories != null && !categories.isEmpty() ) {
            for( Category category : categories ) {
                items.add(getClassificationItem(category));
            }
        }
        return items.build();
    }    
    
    private JsonObject getClassificationItem(Category category) {
        LOG.log( Level.FINEST, "getMenuTreeTopNode(...)");
        JsonObjectBuilder classificationItemBuilder = Json.createObjectBuilder().
                add(FieldNames.ID, category.getUniqueId().toString()).
                add(FieldNames.NAME, category.getName()).
                add(FieldNames.DESCRIPTION, category.getDescription());
        if( category.getCode() != null ) {
            classificationItemBuilder.add(FieldNames.CLASSIFICATION_CODE, category.getCode());
        }
        if( category.getParent() != null ) {
            classificationItemBuilder.add(FieldNames.CLASSIFICATION_PARENT, Json.createObjectBuilder().
                    add(FieldNames.ID, category.getParent().getUniqueId().toString()).
                    build());
        }
        return classificationItemBuilder.build();
    }
    
    
    private JsonArray searchMenuTree(SearchRequestAPI searchRequestAPI) {
        LOG.log( Level.FINEST, "searchMenuTree(...)");
        boolean sortDescending = isSortDescending(searchRequestAPI);
        String sortField = getSortField(searchRequestAPI);
        JsonArrayBuilder items = Json.createArrayBuilder();        
        items.add(getMenuTreeTopNode());
        List<ExternalCategoryGrouping> externalCategoryGroupings = getExternalCategoryGroupings(
                sortDescending, 
                sortField);
        for( ExternalCategoryGrouping externalCategoryGrouping : externalCategoryGroupings ) {
            items.add(getMenuTreeItem(externalCategoryGrouping));
        }
        return items.build();
    }
    
    private JsonObject getMenuTreeTopNode() {
        LOG.log( Level.FINEST, "getMenuTreeTopNode(...)");
        return Json.createObjectBuilder().
                add(FieldNames.ID, "c,0," + CATEGORY_ID_MENUTREE). // fictional id
                add(FieldNames.CATEGORY_ID, CATEGORY_ID_MENUTREE).
                add(FieldNames.NAME, baseNavigationLevelName).
                add(FieldNames.MENU_TREE_DESCRIPTION, baseNavigationLevelDescription).
                build();
    }
    
    private JsonObject getMenuTreeItem(ExternalCategoryGrouping externalCategoryGrouping) {
        LOG.log( Level.FINEST, "getMenuTreeItem(...)");
        JsonObjectBuilder menuItemBuilder = Json.createObjectBuilder().
                add(FieldNames.ID, "c," + externalCategoryGrouping.getUniqueId().toString() + "," + CATEGORY_ID_MENUTREE).
                add(FieldNames.CATEGORY_ID, CATEGORY_ID_MENUTREE).
                add(FieldNames.NAME, externalCategoryGrouping.getName()).
                add(FieldNames.MENU_TREE_DESCRIPTION, externalCategoryGrouping.getDescription()).
                add(FieldNames.MENU_TREE_SORT_ORDER,externalCategoryGrouping.getDisplayOrder());
        
        // parent
        JsonObjectBuilder parentItemBuilder = Json.createObjectBuilder();
        if( externalCategoryGrouping.getParent() != null ) {
            parentItemBuilder.
                    add(FieldNames.ID, "c," + externalCategoryGrouping.getParent().getUniqueId().toString() + "," + CATEGORY_ID_MENUTREE).
                    add(FieldNames.LABEL, externalCategoryGrouping.getParent().getName());
        } else {
            parentItemBuilder.
                    add(FieldNames.ID, "c,0," + CATEGORY_ID_MENUTREE).
                    add(FieldNames.LABEL, baseNavigationLevelName);
        }
        menuItemBuilder.add(FieldNames.MENU_TREE_PARENT_ITEM, parentItemBuilder.build());
        
        // categories
        List<Category> childCategories = getExternalCategoryGroupingChildrenWithCategory(externalCategoryGrouping.getUniqueId());
        if( childCategories != null && !childCategories.isEmpty() ) {
            JsonArrayBuilder categoriesBuilder = Json.createArrayBuilder();
            for( Category category : childCategories ) {
                categoriesBuilder.add(Json.createObjectBuilder().
                                add(FieldNames.ID, category.getUniqueId().toString()).
                                add(FieldNames.LABEL, category.getCode()));
            }
            menuItemBuilder.add(FieldNames.MENU_TREE_CATEGORIES, categoriesBuilder.build());
        }
        return menuItemBuilder.build();
    }
            
    private JsonObject getMediaItem(Media media) {
        JsonObjectBuilder mediaBuilder = Json.createObjectBuilder().
                add(FieldNames.PRODUCT_URL, media.getUrl());
        try {
            String fileEnding = getFileEnding(new URL(media.getUrl()));
            if( fileEnding == null ) {
                LOG.log( Level.WARNING, "Url of media {0} does not have a valid file ending", new Object[]{media.getUniqueId()});
            } else {
                String mimeType = fileEndingsToMimeTypesMap.get(fileEnding);
                if( mimeType != null ) {
                    mediaBuilder.add(FieldNames.PRODUCT_MIME_TYPE, mimeType);
                } else {
                    LOG.log( Level.WARNING, "File ending {0} does not have a valid mime type configured", new Object[]{fileEnding});
                }
            }
            
            if( media instanceof MediaImage ) {
                // scaled images, only 73 and 740 sizes are required 
                JsonArrayBuilder scaledImagesBuilder = Json.createArrayBuilder();
                String url73 = media.getUrl() + (media.getUrl().contains("?") ? "&": "?" + "size=73");
                String url740 = media.getUrl() + (media.getUrl().contains("?") ? "&": "?" + "size=740");
                scaledImagesBuilder.add(Json.createObjectBuilder().
                        add(FieldNames.PRODUCT_URL, url73).
                        add(FieldNames.PRODUCT_MAXWIDTH, 73)
                );
                scaledImagesBuilder.add(Json.createObjectBuilder().
                        add(FieldNames.PRODUCT_URL, url740).
                        add(FieldNames.PRODUCT_MAXWIDTH, 740)
                );
                mediaBuilder.add(FieldNames.PRODUCT_SCALED_IMAGES, scaledImagesBuilder.build());
            }
        } catch ( MalformedURLException e ) {
            LOG.log( Level.WARNING, "Url of media {0} does not have a valid file ending", new Object[]{media.getUniqueId()});
        }
        return mediaBuilder.build();            
    }
    
    public String getFileEnding(URL url) {
        String fileName = url.getPath().substring(url.getPath().lastIndexOf("/")+1, url.getPath().length());
        String fileEnding = null;
        if(fileName.lastIndexOf(".") != -1) {
            fileEnding = fileName.substring(fileName.lastIndexOf(".")+1, fileName.length() );
        }
        return fileEnding;
    }
    
    private List<Long> getCategoryIdsFromMatchIds(List<String> matchIds) {
        List<Long> categoryIds = new ArrayList<>();
        for( String matchId : matchIds ) {
            try {
                Long categoryId = Long.parseLong(matchId);
                Category category = em.find(Category.class, categoryId);
                if( category == null ) {
                    LOG.log( Level.WARNING, "Failed to search product. Category cannot be found" );
                    continue;
                }
                if( category.getArticleType() == null ) {
                    // not a leaf node, we need to find all child categories
                    List<Category> childCategories = em.createQuery("SELECT c FROM Category c WHERE c.parent.uniqueId = :categoryUniqueId").
                            setParameter("categoryUniqueId", category.getUniqueId()).
                            getResultList();
                    if( childCategories != null ) {
                        for( Category childCategory : childCategories ) {
                            categoryIds.add(childCategory.getUniqueId());
                        }
                    }
                }
                categoryIds.add(Long.parseLong(matchId));
            } catch( NumberFormatException e ) {
                LOG.log( Level.WARNING, "Failed to search product. Category Id is not a valid number" );
                continue;
            }
        }
        return categoryIds;
    }
    
    private List<ExternalCategoryGrouping> getExternalCategoryGroupings( 
            boolean sortDescending, 
            String sortField) {
        LOG.log( Level.FINEST, "getExternalCategoryGroupings(...)");
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT ecg FROM ExternalCategoryGrouping ecg WHERE ecg.category IS NULL");
        if( sortField != null ) {
            if ( SORT_ORDER.equals(sortField) ) {
                queryBuilder.append(" ORDER BY ecg.displayOrder");
                if( sortDescending ) {
                    queryBuilder.append(" DESC");
                } else {
                    queryBuilder.append(" ASC");
                }
            }
            
        }
        LOG.log( Level.FINEST, "query: {0}", new Object[] {queryBuilder.toString()});
        return em.createQuery(queryBuilder.toString()).getResultList();
    }

    private List<Category> getExternalCategoryGroupingChildrenWithCategory(long externalCategoryGroupingUniqueId) {
        LOG.log( Level.FINEST, "getExternalCategoryGroupings(...)");
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT ecg.category FROM ExternalCategoryGrouping ecg "
                + "WHERE ecg.parent.uniqueId = :externalCategoryGroupingUniqueId AND ecg.category IS NOT NULL "
                + "ORDER BY ecg.displayOrder ASC");
        LOG.log( Level.FINEST, "query: {0}", new Object[] {queryBuilder.toString()});
        return em.createQuery(queryBuilder.toString()).
                setParameter("externalCategoryGroupingUniqueId", externalCategoryGroupingUniqueId).
                getResultList();
    }
    
    private List<CVCounty> getCounties(boolean sortDescending, 
            String sortField) {
        LOG.log( Level.FINEST, "getCounties(...)");
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("SELECT c FROM CVCounty c");
        if( sortField != null ) {
            if ( SORT_CODE.equals(sortField) ) {
                queryBuilder.append(" ORDER BY c.code");
                if( sortDescending ) {
                    queryBuilder.append(" DESC");
                } else {
                    queryBuilder.append(" ASC");
                }
            }
        }
        LOG.log( Level.FINEST, "query: {0}", new Object[] {queryBuilder.toString()});
        return em.createQuery(queryBuilder.toString()).getResultList();
    }
    
    private String getSortField(SearchRequestAPI searchRequestAPI) {
        if( searchRequestAPI.getOrderBys() != null && 
                !searchRequestAPI.getOrderBys().isEmpty() && 
                searchRequestAPI.getOrderBys().get(0).getIdPath() != null ) {
            String orderByIdPath = searchRequestAPI.getOrderBys().get(0).getIdPath().get(0);
            if( FieldNames.MENU_TREE_SORT_ORDER.equals(orderByIdPath) ) {
                return SORT_ORDER;
            } else if( FieldNames.REGION_CODE.equals(orderByIdPath) ) {
                return SORT_CODE;
            } else if( FieldNames.PRODUCT_NAME.equals(orderByIdPath) ) {
                return SORT_PRODUCT_NAME;
            }
        }
        return SORT_NAME;
    }

    private boolean isSortDescending(SearchRequestAPI searchRequestAPI) {
        if( searchRequestAPI.getOrderBys() != null && 
                !searchRequestAPI.getOrderBys().isEmpty() && 
                searchRequestAPI.getOrderBys().get(0).getAscending() != null &&
                searchRequestAPI.getOrderBys().get(0).getAscending().equals("true") ) {
            return false;
        }
        return true;
    }
    
    @PostConstruct
    private void initialize() {
        if( fileEndingsToMimeTypes == null || fileEndingsToMimeTypes.isEmpty() ) {
            LOG.log(Level.WARNING, "Missing configuration fileEndingsToMimeTypes to map file endings to mime types");
            return;
        }
        String[] fileEndingsToMimeTypesArray = fileEndingsToMimeTypes.split(",");
        for( String fileEndingToMimeType: fileEndingsToMimeTypesArray ) {
            String[] fileEndingToMimeTypeArray = fileEndingToMimeType.split(":");
            fileEndingsToMimeTypesMap.put(fileEndingToMimeTypeArray[0], fileEndingToMimeTypeArray[1]);
        }
    }
    
}

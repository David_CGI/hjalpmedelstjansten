/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.externalapi.search.controller;

/**
 * Names of fields in the api. 
 * 
 * @author Tommy Berglund
 */
public class FieldNames {
    
    public static final String ID = "Id";
    public static final String TOTAL_HITS = "TotalHits";
    public static final String CATEGORY_ID = "CategoryId";
    public static final String CATEGORY_TEXT = "CategoryText";
    public static final String NAME = "Name";
    public static final String LABEL = "Label";
    public static final String ITEMS = "Items";
    public static final String DESCRIPTION = "Description";
    
    // menutree specific
    public static final String MENU_TREE_DESCRIPTION = "939c8fcfa88c4c80b23ee0fb7dc7f1f8";
    public static final String MENU_TREE_SORT_ORDER = "5a551ac606f4471e91a7b06ccd8cb6da";
    public static final String MENU_TREE_PARENT_ITEM = "Parentmenutree";
    public static final String MENU_TREE_CATEGORIES = "eb7bf404257a467b8286d8b75085f576";
    
    // classification specific
    public static final String CLASSIFICATION_CODE = "Code";
    public static final String CLASSIFICATION_PARENT = "ParentClassification";
    
    // region specific
    public static final String REGION_CODE = "3d0e2aa3c20148e481e6ba3fcc9838a3";
    public static final String REGION_PARENT = "ParentCountyMunicipaltyCode";
    
    // organization specific
    public static final String ORGANIZATION_GLN = "GLN";
    public static final String ORGANIZATION_ADDRESS = "Address";
    public static final String ORGANIZATION_ADDRESS1 = "Address1";
    public static final String ORGANIZATION_POSTAL_CODE = "PostalCode";
    public static final String ORGANIZATION_TOWN_CITY = "TownCity";
    public static final String ORGANIZATION_COUNTRY = "Country";
    public static final String ORGANIZATION_PHONE_NUMBER = "PhoneNo";
    public static final String ORGANIZATION_ECONOMIC_INFORMATION = "EconomicInformation";
    public static final String ORGANIZATION_NUMBER = "OrganizationNo";
    
    // product specific
    public static final String PRODUCT_PROD_NO = "VGRProdNo";
    public static final String PRODUCT_NAME = "SupplierProductName";
    public static final String PRODUCT_IS_TEMPLATE_PRODUCT = "IsTemplateProduct";
    public static final String PRODUCT_SUPPLIER = "Supplier";
    public static final String PRODUCT_MAIN_IMAGE = "Bild";
    public static final String PRODUCT_IMAGES = "Images";
    public static final String PRODUCT_DOCUMENTS = "Documents";
    public static final String PRODUCT_URL = "Url";
    public static final String PRODUCT_MIME_TYPE = "MimeType";
    public static final String PRODUCT_CE = "17fa973736524d658b7b944561195907";
    public static final String PRODUCT_CE_MARKED = "CEmark";
    public static final String PRODUCT_CLASSIFICATION = "Classification";
    public static final String PRODUCT_BASED_ON_PRODUCT = "BasedOnTemplateProduct";
    public static final String PRODUCT_CATEGORY_SPECIFIC_PROPERTY_PREFIX = "isoattr_";
    public static final String PRODUCT_CATEGORY_SPECIFIC_PROPERTY_MIN = "Min";
    public static final String PRODUCT_CATEGORY_SPECIFIC_PROPERTY_MAX = "Max";
    public static final String PRODUCT_MAXWIDTH = "MaxWidth";
    public static final String PRODUCT_SCALED_IMAGES = "ScaledImages";
    public static final String PRODUCT_FILE = "File";
    
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package se.inera.hjalpmedelstjansten.externalapi.search.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;
import se.inera.hjalpmedelstjansten.model.api.external.FieldRequestAPI;
import se.inera.hjalpmedelstjansten.model.api.external.FieldResponseItemAPI;
import se.inera.hjalpmedelstjansten.model.entity.CategorySpecificProperty;
import se.inera.hjalpmedelstjansten.property.AppConfigurationService;

/**
 *
 * @author Tommy Berglund
 */
@Stateless
public class APIFieldController {

    private static final String ID_PATH_MAIN_PART_ID = "Id";
    private static final String ID_PATH_MAIN_PART_CATEGORY_ID = "CategoryId";
    private static final String ID_PATH_MAIN_PART_CATEGORY_TEXT = "CategoryText";
    private static final String ID_PATH_EXTENSION_PART_ID = "Id";
    private static final String ID_PATH_EXTENSION_PART_LABEL = "Label";
    
    private static final String ISO_ATTR_PREFIX = "isoattr_";
    
    @Inject
    HjmtLogger LOG;
    
    @Inject
    AppConfigurationService appConfigurationService;
    
    @PersistenceContext( unitName = "HjmtjPU")
    EntityManager em;
        
    public List<FieldResponseItemAPI> search(FieldRequestAPI fieldRequestAPI) {
        LOG.log( Level.FINEST, "search(...)");
        List<FieldResponseItemAPI> fieldResponseItemAPIs = new ArrayList<>();
        if( fieldRequestAPI.getIdPathList() != null && !fieldRequestAPI.getIdPathList().isEmpty() ) {
            for( List<String> idPath : fieldRequestAPI.getIdPathList() ) {
                FieldResponseItemAPI fieldResponseItemAPI = handleIdPathItem(idPath);
                if( fieldResponseItemAPI != null ) {
                    fieldResponseItemAPIs.add(fieldResponseItemAPI);
                }
            }
        }
        return fieldResponseItemAPIs;
    }
       
    private FieldResponseItemAPI handleIdPathItem(List<String> idPath) {
        if( idPath != null && !idPath.isEmpty() ) {
            if( idPath.size() == 1 ) {
                return handleOneIdPathItem(idPath);
            } else if( idPath.size() == 2 ) {
                return handleTwoIdPathItem(idPath);
            } else {
                LOG.log( Level.WARNING, "Id path has more than two parts which is not handled." );
            }
        } else {
            LOG.log( Level.WARNING, "Id path is null or empty which is not handled." );
        }
        return null;
    }
    
    private FieldResponseItemAPI handleOneIdPathItem(List<String> idPathList) {
        String idPath = idPathList.get(0);
        if( idPath.equals(ID_PATH_MAIN_PART_ID) ||
                idPath.equals(ID_PATH_MAIN_PART_CATEGORY_ID) ||
                idPath.equals(ID_PATH_MAIN_PART_CATEGORY_TEXT)) {
            // these are not returned
            return null;
        }
        FieldResponseItemAPI fieldResponseItemAPI = new FieldResponseItemAPI();
        fieldResponseItemAPI.setIdPath(idPathList);
        if( idPath.startsWith(ISO_ATTR_PREFIX) ) {
            return handleIsoPathItem(idPath, idPathList);
        } else {
            String idPathName = appConfigurationService.getString("field." + idPath + ".name");
            if( idPathName != null ) {
                fieldResponseItemAPI.setName(idPathName);
                String idPathValueType = appConfigurationService.getString("field." + idPath + ".valuetype");
                fieldResponseItemAPI.setValueType(idPathValueType == null || idPathValueType.isEmpty() ? null: FieldResponseItemAPI.ValueType.valueOf(idPathValueType).getActualName());
            } else {
                LOG.log( Level.WARNING, "Missing properties name and/or valuetype for field with idpath {0}", new Object[] {idPath});
            }
        }
        return fieldResponseItemAPI;
    }
    
    private FieldResponseItemAPI handleTwoIdPathItem(List<String> idPathList) {
        String idPathExtensionPart = idPathList.get(1);
        if( idPathExtensionPart.equals(ID_PATH_EXTENSION_PART_ID) || 
                idPathExtensionPart.equals(ID_PATH_EXTENSION_PART_LABEL) ) {
            // these are not handled
            return null;
        }
        FieldResponseItemAPI fieldResponseItemAPI = new FieldResponseItemAPI();
        fieldResponseItemAPI.setIdPath(idPathList);
        String idPath = idPathList.get(0);
        String idPathExtension = idPathList.get(1);
        String idPathExtensionName = appConfigurationService.getString("field." + idPath + "." + idPathExtension + ".name");
        if( idPathExtensionName != null ) {
            fieldResponseItemAPI.setName(idPathExtensionName);
            String idPathValueType = appConfigurationService.getString("field."  + idPath + "." + idPathExtension + ".valuetype");
            fieldResponseItemAPI.setValueType(idPathValueType == null || idPathValueType.isEmpty() ? null: FieldResponseItemAPI.ValueType.valueOf(idPathValueType).getActualName());
        } else {
            LOG.log( Level.WARNING, "Missing properties name and/or valuetype for field with idpath {0}", new Object[] {idPath + "->" + idPathExtension});
        }
        return fieldResponseItemAPI;
    }
    
    private FieldResponseItemAPI handleIsoPathItem(String idPath, List<String> idPathList) {
        Long categorySpecificPropertyUniqueId = Long.parseLong(idPath.substring(ISO_ATTR_PREFIX.length()));
        CategorySpecificProperty categorySpecificProperty = em.find(CategorySpecificProperty.class, categorySpecificPropertyUniqueId);
        if( categorySpecificProperty == null ) {
            LOG.log( Level.WARNING, "Request for category specific property with id {0} which does not exist", new Object[] {categorySpecificPropertyUniqueId});
            return null;
        }
        FieldResponseItemAPI fieldResponseItemAPI = new FieldResponseItemAPI();
        fieldResponseItemAPI.setIdPath(idPathList);
        if( categorySpecificProperty.getType() == CategorySpecificProperty.Type.DECIMAL ) {
            fieldResponseItemAPI.setValueType(FieldResponseItemAPI.ValueType.DECIMAL.getActualName());
        } else if( categorySpecificProperty.getType() == CategorySpecificProperty.Type.INTERVAL ) {
            fieldResponseItemAPI.setValueType(null);
        } else {
            fieldResponseItemAPI.setValueType(FieldResponseItemAPI.ValueType.REFERENCE_TO.getActualName());            
        }
        fieldResponseItemAPI.setName(categorySpecificProperty.getName());
        return fieldResponseItemAPI;
    }
    
}

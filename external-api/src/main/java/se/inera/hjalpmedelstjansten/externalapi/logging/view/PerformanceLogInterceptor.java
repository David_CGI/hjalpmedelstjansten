/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.externalapi.logging.view;

import java.lang.reflect.Modifier;
import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.AroundTimeout;
import javax.interceptor.InvocationContext;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;

/**
 * Interceptor for logging performance of methods
 * 
 * @author Tommy Berglund
 */
public class PerformanceLogInterceptor {
    
    @Inject
    private HjmtLogger LOG;

    @AroundInvoke
    @AroundTimeout
    public Object logPerformance( InvocationContext invocationContext ) throws Exception {
        if( Modifier.isPublic(invocationContext.getMethod().getModifiers()) ) {
            // only log public methods. other REST-class methods should be private
            String className = invocationContext.getMethod().getDeclaringClass().getName();
            String methodName = invocationContext.getMethod().getName();
            String sessionId = null; // no session in external api
            long start = System.currentTimeMillis();
            try {
                return invocationContext.proceed();
            } finally {
                long total = System.currentTimeMillis() - start;
                LOG.logPerformance(className, methodName, total, sessionId);
            }
        } else {
            return invocationContext.proceed();
        }
    }
    
}

/*
* Copyright (C) 2018 Inera AB (http://www.inera.se)
*
* This file is part of Hjalpmedelstjansten (https://bitbucket.org/ineraservices/hjalpmedelstjansten).
*
* Hjalpmedelstjansten is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* Hjalpmedelstjansten is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package se.inera.hjalpmedelstjansten.externalapi.search.controller;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;
import se.inera.hjalpmedelstjansten.business.logging.view.HjmtLogger;

/**
 * Filter to log request body
 * 
 * @author Tommy Berglund
 */
@LogRequest
@Provider
public class LogRequestFilter implements ContainerRequestFilter {
    
    @Inject
    HjmtLogger LOG;    
    
    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        try (BufferedReader buffer = new BufferedReader(new InputStreamReader(containerRequestContext.getEntityStream()))) {
            String body = buffer.lines().collect(Collectors.joining("\n"));            
            LOG.log( Level.INFO, "body: {0}", new Object[] {body});
            containerRequestContext.setEntityStream(new ByteArrayInputStream(body.getBytes()));
        }
    }
    
}
